({
    prepareAllCategories : function (component) {
        	    var articleInfo = component.get("v.articleInfo");
        	    var settingInfo = component.get("v.settingInfo");
        		var action = component.get("c.getCaseLayoutWrapper");

        		action.setParams({
        			'recordId' : articleInfo.recordId,
        			'articleType' : articleInfo.type
        		});
        		action.setCallback(this, function(response) {
        			var state = response.getState();
        			if (state === "SUCCESS") {
        			    $A.createComponent(
                                            "c:CaseLayoutSettings",
                                            {
                                                "articleInfo" : articleInfo,
                                                "settingInfo": settingInfo,
                                                "caseLayoutWrapper": response.getReturnValue()
                                            },
                                            function(caseLayoutSettings, status, errorMessage) {

                                                if (status === "SUCCESS") {
                                                    component.set("v.caseLayoutSettings",caseLayoutSettings);
                                                }
                                                else if (status === "INCOMPLETE") {
                                                }
                                                else if (status === "ERROR") {
                                                }
                                            }
                                        );
        			}
        		});
                $A.enqueueAction(action);
        	},
})