({
	render: function(component, helper) {
		var returnVal = this.superRender();
		var colorValue = component.get('v.textAndIconsColor');
		var tabItem = component.get('v.tabItem.cncrg__Icon_Pathes__c');
		if (colorValue && tabItem) {
			var button = returnVal[1];
			if (button) {
				var value = tabItem.replace('{!v.textAndIconsColor}', colorValue);
				button.querySelector('div.icon_svg_content').innerHTML = value; 
			}
		}
		return returnVal;
	}
})