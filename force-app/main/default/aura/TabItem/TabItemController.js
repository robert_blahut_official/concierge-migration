({
	doInit: function(component, event, helper) {
		var tabItem = component.get('v.tabItem');
		var labelReference = $A.getReference("$Label.c." + tabItem.DeveloperName);
		component.set('v.labelName', tabItem.DeveloperName);
		component.set('v.temporaryLabel', labelReference);
		if (tabItem.cncrg__Counter_Component_Name__c) {
			var dynamicArray = [];
			var dynamicSpanElement = {
				"tag": "span",
				"HTMLAttributes": {
					"class": 'internalAlert'
				}
			};
			dynamicArray.push(["aura:html", dynamicSpanElement]);
			dynamicArray.push([tabItem.cncrg__Counter_Component_Name__c, {}]);

			$A.createComponents(
				dynamicArray,
				function(components, status, errorMessage) {
					if (status === "SUCCESS") {
						var pageBody = component.get("v.body");
						var span = components[0];
						var counter = components[1];
						var spanBody = span.get("v.body");
						spanBody.push(counter);
						span.set("v.body", spanBody);
						pageBody.push(span);
						component.set("v.body", pageBody);
					}
				}
			);
		}
	},
	fireEventTabWasClicked: function(component, event, helper) {
		var tabItem = component.get('v.tabItem');
		var appEvent = $A.get("e.c:CloseNewCasePopup");
		if (tabItem.cncrg__Component_API_Name__c &&
			tabItem.cncrg__Component_API_Name__c.indexOf('LogTicket') < 0) {

			var urlWithoutSerach = window.location.href.replace(window.location.search, '');
			window.history.replaceState({}, null, urlWithoutSerach);			
			appEvent.fire();			
		}
		else if (tabItem.cncrg__Component_API_Name__c &&
				 tabItem.cncrg__Component_API_Name__c.indexOf('LogTicket') >= 0) {
				
				component.set('v.showHideNavLeftMini', false);
		}

		$A.get("e.c:ClickedAppTabEvent").setParams({
			"componentName": tabItem.cncrg__Component_API_Name__c
		}).fire();
	},

	createLabel: function(component, event, helper) {
		var result = component.get('v.temporaryLabel');
		if (result) {
			var labelName = component.get('v.labelName');
			if (result.search(/\$Label./) === 0) {
				var labelReference = $A.getReference("$Label.cncrg." + labelName);
				component.set('v.temporaryLabel', labelReference);
			} else {
				component.set('v.label', result);
			}
		}
	}
})