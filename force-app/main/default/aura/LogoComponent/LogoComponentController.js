({
    doInit : function(component, event, helper) {
        helper.setUrlPrefix(component);
        helper.getLogoUrl(component);
        helper.resizeWindow(component);
        helper.getLanguage(component);
		if( window.innerWidth < 1025 ){
			component.set("v.isMobileMode", true);
		}
    }
})