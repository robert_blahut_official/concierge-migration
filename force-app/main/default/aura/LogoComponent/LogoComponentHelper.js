({
    setUrlPrefix : function(component) {
        var currUrl;
        try {
            currUrl = window.location.pathname;
        }
        catch (e) {
            currUrl = $A.get('$Resource.Media');
        }

        var prefixArray = currUrl.split('/');
        if(prefixArray[prefixArray.length - 3]!=='')
            component.set("v.communityPrefix", '/' + prefixArray[prefixArray.length - 3]);
        else
            component.set("v.communityPrefix", '');
    },
    getLogoUrl: function (component) {
        var action = component.get("c.getLogoLastVersionId");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var logoUrl = response.getReturnValue();
                component.set('v.logoUrl',logoUrl);
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                    }
                } else {
                }
            }
        });
        $A.enqueueAction(action);
    },
    resizeWindow: function (component) {
        if( window.innerWidth >= 1025 ){    
            component.set("v.isMobileMode", false);
        } else{             
            component.set("v.isMobileMode", true);
        }
        window.addEventListener('resize', function() {
            if( window.innerWidth >= 1025 ){                    
                component.set("v.isMobileMode", false);     
            }
            else{               
                component.set("v.isMobileMode", true);
            } 
        })
    },
    getLanguage: function (component) {
        var action = component.get("c.checkLanguage");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var language = response.getReturnValue();
                component.set('v.language', language);
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                    }
                } else {
                }
            }
        });
        $A.enqueueAction(action);
    }

})