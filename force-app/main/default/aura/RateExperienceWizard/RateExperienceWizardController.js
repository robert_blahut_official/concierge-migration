({
    handleStarRating : function(component, event) {
        var starRating = event.getParam("starRatingValue");
        component.set("v.currentCase.cncrg__Score__c", starRating);
    },

    getCase : function(component, event) {
        var caseId = event.getParam("CaseId");
        var action = component.get("c.getCurrentCase");
     	action.setParams({
        	 "caseid" : caseId
     	});
       
     	action.setCallback(this, function(response){
         	var state = response.getState();    
         	if (component.isValid() && state === "SUCCESS") {
                component.set("v.currentCase", response.getReturnValue());            
			}
     	});

        $A.enqueueAction(action);

		window.setTimeout(
    		$A.getCallback(function() {
                if (component.isValid()) {
                    var divstarRating = component.find("starRatingId");
                    $A.util.removeClass(divstarRating, "blockHidden");
                    $A.util.addClass(divstarRating, "displayFirst");

                }
    		}), 400
		);

    },

    handleFeedBackNote : function(component, event) {
        var note = event.getParam("noteMessage");      
        component.set("v.currentCase.cncrg__Comments__c", note);
    },

    handleFeedBackSuggestion : function(component, event, helper) {
        var suggestion = event.getParam("noteMessage");
        component.set("v.currentCase.cncrg__Suggestion__c", suggestion);
       
        var checkStarRating =  component.get("v.currentCase.cncrg__Score__c");
      
        if(checkStarRating < 1 || checkStarRating === undefined){
            component.set("v.currentCase.cncrg__Score__c", 1);
        }
        var score = component.get("v.currentCase.cncrg__Score__c");         
        var currCase =  component.get("v.currentCase");
		var action = component.get("c.moveFeedbackToCase");

         action.setParams({
         	"caseWithFeedBack": currCase
         });

         action.setCallback(this, function(response){
         var state = response.getState();

     	});

     	$A.enqueueAction(action);

    },

    setActiveWindow : function(component, event, helper) {
        var activeWindowNumber = event.getParam("nextStepWindow");        
        var divstarRating = component.find("starRatingId");
        var divfeedBackNote = component.find("feedBackNoteId");
        var divSuggestion = component.find("feedBackSuggestionId");
          
        if(activeWindowNumber === "1"){
            $A.util.removeClass(divfeedBackNote, "blockHidden");
            $A.util.addClass(divfeedBackNote, "displayFirst");

            $A.util.removeClass(divstarRating, "displayFirst");
            $A.util.addClass(divstarRating, "blockHidden");

        }else if(activeWindowNumber === "2"){
            $A.util.removeClass(divfeedBackNote, "displayFirst");
            $A.util.addClass(divfeedBackNote, "blockHidden");

            $A.util.removeClass(divSuggestion, "blockHidden");
            $A.util.addClass(divSuggestion, "displayFirst");

            component.set("v.numberOfPreviousWindow", 2);


        }else if(activeWindowNumber === "3"){
            $A.util.removeClass(divSuggestion, "displayFirst");
            $A.util.addClass(divSuggestion, "blockHidden");

            $A.util.removeClass(divfeedBackNote, "blockHidden");
            $A.util.addClass(divfeedBackNote, "displayFirst");

        }else if(activeWindowNumber === "4"){
            $A.util.removeClass(divstarRating, "displayFirst");
            $A.util.addClass(divstarRating, "blockHidden");

        }else if(activeWindowNumber === "5"){

            $A.util.removeClass(divfeedBackNote, "displayFirst");
            $A.util.addClass(divfeedBackNote, "blockHidden");


        }else if(activeWindowNumber === "6"){
            $A.util.removeClass(divSuggestion, "displayFirst");
            $A.util.addClass(divSuggestion, "blockHidden");

        }else if(activeWindowNumber === "7"){
            $A.util.removeClass(divstarRating, "displayFirst");
            $A.util.addClass(divstarRating, "blockHidden");

            $A.util.removeClass(divSuggestion, "blockHidden");
            $A.util.addClass(divSuggestion, "displayFirst");

            component.set("v.numberOfPreviousWindow", 1);

        }else if(activeWindowNumber === "8"){
            $A.util.removeClass(divSuggestion, "displayFirst");
            $A.util.addClass(divSuggestion, "blockHidden");

			$A.util.removeClass(divfeedBackNote, "displayFirst");
            $A.util.addClass(divfeedBackNote, "blockHidden");

            $A.util.removeClass(divstarRating, "blockHidden");
            $A.util.addClass(divstarRating, "displayFirst");

        }    

    }

})