({
	dynamicCreateBlockSettings : function(nameOfComponent, component) {
		var dynamicArray = [];
        var dynamicAtributesElement = {};
        var dynamicElement = ["cncrg:" + nameOfComponent, dynamicAtributesElement];
        dynamicArray.push(dynamicElement);

        $A.createComponents(
            dynamicArray,
            function(components, status, errorMessage){
                if (status === "SUCCESS") {
                    component.set("v.dynamicBlock", components);
                }
                else if (status === "INCOMPLETE") {
                    // Show offline error
                }
                    else if (status === "ERROR") {
                        // Show error message
                    }
            }
        );
	},
	checkProfile : function(component) {
		var action = component.get("c.isCurrentUserAdmin");
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				if(response.getReturnValue()) {
					component.set("v.isCurrentUserAdmin", response.getReturnValue());
				}
				else {
					$A.util.removeClass(component.find("message"), "hidden_div");
				}
				
			} else if (state === "ERROR") {
				var errors = response.getError();
			}
		});
        $A.enqueueAction(action);
	}
})