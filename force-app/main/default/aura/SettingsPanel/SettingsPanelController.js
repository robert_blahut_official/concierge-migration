({
	showConciergeSettings : function(component, event, helper) {
		helper.dynamicCreateBlockSettings('ConciergeSettings', component);
	},
    showArticleSettings : function(component, event, helper) {
		helper.dynamicCreateBlockSettings('ArticleSettings', component);
	},
    showSearchSettings : function(component, event, helper) {
		helper.dynamicCreateBlockSettings('SearchSettings', component);
	},
    showFieldMappings : function(component, event, helper) {
		helper.dynamicCreateBlockSettings('FieldMappings', component);
	},
    showIconsBlock : function(component, event, helper) {
    	component.set("v.dynamicBlock", null);
    },
    doInit : function(component, event, helper) {
    	helper.checkProfile(component);
    },
	showCodeSettings : function(component, event, helper) {
		helper.dynamicCreateBlockSettings('CodeSettings', component);
	},
    showSchedulerSettings : function(component, event, helper) {
        helper.dynamicCreateBlockSettings('SchedulerSettings', component);
    },
	showObjectSearchSettings : function(component, event, helper) {
		 helper.dynamicCreateBlockSettings('ObjectSearchSettings', component);
	}
})