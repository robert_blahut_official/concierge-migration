({
	doInit: function(component, event, helper) {
		helper.doInitSearchInput(component);
	},

	handleInit: function(component, event, helper) {
		var currentQuery = component.get("v.searchQuery");
		component.set("v.searchQueryTemp", currentQuery);
	},

	returnFalse: function(component, event, helper) {
		if (event.keyCode === 40 || event.keyCode === 38) {
			event.preventDefault();
		}
	},

	handleSearchQueryChange: function(component, event, helper) {
		var new_value = component.find("searchQueryInputID").getElement().value;
		var searchQueryInput = component.find("searchQueryInputID");
		var needSuggestion = true;

		component.set("v.searchQuery", new_value);
		var searchInput = document.getElementById("searchQueryInputID").value;
		if (searchInput.length > 0) {
			component.set("v.showMagnifyingGlass", false);
		} else {
			component.set("v.showMagnifyingGlass", true);
		}
		if (event.keyCode === 40 || event.keyCode === 38) {
			var listOfSuggestions = component.get("v.listOfSuggestions");
			var selectedSuggestion = component.get("v.selectedSuggestion");
			var selectedKeyCode = component.get("v.selectedKeyCode");

			if (listOfSuggestions && listOfSuggestions.length > 0) {
				if (selectedSuggestion === -1) {
					selectedSuggestion = 0;
				} else {
					if (event.keyCode === 40 && listOfSuggestions.length - 1 > selectedSuggestion) {
						selectedSuggestion++;
						selectedKeyCode = 40;
					} else if (event.keyCode === 38 && 0 < selectedSuggestion) {
						selectedSuggestion--;
						selectedKeyCode = 38;
					}
				}
				component.set("v.searchQuery", listOfSuggestions[selectedSuggestion].Phrase);
				component.set("v.selectedSuggestion", selectedSuggestion);

				$A.get("e.c:ActivateSuggestion").setParams({
					"selectedSuggestion": selectedSuggestion,
					"selectedKeyCode": selectedKeyCode
				}).fire();
			}
		} else {
			var results = false;

			if (event.keyCode === 13 || event.target.nodeName === "BUTTON") {
				component.find("searchQueryInputID").getElement().blur();
				needSuggestion = false;
				results = true;
				//logic to display current query in the page URL
				// helper.removeCurrentQuery();
				helper.setUrlParam(helper.QUERY_PARAMS.QUERY, component.get("v.searchQuery"));
				helper.setUrlParam(helper.QUERY_PARAMS.ARTICLE_ID, '');
			} else if (event.keyCode === 27) {
				component.set("v.searchQuery", '');
			}

			component.set("v.results", results);

			var applicationEvent = $A.get("e.c:SearchQueryUpdated");
			applicationEvent.setParams({
				"searchQuery": new_value,
				"needSuggestion": needSuggestion
			});
			applicationEvent.fire();
		}
	},
	implementSelected: function(component, event, helper) {
		var suggestion = event.getParam("suggestion");
		component.set("v.searchQuery", suggestion);
		//logic to display current query in the page URL
		helper.removeCurrentQuery();
		helper.setUrlParam(helper.QUERY_PARAMS.QUERY, suggestion);

		var applicationEvent = $A.get("e.c:SearchQueryUpdated");
		applicationEvent.setParams({
			"searchQuery": suggestion,
			"needSuggestion": false
		});
		applicationEvent.fire();
		component.set("v.listOfSuggestions", undefined);
	},

	clearAll: function(component, event, helper) {
		helper.removeCurrentQuery();
		component.set("v.showMagnifyingGlass", true);
		$A.util.addClass(component.find('greeting'), 'title_margin');
		component.set("v.searchQuery", "");
		$A.get("e.c:ShowOnlyHome").fire();

		var appEvent = $A.get("e.c:HideBlocks");
		appEvent.setParams({
			"hide": component.get('v.statusHide')
		});
		appEvent.fire();
		helper.setFocusOnSearchInput(component);
	},
	hideSuggestions: function(component, event, helper) {
		if (!component.get("v.results")) {
			helper.hideSuggestions(component);
		}
	},

	hideSearchFunctionality: function(component, event, helper) {
		$A.util.addClass(component.find('search_bar_total_span'), 'hide_total_span');
	},
	changeTab: function(component, event, helper) {
		var componentName = event.getParam("componentName");
		if (componentName.indexOf('LogTicket') >= 0) {
			return;
		}

		helper.showSpinner(component.getSuper());
		// helper.removeCurrentQuery();
		if (componentName === 'home') {
			// helper.removeCurrentQuery();
			component.set('v.showHideNavLeftMini', false);
			helper.showSearchFunctionality(component)
		} else {
			component.set('v.showHideNavLeftMini', false);
			$A.util.addClass(component.find('search_bar_total_span'), 'hide_total_span');
			helper.hideSearchParts(component);
		}
	},
	showSearchFunctionality: function(component, event, helper) {
		helper.showSearchFunctionality(component);
	},

	showCaseList: function(component, event, helper) {
		helper.showCaseListHandler(component);
		component.set("v.isHomeTab", false);
	},

	showFavorite: function(component, event, helper) {
		$A.get("e.c:ShowFavorite").fire();
		helper.hideSearchParts(component);
		component.set("v.isHomeTab", false);
		// helper.removeCurrentQuery();
	},

	getSuggestions: function(component, event, helper) {
		var listOfSuggestions = event.getParam("listOfSuggestions");
		component.set("v.listOfSuggestions", listOfSuggestions);
		component.set("v.selectedSuggestion", -1);
	},
	mobileNavActive: function(component, event, helper) {
		var nowState = component.get('v.showHideNavLeftMini');
		if (nowState) {
			component.set('v.showHideNavLeftMini', false);
		} else {
			component.set('v.showHideNavLeftMini', true);
		}
	},
	hideClick: function(component, event, helper) {
		event.preventDefault();
	},
	hideWrapMenuBg: function(component, event, helper) {
		component.set('v.showHideNavLeftMini', false);
		var wrapMenuBackground = component.find("wrapMenuBg");
		$A.util.removeClass(wrapMenuBackground, "active");
	},

})