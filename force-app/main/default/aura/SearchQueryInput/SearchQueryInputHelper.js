({
	mapOfLanguages: {},
	showSearchFunctionality: function(component) {
		$A.get("e.c:ShowOnlyHome").fire();
		component.set("v.isHomeTab", true);
		$A.util.removeClass(component.find('search_bar_total_span'), 'hide_total_span');

		component.set("v.searchQuery", '');
		$A.get("e.c:HideFeedbackButton").fire();

		$A.util.removeClass(component.find('section_nav'), 'active');
		var toggleIcon = component.find("navActivatorMobile");
		$A.util.toggleClass(toggleIcon, "active");

		var appEvent = $A.get("e.c:HideBlocks");
		appEvent.setParams({
			"hide": component.get('v.statusHide')
		});
		appEvent.fire();
		this.hideSpinnerTimeout(component);
		$A.util.removeClass(component.find('languagePanel'), 'hidden_div');
		// set focus
		this.setFocusOnSearchInput(component);
	},
	hideSearchParts: function(component) {
		$A.util.removeClass(component.find('section_nav'), 'active');
		var toggleIcon = component.find("navActivatorMobile");
		$A.util.toggleClass(toggleIcon, "active");

		var appEvent = $A.get("e.c:HideBlocks");
		appEvent.setParams({
			"hide": component.get('v.statusHide')
		});
		appEvent.fire();
		this.hideSpinnerTimeout(component);
		$A.util.addClass(component.find('languagePanel'), 'hidden_div');
	},
	hideSpinnerTimeout: function(component) {
		var helper = this;
		helper.hideSpinner(component.getSuper(), 1000);
	},
	hideSuggestions: function(component) {
		var applicationEvent = $A.get("e.c:SearchQueryUpdated");
		applicationEvent.setParams({
			"searchQuery": ''
		});
		applicationEvent.setParams({
			"needSuggestion": true
		});
		applicationEvent.fire();
	},
	doInitSearchInput: function(component) {
		var action = component.get("c.doInitSearchInput");
		action.setStorable();
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var responceList = response.getReturnValue();
				this.implementStartQuery(component);

				var greet;
				if (responceList[0][0] === '')
					component.set("v.greetingMessage", '');
				else {
					if (responceList[0][0].indexOf('Morning') >= 0) {
						greet = $A.get("$Label.c.Good_Morning");
					} else {
						if (responceList[0][0].indexOf('Evening') >= 0) {
							greet = $A.get("$Label.c.Good_Evening");
						} else {
							if (responceList[0][0].indexOf('Afternoon') >= 0) {
								greet = $A.get("$Label.c.Good_Afternoon");
							}
						}
					}
					component.set("v.greetingMessage", greet + ' ' + responceList[0][1]);
				}

				var lngWrapper = responceList[1];
				if (lngWrapper) {
					var languageComponentName = 'c:' + 'LanguageList';
					var dynamicArray = [];
					var dynamicSpanElement = {
						"tag": "div",
						"HTMLAttributes": {
							"class": 'languagePanel'
						}
					};
					dynamicArray.push(["aura:html", dynamicSpanElement]);
					dynamicArray.push([languageComponentName, {
						searchQuery: component.getReference('v.searchQuery')
					}]);

					$A.createComponents(
						dynamicArray,
						function(components, status, errorMessage) {
							if (status === "SUCCESS") {
								var pageBody = component.get("v.body");
								var div = components[0];
								var languageComponent = components[1];
								var divBody = div.get("v.body");
								divBody.push(languageComponent);
								div.set("v.body", divBody);
								pageBody.push(div);
								component.set("v.body", pageBody);
							}
						}
					);
				}
				component.set("v.applicationTabsList", responceList[2]);
				// set focus
				this.setFocusOnSearchInput(component);
			} else if (state === "ERROR") {
				var errors = response.getError();
			}
		});
		$A.enqueueAction(action);
	},

	showCaseListHandler: function(component) {
		$A.get("e.c:HideFeedbackButton").fire();
		this.hideSearchParts(component);
	},

	//logic to run query from the page URL
	implementStartQuery: function(component) {
		var resultMap = this.getUrlParams();
		var query = resultMap[this.QUERY_PARAMS.QUERY];
		var ticketId = resultMap[this.QUERY_PARAMS.CASE_ID];
		/*if(query){
			var applicationEvent = $A.get("e.c:SearchQueryUpdated");
			component.set("v.searchQuery", query);
			applicationEvent.setParams({
				"searchQuery": query,
				"needSuggestion": false
			});
			applicationEvent.fire();
		}*/
		if (ticketId) {
			$A.get("e.c:ClickedAppTabEvent").setParams({
				"componentName": "c:CasesList"
			}).fire();
			this.showCaseListHandler(component);
		}
	},

	removeCurrentQuery: function() {
		var urlWithoutSerach = window.location.href.replace(window.location.search, '');
		window.history.replaceState({}, null, urlWithoutSerach);
	},

	setFocusOnSearchInput: function(component) {
		// auto focus on Search input
		window.setTimeout($A.getCallback(function() {
			if (component.isValid()) {
				try {
					var $searchInput = component.find('searchQueryInputID');
					if ($searchInput && $searchInput.getElement()) {
						$searchInput.getElement().focus();
					}
				} catch (e) {}
			}
		}), 1000);
	}
})