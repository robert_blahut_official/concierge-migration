({
    afterRender: function(component, helper) {
        
		function displaySvgIcons() {
			var svg = component.find("home_svg_content");
			if(svg){
				var value = svg.getElement().innerText;
				value = value.replace("<![CDATA[", "").replace("]]>", "");
				svg.getElement().innerHTML = value; 
			}
        
			svg = component.find("tickets_svg_content");
			if(svg) {
				value = svg.getElement().innerText;
				value = value.replace("<![CDATA[", "").replace("]]>", "");
				svg.getElement().innerHTML = value;  
			}
        
			svg = component.find("favorites_svg_content");
			if(svg) {
				value = svg.getElement().innerText;
				value = value.replace("<![CDATA[", "").replace("]]>", "");
				svg.getElement().innerHTML = value;
			}
		}
		if(component.isValid()) {
			displaySvgIcons(component);
        } else {
			window.setTimeout(
                $A.getCallback(function() {
                    if (component.isValid()) {
                       displaySvgIcons();
                    }
                }), 1500
            );
        }
         
    }
})