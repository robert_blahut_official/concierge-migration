({
	CONCIERGE_NAMESPACE: 'cncrg__',
	QUERY_PARAMS: {
		QUERY: 'cncrg__q',
		DETAILS: 'cncrg__det',
		ARTICLE_ID: 'cncrg__sid',
		CASE_ID: 'cncrg__tid',
		HREF: 'cncrg__href'
	},
	showSpinner : function(component) {
		var spinner = component.find("spinner");
		$A.util.removeClass(spinner, "hidden_div");
	},

	hideSpinner : function(component, timeOut) {
		if(timeOut == null) {
			timeOut = 200;
		} 
		window.setTimeout(
			$A.getCallback(function() {
				if (component.isValid()) {
					var spinner = component.find("spinner");
					$A.util.addClass(spinner, "hidden_div");
				}
			}), timeOut
		);
	},

	/**
	 * DEAR SECURITY REVIEWER! this method is not modifing the DOM. 
	 * This method is started from Rerender functions
	 */
	scrollToAnchor: function(component, event, helper) {
		var links = document.getElementsByTagName("A");
		for(var i=0; i < links.length; i++) {
			if(!links[i].hash) continue;
			if((links[i].hash && (links[i].getAttribute("href").slice(0, 1) === '#'))){
				var anchorPointHash = '' + links[i].hash;
				(function(anchorPointHash, anchorPoint) {
					anchorPoint.addEventListener("click", function(e) {
						e.preventDefault();
						var targetId = anchorPointHash.replace(/#/, "");
						document.getElementById(targetId).scrollIntoView(true);
					}, false);
				})(anchorPointHash, links[i]);
			}
		}
	},

	setUrlParam: function (key, value) {
		var url = window.location.search;
		if(value !== undefined){
			value = encodeURIComponent(value);
		}
		var urls = url.split('?');
		var baseUrl = urls[0];
		var parameters = '';
		var outPara = {};
		if(urls.length>1){
			parameters = urls[1];
		}
		if(parameters !== ''){
			parameters = parameters.split('&');
			for(var k in parameters){
				var keyVal = parameters[k];
				keyVal = keyVal.split('=');
				var eKey = keyVal[0];
				var evl = '';
				if(keyVal.length>1){
					evl = keyVal[1];
				}
				outPara[eKey] = evl;
			}
		}
		if(value !== undefined){
			outPara[key] = value;
		}else{
			delete outPara[key];
		}
		parameters = [];
		for(k in outPara){
			parameters.push(k + '=' + outPara[k]);
		}
		var finalUrl = baseUrl;
		if(parameters.length>0){
			finalUrl += '?' + parameters.join('&');
		}
		window.history.replaceState({}, null, finalUrl);
	},

	getUrlParams: function () {
		var queryString = window.location.search.slice(1).replace(/\+/g, "%20");
		var resultMap = {};
		if (queryString) {
			queryString = queryString.split('#')[0];
			var arr = queryString.split('&');
			for (var i = 0; i < arr.length; i++) {
				var a = arr[i].split('=');
				var paramNum = undefined;
				var paramName = a[0].replace(/\[\d*\]/, function (v) {
					paramNum = v.slice(1, -1);
					return '';
				});
				var paramValue = typeof(a[1]) === 'undefined' ? true : decodeURIComponent(a[1]);
				if (resultMap[paramName]) {
					if (typeof resultMap[paramName] === 'string') {
						resultMap[paramName] = [resultMap[paramName]];
					}
					if (typeof paramNum === 'undefined') {
						resultMap[paramName].push(paramValue);
					}
					else {
						resultMap[paramName][paramNum] = paramValue;
					}
				}
				else {
					resultMap[paramName] = paramValue;
				}
			}
		}
		return resultMap;
	},

	getUrlParam: function(param) {
		var resultMap = this.getUrlParams();
		return resultMap[param];
	},

	getCommuityPrefix: function(component) {
		var prefix = '';
		var currUrl;
		try {
			currUrl = window.location.pathname;
		}
		catch (e) {
			currUrl = $A.get('$Resource.Media');
		}
		var prefixArray = currUrl.split('/');
		if (prefixArray[1] !== 's') {
			prefix = prefixArray[1];
		}
		return prefix;
	},

	getClosestElement: function(elem, selector) {
		// matches() polyfill @todo add other special functions if requried
		// for IE only, i hope other browsers are good...
		var matchesFnc = elem.matches ? elem.matches : elem.msMatchesSelector;

		// Get the closest matching element
		for ( ; elem && elem !== document; elem = elem.parentNode ) {
			if ( matchesFnc.call(elem, selector ) ) {
				return elem;
			}
		}
		return null;
	}
})