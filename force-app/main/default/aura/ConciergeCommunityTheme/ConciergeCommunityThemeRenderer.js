({
	afterRender: function(component, helper) {
		this.superAfterRender();
        try {
            //if(self === top && component.get("v.useCustomNavBar") && $A.get("$Browser.formFactor") === 'DESKTOP') {
            if(self === top && component.get("v.useCustomNavBar")) {
            	var navigation = component.find("navigation");
        		navigation.getElement().innerHTML = null;
                $A.createComponent(
                    "c:NavigationMenuOverride",
                    {
                    	replaceHomeTextWithIcon: component.get("v.replaceHomeTextWithIcon"),
                    	hideAppLauncherInCommunityHeader: component.get("v.hideAppLauncherInCommunityHeader"),
                    },
                    function(newElement, status, errorMessage){
                        if (status === "SUCCESS") {
                           component.set("v.navigationCustom", newElement);
                        }
                        else if (status === "INCOMPLETE") {
                        }
                        else if (status === "ERROR") {
                        }
                    }
                );
            }
        }
        catch(e) {
            
        }
    }
})