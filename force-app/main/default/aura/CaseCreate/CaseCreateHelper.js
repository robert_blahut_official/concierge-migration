({
	createDynamicBlock : function(component, result, searchText, componentName, helper) {
		if(componentName!== '' && componentName!==undefined){
			$A.createComponent(
				componentName,
				{
					"result" : result,
					"searchText" : searchText,
					"isCommunity" : component.get("v.isCommunity"),
					"isRelatedArticle" : component.get("v.isRelatedArticle")
				},
				function(dynamicBlock, status, errorMessage) {
					if (status === "SUCCESS") {
						component.set("v.dynamicBlock",dynamicBlock);
							
					}
					else if (status === "INCOMPLETE") {
					}
					else if (status === "ERROR") {
						helper.createDynamicBlock(component, result, searchText, 'c:LogTicket');
					}
				}
			);
		}
	},

	getLogTicketName : function(component, helper) {
		var action = component.get("c.getLogTicketComponentName");
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var logTicketComponentName = response.getReturnValue();
				if(logTicketComponentName != null) {
					component.set("v.componentName", logTicketComponentName);
				}
				if(component.get("v.isRelatedArticle")) {
					this.createDynamicBlock(component, component.get("v.result"), '', component.get("v.componentName"), helper);
				}
			} 
		});
		$A.enqueueAction(action);
	}
})