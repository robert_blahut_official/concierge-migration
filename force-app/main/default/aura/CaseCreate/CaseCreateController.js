({
	doInit : function(component, event, helper) {
		helper.getLogTicketName(component, helper);
	},

	startCaseCreation : function(component, event, helper) {
		helper.createDynamicBlock(
			component,
			event.getParam("result"),
			event.getParam("query"),
			component.get("v.componentName"),
			helper
		);
	}
})