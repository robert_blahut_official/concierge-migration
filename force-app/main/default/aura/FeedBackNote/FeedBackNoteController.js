({
    setNote : function(component, event, helper) {
        var notes = component.find("notes").get("v.value");
               
        // if(!(notes === undefined || notes  === null ||
        //     notes.trim() === null || notes.trim() === undefined || notes.trim() === "")){
            var noteEvent  = component.getEvent("sendNoteMessage");
            noteEvent.setParams({ "noteMessage": notes });
            noteEvent.fire();
            
            var setNextWindow  = component.getEvent("sendNextWindow");
            helper.setActiveWindow(setNextWindow, "2");
    
            component.find("notes").set("v.placeholder", $A.get("$Label.c.Enter_Text_Placeholder"));
        // }else {
        //
        //     component.find("notes").set("v.errors", [{message: $A.get("$Label.c.Notes_Field_Can_not_Be_Blank")}]);
        //     $A.util.addClass(component.find("notes"), "errorBorder");
        //     /*window.setTimeout(
			// 	$A.getCallback(function() {
			// 		if (component.isValid()) {
        //     			component.find("notes").set("v.errors", [{message:""}]);
			// 		}
			// 	}), 1000
			// );*/
        // }
        
    },
    hideErrorBorder : function(component, event, helper) {
        component.find("notes").set("v.errors", []);
        $A.util.removeClass(component.find("notes"), "errorBorder");
    },
    reloadNoteRating : function(component, event, helper) {
        component.find("notes").set("v.value", "");

    },
    removeDefault : function(component, event, helper) {     
        component.find("notes").set("v.placeholder","");
    },
    
    skipNote: function(component, event, helper) {
        component.find("notes").set("v.placeholder", $A.get("$Label.c.Enter_Text_Placeholder"));
        var setNextWindow  = component.getEvent("sendNextWindow");
        helper.setActiveWindow(setNextWindow, "8");       
    },
    
    hideFeedBackNote : function(component, event, helper) {
        component.find("notes").set("v.placeholder", $A.get("$Label.c.Enter_Text_Placeholder"));
        var setNextWindow  = component.getEvent("sendNextWindow");
        helper.setActiveWindow(setNextWindow, "5");
        
		var reloadStarRating = $A.get("e.c:ReloadFeedBack");
		reloadStarRating.fire();
        
        
    }
})