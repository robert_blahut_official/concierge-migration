({
	doInit: function(component, event, helper) {
		helper.setTypeSettingValues(component);
	},

	deleteSetting : function(component, event, helper) {
		var selectedSetting = event.target.id;
		var cmpEvent = component.getEvent("deleteOldSetting");
		cmpEvent.setParams({
			"positionInList" : component.get("v.positionInList"),
			"fieldPosition" : selectedSetting
		}).fire();
	},

	addNewRecord : function (component, event, helper) {
		$A.get("e.c:CreateNewSetting").setParams({
			"positionInList": component.get("v.positionInList")
		}).fire();
	},

	handleRelatedFileChange: function(component, event, helper) {
		var positionInList = component.get("v.positionInList");
		if (!event.getSource().get('v.checked')) {
			var fieldPosition;
			var typeSetting = component.get("v.newTypeSetting");
			if (typeSetting && typeSetting.selectedFields) {
				for (var i = 0; i < typeSetting.selectedFields.length; i++) {
					var field = typeSetting.selectedFields[i];
					if (field.fieldType && field.fieldType == helper.ATTACHMENTS_FLAG) {
						fieldPosition = i;
						break;
					}
				}
			}
			if (fieldPosition) {
				component.getEvent("deleteOldSetting").setParams({
					"positionInList" : positionInList,
					"fieldPosition" : fieldPosition
				}).fire();
			}
		} else {
			$A.get("e.c:CreateNewSetting").setParams({
				"positionInList": positionInList,
				"fieldType": helper.ATTACHMENTS_FLAG,
				"fieldName": 'Attachments'
			}).fire();
		}
	}
})