({
	ATTACHMENTS_FLAG: 'attachments_flag',
	listOfActiveFlags : {
		'CLEAR_FILE' : false
	},
	setNewTypeSetting : function(component, helper){
		var newSetting = component.get("v.newTypeSetting");
		this.listOfActiveFlags['CLEAR_FILE'] = false
		component.set("v.typeSetting",newSetting)
	},
	setTypeSettingValues: function(component) {
		var helper = this;
		var typeSetting = component.get("v.typeSetting");
		var $checkbox = component.find(helper.ATTACHMENTS_FLAG);

		if (typeSetting) {
			;(typeSetting.selectedFields || []).forEach(function(field) {
				if (field.fieldType == helper.ATTACHMENTS_FLAG) {
					if ($checkbox) {
						$checkbox.set('v.checked', true);
					}
				}
			});
			;(typeSetting.allAvailableFields || []).forEach(function(field) {
				if (field.fieldType == helper.ATTACHMENTS_FLAG) {
					$A.util.removeClass(component.find('checkbox-section'), 'slds-hide');
				}
			});
		}

		component.set("v.newTypeSetting", typeSetting);
	}
})