({
	initTypesInfo: function(component) {
		var action = component.get("c.getAllArticleTypes");
		action.setCallback(this, function(response) {
			if (response.getState() === "SUCCESS") {
				var responseList = response.getReturnValue();
				component.set("v.allTypes", responseList[0]);
				component.set("v.chanButtonsList", responseList[1]);
			} else {
				var errors = response.getError();
			}
		});
		$A.enqueueAction(action);
	},
	initSettingInfo: function(component, articleInfo, activeActions) {
		component.set("v.validationMessages", []);
		component.set("v.articleInfo", articleInfo);
		component.set('v.previousSettingInfo', activeActions);
		$A.util.addClass(component.find("support_validation"), "hidden_div");
		$A.util.addClass(component.find("url_validation"), "hidden_div");

		this.implementDefaultSettings(component);
		this.implementDefaultPosition(component);

		var action = component.get("c.getAllArticleCategories");
		action.setParams({
			'recordId': articleInfo.recordId,
			'articleType': articleInfo.type
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				component.set("v.allCategories", response.getReturnValue());

				if (articleInfo && articleInfo.type) {
					var allTypes = component.get("v.allTypes");
					for (var i = 0; i < allTypes.length; i++) {
						if (allTypes[i].name === articleInfo.type) {
							component.set("v.availableType", allTypes[i].label);
							break;
						}
					}
				}
				if (activeActions && activeActions.Id) {
					component.set("v.settingInfo", JSON.parse(JSON.stringify(activeActions)));
					this.changeVisibility(component, 'change_category', 'change_type', 'change_article_id', undefined, activeActions.cncrg__Type__c);
					this.changeVisibility(component, 'change_category', 'change_type', 'change_article_id', undefined, activeActions.cncrg__Category__c);
					this.changeVisibility(component, 'change_article_id', undefined, 'change_type', 'change_category', activeActions.cncrg__ArticleId__c);
				}

				/*$A.util.removeClass(component.find('setup_actions_total_div'), 'hidden_div');*/
				component.set('v.blockDisplay', true);
			} else if (state === "ERROR") {
				var errors = response.getError();
			}
		});
		$A.enqueueAction(action);
	},

	saveNewSetting: function(component, removeId) {
		var oldSettingsInfo = component.get("v.previousSettingInfo");
		var currentSettingsInfo = component.get("v.settingInfo");
		var currentArticleInfo = component.get("v.articleInfo");

		var action = component.get("c.createNewSetting");
		action.setParams({
			'settingStr': JSON.stringify([oldSettingsInfo, currentSettingsInfo])
		});

		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var settingInfo = response.getReturnValue();

				component.set("v.settingInfo", settingInfo);
				component.set('v.previousSettingInfo', JSON.parse(JSON.stringify(settingInfo)));

				$A.util.removeClass(component.find("confirmation"), "hidden_div");
				$A.util.addClass(component.find("validation"), "hidden_div");

				$A.get("e.c:UpdateActiveSettings").setParams({
					"updatedSettingsInfo": JSON.stringify(settingInfo)
				}).fire();
				window.setTimeout(
					$A.getCallback(function() {
						if (component.isValid()) {
							$A.util.addClass(component.find("confirmation"), "hidden_div");
						}
					}), 3000
				);
			} else if (state === "ERROR") {
				var errors = response.getError();
				if (errors) {
					component.set("v.validationMessages", errors[0].pageErrors);
					$A.util.removeClass(component.find("validation"), "hidden_div");
				}
			}
		});
		$A.enqueueAction(action);
	},

	deleteSetting: function(component) {
		var action = component.get("c.deleteSettingAPEX");
		action.setParams({
			'newSetting': component.get("v.settingInfo")
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				$A.get("e.c:UpdateActiveSettings").fire();
				var settingInfo = response.getReturnValue();
				this.implementDefaultSettings(component);

			} else if (state === "ERROR") {
				var errors = response.getError();
			}
		});
		$A.enqueueAction(action);
	},

	implementDefaultSettings: function(component) {
		var currentArticleInfo = component.get("v.articleInfo") || {};
		component.set(
			"v.settingInfo", {
				'sobjectType': 'cncrg__Action_Setting__c',
				'cncrg__Active__c': true,
				'cncrg__Type__c': '',
				'cncrg__Source_System__c': currentArticleInfo.storage || 'salesforce',
				'cncrg__Category__c': '',
				'cncrg__ArticleId__c': '',
				'cncrg__Log_a_Ticket__c': false,
				'cncrg__Support_Name__c': '',
				'cncrg__Support_Number__c': '',
				'cncrg__URL__c': '',
				'cncrg__URL_Label__c': '',
				'cncrg__Chat__c': '',
				'cncrg__Email__c': false,
				'cncrg__Email_Address__c': '',
				'cncrg__Email_Subject__c': '',
				'cncrg__Email_Recipient_Name__c': '',
				'cncrg__Flow_Name__c': '',
				'cncrg__Flow_Label__c': ''
			}
		);
		component.find('support_name').set("v.value", "");
		component.find('support_number').set("v.value", "");
		component.find('visit_page').set("v.value", "");
		component.find('url_label').set("v.value", "");
		component.find('email_subject').set("v.value", "");
		component.find('email_address').set("v.value", "");
		component.find('email_recipient_name').set("v.value", "");
		component.find('active').set("v.value", true);
		this.changePicklistDisabled(component);
	},

	implementDefaultPosition: function(component) {
		component.find('change_category').set("v.disabled", false);
		$A.util.removeClass(component.find('change_category'), 'disabled_select');

		component.find('change_type').set("v.disabled", false);
		$A.util.removeClass(component.find('change_type'), 'disabled_select');

		component.find('change_article_id').set("v.disabled", false);
		$A.util.removeClass(component.find('change_article_id'), 'disabled_select');
	},

	changeVisibility: function(component, changedParam1, changedParam2, firstToChange, secondToChange, newValue) {
		if (!newValue) {
			newValue = component.find(changedParam1).get('v.value');
		}

		var haveSecondValue = false;
		if (changedParam2) {
			var additionalValue = component.find(changedParam2).get('v.value');
			haveSecondValue = (additionalValue && additionalValue !== null && additionalValue.trim() !== '');
		}

		var haveFirstValue = (newValue && newValue !== null && newValue.trim() !== '');
		if (haveFirstValue || haveSecondValue) {
			component.find(firstToChange).set("v.disabled", true);
			$A.util.addClass(component.find(firstToChange), 'disabled_select');
			if (secondToChange) {
				component.find(secondToChange).set("v.disabled", true);
				$A.util.addClass(component.find(secondToChange), 'disabled_select');
			}
		} else {
			component.find(firstToChange).set("v.disabled", false);
			$A.util.removeClass(component.find(firstToChange), 'disabled_select');
			if (secondToChange) {
				component.find(secondToChange).set("v.disabled", false);
				$A.util.removeClass(component.find(secondToChange), 'disabled_select');
			}
		}

	},
	changePicklistDisabled: function(component) {
		var auraIdArray = ['change_category', 'change_type', 'change_article_id'];
		for (var i = 0; i < auraIdArray.length; i++) {
			component.find(auraIdArray[i]).set("v.disabled", false);
			$A.util.removeClass(component.find(auraIdArray[i]), 'disabled_select');
		}
	}
})