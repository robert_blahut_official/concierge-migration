({
	doInit: function(component, event, helper) {
		helper.initTypesInfo(component);
	},

	showActionsSetup: function(component, event, helper) {
		var articleInfo = event.getParam("articleInfo");
		var activeActions = event.getParam("activeActions");
		helper.initSettingInfo(component, articleInfo, activeActions);

	},
	hideDetails: function(component, event, helper) {
		component.set('v.blockDisplay', false);
	},

	saveChanges: function(component, event, helper) {
		var change_source = component.find("change_source").get("v.value");
		var type_value = component.find("change_type").get("v.value");
		var category_value = component.find("change_category").get("v.value");
		var article_value = component.find("change_article_id").get("v.value");
		var newSetting = component.get("v.settingInfo");

		if (
			(change_source === 'salesforce' && (type_value || category_value || article_value))
			|| (change_source !== 'salesforce')) {

			var emailValid = true;
			var emailAddress = component.find("email_address").get("v.value");
			if (newSetting.cncrg__Email__c && emailAddress) {
				if (!emailAddress.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,63})+$/)) {
					$A.util.removeClass(component.find("email_syntax"), "hidden_div");
					emailValid = false;

					window.setTimeout(
						$A.getCallback(function() {
							if (component.isValid()) {
								$A.util.addClass(component.find("email_syntax"), "hidden_div");
							}
						}), 3000
					);
				}
			}

			var skypeContactValid = true;
			if (newSetting.Skype_For_Business__c) {
				var skypeContact = newSetting.Skype_For_Business_Contact__c;
				if (skypeContact && !skypeContact.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,63})+$/)) {
					$A.util.removeClass(component.find("email_syntax"), "hidden_div");
					skypeContactValid = false;

					window.setTimeout(
						$A.getCallback(function() {
							if (component.isValid()) {
								$A.util.addClass(component.find("email_syntax"), "hidden_div");
							}
						}), 3000
					);
				}
			}

			if (emailValid && skypeContactValid) {
				var articleInfo = component.get("v.articleInfo");
				var oldSetting = component.get('v.previousSettingInfo');
				var needConfirmation = false;

				newSetting.cncrg__Source_System__c = articleInfo.storage;
				component.set('v.settingInfo', newSetting);

				if (oldSetting) {
					if (newSetting.cncrg__Category__c === undefined) {
						newSetting.cncrg__Category__c = '';
					}
					if (oldSetting.cncrg__Category__c === undefined) {
						oldSetting.cncrg__Category__c = '';
					}
					if (newSetting.cncrg__Type__c === undefined) {
						newSetting.cncrg__Type__c = '';
					}
					if (oldSetting.cncrg__Type__c === undefined) {
						oldSetting.cncrg__Type__c = '';
					}

					if (oldSetting.cncrg__ArticleId__c 
							&& oldSetting.cncrg__ArticleId__c !== newSetting.cncrg__ArticleId__c) {

						component.set("v.confirmationText", $A.get("$Label.c.Actions_Settings_Article_Update"));
						component.set("v.confirmSave", true);
						needConfirmation = true;

					} else if (newSetting.cncrg__Category__c !== oldSetting.cncrg__Category__c
								|| newSetting.cncrg__Type__c !== oldSetting.cncrg__Type__c) {

						component.set("v.confirmationText", $A.get("$Label.c.Actions_Settings_Type_or_Category_Update"));
						component.set("v.confirmSave", true);
						needConfirmation = true;
					}

				}

				if (needConfirmation) {
					$A.util.addClass(component.find("support_validation"), "hidden_div");
					$A.util.removeClass(component.find('confirmation_div'), 'hidden_div');
					component.set("v.scrollVisible", false);
				} else {
					helper.saveNewSetting(component);
				}
			}

		} else {
			$A.util.removeClass(component.find("settings_missing"), "hidden_div");
			window.setTimeout(
				$A.getCallback(function() {
					if (component.isValid()) {
						$A.util.addClass(component.find("settings_missing"), "hidden_div");
					}
				}), 3000
			);
		}
	},

	confirmAction: function(component, event, helper) {
		$A.util.addClass(component.find('confirmation_div'), 'hidden_div');
		component.set("v.scrollVisible", true);
		if (component.get("v.confirmSave") === true) {
			helper.saveNewSetting(component);
		}
		else {
			helper.deleteSetting(component);
		}
	},

	hideConfirmation: function(component, event, helper) {
		$A.util.addClass(component.find('confirmation_div'), 'hidden_div');
		component.set("v.scrollVisible", true);
	},

	deleteSetting: function(component, event, helper) {
		var settingInfo = component.get("v.settingInfo");

		if (settingInfo.Id && settingInfo.Id !== null) {
			var deletingWarningMessage = (settingInfo.cncrg__ArticleId__c == null ? $A.get("$Label.c.Actions_Settings_Confirm_Delete") : $A.get("$Label.c.Actions_Settings_Confirm_Delete_By_ArticleId"));
			settingInfo.cncrg__ArticleId__c == null
			component.set("v.confirmationText", deletingWarningMessage);
			component.set("v.confirmSave", false);
			$A.util.removeClass(component.find('confirmation_div'), 'hidden_div');
			component.set("v.scrollVisible", false);
		} else {
			helper.deleteSetting(component);
		}
	},

	changeType: function(component, event, helper) {
		var type_value = component.find("change_type").get("v.value");
		if (type_value === '') {
			component.set("v.visibilitySettingsSelected", false);
		} else {
			component.set("v.visibilitySettingsSelected", true);
		}
		helper.changeVisibility(component, 'change_category', 'change_type', 'change_article_id', undefined, undefined);
	},

	changeCategory: function(component, event, helper) {
		var type_value = component.find("change_category").get("v.value");
		if (type_value === '') {
			component.set("v.visibilitySettingsSelected", false);
		} else {
			component.set("v.visibilitySettingsSelected", true);
		}
		helper.changeVisibility(component, 'change_category', 'change_type', 'change_article_id', undefined, undefined);
	},

	changeArticleId: function(component, event, helper) {
		var type_value = component.find("change_article_id").get("v.value");
		if (type_value === '') {
			component.set("v.visibilitySettingsSelected", false);
		} else {
			component.set("v.visibilitySettingsSelected", true);
		}
		helper.changeVisibility(component, 'change_article_id', undefined, 'change_type', 'change_category', undefined);
	},
	emailActionValidation: function(cmp, event, helper) {
		if (!cmp.get("v.settingInfo.cncrg__Email__c")) {
			cmp.set("v.settingInfo.cncrg__Email_Subject__c", "");
			cmp.set("v.settingInfo.cncrg__Email_Address__c", "");
			cmp.set("v.settingInfo.cncrg__Email_Recipient_Name__c", "");
		}
	},
	skypeActionValidation: function(cmp, event, helper) {
		if (!cmp.get("v.settingInfo.Skype_For_Business__c")) {
			cmp.set("v.settingInfo.Skype_For_Business_Contact__c", "");
		}
	},
})