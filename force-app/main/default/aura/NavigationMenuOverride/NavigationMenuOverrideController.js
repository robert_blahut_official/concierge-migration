({
	onClick : function(component, event, helper) {
        var id = event.target.dataset.menuItemId;
        var elem = event.target;
        if(!elem.classList.contains("isLink")){
            elem = elem.parentNode;
			if(elem.dataset.menuItemId) {
				id = elem.dataset.menuItemId;
			}
        }
        if (id) {
            component.getSuper().navigate(id);
         }
   },
   showMediaNav : function(component, event, helper){
   		$A.util.removeClass(component.find('mediaNavBar'), 'inactive');
   },
   hideMediaNav : function(component, event, helper){
   		$A.util.addClass(component.find('mediaNavBar'), 'inactive');
   },
   doInit : function(component, event, helper) {
		var action = component.get("c.isUserHasAccessToCase");
        action.setCallback(self, function(response){
            var state = response.getState();
            if ( state === 'SUCCESS' ) {
                if(!response.getReturnValue()) {
                    component.set("v.isUserHasAccessToCase", false);
                }
            }
        });
        $A.enqueueAction(action);
   }
})