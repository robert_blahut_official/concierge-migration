({
    createNewSetting : function (component) {
    		var action = component.get("c.createNewSetting");
    		action.setParams({
    			'caseLayoutRecordStr' : JSON.stringify(component.get("v.settingInfo"))
    		});
    		action.setCallback(this, function(response) {
    			var state = response.getState();

    			if (state === "SUCCESS") {
    				var settingInfo = response.getReturnValue();

    				component.set("v.settingInfo", settingInfo);
    				$A.util.removeClass(component.find("confirmation"), "hidden_div");
    				window.setTimeout(
                    					$A.getCallback(function() {
                                			$A.util.addClass(component.find("setup_actions_total_div"), "hidden_div");
                    					}), 2000
                                    );
                }
    			$A.get("e.c:UpdateActiveSettings").fire();
    		});
            $A.enqueueAction(action);
    	},

    deleteSetting : function (component) {
    		var action = component.get("c.deleteSetting");
    		action.setParams({
    			'caseLayoutRecordStr' : JSON.stringify(component.get('v.settingInfo'))
    		});
    		action.setCallback(this, function(response) {
    			var state = response.getState();
    			if (state === "SUCCESS") {
    				$A.get("e.c:UpdateActiveSettings").fire();
    				//var settingInfo = response.getReturnValue();
    				//component.set("v.settingInfo", settingInfo);
    				$A.util.removeClass(component.find("confirmation"), "hidden_div");
                    window.setTimeout(
                                        $A.getCallback(function() {
                                            $A.util.addClass(component.find("setup_actions_total_div"), "hidden_div");
                                        }), 2000
                                    );
    			}
    		});
            $A.enqueueAction(action);
    	}
})