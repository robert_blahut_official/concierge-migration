({
    hideDetails : function (component, event, helper) {
        $A.util.addClass(component.find('setup_actions_total_div'), 'hidden_div');
    },

    saveChanges : function (component, event, helper) {
        var articleInfo = component.get("v.articleInfo");
        var newSetting = component.get("v.settingInfo");
		var fieldSetValue = newSetting.cncrg__Field_Set__c;
		var type_value = component.find("change_type_input").get("v.value");
        var category_value = component.find("change_category_input").get("v.value");
        var article_value = component.find("change_article_id_input").get("v.value");

		if(type_value !== '' && typeof type_value !== 'undefined'
           || category_value !== '' && typeof category_value !== 'undefined'
           || article_value !== '' && typeof article_value !== 'undefined'){

			if(fieldSetValue === '')	{
				$A.util.removeClass(component.find("settings_missing"), "hidden_div");
				window.setTimeout(
					$A.getCallback(function() {
						if (component.isValid()) {
            				$A.util.addClass(component.find("settings_missing"), "hidden_div");
						}
					}), 3000
				);
			} else
				helper.createNewSetting(component);			
		} else {
			$A.util.removeClass(component.find("settings_missing"), "hidden_div");
            window.setTimeout(
                $A.getCallback(function() {
                    if (component.isValid()) {
                        $A.util.addClass(component.find("settings_missing"), "hidden_div");
                    }
                }), 3000
            );
		}	 
    },
    deleteLastSetting : function (component, event, helper) {
        helper.deleteSetting(component);
    },
    changeType : function (component, event, helper) {
        component.find('change_article_id_input').set('v.value', '');

    },
    changeCategory : function (component, event, helper) {
        component.find('change_article_id_input').set('v.value', '');
    },
    changeArticleId : function (component, event, helper) {
        component.find('change_category_input').set('v.value', '');
        component.find('change_type_input').set('v.value', '');
    },
})