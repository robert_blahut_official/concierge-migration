({
	loadTriggerActivatorSettings : function(component, event, helper) {
        this.showSpinner(component.getSuper());
		var action = component.get("c.getTriggerActivatorSettingsList");
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
                var triggerActivatorSettings = response.getReturnValue();
                var settingsList = [];
                // Created 19.7.2018
				// ATTENTION!!! Don't delete this comment. Without next strings custom labels won't be added dynamically.
                // $Label.с.Case_Sharing_Trigger
                // $Label.с.Contact_Case_Sharing_Trigger
                // $Label.с.User_Case_Sharing_Trigger
				// $Label.c.Case_Sharing_Trigger
                // $Label.c.Contact_Case_Sharing_Trigger
                // $Label.c.User_Case_Sharing_Trigger
                for (var i = 0; i < triggerActivatorSettings.length; i++) {
                    var nameLabel = triggerActivatorSettings[i].Name.replace(/\s/g, "_");
                	var label = $A.get("$Label.cncrg." + nameLabel);
                	if(!label){
                        label = $A.get("$Label.c." + nameLabel);
					}
                    settingsList.push({value:triggerActivatorSettings[i], key:label});
                }
                this.hideSpinner(component.getSuper());
                component.set("v.triggerActivatorSettingsMap", settingsList);

			} else if (state === "ERROR") {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
					}
				} else {
				}
			}
		});
        $A.enqueueAction(action);
	},
	implementNewSettings : function(component) {
		var triggerActivatorSettingsMap = component.get("v.triggerActivatorSettingsMap");
        var triggerActivatorSettings = [];
        for (var i = 0; i < triggerActivatorSettingsMap.length; i++) {
            triggerActivatorSettings.push(triggerActivatorSettingsMap[i].value);
        }
		var action = component.get("c.implementNewTriggerActivatorSettings");
		action.setParams({
			triggerActivatorSettings : JSON.stringify(triggerActivatorSettings)
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				component.find("messageTop").set("v.severity", "confirm");
                component.find("messageBottom").set("v.severity", "confirm");
                $A.util.removeClass(component.find("confirmationTop"), "display_false");
                $A.util.removeClass(component.find("confirmationBottom"), "display_false");
				component.set("v.message", $A.get("$Label.c.All_Changes_Were_Saved_Successfully"));
                window.setTimeout(
                    $A.getCallback(function() {
                        $A.util.addClass(component.find("confirmationTop"), "display_false");
                        $A.util.addClass(component.find("confirmationBottom"), "display_false");
                    }), 2000
                );
			} else if (state === "ERROR") {
                var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
					}
				} else {
				}
			}
			this.hideSpinner(component.getSuper());
		});
        $A.enqueueAction(action);
	}
})