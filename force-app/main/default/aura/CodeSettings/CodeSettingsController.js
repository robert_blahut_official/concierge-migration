({
	doInit : function(component, event, helper) {
		helper.loadTriggerActivatorSettings(component, event, helper);
	},
	goBack : function(component, event, helper) {
		$A.get("e.c:BackToSettings").fire();
	},
	implementNewSettingFirstTable : function(component, event, helper) {
		var tempName = event.target.id;
		var newValue = event.target.checked;
		var triggerActivatorSettings = component.get("v.triggerActivatorSettingsMap");

		triggerActivatorSettings[tempName].value.cncrg__Active__c = newValue;
		component.set("v.triggerActivatorSettingsMap",triggerActivatorSettings);
	},
	saveChanges : function (component, event, helper) {
        helper.showSpinner(component.getSuper());
		helper.implementNewSettings(component);
	},
})