({
	doInit: function (component, event, helper) {
		var expressionList = [];//component.get("v.expressionList");
		expressionList.push({value: '=', label: $A.get("$Label.c.Equals")});
		expressionList.push({value: '!=', label: $A.get("$Label.c.NoEquals")});
		component.set("v.expressionList", expressionList);
	}
})