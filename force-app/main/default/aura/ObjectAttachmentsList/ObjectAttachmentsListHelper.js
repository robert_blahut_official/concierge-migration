({
	getAttachments: function(component) {
		var action = component.get("c.getAttachmentsMap");
		action.setParams({
			'recordId': component.get('v.recordId')
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var attachmentsMap = response.getReturnValue();
				var attachmentsList = [];
				for (var key in attachmentsMap) {
					if (attachmentsMap.hasOwnProperty(key)) {
						attachmentsList.push({
							value: key,
							label: attachmentsMap[key]
						});
					}
				};
				component.set('v.attachmentsList', attachmentsList);
			}
		});
		$A.enqueueAction(action);
	}
})