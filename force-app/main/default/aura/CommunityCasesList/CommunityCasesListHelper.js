({
    getLiveAgentPage : function(component) {
        var action = component.get("c.getLiveAgentPage");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.liveAgentPage", response.getReturnValue()[0]);
				component.set("v.useLiveagentStartChatWithWindowMethod", response.getReturnValue()[1]);
				
            } else if (state === "ERROR") {
                var errors = response.getError();
            }
        });
        $A.enqueueAction(action);
    }
})