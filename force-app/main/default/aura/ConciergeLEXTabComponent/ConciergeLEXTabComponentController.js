({
	gotoConciergeLEXTabIframe: function(component, event, helper) {
		var device = $A.get("$Browser.formFactor");	
		if(device !== 'DESKTOP') {
			component.set('v.isShowIframe', true);
			window.setTimeout(
				$A.getCallback(function() {
					var pageOrigin = window.location.origin;
					if(pageOrigin !== undefined){
						component.find('iFrame').getElement().contentWindow.postMessage('heigthSf1', pageOrigin);
					}
				}), 2500
			);
		} else {
			component.set('v.isShowIframe', false);
		}
	}
})