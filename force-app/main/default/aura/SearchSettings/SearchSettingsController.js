({
    doInit: function (component, event, helper) {
        helper.loadSearchEngineConfiguration(component);
    },
    goBack: function (component, event, helper) {
        $A.get("e.c:BackToSettings").fire();
    },
    saveChanges: function (component, event, helper) {
        helper.showSpinner(component.getSuper());
        helper.implementNewSettings(component, helper);
    },
    implementNewSettingSecondTable: function (component, event, helper) {
        var tempName = event.target.id;
        var type = event.target.type;
        var newValue = event.target.value;
        var searchEngineConfiguration = component.get("v.searchEngineConfiguration");

        if (type === 'text')
            searchEngineConfiguration[tempName] = newValue;
        else if (type === 'number')
            searchEngineConfiguration[tempName] = parseInt(newValue, 10);
        component.set("v.searchEngineConfiguration", searchEngineConfiguration);
    },
    implementNewsearchSettings: function (component, event, helper) {
        var tempName = event.target.id;
        var newValue =  Number(event.target.value);
        var searchSettings = component.get("v.searchSettings");
        searchSettings[tempName] = parseInt(newValue, 10);
        component.set("v.searchSettings", searchSettings);
        helper.sumTotalWeightings(component);
    },

    validateNumberOfArticles: function (component, event, helper) {
        var number = event.target.value;
        if (number) {
            if (number.match(/\D/g)) {
                $A.util.removeClass(component.find("number_validation_message"), "hidden");
                component.set("v.isAvailable", false);
            } else if (number.length > 1 && number.substring(0, 1) === '0') {
                $A.util.removeClass(component.find("number_validation_message"), "hidden");
                component.set("v.isAvailable", false);
            } else if (number < 0 || number > 2000) {
                $A.util.removeClass(component.find("number_validation_message"), "hidden");
                component.set("v.isAvailable", false);
            } else {
                $A.util.addClass(component.find("number_validation_message"), "hidden");
                component.set("v.isAvailable", true);
            }
        }
    },
    implementWebServiceSetting: function (component, event, helper) {
        var tempName = event.target.id;
        var type = event.target.type;
        var newValue = event.target.value;
        var webServiceConfiguration = component.get("v.webServiceSettings");
        webServiceConfiguration[tempName] = newValue;
        component.set("v.webServiceSettings", webServiceConfiguration);
    },
    validateNumber: function (component, event, helper) {
    }
})