({
    loadSearchEngineConfiguration: function (component) {
        this.showSpinner(component.getSuper());
		var helper = this;
        var action = component.get("c.getSearchEngineConfiguration");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.searchEngineConfiguration", response.getReturnValue()[0]);
                component.set("v.searchSettings", response.getReturnValue()[1]);
                component.set("v.webServiceSettings", response.getReturnValue()[2]);
                this.sumTotalWeightings(component);

            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                    }
                } else {
                }
            }
			helper.hideSpinner(component.getSuper());
        });
        $A.enqueueAction(action);
    },
    implementNewSettings: function (component, helper) {
        var action = component.get("c.implementNewSettings");

        var searchEngineConfiguration = component.get("v.searchEngineConfiguration");
        var searchSettings = component.get("v.searchSettings");
        var webServiceSettings = component.get("v.webServiceSettings");
        var totalObject = {
            'sec': searchEngineConfiguration,
            'search': searchSettings,
            'ws': webServiceSettings
        };
        if (searchEngineConfiguration["cncrg__Max_Suggestions__c"] >= 100) {
            this.hideSpinner(component.getSuper());
            helper.showMessageErrorMessage(component, null, $A.get("$Label.c.Max_Suggestions_Error"));
            return;
        }
        if (searchEngineConfiguration["cncrg__Max_Suggestions__c"] < 0 || isNaN(searchEngineConfiguration["cncrg__Max_Suggestions__c"])) {
            this.hideSpinner(component.getSuper());
            helper.showMessageErrorMessage(component, null, $A.get("$Label.c.Max_Suggestions_Small"));
            return;
        }
        var fields = ['cncrg__Votes__c', 'cncrg__Age__c', 'cncrg__Relevance__c', 'cncrg__Search_History__c'];
        var isError = false;
        var totalPercent = 0;
        fields.forEach(function (element) {
            if (searchSettings[element]) {
                totalPercent +=  Number(searchSettings[element]);
                if (searchSettings[element] < 0 || searchSettings[element] > 100) {
                    helper.showMessageErrorMessage(component, null, $A.get("$Label.c.Unknown_error"));
                    isError = true;
                }
            }
        });

        if (totalPercent !== 100) {
            this.hideSpinner(component.getSuper());
            helper.showMessageErrorMessage(component, null, $A.get("$Label.c.Sum_Values"));
            isError = true;
        }
        action.setParams({
            newSettings: JSON.stringify(totalObject)
        });
        if (!isError) {
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    helper.showMessageAccessMessage(component);
                } else if (state === "ERROR") {
                    helper.hideSpinner(component.getSuper());
                    helper.showMessageErrorMessage(component, response.getError(), $A.get("$Label.c.Unknown_error"));
                }
            });
            $A.enqueueAction(action);
        }
    },
    showMessageAccessMessage: function (component) {
        component.find("messageTop").set("v.severity", "confirm");
        component.find("messageBottom").set("v.severity", "confirm");
        $A.util.removeClass(component.find("confirmationTop"), "display_false");
        $A.util.removeClass(component.find("confirmationBottom"), "display_false");
        this.hideSpinner(component.getSuper());
        component.set("v.message", "All changes were saved successfully.");
        window.setTimeout(
            $A.getCallback(function () {
                if (component.isValid()) {
                    $A.util.addClass(component.find("confirmationTop"), "display_false");
                    $A.util.addClass(component.find("confirmationBottom"), "display_false");
                }
            }), 2000
        );
    },
    showMessageErrorMessage: function (component, errors, message) {
        if (errors) {
            if (errors[0] && errors[0].message) {
                message = "Error message: " + errors[0].message;
            }
        }
        component.set("v.message", message);
        component.find("messageTop").set("v.severity", "error");
        component.find("messageBottom").set("v.severity", "error");
        $A.util.removeClass(component.find("confirmationBottom"), "display_false");
        $A.util.removeClass(component.find("confirmationTop"), "display_false");
        //component.set("v.message", message);
    },

    sumTotalWeightings: function (component) {
        var searchSettings = component.get("v.searchSettings");
        var totalWeightings = 0;

        if (searchSettings.cncrg__Votes__c < 0) {
            component.set('v.searchSettings.cncrg__Votes__c', null);
            searchSettings.cncrg__Votes__c = 0;
            this.showMessageErrorMessage(component, null, $A.get("$Label.c.Values_must_be_positive"));
        }
        totalWeightings = totalWeightings + Number(searchSettings.cncrg__Votes__c);
        if (searchSettings.cncrg__Search_History__c < 0) {
            component.set('v.searchSettings.cncrg__Search_History__c', null);
            searchSettings.cncrg__Search_History__c = 0;
            this.showMessageErrorMessage(component, null, $A.get("$Label.c.Values_must_be_positive"));
        }
        totalWeightings = totalWeightings + Number(searchSettings.cncrg__Search_History__c);
        if (searchSettings.cncrg__Relevance__c < 0) {
            component.set('v.searchSettings.cncrg__Relevance__c', null);
            searchSettings.cncrg__Relevance__c = 0;
            this.showMessageErrorMessage(component, null, $A.get("$Label.c.Values_must_be_positive"));
        }
        totalWeightings = totalWeightings + Number(searchSettings.cncrg__Relevance__c);
        if (searchSettings.cncrg__Age__c < 0) {
            component.set('v.searchSettings.cncrg__Age__c', null);
            searchSettings.cncrg__Age__c = 0;
            this.showMessageErrorMessage(component, null, $A.get("$Label.c.Values_must_be_positive"));
        }
        totalWeightings = totalWeightings + Number(searchSettings.cncrg__Age__c);
        var totalWeightingsCmp = component.find('totalWeightings');
        if(totalWeightings === 100){
            $A.util.removeClass(totalWeightingsCmp, 'isNoValid');
            $A.util.addClass(totalWeightingsCmp, 'isValid');
        }
        else{
            $A.util.removeClass(totalWeightingsCmp, 'isValid');
            $A.util.addClass(totalWeightingsCmp, 'isNoValid');
        }
        component.set("v.totalWeightings", totalWeightings);
    }
})