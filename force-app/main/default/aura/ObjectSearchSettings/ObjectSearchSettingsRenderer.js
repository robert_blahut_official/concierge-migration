({
	render: function(cmp, helper) {
        helper.showSpinner(cmp.getSuper());
		helper.getTypesSettings(cmp,false);
        var ret = cmp.superRender(); 
        return ret; 
    },
})