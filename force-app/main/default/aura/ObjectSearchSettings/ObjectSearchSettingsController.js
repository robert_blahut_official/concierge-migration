({
    goBack : function(component, event, helper) {
		$A.get("e.c:BackToSettings").fire();
	},
    saveChanges : function (component, event, helper) {
		helper.implementNewSettings(component);
        helper.showSpinner(component.getSuper());
	},
    addDefaultField : function(component, event){
		 var listOfTypeSettings = component.get("v.listOfTypeSettings");
		 var pos = event.currentTarget.dataset.record;
		 if(listOfTypeSettings[pos].isActive)
			listOfTypeSettings[pos].isActive = false;
		 else
		    listOfTypeSettings[pos].isActive = true;
		 component.set("v.listOfTypeSettings",listOfTypeSettings);

		 if(listOfTypeSettings[pos].isActive && (listOfTypeSettings[pos].selectedFields.length === 0 || (!listOfTypeSettings[pos].selectedFields[0].hasOwnProperty('position'))))
			$A.get("e.c:CreateNewSetting").setParams({"positionInList" : pos}).fire();

	},
    addNewRecord : function (component, event, helper) {
		var typeNumber = event.getParam('positionInList');
		var listOfTypeSettings = component.get("v.listOfTypeSettings");
		var listOfSelectedFields = JSON.parse(JSON.stringify(listOfTypeSettings[typeNumber].selectedFields));

		for (var i = listOfSelectedFields.length - 1; i >= 0; i--) {
			if (listOfSelectedFields[i].position === undefined) {
				listOfSelectedFields.splice(i,1);
			}
		}

		var settings = {
			'fieldName' : 'Summary',
			'isActive' : false,
			'position' : listOfSelectedFields.length + 1
		};
		if (event.getParam('fieldName')) {
			settings.fieldName = event.getParam('fieldName');
		}
		if (event.getParam('fieldType')) {
			settings.fieldType = event.getParam('fieldType');
		}
		listOfSelectedFields.push(settings);

		var attachments = listOfSelectedFields.filter(function(el, index) {
			return el.fieldType === 'attachments_flag';
		});
		var finalSelectedFields = listOfSelectedFields.filter(function(el, index) {
			return el.fieldType !== 'attachments_flag';
		});
		if (attachments.length === 1) {
			finalSelectedFields.push(attachments[0]);
		}

		for (var i = 0; i < finalSelectedFields.length; i++) {
			finalSelectedFields[i].position = i + 1;
		}
		listOfTypeSettings[typeNumber].selectedFields = finalSelectedFields;
		component.set("v.listOfTypeSettings",[]);
		component.set("v.listOfTypeSettings", listOfTypeSettings);
	},

	deleteTypeField : function(component, event){
		var typeNumber = event.getParam('positionInList');
		var fieldPosition = event.getParam('fieldPosition');
		var typeSettingsList = component.get("v.listOfTypeSettings");
		var listOfTypeSettings = component.get("v.listOfTypeSettings");
		var selectedFields = JSON.parse(JSON.stringify(listOfTypeSettings[typeNumber].selectedFields));
		selectedFields.splice(fieldPosition,1);

		var attachments = selectedFields.filter(function(el, index) {
			return el.fieldType === 'attachments_flag';
		});
		var finalSelectedFields = selectedFields.filter(function(el, index) {
			return el.fieldType !== 'attachments_flag';
		});
		if (attachments.length === 1) {
			finalSelectedFields.push(attachments[0]);
		}

		for (var i = 0; i < finalSelectedFields.length; i++) {
			finalSelectedFields[i].position = i + 1;
		}
		listOfTypeSettings[typeNumber].selectedFields = finalSelectedFields;
		component.set("v.listOfTypeSettings",[]);
		component.set("v.listOfTypeSettings",listOfTypeSettings);
			
	},
	addNewObjectRecord : function(component, event){
		/*Show popUp with list of the articles*/
		var objectList = component.get("v.listOfTypeSettings");
		var listOfAvailableObjects = [];
		for(var i=0, j=objectList.length; i<j; i++) {
			//if(!objectList[i].isActive) {
			//	objectList[i].isActive = true;
			//	break;
			if(!objectList[i].isActive) {
				listOfAvailableObjects.push(objectList[i].articleType);
			}
			//}
			
		}
		listOfAvailableObjects.sort();
		//component.set("v.listOfTypeSettings", objectList);
		component.set("v.listOfAvailableObjects", listOfAvailableObjects);
		component.set("v.showPopUp", true);

	},
	changeObjectPosition : function (component, event, helper) {
		var selectedObject = event.getSource().get("v.value");
		var selectedWrapper;
		if(selectedObject === 'None') {
			return;
		}
		var objectList = component.get("v.listOfTypeSettings");
		var newObjectList = [];
		for(var i=0, j=objectList.length; i<j; i++) {
			if(objectList[i].articleType == selectedObject) {
				objectList[i].isActive = true;
				selectedWrapper = objectList[i];
			}
			else {
				newObjectList.push(objectList[i]);
			}
		}
		newObjectList.push(selectedWrapper);
		objectList.push(selectedWrapper);
		component.set("v.listOfTypeSettings", newObjectList);
		component.set("v.showPopUp", false);
		
	}, 
	closeWindow : function (component, event, helper) {
		component.set("v.showPopUp", false);
	},
	showFiltersPopUp : function (component, event, helper) {
		var objectName = event.target.id;
		$A.createComponent(
			"c:ObjectFieldFilters",
			{
				"listOfTypeSettings" : component.getReference("v.listOfTypeSettings"),
				"objectName" : objectName,
				"filtersPopUp" : component.getReference("v.filtersPopUp")
			},
			function(filtersPopUp, status, errorMessage) {

				if (status === "SUCCESS") {
					component.set("v.filtersPopUp", filtersPopUp);
				}
				else {
				}
			}
		);
	}
})