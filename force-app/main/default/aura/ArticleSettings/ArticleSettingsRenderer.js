({
	render: function(cmp, helper) {
		helper.isLightningKnowledgeEnabled(cmp,false);
        var ret = cmp.superRender(); 
        return ret; 
    },
})