({
	sortTypeFields: function(component,obj){
		var filteredTypeFields = [];
		obj.forEach(function(item, i, list) {
			var tempFiledsArray = item['allAvailableFields'];
			tempFiledsArray = tempFiledsArray.sort(function(a, b) {
				var textA = a.fieldLabel;
				var textB = b.fieldLabel;
				return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
			});
			item['allAvailableFields'] = tempFiledsArray;
			filteredTypeFields.push(item);
		});
		component.set("v.listOfTypeSettings",filteredTypeFields);
	},

	implementNewSettings: function (component) {
		var listOfTypeSettings = component.get("v.listOfTypeSettings");
		var listOfIdsToDelete = component.get("v.listOfIdsToDelete");
		var duplicates = false;
		var articleTypesValid = true;
		var existingArticleNamesArray = [];
		var helper = this;

		listOfTypeSettings.forEach(function(oneSetting, i) {
			if(
				oneSetting.isActive
				&& (
					oneSetting.selectedFields.length === 0 
					|| (!oneSetting.selectedFields[0].hasOwnProperty('position'))
					|| (oneSetting.selectedFields.length === 1 
						&& oneSetting.selectedFields[0].fieldType === 'attachments_flag')
					)
				) {

				component.find("messageTop").set("v.severity", "warning");
				component.find("messageBottom").set("v.severity", "warning");
				$A.util.removeClass(component.find("confirmationTop"), "display_false");
				$A.util.removeClass(component.find("confirmationBottom"), "display_false");
				component.set("v.message", $A.get("$Label.c.Please_Select_a_Field_to_Display_for") + " "+oneSetting.articleType+" " + $A.get("$Label.c.Type"));
				articleTypesValid = false;
				/*$A.get("e.c:HideSpinner").fire();*/
				helper.hideSpinner(component.getSuper());
			}
			if(oneSetting.name!=='') {
				existingArticleNamesArray.push(oneSetting.name);
			}
		});
		
		if(articleTypesValid){
			this.assignNamesToArticleTypes(component,listOfTypeSettings,existingArticleNamesArray);
			listOfTypeSettings = component.get("v.listOfTypeSettings");

			listOfTypeSettings.forEach(function(oneSetting, i) {
				var listOfValues = [];

				oneSetting.selectedFields.forEach(function(field, j) {
					if (listOfValues.indexOf(field.fieldName) === -1) {
						listOfValues.push(field.fieldName);
					} else {
						duplicates = true;
					}
				});
			});

			if (duplicates) {
				component.find("messageTop").set("v.severity", "error");
				component.find("messageBottom").set("v.severity", "error");
				$A.util.removeClass(component.find("confirmationTop"), "display_false");
				$A.util.removeClass(component.find("confirmationBottom"), "display_false");
				var staticLabel = $A.get("$Label.c.Duplicate_Values");
				component.set("v.message", staticLabel);
				/*$A.get("e.c:HideSpinner").fire();*/
				this.hideSpinner(component.getSuper());
				return;
			}

			var action = component.get("c.implementNewSettings");
			action.setParams({
				newSettings : JSON.stringify({ 'ts' : listOfTypeSettings })
			});
			action.setCallback(this, function(response) {
				var state = response.getState();
				if (state === "SUCCESS") {
					this.getTypesSettings(component, true);
				} else if (state === "ERROR") {
					var errors = response.getError();
					var message = "Unknown error";
					if (errors) {
						if (errors[0] && errors[0].message) {
							message = "Error message: " + errors[0].message;
						}
					} else {
					}
					/*$A.get("e.c:HideSpinner").fire();*/
					this.hideSpinner(component.getSuper());
					component.find("message").set("v.severity", "error");
					$A.util.removeClass(component.find("confirmation"), "display_false");
					component.set("v.message", message);
				}
			});
			$A.enqueueAction(action);
		}
	},
	isLightningKnowledgeEnabled : function (component,showMessage) {
		component.set("v.showPopUp", false);
		var action = component.get("c.isLightningKnowledgeEnabled");
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var responceRecord = response.getReturnValue();
				component.set("v.isLightningKnowlegesEnabled", responceRecord[0]);
				if(responceRecord.length === 1) {
					this.getTypesSettings(component, showMessage);
				}
				else {
					component.set("v.showPopUp", true);
					var opts = [];
					for(var i=0, j=responceRecord[1].length; i<j; i++) {
						if(i === 0) {
							opts.push({ label: responceRecord[1][i], value: responceRecord[1][i], selected: "true" });
						}
						else {
							opts.push({ label: responceRecord[1][i], value: responceRecord[1][i] });
						}
					}
					component.find("InputSelectDynamic").set("v.options", opts);
					
					
					/*show popup with availlable articleTypes*/
				}
			} 
		});
		$A.enqueueAction(action);
	},
	getTypesSettings : function (component,showMessage) {
		/*$A.get("e.c:ShowSpinner").fire();*/
		this.showSpinner(component.getSuper());
		var action = component.get("c.getTypesSettings");
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				component.set("v.listOfTypeSettings", response.getReturnValue());
				var listOfTypes = component.get("v.listOfTypeSettings");
				this.sortTypeFields(component, listOfTypes);
				/*$A.get("e.c:HideSpinner").fire();	*/
				this.hideSpinner(component.getSuper());			
				if (showMessage) {
					component.find("messageTop").set("v.severity", "confirm");
					component.find("messageBottom").set("v.severity", "confirm");
					$A.util.removeClass(component.find("confirmationTop"), "display_false");
					$A.util.removeClass(component.find("confirmationBottom"), "display_false");
					var staticLabel = $A.get("$Label.c.Changes_Implemented");
					component.set("v.message", staticLabel);

					window.setTimeout(
					$A.getCallback(function() {
						if (component.isValid()) {
							$A.util.addClass(component.find("confirmationTop"), "display_false");
							$A.util.addClass(component.find("confirmationBottom"), "display_false");
						}
					}), 6000
				);
				}
			} else if (state === "ERROR") {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
					}
				} else {
				}
			}
		});
		$A.enqueueAction(action);
	},
	assignNamesToArticleTypes : function(component,allTypes,existingArticleNamesArray){
		
		allTypes.forEach(function(item, i, typesArray){
			if(item.name === ''){
				if(item.articleType.length > 36){
					var shortName = item.articleType.substring(0,35) + '01';
					var maxExistingIndex = 0;
					if(existingArticleNamesArray){
						existingArticleNamesArray.forEach(function(articleName, j, articleNamesArray){
							var tempIndex;
							if(articleName === shortName){
								tempIndex = parseInt(articleName.substring(36,38),10);
								maxExistingIndex = maxExistingIndex < tempIndex ? tempIndex : maxExistingIndex;
								tempIndex++;
								shortName = tempIndex >= 10 ? shortName.substring(0,35) +tempIndex : 
											shortName.substring(0,35) + '0' + tempIndex;
							}									
						});
					}
					maxExistingIndex++;
					
					if(maxExistingIndex >= 10){
						item.name = item.articleType.substring(0,35) + maxExistingIndex;
						existingArticleNamesArray.push(item.name);
					}
					else{
						item.name = item.articleType.substring(0,35) +'0'+ maxExistingIndex;
						existingArticleNamesArray.push(item.name);
					}
				}else
					item.name = item.articleType;
			}
		});
		component.set("v.listOfTypeSettings",allTypes);

	},
	saveLightningArticleType : function (component) {
		var action = component.get("c.saveLightningArticleType");
		action.setParams({
			articleType : component.find("InputSelectDynamic").get("v.value")
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				/*$A.get("e.c:HideSpinner").fire();*/
				this.hideSpinner(component.getSuper());
				this.isLightningKnowledgeEnabled(component,false);
			} 
		});
		$A.enqueueAction(action);
	}
})