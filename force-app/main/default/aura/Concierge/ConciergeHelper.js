({	
	getApplicationName : function(component) {
		this.showSpinner(component.getSuper());
		var action = component.get("c.getApplicationName");
		var helper = this;
		action.setStorable();
        action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var appName = response.getReturnValue();
				if (!appName || appName === null || appName.trim() === '') {
					appName = 'Cadalys Concierge ™';
				}
				component.set('v.appName',appName);

			} else if (state === "ERROR") {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
					}
				} else {
				}
			}
			helper.hideSpinner(component.getSuper());
		});
        $A.enqueueAction(action);
	},
    getLiveAgentPage : function(component) {
		var helper = this;
		this.showSpinner(component.getSuper());
        var action = component.get("c.getLiveAgentPage");
		action.setStorable();
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.liveAgentPage", response.getReturnValue()[0]);
				component.set("v.useLiveagentStartChatWithWindowMethod", response.getReturnValue()[1]);
            } else if (state === "ERROR") {
                var errors = response.getError();
            }
			helper.hideSpinner(component.getSuper());
        });
        $A.enqueueAction(action);
    }
})