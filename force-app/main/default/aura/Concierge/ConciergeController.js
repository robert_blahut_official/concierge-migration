({
	doInit: function(component, event, helper) {
		helper.showSpinner(component.getSuper());
		helper.getLiveAgentPage(component);
		var appName = component.get("v.appName");

		if (!appName || appName === null || appName.trim() === '') {
			helper.getApplicationName(component);
		}

        var isWEBKIT = $A.get('$Browser.isWEBKIT');
		var isAndroid = $A.get("$Browser.isAndroid");
        window.setTimeout(
            $A.getCallback(function() {
				var box1 = component.find("total_div");
				if (box1 && box1.getElement()) {
					if(isAndroid && !isWEBKIT){
						$A.util.addClass(box1.getElement(), "sf1AndrBg");
					} else {
						$A.util.removeClass(box1.getElement(), "sf1AndrBg");
					}
				}
            }), 3000
        );
        
		
	},

	ttstart: function(component, event) {
		component.set('v.touchStart', event.touches[0].clientX);
	},

	ttmove: function(component, event) {
		var start = component.get('v.touchStart');
		var now = event.touches[0].clientX;
		var dist = now - start;
		if (dist > 130) {
			component.set('v.showHideNavLeftMini', true);
		}
		if (dist < -130) {
			component.set('v.showHideNavLeftMini', false);
		}
	},

	hideMessage: function(component, event, helper) {
		if (component.get('v.showInfoMessage') === true 
				&& component.find('message_section')
				&& component.find('message_section').getElement()) {
			component.set('v.showInfoMessage', false);
			$A.util.removeClass(component.find('message_section').getElement(), "active");
		}
		var cmpEvent = $A.get("e.c:HideSearchSuggestions");
		cmpEvent.fire();
	},

	changeTab: function(component, event, helper) {
		var componentName = event.getParam("componentName");
		var dynamicArray = [];
		var dynamicElement = [componentName, {
			"isCommunity": false
		}];
		dynamicArray.push(dynamicElement);
		if (componentName.indexOf('CasesList') >= 0) {
			dynamicElement[1].availableLA = component.get('v.availableLA');
			dynamicElement[1].isLEXMobile = component.get('v.isLEXMobile');
		} else if (componentName.indexOf('FavoriteArticlesList') >= 0) {
			dynamicElement[1].isLEXMobile = component.get('v.isLEXMobile');
		}

		if (componentName !== 'home') {
			// $A.util.addClass(component.find('searchResultsSuggestions').getElement(), "display_false");
			$A.createComponents(
				dynamicArray,
				function(components, status, errorMessage) {
					if (status === "SUCCESS") {
						if (componentName.indexOf('LogTicket') >= 0) {
							component.set("v.logTicketBody", components);
						} else {
							component.set("v.body", components);
						}
					}
				}
			);
		} else {
			component.set("v.logTicketBody", null);
			component.set("v.body", null);
			$A.util.removeClass(component.find('searchResultsSuggestions').getElement(), "display_false");
		}
		return false;
	},

	setPositionBg: function(component, event) {
		component.get('v.isHomeTab');
	}
})