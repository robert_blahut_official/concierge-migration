({
	doInit: function(component) {
		var helper = this;

		var action = component.get("c.getInitialSettings");
		action.setStorable();
		action.setCallback(helper, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {

				var responseList = response.getReturnValue();
				helper.showActionsSettings(component, responseList[5]);
				component.set("v.recordsOnOnePage", responseList[0]);
				component.set("v.showActionsSetupButton", responseList[1]);
				component.set("v.itHelpUrl", responseList[2]);
				component.set("v.contactRecord", responseList[3]);
				component.set("v.isUserHasPermitionToCreateCase", responseList[4]);
				component.set("v.hasUserPermitionToCreateCase", responseList[6]);

				var query = helper.getUrlParam(helper.QUERY_PARAMS.QUERY);
				if (query) {
					var applicationEvent = $A.get("e.c:SearchQueryUpdated");
					applicationEvent.setParams({
						"searchQuery": query,
						"needSuggestion": false
					});
					applicationEvent.fire();
				}

				if (window.innerWidth >= 1025) {
					component.set("v.isShowListOfSuggests", true);
					component.set("v.isShowDetailOfSuggests", true);
					component.set("v.isMobileMode", false);
				} else {
					component.set("v.isShowListOfSuggests", true);
					component.set("v.isShowDetailOfSuggests", false);
					component.set("v.isMobileMode", true);
					component.set("v.showBtnBack", false);
				}
				window.addEventListener('resize', function() {
					if (window.innerWidth >= 1025) {
						component.set("v.isShowListOfSuggests", true);
						component.set("v.isShowDetailOfSuggests", true);
						component.set("v.isMobileMode", false);
					} else {
						component.set("v.isMobileMode", true);
						var isExpandable = component.get("v.isShowDetailOfSuggests");
						if (isExpandable === true) {
							component.set("v.isShowListOfSuggests", false);
							component.set("v.isShowDetailOfSuggests", true);
							component.set("v.showBtnBack", true);
						} else {
							component.set("v.isShowListOfSuggests", true);
							component.set("v.isShowDetailOfSuggests", false);
							component.set("v.showBtnBack", false);
						}
					}
				})

			} else if (state === "ERROR") {
				var errors = response.getError();
			}
		});
		$A.enqueueAction(action);
	},

	startSearchSuggestions: function(component, requestParams) {
		var helper = this;
		var timeOut = component.get('v.searchTimeout');
		if (timeOut) {
			clearTimeout(timeOut);
		}

		timeOut = window.setTimeout(
			$A.getCallback(function() {
				helper.getSearchSuggestions(component, requestParams);
			}),
			500
		);
		component.set('v.searchTimeout', timeOut);
	},

	getSearchSuggestions: function(component, requestParams) {
		var helper = this;
		var action = component.get("c.getSuggestionsForQuery");
		action.setParams(requestParams);
		action.setCallback(helper, function(response) {
			if (response.getState() === "SUCCESS") {
				var searchSuggestions = response.getReturnValue();
				component.set("v.searchSuggestions", searchSuggestions);
				$A.get("e.c:SendSuggestionsToInput").setParams({
					"listOfSuggestions": searchSuggestions
				}).fire();
				$A.util.removeClass(component.find('tr_suggest_all_table'), 'hide');
			}
			helper.hideSpinner(component.getSuper());
		});
		$A.enqueueAction(action);
	},

	getSearchResults: function(component, requestParams) {
		var helper = this;
		$A.util.addClass(component.find('tr_suggest_all_table'), 'hide');
		helper.showSpinner(component.getSuper());
		component.set("v.result", undefined);

		var action = component.get("c.getSearchResults");
		action.setParams(requestParams);
		action.setCallback(helper, function(response) {
			if (response.getState() === "SUCCESS") {

				$A.util.removeClass(component.find('results_div'), 'hidden_div');
				component.set("v.pageNumber", 0);
				component.set("v.selectedPosition", undefined);
				component.set("v.searchText", requestParams.query);
				component.set("v.searchSuggestions", []);
				component.set("v.pageNumberDelta", 0);

				var recordsOnOnePage = component.get("v.recordsOnOnePage");
				var results = response.getReturnValue()[0];
				var searchStagingRecord = response.getReturnValue()[1];

				component.set("v.lastPageNumber", Math.ceil(results.length / recordsOnOnePage));

				helper.updateListOfPages(component, 0);

				component.set("v.searchStagingRecord", searchStagingRecord);
				component.set("v.searchResults", results);
				component.set("v.searchResultsWithSettings", JSON.parse(JSON.stringify(results)));
				component.set("v.selectedPosition", 0);

				if (results.length > 0) {
					component.set("v.isWhitesmokeBg", false);
					$A.util.addClass(component.find('no_results_section'), 'no_res_hidden');

					var resultSnippet = results[0].snippet;
					if (resultSnippet) {
						resultSnippet = resultSnippet.replace(/<mark>/g, '<b>').replace(/<\/mark>/g, '</b>');
						resultSnippet = resultSnippet.replace(/<em>/g, '<b>').replace(/<\/em>/g, '</b>');
					} else {
						resultSnippet = ' ';
					}
					results[0].snippet = resultSnippet;

					if (results[0] && results[0].previewBody) {
						results[0].previewBody = (new DOMParser()).parseFromString(results[0].previewBody, "text/html").documentElement.textContent;
					}
					if (results[0].body.indexOf('{!ltngext.') !== -1) {
						var className = helper.prepareComponentName(component, results[0].body);
						helper.getSnippetExtentionValue(component, className);
						results[0].previewBody = results[0].snippet = '';
					}
					component.set("v.result", results[0]);

					if (results[0].setting !== undefined || results[0].setting !== null) {
						component.set("v.activeActions", results[0].setting);
						component.set("v.activeCaseLayoutRecord", results[0].settingCaseLayout);
						component.set("v.selectedElement", results[0]);
						component.set("v.isActionsExists", true);
					}

					var result = component.get("v.result");
					if (result.rating === 1) {
						component.set("v.ratingLabel", $A.get("$Label.c.Person_thought_this_article_was_helpful"));
					} else {
						component.set("v.ratingLabel", $A.get("$Label.c.People_thought_this_article_was_helpful"));
					}
					// logic to implement query that stored in the URL
					helper.implementQueryFromURL(component, results);
				} else {
					component.set("v.isWhitesmokeBg", true);
					$A.util.removeClass(component.find('no_results_section'), 'no_res_hidden');
				}
			}

			helper.hideSpinner(component.getSuper());
		});
		$A.enqueueAction(action);
	},

	highlightArticleTitle: function(component, currentArticle, loadArticleData) {
		var previousElement = component.get("v.selectedElement");
		var allResults = component.get("v.searchResults");
		if (loadArticleData) {
			if (!previousElement || currentArticle.id !== previousElement.id) {
				if (previousElement) {
					$A.util.removeClass(previousElement, "active");
				}
				$A.util.addClass(currentArticle, "active");
				component.set("v.selectedElement", currentArticle);
				//component.set("v.selectedPosition", currentArticle.id);	
			}
		}
	},

	getArticleSettingsInformation: function(component, newResult, position, currentArticle) {
		var helper = this;
		if (newResult.rating !== 0) {
			newResult.rating = 0;
		}

		var action = component.get("c.getAllArticleSettings");
		action.setParams({
			'articleRecord': JSON.stringify(newResult),
			'prefix': component.get('v.communityPrefix')
		});
		action.setCallback(helper, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var newActionResult = response.getReturnValue();
				helper.checkExponentialSlider(component, newActionResult, 1);
				component.set("v.activeActions", newActionResult.setting);
				component.set("v.activeCaseLayoutRecord", newActionResult.settingCaseLayout);
				component.set("v.isActionsExists", true);

				helper.filterSnippetValue(component, newActionResult, position);

				if (newActionResult.rating === 1) {
					component.set("v.ratingLabel", $A.get("$Label.c.Person_thought_this_article_was_helpful"));
				} else {
					component.set("v.ratingLabel", $A.get("$Label.c.People_thought_this_article_was_helpful"));
				}

			} else if (state === "ERROR") {
				var errors = response.getError();
			}

			if (helper.getUrlParam(helper.QUERY_PARAMS.DETAILS) === 'true') {
				helper.showFullArticle(component);
			}
			helper.hideSpinner(component.getSuper());
		});
		$A.enqueueAction(action);
	},

	filterSnippetValue: function(component, newResult, position) {
		var previousButtonId = component.get('v.previousButtonId');
		var buttonId = component.get('v.activeActions.cncrg__Chat_Button__c');
		if (previousButtonId != buttonId) {
			component.set('v.previousButtonId', buttonId);
			var liveAgentCustomEvent = $A.get("e.c:LiveAgentCustomEvent");
			liveAgentCustomEvent.setParams({
				"buttonId": buttonId,
				"changeButtonId": true
			});
			liveAgentCustomEvent.fire();
		}

		var resultSnippet = '';
		if (newResult.snippet) {
			if (newResult.snippet.indexOf('{!') === -1) {
				resultSnippet = newResult.snippet;
			}
		}

		if (resultSnippet) {
			resultSnippet = resultSnippet.replace(/<mark>/g, '<b>').replace(/<\/mark>/g, '</b>');
			resultSnippet = resultSnippet.replace(/<em>/g, '<b>').replace(/<\/em>/g, '</b>');
		} else
			resultSnippet = ' ';

		newResult.snippet = resultSnippet;
		if (newResult && newResult.previewBody) {
			newResult.previewBody = (new DOMParser()).parseFromString(newResult.previewBody, "text/html").documentElement.textContent;
		}
		component.set("v.result", newResult);
		var allResults = component.get('v.searchResultsWithSettings');
		allResults[position] = newResult;
		component.set('v.searchResultsWithSettings', allResults);
		component.set("v.selectedPosition", Number(position));
		// logic to provide Sid
		// this.updateListOfPages(component, 0);
		this.implementSid(component, position);
	},

	updateListOfPages: function(component, pageNumber) {
		var lastPageNumber = component.get("v.lastPageNumber");
		var newListOfPagesToDisplay = [];
		newListOfPagesToDisplay.push({
			'label': 1,
			'value': 0,
			'clickable': true,
			'selected': 0 === pageNumber
		});

		if (pageNumber > 2 && lastPageNumber > 5) {
			newListOfPagesToDisplay.push({
				'label': '...',
				'value': 'empty',
				'clickable': false,
				'selected': false
			});
		}
		var startIndex = pageNumber - 1;
		if (startIndex <= 0) {
			startIndex = 1;
		}

		var endIndex = startIndex + 3;
		if (endIndex > lastPageNumber) {
			endIndex = lastPageNumber;
		}

		if (lastPageNumber < 6) {
			startIndex = 1;
		}

		for (var i = startIndex; i < endIndex; i++) {
			newListOfPagesToDisplay.push({
				'label': i + 1,
				'value': i,
				'clickable': true,
				'selected': i === pageNumber
			});
		}
		if (endIndex < lastPageNumber) {
			if (endIndex + 1 < lastPageNumber) {
				newListOfPagesToDisplay.push({
					'label': '...',
					'value': 'empty',
					'clickable': false,
					'selected': false
				});
			}
			newListOfPagesToDisplay.push({
				'label': lastPageNumber,
				'value': lastPageNumber - 1,
				'clickable': true,
				'selected': lastPageNumber - 1 === pageNumber
			});
		}
		component.get("v.listOfPagesToDisplay", newListOfPagesToDisplay);
		$A.createComponent(
			"c:PageForPagination", {
				"listOfPagesToDisplay": newListOfPagesToDisplay
			},
			function(allPages, status, errorMessage) {
				if (status === "SUCCESS") {
					var listOfPagesSPan = component.find("list_of_pages");
					listOfPagesSPan.set("v.body", allPages);
				}
			}
		);
	},

	getCurrentUrl: function(component) {
		if (component.get("v.communityLoading")) {
			component.set("v.communityPrefix", this.getCommuityPrefix());
		} else {
			component.set("v.communityPrefix", '');
		}
	},

	checkExponentialSlider: function(component, record, score) {
		var recordId = record.knowledgeArticleId;
		var searchStagingRecord = component.get("v.searchStagingRecord");
		var mirrorSearchStagingRecord = JSON.stringify(component.get("v.searchStagingRecord"));
		if (score === 1) {
			searchStagingRecord.schwartz = recordId;
		}
		var currentScore = searchStagingRecord.profigliano[recordId];
		if (!currentScore || currentScore.score == null) {
			if (!currentScore) {
				var newItem = new Object();
				newItem.score = score;
				newItem.type = record.type;
				newItem.dataCategories = record.dataCategories;
				newItem.title = record.title;
				newItem.ownerId = record.ownerId;
				searchStagingRecord.profigliano[recordId] = newItem;
			} else {
				searchStagingRecord.profigliano[recordId].score = score;
			}
			//searchStagingRecord.profigliano[recordId].score = score;
		} else if (!this.checkCurrentExponential(currentScore.score, score)) {
			searchStagingRecord.profigliano[recordId].score += score;
		}
		if (JSON.stringify(searchStagingRecord) !== mirrorSearchStagingRecord) {
			component.set("v.searchStagingRecord", searchStagingRecord);
			if (score > 0) {
				this.saveSearchStagingRecord(JSON.stringify(searchStagingRecord), component);
			}
		}

	},
	addFeedBack: function(component, recordId, reasons, comment) {
		if (reasons || comment) {
			var searchStagingRecord = component.get("v.searchStagingRecord");
			var currentprofigliano = searchStagingRecord.profigliano[recordId];
			searchStagingRecord.profigliano[recordId].reasons = reasons;
			searchStagingRecord.profigliano[recordId].comment = comment;
			component.set("v.searchStagingRecord", searchStagingRecord);
			this.saveSearchStagingRecord(JSON.stringify(searchStagingRecord), component);
		}
	},
	checkCurrentExponential: function(currentScore, score) {
		if (currentScore === score) {
			return true;
		}
		if (currentScore === 0 || score < 0) {
			return false;
		}
		var exponentialArray = [1024, 512, 256, 128, 64, 32, 16, 8, 4, 2, 1];
		for (var i = 0; i < exponentialArray.length; i++) {
			if (currentScore > exponentialArray[i]) {
				if (score === exponentialArray[i]) {
					return true;
				} else {
					return this.checkCurrentExponential(currentScore - exponentialArray[i], score);
				}
			}
		}
	},
	saveSearchStagingRecord: function(searchStagingRecord, component) {
		var action = component.get("c.saveSearchStagingRecord");
		action.setParams({
			'searchStagingRecordStr': searchStagingRecord
		});
		action.setCallback(this, function(response) {});
		$A.enqueueAction(action);
	},
	deleteSchwartzc: function(component) {
		var searchStagingRecord = component.get("v.searchStagingRecord");
		var mirrorSearchStagingRecord = JSON.stringify(searchStagingRecord);
		searchStagingRecord.schwartz = null;
		if (JSON.stringify(searchStagingRecord) !== mirrorSearchStagingRecord) {
			component.set("v.searchStagingRecord", searchStagingRecord);
			this.saveSearchStagingRecord(JSON.stringify(searchStagingRecord), component);
		}
	},
	createArticleDetailsComponent: function(result, component) {
		var helper = this;
		$A.createComponent(
			"c:ArticleDetails", {
				"result": component.getReference("v.result"),
				"searchText": component.get("v.searchText"),
				"appName": component.get("v.appName"),
				"activeActions": component.get("v.activeActions"),
				"activeCaseLayoutRecord": component.get("v.activeCaseLayoutRecord"),
				"articleDetailPopup": component.getReference("v.articleDetailPopup"),
				"communityLoading": component.get("v.communityLoading"),
				"availableLA": component.get("v.availableLA"),
				"communityPrefix": component.get("v.communityPrefix"),
				"isUserHasPermitionToCreateCase": component.get("v.isUserHasPermitionToCreateCase")
			},
			function(articleDetailsBlock, status, errorMessage) {
				if (status === "SUCCESS") {
					component.set("v.articleDetailPopup", articleDetailsBlock);
					// logic to provide Sid
					helper.openRelatedArticleFromURL();
				}
			}
		);
	},

	liveAgentHandler: function(component) {
		window.addEventListener('message', function(event) {
			if (event.data === 'AVAILABLE') {
				component.set('v.availableLA', true);
				component.set('v.liveAgentPageOrigin', event.origin);
			} else if (event.data === 'UNAVAILABLE') {
				component.set('v.availableLA', false);
			}
		}, false);
	},

	showSnippet: function(component, articleNumber) {
		this.showSpinner(component.getSuper());
		var allResults = component.get("v.searchResultsWithSettings");
		var previousElement = component.get("v.selectedElement");
		var firstElementIndex = component.get("v.recordsOnOnePage") * component.get("v.pageNumber");
		if (previousElement.id === undefined && Number(articleNumber) === firstElementIndex) {
			component.set("v.selectedPosition", Number(articleNumber));
		}
		component.set("v.isActionsExists", false);

		if (articleNumber !== component.get("v.selectedElement").id) {
			if (!allResults[articleNumber].setting) {
				component.set("v.selectedPosition", Number(articleNumber));
				this.getArticleSettingsInformation(component, allResults[articleNumber], articleNumber);
			} else {
				component.set("v.activeActions", allResults[articleNumber].setting);
				component.set("v.activeCaseLayoutRecord", allResults[articleNumber].settingCaseLayout);
				component.set("v.isActionsExists", true);
				this.filterSnippetValue(component, allResults[articleNumber], articleNumber);
				// this.highlightArticleTitle(component, currentArticle, true);
				this.hideSpinner(component.getSuper());
				component.set("v.result", allResults[articleNumber]);
			}
			if (allResults[articleNumber].body.indexOf('{!ltngext.') !== -1) {
				var className = this.prepareComponentName(component, allResults[articleNumber].body);
				this.getSnippetExtentionValue(component, className);
			}
		} else {
			component.set("v.isActionsExists", true);
			this.hideSpinner(component.getSuper());
			// logic to provide Sid
			this.implementSid(component, articleNumber);
		}
	},

	// logic to provide Sid
	implementSid: function(component, articleNumber) {
		var helper = this;
		var results = component.get("v.searchResults");
		(results || []).forEach(function(item, index) {
			if (Number(articleNumber) === index) {
				helper.setUrlParam(helper.QUERY_PARAMS.ARTICLE_ID, item.knowledgeArticleId);
			}
		});
	},

	// logic to provide Sid
	implementQueryFromURL: function(component, results) {
		var helper = this;

		var articlePosition = 0;
		var resultMap = helper.getUrlParams();
		var sid = resultMap[helper.QUERY_PARAMS.ARTICLE_ID];

		if (sid) {
			results.forEach(function(item, index) {
				if (sid === item.knowledgeArticleId) {
					articlePosition = index;
				}
			});
		} else {
			articlePosition = Number(component.get('v.selectedPosition'));
		}

		var result = component.get("v.result");
		this.showSnippet(component, articlePosition);
		component.set("v.selectedPosition", articlePosition);

		var recordsOnOnePage = component.get('v.recordsOnOnePage');
		var currentPage = Math.floor(articlePosition / recordsOnOnePage);
		component.set('v.pageNumber', currentPage);
		this.updateListOfPages(component, currentPage);

		if (resultMap[helper.QUERY_PARAMS.DETAILS] === 'true') {
			this.showFullArticle(component);
		}
	},

	openRelatedArticleFromURL: function() {
		var href = this.getUrlParam(this.QUERY_PARAMS.HREF);
		if (href) {
			var relatedArticleClickEvent = $A.get("e.c:RelatedArticleClick");
			relatedArticleClickEvent.setParams({
				"href": href
			});
			relatedArticleClickEvent.fire();
		}
	},

	showFullArticle: function(component) {
		var helper = this;
		var result = component.get("v.result");
		var action = component.get("c.getDetailArticleInformation");

		if (result.body === 'blankValue') {
			action.setParams({
				'articleRecordStr': JSON.stringify(result),
				'prefix': component.get("v.communityPrefix")
			});

			action.setCallback(this, function(response) {
				var state = response.getState();
				if (state === 'SUCCESS') {

					var newResult = response.getReturnValue();
					component.set("v.result", newResult);
					var allResults = component.get("v.searchResultsWithSettings");
					var position = component.get("v.selectedPosition");
					allResults[position] = newResult;
					//component.set('v.searchResults', allResults);
					component.set('v.searchResultsWithSettings', allResults);
					helper.createArticleDetailsComponent(newResult, component);
				} else if (state === "ERROR") {
					var errors = response.getError();
				}
			});
			$A.enqueueAction(action);
		} else {
			helper.createArticleDetailsComponent(result, component);
		}

		// logic to provide opening article detail from page URL
		helper.setUrlParam(helper.QUERY_PARAMS.DETAILS, 'true');
		helper.checkExponentialSlider(component, result, 2);
	},

	showActionsSettings: function(component, componentName) {
		$A.createComponent(
			componentName, {},
			function(actionsSettingsSection, status, errorMessage) {
				if (status === "SUCCESS") {
					component.set("v.actionsSettingsSection", actionsSettingsSection);
				}
			}
		);
	},
	getSnippetExtentionValue: function(component, className) {
		className += 'Controller';
		className = className.split(':')[1];
		var action = component.get("c.getExtensionSnippet");
		action.setParams({
			'className': className
		});
		var result = component.get("v.result");
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				component.set("v.result.previewBody", response.getReturnValue());
			} else if (state === "ERROR") {
				var errors = response.getError();
			}
		});
		$A.enqueueAction(action);
	},
	prepareComponentName: function(component, articleContent) {
		var componentName;
		if (articleContent !== undefined) {
			var startIndex = articleContent.indexOf('{!ltngext.');
			if (startIndex !== -1) {
				var endIndex = articleContent.indexOf('}');
				var cuttedContent = articleContent.substring(startIndex, endIndex);
				componentName = cuttedContent.substring(10, cuttedContent.length);
			}
		}
		return componentName;
	}
})