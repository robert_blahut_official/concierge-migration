({
	doInit: function(component, event, helper) {
		helper.doInit(component);
		helper.getCurrentUrl(component);
		helper.liveAgentHandler(component);
	},

	bodyStatus: function(component, event, helper) {
		var status = event.getParam("hide");
		if (status === true) {
			component.set('v.articleDetailPopup', null);
			component.set("v.isShowListOfSuggests", true);
			component.set("v.isShowDetailOfSuggests", true);
			component.set("v.showBtnBack", false);
			component.set("v.isMobileMode", false);
		}
		if (window.innerWidth < 1025) {
			component.set("v.isShowListOfSuggests", true);
			component.set("v.isShowDetailOfSuggests", false);
		}
	},

	hideTableValues: function(component, event, helper) {
		var results = component.get("v.searchSuggestions");
		if (results) {
			component.set("v.searchSuggestions", []);
		}
	},

	handleSearchQueryUpdated: function(component, event, helper) {
		var searchQuery = event.getParam("searchQuery");
		var needSuggestion = event.getParam("needSuggestion");
		var requestParams = {
			searchStagingOldRecordStr: JSON.stringify(component.get("v.searchStagingRecord")),
			query: searchQuery,
			prefix: component.get("v.communityPrefix"),
			contactRecord: component.get("v.contactRecord")
		};

		if (needSuggestion) {
			if (searchQuery && searchQuery.length >= 2) {
			helper.startSearchSuggestions(component, requestParams);
			}
		} else {
			helper.getSearchResults(component, requestParams);
		}
	},

	selectSuggested: function(component, event, helper) {
		$A.get("e.c:SuggestionSelected").setParams({
			"suggestion": event.target.textContent
		}).fire();
		$A.util.addClass(component.find('tr_suggest_all_table'), 'hide');
		component.set("v.searchSuggestions", []);
	},
	returnBack: function(component, event, helper) {
		if (window.innerWidth < 1025) {
			component.set("v.isShowListOfSuggests", true);
			component.set("v.isShowDetailOfSuggests", false);
			component.set("v.showBtnBack", false);
		}
	},

	showSnippetClick: function(component, event, helper) {
		var currentArticle = event.target;
		if (!currentArticle.id) {
			currentArticle = helper.getClosestElement(currentArticle, ".resTitle");
		}

		if (currentArticle) {
			var articleNumber = currentArticle.id;
			helper.showSnippet(component, articleNumber);
			articleNumber = Number(articleNumber);
			component.set("v.selectedPosition", articleNumber);
		}

		if (window.innerWidth < 1025) {
			component.set("v.isShowListOfSuggests", false);
			component.set("v.isShowDetailOfSuggests", true);
			component.set("v.showBtnBack", true);
			component.set("v.isMobileMode", true);
		}
		return false;
	},

	runSettingsUpdate: function(component, event, helper) {
		var allResults = component.get("v.searchResultsWithSettings");
		var position = component.get("v.selectedPosition");
		var updatedSettingsInfo = event.getParam('updatedSettingsInfo');
		var newResult = allResults[position];
		for (var i = 0, j = allResults.length; i < j; i++) {
			allResults[i].setting = null;
		}
		if (updatedSettingsInfo) {
			allResults[position].setting = JSON.parse(updatedSettingsInfo);
			component.set("v.searchResultsWithSettings", allResults);
		}
		helper.getArticleSettingsInformation(component, newResult, position, false);
	},


	showCaseLayoutSetup: function(component, event, helper) {
		var articleInfo = component.get("v.result");
		var activeCaseLayoutRecord = component.get("v.activeCaseLayoutRecord");
		$A.createComponent(
			"c:CaseLayoutWrapper", {
				"articleInfo": articleInfo,
				"settingInfo": activeCaseLayoutRecord
			},
			function(caseLayoutPopup, status, errorMessage) {

				if (status === "SUCCESS") {
					component.set("v.caseLayoutPopup", caseLayoutPopup);
				} else {}
			}
		);
	},

	showFullArticleClick: function(component, event, helper) {
		helper.showFullArticle(component);
	},

	applyLikes: function(component, event, helper) {
		var result = component.get("v.result");

		result.rating = event.getParam("rating");
		result.isLike = event.getParam("isLike");

		component.set("v.result", result);
		if (result.rating === 1)
			component.set("v.ratingLabel", $A.get("$Label.c.Person_thought_this_article_was_helpful"));
		else
			component.set("v.ratingLabel", $A.get("$Label.c.People_thought_this_article_was_helpful"));
	},

	logATicket: function(component, event, helper) {
		var result = component.get("v.result");
		var settingCaseLayout = component.get("v.activeCaseLayoutRecord");
		if (settingCaseLayout.cncrg__Field_Set__c) {
			if (result !== undefined)
				result.settingCaseLayout = settingCaseLayout;
		}

		$A.get("e.c:CreateCase").setParams({
			"result": result,
			"query": component.get("v.searchText")
		}).fire();
	},

	sendEmail: function(component, event, helper) {

		var actions = component.get("v.activeActions");
		$A.get("e.c:CreateEmail").setParams({
			"emailAddress": actions.cncrg__Email_Address__c
		}).fire();
	},

	hideResultsSection: function(component, event, helper) {
		$A.util.addClass(component.find('results_div'), 'hidden_div');
	},

	showActionsSetup: function(component, event, helper) {

		var setting = component.get("v.activeActions")
		$A.get("e.c:ShowActionsSetup").setParams({
			"articleInfo": component.get("v.result"),
			"activeActions": component.get("v.activeActions")
		}).fire();
	},

	activateSuggestion: function(component, event, helper) {
		var selectedSuggestion = event.getParam("selectedSuggestion");
		var selectedKeyCode = event.getParam("selectedKeyCode");
		var allSuggestions = component.find('suggestion_row');
		if (allSuggestions && allSuggestions.length > 0) {
			for (var i = 0; i < allSuggestions.length; i++) {
				if (i !== selectedSuggestion)
					$A.util.removeClass(allSuggestions[i].getElement(), 'activated_tr_suggest');
				else
					$A.util.addClass(allSuggestions[i].getElement(), 'activated_tr_suggest');
			}
			var block = component.find('tr_suggest_all_table').getElement();
			if (selectedSuggestion > 4) {
				block.scrollTop = block.scrollHeight;
			} else {
				block.scrollTop = 0;
			}
		} else {
			$A.get("e.c:SendSuggestionsToInput").setParams({
				"listOfSuggestions": undefined
			}).fire();
		}
	},

	nextPage: function(component, event, helper) {
		var pageNumber = component.get("v.pageNumber");
		var lastPageNumber = component.get("v.lastPageNumber");
		var recordsOnPage = component.get("v.recordsOnOnePage");

		if (lastPageNumber - 1 > pageNumber) {
			helper.updateListOfPages(component, pageNumber + 1);
			component.set("v.pageNumber", pageNumber + 1);
			var allResults = component.get("v.searchResults");
			var newResult = allResults[(pageNumber + 1) * recordsOnPage];
			component.set("v.isActionsExists", false);
			component.set("v.selectedElement", {});
			component.set("v.selectedPosition", (pageNumber + 1) * recordsOnPage);
			helper.getArticleSettingsInformation(component, newResult, (pageNumber + 1) * recordsOnPage);
		}
	},

	previousPage: function(component, event, helper) {
		var pageNumber = component.get("v.pageNumber");
		var recordsOnPage = component.get("v.recordsOnOnePage");
		if (pageNumber > 0) {
			helper.updateListOfPages(component, pageNumber - 1);
			component.set("v.pageNumber", pageNumber - 1);
			var allResults = component.get("v.searchResults");
			var newResult = allResults[(pageNumber - 1) * recordsOnPage];
			component.set("v.isActionsExists", false);
			component.set("v.selectedElement", {});
			component.set("v.selectedPosition", (pageNumber - 1) * recordsOnPage);
			helper.getArticleSettingsInformation(component, newResult, (pageNumber - 1) * recordsOnPage);
		}
	},

	goToSelectedPage: function(component, event, helper) {
		var selectedPage = event.getParam('pageNumber');
		var pageNumber = component.get("v.pageNumber");
		var lastPageNumber = component.get("v.lastPageNumber");
		var recordsOnPage = component.get("v.recordsOnOnePage");

		if (pageNumber !== selectedPage) {
			helper.updateListOfPages(component, selectedPage);
			component.set("v.pageNumber", selectedPage);
			var allResults = component.get("v.searchResults");
			var newResult = allResults[selectedPage * recordsOnPage];
			component.set("v.isActionsExists", false);
			component.set("v.selectedElement", {});
			component.set("v.selectedPosition", selectedPage * recordsOnPage);
			helper.getArticleSettingsInformation(component, newResult, selectedPage * recordsOnPage);
		}
	},
	changeSearchStaging: function(component, event, helper) {
		var score = event.getParam("score");
		if (score) {
			helper.checkExponentialSlider(component, component.get("v.result"), score);
		} else {
			helper.addFeedBack(component, component.get("v.result").knowledgeArticleId, event.getParam("reasons"), event.getParam("comment"));
		}
	},

	logTicket: function(component, event, helper) {
		var itHelp = component.get("v.itHelpUrl");
		var componentName = 'c' + ':GetHelpComponent';

		if (itHelp !== '') {
			$A.createComponent(
				componentName, {
					"itHelpUrl": component.get("v.itHelpUrl"),
					"searchText": component.get("v.searchText"),
					"itHelpPopUp": component.getReference("v.getHelpPopup")
				},
				function(element, status, errorMessage) {

					if (status === "SUCCESS") {
						component.set("v.getHelpPopup", element);
					}
				}
			);
		} else {
			$A.get("e.c:CreateCase").setParams({
				"result": undefined,
				"query": component.get("v.searchText")
			}).fire();
		}

		//$A.get("e.c:CreateCase").setParams({"result" : undefined, "query" : component.get("v.searchText")}).fire();
		helper.deleteSchwartzc(component);
	},
	handleOpenFlow: function(component, event, helper) {
		event.stopPropagation();
		var flowName = event.getParam("flowName");
		if (flowName) {
			component.find('flowCmp').doOpenFlow(flowName);
		}
		return false;
	}
})