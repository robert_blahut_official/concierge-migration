({
    submitFeedBack : function(component, event, helper) {
        component.find("suggestions").set("v.placeholder",$A.get("$Label.c.Enter_Text_Placeholder"));

        var suggestions = component.find("suggestions").get("v.value");
              
        // if(!(suggestions === undefined || suggestions  === null ||
        // suggestions.trim() === null || suggestions.trim() === undefined || suggestions.trim() === "")){
            var suggestionEvent  = component.getEvent("sendSuggestionMessage");
            suggestionEvent.setParams({ "noteMessage": suggestions });
            suggestionEvent.fire();
                       
            var textMessage = component.find("textMessage");
           
            $A.util.removeClass(textMessage, "textMessageHidden");
            window.setTimeout(
            $A.getCallback(function() {
                if (component.isValid()) {
            		$A.util.addClass(textMessage, "textMessageHidden");
                    var setPreviousWindow  = component.getEvent("sendNextWindow");
            		helper.setActiveWindow(setPreviousWindow, "6");
                    component.find("suggestions").set("v.value", "");
                    
                    var reloadStarRating = $A.get("e.c:ReloadFeedBack");
        			reloadStarRating.fire();

                }
            }), 2000
                
        ); 
            
        // }else{
        //     component.find("suggestions").set("v.errors", [{message: $A.get("$Label.c.Blank_Suggestions_Message")}]);
        //     $A.util.addClass(component.find("suggestions"), "errorBorder");
            /*window.setTimeout(
                $A.getCallback(function() {
                    if (component.isValid()) {
                		component.find("suggestions").set("v.errors", [{message:""}]);
                    }
                }), 1000
            );  */  
        // }
    },
    hideErrorBorder : function(component, event, helper) {
        $A.util.removeClass(component.find("suggestions"), "errorBorder");
        component.find("suggestions").set("v.errors", []);
    },  
    previousWindow : function(component, event, helper) {
        component.find("suggestions").set("v.placeholder", $A.get("$Label.c.Enter_Text_Placeholder"));

        var setPreviousWindow  = component.getEvent("sendNextWindow");
        var previousWindowNumber = component.get("v.numberOfPreviousWindow");
        
        if(previousWindowNumber !== 1){
        	helper.setActiveWindow(setPreviousWindow, "3");
        }else{
        	helper.setActiveWindow(setPreviousWindow, "8");
        }
    },
    
    removeDefault : function(component, event, helper) {      
        component.find("suggestions").set("v.placeholder","");
    },
    
    hideFeedSuggestion : function(cmp, event, helper) {     
        cmp.find("suggestions").set("v.value", "");
        cmp.find("suggestions").set("v.placeholder", $A.get("$Label.c.Enter_Text_Placeholder"));

        var setNextWindow  = cmp.getEvent("sendNextWindow");
        helper.setActiveWindow(setNextWindow, "6");
        var reloadStarRating = $A.get("e.c:ReloadFeedBack");
        reloadStarRating.fire();

    },
    reloadSuggestionRating : function(cmp, event, helper) {
        cmp.find("suggestions").set("v.value", "");
        cmp.find("suggestions").set("v.placeholder", $A.get("$Label.c.Enter_Text_Placeholder"));

        
    }
})