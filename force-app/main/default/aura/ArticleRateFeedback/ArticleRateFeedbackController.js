({
    doInit : function(component, event, helper){
        helper.doInit(component);
    },

    saveAndClose : function(component, event, helper){
        var location = component.get('v.location');
        if(location === 'ArticleDetails') {
            helper.saveAndClose(component, event, helper);
        } else {
            helper.createStaging(component);
        }
    },

	closePopup : function(component, event, helper){
		var vx = component.get("v.closePopup");
        $A.enqueueAction(vx);
    },

    getStatus : function(component, event, helper){
        helper.getStatus(component, event, helper);
    },

    checkFeedback : function(component, event, helper){
        helper.checkFeedback(component, event, helper);
    }
})