({
    doInit : function(component){
        var action = component.get("c.getNewArticleFeedbackWrapper");
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                var newArticleFeedback = response.getReturnValue();
                component.set("v.articleFeedback", newArticleFeedback);
            } else {
            }
        });
        $A.enqueueAction(action);
    },

    saveAndClose : function(component, event, helper){        
        var feedbackRecord = component.get('v.articleFeedback');        
		var feedbackItemsList = feedbackRecord.articleFeedbackWrapperItemList;
		var selectedItemsList = [];      
		var isOtherOption = component.get('v.isOtherOption');  
		for(var i = 0; i < feedbackItemsList.length; i++){
			if(feedbackItemsList[i].isChecked){
				selectedItemsList.push(feedbackItemsList[i].articleRateItemName);                
			}
		}
		if(isOtherOption) {
			selectedItemsList.push('Other');
		}
		var starRatingValue  = component.getEvent("ChangeSearchStaging");
		starRatingValue.setParams(
			{
				comment : feedbackRecord.comment, 
				reasons : selectedItemsList
			}
		).fire();
        this.close(component);
    },

    checkFeedback : function(component, event, helper){
        component.set('v.isFeedbackChecked', false);
        var feedBackItems = component.get('v.articleFeedback.articleFeedbackWrapperItemList');        
        for(var i = 0; i < feedBackItems.length; i++){
            if(feedBackItems[i].isChecked){
                component.set('v.isFeedbackChecked', true);
            }
        }
    },

    createStaging: function (component) {
        var self = this;
        var action = component.get("c.createSearchStaging");
        var params = component.get('v.articleFeedback');
        var article = component.get('v.articleDetails');
        var isOtherOption = component.get('v.isOtherOption');
        action.setParams(
            {
                'wrappString': JSON.stringify(params),
                'articleId' : article.recordId,
                'articleTitle': article.title,
                'isOther': isOtherOption
            }
        );
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                self.close(component);
            } else {
            }
        });
        $A.enqueueAction(action);
    },

    close: function (component) {
        $A.get("e.c:ShowSuccessMessage").setParams(
            {
                messageText : $A.get("$Label.c.Thank_you_for_your_Feedback")
            }
        ).fire();
        $A.enqueueAction(component.get("v.downVote"));
        var vx = component.get("v.closePopup");
        $A.enqueueAction(vx);
    }

})