({
	relatedMap: {},
	relatedRevertMap: {},
	breadcrumbsOptions: [],

	changeIframeTags: function(component, allIframe) {
		var body = component.find("body").getElement();
		var iframePlaces = body.getElementsByClassName("video-container");
		var iframesArray = [];
		for(var i=0, j=allIframe.length; i<j; i++) {
			var para = document.createElement("iframe");
			var regex = /<iframe.*?src="(.*?)"/;
			var regexWidth = /<iframe.*?width="(.*?)"/;
			var regexHeight = /<iframe.*?height="(.*?)"/;
			var src = regex.exec(allIframe[i])[1];
			var width = regexWidth.exec(allIframe[i])[1];
			var height = regexHeight.exec(allIframe[i])[1];
			para.src = src;
			para.width = width;
			para.height = height;
			iframePlaces[i].appendChild(para);
		}
	},
	parseLinkTags: function(component, linksTags) {
		for(var i=0, j=linksTags.length; i<j; i++) {
			linksTags[i].removeAttribute('download');
			if(linksTags[i].href.indexOf('/articles/') !== -1) {
				linksTags[i].addEventListener("click", $A.getCallback(function(event) {
					$A.get("e.c:RelatedArticleClick").setParams({
						"href": event.target.target,
						"component" : component.get("v.parentComponent"),
						"outerText": event.target.outerText
					}).fire();
				}));
				linksTags[i].target = linksTags[i].href.substring(linksTags[i].href.indexOf('articles/'), (linksTags[i].href.indexOf('?') !== -1 ? linksTags[i].href.indexOf('?') : linksTags[i].href.length));
				linksTags[i].removeAttribute("href");
			}
		}
	},
	rateItem : function(component, isPlus, showMessageBoolean) {
		var result = component.get("v.result");
		if (isPlus && (result.isLike === undefined || !result.isLike)) {
			component.set('v.helpfulButtonsIsDisabled', true);
			result.rating++;
			result.isLike = true;
			this.rateToServer(component, result, showMessageBoolean);
		} else if (!isPlus && (result.isLike === undefined || result.isLike)) {
			component.set('v.helpfulButtonsIsDisabled', true);
			result.rating--;
			result.isLike = false;
			this.rateToServer(component, result, showMessageBoolean);
		}
	},

	rateToServer: function (component, result, showMessageBoolean) {
		if(showMessageBoolean) {
			var popup = component.find('thankMessage');
			component.set("v.isVoteMessage" , true);
			window.setTimeout($A.getCallback( function(){
				component.set("v.isVoteMessage" , false);
			}),	1500);
		}

		var action = component.get("c.rateItem");
		action.setParams({
			item : JSON.stringify(result)
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				component.set('v.helpfulButtonsIsDisabled', false);
				component.set("v.result",result);
				if(result.rating === 1)
					component.set("v.ratingLabel", $A.get("$Label.c.Person_thought_this_article_was_helpful"));
				else
					component.set("v.ratingLabel", $A.get("$Label.c.People_thought_this_article_was_helpful"));
			} else if (state === "ERROR") {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
					}
				} else {
				}
			}
		});
		$A.enqueueAction(action);
	},

	showErrorMessage: function(component, message) {
		var messageCmp = component.find('exceptionMessage');
		component.set('v.errorMessage', message);
		$A.util.removeClass(messageCmp, 'hidden_div');
		window.setTimeout($A.getCallback(function() {
			if (component.isValid()) {
				$A.util.addClass(messageCmp, 'hidden_div');
			}
		}),	2000);
	},
	replaceAll : function(target, search, replacement) {
		return target.replace(new RegExp(search, 'g'), replacement);
	},
	openPDF: function (component) {
		var path = window.location.pathname + '';
		path = (path.includes('/s/')) ? path.substring(0, path.indexOf('/s/')) : '';
		var url = path + '/apex/cncrg__LightningPDFGenerator?Id=' + component.get("v.result").recordId + '&Type=' + component.get("v.result").type;
		window.open(url);
	},
	getInitialSettings: function (component) {
		var action = component.get("c.getInitialSettings");
		action.setStorable();
		action.setCallback(this, function (response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var res = response.getReturnValue();
				component.set("v.alwaysDiaplayLogTicket", res[0]);
				component.set("v.allowUsersToPrintArticlesInPDF", res[1]);
			}
		});
		$A.enqueueAction(action);
	},
	addFeedback: function (component) {
		component.set("v.showRatePopup", true);
	},

	initializeBreadcrumbsSettings: function(component, helper) {
		helper.relatedMap = new Map();
		helper.relatedRevertMap = new Map();
		helper.breadcrumbsOptions = component.get("v.breadcrumbsOptions");

		// the last article in the list is Current Related article
		var currentArticleLabel = helper.breadcrumbsOptions[helper.breadcrumbsOptions.length - 1].label;
		// so the prev article will be BC[BC.length - 2]
		var hasPreviousRelatedArticle = helper.breadcrumbsOptions.length >= 2;
		var previousRelatedLabel = hasPreviousRelatedArticle 
			? helper.breadcrumbsOptions[helper.breadcrumbsOptions.length-2].label
			: '';
		// if the Related Article opened from Favorites or from Search result.
		// we have return user back to the start point
		if (helper.breadcrumbsOptions.length === 1 
				&& component.get("v.parentArticle")
				&& component.get("v.parentArticle").title) {
			previousRelatedLabel = component.get("v.parentArticle").title;
			hasPreviousRelatedArticle = true;
		}
		// put the flag to show the breadcrumbs
		component.set("v.isVisible", hasPreviousRelatedArticle);

		var breadcrumbsArray = [];
		helper.breadcrumbsOptions.forEach(function(item) {
			if(!helper.relatedMap.has(item.label)) {
				helper.relatedMap.set(item.label, item.value);
				helper.relatedRevertMap.set(item.value, item.label);
				if(item.label !== previousRelatedLabel && item.label !== currentArticleLabel) {
					breadcrumbsArray.push(item);
				}
			}
		});

		component.set("v.previousRelatedLabel", previousRelatedLabel);
		component.set("v.breadcrumbsOptionsSet", breadcrumbsArray);
	}
})