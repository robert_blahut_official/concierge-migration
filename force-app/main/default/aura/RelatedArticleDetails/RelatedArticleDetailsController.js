({
	doInit : function(component, event, helper) {
		helper.getInitialSettings(component);
		helper.initializeBreadcrumbsSettings(component, helper);

		var htmlTitle = component.get("v.result.body");
		if(htmlTitle.indexOf('class="section_card"') !== -1){
			component.set("v.isBusinessCards", false);
		}

		var newResult = component.get("v.result");
		if(newResult.rating === 1) {
			component.set("v.ratingLabel", $A.get("$Label.c.Person_thought_this_article_was_helpful"));
		}
		else {
			component.set("v.ratingLabel", $A.get("$Label.c.People_thought_this_article_was_helpful"));		
		}
	},
	goToPreviousRelatedArticle : function(component, event, helper) {
		var eventParams = {href: null};
		var previousRelatedLabel = component.get("v.previousRelatedLabel");

		if (helper.breadcrumbsOptions.length > 2) {
			helper.breadcrumbsOptions.splice(helper.breadcrumbsOptions.length -2, 2);
		} else {
			helper.breadcrumbsOptions.splice(0, helper.breadcrumbsOptions.length);
		}

		if (helper.breadcrumbsOptions.length >= 0) {
			eventParams.href = helper.relatedMap.get(previousRelatedLabel);
			eventParams.component = component.get("v.parentComponent");
		}

		component.set("v.breadcrumbsOptions", helper.breadcrumbsOptions);
		$A.get("e.c:RelatedArticleClick").setParams(eventParams).fire();
		setTimeout($A.getCallback(function() {
			component.destroy();
		}), 500);
	},
	handleChange : function(component, event, helper) {
		var selectedOptionValue = event.getParam("value");
		$A.get("e.c:RelatedArticleClick").setParams({
			"href": event.getParam("value"),
			"component" : component.get("v.parentComponent"),
			"outerText": helper.relatedRevertMap.get(event.getParam("value"))
		}).fire();
	},
	hideDetails : function(component, event, helper) {
		helper.breadcrumbsOptions.splice(0, helper.breadcrumbsOptions.length);
		component.set("v.breadcrumbsOptions", helper.breadcrumbsOptions);
		$A.get("e.c:RelatedArticleClick").setParams({
			href : null
		}).fire();
	},
	toggleFavorite : function(component, event, helper) {
		var action = component.get("c.toggleFavoriteArticle");
		var result = component.get("v.result");
		action.setParams({
			recordId : result["knowledgeArticleId"],
			isFavorite : result["isFavorite"]
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				result["isFavorite"] = !result["isFavorite"];
				component.set("v.result",result);
			} else if (state === "ERROR") {
				var message = $A.get("$Label.c.Unknown_error");
				var errors = response.getError();
			
				if (errors) {
					if (errors[0] && errors[0].message) {
						message = errors[0].message;
					} 
					else if (errors.message) {
						message = errors.message;
					}
				}
				helper.showErrorMessage(component, message);
			}
		});
		$A.enqueueAction(action);
	},
	yes : function(component, event, helper) {
		helper.rateItem(component,true, true);
	},

	no : function(component, event, helper) {
		helper.rateItem(component,false, true);
	},

	noFromFeedback : function(component, event, helper) {
		helper.rateItem(component,false, false);
	},
	logTicket : function(component, event, helper) {
		$A.createComponent(
			"c:CaseCreate",
			{
				"result" : component.get("v.result"),
				"isCommunity" : component.get("v.communityLoading"),
				"isRelatedArticle" : true
			},
			function(dynamicBlock, status, errorMessage) {
				if (status === "SUCCESS") {
					component.set("v.dynamicBlock",dynamicBlock);
					//var relatedArticle = component.find("relatedArticleBlock");
					//$A.util.addClass(relatedArticle, 'display_false');
				}
				else if (status === "INCOMPLETE") {
				}
				else if (status === "ERROR") {
				}
			}
		);		
	},
	downloadDocument: function (component, event, helper) {
		helper.openPDF(component);
	},
	more : function(component, event, helper){
		helper.addFeedback(component);
	},
	closePopup : function(component, event, helper){
		component.set("v.showRatePopup", false);
	}
})