({
	afterRender: function(component, helper) {
		this.superAfterRender();
		var htmlBody = component.get("v.result.body");
		if (htmlBody) {
			var startIndex = htmlBody.indexOf('{!ltngext.');
			if (startIndex !== -1) {

				var endIndex = htmlBody.indexOf('}');
				htmlBody = htmlBody.substring(startIndex, endIndex);
				var componentName = htmlBody.substring(10, htmlBody.length);
				component.set("v.externalComponentName", componentName);
				$A.createComponent(
					componentName, {
						"knowledgeArticleId": component.get("v.result.knowledgeArticleId")
					},
					function(extentionBlock, status, errorMessage) {
						if (status === "SUCCESS") {
							component.set("v.body", extentionBlock);
						}
					}
				);
			} else {
				htmlBody = helper.replaceAll(helper.replaceAll(htmlBody, '<iframe', '<div class="video-container"><iframe'), '</iframe>', '</iframe></div>');

				var body = component.find("body").getElement();
				var regexIframe = /(?:<iframe[^>]*)(?:(?:\/>)|(?:>.*?<\/iframe>))/g;
				body.innerHTML = htmlBody;
				if (htmlBody) {
					var allIframe = htmlBody.match(regexIframe);
					if (allIframe && allIframe.length > 0 && body.innerHTML.match(regexIframe) == null) {
						helper.changeIframeTags(component, allIframe);
					}
				}
				var linksTags = body.getElementsByTagName('a');
				if (linksTags && linksTags.length > 0) {
					helper.parseLinkTags(component, linksTags);
				}
				helper.scrollToAnchor();
			}
		}
	}
})