({
	getInitialData : function (component) {
		var action = component.get("c.getInitialData");
		action.setParams({
			'knowledgeRecord' : component.get("v.knowledgeRecord")
		});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
				component.set("v.isUserHasPermissionRunFlow", result[0]);
				component.set("v.actionsOrderList", result[1]);
				component.set("v.callActionComponentName", result[2]);
				this.renderCallComponent(component, result[2]);
                this.createExternalComponent(component);
            } else if (state === "ERROR") {
                var errors = response.getError();
            }
		});
        $A.enqueueAction(action);
	},
	replaceAll: function (lineStr, search, replacement) {
		return lineStr.replace(new RegExp(search, 'g'), replacement);
	},
	renderCallComponent : function (component, callActionComponentName) {
		$A.createComponent(
			callActionComponentName,
			{
				"activeActions" : component.getReference("v.activeActions"),
			},
			function(dynamicCallBlock, status, errorMessage) {
				if (status === "SUCCESS") {
					component.set("v.dynamicCallBlock", dynamicCallBlock);
				}
			}
		);
	},
	createExternalComponent: function(component) {
		var activeActions = component.get('v.activeActions');
		if (activeActions){
			if((typeof activeActions)!=='string' && activeActions!=='undefined' && activeActions.hasOwnProperty('cncrg__URL__c')){		 	
				var visitPageUrl = activeActions.cncrg__URL__c.replace(/((^\w+:|^)\/\/)/, '');
				//visitPageUrl = secureFilters.html(visitPageUrl);   //sanitizes output for HTML element and attribute contexts using entity-encoding.
				//visitPageUrl = secureFilters.uri(visitPageUrl);   //Sanitizes output in URI component contexts by using percent-encoding.
				visitPageUrl = 'https:\\\\' + visitPageUrl;
				component.set('v.filteredVisitPageUrl',visitPageUrl);		
			}
		}

		component.set('v.isMoreThanOneActiveAction', false);

        if (activeActions &&
			(
			// flow action with access
			(activeActions.cncrg__Flow_Name__c && component.get("v.isUserHasPermissionRunFlow")) ||
			activeActions.cncrg__Support_Name__c || activeActions.cncrg__Support_Number__c ||
			activeActions.cncrg__URL_Label__c || activeActions.cncrg__URL__c ||
			activeActions.cncrg__Log_a_Ticket__c === true || activeActions.cncrg__Live_Chat__c || 
			activeActions.cncrg__Email__c || activeActions.Skype_For_Business__c)) {

            component.set('v.isMoreThanOneActiveAction', true);
			if (component.get("v.appName")) {
				var appName = component.get("v.appName");
				component.set("v.suggestLabel", appName + ' '+ $A.get("$Label.c.Suggests"));
			}
			else {
				component.set("v.suggestLabel",  '');
			}
		}

		/*var externalComponentName = component.get("v.externalComponentName");		
		var externalComponentCreated = component.get("v.externalComponentCreated");
		if (!externalComponentCreated && externalComponentName && externalComponentName.trim() !== '') {
			window.setTimeout(
				$A.getCallback(function() {
					$A.createComponent(
						externalComponentName,
						{},
						function(newAction, status, errorMessage) {
							if (status === "SUCCESS") {
								var external_action = component.find("external_action");
								external_action.set("v.body",newAction);
								$A.util.removeClass(component.find('external_action'), 'display_false');
								component.set("v.externalComponentCreated",true);

							}
							else if (status === "INCOMPLETE") {
							}
							else if (status === "ERROR") {
							}
						}
					);
				}), 2500
			);
		}*/
	}
})