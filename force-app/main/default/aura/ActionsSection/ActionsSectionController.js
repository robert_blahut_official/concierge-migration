({
	doInit : function (component, event, helper) {
		helper.getInitialData(component);
	},
	createExternalComponent : function(component, event, helper) {
        helper.createExternalComponent(component);
	},
	logATicket: function (component, event, helper) {
		if(component.get("v.isRelatedArticle")) {
			$A.get("e.c:CallLogTicketRelatedArticle").fire();
		}
		else {
			var starRatingValue  = component.getEvent("ChangeSearchStaging");
			starRatingValue.setParams({ "score": 4 });
			starRatingValue.fire();
			$A.get("e.c:CallLogATicket").fire();
		}
	},
	sendEmail : function (component, event, helper) {
		$A.get("e.c:SendEmail").fire();
	},
	clickToUrl: function (component, event, helper) {
		if(!component.get("v.isRelatedArticle")) {
			var starRatingValue  = component.getEvent("ChangeSearchStaging");
			starRatingValue.setParams({ "score": 16 });
			starRatingValue.fire();
		}
	},
	visitTheSite: function (component, event, helper) {
		var address = component.get('v.activeActions.cncrg__URL__c');		
		$A.get("e.force:navigateToURL").setParams({
			'url' : address
		}).fire();
	},
	closeWindow: function (component, event, helper) {
		component.set("v.showPopUp", false);
	},
    activateLiveChat: function (component) {
        $A.get("e.c:LiveAgentCustomEvent").fire();
        var starRatingValue  = component.getEvent("ChangeSearchStaging");
        starRatingValue.setParams({ "score": 8 });
        starRatingValue.fire();
    },
	openFlow : function (component, event, helper) {
		var flowName = helper.replaceAll(component.get("v.activeActions.cncrg__Flow_Name__c"), ' ', '_');
		if (flowName && flowName.length > 0) {
			component.getEvent('FlowOpenEvent').setParams({
				"flowName": flowName
			}).fire();
		}
	}
})