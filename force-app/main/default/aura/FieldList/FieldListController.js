({
	deleteSetting : function(component, event, helper) {
		var selectedSetting = event.target.id;		
        var fieldsList = component.get("v.fieldsList");
        fieldsList[selectedSetting].deleted=true;
		component.set("v.fieldsList", fieldsList);
	},

	addNewRecord : function (component, event, helper) {
        var fieldsList = component.get("v.fieldsList");
        fieldsList.push({'usersField' : component.get("v.allAvailableFields")[0], 'historyField' : component.get("v.allAvailableHistoryFields")[0], 'deleted' : false});
        component.set('v.fieldsList', fieldsList);
	},
})