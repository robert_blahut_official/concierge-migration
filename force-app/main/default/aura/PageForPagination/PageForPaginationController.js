({
	goToSelectedPage : function (component, event, helper) {
		var selectedPageText = event.target.id;
		if (selectedPageText !== 'empty') {
			$A.get("e.c:GoToSelectedPage").setParams({
				"pageNumber" : parseInt(selectedPageText, 10)
			}).fire();
		}
	}
})