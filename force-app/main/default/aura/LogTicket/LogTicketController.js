({
	doInit: function(component, event, helper) {
		if (component.get("v.isCommunity") === true) {
			$A.util.removeClass(component.find('log-ticket'), 'total_div');
			$A.util.removeClass(component.find('no_drop'), 'total_div');
		}
		var submitBtn = component.find("submit-btn");
		submitBtn.set('v.disabled', false);
		$A.util.addClass(submitBtn, "enabled-btn");
		$A.util.removeClass(submitBtn, "disabled-btn");
		helper.loadEmptyCase(component, false);
		helper.loadCustomCaseSettings(component);
	},

	closePopUp: function(component, event, helper) {
		helper.closePopup(component);
	},

	loadFile: function(component, event, helper) {
		var validatedFilesArray = [];
		var fileInput = component.get('v.fileList');
		if (!fileInput || !fileInput.length) {
			return;
		}
		var totalFilesSize = 0;
		for (var i = 0, j = fileInput[0].length; i < j; i++) {
			var file = fileInput[0][i];
			totalFilesSize += file.size;
			var message = helper.checkFilesSize(component, totalFilesSize, file);
			if (message != null) {
				helper.displayErrorMessage(component, message);
			} else {
				validatedFilesArray.push(fileInput[0][i]);
			}
		}
		component.set('v.fileList[0]', validatedFilesArray);
	},

	implementNewPriority: function(component, event, helper) {
		var newCase = component.get("v.newCase");
		var activeCase = event.target;
		var allCases = component.find('all_cases');
		newCase.Priority = event.target.id;
		component.set("v.newCase", newCase);
		for (var i = 0; i < allCases.length; i++) {
			if (allCases[i].getElement() !== null && allCases[i].getElement())
				$A.util.removeClass(allCases[i].getElement(), 'active');
		}
		$A.util.addClass(activeCase, 'active');
	},

	implementNewRecordType: function(component, event, helper) {
		var newCase = component.get("v.newCase");
		var activeRecordType = event.target;
		var allRecordTypes = component.find('all_record_types');

		newCase.Type = event.target.id;
		component.set("v.newCase", newCase);
		for (var i = 0; i < allRecordTypes.length; i++) {
			if (allRecordTypes[i].getElement() !== null && allRecordTypes[i].getElement())
				$A.util.removeClass(allRecordTypes[i].getElement(), 'active');
		}
		$A.util.addClass(activeRecordType, 'active');
	},

	implementSubject: function(component, event, helper) {
		var newCase = component.get("v.newCase");
		newCase.Subject = event.target.value;
		component.set("v.newCase", newCase);
	},

	createNewCase: function(component, event, helper) {
		var caseObject = component.get("v.newCase");
		var submitBtn = component.find("submit-btn");
		var filesList = component.get('v.fileList');
		if (filesList && filesList[0].length > 0) {
			var totalFilesSize = 0;
			for (var i = 0, j = filesList[0].length; i < j; i++) {
				var file = filesList[0][i];
				totalFilesSize += file.size;
				var message = helper.checkFilesSize(component, totalFilesSize, file);
				if (message != null) {
					helper.displayErrorMessage(component, message);
					return;
				}
			}
		}
		helper.showSpinner(component.getSuper());

		var needAddInformation = helper.checkRequaredField(component);
		if (needAddInformation.length > 0) {
			var errorMessage = needAddInformation.join();
			component.set('v.errorMessage', errorMessage);
			helper.hideSpinner(component.getSuper());
			$A.util.removeClass(component.find("settings_missing"), "hidden_div");
			window.setTimeout(
				$A.getCallback(function() {
					if (component.isValid()) {
						$A.util.addClass(component.find("settings_missing"), "hidden_div");
					}
				}), 3000
			);
		} else {
			submitBtn.set('v.disabled', true);
			var emailReciever = component.get("v.emailReciever");
			if (emailReciever || component.get("v.newCase").HRS_Category__c === 'Learning') {
				var requestArray = {};
				requestArray.emailReciever = emailReciever;

				var fileInput = component.get('v.fileList');
				if (!fileInput || !fileInput.length) {
					helper.insertNewCase(component, caseObject, requestArray);
				} else {
					requestArray.files = [];
					for (var i = 0, j = fileInput[0].length; i < j; i++) {
						helper.processedFiles(component, requestArray, fileInput[0][i], fileInput[0].length, caseObject);
					}
				}
			} else {
				helper.insertNewCase(component, caseObject, null);
			}
		}
	},
	handleRemove: function(component, event, helper) {
		var pillNumber = event.getSource().get('v.name');
		var files = component.get('v.fileList[0]');
		var newFiles = [];
		for (var i = 0; i < files.length; i++) {
			if (pillNumber !== i) {
				newFiles.push(files[i]);
			}
		}

		component.set('v.fileList[0]', newFiles);
	},
	toggleCustomCaseForm: function(component, event, helper) {
		event.preventDefault();
		component.set("v.showCustomCaseForm", !component.get('v.showCustomCaseForm'));
		if (component.get('v.showCustomCaseForm')) {
			helper.loadEmptyCase(component, true);
		} else {
			helper.loadEmptyCase(component, false);
		}
		return false;
	}
})