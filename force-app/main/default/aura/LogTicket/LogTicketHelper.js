({
	insertNewCase: function(component, caseObject, requestArray) {
		var self = this;
		var action = component.get("c.createNewCaseApex");
		action.setParams({
			newCase: JSON.stringify(caseObject),
			requestArrayStr: requestArray ? JSON.stringify(requestArray) : null
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var responseList = response.getReturnValue();
				var caseId = responseList[0];
				component.set("v.newCaseId", caseId);
				if (caseId) {
					self.savePhotoData(component);
				}
				else { 
					$A.util.addClass(component.find('log-ticket'), 'hidden_div');
					var submitMessage = $A.get("$Label.c.Case_Submit_Message");
					$A.get("e.c:ShowSuccessMessage").setParams({
						'messageText' : responseList[1] ? responseList[1] : submitMessage,
						'isInfo' : false,
						'isError' : responseList[2] ? responseList[2] : false,
						'isRelatedArticle' : component.get("v.isRelatedArticle")
					}).fire();
					self.hideSpinner(component.getSuper());
					return;
				}
			} else if (state === "ERROR") {
				var errors = response.getError();
				var errorMessage;
				if (errors[0] && errors[0].pageErrors && errors[0].pageErrors[0] && errors[0].pageErrors[0].message) {
					errorMessage = errors[0].pageErrors[0].message;
				} else if (errors[0] && errors[0].fieldErrors) {
					errorMessage = '';
					var keys = (function(o) {
						var ks = [];
						for (var k in o) ks.push(k);
						return ks;
					});
					var errorKeysArray = keys(errors[0].fieldErrors);
					for (var i = 0, j = errorKeysArray.length; i < j; i++) {
						for (var k = 0, l = errors[0].fieldErrors[errorKeysArray[i]].length; k < l; k++) {
							errorMessage += errors[0].fieldErrors[errorKeysArray[i]][k].message + '<br/>';
						}
					}
					$A.get("e.c:MarkFieldErrors").setParams({
						'fieldsWithErrors': errorKeysArray
					}).fire();
				}
				self.displayErrorMessage(component, errorMessage);
				self.hideSpinner(component.getSuper());
			}

		});
		$A.enqueueAction(action);
	},
	closePopup: function(component) {
		if (!component.get("v.result") 
			&& !component.get("v.searchText")
			&& component.get('v.isCommunity')) {

			return window.history.go(-1);
		} else {
			$A.util.addClass(component.find('log-ticket'), 'hidden_div');
			this.hideSpinner(component.getSuper());	
		}
	},
	loadEmptyCase: function(component, customCase) {
		var self = this;
		var action = component.get("c.loadEmptyCase");
		var result = component.get("v.result");
		var articleId = "";
		var articleType = "";
		var masterVersionId;
		var settingCaseLayout;
		if (result) {
			settingCaseLayout = result.settingCaseLayout;
			articleId = result.recordId;
			articleType = result.type
			masterVersionId = result.masterVersionId;
		}
		action.setParams({
			articleId: articleId,
			articleType: articleType,
			settingCaseLayout: settingCaseLayout,
			masterVersionId: masterVersionId,
			customObj: customCase
		});

		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var caseWrapper = response.getReturnValue()[0];
				component.set("v.emailReciever", caseWrapper.emailReciever);
				var fields = response.getReturnValue()[1];
				component.set('v.fields', fields);
				var newCase = caseWrapper.newCase;
				if (!customCase) {
					newCase.Subject = component.get("v.searchText");
					component.set("v.newCase", newCase);

					self.createComponent(
						component,
						fields,
						component.getReference("v.newCase"),
						'Case',
						caseWrapper.caseLayoutFieldSet
					);
				} else {
					var customCaseSettings = component.get('v.customCaseSettings');
					if (customCaseSettings.Subject) {
						newCase[customCaseSettings.Subject] = component.get("v.searchText");
					}
					component.set("v.newCase", newCase);

					self.createComponent(
						component,
						fields,
						component.getReference("v.newCase"),
						customCaseSettings.ObjectName,
						caseWrapper.caseLayoutFieldSet
					);
				}
			}
		});
		$A.enqueueAction(action);
	},
	loadCustomCaseSettings: function(component) {
		var self = this;
		var action = component.get("c.getCustomCaseSettings");
		action.setCallback(self, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var wrapper = response.getReturnValue();
				if (wrapper.ObjectName) {
					component.set('v.customCaseSettings', wrapper);
				}
			}
		});
		$A.enqueueAction(action);
	},
	createComponent: function(component, fields, record, typeName, fsName) {
		$A.createComponent(
			"c:FieldSetForm", {
				"fields": fields,
				"record": record,
				"typeName": typeName,
				"fsName": fsName
			},
			function(dynamicCaseBlock, status, errorMessage) {
				if (status === "SUCCESS") {
					component.set("v.dynamicCaseBlock", dynamicCaseBlock);
					$A.util.removeClass(component.find('log-ticket'), 'hidden_div');
				}
			}
		);
	},

	MAX_FILE_SIZE: 4194304,
	MAX_CHUNK_SIZE: 2621440,
	/* 6 000 000 * 3/4 to account for base64 */
	CHUNK_SIZE: 3670016,
	/* Use a multiple of 4 */

	savePhotoData: function(component) {
		var fileInput;
		var helper = this;
		fileInput = component.get('v.fileList'); //component.find('inputFile').getElement();
		if (!fileInput || !fileInput.length) {
			$A.util.addClass(component.find('log-ticket'), 'hidden_div');
			var submitMessage = $A.get("$Label.c.Case_Submit_Message");
			helper.hideSpinner(component.getSuper());
			$A.get("e.c:ShowSuccessMessage").setParams({
				'messageText': submitMessage,
				'isInfo': false,
				'isError': false,
				'isRelatedArticle': component.get("v.isRelatedArticle")
			}).fire();
			helper.fireRefreshViewEvent(component);
			return;
		}
		var files = fileInput;
		var cmp = component;
		component.set("v.downloadedFilesNumber", files[0].length);
		for (var i = 0, j = files[0].length; i < j; i++) {
			window.setTimeout(
				$A.getCallback(function() {
					var fileItem = component.get('v.fileList[0]');
					var file = fileItem.pop();
					helper.readFile(cmp, file, file.name);
					component.set('v.fileList[0]', fileItem)
				}), i * 1700
			);
		}
	},
	readFile: function(component, file, fileName) {
		var reader = new FileReader();
		var self = this;
		reader.onload = function(e) {
			var fileContents = e.target.result;
			var base64Mark = 'base64,';
			var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
			fileContents = fileContents.substring(dataStart);
			self.upload(component, file, fileContents, fileName);
		};
		reader.readAsDataURL(file);
	},
	upload: function(component, file, fileContents, fileName) {
		var fromPos = 0;
		var toPos = Math.min(fileContents.length, fromPos + this.CHUNK_SIZE);
		// start with the initial chunk
		this.uploadChunk(component, file, fileContents, fromPos, toPos, '', fileName);
	},
	uploadChunk: function(component, file, fileContents, fromPos, toPos, attachId, fileName) {
		var action = component.get("c.saveTheChunk");
		var chunk = fileContents.substring(fromPos, toPos);
		action.setParams({
			parentId: component.get("v.newCaseId"),
			fileName: fileName,
			base64Data: encodeURI(chunk).replace(/%5B/g, '[').replace(/%5D/g, ']'),
			contentType: file.type,
			fileId: attachId,
			isAttachment: false
		});
		var self = this;
		action.setCallback(this, function(response) {
			if (response.getState() === "SUCCESS") {
				attachId = response.getReturnValue();
				fromPos = toPos;
				if (attachId) {
					toPos = fileContents.length;
				} else {
					toPos = Math.min(fileContents.length, fromPos + self.CHUNK_SIZE);
				}

				if (fromPos < toPos) {
					self.uploadChunk(component, file, fileContents, fromPos, toPos, attachId, fileName);
				} else {
					component.set("v.subject", '');
					var downloadedFilesNumber = component.get("v.downloadedFilesNumber") - 1;
					component.set("v.downloadedFilesNumber", downloadedFilesNumber);
					self.addNewComment(component, attachId, downloadedFilesNumber);
				}
			}
		});
		window.setTimeout(
			$A.getCallback(function() {
				$A.enqueueAction(action);
			}), 1300
		);
	},

	addNewComment: function(component, attachId, downloadedFilesNumber) {
		var action = component.get("c.addNewFeedItem");
		action.setParams({
			"caseId": component.get("v.newCaseId"),
			"contentVersionId": attachId
		});
		var helper = this;
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				if (downloadedFilesNumber == 0) {
					helper.hideSpinner(component.getSuper());
					$A.util.addClass(component.find('log-ticket'), 'hidden_div');
					var submitMessage = $A.get("$Label.c.Case_Submit_Message");
					$A.get("e.c:ShowSuccessMessage").setParams({
						'messageText': submitMessage,
						'isInfo': false,
						'isError': false,
						'isRelatedArticle': component.get("v.isRelatedArticle")
					}).fire();
					helper.fireRefreshViewEvent(component);
				}
			} else {
				var errors = response.getError();
				helper.hideSpinner(component.getSuper());
			}
		});
		$A.enqueueAction(action);
	},
	checkRequaredField: function(component) {
		var fields = component.get('v.fields');
		var caseObject = component.get("v.newCase");
		var reqaredField = [];
		for (var i = 0; i < fields.length; i++) {
			if ((fields[i].required || fields[i].DBRequired) && (caseObject[fields[i].fieldPath] == null || caseObject[fields[i].fieldPath] === '')) {
				reqaredField.push(fields[i].label);
			}
		}
		return reqaredField;
	},
	displayErrorMessage: function(component, message) {
		var submitBtn = component.find("submit-btn");
		submitBtn.set('v.disabled', false);
		var evantMessage = $A.get("e.c:ShowSuccessMessage");
		if (evantMessage) {
			evantMessage.setParams({
				'messageText': (message ? message : $A.get("$Label.c.Case_Create_Error_Message")),
				'isError': true
			}).fire();
		}
	},
	processedFiles: function(component, requestArray, file, filesNumber, caseObject) {
		var cmp = component;
		var reader = new FileReader();
		var self = this;
		reader.onload = function(e) {
			var fileItem = {};
			var fileContents = e.target.result;
			var base64Mark = 'base64,';
			var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
			fileContents = fileContents.substring(dataStart);
			fileItem.fileContents = encodeURI(fileContents).replace(/%5B/g, '[').replace(/%5D/g, ']');
			fileItem.fileName = file.name;
			requestArray.files.push(fileItem);
			if (requestArray.files.length == filesNumber) {
				self.insertNewCase(component, caseObject, requestArray);
			}
		};
		reader.readAsDataURL(file);
	},
	checkFilesSize: function(component, totalFilesSize, file) {
		var message;
		if ((component.get("v.emailReciever") || component.get("v.newCase").HRS_Category__c === 'Learning') && file.size > this.MAX_CHUNK_SIZE) {
			message = $A.get("$Label.c.File_size_cannot_exceed").replace('{1}', this.MAX_CHUNK_SIZE / 1024).replace('{2}', (file.size / 1024).toFixed(2));
		} else if ((component.get("v.emailReciever") || component.get("v.newCase").HRS_Category__c === 'Learning') && totalFilesSize > this.MAX_CHUNK_SIZE) {
			message = $A.get("$Label.c.Total_files_size_cannot_exceed").replace('{1}', this.MAX_CHUNK_SIZE / 1024).replace('{2}', (totalFilesSize / 1024).toFixed(2));
		} else if (file.size > this.MAX_FILE_SIZE) {
			message = $A.get("$Label.c.File_size_cannot_exceed").replace('{1}', this.MAX_FILE_SIZE / 1024).replace('{2}', (file.size / 1024).toFixed(2));
		} else if (file.size == 0) {
			message = $A.get("$Label.c.You_are_trying_to_download_an_empty_file");
		}
		return message;
	},
	fireRefreshViewEvent: function(component) {
		if (!component.get("v.result") 
			&& !component.get("v.searchText")
			&& component.get('v.isCommunity')) {

			setTimeout($A.getCallback(function() {
				window.history.go(-1);
			}), 1000);
		}
	}
})