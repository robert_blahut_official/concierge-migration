({
	doInit: function(component) {
		var helper = this;
		var recordId = component.get("v.recordId");

		var action = component.get('c.getRecordWithFields');
		action.setParams({ "recordId": recordId });
		action.setCallback(this, $A.getCallback(function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var caseFields = response.getReturnValue();
				if (!caseFields) {
					component.set("v.showCloseTicket", false);
					return;
				}

				caseFields.forEach(function(field) {
					if (field.type === 'picklist') {
						var options = [];
						field.pickList.forEach(function(pickVal) {
							options.push({
								value: pickVal.value,
								label: pickVal.label,
								selected: pickVal.isDefaultValue
							});
						});
						field.options = options;
						helper.handlePicklistChange(component, caseFields, field.fieldPath);
					}
				});
				component.set("v.showCloseTicket", true);
				component.set("v.fields", caseFields);

			} else if (state === "ERROR") {
				var errorMessage = "Internal server error";
				var errors = response.getError();
				if (errors) {
					errorMessage = "Error message: ";
					errors.forEach(function (err) {
						errorMessage += err.message + '<br/>';
					});
				}
				helper.showErrorMessage(component, errorMessage);
			}
		}));
		$A.enqueueAction(action);
	},

	saveRecord: function(component) {
		var helper = this;
		var record = helper.getTargetRecord(component);
		var action = component.get('c.saveSObject');

		action.setParams({
			"serializedRecord": JSON.stringify(record)
		});
		action.setCallback(this, $A.getCallback(function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				component.set("v.displayPopUp", false);
				helper.closePopup(component);

				$A.get("e.c:ShowCaseList").fire();
			} else if (state === "ERROR") {
				var errorMessage = "Internal server error";
				var errors = response.getError();
				if (errors) {
					errorMessage = "";
					errors.forEach(function (err) {
						errorMessage += err.message;
					});
				}

				helper.showErrorMessage(component, errorMessage);
				window.setTimeout($A.getCallback(function() {
					helper.closePopup(component);
				}), 1000);
			}
		}));
		$A.enqueueAction(action);
	},

	closePopup: function(component) {
		var body = document.querySelector("body");
		var communityName = body.className.indexOf("comm");
		var element = body.querySelector(".my_tikets_div");
		var platform = window.navigator.platform;
		var elementSpaceDiv = body.querySelector(".all_free_space_div");

		if (platform === "iPhone") {
			element.classList.remove("iOS");
			elementSpaceDiv.classList.remove("iOS");
		} 

		if (communityName > 0) {
			element.classList.remove("popupOpenCom");
		} else {
			element.classList.remove("popupOpen");
		}

		//enabled scrolling
		element.style.overflow = "auto";
	},

	getTargetRecord: function(component) {
		var recordId = component.get("v.recordId");
		var fields = component.get("v.fields");
		var record = {
			Id: recordId
		};

		fields.forEach(function(field) {
			record[field.fieldPath] = field.value;
		});
		return record;
	},

	showErrorMessage: function(component, message) {
		var messageCmp = component.find('exceptionMessage');
		component.set('v.errorMessage', message);

		$A.util.removeClass(messageCmp, 'hidden_div');
		window.setTimeout($A.getCallback(function() {
			if (component.isValid()) {
				$A.util.addClass(messageCmp, 'hidden_div');
			}
		}), 2000);
	},

	handlePicklistChange: function(cmp, fieldsList, targetFieldPath) {
		var dependentLists = {};
		var parentConfig = {};

		fieldsList.forEach(function(el) {
			if ((el.type === 'picklist' || el.type === 'multipicklist') && el.isDependent) {
				dependentLists[el.controlField] = el;
			}
			if (el.fieldPath === targetFieldPath) {
				parentConfig = el;
			}
		});

		while (parentConfig && parentConfig.fieldPath in dependentLists) {
			parentConfig = dependentLists[parentConfig.fieldPath];
			if (parentConfig) {
				this.handleDependentPicklistChange(cmp, fieldsList, parentConfig);
				parentConfig = dependentLists[parentConfig.controlField];
			}
		}
	},

	handleDependentPicklistChange: function(cmp, fieldsList, childConfig) {
		if (!childConfig.isDependent) {
			return;
		}

		var isRequired = childConfig.required || childConfig.DBRequired;
		var options = [];

		if (!isRequired) {
			options.push({
				label: $A.get("$Label.c.None"),
				value: "",
				selected: false
			});
		}

		var parentValue = '';
		fieldsList.forEach(function(el) {
			if (el.fieldPath === childConfig.fieldPath) {
				el.value = '';
			}
			if (el.fieldPath === childConfig.controlField) {
				parentValue = el.value;
			}
		});
		
		var availValues = childConfig.dependentValues[parentValue] || [];
		childConfig.pickList.forEach(function(pickVal) {
			availValues.forEach(function(availValue) {
				if (availValue === pickVal.value) {
					options.push({
						value: pickVal.value,
						label: pickVal.label,
						selected: pickVal.isDefaultValue
					});
					if (pickVal.isDefaultValue) {
						childConfig.value = pickVal.value;
					}
				}
			});
		});

		childConfig.options = options;

		var selectedItems = options.filter(function(item) {
			return item.selected;
		});

		if (!selectedItems.length && options.length > 0) {
			options[0].selected = true;
			childConfig.value = options[0].value;
		}
	}
})