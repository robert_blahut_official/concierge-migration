({
	init: function (component, event, helper) {
		helper.doInit(component);
	},

	displayPopUp: function (component, event, helper) {
		//this code for fix position popup when user click on close ticket
		var body = document.querySelector("body");
		var communityName = body.className.indexOf("comm");
		var platform = window.navigator.platform;
		var element = body.querySelector(".my_tikets_div");
		var elementSpaceDiv = body.querySelector(".all_free_space_div");

		if (platform === "iPhone") {
			elementSpaceDiv.classList.add("iOS");
			element.classList.add("iOS");
		} 
		
		if (communityName > 0) {
			element.classList.add("popupOpenCom");
		} else {
			element.classList.add("popupOpen");
		}
		
		//disabled scrolling
		element.style.overflow = "hidden";
		component.set("v.displayPopUp", true);
	},

	handleValueChange: function(component, event, helper) {
		var targetFieldPath = event.getSource().get('v.name');
		var caseFields = component.get('v.fields');
		helper.handlePicklistChange(component, caseFields, targetFieldPath);
		component.set('v.fields', caseFields);
	},

	hidePopUp: function (component, event, helper) {
		helper.closePopup(component);
		component.set("v.displayPopUp", false);
	},

	saveRecord: function (component, event, helper) {
		helper.saveRecord(component);
	}
})