({
	checkProfile : function(component) {
		var action = component.get("c.isCurrentUserAdmin");
		action.setCallback(this, function(response) {
			var state = response.getState();
			
			if (state === "SUCCESS") {
				if(response.getReturnValue()) {
					component.set("v.isCurrentUserAdmin", response.getReturnValue());
				}
				else {
					$A.util.removeClass(component.find("message"), "hidden_div");
				}
				
			} else if (state === "ERROR") {
				var errors = response.getError();
			}
		});
        $A.enqueueAction(action);
	},
})