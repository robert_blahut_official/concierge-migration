({
	doInit: function(component, event, helper) {
		var result = component.get("v.result");
		helper.getAllowUsersToPrintArticlesInPDF(component);
		var htmlTitle = component.get("v.result.body");
		if (htmlTitle.indexOf('class="section_card"') !== -1) {
			component.set("v.isBusinessCards", false);
		}
		if (result.rating === 1) {
			component.set("v.ratingLabel", $A.get("$Label.c.Person_thought_this_article_was_helpful"));
		} else {
			component.set("v.ratingLabel", $A.get("$Label.c.People_thought_this_article_was_helpful"));
		}
		$A.get("e.c:UpdateActiveActions").fire();
		if (component.get('v.activeActions') !== undefined && component.get('v.activeActions').cncrg__Log_a_Ticket__c !== null && !component.get('v.activeActions').cncrg__Log_a_Ticket__c) {
			helper.getAlwaysDiaplayLogTicket(component);
		}

		var startIndex = htmlTitle.indexOf('{!ltngext.');
		if (startIndex !== -1) {
			var componentName = '';
			var endIndex = htmlTitle.indexOf('}');
			htmlTitle = htmlTitle.substring(startIndex, endIndex);
			componentName = htmlTitle.substring(10, htmlTitle.length);
			component.set("v.externalComponentName", componentName);
			helper.createExtentionComponent(component, componentName, component.get("v.result.knowledgeArticleId"));
		}
	},

	showArticleDetails: function(component, event, helper) {
		component.set("v.result", event.getParam("result"));
		component.set("v.searchText", event.getParam("query"));
		$A.util.removeClass(component.find('total_div'), 'hidden_div');
	},

	hideDetails: function(component, event, helper) {
		helper.setUrlParam(helper.QUERY_PARAMS.DETAILS);
		component.set("v.articleDetailPopup", null);
	},

	yes: function(component, event, helper) {
		helper.rateItem(component, true, true);
	},

	no: function(component, event, helper) {
		helper.rateItem(component, false, true);
	},

	noFromFeedback: function(component, event, helper) {
		helper.rateItem(component, false, false);
	},

	removePopup: function(component, event, helper) {
		$A.util.removeClass(component.find('thankMessage'), 'active');
	},

	logATicket: function(component, event, helper) {
		var starRatingValue = component.getEvent("ChangeSearchStaging");
		starRatingValue.setParams({
			"score": 4
		}).fire();
		var result = component.get("v.result");
		result.settingCaseLayout = component.get("v.activeCaseLayoutRecord");
		$A.get("e.c:CreateCase").setParams({
			"result": result,
			"query": component.get("v.searchText")
		}).fire();

	},
	giveFeedback: function(component, event, helper) {
		$A.get("e.c:GetCaseIdEvent").setParams({
			"CaseId": component.get("v.result")["recordId"]
		}).fire();
	},

	toggleFavorite: function(component, event, helper) {
		var action = component.get("c.toggleFavoriteAPEX");
		var result = component.get("v.result");
		action.setParams({
			recordId: result["knowledgeArticleId"],
			isFavorite: result["isFavorite"]
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				result["isFavorite"] = !result["isFavorite"];
				component.set("v.result", result);
				var score = result["isFavorite"] ? 32 : 64;
				var starRatingValue = component.getEvent("ChangeSearchStaging");
				starRatingValue.setParams({
					"score": score
				});
				starRatingValue.fire();
			} else if (state === "ERROR") {
				var message = $A.get("$Label.c.Unknown_error");
				var errors = response.getError();

				if (errors) {
					if (errors[0] && errors[0].message) {
						message = errors[0].message;
					} else if (errors.message) {
						message = errors.message;
					}
				}
				helper.showErrorMessage(component, message);
			}
		});
		$A.enqueueAction(action);
	},
	showRelatedArticle: function(component, event, helper) {
		if (event.getParam("href")) {
			var href = event.getParam("href");
			helper.showRelatedArticle(component, event, href);
			helper.storeHrefInURL(href);
		} else {
			$A.util.removeClass(component.find('atricleDetailsSection'), 'hidden_div');
			$A.util.addClass(component.find('relatedArticle'), 'hidden_div');
			component.set("v.relatedArticle", null);
			helper.removeHrefFromURL();
		}
	},

	downloadDocument: function(component, event, helper) {
		helper.openPDF(component);
		var starRatingValue = component.getEvent("ChangeSearchStaging");
		starRatingValue.setParams({
			"score": 512
		});
		starRatingValue.fire();
	},
	more: function(component, event, helper) {
		helper.addFeedback(component);
	},
	closePopup: function(component, event, helper) {
		component.set("v.showRatePopup", false);
	}
})