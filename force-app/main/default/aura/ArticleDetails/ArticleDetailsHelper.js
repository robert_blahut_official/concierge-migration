({

	rateItem: function(component, isPlus, showMessageBoolean) {
		var result = component.get("v.result");
		var starRatingValue = component.getEvent("ChangeSearchStaging");

		if (isPlus && (result.isLike === undefined || !result.isLike)) {
			component.set("v.result.helpfulButtonsIsDisabled", true);
			result.rating++;
			result.isLike = true;
			this.checkVoteScore(component, 128);

			starRatingValue.setParams({
				"score": 128
			}).fire();

			this.rateToServer(component, result, showMessageBoolean);
		} else if (!isPlus && (result.isLike === undefined || result.isLike)) {
			component.set("v.result.helpfulButtonsIsDisabled", true);
			result.rating--;
			result.isLike = false;
			this.checkVoteScore(component, 256);

			starRatingValue.setParams({
				"score": 256
			}).fire();

			this.rateToServer(component, result, showMessageBoolean);
		}
	},

	rateToServer: function(component, result, showMessageBoolean) {
		if (showMessageBoolean) {
			var popup = component.find('thankMessage');
			component.set("v.isVoteMessage", true);
			window.setTimeout($A.getCallback(function() {
				if (component.get("v.articleDetailPopup")) {
					component.set("v.isVoteMessage", false);
				}
			}), 1500);
		}

		var action = component.get("c.rateItem");
		action.setParams({
			item: JSON.stringify(result)
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				component.set("v.result", result);
				component.set("v.result.helpfulButtonsIsDisabled", false);

				$A.get("e.c:ApplyLikes").setParams({
					"rating": result.rating,
					"isLike": result.isLike
				}).fire();

				if (result.rating === 1) {
					component.set("v.ratingLabel", $A.get("$Label.c.Person_thought_this_article_was_helpful"));
				}
				else {
					component.set("v.ratingLabel", $A.get("$Label.c.People_thought_this_article_was_helpful"));
				}
			} else if (state === "ERROR") {
				var errors = response.getError();
			}
		});
		$A.enqueueAction(action);
	},
	checkVoteScore: function(component, newScore) {
		var currentScore = component.get('v.voteScore');
		if (currentScore > 0) {
			var starRatingValue = component.getEvent("ChangeSearchStaging");
			starRatingValue.setParams({
				"score": -currentScore
			}).fire();
		}
		component.set('v.voteScore', newScore);
	},
	getAlwaysDiaplayLogTicket: function(component) {
		var action = component.get("c.getAlwaysDiaplayLogTicket");
		action.setStorable();
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				component.set("v.alwaysDiaplayLogTicket", response.getReturnValue());
			}
		});
		$A.enqueueAction(action);
	},

	getAllowUsersToPrintArticlesInPDF: function(component) {
		var action = component.get("c.getAllowUsersToPrintArticlesInPDF");
		action.setStorable();
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				component.set("v.allowUsersToPrintArticlesInPDF", response.getReturnValue());
			}
		});
		$A.enqueueAction(action);
	},
	showErrorMessage: function(component, message) {
		var messageCmp = component.find('exceptionMessage');
		component.set('v.errorMessage', message);

		$A.util.removeClass(messageCmp, 'hidden_div');
		window.setTimeout($A.getCallback(function() {
			if (component.isValid()) {
				$A.util.addClass(messageCmp, 'hidden_div');
			}
		}), 2000);
	},
	changeIframeTags: function(component, allIframe) {
		var body = component.find("body").getElement();
		var iframePlaces = body.getElementsByClassName("video-container");
		var iframesArray = [];
		for (var i = 0, j = allIframe.length; i < j; i++) {
			var para = document.createElement("iframe");
			var regex = /<iframe.*?src="(.*?)"/;
			var regexWidth = /<iframe.*?width="(.*?)"/;
			var regexHeight = /<iframe.*?height="(.*?)"/;
			var src = regex.exec(allIframe[i])[1];
			var width = regexWidth.exec(allIframe[i])[1];
			var height = regexHeight.exec(allIframe[i])[1];
			para.src = src;
			para.width = width;
			para.height = height;
			para.frameborder = 0;
			para.scrolling = 'no';
			iframePlaces[i].appendChild(para);
		}
	},
	parseLinkTags: function(component, linksTags) {
		for (var i = 0, j = linksTags.length; i < j; i++) {
			linksTags[i].removeAttribute('download');
			if (linksTags[i].href.indexOf('/articles/') !== -1) {
				linksTags[i].addEventListener("click", $A.getCallback(function(event) {
					$A.get("e.c:RelatedArticleClick").setParams({
						"href": event.target.target,
						"component": 'ArticleDetails',
						"outerText": event.target.outerText
					}).fire();
				}));
				linksTags[i].target = linksTags[i].href.substring(linksTags[i].href.indexOf('articles/'), (linksTags[i].href.indexOf('?') !== -1 ? linksTags[i].href.indexOf('?') : linksTags[i].href.length));
				linksTags[i].removeAttribute("href");
			}
		}
	},
	showRelatedArticle: function(component, event, href) {
		var helper = this;
		var action = component.get("c.getRelatedArticleData");
		action.setParams({
			href: href,
			prefix: component.get("v.communityPrefix")
		});

		var error = $A.get("$Label.c.Message_Disable_Article_Link");
		action.setCallback(this, function(response) {
			var state = response.getState();

			if (state === "SUCCESS") {
				var newResult = response.getReturnValue()[0];
				var breadcrumbsOptions = component.get("v.breadcrumbsOptions") || [];
				if (event.getParam("outerText") || newResult.title) {
					breadcrumbsOptions.push({
						'label': (newResult.title || event.getParam("outerText")),
						'value': event.getParam("href")
					});
					component.set("v.breadcrumbsOptions", breadcrumbsOptions);
				}

				$A.createComponents([
					[
						"c:RelatedArticleDetails", {
							href: href,
							activeActions: newResult.setting,
							activeCaseLayoutRecord: newResult.settingCaseLayout,
							result: newResult,
							liveAgentPage: component.get('v.liveAgentPage'),
							communityLoading: component.get("v.communityLoading"),
							availableLA: component.get("v.availableLA"),
							parentComponent: 'ArticleDetails',
							breadcrumbsOptions: component.getReference("v.breadcrumbsOptions"),
							parentArticle: component.get("v.result"),
							appName: component.get("v.appName"),
							isUserHasPermitionToCreateCase: component.get("v.isUserHasPermitionToCreateCase"),
							alwaysDiaplayLogTicket: component.get("v.alwaysDiaplayLogTicket")
						}
					]
				], function(components, status, errorMessage) {
					if (status === "SUCCESS") {
						component.set("v.relatedArticle", components);
						$A.util.addClass(component.find('atricleDetailsSection'), 'hidden_div');
						$A.util.removeClass(component.find('relatedArticle'), 'hidden_div');
					} else if (status === "ERROR") {
						helper.showErrorMessage(component, error);
					}
				});
				/*if(newResult.rating === 1)
					component.set("v.ratingLabel", $A.get("$Label.c.Person_thought_this_article_was_helpful"));
				else 
					component.set("v.ratingLabel", $A.get("$Label.c.People_thought_this_article_was_helpful"));		
					*/
			} else if (state === "ERROR") {
				helper.showErrorMessage(component, error);
			}
		});
		$A.enqueueAction(action);

		/*to do dymamic creation of the RelaLated Article*/
	},
	replaceAll: function(target, search, replacement) {
		return target.replace(new RegExp(search, 'g'), replacement);
	},

	openPDF: function(component) {
		var path = window.location.pathname + '';
		path = (path.includes('/s/')) ? path.substring(0, path.indexOf('/s/')) : '';
		var url = path + '/apex/cncrg__LightningPDFGenerator?Id=' + component.get("v.result").recordId + '&Type=' + component.get("v.result").type;

		window.open(url);
	},
	addFeedback: function(component) {
		component.set("v.showRatePopup", true);
	},

	storeHrefInURL: function(href) {
		this.setUrlParam(this.QUERY_PARAMS.HREF, href);
	},

	removeHrefFromURL: function() {
		this.setUrlParam(this.QUERY_PARAMS.HREF);
	},

	createExtentionComponent: function(component, componentName, knowledgeArticleId) {
		var helper = this;
		//$A.get("e.cncrgdemo:showSpinner").fire();
		if (componentName !== undefined) {
			$A.createComponent(
				componentName, {
					"knowledgeArticleId": knowledgeArticleId
				},
				function(extentionBlock, status, errorMessage) {
					if (status === "SUCCESS") {
						component.set("v.body", extentionBlock);
						//$A.get("e.cncrgdemo:hideSpinner").fire();
					} else if (status === "ERROR") {
						//$A.get("e.cncrgdemo:hideSpinner").fire();
						//helper.createExtentionComponent(component, componentName, knowledgeArticleId);
					}
				}
			);
		}
	}
})