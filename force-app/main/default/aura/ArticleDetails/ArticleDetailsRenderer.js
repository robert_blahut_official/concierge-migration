({
	// Your renderer method overrides go here
	 afterRender: function(component, helper) {
		this.superAfterRender();
		var htmlBody = component.get("v.result.body");
		if(htmlBody.indexOf('{!ltngext') !== -1) {
			
			return;
		}
		if(htmlBody) {
			 htmlBody = helper.replaceAll(helper.replaceAll(htmlBody, '<iframe', '<div class="video-container"><iframe'), '</iframe>', '</iframe></div>');
		}
		var body = component.find("body").getElement();
		var regexIframe = /(?:<iframe[^>]*)(?:(?:\/>)|(?:>.*?<\/iframe>))/g;
		body.innerHTML = htmlBody;
		if(htmlBody) {
			var allIframe = htmlBody.match(regexIframe);
			if(allIframe && allIframe.length > 0 && body.innerHTML.match(regexIframe) == null) {
				helper.changeIframeTags(component, allIframe);
			}
		}
		var linksTags = body.getElementsByTagName('a');

		if(linksTags && linksTags.length > 0) {
			helper.parseLinkTags(component, linksTags);
		}
		// helper.scrollToAnchor();
		 
	 }	
})