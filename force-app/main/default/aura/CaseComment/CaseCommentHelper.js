({
	listOfActiveFlags: {},
	MAX_FILE_SIZE: 4194304,
	/* 6 000 000 * 3/4 to account for base64 */
	//CHUNK_SIZE: 950 000,
	CHUNK_SIZE: 3700000,
	addNewComment: function(component, attachId) {
		this.showSpinner(component.getSuper());
		var newCommentText = component.get('v.newCommentText');
		var parentId = component.get("v.parentId");
		var action = component.get("c.addNewFeedItem");
		var self = this;
		action.setParams({
			"caseId": parentId,
			"text": newCommentText,
			"contentVersionId": attachId
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				component.set('v.newCommentText', '');
				this.addCommentInList(component, response.getReturnValue());
			} else {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {}
				} else {}
			}
			self.hideSpinner(component.getSuper());
		});
		window.setTimeout(
			$A.getCallback(function() {
				$A.enqueueAction(action);
			}), 5000
		);
	},

	addCommentInList: function(component, newItem) {
		if (!newItem.feedBody) {
			newItem.feedBody = '';
		}
		var isCommunity = component.get('v.isCommunity');
		this.buildUrl(isCommunity, newItem);
		var currentList = component.get('v.currentList');
		var currentType = component.get('v.type');
		if (currentType === 'feedItem') {
			currentList.unshift(newItem);
		} else {
			currentList.push(newItem);
		}
		component.set('v.currentList', currentList);
	},

	buildUrl: function(isCommunity, item) {
		if (item.attachedFileInfo) {
			var fileUrl = '/sfc/servlet.shepherd/version/renditionDownload?rendition=ORIGINAL_';
			var fileExtension = item.attachedFileInfo.FileExtension || 'png';

			if (isCommunity) {
				var currPath = window.location.pathname;
				var temp = currPath.split("/");
				var communityPrefix = temp[1];
				if (communityPrefix === 's') {
					item.relatedRecordId = fileUrl + fileExtension + '&versionId=' + secureFilters.html(item.attachedFileInfo.Id);
				} else {
					item.relatedRecordId = '/' + secureFilters.html(communityPrefix) + fileUrl + fileExtension + '&versionId=' + secureFilters.html(item.attachedFileInfo.Id);
					if (item.downloadURL) {
						item.downloadURL = '/' + secureFilters.html(communityPrefix) + item.downloadURL;
					}
				}
			} else {
				item.relatedRecordId = fileUrl + fileExtension + '&versionId=' + secureFilters.html(item.attachedFileInfo.Id);
			}
		}
	},

	savePhotoData: function(component, fileInput) {
		var self = this;
		var file = fileInput.files[0];
		var reader = new FileReader();
		reader.onload = function(e) {
			//fileInput.files = [];
			var fileContents = e.target.result;
			var base64Mark = 'base64,';
			var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
			fileContents = fileContents.substring(dataStart);
			self.upload(component, file, fileContents, file.name);
		};

		reader.readAsDataURL(file);
	},
	upload: function(component, file, fileContents, fileName, tempHelper) {
		var fromPos = 0;
		var toPos = Math.min(fileContents.length, fromPos + this.CHUNK_SIZE);

		this.uploadFeedChunk(component, file, fileContents, fromPos, toPos, '', fileName);
	},
	uploadFeedChunk: function(component, file, fileContents, fromPos, toPos, attachId, fileName) {
		var action = component.get("c.saveTheChunk");
		var chunk = fileContents.substring(fromPos, toPos);

		action.setParams({
			fileName: fileName,
			base64Data: encodeURI(chunk).replace(/%5B/g, '[').replace(/%5D/g, ']'),
			contentType: file.type,
			fileId: attachId,
			isAttachment: false
		});

		var self = this;
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				attachId = response.getReturnValue();
				fromPos = toPos;
				if (attachId) {
					toPos = fileContents.length;
				} else {
					toPos = Math.min(fileContents.length, fromPos + self.CHUNK_SIZE);
				}
				//toPos = Math.min(fileContents.length, fromPos + self.CHUNK_SIZE);

				if (fromPos < toPos) {
					self.uploadFeedChunk(component, file, fileContents, fromPos, toPos, attachId, fileName);
				} else {
					this.addNewComment(component, attachId);
					self.listOfActiveFlags['CLEAR_FILE'] = true;
					component.set('v.fileFormLabel', '');
					component.set('v.fileTooLarge', false);
					component.set('v.fileAttached', false);
				}
			} else if (state === "INCOMPLETE") {} else if (state === "ERROR") {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {}
				} else {}
			}
		});
		window.setTimeout(
			$A.getCallback(function() {
				$A.enqueueAction(action);
			}), 10
		);
	},
})