({
    checkTextArea : function(component, event, helper){
        var areaInput = event.target;
        var areaString = component.get("v.newCommentText");
        var add_new_comment_button = component.find("add_new_comment");
        var fileTooLarge = component.get('v.fileTooLarge');
        var fileAttached = component.get('v.fileAttached');

        if ((!areaInput || areaString.length === 0 || fileTooLarge) && !fileAttached) {
            $A.util.addClass(add_new_comment_button, "disabled_style");
            component.set('v.disabledButton', true);
            return;
        } else {
            component.set('v.disabledButton', false);
            $A.util.removeClass(add_new_comment_button, "disabled_style");
        }
    },

    addNewComment: function (component, event, helper) {
        var disabledButton = component.get('v.disabledButton');
        var commentText = component.get("v.newCommentText");
        var add_new_comment_button = component.find('add_new_comment');
        if (!disabledButton) {
            $A.util.addClass(add_new_comment_button, "disabled_style");
            component.set('v.disabledButton', true);

            var fileInput;
            helper.showSpinner(component.getSuper());
            $A.util.addClass(add_new_comment_button, 'disabled_style');
            component.set('v.disabledButton',true);
            fileInput = component.find('inputFile').getElement();
            var fileAttached = component.get('v.fileAttached');
            if (!fileInput || !fileInput.files.length || fileInput.files.length <= 0 || !fileAttached) {
                helper.addNewComment(component, undefined);
            } else {
                helper.savePhotoData(component, fileInput);
            }
        }
    },

    loadFile : function(component, event, helper) {
        var fileInput = event.target;
        var add_new_comment_button = component.find('add_new_comment');
        if (!fileInput || !fileInput.files || !fileInput.files.length) {
            component.set('v.fileFormLabel', '');
            component.set('v.fileTooLarge', false);
            component.set('v.fileAttached', false);

            var areaString = component.get("v.newCommentText");
            if (areaString && areaString.length === 0) {
                component.set('v.disabledButton', true);
                $A.util.addClass(add_new_comment_button, "disabled_style");
            } else if (areaString && areaString.length > 0) {
                component.set('v.disabledButton', false);
                $A.util.removeClass(add_new_comment_button, "disabled_style");
            }
            return;
        }

        var file = fileInput.files[0];
        if (file.size > helper.MAX_FILE_SIZE) {
            component.set('v.fileFormLabel', $A.get("$Label.c.File_size_cannot_exceed").replace('{1}', helper.MAX_FILE_SIZE/1024).replace('{2}', (file.size/1024).toFixed(2)));
            component.set('v.disabledButton', true);
            $A.util.addClass(add_new_comment_button, "disabled_style");
            component.set('v.fileTooLarge', true);
            component.set('v.fileAttached', false);
        } else {
            component.set('v.fileTooLarge', false);
            component.set('v.fileAttached', true);
            component.set('v.fileFormLabel',file.name);
            component.set('v.disabledButton', false);
            $A.util.removeClass(add_new_comment_button, "disabled_style");
        }
    },

    expandTextArea : function(component, event, helper) { 
        component.set('v.fullTextArea', component.get('v.commentIndex'));
        var currentTextArea = event.currentTarget;
        component.set('v.heightTextArea', true);   
    },

    displayFullComment :function(component, event, helper){
        var commentIndex = component.get('v.commentIndex');
        var fullTextArea = component.get('v.fullTextArea');
        if(commentIndex === fullTextArea){
            component.set("v.heightTextArea", true);
        }else{
            component.set("v.heightTextArea", false);
        }
    }
})