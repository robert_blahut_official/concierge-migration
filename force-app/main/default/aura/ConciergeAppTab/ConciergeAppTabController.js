({
	gotoConcierge : function (component, event, helper) {
		var device = $A.get("$Browser.formFactor");	
		var isWEBKIT = $A.get('$Browser.isWEBKIT');
		var isIOS = $A.get("$Browser.isIOS");
		var isAndroid = $A.get("$Browser.isAndroid");

		if(device === 'DESKTOP') {
			if (document.cookie.replace(/(?:(?:^|.*;\s*)doSomethingOnlyOnce\s*\=\s*([^;]*).*$)|^.*$/, "$1") !== "true") {
				window.location.href = '/cncrg/ConciergeApplication.app';
				document.cookie = "doSomethingOnlyOnce=true; expires=Fri, 31 Dec 9999 23:59:59 GMT";
			}	
			else {
				//component.set("v.showRedirectButton", "true");
				document.cookie = "doSomethingOnlyOnce=false; expires=Fri, 31 Dec 9999 23:59:59 GMT";
			}
		}
		else {
			component.set("v.isNotRedirectToApp", true);

			if(isAndroid && !isWEBKIT) {
				component.set('v.isSF1Android', true);
			}
			// if(isAndroid && isWEBKIT) {
			// 	window.setTimeout(
			// 		$A.getCallback(function() {
			// 			var pageOrigin = window.location.origin;
			// 			if(pageOrigin !== undefined){
			// 				component.find('iFrame').getElement().contentWindow.postMessage('heigthAndrBrowser', pageOrigin);
			// 			}
			// 		}), 2500
			// 	);
			// }
		}
	},
	goConciergeApp : function (component, event, helper) {
		window.location.href = '/cncrg/ConciergeApplication.app';
	}
})