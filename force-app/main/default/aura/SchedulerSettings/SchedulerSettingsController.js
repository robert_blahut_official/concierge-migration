({
    doInit : function(component, event, helper) {
        helper.getSettings(component);
    },

    goBack : function(component, event, helper) {
        $A.get("e.c:BackToSettings").fire();
    },
    saveChanges : function (component, event, helper) {
        helper.implementNewSettings(component);
        helper.getSettings(component);
    },

    hideMenu: function (component, event, helper) {
        $A.util.addClass(component.find('cronContainer'), 'slds-hide');
    },

    openCronMenu: function (component, event, helper) {
        helper.splitCron(component, event.target.id);
        $A.util.removeClass(component.find('cronContainer'), 'slds-hide');
    },

    handleTypeChange: function (component, event, helper) {
        helper.updateRadioButton(component, event);
        helper.validateValue(component, event);
        var cronList = component.get('v.cronList');
        var currentOption = helper.getCurrentOption(component, cronList);
        if(currentOption.customError === '') {
            helper.updateCRONLabelByValue(component, event);
        }
        helper.validateValue(component, event);
    },

    handleValueChange: function (component, event, helper) {
        helper.validateValue(component, event);
        var cronList = component.get('v.cronList');
        var currentOption = helper.getCurrentOption(component, cronList);
        if(currentOption.customError === '') {
            helper.updateCRONLabelByValue(component, event);
        }
    },

    handleCheckboxChange: function (component, event, helper) {
        helper.updateCRONLabelByCheckbox(component, event);
    },

    implementCRON: function (component, event, helper) {
        helper.implementSettings(component);
        $A.util.addClass(component.find('cronContainer'), 'slds-hide');
    },

    validatedValue: function (component, event, helper) {
        helper.validateValue(component, event);
    },

    addNewRecord: function (component, event, helper) {
        var settingName = component.get('v.settingName');
        if(settingName !== undefined && settingName !== '') {
            if(helper.addNewSettings(component)) {
                helper.implementSettings(component);
                $A.util.addClass(component.find('newItemContainer'), 'slds-hide');
            }
        }
    },

    addNewRecordShow: function (component, event, helper) {
        var classes = component.get('v.classList');
        if(classes.length === 0 )
            helper.getClasses(component);
        $A.util.removeClass(component.find('newItemContainer'), 'slds-hide');
    },

    addNewRecordHide: function (component, event, helper) {
        $A.util.addClass(component.find('newItemContainer'), 'slds-hide');
    },

    toggleSort: function (component, event, helper) {
        var sortUrl = component.get('v.sortUrl');
        if(sortUrl.indexOf('arrowdown') < 0) {
            component.set("v.sortUrl", $A.get('$Resource.SLDS') + '/assets/icons/utility/arrowdown.svg');
            helper.sortData(component, 'className', 'desc', 'v.settings');
        } else {
            component.set("v.sortUrl", $A.get('$Resource.SLDS') + '/assets/icons/utility/arrowup.svg');
            helper.sortData(component, 'className', 'asc', 'v.settings');
        }
    }

})