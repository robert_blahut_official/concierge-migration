({
    getSettings : function(component) {
        var self = this;
        this.showSpinner(component.getSuper());
        var action = component.get("c.getSchedulerSettings");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var settings = response.getReturnValue();
                component.set("v.settings", settings);
            } else if (state === "ERROR") {
                self.notificationHandler(component, response, state);
            }
			self.hideSpinner(component.getSuper());
        });
        $A.enqueueAction(action);
    },

    getClasses : function (component) {
        var self = this;
        var action = component.get("c.getBatchList");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var classList = response.getReturnValue();
                component.set("v.classList", classList);
            } else if (state === "ERROR") {
                self.notificationHandler(component, response, state);
            }
        });
        $A.enqueueAction(action);
    },

    addNewSettings : function (component) {
        var settings = component.get("v.settings");
        var cName = component.get("v.className");
        if(cName === undefined || cName === '')
            cName = component.get('v.classList')[0];
        var newSettings = {
            nextRun: null,
            active: false,
            batchSize: component.get("v.batchSize"),
            className: cName,
            settingName: component.get("v.settingName"),
            description: component.get("v.description"),
            isRemove: false,
            isRequired: false,
            lastDuration: null,
            lastRun: null,
        };
        if(newSettings.batchSize === undefined || newSettings.batchSize === '' || newSettings.batchSize === null || newSettings.batchSize > 2000 || newSettings.batchSize < 1) {
            return false;
        }
        settings.push(newSettings);
        component.set("v.settings", settings);
        component.set("v.batchSize", 200);
        component.set("v.description", '');
        component.set("v.className", '');
        component.set("v.settingName", '');
        component.set('v.currentSettings', settings.length - 1);
        component.set('v.currentCRON', '0 * * * * *');
        return true;
    },

    implementNewSettings : function(component) {
        var self = this;
        var settingList = component.get("v.settings");
        var isError = false;
        for(var i = 0; i < settingList.length; i++) {
            if(settingList[i].batchSize === undefined || settingList[i].batchSize === '' || settingList[i].batchSize === null || settingList[i].batchSize > 2000 || settingList[i].batchSize < 1) {
                isError = true;
            }
        }
        if(isError) {
            return;
        }
        var action = component.get("c.updateSchedulerSettings");
        action.setParams({
            jsonSettings : JSON.stringify(settingList)
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            self.notificationHandler(component, response, state);
        });
        $A.enqueueAction(action);
    },
    splitCron : function (component, scheduleIndex) {
        var selectList = component.get('v.selectList');
        var cronList = component.get('v.cronList');
        selectList = [];
        cronList= [];
        var self = this;
        var action = component.get('c.parseCron');
        action.setParams({
            cronValue: component.get('v.settings')[scheduleIndex].CRON
        });
        component.set('v.currentCRON', component.get('v.settings')[scheduleIndex].CRON);
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue().optionList;
                var tempObj;
                for(var  i = 0; i < result.length; i++) {
                    tempObj = {};
                    tempObj.value = result[i].optionName;
                    tempObj.label = result[i].optionLabel;
                    selectList.push(tempObj);
                    result[i].options = [
                        {'label': i, 'value': i + '-' + false},
                        {'label': i, 'value': i + '-' + true}
                    ];
                    result[i].optVl = i + '-' + result[i].isConcrete;
                    cronList.push(result[i]);
                }
                component.set('v.selectList', selectList);
                component.set('v.selectedType', selectList[0].value);
                component.set('v.cronList', cronList);
                component.set('v.currentSettings', scheduleIndex);
            } else if (state === "ERROR") {
                self.notificationHandler(component, response, state);
            }
        });
        $A.enqueueAction(action);
    },

    updateRadioButton: function (component, event) {
        var vlList = event.getParam("value").split('-');
        var cronList = component.get('v.cronList');
        cronList[vlList[0]].isConcrete = vlList[1] === 'true';
        component.set('v.cronList', cronList);
    },

    updateCRONLabelByValue: function (component, event) {
        var selectedType = component.get('v.selectedType');
        var cronList = component.get('v.cronList');
        var cronValue = component.get('v.currentCRON').split(' ');
        for(var i = 0; i < cronList.length; i++) {
            if(cronList[i].optionName === selectedType) {
                if(cronList[i].isConcrete) {
                    if(cronList[i].concrete !== '') {
                        cronValue[i + 1] = cronList[i].concrete;
                    } else {
                        cronValue[i + 1] = '*';
                    }
                } else {
                    if(cronList[i].interval !== '') {
                        cronValue[i + 1] = '1/' + cronList[i].interval;
                    } else {
                        cronValue[i + 1] = '*';
                    }
                }
            }
        }
        component.set('v.currentCRON', cronValue.join(' '));
    },

    updateCRONLabelByCheckbox: function (component, event) {
        var selectedType = component.get('v.selectedType');
        var cronList = component.get('v.cronList');
        var cronValue = component.get('v.currentCRON').split(' ');
        var newValue = [];
        for(var i = 0; i < cronList.length; i++) {
            if(cronList[i].optionName === selectedType) {
                for(var j = 0; j < cronList[i].advancedConf.length; j++) {
                    if(cronList[i].advancedConf[j].isSelected) {
                        newValue.push(cronList[i].advancedConf[j].propertyValue);
                    }
                }
                if(newValue.length > 0) {
                    cronValue[i + 1] = newValue.join(',');
                } else {
                    cronValue[i + 1] = '*';
                }
            }
        }
        component.set('v.currentCRON', cronValue.join(' '));
    },

    implementSettings: function (component) {
        var currentSettings = component.get('v.currentSettings');
        var settings = component.get('v.settings');

        var self = this;
        var action = component.get('c.getNextRunDate');
        action.setParams({
            cronValue: component.get('v.currentCRON'),
            stName: settings[currentSettings].settingName,
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                settings[currentSettings].CRON = component.get('v.currentCRON');
                settings[currentSettings].nextRun = response.getReturnValue();
                component.set('v.settings', settings);
                this.reSort(component);
            } else if (state === "ERROR") {
                //self.notificationHandler(component, response, state);
            }
        });
        $A.enqueueAction(action);

    },

    notificationHandler: function (component, response, status) {
        if (status === "SUCCESS") {
            component.find("messageTop").set("v.severity", "confirm");
            component.find("messageBottom").set("v.severity", "confirm");
            $A.util.removeClass(component.find("confirmationTop"), "display_false");
            $A.util.removeClass(component.find("confirmationBottom"), "display_false");
            component.set("v.message", $A.get("$Label.c.All_Changes_Were_Saved_Successfully"));
            window.setTimeout(
                $A.getCallback(function() {
                    if (component.isValid()) {
                        $A.util.addClass(component.find("confirmationTop"), "display_false");
                        $A.util.addClass(component.find("confirmationBottom"), "display_false");
                    }
                }), 2000
            );
        } else if (status === "ERROR") {
            var errors = response.getError();
            var message = "Unknown error";
            component.find("messageTop").set("v.severity", "error");
            component.find("messageBottom").set("v.severity", "error");
            $A.util.removeClass(component.find("confirmationTop"), "display_false");
            $A.util.removeClass(component.find("confirmationBottom"), "display_false");
            if (errors && Array.isArray(errors) && errors.length > 0) {
                message = errors[0].message;
                if(errors[0].fieldErrors && errors[0].fieldErrors.Name && Array.isArray(errors[0].fieldErrors.Name) && errors[0].fieldErrors.Name.length > 0) {
                    message = errors[0].fieldErrors.Name[0].message;
                }
                if(message === undefined) {
                    if(errors[0].pageErrors && Array.isArray(errors[0].pageErrors) && errors[0].pageErrors.length > 0) {
                        message = errors[0].pageErrors[0].message;
                    }
                }
            }
            component.set("v.message", message);
        }
    },
    validateValue: function (component, event) {
        var currentValue = event.getSource().get('v.value');
        var valid = event.getSource().get("v.validity");
        var cronList = component.get('v.cronList');
        var currentOption = this.getCurrentOption(component, cronList);
        currentOption.customError = '';
        if(currentOption.isConcrete) {
            this.checkFormat(currentOption);
            this.checkMultipleSpecSymbols(currentOption);
            this.checkRightStartAndEnd(currentOption);
            this.checkDoubleCharacter(currentOption);
            this.checkStar(currentOption);
            this.checkRange(currentOption);
        } else {
            this.checkInterval(currentOption);
        }
        component.set('v.cronList', cronList);
    },

    getCurrentOption: function (component, cronList) {
        var result;
        var selectedType = component.get('v.selectedType');
        var newValue = [];
        for(var i = 0; i < cronList.length; i++) {
            if(cronList[i].optionName === selectedType) {
                result = cronList[i];
            }
        }
        return result;
    },


    checkInterval: function (currentOption) {
        var minV = currentOption.optionName === 'MINUTES' ? 3 : 1;
        if(currentOption.interval < minV || currentOption.interval > Math.ceil((currentOption.endValue - currentOption.startValue) / 2)) {
            currentOption.customError = $A.get("$Label.c.Max_Interval_validation_message").replace('{0}', '' + minV).replace('{1}', Math.ceil((currentOption.endValue - currentOption.startValue) / 2));
        }
    },

    checkFormat: function (currentOption) {
        var result = currentOption.concrete.match('[0-9, \\-/*]*');
        if(result[0] !== result.input) {
            currentOption.customError = $A.get("$Label.c.Invalid_format");
        }
    },

    checkRange: function (currentOption) {
        var newValue = [];
        if(currentOption.concrete.indexOf(',') > 0) {
            newValue = currentOption.concrete.split(',');
        } else if(currentOption.concrete.indexOf('-') > 0) {
            newValue = currentOption.concrete.split('-')
        } else if(currentOption.concrete.indexOf('*') < 0) {
            newValue.push(currentOption.concrete);
        }
        var startV;
        for(var j = 0; j < newValue.length; j++) {
            startV = currentOption.startValue;
            if(newValue[j] < startV || newValue[j] > currentOption.endValue) {
                currentOption.customError = $A.get("$Label.c.Max_Range_validation_message") + ' ' + startV + '-' + currentOption.endValue;
            }
        }
    },

    checkMultipleSpecSymbols: function (currentOption) {
        var counter = 0;
        if(currentOption.concrete.indexOf(',') > 0) {
            counter++;
        }
        if(currentOption.concrete.indexOf('-') > 0) {
            counter++;
        }
        if(currentOption.concrete.indexOf('/') > 0) {
            counter++;
        }
        if(counter > 1) {
            currentOption.customError = $A.get("$Label.c.Invalid_format");
        }
    },
    checkRightStartAndEnd:function (currentOption) {
        if(currentOption.concrete.indexOf(',') === 0 || (currentOption.concrete.lastIndexOf(',') + 1) === currentOption.concrete.length && currentOption.concrete.length !== 0) {
            currentOption.customError = $A.get("$Label.c.Invalid_format");
        }
        if(currentOption.concrete.indexOf('-') === 0 || (currentOption.concrete.lastIndexOf('-') + 1) === currentOption.concrete.length && currentOption.concrete.length !== 0) {
            currentOption.customError = $A.get("$Label.c.Invalid_format");
        }
        if(currentOption.concrete.indexOf('/') === 0 || (currentOption.concrete.lastIndexOf('/') + 1) === currentOption.concrete.length && currentOption.concrete.length !== 0) {
            currentOption.customError = $A.get("$Label.c.Invalid_format");
        }
    },
    checkDoubleCharacter: function (currentOption) {
        if(currentOption.concrete.indexOf('-') !== currentOption.concrete.lastIndexOf('-')) {
            currentOption.customError = $A.get("$Label.c.Invalid_format");
        }
        if(currentOption.concrete.indexOf('/') !== currentOption.concrete.lastIndexOf('/')) {
            currentOption.customError = $A.get("$Label.c.Invalid_format");
        }
        if(currentOption.concrete.indexOf('-') !== currentOption.concrete.lastIndexOf('-')) {
            currentOption.customError = $A.get("$Label.c.Invalid_format");
        }

        if(currentOption.concrete.indexOf(',,') > 0) {
            currentOption.customError = $A.get("$Label.c.Invalid_format");
        }
        if(currentOption.concrete.indexOf('--') > 0) {
            currentOption.customError = $A.get("$Label.c.Invalid_format");
        }
        if(currentOption.concrete.indexOf('//') > 0) {
            currentOption.customError = $A.get("$Label.c.Invalid_format");
        }
    },
    checkStar: function (currentOption) {
        if(currentOption.concrete.indexOf('*') >= 0 && currentOption.concrete.length > 1) {
            currentOption.customError = $A.get("$Label.c.Invalid_format");
        }
    },

    reSort: function (component ) {
        var sortUrl = component.get('v.sortUrl');
        if(sortUrl !== '') {
            if(sortUrl.indexOf('arrowdown') > 0) {
                this.sortData(component, 'className', 'desc', 'v.settings');
            } else {
                this.sortData(component, 'className', 'asc', 'v.settings');
            }
        }
    },

    sortData: function (component, fieldName, sortDirection, target) {
        var data = component.get(target);
        component.set(target, []);
        var reverse = sortDirection !== 'asc';
        data.sort(this.sortBy(fieldName, reverse));
        component.set(target, data);
    },
    sortBy: function (field, reverse) {
        var key =  function(x) {
            return x[field]
        };
        reverse = !reverse ? 1 : -1;
        return function (a, b) {
            a = key(a);
            b = key(b);
            if(a === undefined) {
                return reverse * 1;
            }
            if(b === undefined) {
                return reverse * -1;
            }
            return reverse * ((a > b) - (b > a));
        }
    }


})