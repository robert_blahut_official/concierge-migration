({
	doInit : function (component, event, helper) {
		var objectName = component.get("v.objectName");
		var listOfTypeSettings = component.get("v.listOfTypeSettings");
		var objectPosition;
		for(var i=0, j=listOfTypeSettings.length; i<j; i++) {
			if(listOfTypeSettings[i].articleType === objectName) {
				objectPosition = i;
				break;
			}
		}
		//component.set("v.objectPosition", objectPosition);
		component.set("v.typeSettings", listOfTypeSettings[objectPosition]);
		var fieltersList = listOfTypeSettings[objectPosition].filterList == undefined ? [] : listOfTypeSettings[objectPosition].filterList;
		component.set("v.fieltersList", fieltersList);
	},
	addNewLine: function (component, event, helper) {
		var fieltersList = component.get("v.fieltersList");
		fieltersList.push(new Object());
		component.set("v.fieltersList", fieltersList);
	},
	applyFilter : function (component, event, helper) {
		var cmp = component;
		var fieltersList = component.get("v.fieltersList");
		var filtersLine = '';
		for(var i=0, j=fieltersList.length; i<j; i++) {
			filtersLine += fieltersList[i].fieldName + ';' + fieltersList[i].expression + ';' + fieltersList[i].fieldValue + ';';
		}
		if(filtersLine.length > 255) {
			$A.util.removeClass(component.find("custom_message"), "display_false");
			component.set("v.message", $A.get("$Label.c.Too_Many_Expressions"));
			window.setTimeout(                   
				$A.getCallback(function() {
					$A.util.addClass(cmp.find("custom_message"), "display_false");
				}), 2000
			);
			return;
		}
		component.set("v.typeSettings.filterList", fieltersList);
		component.set("v.filtersPopUp", null);
		//var objectPosition = component.get("v.objectPosition");
		//console.log(JSON.stringify(component.get("v.listOfTypeSettings")[objectPosition]));
	},
	closeWindow : function (component, event, helper) {
		component.set("v.filtersPopUp", null);
	},
	removeLine : function (component, event, helper) {
		var conditionPosition = event.target.id;
		var fieltersList = component.get("v.fieltersList");
		fieltersList.splice(conditionPosition, 1);
		component.set("v.fieltersList", fieltersList);
	}
})