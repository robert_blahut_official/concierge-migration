({
	/*
	 *  Map the Schema.FieldSetMember to the desired component config, including specific attribute values
	 *  Source: https://www.salesforce.com/us/developer/docs/apexcode/index_Left.htm#CSHID=apex_class_Schema_FieldSetMember.htm|StartTopic=Content%2Fapex_class_Schema_FieldSetMember.htm|SkinName=webhelp
	 *
	 *  Change the componentDef and attributes as needed for other components
	 */
	configMap: {
		'anytype': { componentDef: 'ui:inputText', attributes: {
			'class': 'input_section'
		} },
		'base64': { componentDef: 'ui:inputText', attributes: {
			'class': 'input_section'
		} },
		'boolean': {componentDef: 'ui:inputCheckbox', attributes: {
			'class': 'input_checkbox'
		} },
		'combobox': { componentDef: 'ui:inputText', attributes: {
			'class': 'input_section'
		} },
		'currency': { componentDef: 'ui:inputText', attributes: {
			'class': 'input_section'
		} },
		'datacategorygroupreference': { componentDef: 'ui:inputText', attributes: {
			'class': 'input_section'
		} },
		'date': {
			componentDef: 'ui:inputDate',
			attributes: {
				displayDatePicker: true
			}
		},
		'datetime': { componentDef: 'ui:inputDateTime', attributes: {
			'class': 'input_DateTime'
		} },
		'double': { componentDef: 'ui:inputNumber', attributes: {
			'class': 'input_Number'
		} },
		'email': { componentDef: 'ui:inputEmail', attributes: {
			'class': 'input_Email'
		} },
		'encryptedstring': { componentDef: 'ui:inputText', attributes: {
			'class': 'input_section'
		} },
		'id': { componentDef: 'ui:inputText', attributes: {
			'class': 'input_section'
		} },
		'integer': { componentDef: 'ui:inputNumber', attributes: {
			'class': 'input_Number'
		} },
		'multipicklist': { componentDef: 'ui:inputSelect', attributes: {
			'class': 'input_section multiple_section',
			'multiple': true
		} },
		'percent': { componentDef: 'ui:inputNumber', attributes: {
			'class': 'input_Number'
		} },
		'phone': { componentDef: 'ui:inputPhone', attributes: {
			'class': 'input_Phone'
		} },
		'picklist': {
			componentDef: 'ui:inputSelect',
			attributes: {
					'class': 'input_section'
				}
			},
		'reference': { componentDef: 'c:LookupField', attributes: {
			//'class': 'input_section'
		} },
		'string': { componentDef: 'ui:inputText', attributes: {
			'class': 'input_section'
		} },
		'textarea': { componentDef: 'ui:inputTextArea', attributes: {
			'class': 'input_Textarea'
		} },
		'time': { componentDef: 'ui:inputDateTime', attributes: {
			'class': 'input_DateTime'
		} },
		'url': { componentDef: 'ui:inputText', attributes: {
			'class': 'input_section'
		} },
		'output': { componentDef: 'ui:inputText', attributes: {
			'class': 'output_section',
			'disabled': true
		} }
	},

	createForm: function(cmp) {
		var fields = cmp.get('v.fields');
		var obj = cmp.get('v.record');
		var inputDesc = [];
		var fieldPaths = [];

		for (var i = 0; i < fields.length; i++) {
			var field = fields[i];
			var config = JSON.parse(JSON.stringify(this.configMap[field.type.toLowerCase()]));

			if (config) {
				config.attributes.label = field.label;
				config.attributes.required = field.required || field.DBRequired;
				config.attributes.value = obj[field.fieldPath];
				config.attributes.fieldPath = field.fieldPath;
				config.attributes['aura:id'] = field.fieldPath;

				if (config.componentDef === 'ui:inputSelect') {
					config.attributes.disabled = field.isDependent;

					var options = [];
					if(!field.required && !field.DBRequired && field.type !== 'MULTIPICKLIST') {
						options.push({label: $A.get("$Label.c.None"), value: "", selected: false});
					}

					var fieldValue = obj[field.fieldPath];
					if (!fieldValue && field.isDependent) {
						config.attributes.disabled = field.isDependent;
					}

					field.pickList.forEach(function(pickVal) {
						if (pickVal.isDefaultValue) {
							obj[field.fieldPath] = pickVal.value;
							fieldValue = pickVal.value;
						}

						if (fieldValue && pickVal.value === fieldValue) {
							options.push({
								value: pickVal.value,
								label: pickVal.label,
								selected: true
							});
						}
						else {
							options.push({
								value: pickVal.value,
								label: pickVal.label,
								selected: pickVal.isDefaultValue
							});
						}
					});

					if((field.required || field.DBRequired) && obj[field.fieldPath] == null && options[0]) {
						obj[field.fieldPath] = options[0].value;
					}
						
					config.attributes.options = options;
				}
				else if(config.componentDef === 'c:LookupField') {
					config.attributes = { 'lookupField' : field.fieldPath, 'objectAPIName' : field.parentObjectName};
					config.attributes.label = field.label;
					config.attributes.required = field.required || field.DBRequired;
				}

				if (field.isReadonly) {
					config.attributes.disabled = field.isReadonly;
				}
				inputDesc.push([config.componentDef, config.attributes]);
				fieldPaths.push(field.fieldPath);
			}
		}

		cmp.set('v.record', obj);

		$A.createComponents(inputDesc, function(cmps) {
			var inputToField = {};
			for (var m = 0; m < fieldPaths.length; m++) {
				cmps[m].addHandler('change', cmp, 'c.handleValueChange');
				inputToField[cmps[m].getGlobalId()] = fieldPaths[m];
			}
			cmp.set('v.form', cmps);
			cmp.set('v.inputToField', inputToField);
		});
	},

	handlePicklistChange: function(cmp, targetRecord, targetFieldPath) {
		var fieldsList = cmp.get('v.fields');
		var dependentLists = {};
		var parentConfig = {};

		fieldsList.forEach(function(el) {
			if ((el.type === 'PICKLIST' || el.type === 'MULTIPICKLIST') && el.isDependent) {
				dependentLists[el.controlField] = el;
			}
			if (el.fieldPath === targetFieldPath) {
				parentConfig = el;
			}
		});

		while (parentConfig && parentConfig.fieldPath in dependentLists) {
			parentConfig = dependentLists[parentConfig.fieldPath];
			if (parentConfig) {
				this.handleDependentPicklistChange(cmp, targetRecord, parentConfig);
				parentConfig = dependentLists[parentConfig.controlField];
			}
		}
	},

	handleDependentPicklistChange: function(cmp, targetRecord, childConfig) {
		if (!childConfig.isDependent) {
			return;
		}

		var isRequired = childConfig.required || childConfig.DBRequired;
		var options = [];

		if (!isRequired) {
			options.push({
				label: $A.get("$Label.c.None"),
				value: "",
				selected: false
			});
		}

		targetRecord[childConfig.fieldPath] = '';
		var parentValue = targetRecord[childConfig.controlField];
		var availValues = childConfig.dependentValues[parentValue] || [];

		childConfig.pickList.forEach(function(pickVal) {
			availValues.forEach(function(availValue) {
				if (availValue === pickVal.value) {
					options.push({
						value: pickVal.value,
						label: pickVal.label,
						selected: pickVal.isDefaultValue
					});

					if (pickVal.isDefaultValue) {
						targetRecord[childConfig.fieldPath] = pickVal.value;
					}
				}
			});
		});

		var selectedItems = options.filter(function(item) {
			return item.selected;
		});

		if (!selectedItems.length && options.length > 0) {
			options[0].selected = true;
			targetRecord[childConfig.fieldPath] = options[0].value;
		}

		cmp.find(childConfig.fieldPath).set('v.options', options);
		cmp.find(childConfig.fieldPath).set('v.disabled', options.length === (isRequired ? 0 : 1) );
	}
})