({
	init: function(cmp, event, helper) {
		helper.createForm(cmp);
	},

	handleValueChange: function(cmp, event, helper) {
		var inputToField = cmp.get('v.inputToField');
		var targetFieldPath = inputToField[event.getSource().getGlobalId()];
		var targetRecord = cmp.get('v.record');

		targetRecord[targetFieldPath] = event.getSource().get('v.value');
		helper.handlePicklistChange(cmp, targetRecord, targetFieldPath);

		cmp.set('v.record', targetRecord);
		$A.util.removeClass(cmp.find(event.getSource().getLocalId()), 'wrongField');
	},
	markFieldErrors : function(cmp, event, helper) {
		var fieldsWithErrors = event.getParam('fieldsWithErrors');
		for(var i=0, j=fieldsWithErrors.length; i<j; i++) {
			var field = cmp.find(fieldsWithErrors[i]);
			$A.util.addClass(field, 'wrongField');
		}
	},
	handleValueChangeInChildComponent : function(cmp, event, helper) {
		var fieldName = event.getParam('fieldName');
		var fieldValue = event.getParam('fieldValue');
		var targetRecord = cmp.get('v.record');

		targetRecord[fieldName] = fieldValue;
		cmp.set('v.record', targetRecord);
	}
})