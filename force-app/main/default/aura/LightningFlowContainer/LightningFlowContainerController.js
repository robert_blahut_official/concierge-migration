({
	statusChange : function (component, event) {
		if (event.getParam('status') === "FINISHED") {
			$A.util.addClass(component.find("flowPopup"), "display_false");
		}
	},
	doOpenFlow : function (component, event, helper) {
		var params = event.getParam('arguments');
		if (params) {
			var flowName = params.flowName;
			helper.openFlowPopup(component, flowName);
		}

		var isWEBKIT = $A.get('$Browser.isWEBKIT');
		var isAndroid = $A.get("$Browser.isAndroid"); 
		if(isAndroid && !isWEBKIT){
			$A.util.addClass(component.find("flowPopup"), "flowAndrBg");
		} else {
			$A.util.removeClass(component.find("flowPopup"), "flowAndrBg");
		}
		
	},
	handleOpenFlow : function (component, event, helper) {
		event.stopPropagation();
		var flowName = event.getParam("flowName");
		helper.openFlowPopup(component, flowName);

		return false;
	},
	closeFlowWindow : function (component, event, helper) {
		$A.util.addClass(component.find("flowPopup"), "display_false");
	}
})