({
	openFlowPopup: function(component, flowName) {
		if (flowName) {
			try {
				this.createLightningInput(component, function(flowCmp) {
					window.setTimeout($A.getCallback(function() {
						var flow = component.find("flow");
						/*var inputVariables = [{name : "knowledgeArticleId",
												type : "String",
												value: component.get("v.knowledgeRecord.knowledgeArticleId")
						}];
						flow.startFlow(flowName, inputVariables);*/
						flow.startFlow(flowName);
	
						$A.util.removeClass(component.find("flowPopup"), "display_false");
						var starRatingValue  = component.getEvent("ChangeSearchStaging");
						starRatingValue.setParams({ "score": 1024 });
						starRatingValue.fire();
					}), 200);
				});
			} catch(e) {
				console.error(e);
			}
		}
	},
	createLightningInput : function(component, postPorcessFnc) {
		var flowCmp = component.get('v.flowCmp');
		if (flowCmp) {
			flowCmp.destroy();
			component.set('v.flowCmp', null);
		}
		$A.createComponent(
			'lightning:flow', {
				"class": "flow-item",
				"aura:id": "flow",
				"onstatuschange": component.getReference("c.statusChange")
			},
			function(dynamicCaseBlock, status, errorMessage) {
				if (status === "SUCCESS") {
					component.set("v.flowCmp", dynamicCaseBlock);
					return postPorcessFnc(dynamicCaseBlock);
				}
			}
		);
	}
})