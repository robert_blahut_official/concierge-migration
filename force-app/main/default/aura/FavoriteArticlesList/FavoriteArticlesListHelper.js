({
	MAX_PART_NUMBER: 5,
	loadAllArticles: function(component) {
		var infoMessage = component.get('v.infoMessage');
		if (infoMessage !== undefined) {
			return;
		}

		var action = component.get("c.getAllFavoriteArticles");
		action.setParams({
			prefix: component.get("v.communityPrefix")
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {

				var allFavoriteArticlesString = response.getReturnValue();
				var allFavoriteArticles = JSON.parse(allFavoriteArticlesString) || [];
				component.set("v.allFavoriteArticles", allFavoriteArticles);
				component.set('v.part', 0);

				this.showMore(component);

				if (allFavoriteArticles.length === 0) {
					$A.util.addClass(component.find('favorite_descriptionID'), "zero_opacity");
					this.showMessage(component, $A.get("$Label.c.Empty_Favorites_Message"), true);
					this.hideSpinner(component.getSuper());
					component.set("v.displayLeftColumn", true);
					component.set("v.displayRightColumn", true);
				} else {
					$A.util.removeClass(component.find('work_space_2'), "work_space_opacity");
					$A.util.removeClass(component.find('message_section'), "active");
					$A.util.removeClass(component.find('favorite_descriptionID'), "zero_opacity");
					component.set("v.selectedArticle", allFavoriteArticles[0]);
					component.set("v.selectedArticleElement", allFavoriteArticles[0]);
					component.set("v.selectedArticlePosition", 0);
					this.createFavoriteArticleDetail(component);
				}

				var hideFeedbackButton = $A.get("e.c:HideFeedbackButton");
				if (hideFeedbackButton) {
					hideFeedbackButton.fire();
				}
				var hideSearchFunctionality = $A.get("e.c:HideSearchFunctionality");
				if (hideSearchFunctionality) {
					hideSearchFunctionality.fire();
				}
				var hideTickets = $A.get("e.c:HideTickets");
				if (hideTickets) {
					hideTickets.fire();
				}

				$A.util.removeClass(component.find('my_favorite_div'), 'hidden_div');
				if (window.innerWidth >= 1025) {
					$A.util.removeClass(component.find('favorites_descriptionId'), 'show');
					$A.util.removeClass(component.find('favorites_areaID'), 'hide');
					component.set("v.isMobileMode", true);
					component.set("v.showBtnBack", false);

				} else {
					component.set("v.isMobileMode", false);
					component.set("v.showFavorites", true);
				}
				window.addEventListener('resize', function() {
					if (window.innerWidth >= 1025) {
						component.set("v.showDescriptionBlock", false);
						component.set("v.showAreaBlock", true);
						component.set("v.isMobileMode", true);
						component.set("v.showBtnBack", false);

					} else {
						component.set("v.isMobileMode", false);
						if (component.get("v.showBtnBack") === false) {
							component.set("v.showFavorites", true);
						} else {
							component.set("v.showFavorites", false);
						}
					}
				});
			}
			this.hideSpinner(component.getSuper());
		});
		$A.enqueueAction(action);
	},

	deleteFromFavorites: function(component, event, helper) {
		var selectedArticle = component.get("v.selectedArticle");
		var action = component.get("c.deleteFavorite");
		action.setParams({
			'recordId': selectedArticle.knowledgeArticleId
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				component.set("v.selectedArticleElement", undefined);
				component.set("v.selectedArticlePosition", undefined);
				component.set("v.selectedArticle", undefined);
				this.loadAllArticles(component, helper);
				var isExpandable = $A.util.hasClass(component.find("favorite_descriptionID"), "display_true");
				if (isExpandable === true) {
					component.set("v.showBtnBack", true);
				} else {
					component.set("v.showBtnBack", false);
				}
			}
		});
		$A.enqueueAction(action);
	},

	setCurrentPrefixUrl: function(component) {
		if (component.get("v.isCommunity")) {
			component.set('v.communityPrefix', this.getCommuityPrefix());
		} else {
			component.set("v.communityPrefix", '');
		}
	},

	getFavoriteArticleDetailInformaton: function(component, event, helper, newResult) {
		var action = component.get("c.getFavoriteArticleDetailInformation");
		action.setStorable();
		action.setParams({
			"articleRecordStr": JSON.stringify(newResult),
			"prefix": component.get("v.communityPrefix")
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === 'SUCCESS') {
				component.set("v.selectedArticle", response.getReturnValue());
				this.createFavoriteArticleDetail(component);
			}
			this.hideSpinner(component.getSuper());
		});
		$A.enqueueAction(action);
	},

	createFavoriteArticleDetail: function(component) {
		var helper = this;
		var selectedArticle = component.get("v.selectedArticle");
		if (selectedArticle) {
			var articleContent = selectedArticle.body;
			var startIndex = articleContent.indexOf('{!ltngext.');
			var componentName = '';
			if (startIndex !== -1) {
				var endIndex = articleContent.indexOf('}');
				articleContent = articleContent.substring(startIndex, endIndex);
				componentName = articleContent.substring(10, articleContent.length);
			}
			$A.createComponent(
				"c:FavoriteDetails", {
					"articlesLength": component.get("v.allFavoriteArticles").length,
					"selectedArticle": component.get("v.selectedArticle"),
					"externalComponentName": componentName
				},
				function(favoritesBlock, status, errorMessage) {
					if (status === "SUCCESS") {
						component.set("v.displayLeftColumn", true);
						component.set("v.displayRightColumn", true);
						component.set("v.favoriteDetails", favoritesBlock);
					}
					helper.hideSpinner(component.getSuper());
				}
			);
		}
	},
	showMore: function(component) {
		var allFavoriteArticles = component.get('v.allFavoriteArticles') || [];
		var part = component.get('v.part');
		var articles = [];

		if (allFavoriteArticles.length >= this.MAX_PART_NUMBER * part) {
			var newPart = part + 1;
			articles = allFavoriteArticles.slice(0, this.MAX_PART_NUMBER * newPart);
			component.set('v.part', newPart);
		} else {
			articles = allFavoriteArticles;
		}
		component.set('v.articles', articles);
	},
	showRelatedArticle: function(component, event, href) {
		var error = $A.get("$Label.c.Message_Disable_Article_Link");
		var action = component.get("c.getRelatedArticleData");
		action.setParams({
			href: href,
			prefix: component.get("v.communityPrefix")
		});

		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var newResult = response.getReturnValue()[0];
				var liveAgentPage = response.getReturnValue()[1];
				var breadcrumbsOptions = component.get("v.breadcrumbsOptions") || [];

				if (event.getParam("outerText") || newResult.title) {
					breadcrumbsOptions.push({
						'label': (newResult.title || event.getParam("outerText")),
						'value': event.getParam("href")
					});
					component.set("v.breadcrumbsOptions", breadcrumbsOptions);
				}

				$A.createComponents([
					[
						"c:RelatedArticleDetails", {
							href: href,
							activeActions: newResult.setting,
							activeCaseLayoutRecord: newResult.settingCaseLayout,
							result: newResult,
							liveAgentPage: liveAgentPage,
							parentComponent: 'FavoriteDetails',
							breadcrumbsOptions: component.getReference("v.breadcrumbsOptions"),
							parentArticle: component.get("v.selectedArticle"),
							communityLoading: component.get("v.isCommunity"),
							isShowActions: false,
						}
					]
				], function(components, status, errorMessage) {
					if (status === "SUCCESS") {
						component.set("v.relatedArticle", components);
						$A.util.addClass(component.find('my_favorite_div'), 'hidden_div');
						$A.util.removeClass(component.find('total_div'), 'hidden_div');
					} else if (status === "ERROR") {
						this.showErrorMessage(component, error);
					}
				});
			} else if (state === "ERROR") {
				this.showErrorMessage(component, error);
			}
		});
		$A.enqueueAction(action);
		/*to do dymamic creation of the RelaLated Article*/
	},
	showErrorMessage: function(component, message) {
		var messageCmp = component.find('exceptionMessage');
		component.set('v.errorMessage', message);
		$A.util.removeClass(messageCmp, 'hidden_div');
		window.setTimeout($A.getCallback(function() {
			if (component.isValid()) {
				$A.util.addClass(messageCmp, 'hidden_div');
			}
		}), 2000);
	},

	showMessage: function(component, messageText, isInfo) {
		$A.util.removeClass(component.find('btn_back'), 'show');
		$A.util.removeClass(component.find('txt_Favorites'), 'show');
		component.set('v.messageText', messageText);
		if (isInfo) {
			$A.util.removeClass(component.find('message_section').getElement(), 'success_message');
			$A.util.addClass(component.find('message_section').getElement(), 'info_message');
			component.set('v.showInfoMessage', true);
		} else {
			$A.util.removeClass(component.find('message_section').getElement(), 'info_message');
			$A.util.addClass(component.find('message_section').getElement(), 'success_message');
			window.setTimeout(
				$A.getCallback(function() {
					$A.util.removeClass(component.find('message_section').getElement(), "active");
				}), 5000
			);
		}
		$A.util.addClass(component.find('message_section').getElement(), "active");
		$A.util.addClass(component.find('work_space_2').getElement(), "work_space_opacity");
	}
})