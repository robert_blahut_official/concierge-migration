({
	doInit: function(component, event, helper) {
		var hrefString = window.location.href;
		var findAppWord = hrefString.indexOf(".app");

		if (findAppWord > 0) {
			setTimeout(function() {
				var element = document.querySelector(".work_space");
				element.classList.add("app");
			}, 1000);
		}

		helper.showSpinner(component.getSuper());
		$A.util.removeClass(component.find('my_favorite_div'), 'hidden_div');
		component.set("v.selectedArticleElement", undefined);
		component.set("v.selectedArticlePosition", undefined);
		component.set("v.selectedArticle", undefined);
		helper.setCurrentPrefixUrl(component);
		helper.loadAllArticles(component);
		//helper.createFavoriteArticleDetail(component);
	},
	showMoreArticles: function(component, event, helper) {
		helper.showMore(component);
		//helper.showSpinner(component.getSuper());
	},
	hideFavorite: function(component, event, helper) {
		$A.util.addClass(component.find('my_favorite_div'), 'hidden_div');

		component.set("v.selectedArticleElement", undefined);
		component.set("v.selectedArticlePosition", undefined);
		component.set("v.selectedArticle", undefined);
	},
	showFavorite: function(component, event, helper) {
		component.set("v.selectedArticleElement", undefined);
		component.set("v.selectedArticlePosition", undefined);
		component.set("v.selectedArticle", undefined);
		helper.setCurrentPrefixUrl(component);
		helper.loadAllArticles(component);
		//helper.createFavoriteArticleDetail(component, event, helper);
	},

	showSelectedArticle: function(component, event, helper) {
		helper.showSpinner(component.getSuper());
		var newArticle = parseInt(event.target.id, 10);
		var newArticleElement = event.target;
		var previousArticle = component.get("v.selectedArticlePosition");
		var previousArticleElement = component.get("v.selectedArticleElement");

		if (newArticleElement.className && newArticle !== previousArticle) {
			while (newArticleElement.className.indexOf('table_cell') < 0) {
				newArticleElement = newArticleElement.parentElement;
			}

			$A.util.addClass(newArticleElement, "selected_favorite_style");
			if (previousArticleElement)
				$A.util.removeClass(previousArticleElement, "selected_favorite_style");
			component.set("v.selectedArticleElement", newArticleElement);
			component.set("v.selectedArticlePosition", newArticle);
			var selArticle = component.get("v.allFavoriteArticles")[newArticle];
			component.set("v.selectedArticle", selArticle); //["article"]
		} else {
			helper.hideSpinner(component.getSuper());
		}
		if (window.innerWidth < 1025) {
			/*$A.util.addClass(component.find('favorite_descriptionID'), 'show');
			$A.util.addClass(component.find('favorites_areaID'), 'hide');*/
			/*$A.util.addClass(component.find('btn_back'), 'show');*/
			component.set("v.showDescriptionBlock", true);
			component.set("v.showAreaBlock", false);
			component.set("v.showBtnBack", true);
			/*$A.util.addClass(component.find('txt_Favorites'), 'hide');*/
			component.set("v.showFavorites", false);
		}
		if (selArticle !== undefined) {
			if (selArticle.body === 'blankBody') {
				helper.getFavoriteArticleDetailInformaton(component, event, helper, component.get("v.selectedArticle"));
			} else {
				helper.createFavoriteArticleDetail(component);
			}
		}
		//helper.createFavoriteArticleDetail(component, event, helper);

	},

	backToTickets: function(component, event, helper) {
		if (window.innerWidth < 1025) {
			/*$A.util.removeClass(component.find('favorite_descriptionID'), 'show');
			$A.util.removeClass(component.find('favorites_areaID'), 'hide');*/
			/*$A.util.removeClass(component.find('btn_back'), 'show');*/
			component.set("v.showDescriptionBlock", false);
			component.set("v.showAreaBlock", true);
			component.set("v.showBtnBack", false);
			/*$A.util.removeClass(component.find('txt_Favorites'), 'hide');*/
			component.set("v.showFavorites", true);
		}
	},

	deleteFromFavorites: function(component, event, helper) {
		helper.showSpinner(component.getSuper());
		component.set("v.displayLeftColumn", false);
		component.set("v.displayRightColumn", false);
		helper.deleteFromFavorites(component);
		if (window.innerWidth < 1025) {
			component.set("v.showDescriptionBlock", false);
			component.set("v.showAreaBlock", true);
		} else {
			component.set("v.showDescriptionBlock", true);
			component.set("v.showAreaBlock", true);
		}
	},
	showMessage: function(component, event, helper) {
		$A.util.removeClass(component.find('btn_back'), 'show');
		$A.util.removeClass(component.find('txt_Favorites'), 'show');
			if (component.get("v.isCommunity")) {
				component.set('v.messageText', event.getParam('messageText'));
				var isInfo = event.getParam('isInfo');

				if (isInfo) {
					$A.util.removeClass(component.find('message_section'), 'success_message');
					/*$A.util.addClass(component.find('message_section'), 'info_message');*/
					component.set('v.showInfoMessage', true);
				} else {
					/*$A.util.removeClass(component.find('message_section'), 'info_message');*/
					$A.util.addClass(component.find('message_section'), 'success_message');
					window.setTimeout(
						$A.getCallback(function() {
							$A.util.removeClass(component.find('message_section'), "active");
						}), 5000
					);
				}
				$A.util.addClass(component.find('message_section'), "active");
			}
			$A.util.addClass(component.find('work_space_2'), "work_space_opacity");
	},
	showRelatedArticle: function(component, event, helper) {
		if (event.getParam("href")) {
			var href = event.getParam("href");
			helper.showRelatedArticle(component, event, href);
		} else {
			$A.util.removeClass(component.find('my_favorite_div'), 'hidden_div');
			$A.util.addClass(component.find('total_div'), 'hidden_div');
			component.set("v.relatedArticle", null);
		}
	}
})