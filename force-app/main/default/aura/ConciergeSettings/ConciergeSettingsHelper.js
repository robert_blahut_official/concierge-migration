({
	tabsVisibilitySetting: {},
	mapOfTypes: {},
	conciergeSettingsTypesMap: {
		'Case Close Status': 'text',
		'Case Origin': 'text',
		'Enable users to close their own case': 'boolean',
		'Background file name for App': 'text',
		'Background For App on Mobile Devices': 'text',
		'Application Name': 'text',
		'CompanyLogo': 'text',
		'Number of Articles Displayed in Search': 'number',
		'Update Interval': 'number',
		'Channel Type For Search': 'text',
		'Community Name': 'text',
		'Max Number of Favorite Articles': 'number',
		'Unique Tools Class Name': 'text',
		'Relevancy Score by Locale(Percent)': 'number',
		'Number of Cases Displayed': 'number',
		'Cases per Chunk': 'number',
		'Article Score Half Life (Days)': 'number',
		'Case Types': 'text',
		'Display Company Logo': 'boolean',
		'Display PDF button': 'boolean',
		'Allow Users to Print Articles in PDF': 'boolean',
		'Show Preview Popup Window': 'boolean',
		'Snippet Length': 'number',
		'Pass Search Term to Suggestions': 'boolean',
		'Hours New/Updated Flag is Displayed': 'number'
	},
	hideConciergeSettingsTypesList: [
		'Name of Case Status Indicator Field',
		'Unique Tools Class Name',
		'Populate Contact ID on Case',
		'Always Display Log a Ticket',
		'Search Log User Id (15 characters)',
		'Search Log Email Address',
		'Component Name for Live Agent',
		'Article Actions Display Order',
		'Concierge Tabs Visibility'
	],
	customLabelsSettingsMapping: {
		'Log a Ticket': "$Label.c.Log_a_Ticket",
		'Call': "$Label.c.Call",
		'Link': '$Label.c.Actions_Settings_Link',
		'Live Chat': '$Label.c.Actions_Settings_Live_Chat',
		'Email': '$Label.c.Actions_Settings_Email',
		'Flow': '$Label.c.Actions_Settings_Flow',
		'Skype': '$Label.c.HRS_Skype_Checkbox_Label'
	},
	loadConciergeSettings: function(component, event, helper) {
		this.showSpinner(component.getSuper());
		var action = component.get("c.getListOfConciergeSettings");
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var responseList = response.getReturnValue();
				var conciergeSettings = responseList[0];
				this.putSortList(component, responseList[2]);
				this.setTabsVisibility(component, responseList);

				var conciergeSettingsNew = [];
				for (var i = 0; i < conciergeSettings.length; i++) {
					if (conciergeSettings[i].Name === 'Background File Type') {
						component.set('v.fileTypePosition', i);
						component.set('v.isImage', conciergeSettings[i].cncrg__Value__c === 'Image');
						//break;
					}
					if (conciergeSettings[i].Name === 'Case Types') {
						component.set("v.displayAllCaseTypes", true);
						component.set('v.caseTypesPosition', i);
						helper.loadCaseTypesSettings(component);
					}
					conciergeSettings[i].type = this.conciergeSettingsTypesMap[conciergeSettings[i].Name];
					if (!this.hideConciergeSettingsTypesList.includes(conciergeSettings[i].Name)) {
						conciergeSettingsNew.push(conciergeSettings[i]);
					}
				}
				conciergeSettings = conciergeSettingsNew;
				var backgroundSettings = response.getReturnValue()[1];
				component.set("v.backgroundSettings", backgroundSettings);
				component.set("v.conciergeSettings", conciergeSettingsNew);
			} else if (state === "ERROR") {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {}
				} else {}
			}
			helper.hideSpinner(component.getSuper());
		});
		$A.enqueueAction(action);
	},
	putSortList: function(component, actionsList) {
		var optionsList = [];
		var valuesList = actionsList;
		for (var i = 0, j = actionsList.length; i < j; i++) {
			optionsList.push({
				value: actionsList[i],
				label: (this.customLabelsSettingsMapping[actionsList[i]] ? $A.get(this.customLabelsSettingsMapping[actionsList[i]]) : actionsList[i])
			});
		}
		component.set("v.sortOptions", optionsList);
		component.set("v.sortValues", valuesList);
	},
	loadCaseTypesSettings: function(component) {
		var action = component.get("c.getCaseTypesSettings");
		action.setCallback(this, function(response) {

			var state = response.getState();

			if (state === "SUCCESS") {
				var allCaseTypes = response.getReturnValue();
				component.set("v.allCaseTypes", allCaseTypes);
				var leftSideCaseTypes = [];
				var rightSideCaseTypes = [];

				var conciergeSettings = component.get("v.conciergeSettings");
				for (var i = 0; i < conciergeSettings.length; i++) {
					if (conciergeSettings[i].Name === 'Case Types') {
						component.set('v.caseTypesPosition', i);
					}
				}
				var existingCaseTypes = conciergeSettings[component.get("v.caseTypesPosition")];
				var self = this.mapOfTypes;
				self = new Map();
				allCaseTypes.forEach(function(item, j, listTypes) {
					self.set(item.name, item.label);
				});
				this.mapOfTypes = self;
				/*if(existingCaseTypes.cncrg__Value__c!== undefined){
					rightSideCaseTypes = existingCaseTypes.cncrg__Value__c.split(',');
					rightSideCaseTypes = rightSideCaseTypes.slice(0, -1);			
					component.set("v.rightSideCaseTypes",rightSideCaseTypes);
				}*/

				if (existingCaseTypes.cncrg__Value__c !== undefined) {
					var tempRightSideCaseTypes = existingCaseTypes.cncrg__Value__c.split(',');
					tempRightSideCaseTypes = tempRightSideCaseTypes.slice(0, -1);
					tempRightSideCaseTypes.forEach(function(item, k, tempListTypes) {
						if (self.get(item) !== undefined) {
							rightSideCaseTypes.push(self.get(item));
						}
					});
					component.set("v.rightSideCaseTypes", rightSideCaseTypes);
				}

				allCaseTypes.forEach(function(item, m) {
					if (rightSideCaseTypes.indexOf(item.label) === -1)
						leftSideCaseTypes.push(item.label);
				});


				component.set("v.leftSideCaseTypes", leftSideCaseTypes);
			}
		});
		$A.enqueueAction(action);
	},
	implementNewSettings: function(component) {
		var conciergeSettings = component.get("v.conciergeSettings") || [];
		var selectedCaseTypes = component.get("v.rightSideCaseTypes");
		var allCaseTypes = component.get("v.allCaseTypes");
		var caseTypesToSave = [];
		var mapOfTypes = this.mapOfTypes;
		var isError = false;

		allCaseTypes.forEach(function(item, i) {
			if (selectedCaseTypes.indexOf(mapOfTypes.get(item.name)) !== -1) {
				caseTypesToSave.push(item.name);
			}
		});

		for (var i = 0; i < conciergeSettings.length; i++) {
			if (conciergeSettings[i].Name === 'Case Types') {
				if (caseTypesToSave.length > 0) {
					conciergeSettings[i].cncrg__Value__c = caseTypesToSave.join() + ',';
				}
				else {
					conciergeSettings[i].cncrg__Value__c = '';
				}
			}
			if (this.conciergeSettingsTypesMap[conciergeSettings[i].Name] === 'number') {
				if (isNaN(Number(conciergeSettings[i].cncrg__Value__c))) {
					this.displayInfoMessage(component, "error", 'Following fields should have numeric values: ' + conciergeSettings[i].Name);
					isError = true;
					this.hideSpinner(component.getSuper());
					break;
				}
				if (Number(conciergeSettings[i].cncrg__Value__c) < 0 || Number(conciergeSettings[i].cncrg__Value__c) === 0 && conciergeSettings[i].Name != 'Hours New/Updated Flag is Displayed') {
					this.displayInfoMessage(component, "error", 'Following fields should have only positive numeric values: ' + conciergeSettings[i].Name);
					isError = true;
					this.hideSpinner(component.getSuper());
					break;
				} else if (Number(conciergeSettings[i].cncrg__Value__c) < 0 && conciergeSettings[i].Name === 'Hours New/Updated Flag is Displayed') {
					this.displayInfoMessage(component, "error", 'Following fields should have only positive numeric values: ' + conciergeSettings[i].Name);
					isError = true;
					this.hideSpinner(component.getSuper());
					break;
				}
			}
		}

		var settingsClone = conciergeSettings;
		if (this.tabsVisibilitySetting && this.tabsVisibilitySetting.Name === 'Concierge Tabs Visibility') {
			this.tabsVisibilitySetting.cncrg__Value__c = (component.get("v.visibleConciergeTabs") || []).join(';');
			settingsClone = JSON.parse(JSON.stringify(conciergeSettings))
			settingsClone.push(this.tabsVisibilitySetting);
		}

		if (!isError) {
			var totalObject = {
				'cs': settingsClone
			};

			var action = component.get("c.implementNewSettings");
			action.setParams({
				newSettings: JSON.stringify(totalObject)
			});
			action.setCallback(this, function(response) {
				var state = response.getState();
				if (state === "SUCCESS") {
					this.displayInfoMessage(component, "confirm", $A.get("$Label.c.All_Changes_Were_Saved_Successfully"));
				} else if (state === "ERROR") {
					var errors = response.getError();
					var message = "Unknown error";
					if (errors) {
						if (errors[0] && errors[0].message) {
							message = "Error message: " + errors[0].message;
						}
					}
					this.displayInfoMessage(component, "error", message);
				}
			});
			$A.enqueueAction(action);
		}
	},
	displayInfoMessage: function(component, severity, message) {
		var helper = this;
		$A.createComponent(
			"ui:message", {
				"severity": severity,
				"body": message,
				"closable": true
			},
			function(messageBlock, status, errorMessage) {
				if (status === 'SUCCESS') {
					component.set("v.message", messageBlock);
					$A.util.removeClass(component.find("confirmationTop"), "display_false");
					$A.util.removeClass(component.find("confirmationBottom"), "display_false");
					if (severity === 'confirm') {
						window.setTimeout(
							$A.getCallback(function() {
								if (component.isValid()) {
									$A.util.addClass(component.find("confirmationTop"), "display_false");
									$A.util.addClass(component.find("confirmationBottom"), "display_false");
								}
							}), 2000
						);
					}
				} else if (status === 'ERROR') {}
				helper.hideSpinner(component.getSuper());
			}
		);
	},
	setTabsVisibility: function(component, settings) {
		var helper = this;
		var allTabs = [];
		;(settings[3] || []).sort(function(a, b) {
			return a.cncrg__Order_Number__c 
				&& b.cncrg__Order_Number__c
				&& a.cncrg__Order_Number__c > b.cncrg__Order_Number__c;
		}).forEach(function(tab) {
			allTabs.push({
				value: tab.DeveloperName,
				label: tab.Label,
			});
		});
		component.set("v.conciergeTabs", allTabs);

		;(settings[0] || []).filter(function(setting) {
			return setting && setting.Name === 'Concierge Tabs Visibility';
		}).forEach(function(visibleTab) {
			helper.tabsVisibilitySetting = visibleTab;
			var valuesList = (visibleTab.cncrg__Value__c || '').split(';');
			component.set("v.visibleConciergeTabs", valuesList);
		});
	},
	changeActionsOrder: function(component, actionsList) {
		var action = component.get("c.changeActionsOrder");
		action.setParams({
			'actionsList': actionsList
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {} else if (state === "ERROR") {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {}
				} else {}
			}
		});
		$A.enqueueAction(action);
	}
})