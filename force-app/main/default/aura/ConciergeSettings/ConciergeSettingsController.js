({
	doInit: function(component, event, helper) {
		helper.loadConciergeSettings(component, event, helper);
	},
	goBack: function(component, event, helper) {
		$A.get("e.c:BackToSettings").fire();
	},
	saveChanges: function(component, event, helper) {
		helper.showSpinner(component.getSuper());
		helper.implementNewSettings(component);
	},
	implementNewSettingFirstTable: function(component, event, helper) {
		var tempName = event.target.id;
		var conciergeSettings = component.get("v.conciergeSettings");
		var type = helper.conciergeSettingsTypesMap[conciergeSettings[tempName].Name];
		var newValue = event.target.value;
		if (type === 'boolean') {
			newValue = event.target.checked;
		}
		else if (type === 'text') {
			newValue = event.target.value;
		}
		else if (type === 'number') {
			newValue = parseInt(newValue, 10);
		}

		conciergeSettings[tempName].cncrg__Value__c = newValue;
		component.set("v.conciergeSettings", conciergeSettings);
	},
	implementBackgroundSetting: function(component, event, helper) {
		var fieldName = 'Background file name for App';
		var selected = component.find(fieldName).get("v.value");
		var conciergeSettings = component.get("v.conciergeSettings");
		for (var i = 0; i < conciergeSettings.length; i++) {
			if (conciergeSettings[i].Name === fieldName) {
				conciergeSettings[i].cncrg__Value__c = selected;
				break;
			}
		}
		component.set("v.conciergeSettings", conciergeSettings);


	},
	implementBackgroundMobileSetting: function(component, event, helper) {
		var fieldName = 'Background For App on Mobile Devices';
		var selected = component.find(fieldName).get("v.value");
		var conciergeSettings = component.get("v.conciergeSettings");
		for (var i = 0; i < conciergeSettings.length; i++) {
			if (conciergeSettings[i].Name === fieldName) {
				conciergeSettings[i].cncrg__Value__c = selected;
				break;
			}
		}
		component.set("v.conciergeSettings", conciergeSettings);
	},
	implementChannelSettings: function(component, event, helper) {
		var fieldName = 'Channel Type For Search';
		var selected = component.find(fieldName).get("v.value");
		var conciergeSettings = component.get("v.conciergeSettings");
		for (var i = 0; i < conciergeSettings.length; i++) {
			if (conciergeSettings[i].Name === fieldName) {
				conciergeSettings[i].cncrg__Value__c = selected;
				break;
			}
		}
		component.set("v.conciergeSettings", conciergeSettings);
	},
	implementSnippetSettings: function(component, event, helper) {
		var fieldName = 'Text to Display in Article Preview';
		var selected = component.find(fieldName).get("v.value");
		var conciergeSettings = component.get("v.conciergeSettings");
		for (var i = 0; i < conciergeSettings.length; i++) {
			if (conciergeSettings[i].Name === fieldName) {
				conciergeSettings[i].cncrg__Value__c = selected;
				break;
			}
		}
		component.set("v.conciergeSettings", conciergeSettings);
	},
	leftSideCaseTypeChanged: function(component, event, helper) {
		var highlightedCases = component.find('leftCaseTypes').get("v.value");
		var highlightedCasesArray = [];
		if (highlightedCases.indexOf(';') !== -1) {
			highlightedCasesArray = highlightedCases.split(';');
		} else {
			highlightedCasesArray.push(highlightedCases);
		}
		component.set("v.highlightedCaseTypes", highlightedCasesArray);
	},
	rightSideCaseTypeChanged: function(component, event, helper) {
		var highlightedCases = component.find('rightCaseTypes').get("v.value");
		var highlightedCasesArray = [];
		if (highlightedCases.indexOf(';') !== -1) {
			highlightedCasesArray = highlightedCases.split(';');
		} else {
			highlightedCasesArray.push(highlightedCases);
		}
		component.set("v.highlightedCaseTypes", highlightedCasesArray);
	},
	addSelectedCategories: function(component, event, helper) {
		var highlightedCases = component.get("v.highlightedCaseTypes");
		var leftCaseTypes = component.get("v.leftSideCaseTypes");
		var rightSideCaseTypes = component.get("v.rightSideCaseTypes");
		var remainingLeftSideCaseTypes = [];
		var setOfRightCaseTypes = new Set(rightSideCaseTypes);
		if (highlightedCases.length > 0) {

			highlightedCases.forEach(function(item) {
				setOfRightCaseTypes.add(item);
			});
			var rightSideCaseTypesArray = [];
			setOfRightCaseTypes.forEach(function(v) {
				rightSideCaseTypesArray.push(v);
			});
			component.set("v.rightSideCaseTypes", rightSideCaseTypesArray);
			leftCaseTypes.forEach(function(item, i) {
				if (highlightedCases.indexOf(item) === -1) {
					remainingLeftSideCaseTypes.push(item);
				}
			});
			component.set("v.leftSideCaseTypes", remainingLeftSideCaseTypes);
			component.set("v.highlightedCaseTypes", []);
		}
		var a = component.get('c.rightSideCaseTypeChanged');
		$A.enqueueAction(a);
	},
	removeSelectedCategories: function(component, event, helper) {

		var highlightedCases = component.get("v.highlightedCaseTypes");
		var selectedCases = component.get("v.rightSideCaseTypes");
		var leftCaseTypes = component.get("v.leftSideCaseTypes");
		var remainingRightSideCaseTypes = [];
		var setOfLeftCaseTypes = new Set(leftCaseTypes);
		if (highlightedCases.length > 0) {

			highlightedCases.forEach(function(item) {
				setOfLeftCaseTypes.add(item);
			});

			var leftSideCaseTypesArray = [];
			setOfLeftCaseTypes.forEach(function(v) {
				leftSideCaseTypesArray.push(v);
			});
			component.set("v.leftSideCaseTypes", leftSideCaseTypesArray);

			selectedCases.forEach(function(item, i) {
				if (highlightedCases.indexOf(item) === -1) {
					remainingRightSideCaseTypes.push(item);
				}
			});
			component.set("v.rightSideCaseTypes", remainingRightSideCaseTypes);
			component.set("v.highlightedCaseTypes", []);
		}
		var a = component.get('c.leftSideCaseTypeChanged');
		$A.enqueueAction(a);
	},
	handleChange: function(cmp, event, helper) {
		var selectedOptionValue = event.getParam("value");
		helper.changeActionsOrder(cmp, selectedOptionValue.toString());
	}
})