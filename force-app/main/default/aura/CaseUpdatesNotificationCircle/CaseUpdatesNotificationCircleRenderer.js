({
	rerender : function(component, helper){
        this.superRerender();
        if (helper.listOfActiveFlags['CLEAR_FILE']) {
			component.find('inputFile').getElement().value = null;
			helper.listOfActiveFlags['CLEAR_FILE'] = undefined;
        }
    }
})