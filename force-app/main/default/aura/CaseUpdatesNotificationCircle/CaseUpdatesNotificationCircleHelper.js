({
	listOfActiveFlags: {},
	setIntervalForUpdates: function(component) {
		var interval = component.get("v.updatesInterval");
		var that = this;
		that.getUpdatesCount(component);
		window.setInterval(
			$A.getCallback(function() {
				that.getUpdatesCount(component);
			}), interval
		);
	},

	getUpdateIntervalHelper: function(component) {
		var action = component.get("c.getConciergeSetting");
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === 'SUCCESS') {
				component.set("v.updatesInterval", parseInt(response.getReturnValue()[0], 10));
				component.set("v.contactId", response.getReturnValue()[1]);
				this.setIntervalForUpdates(component);
				this.getCircleOffset(component);
			}
		});
		$A.enqueueAction(action);
	},

	getUpdatesCount: function(component) {
		var action = component.get("c.getCaseUpdatesCount");
		if (action) {
			action.setParams({
				"contactId": component.get("v.contactId")
			});
			action.setCallback(this, function(response) {
				var state = response.getState();
				if (state === 'SUCCESS') {
					component.set("v.updatedCaseNumber", response.getReturnValue());
				}
			});
			$A.enqueueAction(action);
		}
	},

	getCircleOffset: function(cmp) {
		var action = cmp.get("c.getNotificationCircleTabNumber");
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === 'SUCCESS') {

				if (cmp.get("v.myTicketPosition") !== response.getReturnValue()) {
					cmp.set("v.myTicketPosition", response.getReturnValue());
				}

				if (!cmp.get("v.useCustomNavBar")) {
					window.setTimeout($A.getCallback(function() {
						var hiddenBlock = cmp.find('hiddenBlock');
						var myTicketsItem = cmp.find('myTicketsItem');
						if (myTicketsItem && myTicketsItem.getElement()) {
							var width = myTicketsItem.getElement().getBoundingClientRect().width;
							if (width > 0) {
								var defaultNavItemsOffset = 35;
								var notificationCircleOffset = 90;
								notificationCircleOffset += width - defaultNavItemsOffset / 2;
								cmp.set("v.offset", notificationCircleOffset);
								if (notificationCircleOffset) {
									cmp.set('v.offsetCalculated', true);
								}
							}
						}
					}), 1500);
				}
			}
		});
		$A.enqueueAction(action);
	}
})