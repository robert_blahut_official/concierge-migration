({
    doInit : function(component, event, helper) {       
		helper.getCurrentUrl(component);
	},

    actionChat: function (component, event, helper) {
		
		var changeButtonId = event.getParam("changeButtonId");
		if(changeButtonId) {
			component.set("v.buttonId", event.getParam("buttonId"))
			helper.getCurrentUrl(component);
		}
		else {
			var pageOrigin = component.get('v.liveAgentPageOrigin');
			if(pageOrigin === undefined)
				pageOrigin = helper.getOrigin(component);
			component.find('liveAgentFrame').getElement().contentWindow.postMessage('openChat', pageOrigin);
			if(!component.get("v.useLiveagentStartChatWithWindowMethod")) {
				$A.util.removeClass(component.find('LAMainContainer'), 'hidden_div');
			}
		}
    },

    hideDetails : function (component, event, helper) {
        var pageOrigin = component.get('v.liveAgentPageOrigin');
        if(pageOrigin === undefined)
            pageOrigin = helper.getOrigin(component);
        component.find('liveAgentFrame').getElement().contentWindow.postMessage('rejectChat', pageOrigin);
        component.set('v.liveAgentPageUrl', component.get('v.liveAgentPageUrl'));
        $A.util.addClass(component.find('LAMainContainer'), 'hidden_div');
    },
})