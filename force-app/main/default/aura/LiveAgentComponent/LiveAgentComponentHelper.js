({
	getCurrentUrl : function(component) {
		component.set("v.liveAgentPageUrl", null);
		var pageOrigin = this.getOrigin(component);
		//19.01 - Update URL. Create method for getting sfbaseurl and use this full url like src in iframe
		var liveAgentPageUrl = component.get('v.liveAgentPage');
		var buttonId = component.get('v.buttonId');
		if(component.get("v.communityLoading")){
			var currUrl;
			try {
			   	currUrl = window.location.pathname;
			}
			catch (e) {
			   	currUrl = $A.get('$Resource.Media');
			}
			var prefixArray = currUrl.split('/');
			if(prefixArray[2]!=='')
				component.set("v.liveAgentPageUrl", '/'+prefixArray[1]+ liveAgentPageUrl + '?origin=' + pageOrigin + '&buttonId=' + buttonId);
			else
				component.set("v.liveAgentPageUrl", liveAgentPageUrl + '?origin=' + pageOrigin + '&buttonId=' + buttonId);
		} else {
			component.set("v.liveAgentPageUrl", pageOrigin + liveAgentPageUrl + '?origin=' + pageOrigin + '&buttonId=' + buttonId);
		}
	},

	getOrigin: function (component) {
		var pageOrigin;
        try {
            pageOrigin = window.location.origin;
        }
        catch (e) {
            pageOrigin = '';
            if(component.find('liveAgentFrame').getElement() !== null) {
                var parts = component.find('liveAgentFrame').getElement().baseURI.split('/');
                pageOrigin = parts[0] + '//' + parts[2];
            }
        }
        return pageOrigin;
    }
})