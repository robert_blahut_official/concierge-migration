({
	doInit : function(component, event, helper) {	
		if(component.get("v.isCommunity") === true) {		
			$A.util.removeClass(component.find('total_div'), 'total_div');
			$A.util.removeClass(component.find('no_drop'), 'total_div');
		}
	},

	showEmailForm : function(component, event, helper) {
		component.set("v.emailAddress", event.getParam("emailAddress"));
		$A.util.removeClass(component.find('total_div'), 'hidden_div');
	},

	closePopUp : function(component, event, helper) {
		helper.closePopup(component);
	},

	checkFields : function(component, event, helper) {
		
		var recipientName = component.get("v.recipientName");
		var emailAddress = component.get("v.emailAddress");
		var subject = component.get("v.subject");

		if(recipientName !== '' && recipientName !== null){
			component.set("v.nameNotEmpty", true);
			if(emailAddress !== '' && emailAddress !== null){
				if(typeof emailAddress !== 'undefined'){
					component.set("v.addressNotEmpty", true);
				}
				component.set("v.addressNotEmpty", true);
			}else{
				component.set("v.addressNotEmpty", false);
			}
			if(subject !== '' && subject !== null){
				if(typeof subject !== 'undefined'){
					component.set("v.subjectNotEmpty", true);
				}
			}else{
				component.set("v.subjectNotEmpty", false);
			}
		}else{
			component.set("v.nameNotEmpty", false);
		}
	}
})