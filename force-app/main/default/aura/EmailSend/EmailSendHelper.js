({
	closePopup : function(component) {
		$A.util.addClass(component.find('total_div'), 'hidden_div');
	
		if(component.get("v.isCommunity") === true) {
			var urlEvent = $A.get("e.force:navigateToURL");
			urlEvent.setParams({
				 "url": "/"
			});
			urlEvent.fire();
		}
	}
})