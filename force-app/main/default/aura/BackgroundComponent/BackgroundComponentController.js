({
    doInit : function(component, event, helper) {
        var isAndroid = $A.get("$Browser.isAndroid");
        component.set('v.isAndroid', isAndroid);
        
        getImage(component);
        window.addEventListener(
            "orientationchange", 
            $A.getCallback(function() {
                getImage();
            })
        );
        function getImage(){
            //var isMobile = screen.width < screen.height;
            var isMobile = window.innerWidth < window.innerHeight; 
            var isMobile = helper.isMobileDevice();
            var bgName = isMobile ? component.get("v.bgMobileName") : component.get("v.bgName");
            if(bgName !== '--None--') {
                helper.implementBG(component, bgName, false);
                
            } else {
                helper.getBGUrl(component);
            }
        }
        var appName = component.get("v.appName");
        if (!appName || appName === null || appName.trim() === '') {
            helper.getApplicationName(component);
        }
    },
    handleRouteChange : function(component, event, helper) {
        $A.util.removeClass(component.find('message_section').getElement(), "active");
    },
    showMessage : function(component, event, helper) {
        component.set('v.messageText',event.getParam('messageText'));
        var isInfo = event.getParam('isInfo');
        var isError = event.getParam('isError');
        if(!isError){
            if (isInfo) {
                $A.util.removeClass(component.find('message_section').getElement(), 'success_message');
                $A.util.addClass(component.find('message_section').getElement(), 'info_message');
            } else {

                $A.util.removeClass(component.find('message_section').getElement(), 'info_message');
                $A.util.addClass(component.find('message_section').getElement(), 'success_message');
                $A.util.removeClass(component.find('message_section').getElement(), 'error_message');
                window.setTimeout(
                    $A.getCallback(function() {
                        $A.util.removeClass(component.find('message_section').getElement(), "active");
                    }), 5000
                );
            }
            $A.util.addClass(component.find('message_section').getElement(), "active");
        } else {
            $A.util.removeClass(component.find('message_section').getElement(), 'info_message');
            $A.util.addClass(component.find('message_section').getElement(), 'error_message');
            $A.util.addClass(component.find('message_section').getElement(), "active");
            window.setTimeout(
                $A.getCallback(function() {
                    $A.util.removeClass(component.find('message_section').getElement(), "active");
                    $A.util.removeClass(component.find('message_section').getElement(), 'error_message');
                }), 5000
            );
            $A.util.addClass(component.find('message_section').getElement(), "active");
        }
    },
    bodyStatus: function(component, event, helper) {
        $A.util.removeClass(component.find('message_section').getElement(), "active");
    },
})