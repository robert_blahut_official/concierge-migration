({
	getBGUrl: function(component) {
		var isMobile = window.innerWidth < window.innerHeight;
		var isMobile = this.isMobileDevice();
		var backgroundWrapper = component.get('v.backgroundWrapper');
		if (backgroundWrapper) {
			//var isLandscape = screen.width > screen.height;
			var isLandscape = window.innerWidth > window.innerHeight;
			this.implementVideoBG(component, backgroundWrapper.FileLocation + (isLandscape ? backgroundWrapper.LandscapeDocumintId : backgroundWrapper.PortraitDocumintId), true);
		} else {
			this.implementBG(component, null, isMobile);
		}
		/*var action = component.get("c.getBGUrl");
		action.setParams({ isMobile : isMobile });
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var bgInfo = response.getReturnValue();
				this.implementBG(component, bgInfo)
			} else if (state === "ERROR") {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
					}
				} else {
				} 
			} 
		});
		$A.enqueueAction(action);*/
	},
	implementVideoBG: function(component, urlToVideo, isImage) {
		$A.createComponent(
			"c:VideoBackground", {
				"urlToFile": urlToVideo,
				"isImage": isImage
			},
			function(videoBlock, status, errorMessage) {
				if (status === "SUCCESS") {
					var videoSpan = component.find("video");
					videoSpan.set("v.body", videoBlock);
				} else if (status === "INCOMPLETE") {} else if (status === "ERROR") {}
			}
		);

		window.setTimeout(
			$A.getCallback(function() {
				component.set('v.isVisible', true);
				// $A.util.removeClass(component.find('total_div'), 'display_false');
			}), 1500
		);
	},
	implementBG: function(component, bgName, isMobile) {

		try {
			var action = component.get("c.getBackgroundSettings");
			action.setParams({
				bgName: bgName,
				isMobile: isMobile
			});
			action.setCallback(this, function(response) {
				var state = response.getState();
				if (state === "SUCCESS") {
					var result = response.getReturnValue();
					if (result) {
						if (result.IsStaticResurces) {
							var urlToVideo;
							if (result.FileLocation.indexOf('/') !== -1) {
								var brWayArray = result.FileLocation.split('/');
								urlToVideo = $A.get('$Resource.' + brWayArray[0]);
								for (var i = 1; i < brWayArray.length; i++) {
									urlToVideo += '/' + brWayArray[i];
								}
							} else {
								urlToVideo = $A.get('$Resource.' + result.FileLocation);
							}
							var bgType = result.BackgroundFileType.toLowerCase();
							this.implementVideoBG(component, urlToVideo, bgType === 'image');
						} else {

							var currUrl;
							try {
								currUrl = window.location.pathname;
							} catch (e) {
								currUrl = $A.get('$Resource.Media');
							}
							var prefixArray = currUrl.split('/');
							var prefix = '';
							if (prefixArray[1] !== 's' && prefixArray[2] === 's')
								prefix = '/' + prefixArray[1];
							result.FileLocation = prefix + '/servlet/servlet.FileDownload?file=';
							component.set('v.backgroundWrapper', result);
							var isLandscape = screen.width > screen.height;
							this.implementVideoBG(component, result.FileLocation + (isLandscape ? result.LandscapeDocumintId : result.PortraitDocumintId), true);
						}
					}
				} else if (state === "ERROR") {
					var errors = response.getError();
					if (errors) {
						if (errors[0] && errors[0].message) {}
					} else {}
				}
			});
			$A.enqueueAction(action);
		} catch (e) {
			console.log(e);
		};
	},
	isMobileDevice: function() {
		//return (window.navigator.maxTouchPoints || 'ontouchstart' in document);
		try {
			var device = $A.get("$Browser.formFactor");
			return device === 'DESKTOP' ? false : true;
		} catch (e) {
			return false;
		}
	},
	getApplicationName: function(component) {
		var action = component.get("c.getApplicationName");
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var appName = response.getReturnValue();
				if (appName && appName !== null && appName.trim() !== '') {
					component.set('v.appName', appName);
				}
			} else if (state === "ERROR") {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {}
				} else {}
			}
		});
		$A.enqueueAction(action);
	}
})