({
    doInit : function(component, event, helper) {
        helper.getArticleWrapper(component);
    },

    rArticle : function (component, event, helper) {
        helper.sendArticleOnReview(component);
    },

    editArticle: function (component, event, helper) {
        helper.editArticle(component);
    }
})