({
    getArticleWrapper : function(component) {
        var recordId = component.get('v.recordId');
        var action = component.get("c.getArticle");
        action.setParams({
            "recordId" : recordId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                var record = result[0];
                component.set('v.result', record);
                component.set('v.isMasterLanguage', record.isLike);
                component.set('v.applicationName', result[1]);
                this.createContent(component, record.body);
            } else if (state === "ERROR") {
                var errors = response.getError();
            
                if (errors) {
                    if (errors[0] && errors[0].message) {
                    }
                } else {
                }
            }
        });
        $A.enqueueAction(action);
    },

    createContent: function (component, content) {
        var htmlBody = this.replaceAll(this.replaceAll(content, '<iframe', '<div class="video-container"><iframe'), '</iframe>', '</iframe></div>');
        $A.createComponent("aura:unescapedHtml", {"tag":"div", "value": htmlBody},
            function(newCmp, status) {
                var body = component.get("v.body");
                body.push(newCmp);
                component.set("v.body", body);
            }
        );
    },

    replaceAll : function(target, search, replacement) {
        return target.replace(new RegExp(search, 'g'), replacement);
    },

    sendArticleOnReview: function (component) {
        var recordId = component.get('v.recordId');
        var action = component.get("c.reviewArticle");
        action.setParams({
            "recordId" : recordId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                    }
                } else {
                }
            }
        });
        $A.enqueueAction(action);
        setTimeout(function () {
            var closeWindow = $A.get("e.force:closeQuickAction")
            if(closeWindow)
                closeWindow.fire();
        }, 100);
    },

    editArticle: function (component) {
        var recordId = component.get('v.recordId');
        var cur = component.get('v.result');
        if(cur.isExternalArticle) {
            return;
        }
        var action = component.get("c.moveToEdit");
        action.setParams({
            "recordId" : recordId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                window.location.href = response.getReturnValue();
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                    }
                } else {
                }
            }
            var closeWindow = $A.get("e.force:closeQuickAction");
            if(closeWindow)
                closeWindow.fire();
        });
        $A.enqueueAction(action);
    }
})