({
	doInit : function(cmp, event) {
		for (var i = 0; i < 5; i++){
			cmp.set("v.starImage"+i, $A.get('$Resource.cncrg__Media') + "/star_border_icon.svg");
		}
  },

  reloadStarRating : function(cmp, event, helper){
  	for (var i = 0; i < 5; i++){
		cmp.set("v.starImage"+i, $A.get('$Resource.cncrg__Media') + "/star_border_icon.svg");
    }
  },

  handleCaseScoreChange : function(cmp, event, helper) {
      var previousAction =  cmp.get("v.previousAction");
      if(previousAction === "applyStarRating"){
      	  cmp.set("v.previousAction", "handleCaseScoreChange");
          return;
      }
      cmp.set("v.previousAction", "handleCaseScoreChange");

      var currentCase = cmp.get("v.currentCase");
      var caseScore = currentCase.cncrg__Score__c;
	  var i;

      if(caseScore > 0){
          for (i = 0; i < caseScore; i++){
              var index = "star"+i;
              var cmpTarget = cmp.find(index);
              $A.util.addClass(cmpTarget, 'changeMe');
			  cmp.set("v.starImage"+i, $A.get('$Resource.cncrg__Media') + "/star_icon.svg");
          }
      }else{
          for (i = 0; i < 5; i++){
			cmp.set("v.starImage"+i, $A.get('$Resource.cncrg__Media') + "/star_border_icon.svg");
          }
      }
  },

  applyStarRating : function(cmp, event, helper) {
      cmp.set("v.previousAction", "applyStarRating");
      var id = event.target.id;

      for (var i = 0; i < 5; i++){
          var index = "star" + i;
          var cmpTarget = cmp.find(index);
          $A.util.removeClass(cmpTarget, 'changeMe');
		  cmp.set("v.starImage"+i, $A.get('$Resource.cncrg__Media') + "/star_border_icon.svg");
      }
      for (var j = 0; j < id; j++){
          var index1 = "star" + j;
          var cmpTarget1 = cmp.find(index1);
          $A.util.addClass(cmpTarget1, 'changeMe');
		  cmp.set("v.starImage"+j, $A.get('$Resource.cncrg__Media') + "/star_icon.svg");
      }

      var starRatingValue  = cmp.getEvent("sendStarRating");
      starRatingValue.setParams({ "starRatingValue": id });
      starRatingValue.fire();

      window.setTimeout(
            $A.getCallback(function() {
				var setNextWindow;
                if (cmp.isValid()) {
                    if(id >= '3' && id <= '5'
                    ){
                        setNextWindow  = cmp.getEvent("sendNextWindow");
                        helper.setActiveWindow(setNextWindow, "7");
                    } else{
                        setNextWindow  = cmp.getEvent("sendNextWindow");
                        helper.setActiveWindow(setNextWindow, "1");
                    }

                }
            }), 700
        );

  },
  setNextStep : function(cmp, event, helper) {
      var setNextWindow  = cmp.getEvent("sendNextWindow");
      helper.setActiveWindow(setNextWindow, "1");

  },
  hideStarRating : function(cmp, event, helper) {
      var setNextWindow  = cmp.getEvent("sendNextWindow");
      helper.setActiveWindow(setNextWindow, "4");

  },

 })