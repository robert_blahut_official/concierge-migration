({
	loadCasesSettingsAndLoadCases: function(component) {
		var helper = this;
		var action = component.get("c.loadCasesSettings");
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {

				var allSettings = response.getReturnValue();
				component.set("v.defaultNumberCases", parseInt(allSettings.defaultNumberCases, 10));
				component.set("v.numberChunksCase", parseInt(allSettings.numberChunksCase, 10));
				component.set("v.caseCommentComponentName", allSettings.dynamicComponentName);
				component.set("v.allSettings", allSettings);

				helper.loadMoreCases(component, parseInt(allSettings.defaultNumberCases, 10), true, false);
			} else {
				helper.hideSpinner(component.getSuper());
			}

			if (window.innerWidth >= 1025) {
				$A.util.removeClass(component.find('case_descriptionId'), 'show');
				$A.util.removeClass(component.find('cases_areaID'), 'hide');
				component.set("v.isMobileMode", true);
				component.set("v.showBtnBack", false);
				$A.util.removeClass(component.find('txt_tickets'), 'hide');

			} else {
				component.set("v.isMobileMode", false);
			}
			window.addEventListener('resize', function() {
				if (window.innerWidth >= 1025) {
					$A.util.removeClass(component.find('case_descriptionId'), 'show');
					$A.util.removeClass(component.find('cases_areaID'), 'hide');
					component.set("v.isMobileMode", true);
					component.set("v.showBtnBack", false);
					$A.util.removeClass(component.find('txt_tickets'), 'hide');

				} else {
					component.set("v.isMobileMode", false);
				}
			})
		});
		$A.enqueueAction(action);
	},

	markUpdatesAsRead: function(component, caseRecord) {
		var action = component.get("c.markUpdatesAsRead");
		action.setParams({
			"caseWrapperJSON": JSON.stringify(caseRecord)
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "ERROR") {
				var errors = response.getError();
				connsole.error(errors);
			}
		});
		$A.enqueueAction(action);
	},

	getUpdatedCasesNumber: function(component) {
		var action = component.get("c.getCaseUpdatesCount");
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === 'SUCCESS') {
				component.set("v.updatedCaseNumber", response.getReturnValue());
			}
		});
		$A.enqueueAction(action);
	},

	selectFirstCase: function(component) {
		var helper = this;
		var allCases = component.get("v.allCases");
		helper.showSpinner(component.getSuper());
		component.set("v.selectedCase", undefined);

		if (allCases.length > 0) {
			var caseRecord = allCases[0];
			//caseRecord["containUpdates"] = false;
			component.set("v.selectedCase", caseRecord["c"]);
			component.set("v.selectedCasePosition", 0);

			$A.get("e.c:ShowCaseDetails").setParams({
				"selectedCase": caseRecord["c"]
			}).fire();

			$A.util.removeClass(component.find('work_space_2').getElement(), "work_space_opacity");
			$A.util.removeClass(component.find('message_section').getElement(), "active");

			helper.hideSpinner(component.getSuper());
			if (caseRecord.c.cncrg__Unread_Update_for_Contact__c === true) {
				setTimeout(
					$A.getCallback(function() {
						var caseUpdates = component.get("c.markUpdatesAsRead");
						caseRecord["containUpdates"] = false;
						caseUpdates.setParams({
							"caseWrapperJSON": JSON.stringify(caseRecord)
						});

						caseUpdates.setCallback(helper, function(response) {
							var status = response.getState();
							if (status === 'SUCCESS') {
								var elFind = [];
								elFind = component.find("element");
								if ($A.util.isArray(elFind)) {
									elFind.forEach(function(tdElement, i) {
										if (tdElement.getElement() != null) {
											if ((tdElement.getElement().className === 'div1' || tdElement.getElement().className === 'div1 ') &&
												(tdElement.getElement().firstChild.clientHeight > tdElement.getElement().clientHeight)) {
												var position = tdElement.getElement().id;
												$A.util.addClass(tdElement.getElement(), 'divEllipsis');
												allCases[position].showDots = true;
											}
										}
									})
									component.set('v.allCases', allCases);
								}

								helper.getUpdatedCasesNumber(component);
							}
						});

						$A.enqueueAction(caseUpdates);
				}), 5000);
			}

		} else {
			helper.hideSpinner(component.getSuper());
			helper.showMessage(component, $A.get("$Label.c.Empty_Tickets_Message"), true);
		}

		var hideSearchFunctionality = $A.get("e.c:HideSearchFunctionality");
		if (hideSearchFunctionality) {
			hideSearchFunctionality.fire();
		}
		var hideFavorite = $A.get("e.c:HideFavorite");
		if (hideFavorite) {
			hideFavorite.fire();
		}
		$A.util.removeClass(component.find('my_tikets_div'), 'hidden_div');
	},

	loadMoreCases: function(component, rows, isInitialGetting, refreshCaseView) {
		var helper = this;

		if (!isInitialGetting) {
			helper.showSpinner(component.getSuper());
		}

		var action = component.get("c.getMoreCases");
		action.setParams({"rows": (parseInt(rows, 10) + 1) });
		action.setCallback(helper, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var allCases = response.getReturnValue() || [];

				component.set("v.showShowMore", (allCases.length > component.get("v.defaultNumberCases")) );
				component.set("v.allCases", allCases);
				if (allCases && allCases.length > 0) {
					if (isInitialGetting) {
						var ticketId = helper.getUrlParam(this.QUERY_PARAMS.CASE_ID) || '';
							helper.loadSelectedCase(component, ticketId, function(selectedCase) {

								if (selectedCase) {
									helper.loadCaseFromURL(component, ticketId);
								} else {
									helper.selectFirstCase(component);
								}
							});
					}

					if (refreshCaseView) {
						helper.selectFirstCase(component);
					}
				}

				helper.hideSpinner(component.getSuper(), 500);
				if (allCases.length === 0) {
					helper.showMessage(component, $A.get("$Label.c.Empty_Tickets_Message"), true);
				}
			} else {
				helper.hideSpinner(component.getSuper());
			}
		});
		$A.enqueueAction(action);
	},

	loadSelectedCase: function(component, caseId, callback) {
		if (!caseId) {
			return callback();
		}

		var allCases = component.get("v.allCases");
		var selectedCase = allCases.filter(function(theCase) {
			return theCase.c.Id === caseId;
		});
		if (selectedCase.length > 0) {
			return callback(selectedCase[0]);
		}
		var action = component.get("c.getCaseById");
		action.setParams({"caseId": caseId });
		action.setCallback(this, function(response) {
			if (response.getState() === "SUCCESS") {
				var selectedCase = response.getReturnValue();
				allCases.push(selectedCase);
				component.set("v.showShowMore", (allCases.length > component.get("v.defaultNumberCases")) );
				component.set("v.allCases", allCases);

				callback(selectedCase);
			} else {
				callback(null);
			}
		});
		$A.enqueueAction(action);
	},

	loadCaseFromURL: function(component, ticketId) {
		var allCases = component.get("v.allCases");
		for (var i = 0; i < allCases.length; i++) {
			if (allCases[i].c.Id === ticketId) {
				if (window.innerWidth > 1025) {
					window.setTimeout(
						$A.getCallback(function() {
							if (component.find('caseElement')) {
								var selectedCaseTmp = component.find('caseElement').getElements();
								var scrollableArea = component.find("cases-area-scrollable").getElement();

								if (selectedCaseTmp &&
									selectedCaseTmp[0] &&
									selectedCaseTmp[0].getBoundingClientRect() &&
									scrollableArea.getBoundingClientRect()) {

									var selectedCaseTmpCoord = selectedCaseTmp[0].getBoundingClientRect().top + window.pageYOffset;
									var scrollableAreaFromTop = scrollableArea.getBoundingClientRect().top + window.pageYOffset;
									scrollableArea.scrollTop = selectedCaseTmpCoord - scrollableAreaFromTop;
								}
							}
						})
					, 1000);
				}

				var defaultNumberCases = component.get("v.defaultNumberCases");
				if (i + 1 > defaultNumberCases) {
					component.set("v.defaultNumberCases", i + 1);
				}

				component.set("v.selectedCasePosition", i);
				var caseRecord = allCases[i];
				caseRecord["containUpdates"] = false;
                if(caseRecord["c"].Id.startsWith('500')) {
                    caseRecord["c"].attributes = {"type":"Case"};
                }
				component.set("v.selectedCase", caseRecord["c"]);
				$A.get("e.c:ShowCaseDetails").setParams({
					"selectedCase": component.get("v.selectedCase")
				}).fire();
				if (caseRecord.c.cncrg__Unread_Update_for_Contact__c === true) {
					this.markUpdatesAsRead(component, caseRecord);
					this.getUpdatedCasesNumber(component);
				}
				break;
			}
		}
		this.scrollHandler(component);
	},

	showRelatedArticle: function(component, event, href) {
		var helper = this;
		var prefix = '';
		if (component.get("v.isCommunity")) {
			prefix = helper.getCommuityPrefix();
		}

		var error = $A.get("$Label.c.Message_Disable_Article_Link");
		var action = component.get("c.getRelatedArticleData");
		action.setParams({
			href: href,
			prefix: prefix
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var newResult = response.getReturnValue()[0];
				var liveAgentPage = response.getReturnValue()[1];
				var isUserHasPermitionToCreateCase = response.getReturnValue()[2];
				var appName = response.getReturnValue()[3] || 'Cadalys Concierge ™';
				var breadcrumbsOptions = component.get("v.breadcrumbsOptions") || [];

				if (event.getParam("outerText") || newResult.title) {
					breadcrumbsOptions.push({
						'label': (newResult.title || event.getParam("outerText")),
						'value': event.getParam("href")
					});
					component.set("v.breadcrumbsOptions", breadcrumbsOptions);
				}

				$A.createComponents([
					[
						"c:RelatedArticleDetails", {
							href: href,
							appName: appName,
							activeActions: newResult.setting,
							activeCaseLayoutRecord: newResult.settingCaseLayout,
							result: newResult,
							liveAgentPage: liveAgentPage,
							availableLA: component.get('v.availableLA'),
							communityLoading: component.get("v.isCommunity"),
							isUserHasPermitionToCreateCase: isUserHasPermitionToCreateCase,
							parentComponent: 'ArticleDetails',
							breadcrumbsOptions: component.getReference("v.breadcrumbsOptions")
						}
					]
				], function(components, status, errorMessage) {
					if (status === "SUCCESS") {
						$A.util.addClass(component.find('my_tikets_div'), 'hidden_div');
						component.set("v.relatedArticle", components);
						$A.util.removeClass(component.find('total_div'), 'hidden_div');
					} else if (status === "ERROR") {
						helper.showErrorMessage(component, errorMessage);
					}
				});
			} else if (state === "ERROR") {
				helper.showErrorMessage(component, error);
			}
		});
		$A.enqueueAction(action);
		/*to do dymamic creation of the RelaLated Article*/
	},
	showErrorMessage: function(component, message) {
		var messageCmp = component.find('exceptionMessage');
		component.set('v.errorMessage', message);
		for (var i = 0; i < messageCmp.length; i++) {
			$A.util.removeClass(messageCmp[i], 'hidden_div');
		}
		window.setTimeout($A.getCallback(function() {
			if (component.isValid()) {
				for (var j = 0; j < messageCmp.length; j++) {
					$A.util.addClass(messageCmp[j], 'hidden_div');
				}
			}
		}), 2000);
	},

	liveAgentHandler: function(component) {
		window.onmessage = function(event) {
			if (event.data === 'AVAILABLE') {
				component.set('v.availableLA', true);
			}
			if (event.data === 'UNAVAILABLE') {
				component.set('v.availableLA', false);
			}
		};
	},
	scrollToCommentMobile: function(component) {
		if (window.innerWidth <= 1025) {
			window.setTimeout(
				$A.getCallback(function() {
					var scrollBlockComm = component.find('work_space_2').getElement();
					var scrollBlockApp = component.find('scrollBlock').getElement();
					var valueToScrollMobile = component.get('v.valueToScrollMobile');
					if (scrollBlockComm) scrollBlockComm.scrollTop = valueToScrollMobile;
					if (scrollBlockApp) scrollBlockApp.scrollTop = valueToScrollMobile;
				})
			, 500);
		}
	},

	scrollHandler: function(component) {
		if (window.innerWidth < 1025) {
			window.setTimeout(
				$A.getCallback(function() {
					var scrollBlock = component.find('work_space_2').getElement();
					if (scrollBlock) {
						component.set('v.scrollValue', scrollBlock.scrollTop);
						scrollBlock.scrollTop = 0;
					}
					var scrollSupBlock = component.find('scrollBlock').getElement();
					if (scrollSupBlock) {
						component.set('v.scrollSubValue', scrollSupBlock.scrollTop);
						scrollSupBlock.scrollTop = 0;
					}
					$A.util.addClass(component.find('case_descriptionId'), 'show');
					$A.util.addClass(component.find('cases_areaID'), 'hide');
					component.set("v.showBtnBack", true);
					$A.util.addClass(component.find('txt_tickets'), 'hide');
				})
			, 500);
		}
	},

	showMessage: function(component, messageText, isInfo) {
		component.set('v.messageText', messageText);
		if (isInfo) {
			$A.util.addClass(component.find('message_section').getElement(), 'info_message');
			component.set('v.showInfoMessage', true);
		} else {
			$A.util.addClass(component.find('message_section').getElement(), 'success_message');
			window.setTimeout(
				$A.getCallback(function() {
					$A.util.removeClass(component.find('message_section').getElement(), "active");
				}), 5000
			);
		}
		$A.util.addClass(component.find('message_section').getElement(), "active");
		$A.util.addClass(component.find('work_space_2').getElement(), "work_space_opacity");
	}
})