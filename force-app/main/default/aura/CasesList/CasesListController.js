({
	doInit: function(component, event, helper) {
		$A.util.removeClass(component.find('my_tikets_div'), 'hidden_div');
		component.set("v.selectedCaseElement", undefined);
		component.set("v.selectedCasePosition", undefined);
		component.set("v.allCases", []);
		helper.loadCasesSettingsAndLoadCases(component);
		helper.liveAgentHandler(component);
	},

	refreshCaseView: function(component, event, helper) {
		component.set("v.selectedCaseElement", undefined);
		component.set("v.selectedCasePosition", undefined);
		$A.get("e.c:ShowCaseDetails").setParams({
			"selectedCase": undefined
		}).fire();
		component.set("v.allCases", []);
		helper.loadMoreCases(component, component.get("v.defaultNumberCases"), false, true);
	},

	showTikets: function(component, event, helper) {
		helper.showSpinner(component.getSuper());
		component.set("v.selectedCaseElement", undefined);
		component.set("v.selectedCasePosition", undefined);
		$A.get("e.c:ShowCaseDetails").setParams({
			"selectedCase": undefined
		}).fire();
		component.set("v.allCases", []);
		helper.loadCasesSettingsAndLoadCases(component);
		if (window.innerWidth < 1025) {
			$A.util.removeClass(component.find('case_descriptionId'), 'show');
			$A.util.removeClass(component.find('cases_areaID'), 'hide');
			$A.util.removeClass(component.find('txt_tickets'), 'hide');
			component.set("v.showBtnBack", false);
		}
	},

	hideTikets: function(component, event, helper) {
		$A.util.removeClass(component.find('my_tickets_btn'), 'active');
		$A.util.addClass(component.find('my_tikets_div'), 'hidden_div');
	},

	showCase: function(component, event, helper) {
		helper.scrollHandler(component);

		var newCase = parseInt(event.target.id, 10);
		var newCaseElement = event.target;
		var previousCase = component.get("v.selectedCasePosition");
		var previousCaseElement = component.get("v.selectedCaseElement");

		if (newCaseElement.className && newCase !== previousCase) {
			while (newCaseElement.className.indexOf('table_cell') < 0) {
				newCaseElement = newCaseElement.parentElement;
			}
			//$A.util.addClass(newCaseElement, "selected_case_style");
			if (previousCaseElement) {
				//$A.util.removeClass(previousCaseElement, "selected_case_style");
			}
			component.set("v.selectedCaseElement", newCaseElement);
			component.set("v.selectedCasePosition", newCase);

			var caseRecord = component.get("v.allCases")[newCase];
			if (caseRecord !== undefined) {
				caseRecord["containUpdates"] = false;
				component.set("v.selectedCase", caseRecord["c"]);
				$A.get("e.c:ShowCaseDetails").setParams({
					"selectedCase": component.get("v.selectedCase")
				}).fire();
				if (caseRecord.c.cncrg__Unread_Update_for_Contact__c === true) {
					helper.markUpdatesAsRead(component, caseRecord);
					helper.getUpdatedCasesNumber(component);
				}
			}
		}
	},

	backToTickets: function(component, event, helper) {
		if (window.innerWidth < 1025) {
			$A.util.removeClass(component.find('case_descriptionId'), 'show');
			$A.util.removeClass(component.find('cases_areaID'), 'hide');
			component.set("v.showBtnBack", false);
			$A.util.removeClass(component.find('txt_tickets'), 'hide');
			window.setTimeout(
				$A.getCallback(function() {
					var scrollValue = component.get('v.scrollValue');
					var scrollBlock = component.find('work_space_2').getElement();
					if (scrollBlock) {
						scrollBlock.scrollTop = scrollValue;
					}
				}), 30
			);
			window.setTimeout(
				$A.getCallback(function() {
					var scrollSubValue = component.get('v.scrollSubValue');
					var scrollSubBlock = component.find('scrollBlock').getElement();
					if (scrollSubBlock) {
						scrollSubBlock.scrollTop = scrollSubValue;
					}
				}), 50
			);
		}
	},

	showMessage: function(component, event, helper) {
		if (event.getParam('isRelatedArticle') && !event.getParam('isError')) {
			helper.loadMoreCases(component, component.get("v.defaultNumberCases"), true, false);
		}
	},
	showMoreCases: function(component, event, helper) {
		var rows = component.get("v.numberChunksCase");
		var defaultNumberCases = component.get("v.defaultNumberCases") + rows;

		component.set("v.defaultNumberCases", defaultNumberCases);
		helper.loadMoreCases(component, defaultNumberCases, false, false);
	},
	relatedArticleClick: function(component, event, helper) {
		if (event.getParam("href")) {
			var href = event.getParam("href");
			helper.showRelatedArticle(component, event, href);
		} else {
			$A.util.removeClass(component.find('my_tikets_div'), 'hidden_div');
			$A.util.addClass(component.find('total_div'), 'hidden_div');
			component.set("v.relatedArticle", null);
			component.set('v.breadcrumbsOptions', []);
		}
	},
	scrollToCommentMobile: function(component, event, helper) {
		helper.scrollToCommentMobile(component);
	},
	handleOpenFlow: function(component, event, helper) {
		event.stopPropagation();
		var flowName = event.getParam("flowName");
		if (flowName) {
			component.find('flowCmp').doOpenFlow(flowName);
		}
		return false;
	}
})