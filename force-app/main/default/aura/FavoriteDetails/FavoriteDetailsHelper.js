({
	changeIframeTags : function(component, allIframe) {
		var body = component.find("body").getElement();
		var iframePlaces = body.getElementsByClassName("video-container");
		var iframesArray = [];
		for(var i=0, j=allIframe.length; i<j; i++) {
			var para = document.createElement("iframe");
			var regex = /<iframe.*?src="(.*?)"/;
			var regexWidth = /<iframe.*?width="(.*?)"/;
			var regexHeight = /<iframe.*?height="(.*?)"/;
			var src = regex.exec(allIframe[i])[1];
			var width = regexWidth.exec(allIframe[i])[1];
			var height = regexHeight.exec(allIframe[i])[1];
			para.src = src;
			para.width = width;
			para.height = height;
			iframePlaces[i].appendChild(para);
		}
	},

	parseLinkTags: function(component, linksTags) {
		for(var i=0, j=linksTags.length; i<j; i++) {
			linksTags[i].removeAttribute('download');
			if(linksTags[i].href.indexOf('/articles/') !== -1) {
				linksTags[i].addEventListener("click", $A.getCallback(function(event) {
					$A.get("e.c:RelatedArticleClick").setParams({
						"href": event.target.target
					}).fire();
				}));
				linksTags[i].target = linksTags[i].href.substring(linksTags[i].href.indexOf('articles/'), (linksTags[i].href.indexOf('?') !== -1 ? linksTags[i].href.indexOf('?') : linksTags[i].href.length));
				linksTags[i].removeAttribute("href");
			}
		}
	},

	replaceAll : function(target, search, replacement) {
		return target.replace(new RegExp(search, 'g'), replacement);
	}
})