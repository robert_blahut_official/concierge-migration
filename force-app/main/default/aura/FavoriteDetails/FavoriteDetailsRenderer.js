({
	afterRender: function(component, helper) {
		this.superAfterRender();
		var htmlBody = component.get("v.selectedArticle.body");
		if (htmlBody) {
			htmlBody = helper.replaceAll(
				helper.replaceAll(
					htmlBody,
					'<iframe',
					'<div class="video-container"><iframe'
				),
				'</iframe>',
				'</iframe></div>');
		}

		if (component.get("v.externalComponentName")) {
			$A.createComponent(
				component.get("v.externalComponentName"), {
					"knowledgeArticleId": component.get("v.selectedArticle.article")['KnowledgeArticleId']
				},
				function(extentionBlock, status, errorMessage) {
					if (status === "SUCCESS") {
						component.set("v.body", extentionBlock);
					}
				}
			);
		} else {
			var body = component.find("body").getElement();
			var regexIframe = /(?:<iframe[^>]*)(?:(?:\/>)|(?:>.*?<\/iframe>))/g;
			body.innerHTML = htmlBody;
			if (htmlBody) {
				var allIframe = htmlBody.match(regexIframe);
				if (allIframe && allIframe.length > 0 && body.innerHTML.match(regexIframe) == null) {
					helper.changeIframeTags(component, allIframe);
				}
			}
			var linksTags = body.getElementsByTagName('a');
			if (linksTags && linksTags.length > 0) {
				helper.parseLinkTags(component, linksTags);
			}
			helper.scrollToAnchor();
		}
	}
})