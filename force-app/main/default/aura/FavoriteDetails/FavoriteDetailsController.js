({
	deleteFromFavorites : function (component, event, helper) {
		var deleteButton = component.find("deleteButton");
		deleteButton.set("v.disabled", true);
		$A.get("e.c:DeleteFromFavorites").fire();
	},
	showRelatedArticle : function (component, event, helper) {
		if(event.getParam("href")) {
			var href = event.getParam("href");
			helper.showRelatedArticle(component, href);
		}
		else {
			$A.util.removeClass(component.find('atricleDetailsSection'), 'hidden_div');
			$A.util.addClass(component.find('relatedArticle'), 'hidden_div');
			component.set("v.relatedArticle", null);
		}
	}
})