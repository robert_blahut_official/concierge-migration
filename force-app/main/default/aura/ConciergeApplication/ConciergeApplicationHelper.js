({
	getAppName : function(component) {
		var action = component.get("c.getApplicationName");
		action.setStorable();
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var applicationName = response.getReturnValue();
				if(!applicationName) {
					applicationName = component.get("v.applicationName")
				}
				else {
					component.set("v.applicationName", applicationName);
				}
				document.title = applicationName;
			} else if (state === "INCOMPLETE") {
			} else if (state === "ERROR") {
				var errors = response.getError();
			}
		});
		$A.enqueueAction(action);
	}
})