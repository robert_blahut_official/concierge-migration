({
	doInit : function(component, event, helper) {
		var device = $A.get("$Browser.formFactor");
		var isWEBKIT = $A.get('$Browser.isWEBKIT');
		var isIOS = $A.get("$Browser.isIOS");
		var isAndroid = $A.get("$Browser.isAndroid");

		window.onmessage = function(event) {
			if(event.data === 'heigthSf1' && device !== 'DESKTOP' && isWEBKIT) {
				if(isIOS) {
					component.set('v.isLEXMobile', true);
				}
				if(isAndroid) {
					component.set('v.isLEXMobileAndroid', true);
				}
			}
		};
	}
})