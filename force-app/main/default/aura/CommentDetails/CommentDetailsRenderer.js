({
	afterRender: function(component, helper) {
		this.superAfterRender();
		var feedItem = component.get("v.comment");
		if(feedItem.linkUrl !== undefined) {
			var bodyComponent = component.find("feedItemBody").getElement();
			var para = document.createElement("a");
			var prefix = '';
			if (component.get("v.isCommunity")) {
				var currUrl;
				try {
					currUrl = window.location.pathname;
				}
				catch (e) {
					currUrl = $A.get('$Resource.Media');
				}
				var prefixArray = currUrl.split('/');
				if (prefixArray[1] !== 's')
					prefix = prefixArray[1];
			} 
			if(!feedItem.linkUrl.startsWith((prefix ? '/' + prefix : '') +'/00P')) { //not Attachment
				var endPos = feedItem.linkUrl.indexOf('?') < 0 ? feedItem.linkUrl.length : feedItem.linkUrl.indexOf('?');
				var correctLink = feedItem.linkUrl.substring(feedItem.linkUrl.indexOf('/articles/'), endPos);
				para.addEventListener("click", $A.getCallback(function(event) {
					$A.get("e.c:RelatedArticleClick").setParams({
						"href": correctLink,
						"component": 'CaseList',
						"outerText": event.target.outerText
					}).fire();
				})); 
				para.target = correctLink;
				para.innerHTML = '<span>' + feedItem.linkTitle + '</span>';
			}
			else {
				
				para.target = "_blank";
				para.href = (prefix ? '/' + prefix : '') + '/servlet/servlet.FileDownload?file=' + feedItem.linkUrl.substring((prefix ? prefix.length + 2 : 1));
				para.innerHTML = '<span>' + feedItem.linkTitle + '</span>';
			}
			bodyComponent.appendChild(para);
		}
	}
})