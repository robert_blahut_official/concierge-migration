({
    like: function (component, event, helper) {
        helper.sendLike(component);
    },

    unlike: function (component, event, helper) {
        helper.sendUnlike(component);
    },

    scrollDown: function (component, event, helper) {
        helper.scrollDown(component);
    }
})