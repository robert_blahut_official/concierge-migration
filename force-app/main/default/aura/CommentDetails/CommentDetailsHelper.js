({
    sendLike : function(component) {
        var recordId = component.get('v.comment.recordId');
        var feedItemId = component.get('v.feedItemId');
        var action = component.get("c.insertLike");
        action.setParams({
            "recordId" : recordId,
            "feedItemId": feedItemId

        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var newLike = response.getReturnValue();
                component.set('v.comment.isLiked', true);
                component.set('v.comment.likeId', newLike.Id);
                component.set('v.comment.likeCount', component.get('v.comment.likeCount') + 1);
            } else if (state === "INCOMPLETE") {
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                    }
                } else {
                }
            }
        });
        $A.enqueueAction(action);
    },

    sendUnlike: function (component) {
        var likeId = component.get('v.comment.likeId');
        var action = component.get("c.deleteLike");
        action.setParams({
            "likeId" : likeId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.comment.isLiked', false);
                component.set('v.comment.likeCount', component.get('v.comment.likeCount') - 1);
            } else if (state === "INCOMPLETE") {
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                    }
                } else {
                }
            }
        });
        $A.enqueueAction(action);
    },

    scrollDown: function (component) {
        var clickedIndex = component.get('v.commentIndex');
        component.set('v.activeIndex', clickedIndex);

    }
})