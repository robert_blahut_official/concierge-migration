({
	rerender : function(component, helper) {
        this.superRerender();
        if (helper.listOfActiveFlags['CLEAR_FILE']) {
			helper.listOfActiveFlags['CLEAR_FILE'] = undefined;
        }
    }
})