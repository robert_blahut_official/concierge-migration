({
	showCaseDetails: function(component, event, helper) {
		if (component.get("v.isCommunity")) {
			component.set("v.communityPrefix", '/' + helper.getCommuityPrefix());
		}
		helper.showSpinner(component.getSuper());

		component.set('v.selectedComment', null);
		component.set('v.fullTextArea', null);
		component.set('v.fileTooLarge', false);
		component.set('v.fileAttached', false);
		component.set('v.disabledButton', true);
		component.set('v.description', '');

		var selectedCase = event.getParam("selectedCase");
		if (selectedCase) {
			component.set('v.description', selectedCase.Description || '');
			component.set("v.creator", selectedCase.CreatedById);

			helper.loadAttachments(component, selectedCase.Id);
			helper.loadFeedItems(component, selectedCase);
			helper.dynamicCaseFieldCreate(component, selectedCase);
		} else {
			component.set("v.listOfCaseAttachments", []);
			component.set("v.selectedCase", null);
			component.set("v.listOfFeedItems", null);

			helper.hideSpinner(component.getSuper());
		}

		//Reset new comment text and file after the displayed case was changed
		component.set('v.newCommentText', '');
		helper.listOfActiveFlags['CLEAR_FILE'] = true;
		component.set('v.fileFormLabel', '');
	},

	giveFeedback: function(component, event, helper) {
		var selectedCase = component.get('v.selectedCase');
		if (!selectedCase) {
			return
		}
		$A.get("e.c:GetCaseIdEvent").setParams({
			"CaseId": selectedCase.Id
		}).fire();
	},

	hideFeedBack: function(component, event, helper) {
		$A.util.addClass(component.find('giveFeedbackSection'), 'feedback_hidden');
	},

	openCaseDetails: function(component, event, helper) {
		// window.location = '/' + component.get("v.selectedCase.Id");
	},

	showMore: function(component, event, helper) {
		helper.showSpinner(component.getSuper());
		helper.showMoreComments(component, event.target.id);
	},

	scrollToComment: function(component, event, helper) {
		helper.scrollToComment(component, event, helper);
	},

	goToNextPage: function(component, event, helper) {
		if (component.get('v.selectedCase')) {
			helper.showSpinner(component.getSuper());
			helper.implNewPage(component);
		}
	},
	renderCaseComment: function(component, event, helper) {
		var selectedCase = component.get('v.selectedCase');
		if (!selectedCase) {
			return
		}

		$A.createComponent(
			component.get("v.caseCommentComponentName"), {
				"parentId": component.getReference("v.selectedCase.Id"),
				"selectedCase": component.getReference("v.selectedCase"),
				"currentList": component.getReference("v.listOfFeedItems"),
				"type": 'feedItem',
				"isCommunity": component.get("v.isCommunity"),
			},
			function(dynamicCaseBlock, status, errorMessage) {
				if (status === "SUCCESS") {
					component.set("v.body", dynamicCaseBlock);
				}
			}
		);
	}
})