({
	listOfActiveFlags: {},
	loadAttachments: function(component, caseId) {
		var action = component.get("c.loadAllCaseAttachments");
		action.setStorable();
		action.setParams({
			"caseId": caseId
		});

		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				component.set("v.listOfCaseAttachments", response.getReturnValue());
			} else if (state === "ERROR") {
				var errors = response.getError();
			}
		});
		$A.enqueueAction(action);
	},

	showMoreComments: function(component, feedIndex) {
		var action = component.get("c.getAllCommentsByFeedItem");
		var listOfFeedItems = component.get('v.listOfFeedItems');
		action.setParams({
			"caseFeedId": listOfFeedItems[feedIndex].recordId
		});

		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var listOfAllFeedItems = response.getReturnValue();
				var isCommunity = component.get("v.isCommunity");
				for (var i = 0; i < listOfAllFeedItems.length; i++) {
					if (listOfAllFeedItems[i].relatedRecordId) {
						this.buildUrl(isCommunity, listOfAllFeedItems[i]);
					}
					for (var j = 0; j < listOfAllFeedItems[i].feedCommentList.length; j++) {
						if (listOfAllFeedItems[i].feedCommentList[j].relatedRecordId) {
							this.buildUrl(isCommunity, listOfFeedItems[i].feedCommentList[j]);
						}
					}
				}
				listOfFeedItems[feedIndex].feedCommentList = listOfAllFeedItems;
				listOfFeedItems[feedIndex].totalComments = 0;
				component.set("v.listOfFeedItems", listOfFeedItems);
			} else if (state === "ERROR") {
				var errors = response.getError();
			}
			this.hideSpinner(component.getSuper());
		});
		$A.enqueueAction(action);
	},

	buildLabel: function(listOfFeedItems) {
		var showCommentLabel = $A.get("$Label.c.Show_all_Comments");
		for (var i = 0; i < listOfFeedItems.length; i++) {
			listOfFeedItems[i].totalCommentsLabel = showCommentLabel.replace('{0}', listOfFeedItems[i].totalComments)
		}
	},

	buildUrl: function(isCommunity, item) {
		var fileUrl =  '/sfc/servlet.shepherd/version/renditionDownload?rendition=ORIGINAL_';
		var fileExt = item.fileName.split('.')[1] || 'png';

		if (isCommunity) {
			var communityPrefix = this.getCommuityPrefix();
			if (communityPrefix === '') {
				item.relatedRecordId = fileUrl + fileExt + '&versionId=' + secureFilters.html(item.relatedRecordId);
			} else {
				item.relatedRecordId = '/' + secureFilters.html(communityPrefix) + fileUrl + fileExt + '&versionId=' + secureFilters.html(item.relatedRecordId);
				if (item.downloadURL) {
					item.downloadURL = '/' + secureFilters.html(communityPrefix) + item.downloadURL;
				}
			}
		} else {
			item.relatedRecordId = fileUrl + fileExt + '&versionId=' + secureFilters.html(item.relatedRecordId);
		}
	},

	buildUrlForRelatedRecords: function(component, listOfFeedItems) {
		var isCommunity = component.get("v.isCommunity");
		for (var i = 0; i < listOfFeedItems.length; i++) {
			if (listOfFeedItems[i].relatedRecordId) {
				this.buildUrl(isCommunity, listOfFeedItems[i]);
			}
			for (var j = 0; j < listOfFeedItems[i].feedCommentList.length; j++) {
				if (listOfFeedItems[i].feedCommentList[j].relatedRecordId) {
					this.buildUrl(isCommunity, listOfFeedItems[i].feedCommentList[j]);
				}
			}
		}
	},

	dynamicCaseFieldCreate: function(component, selectedCase) {
		var action = component.get("c.getCaseWrapper");
		action.setParams({
			caseId: selectedCase.Id
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				this.dynamicCreateComponent(component, (response.getReturnValue() || []), selectedCase);
			} else if (state === "ERROR") {
				var errors = response.getError();
			}
		});
		$A.enqueueAction(action);
	},

	dynamicCreateComponent: function(component, fieldsList, selectedCase) {
		var descriptionField = 'Description';
		var actionLinkField = 'CaseNumber';

		// for custom Case object the diff fields will be used
		if (selectedCase && selectedCase.attributes && selectedCase.attributes.type !== 'Case') {
			var customCaseSettings = JSON.parse(component.get('v.allSettings.customCaseSettings') || '{}');
			if (customCaseSettings.Description) {
				descriptionField = customCaseSettings.Description;
			}
			actionLinkField = 'Name';
		}

		var dynamicArray = [];
		fieldsList.forEach(function(fieldConfig) {
			// description will go separately
			if (fieldConfig.name === descriptionField) {
				component.set('v.description', fieldConfig.displayValue);
			}

			// fields which should be skipped from general process
			if (fieldConfig.name !== 'IsClosed' && fieldConfig.name !== descriptionField) {
				var dynamicAtributesElement;
				if (fieldConfig.name === actionLinkField) {
					dynamicAtributesElement = {
						"tag": "span",
						"body": '# ' + fieldConfig.displayValue,
						"HTMLAttributes": {
							"class": 'text',
							"onclick": component.getReference("c.openCaseDetails"),
							"style": "cursor:pointer"
						}
					};
				} else {
					var fieldValue = fieldConfig.label + ': ' + fieldConfig.displayValue;
					if (fieldConfig.name === 'Priority') {
						fieldValue = fieldConfig.displayValue + ' ' + fieldConfig.label;
					}

					dynamicAtributesElement = {
						"tag": "span",
						"body": fieldValue,
						"HTMLAttributes": {
							"class": 'text'
						}
					};
				}

				if (dynamicAtributesElement) {
					dynamicArray.push(["aura:html", dynamicAtributesElement]);
				}
			}
		});

		$A.createComponents(
			dynamicArray,
			function(components, status, errorMessage) {
				if (status === "SUCCESS") {
					component.set("v.dinamicCaseBlock", components);
				} else if (status === "ERROR") {

				}
			}
		);
	},

	scrollToComment: function(component, event, helper) {
		var selectedComment = component.get('v.selectedComment');
		var scrollable = component.find('scrollable').getElement();
		if (selectedComment && scrollable) {
			var topComments = component.find('topComment');
			var commentForms = component.find('commentForm');
			if (topComments[0] !== undefined) {
				for (var i = 0; i < topComments.length; i++) {
					helper.scrollTop(component, selectedComment, scrollable, topComments[i].getElement(), commentForms[i].getElement());
				}
			} else {
				helper.scrollTop(component, selectedComment, scrollable, topComments.getElement(), commentForms.getElement());
			}
		}
		component.set('v.selectedComment', null);
	},

	scrollTop: function(component, selectedComment, scrollable, topComment, commentForm) {
		if (selectedComment == topComment.dataset.id) {
			component.set('v.fullTextArea', selectedComment);
			var tCoordinates = topComment.getBoundingClientRect().bottom + window.pageYOffset;
			var cCoordinates = commentForm.getBoundingClientRect().top + window.pageYOffset;
			var cScrollableCoordinates = scrollable.getBoundingClientRect().top + window.pageYOffset;
			var valueToScroll = scrollable.scrollTop + cCoordinates - cScrollableCoordinates - 50;
			scrollable.scrollTop = valueToScroll;
			if (window.innerWidth <= 1025) {
				component.set('v.valueToScrollMobile', valueToScroll);
			}
		}
	},

	implNewPage: function(component) {
		var action = component.get("c.getNextChatterPage");
		action.setParams({
			caseId: component.get('v.selectedCase').Id,
			nextPageToken: component.get('v.nextPageToken')
		});
		action.setStorable();
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var chatterPage = response.getReturnValue();
				var listOfFeedItems = component.get('v.listOfFeedItems');
				this.buildLabel(chatterPage.feedItemList);
				this.buildUrlForRelatedRecords(component, chatterPage.feedItemList);
				listOfFeedItems = listOfFeedItems.concat(chatterPage.feedItemList);
				component.set('v.listOfFeedItems', listOfFeedItems);
				component.set('v.nextPageToken', chatterPage.nextPageToken);

			} else if (state === "ERROR") {
				var errors = response.getError();
			}
			this.hideSpinner(component.getSuper());
		});
		$A.enqueueAction(action);
	},

	loadFeedItems: function(component, selectedCase) {
		var helper = this;

		var action = component.get("c.getAllFeedItems");
		action.setParams({
			"caseId": selectedCase.Id
		});
		action.setCallback(this, function(response) {
			component.set("v.selectedCase", selectedCase);

			var state = response.getState();
			if (state === "SUCCESS") {
				helper.setUrlParam(helper.QUERY_PARAMS.CASE_ID, selectedCase.Id);

				var chatterPage = response.getReturnValue();
				var listOfFeedItems = chatterPage.feedItemList;
				helper.buildLabel(listOfFeedItems);
				helper.buildUrlForRelatedRecords(component, listOfFeedItems);
				component.set("v.listOfFeedItems", listOfFeedItems);
				component.set("v.nextPageToken", chatterPage.nextPageToken);

				if (listOfFeedItems.length) {
					$A.util.removeClass(component.find('activity-div'), 'div_to_hide');
				} else {
					$A.util.addClass(component.find('activity-div'), 'div_to_hide');
				}
				$A.util.removeClass(component.find('buttonsSection'), 'feedback_hidden');
				$A.util.removeClass(component.find('giveFeedbackSection'), 'feedback_hidden');

			} else if (state === "ERROR") {
				var errors = response.getError();
			}
			helper.hideSpinner(component.getSuper());
		});
		$A.enqueueAction(action);
	}
})