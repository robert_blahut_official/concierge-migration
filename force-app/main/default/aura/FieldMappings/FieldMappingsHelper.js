({
	getFieldMappings : function(component) {
		var helper = this;
        this.showSpinner(component.getSuper());
		var action = component.get("c.getFieldMappings");
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
                var fieldMappings = response.getReturnValue();
				component.set("v.fieldMappings", fieldMappings);
                var fieldsList = [];
                for(var i = 0; i<fieldMappings.fieldMappingList.length; i++) {
					fieldsList.push({'usersField' : fieldMappings.fieldMappingList[i].usersField, 'historyField' : fieldMappings.fieldMappingList[i].historyField, 'deleted' : false});
                }
                component.set("v.fieldsList", fieldsList);
			} else if (state === "ERROR") {
				var errors = response.getError();
			}
			helper.hideSpinner(component.getSuper());
		});
        $A.enqueueAction(action);
	},
    implementNewSettings : function(component) {
		var helper = this;
        var fieldsList = component.get("v.fieldsList");
        var historyFieldsListForApex = [];
        var usersFieldsListForApex = [];
        for(var i=0; i<fieldsList.length; i++) {
            if(!fieldsList[i].deleted) {
                historyFieldsListForApex.push(fieldsList[i].historyField);
				usersFieldsListForApex.push(fieldsList[i].usersField);
            }
        }
		var hasDublicate = this.hasDuplicates(historyFieldsListForApex);
		if(!hasDublicate) {
			hasDublicate = this.hasDuplicates(usersFieldsListForApex);
		}
		if(!hasDublicate) {
			var action = component.get("c.implementFieldMaps");
			action.setParams({
				usersFieldsList : usersFieldsListForApex,
				historyFieldsList : historyFieldsListForApex
			});
			action.setCallback(this, function(response) {
				var state = response.getState();
				if (state === "SUCCESS") {
					component.find("messageTop").set("v.severity", "confirm");
					component.find("messageBottom").set("v.severity", "confirm");
					$A.util.removeClass(component.find("confirmationTop"), "display_false");
					$A.util.removeClass(component.find("confirmationBottom"), "display_false");
					component.set("v.message", $A.get("$Label.c.All_Changes_Were_Saved_Successfully"));
					window.setTimeout(
						$A.getCallback(function() {
							if (component.isValid()) {
								$A.util.addClass(component.find("confirmationTop"), "display_false");
								$A.util.addClass(component.find("confirmationBottom"), "display_false");
							}
						}), 2000
					);
				} else if (state === "ERROR") {
					var errors = response.getError();
					var message = "Unknown error";
					component.find("messageTop").set("v.severity", "error");
					component.find("messageBottom").set("v.severity", "error");
					$A.util.removeClass(component.find("confirmationTop"), "display_false");
					$A.util.removeClass(component.find("confirmationBottom"), "display_false");
					component.set("v.message", message);
				}
				helper.hideSpinner(component.getSuper());
			});
			$A.enqueueAction(action);
		}
		else {
            helper.hideSpinner(component.getSuper());
			component.find("messageTop").set("v.severity", "error");
			component.find("messageBottom").set("v.severity", "error");
			$A.util.removeClass(component.find("confirmationTop"), "display_false");
			$A.util.removeClass(component.find("confirmationBottom"), "display_false");
			component.set("v.message", $A.get("$Label.c.Duplicate_Values"));
		}
    },
	hasDuplicates : function(array) {
		var valuesSoFar = [];
		for (var i = 0; i < array.length; ++i) {
			var value = array[i];
			if (valuesSoFar.indexOf(value) !== -1) {
				return true;
			}
			valuesSoFar.push(value);
		}
		return false;
	}
})