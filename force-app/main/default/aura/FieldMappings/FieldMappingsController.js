({
    doInit : function(component, event, helper) {
        helper.getFieldMappings(component);
    },
	goBack : function(component, event, helper) {
		$A.get("e.c:BackToSettings").fire();
	},
    saveChanges : function (component, event, helper) {
        helper.showSpinner(component.getSuper());
		helper.implementNewSettings(component);
	}, 
    deleteRow : function (component, event, helper) {
        var newSelectedFields = event.getParam('newSelectedFields');
        var sObjectName = event.getParam('sObjectName');
        var emptyArray = [];
        if(sObjectName === 'User') {
            component.set("v.usersFieldsList", emptyArray);
            component.set("v.usersFieldsList", newSelectedFields);
        }
        else {
            component.set("v.historyFieldsList", newSelectedFields);
        }
    },
})