public with sharing class ArticleRateFeedbackController {

    @AuraEnabled
    public static ArticleFeedbackWrapper getNewArticleFeedbackWrapper() {
        return new ArticleFeedbackWrapper();
    }
    public class ArticleFeedbackWrapperItem {
        @AuraEnabled
        public String articleRateItemLabel;

		@AuraEnabled
        public String articleRateItemName;
        
        @AuraEnabled
        public Boolean isChecked;

        public ArticleFeedbackWrapperItem(String articleRateItemLabel, String articleRateItemName, Boolean checked){
            this.articleRateItemLabel = articleRateItemLabel;
			this.articleRateItemName = articleRateItemName;
            this.isChecked = checked;
        }
    }

    public class ArticleFeedbackWrapper {
        @AuraEnabled
        public List<ArticleFeedbackWrapperItem> articleFeedbackWrapperItemList;
        
        @AuraEnabled
        public String comment;

        public ArticleFeedbackWrapper() {
            articleFeedbackWrapperItemList = initArticleFeedbackWrapperItemList();
        }
    }

    public static List<ArticleFeedbackWrapperItem> initArticleFeedbackWrapperItemList() {
		List<ArticleFeedbackWrapperItem> articleFeedbackWrapperItemList = new List<ArticleFeedbackWrapperItem>();
        Map<String, String> picklistOptionsMap = new Map<String, String>();
        Schema.DescribeFieldResult fieldResult = Article_Feedback__c.Feedback_Reason__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for(Schema.PicklistEntry item : ple) {
			articleFeedbackWrapperItemList.add(new ArticleFeedbackWrapperItem(item.getLabel(), item.getValue(), item.isDefaultValue()));
        }
        return articleFeedbackWrapperItemList;
    }

    @AuraEnabled
    public static void createSearchStaging(String wrappString, String articleId, String articleTitle, Boolean isOther) {
        ArticleFeedbackWrapper wrapp = (ArticleFeedbackWrapper) JSON.deserialize(wrappString, ArticleFeedbackWrapper.class);
        Search_Staging__c searchSettingRecord = new Search_Staging__c();
        searchSettingRecord.Biederman__c = JSON.serialize(new Article_Feedback__c(
                Article_ID__c = articleId,
                Article_Title__c = articleTitle,
                Comments__c = wrapp.comment,
                Feedback_Reason__c = getReasons(wrapp.articleFeedbackWrapperItemList, isOther)
        ));
        searchSettingRecord.RecordTypeId = FeedbackBatch.FEEDBACK_RECORD_TYPE_ID;
        WorkWithArticles.encryptedSearchStaging(searchSettingRecord);
        ESAPI.securityUtils().validatedInsert(new List<Search_Staging__c>{searchSettingRecord});
    }

    private static String getReasons(List<ArticleFeedbackWrapperItem> itemList, Boolean isOtherOption) {
        List<String> feedbackReasonList = new List<String>();
        for(Integer i = 0; i < itemList.size(); i++) {
            if(itemList[i].isChecked) {
                feedbackReasonList.add(itemList[i].articleRateItemName);
            }
        }
        if(isOtherOption) {
            feedbackReasonList.add('Other');
        }
        return String.join(feedbackReasonList, ';');
    }
}