@isTest
public class ContactCaseSharingBatchTest {
    @testSetup
    static void setup(){
        Database.insert(new List<cncrg__Concierge_Settings__c> {new cncrg__Concierge_Settings__c(Name = ContactCaseSharingBatch.CASE_ORIGIN_SETTING_NAME, Value__c = 'Concierge')}, false);
        insert new List<Trigger_Activator__c>{
            new Trigger_Activator__c(
                    Name = 'Case Sharing Trigger',
                    Active__c = true
                ),
            new Trigger_Activator__c(
                    Name = 'Contact Case Sharing Trigger',
                    Active__c = true
                ),
            new Trigger_Activator__c(
                    Name = 'User Case Sharing Trigger',
                    Active__c = true
                )
        };

        Contact newContact = new Contact(
            Firstname = 'Test',
            Lastname = 'Test',
            cncrg__UserId__c = TestClassUtility.createTestUserByProfileId(TestClassUtility.getIdOfUsualProfile()).Id
        );
        
        insert newContact;

        Case newCase = new Case(
            subject = 'Test Case',
            Origin = 'Concierge',
            ContactId = newContact.Id
        );
        
        insert newCase;
        newCase.Subject = 'updated Subject';
        update newCase;
        System.assertEquals(newCase.Subject, 'updated Subject');
    }

    static testMethod void batchExecuteTest(){
        Map<Id, Contact> contactIdsMap = new Map<Id, Contact>   ([
                                                                    SELECT  Id
                                                                    FROM    Contact
                                                                ]);
        Test.startTest();
        Database.executeBatch(new ContactCaseSharingBatch(contactIdsMap.keySet()), 200);
        Test.stopTest();
        System.assert(Limits.getDMLStatements() > 5);
    }
    static testMethod void updateExecuteTest(){
        List<Case> caseList =   [
                                    SELECT  Id,
                                            Origin,
                                            ContactId,
                                            OwnerId
                                    FROM    Case
                                ];
        CaseSharingHandler.updeteExecute(caseList, null);
        System.assertEquals(caseList.size(), 1);
    }
    static testMethod void caseAndContactTriggerBeforeUpdateTest(){
        Contact cont =  [
                        SELECT  Id,
                                Firstname,
                                Lastname,
                                cncrg__UserID__c
                        FROM    Contact
                        LIMIT   1
                    ];
        cont.UserID__c = TestClassUtility.createTestUserByProfileId(TestClassUtility.getIdOfUsualProfile()).Id;
        update cont;
        System.assertNotEquals(cont, null);
    }
    static testMethod void insertSharingRulesTest() {
        Case caseRecord = [	SELECT 	Id
                          	FROM 	Case
                           	LIMIT 	1
                          ];
        CaseSharingHandler.insertSharingRules(new Map<Id, Id> {caseRecord.Id => UserInfo.getUserId()}, new Set<Id> {UserInfo.getUserId()});
        System.assert(caseRecord != null);
    }
}