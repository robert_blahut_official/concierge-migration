public class ObjectAttachmentsListController  {
	@AuraEnabled
	public static Map<String, String> getAttachmentsMap(String recordId) {
		Map<String, String> attachmentsMap = new Map<String, String>();
		attachmentsMap.putAll(getAttachnemtRecordsMap(recordId));
		attachmentsMap.putAll(getContentDocumentRecordsMap(recordId));
		return attachmentsMap;
	}

	private static Map<String, String> getAttachnemtRecordsMap(String recordId) {
		Map<String, String> attachmentsMap = new Map<String, String>();
		List<Attachment> allAttachmentParentList = [SELECT Id, Name FROM Attachment WHERE ParentId = :recordId];
		for(Attachment item : allAttachmentParentList) {
			attachmentsMap.put('/servlet/servlet.FileDownload?file=' + item.Id, item.Name);
		}
		return attachmentsMap;
	}

	private static Map<String, String> getContentDocumentRecordsMap(String recordId) {
		List<Network> activeCommunities = [SELECT Id, UrlPathPrefix FROM Network WHERE Id = :Network.getNetworkId()];
		String communityPrefix = activeCommunities.isEmpty() ? '' : activeCommunities.get(0).UrlPathPrefix;
		communityPrefix = String.isNotBlank(communityPrefix) ? ('/' + communityPrefix) : '';

		Map<String, String> attachmentsMap = new Map<String, String>();
		List<ContentDocumentLink> allAttachmentParentList = [
			SELECT	Id, 
					ContentDocument.Title, 
					ContentDocument.LatestPublishedVersionId 
			FROM ContentDocumentLink 
			WHERE LinkedEntityId = :recordId
		];

		for(ContentDocumentLink item : allAttachmentParentList) {
			attachmentsMap.put(communityPrefix + '/sfc/servlet.shepherd/version/download/' + item.ContentDocument.LatestPublishedVersionId, item.ContentDocument.Title);
		}
		return attachmentsMap;
	}
}