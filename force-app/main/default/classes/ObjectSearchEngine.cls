/**
* @description

	This class contains logic for search into custom and standard objects (except knowledge)

* @methods
	getSearchResults -				method for getting search results
	rateItem -						method for upvoting or downvoting records (create/update Vote__c records)
	getCustomFavoritesArticles -	method for getting favorites records from custom and standard objects by Ids
*/
public with sharing class ObjectSearchEngine extends AbstractSearchEngine implements SearchEngineInterface {
	public final static Set<String> REQUIRED_FIELDS_FOR_SEARCH = new Set<String> {
		'Id',
		'LastModifiedDate',
		'CreatedDate'
	};

	public ObjectSearchEngine() {
		this.isObjectSearch = true;
	}

	public List<SearchResultsWrapper> getSearchResults(String query, String prefix) {
		List<SearchResultsWrapper> result = new List<SearchResultsWrapper>();
		query = query.toLowerCase().trim();

		if (query.length() > 1 || Utils.containsDoubleByteCharacters(query)) {

			Map<String, Utils.sObjectWrapper> mapOfTypesSettings = Utils.prepareTypesSettings(true);
			if (mapOfTypesSettings.size() == 0) {
				return result;
			}

			Search.SearchResults searchResults = getSOSLResults(query, mapOfTypesSettings);
			if (searchResults == null) {
				return result;
			}

			Map<String, String> titleObjectMap = getTitleObjectMap();
			Map<String, String> titleNotAccessibleObjectMap = new Map<String, String>();
			List<Schema.DescribeSObjectResult> descResults = Schema.describeSObjects(new List<String>(mapOfTypesSettings.keySet()));

			for (String typeName: mapOfTypesSettings.keySet()) {
				if (String.isBlank(mapOfTypesSettings.get(typeName).titleFieldName)) {
					for (Schema.DescribeSObjectResult descResult: descResults) {
						if (descResult.getName().toLowerCase() == typeName.toLowerCase()) {
							String titleFieldName = titleObjectMap.get(typeName);
							titleNotAccessibleObjectMap.put(
								typeName,
								descResult.fields.getMap().get(titleFieldName).getDescribe().getLabel()
							);
						}
					}
				}
			}

			Integer indicatorLiveTime = 0;
			String settingsValue = Utils.getValueFromConciergeSettings('Hours New/Updated Flag is Displayed');
			if (settingsValue != null) {
				indicatorLiveTime = Integer.valueOf(settingsValue);
			}

			for (String typeName: mapOfTypesSettings.keySet()) {
				List<Search.SearchResult> searchResultsList = searchResults.get(typeName);
				Utils.sObjectWrapper objectConfig = mapOfTypesSettings.get(typeName);
				String snippetFieldName = objectConfig.availableFieldsList[0];

				for (Search.SearchResult searchResult: searchResultsList) {
					sObject record = searchResult.getSObject();

					String snippet = String.valueOf(record.get(snippetFieldName));
					snippet = CustomerExtensionBase.cutPreviewToLength(snippet);

					Integer age = (Date.valueOf(record.get('LastModifiedDate'))).daysBetween(Date.today());
					age = age > 365 ? 365 : age;
					String updateIndicator = '';
					Datetime createdDate = Datetime.valueOf(record.get('CreatedDate'));
					Datetime modifiedDate = Datetime.valueOf(record.get('LastModifiedDate'));
					if (modifiedDate == createdDate && modifiedDate.addHours(indicatorLiveTime) > Datetime.now()) {
						updateIndicator = 'new';
					} else if (modifiedDate != createdDate && modifiedDate.addHours(indicatorLiveTime) > Datetime.now()) {
						updateIndicator = 'updated';
					}

					String title = '';
					String recordId = (String) record.get('Id');
					if (String.isNotBlank(objectConfig.titleFieldName)) {
						Object titleValue = record.get(objectConfig.titleFieldName);
						title = titleValue != null ? String.valueOf(titleValue) : '';
					} else {
						title = Label.Access_denied_for_0_field.replace('{0}', titleNotAccessibleObjectMap.get(typeName));
					}

					List<String> availableFieldsList = Utils.getAllAvailableFields(typeName);
					String recordOwnerId = (String) (availableFieldsList.contains('ownerid') 
						? record.get('OwnerId')
						: record.get('CreatedById')
					);

					SearchResultsWrapper searchResultsWrapperRecord = new SearchResultsWrapper(
						title,
						snippet,
						recordId,
						'blankValue',
						typeName,
						age,
						recordId,
						recordId,
						updateIndicator,
						recordOwnerId
					);
					searchResultsWrapperRecord.isKnowledgeObject = false;
					searchResultsWrapperRecord.typeName = typeName;
					result.add(searchResultsWrapperRecord);
				}
			}
		}
		return result;
	}

	public override String toggleFavoriteAPEX(String recordId, Boolean isFavorite) {
		return null;
	}

	public override List<SearchPhraseWrapper> getSuggestionsForQuery(String query) {
		return new List<SearchPhraseWrapper>();
	}

	public override String rateItem(SearchResultsWrapper item) {
		String articleId = item.knowledgeArticleId;
		List<Vote__c> votes = [
			SELECT	Id, Type__c, Parent_Id__c
			FROM Vote__c
			WHERE Parent_Id__c = :item.knowledgeArticleId AND CreatedById = :UserInfo.getUserId()
			LIMIT 50000
		];

		if (!votes.isEmpty()) { 
			if (
				(votes.get(0).Type__c == 'Up' && !item.isLike)
					|| (votes.get(0).Type__c == 'Down' && item.isLike)
					|| Test.isRunningTest()
			) {
				votes.get(0).Type__c = (item.isLike ? 'Up' : 'Down');
				ESAPI.securityUtils().validatedUpdate(votes);
			}
		} else {
			ESAPI.securityUtils().validatedInsert(new List<Vote__c> {
				new Vote__c(Type__c = (item.isLike ? 'Up' : 'Down'), Parent_Id__c = item.knowledgeArticleId)
			});
		}

		if (item.isLike) {
			ArticleReviewUtils.updateLikedORFavoritedArticle(item, UserInfo.getLanguage());
		}
		return '';
	}

	private Search.SearchResults getSOSLResults(String query, Map<String, Utils.sObjectWrapper> mapOfTypesSettings ) {
		Search_Engine_Configurations__c settingSearch_Engine = Search_Engine_Configurations__c.getValues(ENGINE_CONFIG_SETTING_NAME);
		String totalQuery = 'FIND \'' + Utils.escapeSOSLQuery(query) + '\' IN ALL FIELDS RETURNING ';

		for (String typeName : mapOfTypesSettings.keySet()) {
			totalQuery += typeName + '(';

			Set<String> searchSet = new Set<String>(REQUIRED_FIELDS_FOR_SEARCH);
			searchSet.add(mapOfTypesSettings.get(typeName).availableFieldsList[0]);

			if (String.isNotBlank(mapOfTypesSettings.get(typeName).titleFieldName)) {
				searchSet.add(mapOfTypesSettings.get(typeName).titleFieldName);
			}

			List<String> availableFieldsList = Utils.getAllAvailableFields(typeName);
			String ownerFieldName = availableFieldsList.contains('ownerid') ? 'OwnerId' : 'CreatedById';
			searchSet.add(ownerFieldName);

			totalQuery += ' ' + String.join((Iterable<String>)searchSet, ',') + ' ';

			List<SettingsPanelController.FieldFilter> filtres = mapOfTypesSettings.get(typeName).filtres;
			if (filtres != null && filtres.size() > 0) {
				totalQuery += ' WHERE';
				for (SettingsPanelController.FieldFilter item : filtres) {
					totalQuery += ' ' + item.field + item.expression + (item.fieldValue != 'null' ? ' \'' + item.fieldValue + '\'' : item.fieldValue) + ' AND';
				}
			}
			totalQuery.removeEndIgnoreCase('AND');
			totalQuery = totalQuery.removeEndIgnoreCase('AND') + '),';
		}

		totalQuery = totalQuery.removeEnd(',');
		totalQuery += settingSearch_Engine != null &&  settingSearch_Engine.Maximum_number_of_articles_to_retrieve__c != null ? ' LIMIT ' + Integer.valueOf(settingSearch_Engine.Maximum_number_of_articles_to_retrieve__c) : '';

		Search.SearchResults searchResults;
		try {
			searchResults = Search.find(totalQuery);
		} catch (Exception e) {}
		return searchResults;
	}

	public override List<Object> getFavoritesbyId(List<String> recordIds, String prefix) {
		return new List<FavoriteArticlesWrapper>();
	}

	public override FavoriteArticlesWrapper getFavoriteArticleDetail(FavoriteArticlesWrapper favorite, String prefix) {
		// @TODO
		return favorite;
	}

	public static List<Object> getCustomFavoritesArticles(List<String> externalArticlesIdList) {
		Map<String, String> titleObjectMap = getTitleObjectMap();
		Map<String, Utils.sObjectWrapper> mapOfTypesSettings = Utils.prepareTypesSettings(true);
		List<Object> articlesList = new List<Object>();
		Map<String, List<String>> favoritesIdsMap = new Map<String, List<String>>();
		Map<String, Schema.DescribeSobjectResult> describeMap = new Map<String, Schema.DescribeSobjectResult>();

		for (String item : externalArticlesIdList) {
			Schema.DescribeSobjectResult objectDescribe = null;
			try {
				objectDescribe = ((Id) item).getSObjectType().getDescribe();
			} catch (Exception e) {
				continue;
			}

			String typeName = objectDescribe.getName();
			describeMap.put(typeName, objectDescribe);

			if (mapOfTypesSettings.containsKey(typeName)) {
				if (favoritesIdsMap.containsKey(typeName)) {
					favoritesIdsMap.get(typeName).add(item);
				} else {
					favoritesIdsMap.put(typeName, new List<String> {item});
				}
			}
		}
		for (String objectType : favoritesIdsMap.keySet()) {
			List<Object> objectTypeFavoritesList = getTypeFavorites(favoritesIdsMap, objectType, favoritesIdsMap.get(objectType), mapOfTypesSettings.get(objectType), describeMap.get(objectType), titleObjectMap.get(objectType));
			if (objectTypeFavoritesList != null && objectTypeFavoritesList.size() > 0) {
				articlesList.addAll(objectTypeFavoritesList);
			}
		}
		return articlesList;
	}

	private static List<Object> getTypeFavorites(Map<String, List<String>> favoritesIdsMap, String objectType, List<String> articlesIdList, Utils.sObjectWrapper sObjectWrapperRecord, DescribeSObjectResult describe, String titleNeme) {
		Map<String, Schema.SobjectField> sObjectFieldsDiscribe = describe.fields.getMap();
		List<FavoriteArticlesWrapper> allExternalFavoritesList = new List<FavoriteArticlesWrapper>();

		List<String> requiredFields = new List<String> {'Id', 'CreatedDate', 'LastModifiedDate'};
		String query = 'SELECT ' + String.join(requiredFields, ',');
		Boolean showRelatedAttachments = sObjectWrapperRecord.showAttachments;
		String titleFieldName = sObjectWrapperRecord.titleFieldName;

		for (String item : sObjectWrapperRecord.availableFieldsList) {
			if (!requiredFields.contains(item)) {
				query += ',' + item;
			}
		}

		if (String.isNotBlank(titleFieldName) && !sObjectWrapperRecord.availableFieldsList.contains(titleFieldName)) {
			query += ',' + titleFieldName;
		}
		query += ' FROM ' + objectType;
		query += ' WHERE Id IN :articlesIdList';

		List<SObject> favoriteList = Database.query(query);
		for (sObject item : favoriteList) {
			String title = (String.isBlank(titleFieldName) ? Label.Access_denied_for_0_field.replace('{0}', titleNeme) : ((String) item.get(titleFieldName)));

			String summary = Utils.buildArticleDetail(sObjectWrapperRecord.availableFieldsList, item, sObjectFieldsDiscribe, describe.getName(), new Set<String> (), '');
			summary = summary.removeEnd('<br/>');

			FavoriteArticlesWrapper favorite = new FavoriteArticlesWrapper(item, summary, title);
			favorite.type = objectType;
			favorite.showRelatedAttachments = showRelatedAttachments;
			favorite.knowledgeArticleId = (String)item.get('Id');
			allExternalFavoritesList.add(favorite);
		}
		return allExternalFavoritesList;
	}

	private static Map<String, String> getTitleObjectMap() {
		List<cncrg__Search_Types_Settings__c> searchUsualObjectList = ObjectSearchSettingsController.getSearchUsualObjectList();
		Map<String, String> titleObjectMap = new Map<String, String>();
		for (cncrg__Search_Types_Settings__c item : searchUsualObjectList) {
			titleObjectMap.put(item.cncrg__Article_Type_API_Name__c, item.cncrg__Title_Field__c);
		}
		return titleObjectMap;
	}

	public override SearchResultsWrapper getArticleVotes(SearchResultsWrapper article) {
		List<String> fieldsForKnowledgeCheck = new List<String> { 'cncrg__Parent_Id__c', 'LastModifiedDate', 'cncrg__Type__c' };

		if (ESAPI.securityUtils().isAuthorizedToView('cncrg__Vote__c', fieldsForKnowledgeCheck)) {
			String articleId = article.knowledgeArticleId;
			article.rating = [
				SELECT count()
				FROM cncrg__Vote__c
				WHERE cncrg__Parent_Id__c = :articleId AND cncrg__Type__c = 'Up'
			];
			List<cncrg__Vote__c> votesList = [
				SELECT cncrg__Type__c, LastModifiedDate
				FROM cncrg__Vote__c
				WHERE cncrg__Type__c IN ('Up', 'Down')
					AND cncrg__Parent_Id__c = :articleId
					AND CreatedById = :UserInfo.getUserId()
			];
			if (votesList.size() > 0) {
				for (cncrg__Vote__c item : votesList) {
					article.isLike = (item.cncrg__Type__c == 'Up');
					article.voteLastModifiedDate = item.LastModifiedDate.format();
				}
				article.isDisabled = true;
			}
		}
		return article;
	}

	public String CreateIndex(String indexConfiguration) {return null;}
	public void DeleteIndex(String indexId) {}
	public String GetIndexStatus(String indexId) {return null;}
	public void IndexDocuments() {}
}