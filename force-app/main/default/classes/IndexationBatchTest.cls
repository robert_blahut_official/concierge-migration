@isTest
private class IndexationBatchTest {

	@isTest static void batchIndexationTest() {

        init();

        MultiStaticResourceCalloutMock multimock = new MultiStaticResourceCalloutMock();
        multimock.setStaticResource('test.heroku.com/fts-srv/create-index', 'createIndexForTableMock');
        multimock.setStaticResource('test.heroku.com/fts-srv/update-index', 'updateIndexMock');
        multimock.setStaticResource('test.heroku.com/fts-srv/insert', 'populateTableMock');
        multimock.setStaticResource('test.heroku.com/fts-srv/create-tables', 'createTableMock');

        multimock.setStatusCode(200);
        multimock.setHeader('Content-Type', 'application/json');

        Test.setMock(HttpCalloutMock.class, multimock);


        List<String> fieldNames = new List<String>();
        fieldNames.add('Name');
        Test.startTest();
            IndexationBatch indexationBatchInstance = new IndexationBatch('Account', fieldNames, '');
            Database.executeBatch(indexationBatchInstance,200);
        Test.stopTest();
        System.assertEquals('Success', IndexationBatch.batchResult);
    }

    @isTest static void populateTableTest() {

        init();

        MultiStaticResourceCalloutMock multimock = new MultiStaticResourceCalloutMock();
        multimock.setStaticResource('test.heroku.com/fts-srv/create-index', 'createIndexForTableMock');
        multimock.setStaticResource('test.heroku.com/fts-srv/update-index', 'updateIndexMock');
        multimock.setStaticResource('test.heroku.com/fts-srv/insert', 'populateTableMock');
        multimock.setStaticResource('test.heroku.com/fts-srv/create-tables', 'createTableMock');

        multimock.setStatusCode(200);
        multimock.setHeader('Content-Type', 'application/json');

        Test.setMock(HttpCalloutMock.class, multimock);

        List<String> fieldNames = new List<String>();
        fieldNames.add('Name');

        Test.startTest();
            SObject obj = new Account(Name = 'Test Name');
            IndexationBatch indexationBatch = new IndexationBatch('Account', fieldNames, '');
            String result = indexationBatch.populateTable(new List<SObject>{obj});
            System.assertEquals('Success', result);
        Test.stopTest();

    }

    @isTest static void populateTableTestWithNullResponse() {

        init();

        MultiStaticResourceCalloutMock multimock = new MultiStaticResourceCalloutMock();
        multimock.setStaticResource('test.heroku.com/fts-srv/create-index', 'createIndexForTableMock');
        multimock.setStaticResource('test.heroku.com/fts-srv/update-index', 'updateIndexMock');
        multimock.setStaticResource('test.heroku.com/fts-srv/insert', 'populateTableMock');
        multimock.setStaticResource('test.heroku.com/fts-srv/create-tables', 'createTableMock');

        multimock.setStatusCode(404);
        multimock.setHeader('Content-Type', 'application/json');

        Test.setMock(HttpCalloutMock.class, multimock);

        List<String> fieldNames = new List<String>();
        fieldNames.add('Name');

        Test.startTest();
            IndexationBatch indexationBatch = new IndexationBatch('Account', fieldNames, 'WHERE Name != null');
            System.assertEquals(null, indexationBatch.tableName);
        Test.stopTest();

    }

    /*@isTest static void testExecuteBatch() {
        init();
        Test.startTest();
        Account acc = new Account(
            Name = 'Test Name'
        );
        insert acc;

        MultiStaticResourceCalloutMock multimock = new MultiStaticResourceCalloutMock();
        multimock.setStaticResource('test.heroku.com/fts-srv/create-index', 'createIndexForTableMock');
        multimock.setStaticResource('test.heroku.com/fts-srv/update-index', 'updateIndexMock');
        multimock.setStaticResource('test.heroku.com/fts-srv/insert', 'populateTableMock');
        multimock.setStaticResource('test.heroku.com/fts-srv/create-tables', 'createTableMock');

        multimock.setStatusCode(200);
        multimock.setHeader('Content-Type', 'application/json');

        Test.setMock(HttpCalloutMock.class, multimock);

        List<String> fieldNames = new List<String>();
        fieldNames.add('Name');

        IndexationBatch indexationBatch = new IndexationBatch('Account', fieldNames,'');
        Database.executeBatch(indexationBatch);
        System.assert(indexationBatch != null);
        Test.stopTest();
    }

	*/
    public static void init() {
        Account acc = new Account(Name = 'Test Account');
        //insert acc;

        cncrg__Web_Service_Settings__c customSetting = new cncrg__Web_Service_Settings__c(Name = 'HerokuWebService');
        customSetting.Password__c = 'testPassword';
        customSetting.URL__c = 'test.heroku.com';
        customSetting.User__c = 'testUser';

        insert customSetting;
    }

}