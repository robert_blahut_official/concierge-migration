public with sharing class JsonDeserializer {

    public Result result;
    public Boolean error;
    public String error_message;

    public class Records {
        public Integer id;
        public String text;
        public String summary;
        public Integer rank;
    }

    public class Result {
        public List<Records> records;
        public Integer javatimemls;
    }

    public static JsonDeserializer parse(String json) {
        return (JsonDeserializer) System.JSON.deserialize(json, JsonDeserializer.class);
    }
}