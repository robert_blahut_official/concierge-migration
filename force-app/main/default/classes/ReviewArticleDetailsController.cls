public with sharing class ReviewArticleDetailsController {

    @AuraEnabled
    public static List<Object> getArticle(String recordId) {
        SearchResultsWrapper wrap;
        if(String.isBlank(recordId))
            wrap = new SearchResultsWrapper('', '', '', '', '');
        else {
            wrap = ArticleReviewUtils.findArticle(recordId);
        }
        return new List<Object>{wrap, SearchQueryInputController.getApplicationName()};
    }

    @AuraEnabled
    public static String moveToEdit(String recordId) {
        ArticleReviewUtils.reviewArticle(recordId);
        Article_Review__c currentReview = ArticleReviewUtils.getArticleReview(recordId);
        Boolean isMasterLanguage = ArticleReviewUtils.isMasterLanguage(currentReview);
        if(isMasterLanguage) {
            try {
                KbManagement.PublishingService.editOnlineArticle(currentReview.Parent_Article__c, false);
            } catch (Exception e) {
                System.debug(e);
            }
            return '/knowledge/publishing/articleEdit.apexp?fromPage=RENDERER&id=' + currentReview.Parent_Article__c + '&retURL=' + currentReview.Id;
        }
        try {
            KbManagement.PublishingService.editPublishedTranslation(currentReview.Parent_Article__c, currentReview.Language__c, false);

        } catch (Exception e) {
            System.debug(e);
        }
        return '/knowledge/publishing/articleTranslationEdit.apexp?fromPage=RENDERER&id=' + currentReview.Parent_Article__c + '&lang=' + currentReview.Language__c + '&retURL=' + currentReview.Id;

    }

    @AuraEnabled
    public static void reviewArticle(String recordId) {
        ArticleReviewUtils.reviewArticle(recordId);
    }
}