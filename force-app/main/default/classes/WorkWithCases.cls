public with sharing class WorkWithCases {
	public static final String STATUS_INDICATOR_SETTING_NAME	= 'Name of Case Status Indicator Field';

	public static final String ORG_NAME_PREFIX					= Utils.getOrgPrefix();
	public static final String TARGET_TEMPLATE_FIELD_NAME		= 'Field_to_Prepopulate__c';
	public static final String TEXT_TEMPLATE_FIELD_NAME			= 'Prepopulated_Text__c';

	public static final String CASE_TYPE_SETTING_NAME			= 'Case Types';
	public static final String DEFAULT_CASE_CREATE_FEILD_SET	= 'cncrg__Base_Create_Case';
	public static final String CASE_DETAIL_FIELD_SET			= 'cncrg__TicketsCaseFieldSet';
	public static final String CASE_LIST_FIELD_SET				= 'cncrg__CaseListViewFieldSet';
	public static final String UNREAD_UPDATE_ON_CASE			= 'cncrg__Unread_Update_for_Contact__c';

	public static final String CASE_OBJECT_API					= 'Case';

	private static Set<String> IMAGE_TYPES = new Set<String> {
		'image/png',
		'image/jpeg',
		'image/gif'
	};

	private static Map<String, List<String>> SORT_ORDER_FIELDS = new Map<String, List<String>>();
	public static Map<String, List<String>> getSortOrderConfig() {
		if (SORT_ORDER_FIELDS.isEmpty()) {
			// Standard Case
			// @TODO move to the configuration in future to be able to use more fields
			SORT_ORDER_FIELDS.put(WorkWithCases.CASE_OBJECT_API, new List<String>{'IsClosed', 'CreatedDate'});

			Map<String, String> settings = WorkWithCases.getCustomCaseSettings();
			if (settings.get('SortOrder') != null && settings.get('ObjectName') != null) {
				List<String> customSort = (List<String>) JSON.deserialize(settings.get('SortOrder'), List<String>.class);
				SORT_ORDER_FIELDS.put((String)settings.get('ObjectName'), customSort);
			}
		}
		return SORT_ORDER_FIELDS;
	}

	@TestVisible
	private static Boolean isTest = false;

	private static Map<String, Map<String, Map<String, String>>> picklistValuesMap = new Map<String, Map<String, Map<String, String>>>();
	private static Map<String, Map<String, Schema.SObjectField>> objectsFieldsMap = new Map<String, Map<String, Schema.SObjectField>>();

	public static String getPicklistValue(String objectName, String fieldName, String fieldValue) {
		if (!picklistValuesMap.containsKey(objectName)) {
			picklistValuesMap.put(objectName, new Map<String, Map<String, String>>());
		}

		if (!picklistValuesMap.get(objectName).containsKey(fieldName)) {
			Map<String, Schema.SObjectField> objectFields = objectsFieldsMap.get(objectName);
			if (objectFields == null) {
				objectFields = Utils.getDescribeSObjectResult(objectName).fields.getMap();
				objectsFieldsMap.put(objectName, objectFields);
			}

			Map<String, String> picklistLabelsMap = new Map<String, String>();
			for (Schema.PicklistEntry item : objectFields.get(fieldName).getDescribe().getPickListValues()) {
				picklistLabelsMap.put(item.getValue(), item.getLabel());
			}
			picklistValuesMap.get(objectName).put(fieldName, picklistLabelsMap);
		}

		return picklistValuesMap.get(objectName).get(fieldName).get(fieldValue);
	}

	@AuraEnabled
	public static List<String> createNewCaseApex(String newCase, String requestArrayStr) {
		List<String> results = new List<String>();
		sObject caseRecord = (SObject)JSON.deserialize(newCase, SObject.class);
		CustomerExtensionBase uniqueClassMember = Utils.getNewBaseUniqueToolsInstance();

		if (caseRecord instanceof Case) {
			caseRecord = updateCaseTypesWithCustomValues((Case)caseRecord);
			cncrg__Concierge_Settings__c setting = cncrg__Concierge_Settings__c.getValues('Populate Contact ID on Case');
			if (setting != null && Boolean.valueOf(setting.Value__c)) {
				List<String> contactInfo = uniqueClassMember.getContactId(new List<String> {'true'});
				if (String.isNotEmpty(contactInfo[0])) {
					caseRecord.put('ContactId', contactInfo[0]);
				}
			}
			results = uniqueClassMember.saveCaseRecord(new List<String> { JSON.serialize(caseRecord), requestArrayStr });
		} else {
			results = uniqueClassMember.saveCustomCaseRecord(new List<String> { JSON.serialize(caseRecord) });
		}
		return results; 
	}

	public static Case updateCaseTypesWithCustomValues(Case newCase) {
		Map<String, String> mapOfAllCasetypes = new Map<String, String>();
		String caseTypesFromSetting = Utils.getValueFromConciergeSettings(CASE_TYPE_SETTING_NAME);
		if (String.isNotBlank(caseTypesFromSetting)) {
			for (Schema.PicklistEntry item : Schema.SObjectType.Case.fields.Type.getPicklistValues()) {
				mapOfAllCasetypes.put(
					item.getLabel(),
					item.getValue()
				);
			}
			newCase.Type = mapOfAllCasetypes.get(newCase.Type);
		}
		return newCase;
	}

	@AuraEnabled
	public static String getLogTicketComponentName() {
		return Utils.getValueFromConciergeSettings('Component Name for Log a Ticket');
	}

	@AuraEnabled
	public static List<Object> loadEmptyCase(String articleId, String articleType,
			cncrg__Case_Layout_Assignment__c settingCaseLayout, String masterVersionId, Boolean customObj) {

		articleType = Utils.isLightningKnowledgeEnabled() ? Utils.getLightningKnowledgeObjectName() : articleType;

		CustomerExtensionBase uniqueClassMember = Utils.getNewBaseUniqueToolsInstance();
		Map<String, String> customCaseSettings = (Map<String, String>) JSON.deserialize(
			uniqueClassMember.getCustomCaseSettings(null)[0],
			Map<String, String>.class
		);

		String customObjName = customObj ? customCaseSettings.get('ObjectName') : CASE_OBJECT_API;
		sObject theCase = (SObject) Type.forName(customObjName).newInstance();

		CaseWrapper cWrapper = new CaseWrapper();
		cWrapper.newCase = theCase;
		cWrapper.caseLayoutFieldSet = customObj ? customCaseSettings.get('CreateFieldSetName') : DEFAULT_CASE_CREATE_FEILD_SET;

		if (!customObj) {
			List<String> fieldsToCheck = new List<String> {'DataCategoryName', 'ParentId'};
			theCase.put('Origin', Utils.getValueFromConciergeSettings('Case Origin'));

			String categories = '';
			if (String.isNotBlank(articleId) && String.isNotBlank(articleType)) {
				cWrapper.articleId = articleId;
				if (articleType.endsWithIgnoreCase('kav') 
						&& ESAPI.securityUtils().isAuthorizedToView(articleType.removeEnd('kav') + 'DataCategorySelection', fieldsToCheck)) {
					String query = 'SELECT DataCategoryName FROM ' + articleType.removeEnd('kav') + 'DataCategorySelection' +
								   ' WHERE ParentId =: masterVersionId Limit 50000';
					List<sObject> listOfCategories = Database.query(query);
					if (isTest) {
						sObject dataCategoryObject = WorkWithActionsSettingsTest.getDataCategorySelectionRecord();
						if (dataCategoryObject != null) {
							listOfCategories.add(dataCategoryObject);
						}
					}
					for (sObject category : listOfCategories) {
						categories += ' ' + String.valueOf(category.get('DataCategoryName')).replace('_', ' ') + ';';
					}
					if (categories != '') {
						theCase.put('cncrg__Data_Categories__c', categories.removeEnd(';'));
					}
					buildTemplateField(cWrapper, articleId, articleType);
				}
			}

			// !IMPORTANT! default Case Layout Assignment recod.
			// !IMPORTANT! required for custom package extentions, if Default FS should be replaced by Custom FS 
			List<cncrg__Case_Layout_Assignment__c> defAssignment = [
				SELECT cncrg__Field_Set__c
				FROM cncrg__Case_Layout_Assignment__c
				WHERE cncrg__Article_Id__c = null 
					AND cncrg__Data_Category__c = null
					AND cncrg__Article_Type__c = null
					AND cncrg__Field_Set__c != null
				ORDER BY CreatedDate DESC
				LIMIT 1
			];

			if (settingCaseLayout != null && String.isNotBlank(settingCaseLayout.cncrg__Field_Set__c)) {
				cWrapper.caseLayoutFieldSet = settingCaseLayout.cncrg__Field_Set__c;
			}
			if ((settingCaseLayout == null || settingCaseLayout.cncrg__Field_Set__c == null) && !defAssignment.isEmpty()) {
				cWrapper.caseLayoutFieldSet = defAssignment.get(0).cncrg__Field_Set__c;
			}
		}

		// fields configuration based on Field Set
		List<FieldSetController.FieldSetMember> fieldsList = FieldSetController.getFields(
			customObjName,
			cWrapper.caseLayoutFieldSet
		);

		// run custom logic from Extention class if exists
		List<String> requestList = new List<String> {JSON.serialize(fieldsList), JSON.serialize(cWrapper)};
		List<String> responseList = uniqueClassMember.modifyFieldSetWrapper(requestList);
		fieldsList = (List<FieldSetController.FieldSetMember>) JSON.deserialize(responseList[0], List<FieldSetController.FieldSetMember>.class);
		// get results 
		cWrapper = (CaseWrapper) JSON.deserialize(responseList[1], CaseWrapper.class);
		return new List<Object> { cWrapper, fieldsList};
	}

	@AuraEnabled
	public static Map<String, String> getCustomCaseSettings() {
		CustomerExtensionBase uniqueClassMember = Utils.getNewBaseUniqueToolsInstance();
		return (Map<String, String>) JSON.deserialize(
			uniqueClassMember.getCustomCaseSettings(null)[0],
			Map<String, String>.class
		);
	}

	private static void buildTemplateField(CaseWrapper cWrapper, String articleId, String articleType) {
		SObject objectRecord = (SObject)Type.forName(articleType).newInstance();
		Schema.SObjectType article = objectRecord.getSObjectType();
		Map<String, Schema.SObjectField> mfields = article.getDescribe().fields.getMap();
		if (mfields.containsKey(ORG_NAME_PREFIX + TARGET_TEMPLATE_FIELD_NAME) && mfields.containsKey(ORG_NAME_PREFIX + TEXT_TEMPLATE_FIELD_NAME)) {
			String articleQuery = 'SELECT ' + ORG_NAME_PREFIX + TARGET_TEMPLATE_FIELD_NAME + ', ' + ORG_NAME_PREFIX + TEXT_TEMPLATE_FIELD_NAME + ' FROM ' + articleType + ' WHERE Id = \'' + articleId + '\'';
			List<sObject> articleRecords = Database.query(articleQuery);
			if (!articleRecords.isEmpty()) {
				sObject currentRecord = articleRecords[0];
				String targetField 	= (String) currentRecord.get(ORG_NAME_PREFIX + TARGET_TEMPLATE_FIELD_NAME);
				String textValue 	= (String) currentRecord.get(ORG_NAME_PREFIX + TEXT_TEMPLATE_FIELD_NAME);
				if (String.isNotBlank(targetField) && String.isNotBlank(textValue)) {
					try {
						cWrapper.newCase.put(targetField, textValue);
					} catch (Exception e) {}
				}
			}
		}
	}

	@AuraEnabled
	public static List<CaseTypesInfoWrapper> loadCaseTypesInfo() {
		List<CaseTypesInfoWrapper> listOfCaseRecordTypes = new List<CaseTypesInfoWrapper>();
		for (Schema.PicklistEntry item : Schema.SObjectType.Case.fields.Type.getPicklistValues()) {
			listOfCaseRecordTypes.add(
				new CaseTypesInfoWrapper(item.getLabel(), item.getValue())
			);
		}
		return listOfCaseRecordTypes;
	}

	@AuraEnabled
	public static Id saveTheChunk(Id parentId, String fileName, String base64Data, String contentType, String fileId, Boolean isAttachment) {
		if (fileId == '') {
			fileId = saveTheFile(parentId, fileName, base64Data, contentType, isAttachment);
		} else {
			appendToFile(fileId, base64Data, isAttachment);
		}
		return Id.valueOf(fileId);
	}

	public static Id saveTheFile(Id parentId, String fileName, String base64Data, String contentType, Boolean isAttachment) {
		Id idToReturn;
		if (isAttachment) {
			Attachment theAttachment = new Attachment(
				parentId = parentId,
				Body = EncodingUtil.base64Decode(base64Data),
				Name = fileName,
				ContentType = contentType
			);
			ESAPI.securityUtils().validatedInsert(new List<Attachment> {theAttachment});
			idToReturn = theAttachment.Id;
		} else {
			ContentVersion cv = new ContentVersion(
				VersionData = EncodingUtil.base64Decode(base64Data),
				Title = fileName,
				PathOnClient = fileName,
				IsMajorVersion = false
			);
			ESAPI.securityUtils().validatedInsert(new List<ContentVersion> {cv});
			idToReturn = cv.Id;
		}

		return idToReturn;
	}

	@Future
	private static void appendToFile(Id fileId, String base64Data, Boolean isAttachment) {
		if (isAttachment) {
			if (ESAPI.securityUtils().isAuthorizedToView('Attachment', new List<String> {'Id', 'Body'} )) {
				Attachment theAttachment = [SELECT Id, Body FROM Attachment WHERE Id = :fileId];
				String existingBody = EncodingUtil.base64Encode(theAttachment.Body);
				theAttachment.Body = EncodingUtil.base64Decode(existingBody + base64Data);
				ESAPI.securityUtils().validatedUpdate(new List<Attachment> {theAttachment});
			}
		} else {
			if (ESAPI.securityUtils().isAuthorizedToView('ContentVersion', new List<String> {'Id', 'VersionData'})) {
				ContentVersion cVersion = [SELECT Id, VersionData FROM ContentVersion WHERE Id = :fileId];
				String body = EncodingUtil.base64Encode(cVersion.VersionData) + base64Data;
				cVersion.VersionData = EncodingUtil.base64Decode(body);
				ESAPI.securityUtils().validatedUpdate(new List<ContentVersion> {cVersion});
			}
		}
	}

	@AuraEnabled
	public static CaseListWrapper getCaseById(String caseId) {
		CaseListWrapper caseDetailsWrapper = null;
		if (String.isBlank(caseId)) {
			return caseDetailsWrapper;
		}

		Boolean isStandardCase = caseId.startsWith('500');
		// object config
		String objectName = CASE_OBJECT_API;
		String fieldSetName = CASE_LIST_FIELD_SET;
		String statusField = 'Status';
		String subjectField = 'Subject';
		// fields config
		String statusIndicatorField = Utils.getValueFromConciergeSettings(STATUS_INDICATOR_SETTING_NAME);
		Set<String> caseFieldsToCheck = WorkWithCases.getRequiredFieldsForDetails();

		// custom case config
		if (!isStandardCase) {
			Map<String, String> settings = WorkWithCases.getCustomCaseSettings();
			objectName = settings.get('ObjectName');
			fieldSetName = settings.get('ListFieldSetName');
			statusIndicatorField = settings.get('Status Indicator');
			statusField = settings.get('Status');
			subjectField = settings.get('Subject');
			caseFieldsToCheck = WorkWithCases.getRequiredCustomCaseFieldsForDetails();
		}

		// fields to checklist
		if (String.isNotBlank(statusIndicatorField)) {
			caseFieldsToCheck.add(statusIndicatorField);
		}
		List<Schema.FieldSetMember> caseFieldSet = new List<Schema.FieldSetMember>();
		try {
			caseFieldSet = Utils.getSchemaFieldSetMembers(objectName, fieldSetName);
		} catch (Exception e) {
			// no object for this configuration found
		}
		for (Schema.FieldSetMember f : caseFieldSet) {
			caseFieldsToCheck.add(f.getFieldPath());
		}

		List<sObject> cases = new List<sObject>();
		if (ESAPI.securityUtils().isAuthorizedToView(objectName, new List<String>(caseFieldsToCheck))) {
			String query = 'SELECT ' + String.join((Iterable<String>)caseFieldsToCheck, ',');
			query += ' FROM ' + objectName;
			query += ' WHERE ID = \'' + String.escapeSingleQuotes(caseId) + '\'';
			try {
				cases = Database.query(query);
			}
			catch(Exception e) { }
		}

		if (!cases.isEmpty()) {
			if (isStandardCase) {
				caseDetailsWrapper = new CaseListWrapper((Case)cases.get(0), statusIndicatorField);
			} else {
				caseDetailsWrapper = new CaseListWrapper(cases.get(0), statusIndicatorField, false);
			}
			caseDetailsWrapper.populateFields(caseFieldSet, statusField, subjectField, objectName);
		}
		return caseDetailsWrapper;
	}

	@AuraEnabled
	public static List<CaseListWrapper> getMoreCases(Integer rows) {
		List<CaseListWrapper> listOfCaseListWrapper = new List<CaseListWrapper>();
		CustomerExtensionBase uniqueClassMember = Utils.getNewBaseUniqueToolsInstance();

		Set<String> caseFieldsToCheck = WorkWithCases.getRequiredFieldsForDetails();
		String statusIndicatorField = Utils.getValueFromConciergeSettings(STATUS_INDICATOR_SETTING_NAME);
		if (String.isNotBlank(statusIndicatorField)) {
			caseFieldsToCheck.add(statusIndicatorField);
		}

		List<Schema.FieldSetMember> caseFieldSet = Utils.getSchemaFieldSetMembers(CASE_OBJECT_API, CASE_LIST_FIELD_SET);
		for (Schema.FieldSetMember f : caseFieldSet) {
			caseFieldsToCheck.add(f.getFieldPath());
		}

		List<String> genericJSONParameters = new List<String>();
		genericJSONParameters.add( JSON.serialize(caseFieldsToCheck) );
		List<String> contactInfo = uniqueClassMember.getContactId(new List<String> {''});
		genericJSONParameters.add( (contactInfo.isEmpty() ? '' : contactInfo[0]) );
		genericJSONParameters.add( String.valueOf(rows) );

		List<Case> allCases = (List<Case>) JSON.deserialize(
			uniqueClassMember.getPackCasesList( genericJSONParameters )[0], List<Case>.class
		);
		for (Case theCase : allCases) {
			CaseListWrapper caseWrap = new CaseListWrapper(theCase, statusIndicatorField);
			caseWrap.populateFields(caseFieldSet, 'Status', 'Subject', CASE_OBJECT_API);
			listOfCaseListWrapper.add(caseWrap);
		}

		// load custom cases if required
		Map<String, String> customCaseSettings = (Map<String, String>) JSON.deserialize(
			uniqueClassMember.getCustomCaseSettings(null)[0],
			Map<String, String>.class
		);

		String customCaseObjectName = customCaseSettings.get('ObjectName');
		if (String.isNotBlank(customCaseObjectName)){
			listOfCaseListWrapper.addAll( getCustomCases(rows, uniqueClassMember) );
			listOfCaseListWrapper.sort();
		}

		if (listOfCaseListWrapper.size() > rows) {
			for(Integer i = listOfCaseListWrapper.size() - 1; i >= rows; i--) {
				listOfCaseListWrapper.remove(i);
			}
		}
		return listOfCaseListWrapper;
	}

	private static List<CaseListWrapper> getCustomCases(Integer rows, CustomerExtensionBase uniqueClassMember) {
		List<CaseListWrapper> listOfCaseListWrapper = new List<CaseListWrapper>();

		Map<String, String> customCaseSettings = (Map<String, String>) JSON.deserialize(
			uniqueClassMember.getCustomCaseSettings(null)[0],
			Map<String, String>.class
		);

		String objectName = customCaseSettings.get('ObjectName');
		String statusIndicatorField = customCaseSettings.get('Status Indicator');
		Set<String> customCaseFieldsToCheck = WorkWithCases.getRequiredCustomCaseFieldsForDetails();
		if (String.isNotBlank(statusIndicatorField)) {
			customCaseFieldsToCheck.add(statusIndicatorField);
		}

		List<Schema.FieldSetMember> customCaseFieldSet = new List<Schema.FieldSetMember>();
		try {
			customCaseFieldSet = Utils.getSchemaFieldSetMembers(objectName, customCaseSettings.get('ListFieldSetName'));
		} catch (Exception e) {
			// no object for this configuration found
			return listOfCaseListWrapper;
		}

		for (Schema.FieldSetMember f : customCaseFieldSet) {
			customCaseFieldsToCheck.add(f.getFieldPath());
		}
		List<SObject> allCustomCases = (List<SObject>) JSON.deserialize(
			uniqueClassMember.getPackCustomCasesList(
				new List<String>{JSON.serialize(customCaseFieldsToCheck), '' , String.valueOf(rows)}
			)[0],
			Type.forName('List<' + objectName + '>')
		); 

		for (SObject theCase : allCustomCases) {
			CaseListWrapper caseWrap = new CaseListWrapper(theCase, statusIndicatorField, false);
			caseWrap.populateFields(
				customCaseFieldSet, customCaseSettings.get('Status'), customCaseSettings.get('Subject'), objectName
			);
			listOfCaseListWrapper.add(caseWrap);
		}
		return listOfCaseListWrapper;
	}

	@AuraEnabled
	public static List<Object> getRelatedArticleData(String href, String prefix) {
		List<Object> initialDataList = RelatedArticleDetailsController.getInitialData(href, prefix);
		initialDataList.add(SearchQueryInputController.getLiveAgentPage()[0]);
		initialDataList.add(Utils.isUserHasPermitionToCreateCase());
		initialDataList.add(Utils.getValueFromConciergeSettings('Application Name'));
		return initialDataList;
	}

	@AuraEnabled
	public static ChatterDataAccessUtils.ChatterPage getAllFeedItems(Id caseId) {
		CustomerExtensionBase uniqueClassMember = Utils.getNewBaseUniqueToolsInstance();
		return (ChatterDataAccessUtils.ChatterPage) JSON.deserialize(uniqueClassMember.getAllFeedItems(new List<String> {caseId})[0], ChatterDataAccessUtils.ChatterPage.class);
	}

	@AuraEnabled
	public static ChatterDataAccessUtils.ChatterPage getNextChatterPage(Id caseId, String nextPageToken) {
		return ChatterDataAccessUtils.getNextPage(caseId, nextPageToken);
	}

	@AuraEnabled
	public static List<ChatterDataAccessUtils.FeedItemWrapper> getAllCommentsByFeedItem(String caseFeedId) {
		return ChatterDataAccessUtils.getAllCommentsByFeedItem(caseFeedId);
	}

	@AuraEnabled
	public static ChatterDataAccessUtils.FeedItemWrapper addNewFeedItem(Id caseId, String contentVersionId) {
		return addNewFeedItem(caseId, null, contentVersionId);
	}

	@AuraEnabled
	public static ChatterDataAccessUtils.FeedItemWrapper addNewFeedItem(Id caseId, String text, String contentVersionId) {
		return ChatterDataAccessUtils.addNewFeedItem(caseId, text, contentVersionId);
	}

	@AuraEnabled
	public static List<AttachmentInfo> loadAllCaseAttachments(String caseId) {
		List<Attachment> listOfAttachments = new List<Attachment>();
		List<String> fieldsToCheck = new List<String> {'Id', 'ContentType', 'Name', 'ParentId'};
		if (ESAPI.securityUtils().isAuthorizedToView('Attachment', fieldsToCheck)) {
			listOfAttachments = [SELECT Id, ContentType, Name FROM Attachment WHERE ParentId = : caseId LIMIT 50000];
		}
		List<AttachmentInfo> listOfAttachmentInfo = new List<AttachmentInfo>();
		for (Attachment theAttachment : listOfAttachments) {
			listOfAttachmentInfo.add(
				new AttachmentInfo(
					'/servlet/servlet.FileDownload?file=' + theAttachment.Id,
					IMAGE_TYPES.contains(theAttachment.ContentType),
					theAttachment.Name
				)
			);
		}
		return listOfAttachmentInfo;
	}

	@AuraEnabled
	public static List<FieldWrapper> getCaseWrapper(String caseId) {
		List<FieldWrapper> caseWrapperList = new List<FieldWrapper>();

		if (String.isNotBlank(caseId)) {
			String objectName = CASE_OBJECT_API;
			String fieldSetName = CASE_DETAIL_FIELD_SET;

			if (!caseId.startsWith('500')) {
				Map<String, String> settings = getCustomCaseSettings();
				objectName = settings.get('ObjectName');
				fieldSetName = settings.get('DetailFieldSetName');
			}

			List<Schema.FieldSetMember> fieldSetMembers = Utils.getSchemaFieldSetMembers(objectName, fieldSetName);
			Map<String, Schema.SObjectField> fieldsInfoMap = Utils.getDescribeSObjectResult(objectName).fields.getMap();

			if (checkAccessToCaseFieldSet(fieldSetMembers, objectName)) { //checking Case object and Field Set values on CRUD/FLS
				String query = prepareQuery(fieldSetMembers, objectName, caseId);
				SObject caseRecord = (SObject) Database.query(query);
				caseWrapperList = prepareRecords(fieldSetMembers, caseRecord, fieldsInfoMap);
			}
		}
		return caseWrapperList;
	}

	private static String prepareQuery(List<Schema.FieldSetMember> fieldSetMembers, String objectName, String caseId) {
		String query = 'SELECT Id,';
		for (Schema.FieldSetMember field : fieldSetMembers) {
			String fieldName = field.getFieldPath();
			if (String.valueOf(field.getType()) == 'PICKLIST') {
				query += 'toLabel(' + fieldName + '),';
			}
			else {
				query += fieldName + ',';
			}
			if (String.valueOf(field.getType()) == 'REFERENCE') {
				if (fieldName.endsWithIgnoreCase('__c')) {
					query += fieldName.removeEndIgnoreCase('c') + 'r.Name,';
				} else if (fieldName.endsWithIgnoreCase('id')) {
					query += fieldName.removeEndIgnoreCase('id') + '.Name,';
				}
			}
		}
		query = query.removeEnd(',');
		query += ' FROM ' + objectName;
		query += ' WHERE Id =: caseId';
		return query;
	}

	private static List<FieldWrapper> prepareRecords(List<Schema.FieldSetMember> fieldSetMembers,
			SObject caseRecord,
			Map<String, Schema.SObjectField> fieldsInfoMap) {

		List<FieldWrapper> listOfFields = new List<FieldWrapper>();
		Object displayValue;
		Object value;

		for (Schema.FieldSetMember field : fieldSetMembers) {
			String fieldName = field.getFieldPath();
			Boolean parentField = fieldName.contains('.');
			Schema.DescribeFieldResult fieldDescribe;

			if (!parentField) {
				fieldDescribe = fieldsInfoMap.get(fieldName).getDescribe();
			}

			if (parentField || fieldDescribe.isAccessible()) {
				String parentObjectType;
				String label = field.getLabel();
				String name = fieldDescribe.getName();
				String type = String.valueOf(field.getType());

				if (parentField) {
					List<String> parentList = fieldName.split('\\.');
					if (caseRecord.getSobject(parentList[0]) != null) {
						displayValue = caseRecord.getSobject(parentList[0]).get(parentList[1]);
					}
				} else {
					displayValue = caseRecord.get(fieldName);
				}

				value = displayValue;
				if (type == 'REFERENCE') {
					parentObjectType = String.valueOf(fieldsInfoMap.get(fieldName).getDescribe().getReferenceTo()[0]);
					if (fieldName == 'RecordTypeId') {
						type = 'STRING';
					}
					if (fieldName.endsWithIgnoreCase('__c') && displayValue != null) {
						displayValue = caseRecord.getSobject(fieldName.removeEndIgnoreCase('c') + 'r').get('Name');
					} else if (fieldName.endsWithIgnoreCase('id')) {
						if (displayValue != null && caseRecord.getSobject(fieldName.removeEndIgnoreCase('id')) != null)
							displayValue = caseRecord.getSobject(fieldName.removeEndIgnoreCase('id')).get('Name');
						label = label.removeEndIgnoreCase('id').trim();
					}
				}

				FieldWrapper fieldRecord = new FieldWrapper(
					type,
					displayValue,
					value,
					label,
					name
				);
				listOfFields.add(fieldRecord);
			}
		}
		return listOfFields;
	}

	private static Boolean checkAccessToCaseFieldSet(List<Schema.FieldSetMember> fieldSetMembers, String sObjectName) {
		List<String> fieldsToCheck = new List<String>();
		for (Schema.FieldSetMember field : fieldSetMembers) {
			String fieldName = field.getFieldPath();
			fieldsToCheck.add(fieldName);
		}
		return ESAPI.securityUtils().isAuthorizedToView(sObjectName, fieldsToCheck);
	}

	public class FieldWrapper {
		@AuraEnabled
		public String type {get; set;}
		@AuraEnabled
		public String displayValue {get; set;}
		@AuraEnabled
		public Object value {get; set;}
		@AuraEnabled
		public String label {get; set;}
		@AuraEnabled
		public String name {get; set;}

		public FieldWrapper(String type, Object displayValue, Object value, String label, String name) {
			this.type = type;
			if (type == 'DATE' || type == 'DATETIME') {
				DateTime dateTimeField = (DateTime) displayValue;
				if (dateTimeField != null) {
					if (type == 'DATE') {
						this.displayValue = dateTimeField.formatGMT('MMM dd YYYY');
					} else {
						this.displayValue = dateTimeField.format('MMM dd hh:mm a YYYY');
					}
				}
			} else {
				this.displayValue = String.valueOf(displayValue);
			}
			this.value = value;
			this.label = label;
			this.name = name;
		}
	}

	@AuraEnabled
	public static Map<String, String> loadCasesSettings() {
		Map<String, String> caseSettings = new Map<String, String>();
		caseSettings.put('defaultNumberCases', Utils.getValueFromConciergeSettings('Number of Cases Displayed'));
		caseSettings.put('numberChunksCase', Utils.getValueFromConciergeSettings('Cases per Chunk'));
		caseSettings.put('logCasesFromMyTicketsTab', Utils.getValueFromConciergeSettings('Log Cases From My Tickets Tab'));
		caseSettings.put('isUserHasPermitionToCreateCase', String.valueOf(Utils.isUserHasPermitionToCreateCase()));
		caseSettings.put('dynamicComponentName', Utils.getDynamicComponentName('CaseComment'));

		CustomerExtensionBase uniqueClassMember = Utils.getNewBaseUniqueToolsInstance();
		caseSettings.put('customCaseSettings', uniqueClassMember.getCustomCaseSettings(null)[0]);
		try {
			caseSettings.put('contactInfo', uniqueClassMember.getContactId(new List<String> {''})[0]);
		} catch (Exception e) {}
		return caseSettings;
	}

	@AuraEnabled
	public static Integer getCaseUpdatesCount(String contactId) {
		CustomerExtensionBase uniqueClassMember = Utils.getNewBaseUniqueToolsInstance();
		return String.isNotBlank(contactId)
			? Integer.valueOf(uniqueClassMember.getCaseUpdatesCount(new List<String> {contactId})[0])
			: 0;
	}

	@AuraEnabled
	public static void markUpdatesAsRead(String caseWrapperJSON) {
		CaseListWrapper caseWrapper = (CaseListWrapper)JSON.deserialize(caseWrapperJSON, CaseListWrapper.class);
		SObject theCase = caseWrapper.c;
		if (theCase != null && theCase.Id != null && String.valueOf(theCase.Id).startsWith('500')) {
			theCase.put(UNREAD_UPDATE_ON_CASE, false);
			Utils.WithoutSharingUtility withoutSharingUtilityInstance = new Utils.WithoutSharingUtility();
			withoutSharingUtilityInstance.updateCases(new List<Case> {(Case)theCase});
		}
	}

	@AuraEnabled
	public static List<String> getConciergeSetting() {
		String intervalValue = Utils.getValueFromConciergeSettings('Update Interval');
		CustomerExtensionBase uniqueClassMember = Utils.getNewBaseUniqueToolsInstance();
		List<String> contactInfo = new List<String>{''};
		try {
			contactInfo = uniqueClassMember.getContactId(new List<String> {''});
		} catch (Exception e) {}
		return new List<String> {intervalValue, contactInfo[0] };
	}
	
	@AuraEnabled
	public static Integer getNotificationCircleTabNumber() {
		Integer myTicketsTabPosition = 1;
		String userLanguage = UserInfo.getLanguage();
		List<Network> activeCommunities = [SELECT Id, Name FROM Network WHERE Id = :Network.getNetworkId()];

		if (!activeCommunities.isEmpty()) {
			String navigationMenuQuery = 'SELECT Target, Label FROM NavigationMenuItem WHERE NavigationLinkSet.NetworkId = \'' + activeCommunities[0].Id + '\' ORDER BY Position';
			List<SObject> navList = Database.query(navigationMenuQuery);
			for ( SObject navItem : navList ) {
				if ( (String)navItem.get('Target') == '/my-tickets' ) {
					break;
				}
				myTicketsTabPosition++;
			}
		}
		return myTicketsTabPosition;
	}

	@AuraEnabled
	public static Boolean isUserHasAccessToCase() {
		return Case.sObjectType.getDescribe().isAccessible();
	}

	public static Set<String> getRequiredFieldsForDetails() {
		return new Set<String> {
			'Id',
			'CaseNumber',
			'CreatedDate',
			'CreatedById',
			'Description',
			'LastModifiedDate',
			'Priority',
			'Status',
			'Subject',
			'isClosed',
			UNREAD_UPDATE_ON_CASE
		};
	}

	public static Set<String> getRequiredCustomCaseFieldsForDetails() {
		return new Set<String> {
			'Id',
			'CreatedDate',
			'CreatedById',
			'LastModifiedDate'
		};
	}

	public class CaseListWrapper implements Comparable {
		@AuraEnabled
		public SObject c {get; set;}
		@AuraEnabled
		public String Color {get; set;}
		@AuraEnabled
		public List<String> fields {get; set;}
		@AuraEnabled
		public Boolean showDots {get; set;}
		@AuraEnabled
		public Boolean containUpdates {get; set;}
		private String objectName {get; set;}

		public CaseListWrapper() {
			this.containUpdates	= false;
			this.showDots		= false;
			this.fields			= new List<String>();
			this.objectName		= WorkWithCases.CASE_OBJECT_API;
		}

		public CaseListWrapper(Case oneCase, String statusIndicatorField) {
			this();
			this.c = oneCase;
			this.containUpdates	= oneCase.cncrg__Unread_Update_for_Contact__c; 
			this.setIndicationColor(statusIndicatorField);
		}

		public CaseListWrapper(SObject oneCase, String statusIndicatorField, Boolean containUpdates) {
			this();
			this.c = oneCase;
			this.setIndicationColor(statusIndicatorField);
		}

		private void setIndicationColor(String statusIndicatorField) {
			if (String.isNotBlank(statusIndicatorField)) {
				this.Color = (String) this.c.get(statusIndicatorField);
			}
		}

		public void populateFields(List<Schema.FieldSetMember> targetFields, String status, String subject, String objectName) {
			this.objectName = objectName;

			for (Schema.FieldSetMember field : targetFields) {
				String typeField = String.valueOf(field.getType());
				String fieldPath = field.getFieldPath();
				String value = String.valueOf(this.c.get(fieldPath));

				if (typeField == 'DATETIME' && value != null) {
					value = DateTime.valueOf(value).format('MMM dd, yyyy');
				} else if (typeField == 'Picklist') {
					value = getPicklistValue(objectName, fieldPath, value);
				}

				value = String.isEmpty(value) ? '' : value;
				if (fieldPath == status || fieldPath == subject) {
					fields.add(value);
				} else {
					fields.add(field.getLabel() + ': ' + value);
				}
			}
		}

		public Integer compareTo(Object compareTo) {
			CaseListWrapper compareObject = (CaseListWrapper) compareTo;
			Map<String, List<String>> sortConfig = WorkWithCases.getSortOrderConfig();

			List<String> currentConfig = sortConfig.get( this.objectName );
			List<String> compareToConfig = sortConfig.get( compareObject.objectName );

			Integer sortResult = 0;
			for (Integer fieldNumber = 0; fieldNumber < currentConfig.size(); fieldNumber++) {
				Object currentVal = this.c.get(currentConfig[fieldNumber]);
				Object compareVal = compareObject.c.get(compareToConfig[fieldNumber]);

				if (currentVal == compareVal) {
					continue;
				}

				//@TODO field types required for comparising
				//i.e. if FieldType == Date || DateTime etc
				if ('CreatedDate' == currentConfig[fieldNumber]) {
					sortResult = ((DateTime)currentVal < (DateTime)compareVal) ? 1 : -1;
				} else {
					Integer currentBool = currentVal != null && (Boolean)currentVal ? 1 : 0;
					Integer compareBool = compareVal != null && (Boolean)compareVal ? 1 : 0;
					sortResult = (currentBool > compareBool) ? 1 : -1;
				}
				break;
			}
			return sortResult;
		}
	}

	public class CaseTypesInfoWrapper {
		@AuraEnabled
		public String caseTypeLabel {get; set;}
		@AuraEnabled
		public String caseTypeName {get; set;}

		public CaseTypesInfoWrapper(String label, String name) {
			this.caseTypeLabel = label;
			this.caseTypeName = name;
		}
	}

	public class AttachmentInfo {
		@AuraEnabled public String url {get; set;}
		@AuraEnabled public String name {get; set;}
		@AuraEnabled public Boolean isImage {get; set;}

		public AttachmentInfo(String url, Boolean isImage, String name) {
			this.url = url;
			this.isImage = isImage;
			this.name = name;
		}
	}
}