public with sharing class LiveAgentIdController {
    
    public String deploymentURL {get;set;}
    public String chat {get;set;}
    public String deploymentId {get;set;}
    public String liveOrgId {get;set;}
    public String buttonId {get;set;}
	public sObject live_Agent_Settings {get; set;}
	
    
    public LiveAgentIdController() {
        live_Agent_Settings = cncrg__Live_Agent_Settings__c.getOrgDefaults();
        if(live_Agent_Settings != null) {
            deploymentURL 	= (String) live_Agent_Settings.get('cncrg__Deployment_URL__c');
            chat 			= (String) live_Agent_Settings.get('cncrg__Chat__c');
            deploymentId 	= (String) live_Agent_Settings.get('cncrg__Deployment_Id__c');
            liveOrgId 		= (String) live_Agent_Settings.get('cncrg__Org_Id__c');
            buttonId 		= (String) live_Agent_Settings.get('cncrg__Button_Id__c');
        }
    }
}