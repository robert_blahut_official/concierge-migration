@isTest 
private class ReviewArticleDetailsControllerTest {

	@TestSetup static void setup(){
		Concierge_Settings__c testREVIEW_NOT_LIKED_CUSTOM_SETTINGS_NAME = new Concierge_Settings__c();
		testREVIEW_NOT_LIKED_CUSTOM_SETTINGS_NAME.Name = 'Review date based on user activity';
		testREVIEW_NOT_LIKED_CUSTOM_SETTINGS_NAME.Value__c = '2';
		Concierge_Settings__c testAUTOMATICALLY_REVIEW_CUSTOM_SETTINGS_NAME = new Concierge_Settings__c();
		testAUTOMATICALLY_REVIEW_CUSTOM_SETTINGS_NAME.Name = 'Review articles after _ days';
		testAUTOMATICALLY_REVIEW_CUSTOM_SETTINGS_NAME.Value__c = '3';
		List<Concierge_Settings__c> setList = new List<Concierge_Settings__c>();
		setList.add(testREVIEW_NOT_LIKED_CUSTOM_SETTINGS_NAME);
		setList.add(testAUTOMATICALLY_REVIEW_CUSTOM_SETTINGS_NAME);
		insert setList;

		List<SchedulerSettings__c> schedSettingList = new List<SchedulerSettings__c>{
				new SchedulerSettings__c(
						Name = TurboEncabulatorScheduler.TURBO_BATCH_NAME,
						Class_Name__c = TurboEncabulatorScheduler.TURBO_BATCH_NAME,
						CRON__c = TurboEncabulatorScheduler.TURBO_CRON,
						Batch_Size__c = TurboEncabulatorScheduler.TURBO_BATCH_SIZE,
						Description__c = TurboEncabulatorScheduler.TURBO_DESCRIPTION,
						Active__c = true
				),
				new SchedulerSettings__c(
						Name = TurboEncabulatorScheduler.REVIEW_BATCH_NAME,
						Class_Name__c = TurboEncabulatorScheduler.REVIEW_BATCH_NAME,
						CRON__c = TurboEncabulatorScheduler.REVIEW_CRON,
						Batch_Size__c = TurboEncabulatorScheduler.REVIEW_BATH_SIZE,
						Description__c = TurboEncabulatorScheduler.REVIEW_DESCRIPTION,
						Active__c = true
				)
		};
		Database.insert(schedSettingList, false);

		String objectType = WorkWithArticlesTest.getKavObjectName();

		cncrg__Search_Types_Settings__c setting = new cncrg__Search_Types_Settings__c(
                Name = objectType,
				cncrg__Article_Type_API_Name__c = objectType,
                cncrg__Active__c = true,
                cncrg__Fields_To_Display_1__c = 'OwnerId'
            );
		List<cncrg__Search_Types_Settings__c> settings = new List<cncrg__Search_Types_Settings__c>();
		settings.add(setting);
		ESAPI.securityUtils().validatedInsert(settings);

		
		
        if(objectType != null) {
            sObject obj = Schema.getGlobalDescribe().get(objectType).newSObject();
            obj.put('Title','test apex');
            obj.put('Language','en_US');
            obj.put('UrlName','test-apex');
            obj.put('Summary','test test test test');
			obj.put('Language','en_US');
			List<sObject> articles = new List<sObject>{obj};
            insert articles;

            List<Id> listOfIds = new List<Id>();
            for (sObject article : articles)
                listOfIds.add((String)article.get('Id'));

            articles = Database.query('SELECT KnowledgeArticleId FROM ' + objectType + ' WHERE Id IN: listOfIds');

            KbManagement.PublishingService.publishArticle((Id)articles[0].get('KnowledgeArticleId'), true);
		
		Article_Review__c artReview = new Article_Review__c(
					Name = 'test name',
					Language__c = 'en_US',
					Article_Type__c = 'Knowledge',
		            Parent_Article__c = (String)articles[0].get('KnowledgeArticleId'),
		            Expiration_Date__c = Datetime.now().addHours(1));
		insert artReview;
		Datetime dt = Datetime.now().addMinutes(-5);
		Long createTime = dt.getTime();
		Test.setCreatedDate(artReview.Id,Datetime.newInstance(createTime));

		Article_Review__c artReview2 = new Article_Review__c(
					Name = 'test name2',
					Language__c = null,
					Article_Type__c = 'Knowledge',
		            Parent_Article__c = (String)articles[0].get('KnowledgeArticleId'),
		            Expiration_Date__c = Datetime.now().addHours(1));
		insert artReview2;
		Test.setCreatedDate(artReview2.Id,Datetime.newInstance(createTime));
		}
	}


	@isTest
	private static void getArticleTest() {
		Article_Review__c artReview = [SELECT Id,
											  Name,
											  Language__c,
											  CreatedDate,
											  Parent_Article__c,
											  LastModifiedDate,
											  Expiration_Date__c
									   FROM Article_Review__c
									   WHERE Name ='test name' 
									  ];
		Test.startTest();
		List<Object> result = ReviewArticleDetailsController.getArticle(artReview.Id);
		Test.stopTest();
		System.assertNotEquals(null,result);
	}

	@isTest
	private static void getNullArticleTest() {
		Article_Review__c artReview = [SELECT Id,
											  Name,
											  Language__c,
											  CreatedDate,
											  Parent_Article__c,
											  LastModifiedDate,
											  Expiration_Date__c
									   FROM Article_Review__c
									   WHERE Name ='test name' 
									  ];
		Test.startTest();
		List<Object> result = ReviewArticleDetailsController.getArticle('');
		Test.stopTest();
		System.assertNotEquals(null,result);
	}

	@isTest
	private static void moveToEditTest() {

		Article_Review__c artReview = [SELECT Id,
											  Name,
											  Language__c,
											  CreatedDate,
											  Parent_Article__c,
											  LastModifiedDate,
											  Expiration_Date__c
									   FROM Article_Review__c
									   WHERE Name ='test name' 
									  ];
		String checkResultString = '/knowledge/publishing/articleEdit.apexp?fromPage=RENDERER&id=' + artReview.Parent_Article__c + '&retURL=' + artReview.Id;
		Test.startTest();
		String result = ReviewArticleDetailsController.moveToEdit(artReview.Id);
		Test.stopTest();
		System.assertEquals(checkResultString,result);
	}
	@isTest
	private static void moveToEditTestNOLang() {
		Article_Review__c artReview = [SELECT Id,
											  Name,
											  Language__c,
											  CreatedDate,
											  Parent_Article__c,
											  LastModifiedDate,
											  Expiration_Date__c
									   FROM Article_Review__c
									   WHERE Name ='test name2' 
									  ];
		String checkResultString = '/knowledge/publishing/articleTranslationEdit.apexp?fromPage=RENDERER&id=' + artReview.Parent_Article__c + '&lang=' + artReview.Language__c + '&retURL=' + artReview.Id;
		Test.startTest();
		String result = ReviewArticleDetailsController.moveToEdit(artReview.Id);
		Test.stopTest();
		System.assertEquals(checkResultString,result);
	}

	@isTest
	private static void reviewArticleTest() {
		Article_Review__c artReview = [SELECT Id,
											  Name,
											  Language__c,
											  CreatedDate,
											  Parent_Article__c,
											  LastModifiedDate,
											  Last_Reviewed_Date__c,
											  Expiration_Date__c
									   FROM Article_Review__c
									   WHERE Name ='test name' 
									  ];
		Test.startTest();
		ReviewArticleDetailsController.reviewArticle(artReview.Id);
		Test.stopTest();
		artReview = [SELECT Id,
											  Name,
											  Language__c,
											  CreatedDate,
											  Parent_Article__c,
											  LastModifiedDate,
											  Last_Reviewed_Date__c,
											  Expiration_Date__c
									   FROM Article_Review__c
									   WHERE Name ='test name' 
									  ];
		System.assertEquals(Date.today(),artReview.Last_Reviewed_Date__c);

	}
}