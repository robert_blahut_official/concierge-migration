public class KnowledgeDataCategoryService {
	private static String SESSION_ID;

	public static final String ENDPOINT = URL.getOrgDomainUrl().toExternalForm() + '/services/data/v45.0';
	public static final String DATA_CATEGORY_GROUPS_URL = '/support/dataCategoryGroups?sObjectName=KnowledgeArticleVersion';
	public static final String DATA_CATEGORY_URL = '/support/dataCategoryGroups/{0}/dataCategories/{1}?sObjectName=KnowledgeArticleVersion';

	public static TopCategoryWrapper getDetailsByDataCategory(String mainCategoryName, String currentCategoryName, String sessionId) {
		String currentUrl = String.format(DATA_CATEGORY_URL, new List<String> {mainCategoryName, currentCategoryName});
		SESSION_ID = sessionId;
		String jsonResponse = getResponse(ENDPOINT + currentUrl, 'GET');

		return (TopCategoryWrapper)JSON.deserialize(jsonResponse, TopCategoryWrapper.class);
	}

	public static DataCategoryContainer requestToDataCategory(String sessionId) {
		SESSION_ID = sessionId;
		return requestToDataCategory();
	}

	public static DataCategoryContainer requestToDataCategory() {
		String sessionId = getSessionId();
		String jsonResponse = getResponse(ENDPOINT + DATA_CATEGORY_GROUPS_URL, 'GET');

		return (DataCategoryContainer)JSON.deserialize(jsonResponse, DataCategoryContainer.class);
	}

	public static List<DataCategoryWrapper> parseDataCategory(DataCategoryContainer container) {
		List<DataCategoryWrapper> result = new List<DataCategoryWrapper>();

		for (DataCategoryGroupWrapper mainCat : container.categoryGroups) {
			DataCategoryWrapper currentCategory = new DataCategoryWrapper(mainCat.name);

			for (TopCategoryWrapper topCategory: mainCat.topCategories) {
				currentCategory.childList.add(
					new DataCategoryWrapper( topCategory.name, currentCategory.endPoint )
				);
			}

			result.add(currentCategory);
		}
		return result;
	}

	public static List<String> getAllCategoriesForMe() {
		Set<String> categries = new Set<String>();
		List<String> branchList = getDataCategoryTeeFromFile();
		List<String> topCategories = getTopLevelDataCategories();

		for (String branch : branchList) {
			for (String topCategory : topCategories) {
				List<String> currentBranch = topCategory.split('\\.');

				if (branch.startsWith(currentBranch[0]) && branch.contains(currentBranch[1])) {
					String tempSting = branch.replaceFirst(currentBranch[0] + '.', '');
					for (String category : tempSting.split('\\.')) {
						categries.add(currentBranch[0] + '.' + category);
					}
				}
			}
		}

		List<String> outputCategories = new List<String>(categries);
		outputCategories.sort();
		return outputCategories;
	}

	public static Map<String, Set<String>> getMapOfDataCategories(Set<String> allIds, Set<String> allTypes) {
		// SOQL request in the loop, but no more than 10 requests
		List<sObject> allCategoriesRecords = new List<sObject>();
		Map<String, Set<String>> mapOfCategories = new Map<String, Set<String>> ();
		List<String> fieldsToCheck = new List<String> { 'ParentId', 'DataCategoryName' };

		Integer counter = 0;
		for (String type : allTypes) {
			if (type.endsWithIgnoreCase('kav') && ESAPI.securityUtils().isAuthorizedToView(type.removeEnd('kav') + 'DataCategorySelection', fieldsToCheck)) {
				counter++;
				if (counter> 10) break;
				String query = 'SELECT ParentId, DataCategoryName FROM ' + String.escapeSingleQuotes(type.removeEnd('kav')) + 'DataCategorySelection WHERE ParentId IN: allIds';
				allCategoriesRecords.addAll(Database.query(query));
			}
		}

		if (Test.isRunningTest() ) {
			sObject dataCategoryObject = WorkWithActionsSettingsTest.getDataCategorySelectionRecord();
			if (dataCategoryObject != null) {
				allCategoriesRecords.add(dataCategoryObject);
			}
		}

		for (sObject categoryRecord : allCategoriesRecords) {
			Set<String> tempSetOfCategories = mapOfCategories.get((String) categoryRecord.get('ParentId'));
			if (tempSetOfCategories == null) {
				tempSetOfCategories = new Set<String> ();
			}
			tempSetOfCategories.add((String) categoryRecord.get('DataCategoryName'));
			mapOfCategories.put((String) categoryRecord.get('ParentId'), tempSetOfCategories);
		}
		return mapOfCategories;
	}

	private static List<String> getDataCategoryTeeFromFile() {
		List<String> branchList = new List<String>();
		ContentVersion categoryData = DataCategoryTreeBatch.getDataCategoryTeeFromFile(false);

		if (categoryData != null && categoryData.versionData != null) {
			List<DataCategoryWrapper> tree = (List<DataCategoryWrapper>)JSON.deserialize(
				categoryData.versionData.toString(),
				List<DataCategoryWrapper>.class
			);
			branchList = convertToList(tree);
			branchList.sort();
		}
		return branchList;
	}

	private static List<String> getTopLevelDataCategories() {
		List<DataCategoryWrapper> myGroupList = parseDataCategory(
			requestToDataCategory()
		);
		List<String> topList = convertToList(myGroupList);
		topList.sort();
		return topList;
	}

	private static List<String> convertToList(List<DataCategoryWrapper> tree) {
		List<String> result = new List<String>();
		for (Integer i = 0; i < tree.size(); i++) {
			if (tree[i].endPoint.contains('.')) {
				result.add(tree[i].endPoint);
			}
			if (!tree[i].childList.isEmpty()) {
				result.addAll(convertToList(tree[i].childList));
			}
		}
		return result;
	}

	private static String getResponse(String endpoint, String method) {
		Map<String, String> headers = new Map<String, String> {
			'Authorization' => 'Bearer ' + SESSION_ID,
			'Content-Type' => 'application/json'
		};

		HttpRequest req = new HttpRequest();
		for (String key : headers.keySet()) {
			req.setHeader(key, headers.get(key));
		}
		req.setEndpoint(endpoint);
		req.setMethod(method);

		String responseBody = '';
		try {
			HttpResponse res = new Http().send(req);
			responseBody = res.getBody();
		} catch (Exception e) {
			responseBody = 'Error:' + e.getMessage();
		}
		return responseBody;
	}

	private static String getSessionId() {
		if (SESSION_ID == null) {
			String vfContent = Test.isRunningTest() ? 'Start_Of_Session_IdmySessionIdEnd_Of_Session_Id' : Page.GetSessionId.getContent().toString();
			Integer startP = vfContent.indexOf('Start_Of_Session_Id') + 'Start_Of_Session_Id'.length();
			Integer endP = vfContent.indexOf('End_Of_Session_Id');
			// Get the Session Id<br />
			SESSION_ID = vfContent.substring(startP, endP);
		}
		return SESSION_ID;
	}

	/**************************************************/
	/****************** INNER CLASSES *****************/
	/**************************************************/
	public class DataCategoryContainer {
		public List<DataCategoryGroupWrapper> categoryGroups {get; set;}
	}

	public class DataCategoryGroupWrapper {
		public String label {get; set;}
		public String name {get; set;}
		public String objectUsage {get; set;}
		public List<TopCategoryWrapper> topCategories {get; set;}
	}

	public class TopCategoryWrapper {
		public List<TopCategoryWrapper> childCategories {get; set;}
		public String label {get; set;}
		public String name {get; set;}
		public String url {get; set;}
	}

	public class DataCategoryWrapper {
		public List<DataCategoryWrapper> childList;
		public String dataCategoryName;
		public Boolean isCollapse;
		public String endPoint;

		public DataCategoryWrapper(String dataCategoryName) {
			this.dataCategoryName = dataCategoryName;
			this.endPoint = dataCategoryName;
			this.childList = new List<DataCategoryWrapper>();
			this.isCollapse = true;
		}

		public DataCategoryWrapper(String dataCategoryName, String endPoint) {
			this(dataCategoryName);
			this.endPoint = endPoint + '.' + dataCategoryName;
		}
	}
}