/**
 * DEAR SECURITY REVIEWER!
 * The apex class is not using the “with or without sharing” because the class is referenced in package post install class.
 * Please see details in Salesforce documentation. 
 * https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_install_handler.htm
 * “Post install script can’t call another Apex class in the package if that Apex class uses the with sharing keyword.“
*/
global class FeedbackBatch extends AbstractConciergeBatch implements Database.AllowsCallouts {
	public static final String FEEDBACK_RECORD_TYPE_ID = Schema.SObjectType.Search_Staging__c.getRecordTypeInfosByName().get('Article Feedback').getRecordTypeId();

	public Database.QueryLocator start(Database.BatchableContext BC) {
		DateTime boundaryTracking = Datetime.now().addHours(1);
		return Database.getQueryLocator('SELECT Id, CreatedDate, Name, Biederman__c, Down__c, Profigliano__c, Schwartz__c, Up__c, Zoom__c, CreatedById FROM Search_Staging__c ' +
				'WHERE RecordTypeId = :FEEDBACK_RECORD_TYPE_ID AND ((Down__c = NULL AND CreatedDate <=: boundaryTracking) OR CreatedDate < LAST_N_DAYS:30)');
	}

	public void execute(Database.BatchableContext context, List<Search_Staging__c> scope) {
		List<Search_Staging__c> oldStagingRecords = new List<Search_Staging__c>();
		List<Search_Staging__c> actualStagingRecords = new List<Search_Staging__c>();
		for(Integer i = 0, j=scope.size(); i < j; i++) {
			if(String.isNotBlank(scope[i].Down__c)) {
				oldStagingRecords.add(scope[i]);
			}
			else {
				actualStagingRecords.add(scope[i]);
			}
		}
		scope = actualStagingRecords;
		List<Search_Staging__c> backupSearch_Staging = (List<Search_Staging__c>) JSON.deserialize(JSON.serialize(scope), List<Search_Staging__c>.class);
		List<WorkWithArticles.SearchStagingWrapper> searchStagingsList = new List<WorkWithArticles.SearchStagingWrapper>();
		Set<String> zoomSet = new Set<String>();
		try {
			for(Search_Staging__c item : scope) {
				WorkWithArticles.decryptedSearchStaging(item);
			}
			List<Article_Feedback__c> feedbackList = convertToFeedback(scope);
			if(!feedbackList.isEmpty()) {
				ESAPI.securityUtils().validatedInsert(feedbackList);
			}
			if(oldStagingRecords.size() > 0) {
				ESAPI.securityUtils().validatedDelete(oldStagingRecords);
				Database.emptyRecycleBin(oldStagingRecords);
			}
			ESAPI.securityUtils().validatedDelete(scope);
			Database.emptyRecycleBin(scope);
		}
		catch(Exception e) {
			for(Search_Staging__c item : backupSearch_Staging) {
				item.Down__c = e.getStackTraceString();
				item.Down__c += e.getMessage();
				item.Down__c = item.Down__c.length() > 255 ? item.Down__c.subString(0,254) : item.Down__c;
			}
			ESAPI.securityUtils().validatedUpdate(backupSearch_Staging);
		}
	}

	private static List<Article_Feedback__c> convertToFeedback(List<Search_Staging__c> scope) {
		List<Article_Feedback__c> result = new List<Article_Feedback__c>();
		List<String> currentBiederman;
		for(Integer i = 0; i < scope.size(); i++) {
			result.add((Article_Feedback__c)JSON.deserialize(scope[i].Biederman__c, Article_Feedback__c.class));
		}
		return result;
	}
}