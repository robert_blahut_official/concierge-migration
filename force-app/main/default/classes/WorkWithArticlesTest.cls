@isTest
public class WorkWithArticlesTest {

    @isTest
    public static void testWorkWithArticles() {
        Test.startTest();

        List<cncrg__Search_Types_Settings__c> settings = new List<cncrg__Search_Types_Settings__c>{
            new cncrg__Search_Types_Settings__c(
                Name = 'cncrg__Knowledge__kav',
				cncrg__Article_Type_API_Name__c = 'cncrg__Knowledge__kav',
                cncrg__Active__c = true,
                cncrg__Fields_To_Display_1__c = 'Summary,'
            ),
            new cncrg__Search_Types_Settings__c(
                Name = 'cncrg__Interactive_Article__kav',
				cncrg__Article_Type_API_Name__c = 'cncrg__Interactive_Article__kav',
                cncrg__Active__c = true,
                cncrg__Fields_To_Display_1__c = 'CreatedById,'
            ),
            new cncrg__Search_Types_Settings__c(
                Name = 'cncrg__How_To__kav',
				cncrg__Article_Type_API_Name__c = 'cncrg__How_To__kav',
                cncrg__Active__c = true,
                cncrg__Fields_To_Display_1__c = 'Language,'
            ),
            new cncrg__Search_Types_Settings__c(
                Name = 'cncrg__FAQ__kav',
				cncrg__Article_Type_API_Name__c = 'cncrg__FAQ__kav',
                cncrg__Active__c = true,
                cncrg__Fields_To_Display_1__c = 'OwnerId,'
            )
        };
        ESAPI.securityUtils().validatedInsert(settings);

        String nameOfDocument = 'DocumentLogo';
        List<cncrg__Concierge_Settings__c> conciergeSettings = new List<cncrg__Concierge_Settings__c>();


        cncrg__Concierge_Settings__c logoSetting = new cncrg__Concierge_Settings__c(
            Name = 'CompanyLogo',
            cncrg__Value__c = nameOfDocument
        );
        conciergeSettings.add(logoSetting);

        insert conciergeSettings;

        String standardFolderId = [
                                    SELECT Id
                                    FROM   Folder
                                    WHERE  Type = 'Document'
                                    LIMIT  1
        ].Id;

        Document document = new Document(
            Body = Blob.valueOf('Some Text'),
            ContentType = 'application/pdf',
            DeveloperName = 'my_document',
            IsPublic = true,
            Name = nameOfDocument,
            FolderId = standardFolderId
        );
        insert document;

        // Commented DK 11.04.2017 for delete dependency with Knowledge
        String objectType = WorkWithArticlesTest.getKavObjectName();
        if(objectType != null) {
			Schema.DescribeSObjectResult sObjectDescribe = TestClassUtility.getSobjectDescribe(objectType);
			sObject obj = sObjectDescribe.getSobjectType().newSObject();
            obj.put('Title','Unit Test 2 test');
            obj.put('Language', TestClassUtility.getAvailableKnowlegeLanguage(sObjectDescribe));
            obj.put('UrlName','test');
            obj.put('Summary','test test test test');
            List<sObject> articles = new List<sObject>{obj};
            insert articles;

            List<Id> listOfIds = new List<Id>();
            for (sObject article : articles) {
                listOfIds.add((String)article.get('Id'));
            }
            articles = Database.query('SELECT KnowledgeArticleId FROM ' + objectType + ' WHERE Id IN: listOfIds');
            KbManagement.PublishingService.publishArticle((Id)articles[0].get('KnowledgeArticleId'), true);

            List<cncrg__Favorite_article__c> favorites = new List<cncrg__Favorite_article__c>();
			WorkWithArticles.EncryptionMember member = new WorkWithArticles.EncryptionMember();
			String articleEncryptedId = member.encrypteData((String)articles[0].get('KnowledgeArticleId'));
            cncrg__Favorite_article__c favorite = new cncrg__Favorite_article__c(
                cncrg__ArticleId__c = articleEncryptedId,
                OwnerId = UserInfo.getUserId()
            );
            favorites.add(favorite);
            insert favorites;

            TestClassUtility.createSearchEngineConfig();

			AbstractSearchEngine.isTest = true;

            System.assert(WorkWithArticles.getAllFavoriteArticles('') != null);

            cncrg__Search_Types_Settings__c searchTypesSetting = new cncrg__Search_Types_Settings__c(
                Name = 'Interactive_Article__kav',
                cncrg__Fields_To_Display_1__c = 'first, second,',
                cncrg__Active__c = true
            );
            insert searchTypesSetting;

            System.assert(WorkWithArticles.getAllFavoriteArticles('prefix') != null);
            // Test deleteFavorite() method
            WorkWithArticles.deleteFavorite(articles[0].Id);
            List<cncrg__Favorite_article__c> deletedFavorite = [
                SELECT Id
                FROM   cncrg__Favorite_article__c
                WHERE  cncrg__ArticleId__c =: articleEncryptedId
            ];
            //System.assert(deletedFavorite.isEmpty());
        }
        Test.stopTest();
    }

    static testMethod void testGetFavoriteArticlesWrapperList() {
        cncrg__Search_Types_Settings__c setting = new cncrg__Search_Types_Settings__c(
            Name = 'cncrg__Knowledge__kav 1',
            cncrg__Active__c = true,
            cncrg__Fields_To_Display_1__c = 'Summary'
        );
        insert setting;

        // Commented DK 11.04.2017 for delete dependency with Knowledge
        List<sObject> objects = new List<sObject>();
        String articleType = WorkWithArticlesTest.getKavObjectName();
        /*if(articleType != null) {
            sObject knowledgeStandAlone = WorkWithArticlesTest.getKavRecord('{!User.FirstName} {!Document.CompanyLogo}', 'CreatedDate":"2017-04-06T08:26:50.000+0000', articleType, true);
            objects.add(knowledgeStandAlone);
            System.assert(String.isNotBlank(WorkWithArticles.getClassicFavoriteArticlesWrapperList(AbstractSearchEngine.SALESFORCE_SEARCH_TYPE, 'Logo', objects)));
        }*/

        List<sObject> objectsWithoutType = new List<sObject>();
        sObject knowledgeNotStandAlone = WorkWithArticlesTest.getKavRecord('{!User.FirstName} {!Document.CompanyLogo}', 'CreatedDate":"2017-04-06T08:26:50.000+0000', '', false);
		System.assert(knowledgeNotStandAlone != null);
        /*if(knowledgeNotStandAlone != null) {
            objectsWithoutType.add(knowledgeNotStandAlone);
            System.assert(String.isNotBlank(WorkWithArticles.getClassicFavoriteArticlesWrapperList(AbstractSearchEngine.SALESFORCE_SEARCH_TYPE, 'Logo', objectsWithoutType)));
            System.assert(String.isNotBlank(WorkWithArticles.getClassicFavoriteArticlesWrapperList('Not Salesforce', 'Logo', objectsWithoutType)));

            // Coverage for Modified date field of FavoriteArticlesWrapper wrapper
            FavoriteArticlesWrapper wrapper = new FavoriteArticlesWrapper(knowledgeNotStandAlone, 'Body');
            wrapper.Modified = 'Modified value';
        }*/

        /*List<sObject> objectsWithoutType = new List<sObject>();
        String knowledgeArticleWithoutType = '{"attributes":{"type":"cncrg__Knowledge__kav"},"Summary":"{!User.FirstName} {!Document.CompanyLogo}",' +
            '"CreatedDate":"2017-04-06T08:26:50.000+0000"}';
        sObject knowledgeNotStandAlone = (cncrg__Knowledge__kav)JSON.deserialize(knowledgeArticleWithoutType, cncrg__Knowledge__kav.class);
        objectsWithoutType.add(knowledgeNotStandAlone);

        System.assert(String.isNotBlank(WorkWithArticles.getClassicFavoriteArticlesWrapperList(AbstractSearchEngine.SALESFORCE_SEARCH_TYPE, 'Logo', objectsWithoutType)));
        System.assert(String.isNotBlank(WorkWithArticles.getClassicFavoriteArticlesWrapperList('Not Salesforce', new List<String> {'Logo'}, objectsWithoutType)));

        // Coverage for Modified date field of FavoriteArticlesWrapper wrapper
        WorkWithArticles.FavoriteArticlesWrapper wrapper = new WorkWithArticles.FavoriteArticlesWrapper(knowledgeNotStandAlone, 'Body');
        wrapper.Modified = 'Modified value';*/
    }

    public static sObject getKavRecord(String summary, String createdDate, String articleType, Boolean isArticleType) {
        for(Schema.SObjectType o : Schema.getGlobalDescribe().values()) {
            Schema.DescribeSObjectResult objResult = o.getDescribe();
            if(objResult.getName().endsWith('kav')) {
                String objectName = objResult.getName();
                String kavJSON;
                if(isArticleType) {
                    kavJSON = '{"attributes":{"type":"' + objectName + '"},"Summary":"' + summary + '",' +
                        '"' + createdDate + '","ArticleType":"' + articleType + '"}';
                } else {
                    kavJSON = '{"attributes":{"type":"' + objectName + '"},"Summary":"' + summary + '",' +
                        '"' + createdDate + '"}';
                }

                Type typeOfObject = Type.forName(objectName);
                sObject kavObject = (sObject)JSON.deserialize(kavJSON, typeOfObject);

                return kavObject;
            }

        }
        return null;
    }

    public static String getKavObjectName() {
        for(Schema.SObjectType o : Schema.getGlobalDescribe().values()) {
            Schema.DescribeSObjectResult objResult = o.getDescribe();
            if(objResult.getName().endsWith('kav')) {
                String objectName = objResult.getName();
                return objectName;
            }

        }
        return null;
    }
}