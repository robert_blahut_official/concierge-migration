@isTest
public class TestClassUtility {
	@TestVisible
	private static Id getIdOfUsualProfile() {
		//a query returns not System Admin Standard Salesforce Profile
		List<Profile> profilesList = [
			SELECT Id
			FROM Profile
			WHERE UserType = 'Standard' AND
			UserLicense.Name = 'Salesforce' AND
			PermissionsViewAllData = false
		];
		return profilesList.size() > 0 ? profilesList[0].Id : UserInfo.getProfileId();
	}

	@TestVisible
	private static User createTestUserByProfileId(Id profileId) {
		User usr = new User(
			LastName = 'LIVESTON',
			FirstName = 'JASON',
			Alias = 'jliv',
			Email = 'jason.liveston@asdf.com',
			Username = 'jason.liveston@asdf.asdf.asdf.com',
			ProfileId = profileId,
			TimeZoneSidKey = 'GMT',
			LanguageLocaleKey = 'en_US',
			EmailEncodingKey = 'UTF-8',
			LocaleSidKey = 'en_US'
		);
		try {
			insert usr;
			return usr;
		} catch (Exception e) {
			return new User(Id = UserInfo.getUserId());
		}
	}

	@TestVisible
	private static Schema.DescribeSObjectResult getSobjectDescribe(String objectType) {
		return Schema.getGlobalDescribe().get(objectType).getDescribe();
	}

	@TestVisible
	private static string getAvailableKnowlegeLanguage(Schema.DescribeSObjectResult sObjectDescribe) {
		return sObjectDescribe.fields.getMap().get('Language').getDescribe().getPicklistValues()[0].getValue();
	}

	@TestVisible
	private static Case generateCase(String subject, String description, String status, String origin) {
		return new Case(
			Subject = subject,
			Description = description,
			Status = status,
			Origin = origin
		);
	}

	public static void createSearchEngineConfig(String engineType) {
		createSearchEngineConfig('', engineType);
	}

	public static void createSearchEngineConfig() {
		createSearchEngineConfig('', AbstractSearchEngine.SALESFORCE_SEARCH_TYPE);
	}

	public static void createSearchEngineConfig(String objectType, String engineType) {
		Search_Engine_Configurations__c engineConfig = new Search_Engine_Configurations__c();
		engineConfig.Name = AbstractSearchEngine.ENGINE_CONFIG_SETTING_NAME;
		engineConfig.Engine_Type__c = engineType;
		engineConfig.Max_Suggestions__c = 15;
		engineConfig.Postgres_Table_Name__c = objectType;
		engineConfig.Maximum_number_of_articles_to_retrieve__c = 200;
		insert engineConfig;
	}

	public static void createCaseTypesConfig() {
		Concierge_Settings__c cs =  new Concierge_Settings__c(
			Name = WorkWithCases.CASE_TYPE_SETTING_NAME,
			Value__c = 'IT,General,'
		);
		insert cs;
	}
}