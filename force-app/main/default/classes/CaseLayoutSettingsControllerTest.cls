@IsTest
public with sharing class CaseLayoutSettingsControllerTest {

    static testMethod void getCaseLayoutWrapperTest() {
        TestClassUtility.createSearchEngineConfig();

        WorkWithActionsSettings.isTest = true;
        sObject dataCategoryObject = WorkWithActionsSettingsTest.getDataCategorySelectionRecord();
        CaseLayoutSettingsController.CaseLayoutWrapper result;
        if(dataCategoryObject != null) {
            String dataCategoryName = WorkWithActionsSettingsTest.getDataCategoryObjectName();
            result = CaseLayoutSettingsController.getCaseLayoutWrapper('recordId', dataCategoryName);
            System.assert(result != null);
        } else {
            System.assert(result == null);
        }
    }
    
	@isTest
	static void createNewSettingUpdateTest(){
		cncrg__Case_Layout_Assignment__c caseLayoutAssignmentRecord = new cncrg__Case_Layout_Assignment__c(
			cncrg__Article_Id__c	= 'id',
			cncrg__Article_Type__c	= 'type',
			cncrg__Data_Category__c = 'category',
			cncrg__Field_Set__c		= 'field_set'
		);
		insert caseLayoutAssignmentRecord;
		String caseLayoutRecord = JSON.serialize(caseLayoutAssignmentRecord);
		CaseLayoutSettingsController.createNewSetting(caseLayoutRecord);
		System.assert(Limits.getDMLStatements() != 0);
	}

	@isTest
	static void createNewSettingInsertTest(){
		cncrg__Case_Layout_Assignment__c caseLayoutAssignmentRecord = new cncrg__Case_Layout_Assignment__c(
			cncrg__Article_Id__c	= 'id',
			cncrg__Article_Type__c	= 'type',
			cncrg__Data_Category__c = 'category',
			cncrg__Field_Set__c		= 'field_set'
		);
		String caseLayoutRecord = JSON.serialize(caseLayoutAssignmentRecord);
		CaseLayoutSettingsController.createNewSetting(caseLayoutRecord);
		System.assert(Limits.getDMLStatements() != 0);
	}

	@isTest
	static void createNewSettingMultipleTest(){
		List<cncrg__Case_Layout_Assignment__c> caseLayoutAssignmentList = new List<cncrg__Case_Layout_Assignment__c>();
		for(Integer i = 0; i < 2; i++){
			cncrg__Case_Layout_Assignment__c caseLayoutAssignmentRecord = new cncrg__Case_Layout_Assignment__c(
				cncrg__Article_Id__c	= 'id',
				cncrg__Article_Type__c	= 'type',
				cncrg__Data_Category__c = 'category',
				cncrg__Field_Set__c		= 'field_set'
			);
			caseLayoutAssignmentList.add(caseLayoutAssignmentRecord);
		}
		insert caseLayoutAssignmentList;
		String caseLayoutRecord = JSON.serialize(caseLayoutAssignmentList.get(0));
		CaseLayoutSettingsController.createNewSetting(caseLayoutRecord);
		System.assert(Limits.getDMLStatements() != 0);
	}

	@isTest
	static void createNewSettingWithoutPersmission(){
		System.runAs(TestClassUtility.createTestUserByProfileId(TestClassUtility.getIdOfUsualProfile())){
				cncrg__Case_Layout_Assignment__c caseLayoutAssignmentRecord = new cncrg__Case_Layout_Assignment__c(
				cncrg__Article_Id__c	= 'id',
				cncrg__Article_Type__c	= 'type',
				cncrg__Data_Category__c = 'category',
				cncrg__Field_Set__c		= 'field_set'
			);
			String caseLayoutRecord = JSON.serialize(caseLayoutAssignmentRecord);
			System.assertEquals(null, CaseLayoutSettingsController.createNewSetting(caseLayoutRecord));
		}
	}
}