public with sharing class JsonDeserializerSimpleResponse {
	public String result;
    public Boolean error;

    public static JsonDeserializerSimpleResponse parse(String json) {
        return (JsonDeserializerSimpleResponse) System.JSON.deserialize(json, JsonDeserializerSimpleResponse.class);
    }
}