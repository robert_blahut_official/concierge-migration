@IsTest
public with sharing class ObjectSearchSettingsControllerTest {

	@IsTest
	public static void getUsualObjectSettingsTest() {
		List<sObject> settings = new List<sObject>();
		Search_Types_Settings__c newCaseSettings = new Search_Types_Settings__c(
				Name = 'Case',
				cncrg__Active__c = true,
				cncrg__Fields_To_Display_1__c = 'Subject',
				cncrg__Fields_To_Display_2__c = 'Description,Attachments',
				cncrg__Is_Usual_Object__c = true,
				cncrg__Title_Field__c = 'Origin',
				cncrg__Article_Type_API_Name__c = 'Case'
		);
		settings.add(newCaseSettings);
		insert settings;
		List<SettingsPanelController.TypesWrapper> newTypesWrappersList = ObjectSearchSettingsController.getUsualObjectSettings();
		System.assert(newTypesWrappersList.size() > 0);
		System.assertEquals('Case', newTypesWrappersList[0].articleType);
		System.assert(newTypesWrappersList[0].isActive);
		System.assert(newTypesWrappersList[0].selectedFields.size() > 0);
		System.assertEquals('Subject', newTypesWrappersList[0].selectedFields[0].fieldName);
	}

	@IsTest
	public static void implementNewSettingsTest() {
		SettingsPanelController.NewSettingsClass newWrapper = new SettingsPanelController.NewSettingsClass();
		String serializedWrapper = JSON.serialize(newWrapper);
		String result = ObjectSearchSettingsController.implementNewSettings(serializedWrapper);
		System.assertEquals('Success', result);
	}

}