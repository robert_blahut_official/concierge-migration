public with sharing class ActionsSectionController  {
	public static String ARTICLE_ACTIONS_DISPLAY_ORDER = 'Article Actions Display Order';

	@AuraEnabled
	public static List<Object> getInitialData(Object knowledgeRecord) {
		UserPermissionAccess userPermission = [	SELECT	Id, 
														PermissionsRunFlow, 
														PermissionsModifyMetadata  
												FROM	UserPermissionAccess 
												];
		return new List<Object> {(userPermission.PermissionsRunFlow || userPermission.PermissionsModifyMetadata), getActionsOrder(), getCallArticleActionComponentName(knowledgeRecord) };
	}
	public static List<String> getActionsOrder() {
		//Article Actions Display Order
		String settingsValue = Utils.getValueFromConciergeSettings(ARTICLE_ACTIONS_DISPLAY_ORDER);
		return settingsValue.split(';');
	}
	public static String getCallArticleActionComponentName(Object knowledgeRecord) {
		String CALL_ACTION_NAME = 'CallArticleAction';
		System.debug(knowledgeRecord);
		CustomerExtensionBase instance = Utils.getNewBaseUniqueToolsInstance();
		return ((Map<String, String>) JSON.deserialize(instance.getDynamicComponentNamesMap(new List<String> {JSON.serialize(knowledgeRecord)})[0], Map<String, String>.class)).get(CALL_ACTION_NAME);
		 
	}
}