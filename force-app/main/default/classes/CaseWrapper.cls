public class CaseWrapper {
	@AuraEnabled
	public SObject newCase {get; set;}
	@AuraEnabled
	public String newCategory {get; set;}
	@AuraEnabled
	public String caseLayoutFieldSet {get; set;}
	@AuraEnabled
	public String emailReciever {get; set;}
	@AuraEnabled
	public String articleId {get; set;}
}