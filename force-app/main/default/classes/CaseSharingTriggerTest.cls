@isTest
private class CaseSharingTriggerTest  {

	@testSetup static void setup(){
		insert new Concierge_Settings__c(
			Name = 'Case Origin',
			value__c = 'Email'
		);

		insert new Trigger_Activator__c(
			name = 'Case Sharing Trigger',
			Active__c = true
		);

		insert new Contact(
			Firstname = 'Test',
			Lastname = 'Test'
		);
		System.assert(!Concierge_Settings__c.getall().values().isEmpty());
	}

	static testMethod void caseSharingTest() {
        CaseSharingHandler.isFutereInsert = false;
		String contactId =	[
								SELECT	Id
								FROM	Contact
								WHERE	Lastname = 'Test'
								LIMIT	1
							].Id; 
		Case testCase = new Case(
			Status = 'New',
			Origin = 'Email',
			ContactId = contactId
		);
		insert testCase;
		String oldStatus = testCase.Status;
		
		testCase.Status = 'Working';
		update testCase;

		System.assertNotEquals(oldStatus, testCase.Status);
	}
    static testMethod void caseSharingNegativeTest() {
        CaseSharingHandler.OBJECT_NAME = 'sObject Does not exist';
		String contactId =	[
								SELECT	Id
								FROM	Contact
								WHERE	Lastname = 'Test'
								LIMIT	1
							].Id; 
		Case testCase = new Case(
			Status = 'New',
			Origin = 'Email',
			ContactId = contactId
		);
		insert testCase;
		String oldStatus = testCase.Status;
		
		testCase.Status = 'Working';
		update testCase;

		System.assertNotEquals(oldStatus, testCase.Status);
	}
    
}