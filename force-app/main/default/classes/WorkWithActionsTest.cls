@isTest
private class WorkWithActionsTest {

	@isTest
	private static void testWorkWithActions() {
		Search_Types_Settings__c stSettings = new cncrg__Search_Types_Settings__c();
		stSettings.Name = 'Case';
		stSettings.Active__c = true;
		stSettings.Fields_To_Display_1__c = 'Id,CreatedDate,';
		stSettings.Fields_To_Display_2__c = 'LastModifiedDate';
		stSettings.Article_Type_API_Name__c = 'Case';
		insert stSettings;

		String objectType = WorkWithArticlesTest.getKavObjectName();
		Schema.DescribeSObjectResult sObjectDescribe = TestClassUtility.getSobjectDescribe(objectType);
		sObject obj = sObjectDescribe.getSobjectType().newSObject();
		obj.put('Title', 'Unit Test 1 test');
		obj.put('Language', TestClassUtility.getAvailableKnowlegeLanguage(sObjectDescribe));
		obj.put('UrlName', 'test');
		obj.put('Summary', 'test test test test');
		//obj.put('cncrg__Full_Description__c','test test test test');
		List<sObject> articles = new List<sObject> {obj};
		ESAPI.securityUtils().validatedInsert(articles);

		List<Id> listOfIds = new List<Id>();
		for (sObject article : articles) {
			listOfIds.add((String)article.get('Id'));
		}
		articles = Database.query('SELECT KnowledgeArticleId FROM ' + objectType + ' WHERE Id IN: listOfIds');

		KbManagement.PublishingService.publishArticle((Id)articles[0].get('KnowledgeArticleId'), true);
		TestClassUtility.createSearchEngineConfig();

		System.assert(WorkWithActionsSettings.getAllArticleTypes().size() > 0);
		System.assert(WorkWithActionsSettings.getAllArticleCategories((String)articles[0].get('Id'), objectType) != null);

		cncrg__Action_Setting__c newSetting1 = WorkWithActionsSettings.createNewSetting(
			JSON.serialize(new List<cncrg__Action_Setting__c> {
				null,
				new cncrg__Action_Setting__c(
					cncrg__ArticleId__c = 'testId1',
					cncrg__Active__c = true,
					cncrg__Source_System__c = 'salesforce'
				)
			})
		);
		System.assert(WorkWithActionsSettings.getSettingByName('testId1').Id == newSetting1.Id);

		cncrg__Action_Setting__c createdSetting = [
			SELECT Id, cncrg__ArticleId__c, cncrg__Type__c, cncrg__Source_System__c, cncrg__Category__c
			FROM cncrg__Action_Setting__c
			WHERE cncrg__ArticleId__c != null AND cncrg__Source_System__c = 'salesforce'
		];
		cncrg__Action_Setting__c newSetting2 = WorkWithActionsSettings.createNewSetting(
			JSON.serialize(new List<cncrg__Action_Setting__c> {
				createdSetting,
				new cncrg__Action_Setting__c(
					cncrg__Type__c = objectType,
					cncrg__Active__c = true,
					cncrg__Source_System__c = 'salesforce'
				)
			})
		);

		List<SearchResultsWrapper> listSearchResultsWrapper = new List<SearchResultsWrapper> {
			new SearchResultsWrapper('title', 'snippet', 'testId1', 'body', objectType),
			new SearchResultsWrapper('title', 'snippet', 'testId2', 'body', objectType)
		};
		//System.assert(SearchQueryInputController.prepareActions(JSON.serialize(listSearchResultsWrapper),0).size() == 2);
		System.assert(WorkWithActionsSettings.deleteSettingAPEX(newSetting1) == null);

		try {
			insert new cncrg__Action_Setting__c(
				cncrg__Type__c = objectType,
				cncrg__ArticleId__c = 'testId1',
				cncrg__Active__c = true
			);
		} catch (Exception e) {}
	}
}