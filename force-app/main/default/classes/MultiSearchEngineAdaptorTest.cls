@IsTest
private class MultiSearchEngineAdaptorTest {

	@TestSetup static void setup() {
		TestClassUtility.createSearchEngineConfig(AbstractSearchEngine.FEDERATED_SEARCH_TYPE);

		List<cncrg__Search_Term__c> searchTerms = new List<cncrg__Search_Term__c> {
			new cncrg__Search_Term__c(
				Name = 'test search term'
			)
		};
		ESAPI.securityUtils().validatedInsert(searchTerms);

		String objectType = WorkWithArticlesTest.getKavObjectName();

		List<cncrg__Search_Types_Settings__c> settings = new List<cncrg__Search_Types_Settings__c> {
			new cncrg__Search_Types_Settings__c(
				Name = objectType,
				cncrg__Article_Type_API_Name__c = objectType,
				cncrg__Active__c = true,
				cncrg__Fields_To_Display_1__c = 'Summary,Title'
			)
		};
		ESAPI.securityUtils().validatedInsert(settings);
		System.assertEquals(1, cncrg__Search_Types_Settings__c.getAll().size());

		List<cncrg__Concierge_Settings__c> cs = new List<cncrg__Concierge_Settings__c> {
			new cncrg__Concierge_Settings__c(
				Name = 'Background file name for App',
				cncrg__Value__c = 'test'
			),
			new cncrg__Concierge_Settings__c(
				Name = 'Application Name',
				cncrg__Value__c = 'Concierge'
			),
			new cncrg__Concierge_Settings__c(
				Name = 'Number of Articles Displayed in Search',
				cncrg__Value__c = '5'
			),
			new cncrg__Concierge_Settings__c(
				Name = 'Background For App on Mobile Devices',
				cncrg__Value__c = '6'
			),
			new cncrg__Concierge_Settings__c(
				Name = 'Hours New/Updated Flag is Displayed',
				cncrg__Value__c = '24'
			),
			new cncrg__Concierge_Settings__c(
				Name = 'Search Log Email Address',
				cncrg__Value__c = 'test@test.com'
			),
			new cncrg__Concierge_Settings__c(
				Name = 'Lightning Knowledge Object Name',
				cncrg__Value__c = objectType
			)
		};
		ESAPI.securityUtils().validatedInsert(cs);
		System.assertEquals(7, cncrg__Concierge_Settings__c.getAll().size());

		Schema.DescribeSObjectResult sObjectDescribe = TestClassUtility.getSobjectDescribe(objectType);
		sObject obj = sObjectDescribe.getSobjectType().newSObject();
		obj.put('Title', 'Unit Test 1 test');
		obj.put('Language', Utils.getUserLanguage());
		obj.put('UrlName', 'test'+ String.valueOf(DateTime.now().getTime()));
		obj.put('Summary', 'test test test test');

		sObject objUpdated = sObjectDescribe.getSobjectType().newSObject();
		objUpdated.put('Title', 'Unit Test 2 test');
		objUpdated.put('Language', Utils.getUserLanguage());
		objUpdated.put('UrlName', 'test2' + String.valueOf(DateTime.now().getTime()));
		objUpdated.put('Summary', 'test2 test2 test2 test2');

		List<sObject> articles = new List<sObject> {obj, objUpdated};
		ESAPI.securityUtils().validatedInsert(articles);

		List<Id> listOfIds = new List<Id>();
		for (sObject article : articles) {
			listOfIds.add((String) article.get('Id'));
		}
		articles = Database.query('SELECT KnowledgeArticleId FROM ' + objectType + ' WHERE Id IN: listOfIds');

		KbManagement.PublishingService.publishArticle((Id) articles[0].get('KnowledgeArticleId'), true);
		KbManagement.PublishingService.publishArticle((Id) articles[1].get('KnowledgeArticleId'), true);
	}

	@IsTest
	public static void searchResultsTest() {
		setDataProviderResponse();

		Test.startTest();
		String salesforceStorage = HerokuWebService.StorageType.salesforce.name();
		String sharepointStorage = HerokuWebService.StorageType.sharepoint.name();
		MultiSearchEngineAdaptor searchService = new MultiSearchEngineAdaptor();

		List<SearchResultsWrapper> searchResults = searchService.getSearchResults('Test', null);
		System.assertEquals(4, searchResults.size());

		for (SearchResultsWrapper res: searchResults) {
			res.isLike = true;

			if (res.storage == salesforceStorage) {
				//String err = searchService.rateItem(res);
				//System.assert(String.isBlank(err));

				res = searchService.getAllArticleSettings(res, null);
				System.assert(String.isNotBlank(res.dataCategories), 'categories are not empty!');

				res = searchService.getArticleDetail(res, null);
				System.assert(res.body != 'blankValue', 'article details should present');
				continue;
			}

			if (res.storage == sharepointStorage) {
				String err = searchService.rateItem(res);
				System.assert(String.isBlank(err));

				res = searchService.getAllArticleSettings(res, null);
				System.assert(String.isNotBlank(res.dataCategories), 'categories are not empty!');
			}
		}
		Test.stopTest();
	}

	@IsTest
	public static void favoritesTest() {
		setDataProviderResponse();

		Test.startTest();
		String salesforceStorage = HerokuWebService.StorageType.salesforce.name();
		MultiSearchEngineAdaptor searchService = new MultiSearchEngineAdaptor();

		List<Object> favorites = searchService.getFavoritesbyId(new List<String>(), null);
		favorites = searchService.getFavoritesbyId(new List<String> {'Test'}, null);
		System.assertEquals(4, favorites.size());

		for (FavoriteArticlesWrapper fav: (List<FavoriteArticlesWrapper>)favorites) {
			if (fav.storage == salesforceStorage) {
				fav = searchService.getFavoriteArticleDetail(fav, null);
				System.assert(fav.body != 'blankBody', 'article details should present');
			}
		}
		Test.stopTest();
	}

	@IsTest
	public static void suggestionsTest() {
		setDataProviderResponse();

		Test.startTest();
		MultiSearchEngineAdaptor searchService = new MultiSearchEngineAdaptor();
		List<SearchPhraseWrapper> results = searchService.getSuggestionsForQuery('test');
		System.assertEquals(3, results.size());
		Test.stopTest();
	}

	@IsTest
	public static void knowledgeRestTest() {
		Test.startTest();
		List<KnowledgeDataCategoryRest.KnowledgeResponseWrapper> results = KnowledgeDataCategoryRest.doGet();
		System.assertEquals(2, results.size());

		List<String> articleIDs = new List<String>();
		for (KnowledgeDataCategoryRest.KnowledgeResponseWrapper res: results) {
			articleIDs.add(res.sfdxArticleId);
		}

		RestContext.request = new RestRequest();
		RestContext.response = new RestResponse();
		RestContext.request.requestBody = Blob.valueOf(JSON.serialize(articleIDs));
		results = KnowledgeDataCategoryRest.doPost();
		System.assertEquals(2, results.size());
		Test.stopTest();
	}

	@IsTest
	public static void dataCategoryBatchTest() {
		setDataProviderResponse();

		Test.startTest();
		DataCategoryTreeBatch batchJob = new DataCategoryTreeBatch();
		batchJob.setInitParams(new List<Object>{null, DateTime.now(), 'cncrg.DataCategoryTreeBatch', 200});
		DataBase.executeBatch(batchJob);
		Test.stopTest();
	}

	private static void setDataProviderResponse() {
		String objectType = WorkWithArticlesTest.getKavObjectName();
		List<sObject> records = Database.query('SELECT Id, KnowledgeArticleId FROM ' + objectType);
		String articleId = records.get(0).Id;
		String knowledgeId = (String)records.get(0).get('KnowledgeArticleId');

		String responseBody = '[{"highlight":{"Title":["Requesting <em>Business</em> Cards"],"UrlName":["Requesting-<em>Business</em>-Cards"],"accessattributes.SFDCArticleType.keyword":["<em>'+objectType+'</em>"]},"datasource":"salesforce","accessattributes":{"SFDCDataCategory":["Function.IT"],"SFDCArticleType":["'+objectType+'"]},"sfdxArticleId":"'+articleId+'","Title":"Requesting Business Cards","UrlName":"Requesting-Business-Cards","Id":"'+articleId+'","KnowledgeArticleId":"'+knowledgeId+'","cncrg__Full_Description__c":"My Test","category":"Function.IT,Function.HR"},'
			+ '{"highlight":{"fileName":["111 <em>test</em> 333.txt"],"attachment.content":["111 <em>test</em> asvggg  work word worfbb"],"Key":["111 <em>test</em> 333.txt"]},"fileName":"111 test 333.txt","content_type":"text/plain","attachment":{"content_type":"text/plain; charset=ISO-8859-1","language":"et","content_length":34},"lastUpdateDate":1563842363000,"_id":"1234567890","datasource":"sharepoint","aws_bucket":"testellc","ServerRelativeUrl":"https://testellc.s3.us-west-2.amazonaws.com/111%20test%20333.txt","Key":"111 test 333.txt","category":"Function.IT,Function.HR"},'
			+ '{"highlight":{"attachment.content":["<em>Test</em> document for S3..<em>Test</em> document for S3..<em>Test</em> document","for S3..<em>Test</em> document for S3"]},"content_type":"application/vnd.openxmlformats-officedocument.wordprocessingml.document","attachment":{"date":"2019-07-22T13:39:00Z","content_type":"application/vnd.openxmlformats-officedocument.wordprocessingml.document","author":"jbUser","language":"fr","content_length":85},"_id":"1234567892","aws_bucket":"testellc","URL":"https://testellc.s3.us-west-2.amazonaws.com/111%20test%20333.txt","Key":"TestDocument.docx"},'
			+ '{"highlight":{"attachment.content":["111 <em>test</em> asvggg  work word worfbb"]},"content_type":"text/plain","attachment":{"content_type":"text/plain; charset=ISO-8859-1","language":"et","content_length":34},"aws_bucket":"testellc","Key":"111 test.txt"}]';
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(200, 'OK', responseBody, new Map<String, String>()));
	}
}