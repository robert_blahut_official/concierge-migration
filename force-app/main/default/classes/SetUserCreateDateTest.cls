@IsTest
private class SetUserCreateDateTest {
    @IsTest
    static void SetUserCreateDateTestMethod() {
		User usr = [	SELECT	 Id,
								 User_Create_Date__c
						FROM     User
						WHERE    Email = 'jason.liveston@asdf.com'
              ];
        System.assert(usr.User_Create_Date__c == DateTime.now().dateGMT());
    }
    @testSetup
    static void setup(){
        upsert new cncrg__Concierge_Settings__c(Name = 'Case Origin', Value__c = 'Concierge');
        TestClassUtility.createTestUserByProfileId(TestClassUtility.getIdOfUsualProfile());
        insert new List<Trigger_Activator__c>{
            new Trigger_Activator__c(
                    Name = 'Case Sharing Trigger',
                    Active__c = true
                ),
            new Trigger_Activator__c(
                    Name = 'Contact Case Sharing Trigger',
                    Active__c = true
                ),
            new Trigger_Activator__c(
                    Name = 'User Case Sharing Trigger',
                    Active__c = true
                )
        };
        System.assert(!cncrg__Concierge_Settings__c.getAll().isEmpty());

    }
    @isTest
    static void userCaseShareTriggerBeforeUpdateTest(){
        delete [SELECT Id FROM Case];
        User userRecord = [ SELECT  Id, 
                                    isActive 
                            FROM    User
                            WHERE   Email = 'jason.liveston@asdf.com'
                            ];
        Test.startTest();
        userRecord.isActive = true;
        update userRecord;
        System.assertEquals(Database.query('SELECT Id FROM CaseShare').size(), 0);
    }
}