@IsTest
public with sharing class FieldSetControllerTest {
	static testMethod void testFieldSetCtlrMethod() {
		TestClassUtility.createCaseTypesConfig();

		String strObjectApiName = 'Case';
		Schema.SObjectType sObjType = Schema.getGlobalDescribe().get(strObjectApiName);
		Map<String, Schema.FieldSet> fieldSetMap = sObjType.getDescribe().fieldsets.getMap();
		String fieldSetName = (new List<String>(fieldSetMap.keySet()))[2];
		List<FieldSetController.FieldSetMember> allFields = FieldSetController.getFields(strObjectApiName, fieldSetName);
		System.assert(allFields != null);
	}
}