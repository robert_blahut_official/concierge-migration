public class FieldSetController {
	private static Set<String> READONLYFIELDS = new Set<String> {'cncrg__Data_Categories__c'};

	@AuraEnabled
	public static List<FieldSetMember> getFields(String typeName, String fsName) {
		SObject objectRecord = (SObject)Type.forName(typeName).newInstance();
		Schema.DescribeSObjectResult describe = objectRecord.getSObjectType().getDescribe();
		Map<String, Schema.FieldSet> fsMap = describe.fieldSets.getMap();

		Set<String> selectedCaseTypes = new Set<String>();
		String caseTypesFromSetting = Utils.getValueFromConciergeSettings(WorkWithCases.CASE_TYPE_SETTING_NAME);
		if (String.isNotBlank(caseTypesFromSetting)) {
			selectedCaseTypes.addAll(caseTypesFromSetting.split(','));
		}

		List<FieldSetMember> outputResults = new List<FieldSetMember>();
		for (Schema.FieldSetMember theField : fsMap.get(fsName).getFields()) {
			Schema.DescribeFieldResult fieldDescribe = theField.getSObjectField().getDescribe();
			if (fieldDescribe.isCreateable()) {
				outputResults.add(new FieldSetMember(theField, fieldDescribe, selectedCaseTypes));
			}
		}
		return outputResults;
	}

	public class FieldSetMember {
		@AuraEnabled
		public List<PickListWrapper> pickList { get; set; }
		@AuraEnabled
		public Boolean isDependent {get; set;}
		@AuraEnabled
		public Boolean DBRequired { get; set; }
		@AuraEnabled
		public String defaultValue { get; set; }
		@AuraEnabled
		public String fieldPath { get; set; }
		@AuraEnabled
		public String label { get; set; }
		@AuraEnabled
		public Boolean required { get; set; }
		@AuraEnabled
		public String type { get; set; }
		@AuraEnabled
		public String parentObjectName { get; set; }
		@AuraEnabled
		public Map<String, List<String>> dependentValues { get; set; }
		@AuraEnabled
		public String controlField {get; set;}
		@AuraEnabled
		public Boolean isReadonly {get; set;}
		@AuraEnabled
		public Object value {get; set;}

		public FieldSetMember(Schema.FieldSetMember theField, Schema.DescribeFieldResult fieldDescribe, Set<String> selectedCaseTypes) {
			this.isReadonly = false;

			this.setFieldDetails(theField);
			this.setParentObjectName(fieldDescribe);
			this.setPicklistValues(fieldDescribe, selectedCaseTypes);
		}

		public void setFieldDetails(Schema.FieldSetMember theField) {
			this.DBRequired = theField.DBRequired;
			this.fieldPath = theField.fieldPath;
			this.label = theField.label;
			this.required = theField.required;
			this.type = '' + theField.getType();
			if (FieldSetController.READONLYFIELDS.contains(this.fieldPath)) {
				this.type = 'OUTPUT';
			}
		}

		public void setPicklistValues(Schema.DescribeFieldResult fieldDescribe, Set<String> selectedCaseTypes) {
			this.pickList = new List<PickListWrapper>();

			if (this.type == 'PICKLIST' || this.type == 'MULTIPICKLIST') {
				// added Case Type override for Uniliver specific item
				if (!selectedCaseTypes.isEmpty() && this.fieldPath == 'Type') {
					for (Schema.PicklistEntry item : fieldDescribe.getPicklistValues()) {
						if (item.isActive() && selectedCaseTypes.contains(item.getValue())) {
							this.pickList.add(new PickListWrapper(item));
						}
					}
				} else { // base behaviour
					for (Schema.PicklistEntry item : fieldDescribe.getPicklistValues()) {
						if (item.isActive()) {
							this.pickList.add(new PickListWrapper(item));
						}
					}
				}
			}

			this.isDependent = fieldDescribe.isDependentPicklist();
			this.dependentValues = new Map<String, List<String>>();
			if (this.isDependent) {
				this.dependentValues = Utils.getDependentPicklistValues(fieldDescribe);
				this.controlField = fieldDescribe.getController().getDescribe().getName();
			}
		}

		public void setParentObjectName(Schema.DescribeFieldResult fieldDescribe) {
			if (this.type == 'REFERENCE') {
				this.parentObjectName = fieldDescribe.getReferenceTo()[0].getDescribe().getName();
			}
		}
	}

	public class PickListWrapper {
		@AuraEnabled
		public Boolean isDefaultValue { get; set; }
		@AuraEnabled
		public String label { get; set; }
		@AuraEnabled
		public String value { get; set; }

		public PickListWrapper(Schema.PicklistEntry picklistValue) {
			this.label = picklistValue.getLabel();
			this.value = picklistValue.getValue();
			this.isDefaultValue = picklistValue.isDefaultValue();
		}
	}
}