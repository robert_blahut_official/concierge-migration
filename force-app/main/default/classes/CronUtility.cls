public with sharing class CronUtility {
    public static final String CRON_SEPARATOR = ' ';

    /* '*' Specifies all values. For example, if Month is specified as *, the job is scheduled for every month.*/
    public static final String CRON_ALL_VALUES = '*';

    /* ',' Delimits values. For example, use JAN, MAR, APR to specify more than one month. */
    public static final String CRON_DELIMITS_VALUE = ',';

    /* '-' Specifies a range. For example, use JAN-MAR to specify more than one month. */
    public static final String CRON_SPECIFIES_RANGE = '-';

    /* '?' Specifies no specific value. This is only available for Day_of_month and Day_of_week, and is generally used when specifying a value for one and not the other. */
    public static final String CRON_SPECIFIC_VALUE = '?'; //use *
    /*
        '/'    Specifies increments. The number before the slash specifies when the intervals will begin, and the number after the slash is the interval amount.
        For example, if you specify 1/5 for Day_of_month, the Apex class runs every fifth day of the month, starting on the first of the month.
    */
    public static final String CRON_INCREMENT = '/';

    /*
        'L'    Specifies the end of a range (last). This is only available for Day_of_month and Day_of_week.
        When used with Day of month, L always means the last day of the month, such as January 31, February 29 for leap years, and so on. When used with Day_of_week by itself, it always means 7 or SAT.
        When used with a Day_of_week value, it means the last of that type of day in the month. For example, if you specify 2L, you are specifying the last Monday of the month.
        Do not use a range of values with L as the results might be unexpected.
    */
    public static final String CRON_END = 'L'; //TODO

    /*
        'W'     Specifies the nearest weekday (Monday-Friday) of the given day.
        This is only available for Day_of_month. For example, if you specify 20W, and the 20th is a Saturday, the class runs on the 19th.
        If you specify 1W, and the first is a Saturday, the class does not run in the previous month, but on the third, which is the following Monday.
    */
    public static final String CRON_NEAREST = 'W'; //TODO

    /*
        '#'     Specifies the nth day of the month, in the format weekday#day_of_month.
        This is only available for Day_of_week.
        The number before the # specifies weekday (SUN-SAT).
        The number after the # specifies the day of the month.
        For example, specifying 2#2 means the class runs on the second Monday of every month.
     */
    public static final String CRON_NTH = '#'; //TODO

    public static final String MONTH_DATETIME_FORMAT = 'MMM';
    public static final String DAY_DATETIME_FORMAT = 'EEE';
    public static final String NORMAL_DATETIME_FORMAT = 'yyyy-MM-dd HH:mm';

    public static final Integer START_VALUE_INDEX   = 0;
    public static final Integer END_VALUE_INDEX     = 1;
    public static final Integer INCREMENT_VALUE     = 1;
    public static final Integer MIN_PARTS           = 6;

    public enum CRON_MODE {
        SECONDS, MINUTES, HOURS, DAY_OF_MONTH, MONTH, DAY_OF_WEEK
    }

    public static Map<CRON_MODE, List<Integer>> rangeMap = new Map<CRON_MODE, List<Integer>>{
            CRON_MODE.SECONDS       => new List<Integer>{0, 59},
            CRON_MODE.MINUTES       => new List<Integer>{0, 59},
            CRON_MODE.HOURS         => new List<Integer>{0, 24},
            CRON_MODE.DAY_OF_MONTH  => new List<Integer>{1, 32},
            CRON_MODE.MONTH         => new List<Integer>{1, 13},
            CRON_MODE.DAY_OF_WEEK   => new List<Integer>{1, 8}
    };

    public static Map<CRON_MODE, String> labelMap = new Map<CRON_MODE, String>{
            CRON_MODE.MINUTES       => Label.Minutes,
            CRON_MODE.HOURS         => Label.Hours,
            CRON_MODE.DAY_OF_MONTH  => Label.Day_of_Month,
            CRON_MODE.MONTH         => Label.Month,
            CRON_MODE.DAY_OF_WEEK   => Label.Day_of_week
    };

    public static final Map<CRON_MODE, Map<String, String>> mainMap = new Map<CRON_MODE, Map<String, String>>{
            CRON_MODE.DAY_OF_WEEK => new Map<String, String> {
                    '1' => 'Sun',
                    '2' => 'Mon',
                    '3' => 'Tue',
                    '4' => 'Wed',
                    '5' => 'Thu',
                    '6' => 'Fri',
                    '7' => 'Sat'
            },

            CRON_MODE.MONTH => new Map<String, String> {
                    '1' =>'Jan',
                    '2' =>'Feb',
                    '3' =>'Mar',
                    '4' =>'Apr',
                    '5' =>'May',
                    '6' =>'Jun',
                    '7' =>'Jul',
                    '8' =>'Aug',
                    '9' =>'Sep',
                    '10' =>'Oct',
                    '11' =>'Nov',
                    '12' =>'Dec'
            }
    };


    public static Datetime convertCronToNextDatetime(String cronValue) {
        return convertCronToNextDatetime(cronValue, Datetime.now());
    }


    public static Datetime checkLastRun(SchedulerSettings__c currentSettings) {
        Datetime currentTime = Datetime.now();
        if(currentSettings.Last_Run__c == null) {
            currentSettings.Last_Run__c = currentTime.addMinutes(-1);
        } else {
            Datetime nextDatetime = CronUtility.convertCronToNextDatetime(currentSettings.CRON__c, currentSettings.Last_Run__c);
            if(nextDatetime < currentTime && CronUtility.convertCronToNextDatetime(currentSettings.CRON__c, nextDatetime) < currentTime) {
                currentSettings.Last_Run__c = currentSettings.LastModifiedDate.addMinutes(-2);
            }
        }
        return currentSettings.Last_Run__c;
    }

    public static Datetime convertCronToNextDatetime(String cronValue, Datetime startTime) {
        if(startTime == null) {
            startTime = Datetime.now();
        }
        List<String> splitCronList = cronValue.split(CRON_SEPARATOR);

        if(splitCronList.size() < MIN_PARTS)
            return null;

        CRONOption secondsOption         = parseCRONPart(splitCronList[CRON_MODE.SECONDS.ordinal()],       CRON_MODE.SECONDS);
        CRONOption minutesOption         = parseCRONPart(splitCronList[CRON_MODE.MINUTES.ordinal()],       CRON_MODE.MINUTES);
        CRONOption hoursOption           = parseCRONPart(splitCronList[CRON_MODE.HOURS.ordinal()],         CRON_MODE.HOURS);
        CRONOption dayOfMonthOption      = parseCRONPart(splitCronList[CRON_MODE.DAY_OF_MONTH.ordinal()],  CRON_MODE.DAY_OF_MONTH);
        CRONOption monthOption           = parseCRONPart(splitCronList[CRON_MODE.MONTH.ordinal()],         CRON_MODE.MONTH);
        CRONOption dayOfWeekOption       = parseCRONPart(splitCronList[CRON_MODE.DAY_OF_WEEK.ordinal()],   CRON_MODE.DAY_OF_WEEK);

        Datetime nextRun = getNextRunDatetime(secondsOption, minutesOption, hoursOption, dayOfMonthOption, monthOption, dayOfWeekOption, startTime);
        if(nextRun.getTime() - startTime.getTime() <= 0) {
            startTime = startTime.addMinutes(1);
            return getNextRunDatetime(secondsOption, minutesOption, hoursOption, dayOfMonthOption, monthOption, dayOfWeekOption, startTime);
        }
        return nextRun;
    }

    private static Datetime getNextRunDatetime(CRONOption secondsOption, CRONOption minutesOption, CRONOption hoursOption, CRONOption dayOfMonthOption, CRONOption monthOption, CRONOption dayOfWeekOption, Datetime startTime) {
        Datetime result = Datetime.newInstance(startTime.getTime());
        Boolean isFind = true;
        Integer incr = INCREMENT_VALUE;
        List<Datetime> checkList = new List<Datetime>{startTime};
        do {
            while(isFind) {
                if(isValidForOption(result.day(), dayOfMonthOption)
                        && isValidForOption(result.format(MONTH_DATETIME_FORMAT), monthOption)
                        && isValidForOption(result.format(DAY_DATETIME_FORMAT), dayOfWeekOption)
                        ) {
                    isFind = false;
                    if(!startTime.isSameDay(result))
                        result = Datetime.newInstance(Date.newInstance(result.year(), result.month(), result.day()), Time.newInstance(0, 0, 0, 0));
                } else {
                    result = result.addDays(INCREMENT_VALUE);

                }
            }
            isFind = true;
            while (isFind) {
                if(isValidForOption(result.hour(), hoursOption) && isValidForOption(result.minute(), minutesOption) ) {
                    isFind = false;
                } else {
                    if(isValidForOption(result.minute(), minutesOption)) {
                        incr = 60;
                    } else {
                        incr = 1;
                    }
                    result = result.addMinutes(incr);
                }
            }
            checkList.add(Datetime.newInstance(result.getTime()));
        } while(checkList[checkList.size() - 2] != checkList[checkList.size() - 1]);
        return Datetime.newInstance(Date.newInstance(result.year(), result.month(), result.day()), Time.newInstance(result.hour(), result.minute(), 0, 0));
    }

    private static Boolean isValidForOption(Object vl, CRONOption option) {
        return option.availableValues.contains(String.valueOf(vl));
    }

    private static Set<String> getIntegerByCRON(String vl, CRON_MODE mode) {
        if(vl == CRON_ALL_VALUES)
            return range(rangeMap.get(mode), INCREMENT_VALUE);
        return new Set<String>{vl};
    }

    private static Set<String> range(Integer startRange, Integer endRange) {
        return range(startRange, endRange, INCREMENT_VALUE);
    }

    private static Set<String> range(List<Integer> rangeList, String stepRange) {
        return range(rangeList, Integer.valueOf(stepRange));
    }


    private static Set<String> range(List<Integer> rangeList, Integer stepRange) {
        return range(rangeList[START_VALUE_INDEX], rangeList[END_VALUE_INDEX], stepRange);
    }

    private static Set<String> range(Integer startRange, Integer endRange, Integer stepRange) {
        Set<String> result = new Set<String>();
        for(Integer i = startRange; i < endRange; i++) {
            if(Math.mod(i, stepRange) == 0)
                result.add(String.valueOf(i));
        }
        return result;
    }

    public static CRONOption parseCRONPart(String cronPart, CRON_MODE mode) {
        if(cronPart.contains(CRON_SPECIFIES_RANGE)) {
            return parseCRONRangePart(cronPart, mode);
        }
        if(cronPart.contains(CRON_DELIMITS_VALUE)) {
            return parseCRONStaticRangePart(cronPart, mode);
        }
        if(cronPart.contains(CRON_INCREMENT)) {
            return parseCRONIncrementPart(cronPart, mode);
        }
        return new CRONOption(getIntegerByCRON(cronPart, mode), mode);
    }

    private static CRONOption parseCRONRangePart(String cronPart, CRON_MODE mode) {
        List<String> rangeList = cronPart.split(CRON_SPECIFIES_RANGE);
        return new CRONOption(rangeList[START_VALUE_INDEX], rangeList[END_VALUE_INDEX], mode);
    }

    private static CRONOption parseCRONStaticRangePart(String cronPart, CRON_MODE mode) {
        return new CRONOption(new Set<String>(cronPart.split(CRON_DELIMITS_VALUE)), mode);
    }

    private static CRONOption parseCRONIncrementPart(String cronPart, CRON_MODE mode) {
        return new CRONOption(range(rangeMap.get(mode), cronPart.split(CRON_INCREMENT)[INCREMENT_VALUE]), mode);
    }

    private static Set<String> checkFormat(Set<String> range, CRON_MODE mode) {
        if(mainMap.containsKey(mode)) {
            range = convertFormat(range, mainMap.get(mode));
        }
        return range;
    }

    private static Set<String> convertFormat(Set<String> range, Map<String, String> convertMap) {
        Set<String> result = new Set<String>();
        for(String vl : range) {
            result.add(convertMap.get(vl));
        }
        return result;
    }

    public class CRONOption {
        public Set<String> availableValues;

        public CRONOption(String startRange, String endRange, CRON_MODE mode) {
            this(range(Integer.valueOf(startRange), Integer.valueOf(endRange)), mode);
        }

        public CRONOption(Set<String> staticRange, CRON_MODE mode) {
            this.availableValues = checkFormat(staticRange, mode);
        }
    }


    public class CRONOptionWrapper {
        @AuraEnabled public Integer interval {get;set;}
        @AuraEnabled public String concrete {get;set;}
        @AuraEnabled public Boolean isConcrete {get;set;}
        @AuraEnabled public Integer startValue {get;set;}
        @AuraEnabled public Integer endValue {get;set;}
        @AuraEnabled public String optionName {get;set;}
        @AuraEnabled public String optionLabel {get;set;}
        @AuraEnabled public List<CRONProperty> advancedConf {get;set;}

        public CRONOptionWrapper(String optionName, String cronValue, CRON_MODE mode) {
            this.optionName = optionName;
            this.optionLabel = labelMap.get(mode);
            if(mainMap.containsKey(mode)) {
                advancedConf = new List<CRONProperty>();
                Map<String, String> rangeMap = mainMap.get(mode);
                CRONOption option = parseCRONPart(cronValue, mode);
                for(String vl : rangeMap.keySet()) {
                    advancedConf.add(new CRONProperty(option.availableValues.contains(rangeMap.get(vl)), vl, rangeMap.get(vl)));
                }
            } else {
                if(cronValue.contains(CRON_INCREMENT)) {
                    this.interval = Integer.valueOf(cronValue.split(CRON_INCREMENT)[1]);
                    isConcrete = false;
                    this.concrete = CRON_ALL_VALUES;
                } else {
                    this.concrete = cronValue;
                    isConcrete = true;
                    if(mode == CRON_MODE.MINUTES) {
                        this.interval = 3;
                    } else {
                        this.interval = 1;
                    }
                }
                List<Integer> range = rangeMap.get(mode);
                this.startValue = range[START_VALUE_INDEX];
                this.endValue = range[END_VALUE_INDEX];
            }
        }
    }

    public class CRONProperty {
        @AuraEnabled public Boolean isSelected {get;set;}
        @AuraEnabled public String propertyLabel {get;set;}
        @AuraEnabled public String propertyValue {get;set;}

        public CRONProperty(Boolean isSelected, String propertyValue, String propertyLabel) {
            this.propertyValue = propertyValue;
            this.propertyLabel = propertyLabel;
            this.isSelected = isSelected;
        }
    }
}