@IsTest
private class SearchQueryInputControllerTest {

	@testSetup static void setup() {
		TestClassUtility.createSearchEngineConfig();

		List<cncrg__Concierge_Settings__c> cs = new List<cncrg__Concierge_Settings__c> {
			new cncrg__Concierge_Settings__c(
				Name = 'Display Company Logo',
				cncrg__Value__c = 'false'
			),
			new cncrg__Concierge_Settings__c(
				Name = 'Always Display Log a Ticket',
				cncrg__Value__c = 'true'
			),
			new cncrg__Concierge_Settings__c(
				Name = 'Background file name for App',
				cncrg__Value__c = 'test'
			),
			new cncrg__Concierge_Settings__c(
				Name = 'Application Name',
				cncrg__Value__c = 'Concierge'
			),
			new cncrg__Concierge_Settings__c(
				Name = 'Number of Articles Displayed in Search',
				cncrg__Value__c = '5'
			),
			new cncrg__Concierge_Settings__c(
				Name = 'Background For App on Mobile Devices',
				cncrg__Value__c = '6'
			),
            new cncrg__Concierge_Settings__c(
				Name = 'Hours New/Updated Flag is Displayed',
				cncrg__Value__c = '24'
			),
			new cncrg__Concierge_Settings__c(
				Name = 'Search Log Email Address',
				cncrg__Value__c = 'test@test.com'
			)
		};
		ESAPI.securityUtils().validatedInsert(cs);
		System.assertEquals(8, cncrg__Concierge_Settings__c.getAll().size());

		String objectType = WorkWithArticlesTest.getKavObjectName();
		if (objectType != null) {
			Schema.DescribeSObjectResult sObjectDescribe = TestClassUtility.getSobjectDescribe(objectType);
			sObject obj = sObjectDescribe.getSobjectType().newSObject();
			obj.put('Title', 'Unit Test 1 test');
			obj.put('Language', TestClassUtility.getAvailableKnowlegeLanguage(sObjectDescribe));
			obj.put('UrlName', 'test');
			obj.put('Summary', 'test test test test');
			//obj.put('cncrg__Full_Description__c', 'test test test test');
			sObject objUpdated = sObjectDescribe.getSobjectType().newSObject();
			objUpdated.put('Title', 'Unit Test 2 test');
			objUpdated.put('Language', TestClassUtility.getAvailableKnowlegeLanguage(sObjectDescribe));
			objUpdated.put('UrlName', 'test2');
			objUpdated.put('Summary', 'test2 test2 test2 test2');
			//obj.put('Full_Description__c', 'test test test test');
			List<sObject> articles = new List<sObject>{
					obj,objUpdated
			};
			ESAPI.securityUtils().validatedInsert(articles);

			List<Id> listOfIds = new List<Id>();
			for (sObject article : articles) {
				listOfIds.add((String) article.get('Id'));
			}

			articles = Database.query('SELECT KnowledgeArticleId FROM ' + objectType + ' WHERE Id IN: listOfIds');

			KbManagement.PublishingService.publishArticle((Id) articles[0].get('KnowledgeArticleId'), true);
            KbManagement.PublishingService.publishArticle((Id) articles[1].get('KnowledgeArticleId'), true); 

			List<cncrg__Search_Types_Settings__c> settings = new List<cncrg__Search_Types_Settings__c>{
				new cncrg__Search_Types_Settings__c(
					Name = objectType,
					cncrg__Article_Type_API_Name__c = objectType,
					cncrg__Active__c = true,
					cncrg__Fields_To_Display_1__c = 'Summary'
				)
			};
			ESAPI.securityUtils().validatedInsert(settings);
			System.assertEquals(1, cncrg__Search_Types_Settings__c.getAll().size());
		}
	}

	static testMethod void testRelatedArticleData() {
		Test.startTest();
		SearchQueryInputController.ArticleSortingWrapper rec = new SearchQueryInputController.ArticleSortingWrapper(32.50);
		String objectType = WorkWithArticlesTest.getKavObjectName();
		Schema.DescribeSObjectResult sObjectDescribe = TestClassUtility.getSobjectDescribe(objectType);
		System.assert(SearchQueryInputController.getRelatedArticleData('articles/' + TestClassUtility.getAvailableKnowlegeLanguage(sObjectDescribe) + '/' + (String.isNotBlank(objectType) ? objectType.removeEnd('__kav') : 'cncrg__Knowledge') + '/test', '') != null);
		Test.stopTest();
	}

	static testMethod void testSearchQueryInputController() {
		AbstractSearchEngine.isTest = true;
		SearchQueryInputController.isTest = true;
		System.assert(SearchQueryInputController.getApplicationName() == 'Concierge');

		List<Id> fixedSearchResults = new List<Id>();
		String objectType = WorkWithArticlesTest.getKavObjectName();
		Schema.DescribeSObjectResult sObjectDescribe = TestClassUtility.getSobjectDescribe(objectType);
		List<sObject> article = Database.query('SELECT Id,KnowledgeArticleId FROM ' + objectType + ' WHERE PublishStatus = \'Online\' AND Language = \'' + TestClassUtility.getAvailableKnowlegeLanguage(sObjectDescribe) +  '\'');
		
		KbManagement.PublishingService.editOnlineArticle((Id) article[1].get('KnowledgeArticleId'), true); 
		List<sObject> draftArticle = Database.query('SELECT Id,KnowledgeArticleId FROM ' + objectType + ' WHERE PublishStatus = \'Draft\' AND Language = \'' + TestClassUtility.getAvailableKnowlegeLanguage(sObjectDescribe) +  '\'');
		draftArticle[0].put('Summary','updateTest');
		update draftArticle[0];

		KbManagement.PublishingService.publishArticle((Id) draftArticle[0].get('KnowledgeArticleId'), true);
		fixedSearchResults.add((String) article[0].get('Id'));
		fixedSearchResults.add((String) draftArticle[0].get('Id'));

		Test.setFixedSearchResults(fixedSearchResults);
		Test.startTest();
		System.assertNotEquals(null, SearchQueryInputController.getSuggestionsForQuery('test'));
		List<Object> results = SearchQueryInputController.getSearchResults(JSON.serialize(new WorkWithArticles.SearchStagingWrapper()), null, 'test', '');
		System.assert(results.size() > 0);

		results = SearchQueryInputController.getSearchResults(JSON.serialize(new WorkWithArticles.SearchStagingWrapper()), null, 'test', '');
		System.assert(results.size() > 0);
		System.assert(SearchQueryInputController.getBGUrl(false) == 'test');
		System.assert(SearchQueryInputController.getGreeting() != null);

		SearchResultsWrapper srw = new SearchResultsWrapper('test', 'test', null, null, 'Case');
		System.assert(SearchQueryInputController.getNumberOfRecordsOnOnePage() == 5);

		Search_Engine_Configurations__c engineConfig = Search_Engine_Configurations__c.getInstance(AbstractSearchEngine.ENGINE_CONFIG_SETTING_NAME);
		engineConfig.Engine_Type__c = AbstractSearchEngine.POSTGRESS_SEARCH_TYPE;
		ESAPI.securityUtils().validatedUpsert(new List<Search_Engine_Configurations__c>{
			engineConfig
		});
		Test.stopTest();
		//System.assert(SearchQueryInputController.rateItem(JSON.serialize(results[0]).replace('[', '').replace(']', '')) == null);
	}

	static testMethod void testGetBGUrl() {
		Test.startTest();
		String url = SearchQueryInputController.getBGUrl(true);
		Test.stopTest();
		System.assert(String.isNotBlank(url));
	}

	static testMethod void testToggleFavoriteAPEX() {
		Test.startTest();
		String result = SearchQueryInputController.toggleFavoriteAPEX('recordId', false);
		Test.stopTest();
		WorkWithArticles.EncryptionMember member = new WorkWithArticles.EncryptionMember();
		Boolean foundArticle = false;
		List<cncrg__Favorite_article__c> resultRecords = [
			SELECT Id, OwnerId, cncrg__ArticleId__c
			FROM cncrg__Favorite_article__c
		];
		for (cncrg__Favorite_article__c item : resultRecords) {
			if (member.decrypteData(item.cncrg__ArticleId__c) == 'recordId') {
				foundArticle = true;
				break;
			}
		}
		System.assertEquals(foundArticle, true);
	}

	static testMethod void testGetIsAdminProfile() {
		String profileName = [
			SELECT Name
			FROM Profile
			WHERE Id = :UserInfo.getProfileId()
		].Name;

		cncrg__ConciergeAdminProfiles__c adminProfile = new cncrg__ConciergeAdminProfiles__c(
			Name = profileName
		);
		insert adminProfile;

		Test.startTest();
		Boolean isAdminProfile = SearchQueryInputController.getIsAdminProfile();
		Test.stopTest();
		System.assert(isAdminProfile);
	}

	static testMethod void testGetBackgroundSettings() {
		Background_Settings__c backgroundSetting = new Background_Settings__c(
			Name = 'Background Setting',
			cncrg__File_Location__c = 'Location File',
			cncrg__Background_File_Type__c = 'Background File'
		);
		insert backgroundSetting;

		Test.startTest();
		SearchQueryInputController.BackgroundWrapper setting = SearchQueryInputController.getBackgroundSettings('Background Setting', false);
		Test.stopTest();
		System.assert(SearchQueryInputController.getAlwaysDiaplayLogTicket());
		System.assert(SearchQueryInputController.getAllowUsersToPrintArticlesInPDF());
		System.assertNotEquals(0, SearchQueryInputController.getLiveAgentPage().size());
		System.assertNotEquals(0, SearchQueryInputController.getInitialSettings().size());
		System.assertNotEquals(0, SearchQueryInputController.doInitSearchInput().size());
		System.assert(setting != null);
	}

	static testMethod void testGetGreeting() {
		SearchQueryInputController.isTest = true;

		SearchQueryInputController.valueFoTestGreeting = 5;
		List<String> result = SearchQueryInputController.getGreeting();
		System.assert(result[0].containsIgnoreCase('Morning'));

		SearchQueryInputController.valueFoTestGreeting = 15;
		result = SearchQueryInputController.getGreeting();
		System.assert(result[0].containsIgnoreCase('Afternoon'));

		SearchQueryInputController.valueFoTestGreeting = 20;
		result = SearchQueryInputController.getGreeting();
		System.assert(result[0].containsIgnoreCase('Evening'));
	}

	static testMethod void testGetMapOfCategories() {
		SearchQueryInputController.isTest = true;
		Map<String, Set<String>> result = KnowledgeDataCategoryService.getMapOfDataCategories(new Set<String>{
			'id'
		}, new Set<String>{WorkWithArticlesTest.getKavObjectName()});
		System.assert(!result.isEmpty());
	}

	static testMethod void testGetShortListOfResults() {
		List<SearchResultsWrapper> listOfResults = new List<SearchResultsWrapper>();
		String wrapperType = WorkWithArticlesTest.getKavObjectName();
		SearchResultsWrapper wrapper = new SearchResultsWrapper('title', 'snippet', 'recordId', 'body', wrapperType);

		SearchResultsWrapper result = Utils.initEngine().getAllArticleSettings(wrapper, null);
		System.assert(result != null);

		List<cncrg__Action_Setting__c> allActionsSettings = new List<cncrg__Action_Setting__c>();
		cncrg__Action_Setting__c actionsSetting = new cncrg__Action_Setting__c(
			cncrg__Category__c = 'IT',
			cncrg__Type__c = wrapperType,
			cncrg__Source_System__c = 'salesforce'
		);
		allActionsSettings.add(actionsSetting);
		insert allActionsSettings;

		List<cncrg__Case_Layout_Assignment__c> allCaseLayoutSettings = new List<cncrg__Case_Layout_Assignment__c>();
		cncrg__Case_Layout_Assignment__c caseLayoutSetting = new cncrg__Case_Layout_Assignment__c(
			cncrg__Data_Category__c = 'IT',
			cncrg__Article_Type__c = wrapperType
		);
		allCaseLayoutSettings.add(caseLayoutSetting);
		insert allCaseLayoutSettings;

		List<cncrg__Action_Setting__c> allArticleSettings = SearchQueryInputController.getArticleActionsSettings(wrapper, new Set<String>{
			'IT'
		});
		System.assert(!allArticleSettings.isEmpty());
		result = SearchQueryInputController.getPrioritizedSettingRecord(wrapper, new Set<String>{
			'IT'
		});
	}

	static testMethod void testGetLogoLastVersionId() {
		List<cncrg__Concierge_Settings__c> cs = new List<cncrg__Concierge_Settings__c>{
			new cncrg__Concierge_Settings__c(
					Name = SearchQueryInputController.LOGO_SETTING_NAME,
					cncrg__Value__c = 'test'
			)
		};
		ESAPI.securityUtils().validatedInsert(cs);
		String standardFolderId = [
				SELECT Id
				FROM   Folder
				WHERE  Type = 'Document'
				LIMIT  1
		].Id;

		System.assert(String.isBlank(SearchQueryInputController.getLogoLastVersionId()));
		Document document = new Document(
				Body = Blob.valueOf('Some Text'),
				ContentType = 'application/pdf',
				DeveloperName = 'test',
				IsPublic = true,
				Name = 'test',
				FolderId = standardFolderId
		);
		insert document;
		System.assert(String.isBlank(SearchQueryInputController.getLogoLastVersionId()));
	}
}