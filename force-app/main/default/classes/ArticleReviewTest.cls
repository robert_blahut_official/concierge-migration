@isTest 
private class ArticleReviewTest {

	@TestSetup static void setup(){
		insert new Concierge_Settings__c(Name = ArticleReviewUtils.AUTOMATICALLY_REVIEW_CUSTOM_SETTINGS_NAME, Value__c = '5');

		List<SchedulerSettings__c> schedSettingList = new List<SchedulerSettings__c>{
				new SchedulerSettings__c(
						Name = TurboEncabulatorScheduler.TURBO_BATCH_NAME,
						Class_Name__c = TurboEncabulatorScheduler.TURBO_BATCH_NAME,
						CRON__c = TurboEncabulatorScheduler.TURBO_CRON,
						Batch_Size__c = TurboEncabulatorScheduler.TURBO_BATCH_SIZE,
						Description__c = TurboEncabulatorScheduler.TURBO_DESCRIPTION,
						Active__c = true
				),
				new SchedulerSettings__c(
						Name = TurboEncabulatorScheduler.REVIEW_BATCH_NAME,
						Class_Name__c = TurboEncabulatorScheduler.REVIEW_BATCH_NAME,
						CRON__c = TurboEncabulatorScheduler.REVIEW_CRON,
						Batch_Size__c = TurboEncabulatorScheduler.REVIEW_BATH_SIZE,
						Description__c = TurboEncabulatorScheduler.REVIEW_DESCRIPTION,
						Active__c = true
				)
		};
		Database.insert(schedSettingList, false);

		String objectType = WorkWithArticlesTest.getKavObjectName();

		Search_Types_Settings__c stSettings = new cncrg__Search_Types_Settings__c();
		stSettings.Name = 'cncrg__Knowledge__kav';
		stSettings.Active__c = true;
		stSettings.Fields_To_Display_1__c = 'ArticleArchivedDate';
		stSettings.Fields_To_Display_2__c = '';
		stSettings.Article_Type_API_Name__c = objectType;
		insert stSettings;

        
        List<sObject> articles = new List<sObject>();
        if(objectType != null) {
			for(Integer i = 0 ; i < 4; i++){
				articles.add(Schema.getGlobalDescribe().get(objectType).newSObject());
				articles[i].put('Title','test apex'+i);
				articles[i].put('Language','en_US');
				articles[i].put('UrlName','test-apex'+i);
				articles[i].put('Summary','test test test test');
			}
            insert articles;

			SObject obj = Schema.getGlobalDescribe().get(objectType).newSObject();
			obj.put('Title','test apex');
			obj.put('Language','en_US');
			obj.put('UrlName','test-apex');
			obj.put('Summary','test test test test');
			insert obj;
            List<Id> listOfIds = new List<Id>();
            for (sObject article : articles){
                listOfIds.add((String)article.get('Id'));
			}
	 	listOfIds.add((String)obj.get('Id'));

		articles = Database.query('SELECT KnowledgeArticleId FROM ' + objectType + ' WHERE Id IN: listOfIds');

		KbManagement.PublishingService.publishArticle((Id)articles[0].get('KnowledgeArticleId'), true);
		KbManagement.PublishingService.publishArticle((Id)articles[1].get('KnowledgeArticleId'), true);
		KbManagement.PublishingService.archiveOnlineArticle((Id)articles[1].get('KnowledgeArticleId'),null);
		KbManagement.PublishingService.publishArticle((Id)articles[2].get('KnowledgeArticleId'), true);
		KbManagement.PublishingService.publishArticle((Id)articles[3].get('KnowledgeArticleId'), true);
		KbManagement.PublishingService.editOnlineArticle((Id)articles[3].get('KnowledgeArticleId'), true);
		KbManagement.PublishingService.publishArticle((Id)articles[4].get('KnowledgeArticleId'), true);

		List<Article_Review__c> artReviewList = new List<Article_Review__c>();
        Article_Review__c artReview = new Article_Review__c(
                    Name = 'test name',
                    Language__c = 'en_US',
                    Article_Type__c = 'Knowledge',
					Next_Review_Date__c = Date.today().addDays(-1),
                    Parent_Article__c = (String)articles[0].get('KnowledgeArticleId'),
                    Expiration_Date__c = Datetime.now().addHours(1));
        artReviewList.add(artReview);
		Article_Review__c artReview2 = new Article_Review__c(
                    Name = 'test name2',
                    Language__c = 'en_US',
                    Article_Type__c = 'Knowledge',
                    Parent_Article__c = (String)articles[1].get('KnowledgeArticleId'),
                    Expiration_Date__c = Datetime.now().addHours(1));
        artReviewList.add(artReview2);
		Article_Review__c artReview3 = new Article_Review__c(
                    Name = 'test name3',
                    Language__c = 'en_US',
                    Article_Type__c = 'Knowledge',
                    Parent_Article__c = (String)articles[2].get('KnowledgeArticleId'),
                    Expiration_Date__c = Datetime.now().addHours(-1));
        artReviewList.add(artReview3);
		Article_Review__c artReview4 = new Article_Review__c(
                    Name = 'test name4',
                    Language__c = 'en_US',
                    Article_Type__c = 'Knowledge',
                    Parent_Article__c = (String)articles[3].get('KnowledgeArticleId'),
                    Expiration_Date__c = Datetime.now().addHours(-1));
        artReviewList.add(artReview4);
		insert artReviewList;
        }
    }

	@isTest
	private static void reviewBatchTest() {
		SchedulerSettings__c uptSet = SchedulerSettings__c.getValues(TurboEncabulatorScheduler.REVIEW_BATCH_NAME);
		uptSet.CRON__c = '0 * * * * *';
		update uptSet;
		Test.startTest();
		TurboEncabulatorScheduler tes = new TurboEncabulatorScheduler();
		String exTime = '0 2 6 * * ?';
		Id jobId = System.schedule('Test Schedule',exTime,tes);
		System.assertNotEquals(null,jobId);
		Test.stopTest();
	}

	@isTest
	private static void reviewBatchNoAccessTest() {
		Concierge_Settings__c cosSet = Concierge_Settings__c.getAll().get('Review articles after _ days');
		cosset.Value__c = null;
		update cosSet;
		Test.startTest();
		TurboEncabulatorScheduler tes = new TurboEncabulatorScheduler();
		String exTime = '0 2 6 * * ?';
		Id jobId = System.schedule('Test Schedule',exTime,tes);
		System.assertNotEquals(null,jobId);
		Test.stopTest();
		Concierge_Settings__c retcosSet = Concierge_Settings__c.getAll().get('Review articles after _ days');
		retcosSet.Value__c = '3';
		update retcosSet;
	}

	testMethod static void testParentArticles() {
		String nameOfObject = 'cncrg__Knowledge__kav';
		sObject obj = Schema.getGlobalDescribe().get(nameOfObject).newSObject();
		obj.put('Title', 'TestArticle');
		obj.put('UrlName', 'TestArticle');
		obj.put('Summary', 'TestArticle');
		insert obj;
		String kavId = obj.Id;
		obj = Database.query('SELECT KnowledgeArticleId, ArticleType, OwnerId, UrlName, Title FROM cncrg__Knowledge__kav WHERE Id = :kavId');
		String articleId = (String) obj.get('KnowledgeArticleId');
		KbManagement.PublishingService.publishArticle(articleId, true);

		String articleType = 'Knowledge';
		String title = 'child';
		Article_Review__c children = new Article_Review__c(
				Language__c = 'en_US',
				Next_Review_Date__c = Date.today(),
				Parent_Article__c = String.valueOf(obj.get('KnowledgeArticleId')),
				Name = title,
				Full_Name__c = title,
				System__c = UserInfo.getOrganizationName(),
				OwnerId = UserInfo.getUserId(),
				UrlName__c =  title,
				Article_Type__c =  articleType,
				RecordTypeId = ArticleReviewUtils.TRANSLATION_ARTICLE_RECORD_TYPE_ID
		);
		insert children;
		title = 'parent';
		Article_Review__c parent = new Article_Review__c(
				Language__c = 'en_US',
				Next_Review_Date__c = Date.today(),
				Parent_Article__c = String.valueOf(obj.get('KnowledgeArticleId')),
				Name = title,
				Full_Name__c = title,
				System__c = UserInfo.getOrganizationName(),
				OwnerId = UserInfo.getUserId(),
				UrlName__c =  title,
				Article_Type__c =  articleType,
				RecordTypeId = ArticleReviewUtils.SOURCE_ARTICLE_RECORD_TYPE_ID
		);
		insert parent;

		children = [SELECT Id, Parent_Article_Review__c FROM Article_Review__c WHERE Id = :children.Id];
		System.assertNotEquals(null, children.Parent_Article_Review__c);
		delete children;
		children = new Article_Review__c(
				Language__c = 'en_US',
				Next_Review_Date__c = Date.today(),
				Parent_Article__c = String.valueOf(obj.get('KnowledgeArticleId')),
				Name = title,
				Full_Name__c = title,
				System__c = UserInfo.getOrganizationName(),
				OwnerId = UserInfo.getUserId(),
				UrlName__c =  title,
				Article_Type__c =  articleType,
				RecordTypeId = ArticleReviewUtils.TRANSLATION_ARTICLE_RECORD_TYPE_ID
		);
		insert children;
		children = [SELECT Id, Parent_Article_Review__c FROM Article_Review__c WHERE Id = :children.Id];
		System.assertNotEquals(null, children.Parent_Article_Review__c);
	}

	testMethod static void testUpdateOrFavorite() {
		String nameOfObject = 'cncrg__Knowledge__kav';
		sObject obj = Schema.getGlobalDescribe().get(nameOfObject).newSObject();
		obj.put('Title', 'TestArticle');
		obj.put('UrlName', 'TestArticle');
		obj.put('Summary', 'TestArticle');
		insert obj;
		String kavId = obj.Id;
		obj = Database.query('SELECT KnowledgeArticleId, ArticleType, OwnerId, UrlName, Title FROM cncrg__Knowledge__kav WHERE Id = :kavId');
		String articleId = (String) obj.get('KnowledgeArticleId');
		KbManagement.PublishingService.publishArticle(articleId, true);
		delete [SELECT Id FROM Article_Review__c];
		Integer articleRecords = [SELECT COUNT() FROM Article_Review__c];
		System.assertEquals(0, articleRecords);
		ArticleReviewUtils.updateLikedORFavoritedArticle(articleId, 'en_US');
		ArticleReviewUtils.updateLikedORFavoritedArticle(articleId, 'en_US');
		articleRecords = [SELECT COUNT() FROM Article_Review__c];
		System.assertEquals(1, articleRecords);
	}
}