public without sharing class CaseSharingHandler {
	@TestVisible
	private static String OBJECT_NAME = 'CaseShare';
	private static String CASE_ORIGIN_SETTING_NAME = 'Case Origin';
	public static Boolean isFutereInsert = true;
	private static String CASE_ACCESS_LEVEL = 'Edit';

	public static void insertExecute(List<Case> caseList) {
		shareAccess(filterCase(caseList, null));
	}

	public static void updeteExecute(List<Case> caseListNew, List<Case> caseListOld) {
		shareAccess(filterCase(caseListNew, caseListOld));
	}

	private static List<Case> filterCase(List<Case> caseListNew, List<Case> caseListOld) {
		List<Case> filtredCaseList = new List<Case>();
		String defaultOrigin = cncrg__Concierge_Settings__c.getValues(CASE_ORIGIN_SETTING_NAME).Value__c;
		for (Integer i = 0, j = caseListNew.size(); i < j; i++) {
			if (caseListNew[i].Origin == defaultOrigin &&
				caseListNew[i].ContactId != null &&
				(caseListOld == null 
					|| caseListOld[i].OwnerId != caseListNew[i].OwnerId
					|| caseListOld[i].ContactId != caseListNew[i].ContactId
				)
			){

				filtredCaseList.add(caseListNew[i]);
			}
		}
		return filtredCaseList;
	}

	private static void shareAccess(List<Case> caseList) {
		if (checkCaseDataModel()) {
			Map<Id, Id> userCaseReferenceMap = getUserCaseReferenceMap(caseList);
			Set<Id> inactiveUsersSet = getInactiveUsersSet(userCaseReferenceMap);

			Boolean isFutureOrBatchContext = (!System.isFuture() && !System.isBatch());
			// i have no idea what isFutereInsert means ¯\_(ツ)_/¯ 
			if (isFutereInsert && isFutureOrBatchContext) {
				createCaseShareRecordsFuture(userCaseReferenceMap, inactiveUsersSet);
			} else {
				insertSharingRules(userCaseReferenceMap, inactiveUsersSet);
			}
		}
	}

	private static Set<Id> getInactiveUsersSet(Map<Id, Id> userCaseReferenceMap) {
		Map<Id, User> inActiveUsersMap = new Map<Id, User> ([
			SELECT Id FROM User WHERE Id IN :userCaseReferenceMap.values() AND IsActive = false
		]);
		return inActiveUsersMap.keySet();
	}

	private static Boolean checkCaseDataModel() {
		Boolean isObjectCreateble = false;
		try {
			Schema.DescribeSobjectResult[] results = Schema.describeSObjects(new List<String> { OBJECT_NAME });
			if (results[0].isCreateable()) {
				isObjectCreateble = true;
			}
		} catch (Exception e) {}
		return isObjectCreateble;
	}

	private static Map<Id, Id> getUserCaseReferenceMap(List<Case> caseList) {
		Map<Id, Id> userContactReferenceMap = new Map<Id, Id>();
		for (Case theCase: caseList) {
			userContactReferenceMap.put(theCase.Id, theCase.ContactId);
		}

		Map<Id, Contact> contactsMap = new Map<Id, Contact> ([
			SELECT Id, cncrg__UserId__c FROM Contact WHERE Id IN :userContactReferenceMap.values()
		]);

		for (Id item : userContactReferenceMap.keySet()) {
			if (contactsMap.containsKey(userContactReferenceMap.get(item)) 
				&& String.isNotBlank(contactsMap.get(userContactReferenceMap.get(item)).cncrg__UserId__c)) {
				userContactReferenceMap.put(item, contactsMap.get(userContactReferenceMap.get(item)).cncrg__UserId__c);
			} else {
				userContactReferenceMap.remove(item);
			}
		}
		return userContactReferenceMap;
	}

	@Future
	private static void createCaseShareRecordsFuture(Map<Id, Id> userCaseReferenceMap, Set<Id> inactiveUsersSet) {
		insertSharingRules(userCaseReferenceMap, inactiveUsersSet);
	}

	@TestVisible
	private static void insertSharingRules(Map<Id, Id> userCaseReferenceMap, Set<Id> inactiveUsersSet) {
		List<SObject> caseShareInsertList = new List<SObject>();
		for (Id item : userCaseReferenceMap.keySet()) {
			if (inactiveUsersSet.contains(userCaseReferenceMap.get(item))) {
				continue;
			}
			SObject objectRecord = (SObject)Type.forName(OBJECT_NAME).newInstance();
			objectRecord.put('CaseId', item);
			objectRecord.put('UserOrGroupId', userCaseReferenceMap.get(item));
			objectRecord.put('RowCause', 'Manual');
			objectRecord.put('CaseAccessLevel', CASE_ACCESS_LEVEL);
			caseShareInsertList.add(objectRecord);
		}
		Database.insert(caseShareInsertList, false);
	}
}