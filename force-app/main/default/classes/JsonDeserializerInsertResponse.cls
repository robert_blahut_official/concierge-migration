public with sharing class JsonDeserializerInsertResponse {
	public List<Integer> result;
    public Boolean error;

    public static JsonDeserializerInsertResponse parse(String json) {
        return (JsonDeserializerInsertResponse) System.JSON.deserialize(json, JsonDeserializerInsertResponse.class);
    }
}