@isTest public class DesignBGResourcePickListTest {

	@isTest static void getDefaultValueTest(){
        DesignBGResourcePickList picklistTest = new DesignBGResourcePickList();
		VisualEditor.DataRow result =  picklistTest.getDefaultValue();
		VisualEditor.DataRow defaultValueTest = new VisualEditor.DataRow('--None--', '--None--');
		System.assertEquals(0,result.compareTo(defaultValueTest));
	}

	@isTest static void getValuesTest(){
        
        init();
        
        DesignBGResourcePickList picklistTest = new DesignBGResourcePickList();
		VisualEditor.DynamicPickListRows result = picklistTest.getValues();
        
		List<Background_Settings__c> backgroundSettingsListTest = Background_Settings__c.getall().values();
         
		VisualEditor.DynamicPickListRows  backgroundValuesTest = new VisualEditor.DynamicPickListRows();
        
		backgroundValuesTest.addRow(new VisualEditor.DataRow('--None--', '--None--'));
       
        for(Background_Settings__c item : backgroundSettingsListTest) {
            backgroundValuesTest.addRow(new VisualEditor.DataRow(item.Name, item.Name));
        }
        
		System.assertEquals(backgroundValuesTest.size(),result.size());
		//System.assertEquals(backgroundValuesTest.get(0),backgroundSettingsListTest);
	}
    
    private static void init() {
        cncrg__Background_Settings__c backgroundSettings = new cncrg__Background_Settings__c();
        backgroundSettings.Name = '--None--';
        backgroundSettings.cncrg__File_Location__c = '--None--';
		backgroundSettings.cncrg__Background_File_Type__c = '--None--';
        insert backgroundSettings;
    }
}