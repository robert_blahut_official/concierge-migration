@IsTest
public class JsonDeserializerTest {

    static testMethod void testParse() {
        String json = '{  '+
        '   \"result\":{  '+
        '      \"sqlstatement\":\"SELECT id, product_name FROM products WHERE to_tsvector(\\u0027english\\u0027,coalesce(description,\\u0027\\u0027) || \\u0027 \\u0027 || coalesce(product_name,\\u0027\\u0027)) @@ plainto_tsquery(\\u0027need to connect\\u0027)\",'+
        '      \"records\":[  '+
        '         {  '+
        '            \"id\":33,'+
        '            \"product_name\":\"Genesys Connect for Service Cloud\"'+
        '         }'+
        '      ],'+
        '      \"javatimemls\":1560'+
        '   },'+
        '   \"error\":false'+
        '}';
        JsonDeserializer obj = JsonDeserializer.parse(json);
        System.assert(obj != null);
    }
}