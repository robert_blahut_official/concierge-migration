@isTest
private class ArchiveArticlesBatchTest {

	@TestSetup static void setup() {
		List<sObject> articles = new List<sObject>();
		String objectType = WorkWithArticlesTest.getKavObjectName();
		for(Integer i = 0; i < 2; i++) {
			if(objectType != null) {
				sObject obj = Schema.getGlobalDescribe().get(objectType).newSObject();
				obj.put('Title','test apex'+i);
				obj.put('Language','en_US');
				obj.put('UrlName','test-apex'+i);
				obj.put('Summary','test test test test');
				articles.add(obj);
			}
		}
		insert articles;

		cncrg__Search_Types_Settings__c setting = new cncrg__Search_Types_Settings__c(
			Name = objectType,
			cncrg__Article_Type_API_Name__c = objectType,
			cncrg__Active__c = true,
			cncrg__Fields_To_Display_1__c = 'OwnerId'
		);
		List<cncrg__Search_Types_Settings__c> settings = new List<cncrg__Search_Types_Settings__c>();
		settings.add(setting);
		ESAPI.securityUtils().validatedInsert(settings);

		List<Id> listOfIds = new List<Id>();
		for (sObject article : articles){
			listOfIds.add((String)article.get('Id'));
		}

		articles = Database.query('SELECT KnowledgeArticleId FROM ' + objectType + ' WHERE Id IN: listOfIds');
		KbManagement.PublishingService.publishArticle((Id)articles[0].get('KnowledgeArticleId'), true);
		KbManagement.PublishingService.publishArticle((Id)articles[1].get('KnowledgeArticleId'), true);
		KbManagement.PublishingService.archiveOnlineArticle((Id)articles[1].get('KnowledgeArticleId'),Datetime.now().addHours(1));
		
		Article_Review__c artReview = new Article_Review__c(
					Name = 'test name',
					Parent_Article__c = (String)articles[0].get('KnowledgeArticleId'),
					Expiration_Date__c = Datetime.now());

		Article_Review__c artReviewNoExDate = new Article_Review__c(
					Name = 'test name2',
					Parent_Article__c = (String)articles[1].get('KnowledgeArticleId'),
					Expiration_Date__c = null);

		Article_Review__c artReviewNoBatch = new Article_Review__c(
					Name = 'test name3',
					Parent_Article__c = (String)articles[1].get('KnowledgeArticleId'),
					Expiration_Date__c = Datetime.now());
		List<Article_Review__c> articleReviewList = new List<Article_Review__c>();
		articleReviewList.add(artReview);
		articleReviewList.add(artReviewNoExDate);
		articleReviewList.add(artReviewNoBatch);
		insert articleReviewList;
	}
	

	@isTest
	private static void testArchiveArticlesBatch() {
		Test.startTest();
		Set<Id> articleForArchivedIds = new Set<Id>();
		Article_Review__c artRev = [SELECT Id 
									FROM Article_Review__c 
									WHERE Name = 'test name'
									];
		articleForArchivedIds.add(artRev.Id);
		ArchiveArticlesBatch aab = new ArchiveArticlesBatch(articleForArchivedIds);
		Database.executeBatch(aab);
		Test.stopTest();
		System.assertEquals(false,ArchiveArticlesBatch.isProcessed);
	}
	
	@isTest
	private static void testArchiveArticlesBatchElse() {
		Test.startTest();
		Set<Id> articleForArchivedIds = new Set<Id>();
		Article_Review__c artRev = [SELECT Id 
									FROM Article_Review__c 
									WHERE Name = 'test name2'
									];
		articleForArchivedIds.add(artRev.Id);
		ArchiveArticlesBatch aab = new ArchiveArticlesBatch(articleForArchivedIds);
		Database.executeBatch(aab);
		Test.stopTest();
		System.assertEquals(false,ArchiveArticlesBatch.isProcessed);
	}
}