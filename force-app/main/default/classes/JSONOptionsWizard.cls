public with sharing class JSONOptionsWizard {

    public static Map<String, Integer> deserializeSuggestion (String json) {

        Map<String, Integer> result = new Map<String, Integer>();

        JSONParser parser = System.JSON.createParser(json);

        while (parser.nextToken() != null){
            String key;
            Integer value;
            if(parser.getCurrentToken() == JSONToken.FIELD_NAME && parser.getText() != 'result' && parser.getText() != 'error'){
                key = parser.getText();
                parser.nextToken();
                if(parser.getCurrentToken() == JSONToken.VALUE_NUMBER_INT) {
                    value = parser.getIntegerValue();
                }
            }

            if(key != null && value != null) {
                result.put(key, value);
            }

        }

        return result;
    }


    public static String createIndexJson (String tableName, String indexType) {

        String result = '{"indexName": "'+ tableName +'","indexType": "'+ indexType +'"}';
        return result;
    }

    public static String createTableJson (Map <String, Schema.SObjectField> fieldMap, List<String> needFields, String objectName) {

        String tempObjectName = 'temp_table';
        String result = '{"indexName": "' + tempObjectName + '", "indexType": "GIN",' +
                        '"configuration": "english",' +
                        '"table": {' +
                        '"columns": [';
        List<String> columns = new List<String>();
        for(String field : needFields){

            String fieldType = '';

            DescribeFieldResult fieldInformation = fieldMap.get(field).getDescribe();

            if(fieldInformation.getType() == DisplayType.String || fieldInformation.getType() == DisplayType.TextArea){
				fieldType = 'VARCHAR(' + fieldInformation.getLength() + ')';}

				String columnName = '"name" : "' + field + '",';
				String columnType = '"type": "' + fieldType + '",';
				String columnIndexed = '"indexed": "true",';
				String columnSelectable = '"selectable": true';

				columns.add('{' + columnName + columnType + columnIndexed + columnSelectable + '}');
			}

			result += String.join(columns, ',') + '], "name":"string"}}';

			return result;
    }

    public static String populateTableJson (List <SObject> records, List<String> fieldNames, String tableName) {
  
        String result = '{"tableName": "'+ tableName +'",';
        //Generate columns
        String columns = '"columns" : ["' + String.join(fieldNames, '", "') + '"],';
        result += columns;

        List<String> dataPart = new List<String>();
        for(SObject record : records){
            List<String> fields = new List<String>();
            for(String field : fieldNames) {

                String fieldvalue = (String)record.get(field);

                if(fieldvalue != null) {fields.add('"'+field+'":"' + fieldvalue.escapeJava() + '"'); } else { fields.add('"'+field+'":""'); }

            }

            dataPart.add('{' + String.join(fields, ',') + '}');
        }

        result += '"data":[' + String.join(dataPart, ',') + ']}';

        return result;
    }

    public static String updateIndexTableJson(List<String> fields, String tableName) {
        String result = '';

        result += '{"indexName": "'+ tableName +'",';
        result += '"indexType": "GIN","configuration": "english","table": {"columns": [';

        List<String> columnParts = new List<String>();
        for (String field : fields) {
            columnParts.add('{"name": "'+ field +'", "indexed": "true"}');
        }

        result += String.join(columnParts, ',');
        result += ']}}';

        return result;
    }

    public static String deleteIndexJson () {
        String result = null;
        return 'Test Result';
    }

    public static String creategetDocumentsJson(List<Integer> documentsIds) {

       String tableName;
       try{
            tableName = cncrg__Search_Engine_Configurations__c.getInstance(AbstractSearchEngine.ENGINE_CONFIG_SETTING_NAME).cncrg__Postgres_Table_Name__c;
            tableName = tableName.remove('documents_');       
        }
        catch (Exception e){
        }

        String result = '';

        result += '{"documentIds": [' + String.join(documentsIds, ',') + '],"tableName": "documents_' + tableName + '"}';
        return result;
    }

    public static String searchJson (String query) {

       String tableName;
       try{
            tableName = cncrg__Search_Engine_Configurations__c.getInstance(AbstractSearchEngine.ENGINE_CONFIG_SETTING_NAME).cncrg__Postgres_Table_Name__c;
            tableName = tableName.remove('documents_');      
        }
        catch (Exception e){
        }
        String result =

                '{"indexName": "'+ tableName +'",' +
                '"configuration": "english",' +
                '"maxWords": 0,' +
                '"minWords": 0,' +
                '"queriesFrom": "2016-09-09",' +
                '"showAll": true,' +
                    '"table": {' +
                        '"name": "documents_' + tableName + '",' +
                        '"columns": [{' +
                        '"name": "Title",' +
                        '"selectable": true' +
                        '},{' +
                        '"name": "Summary",' +
                        '"selectable": true' +
                        '}' +
                    ']},' +
                '"query": "'+query+'"}';

        return result;
    }

    public static String suggestionJson (String query) {

		String tableName;
        try{
            tableName = cncrg__Search_Engine_Configurations__c.getInstance(AbstractSearchEngine.ENGINE_CONFIG_SETTING_NAME).cncrg__Postgres_Table_Name__c;
            tableName = tableName.remove('documents_');           
        } catch (Exception e){
		}

        String result =

                '{"indexName": "'+ tableName +'",' +
                '"configuration": "english",' +
                '"maxWords": 5,' +
                '"minWords": 1,' +
                '"queriesFrom": "2016-09-09",' +
                '"showAll": true,' +
                    '"table": {' +
                        '"name": "documents_' + tableName + '",' +
                        '"columns": [{' +
                        '"name": "Title",' +
                        '"selectable": true' +
                        '},{' +
                        '"name": "Summary",' +
                        '"selectable": true' +
                        '}' +
                    ']},' +
                '"query": "'+query+'"}';

        return result;
    }

    public static String checkIndexJson () {

        String result = '{"name":"idx_prddescr"}';
        return result;
    }


}