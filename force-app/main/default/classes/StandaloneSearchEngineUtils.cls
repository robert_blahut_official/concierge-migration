public class StandaloneSearchEngineUtils {
	public static List<SearchPhraseWrapper> getOrderedPhrasesForText(String text, String query_string, Integer MAX_WORDS_IN_PHRASE, Integer MAX_RESULTS, Set<String> setOfOldQueries) {
		return new List<SearchPhraseWrapper>();/*
		datetime qryStart = datetime.now();
		
		text = text.toLowerCase();
		text = text.replaceAll('<[^>]+>',' ');  
		//text = text.replaceAll('[^\\p{L}\\p{M}*+]',' ').trim();
		List<SearchPhraseWrapper> result = new List<SearchPhraseWrapper>();
		Map<String, Integer> phrase_usage = new Map<String, Integer>();
				
		Integer i=0;
		Integer space_position=0;
		Integer w_ending = 0;
		Integer number_of_words =0;
		Integer text_length = text.length();
		
		while(i >= 0)
		{
			i=text.indexOf(query_string, i+1);
			if (i < 0)
				break;
			w_ending = i;
			for (Integer w=0;w<MAX_WORDS_IN_PHRASE;w++) {
				w_ending = text.indexOfChar(32, w_ending+1);
				if (w_ending < 0)
					break;
				String phrase = text.substring(i, w_ending).trim();
				if (!setOfOldQueries.contains(phrase)) {
					if (phrase_usage.containsKey(phrase))
						phrase_usage.put(phrase, phrase_usage.get(phrase) + 1);
					else
						phrase_usage.put(phrase, 1);
				}                        
			}
			
		}
		Set<String> phrases_set = phrase_usage.keySet();
		for (String phrase:phrases_set)
			result.add(new SearchPhraseWrapper(phrase.normalizeSpace(),phrase_usage.get(phrase)));
		
		result.sort();
		
		List<SearchPhraseWrapper> limited_result = new List<SearchPhraseWrapper>();
		Integer results_to_return_number = Math.min(result.size(),MAX_RESULTS);
		if (result.size()>0)
		{
			for (Integer r=0;r<results_to_return_number;r++)
				limited_result.add(result[r]);
		}
				
		return limited_result;
		*/
	}
}