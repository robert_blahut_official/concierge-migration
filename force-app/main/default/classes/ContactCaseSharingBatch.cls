public without sharing class ContactCaseSharingBatch implements Database.Batchable<sObject> {
	private final Set<Id> contactIdsSet;
	@testVisible
	private static String CASE_ORIGIN_SETTING_NAME = 'Case Origin';


	public ContactCaseSharingBatch(Set<Id> contactIdsSet) {
		this.contactIdsSet = contactIdsSet;
	}

	public ContactCaseSharingBatch(Set<String> longUserIdsSet, Set<String> shortUserIdsList) {
		String query = 'SELECT Id';
		query += ' FROM Contact WHERE cncrg__UserId__c IN :longUserIdsSet OR cncrg__UserId__c IN :shortUserIdsList';
		Map<Id, Contact> contactsMap = new Map<Id, Contact>((List<Contact>)Database.query(query));
		this.contactIdsSet = contactsMap.keySet();
	}
	public Database.QueryLocator start(Database.BatchableContext bc) {
		String defaultOrigin = cncrg__Concierge_Settings__c.getValues(CASE_ORIGIN_SETTING_NAME).Value__c;
		String query = 'SELECT Id, ContactId, Origin, OwnerId FROM Case WHERE Origin =: defaultOrigin';
		query += contactIdsSet != null ? ' AND ContactId IN :contactIdsSet' : '';
		query += ' ORDER BY ContactId';
		return Database.getQueryLocator(query);
	}
	public void execute(Database.BatchableContext BC, List<Case> scope) {
		CaseSharingHandler.isFutereInsert = false;
		CaseSharingHandler.insertExecute(scope);
	}
	public void finish(Database.BatchableContext BC) {
	}
}