public with sharing class WorkWithActionsSettings {
	@TestVisible
	private static Boolean isTest = false;

	@AuraEnabled
	public static List<Object> getAllArticleTypes() {
		return new List<Object> { getMapOfTypes(), getAllChatButtons() };
	}

	private static List<InfoWrapper> getAllChatButtons() {
		List<InfoWrapper> responseList = new List<InfoWrapper>();
		List<sObject> chatButtonsList = Database.query('SELECT Id, DeveloperName, MasterLabel FROM LiveChatButton ORDER BY MasterLabel');
		for(sObject item : chatButtonsList) {
			responseList.add(new InfoWrapper(((String)item.get('Id')).subString(0,15), (String)item.get('MasterLabel')));
		}
		return responseList;
	}

	private static List<InfoWrapper> getMapOfTypes() {
		List<InfoWrapper> listOfTypes = new List<InfoWrapper>();
		Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
		List<Search_Types_Settings__c> searchTypesSettingsList = Search_Types_Settings__c.getAll().values();
		for(Search_Types_Settings__c item : searchTypesSettingsList) {
			Schema.SObjectType sobjType;
			SObject objectRecord;
			try {
				objectRecord = (SObject)Type.forName(item.Article_Type_API_Name__c).newInstance();
			}
			catch(Exception e) {
				listOfTypes.add(
					new InfoWrapper(item.Article_Type_API_Name__c, item.Article_Type_API_Name__c)
				);
				continue;
			}
			Schema.DescribeSObjectResult describeObject = objectRecord.getSObjectType().getDescribe();
			listOfTypes.add(
				new InfoWrapper(describeObject.getName(), describeObject.getLabel())
			);
		}
		return listOfTypes;
	}

	@AuraEnabled
	public static List<InfoWrapper> getAllArticleCategories(String recordId, String articleType) {
		
		List<InfoWrapper> allCategories = new List<InfoWrapper>();
		String knowledgeObjectName;
		List<String> fieldsToCheck = new List<String>{'DataCategoryName','ParentId'};
		
		String engineType = cncrg__Search_Engine_Configurations__c.getInstance(AbstractSearchEngine.ENGINE_CONFIG_SETTING_NAME).Engine_Type__c;
		if (isTest){
			cncrg__Search_Engine_Configurations__c engineSetting = cncrg__Search_Engine_Configurations__c.getValues(AbstractSearchEngine.ENGINE_CONFIG_SETTING_NAME);
			engineType = engineSetting.cncrg__Engine_Type__c;
		}
		if (Utils.isLightningKnowledgeEnabled()){
			articleType = Utils.getLightningKnowledgeObjectName();
		}
		if (!articleType.endsWithIgnoreCase('kav')) {
			return allCategories;
		}
		if (engineType == AbstractSearchEngine.SALESFORCE_SEARCH_TYPE || engineType == AbstractSearchEngine.FEDERATED_SEARCH_TYPE){
			if (ESAPI.securityUtils().isAuthorizedToView(articleType.removeEnd('kav')+'DataCategorySelection', fieldsToCheck)){  
				if (recordId != null && articleType != null && recordId.trim() != '' && articleType.trim() != '') {
					String query = 'SELECT DataCategoryName FROM '+String.escapeSingleQuotes(articleType.removeEnd('kav'))+'DataCategorySelection' +
					' WHERE ParentId =: recordId';
			
					List<sObject> listOfCategories = Database.query(query);

					if (isTest) {
						sObject dataCategoryObject = WorkWithActionsSettingsTest.getDataCategorySelectionRecord();
						if (dataCategoryObject != null) {
							listOfCategories.add(dataCategoryObject);
						}
					}

					for (sObject category : listOfCategories) {
						allCategories.add(
							new InfoWrapper(
								String.valueOf(category.get('DataCategoryName')),
								String.valueOf(category.get('DataCategoryName')).replace('_',' ')
							)
						);
					}
					
				}
			}
			
		}
		return allCategories;
	}

	@AuraEnabled
	public static cncrg__Action_Setting__c getSettingByName(String recordId) {
		cncrg__Action_Setting__c record;
		List<String> fieldsToCheck = new List<String>{
			'Id',
			'cncrg__Source_System__c',
			'cncrg__Active__c',
			'cncrg__Category__c',
			'cncrg__ArticleId__c',
			'cncrg__Log_a_Ticket__c',
			'cncrg__Support_Name__c',
			'cncrg__Support_Number__c',
			'cncrg__URL__c',
			'cncrg__URL_Label__c',
			'cncrg__Email_Address__c',
			'cncrg__Email_Subject__c',
			'cncrg__Email_Recipient_Name__c',
			'cncrg__Flow_Name__c',
			'cncrg__Flow_Label__c'
		};

		if (ESAPI.securityUtils().isAuthorizedToView('cncrg__Action_Setting__c', fieldsToCheck)){
			List<cncrg__Action_Setting__c> records = [
				SELECT Id, cncrg__Active__c, cncrg__Source_System__c, cncrg__Type__c, cncrg__Category__c, cncrg__ArticleId__c, 
					cncrg__Log_a_Ticket__c, cncrg__Support_Name__c, cncrg__Support_Number__c, cncrg__URL__c, 
					cncrg__URL_Label__c, cncrg__Email_Address__c, cncrg__Email_Subject__c, 
					cncrg__Email_Recipient_Name__c, cncrg__Flow_Name__c, cncrg__Flow_Label__c
				FROM cncrg__Action_Setting__c
				WHERE cncrg__ArticleId__c =: recordId
			];
			record = records.isEmpty() ? null : records[0];
		}
		return record;
	}

	@AuraEnabled
	public static cncrg__Action_Setting__c createNewSetting(String settingStr) {
		List<cncrg__Action_Setting__c> settings = (List<cncrg__Action_Setting__c>)JSON.deserialize(settingStr, List<cncrg__Action_Setting__c>.class);
		cncrg__Action_Setting__c oldSetting = settings.get(0);
		cncrg__Action_Setting__c newSetting = settings.get(1);

		if (oldSetting == null) {
			// create default
			oldSetting = new cncrg__Action_Setting__c(
				cncrg__Active__c = true,
				cncrg__Log_a_Ticket__c = true,
				cncrg__Source_System__c = newSetting.cncrg__Source_System__c
			);
			ESAPI.securityUtils().validatedInsert(new List<cncrg__Action_Setting__c>{oldSetting});
		}

		Boolean isOldSettingDefault = (
			String.isBlank(oldSetting.cncrg__ArticleId__c)
			&& String.isBlank(oldSetting.cncrg__Category__c)
			&& String.isBlank(oldSetting.cncrg__Type__c)
		);
		Boolean isNewIndividualConfig = (
			String.isNotBlank(newSetting.cncrg__ArticleId__c)
			|| String.isNotBlank(newSetting.cncrg__Category__c)
			|| String.isNotBlank(newSetting.cncrg__Type__c)
		);

		if(!newSetting.cncrg__Email__c && (String.isBlank(newSetting.cncrg__Email_Address__c) &&
												String.isBlank(newSetting.cncrg__Email_Recipient_Name__c) && 
												String.isBlank(newSetting.cncrg__Email_Subject__c))) {
			newSetting.cncrg__Email_Address__c = '';
			newSetting.cncrg__Email_Recipient_Name__c = '';
			newSetting.cncrg__Email_Subject__c = '';
		}

		// if prev default
		if (isOldSettingDefault) {
			//Main configuration fields updated = create new Setting 
			// else update default
			newSetting.Id = isNewIndividualConfig ? null: oldSetting.Id;
		}

		// if prev NOT default && config updated to be default = delete the old default setting and create new Setting
		if (!isOldSettingDefault && !isNewIndividualConfig) {
			if (ESAPI.securityUtils().isAuthorizedToView('cncrg__Action_Setting__c', new List<String>{'Id'})) {
				List<cncrg__Action_Setting__c> defaultSetting = [
					SELECT Id
					FROM cncrg__Action_Setting__c
					WHERE cncrg__Source_System__c =: newSetting.cncrg__Source_System__c
						AND cncrg__ArticleId__c = null
						AND cncrg__Category__c = null
						AND cncrg__Type__c = null
				];
				defaultSetting.add(oldSetting);
				ESAPI.securityUtils().validatedDelete(defaultSetting);
			}
			// clean ID field
			newSetting.Id = null;
		}

		// if prev NOT default and config updated
		if (!isOldSettingDefault && isNewIndividualConfig) {
			// 1. if individual or common updated but main fields are not updated 
			// = just skeep step and update the newSetting
			// 2. if common config updated to be individual = create new Setting
			// 3. if individual config updated to be common = check if already exists and update existing
			if (
					(String.isNotBlank(newSetting.cncrg__ArticleId__c) 
						&& String.isBlank(oldSetting.cncrg__ArticleId__c))

					|| (String.isBlank(newSetting.cncrg__ArticleId__c)
						&& String.isNotBlank(oldSetting.cncrg__ArticleId__c))

					|| (newSetting.cncrg__Category__c != oldSetting.cncrg__Category__c
						&& (String.isNotBlank(newSetting.cncrg__Category__c)
							|| String.isNotBlank(oldSetting.cncrg__Category__c)
						))

					|| (newSetting.cncrg__Type__c != oldSetting.cncrg__Type__c
						&& (String.isNotBlank(newSetting.cncrg__Type__c)
							|| String.isNotBlank(oldSetting.cncrg__Type__c)
						))
				) {
				newSetting.Id = null;

				List<String> fieldsToCheck = new List<String> {
					'cncrg__Type__c',
					'cncrg__Category__c',
					'cncrg__ArticleId__c',
					'cncrg__Source_System__c'
				};

				if (ESAPI.securityUtils().isAuthorizedToView('cncrg__Action_Setting__c', fieldsToCheck)) {
					List<cncrg__Action_Setting__c> allSettings = [
						SELECT Id, cncrg__ArticleId__c, cncrg__Category__c, cncrg__Type__c
						FROM cncrg__Action_Setting__c
						WHERE cncrg__Source_System__c =: newSetting.cncrg__Source_System__c
							AND cncrg__ArticleId__c = null
							AND cncrg__Type__c =: newSetting.cncrg__Type__c
							AND cncrg__Category__c =: newSetting.cncrg__Category__c
						ORDER BY CreatedDate DESC
						LIMIT 50000
					];

					for (cncrg__Action_Setting__c setting: allSettings) {
						if (
							(setting.cncrg__Category__c == newSetting.cncrg__Category__c 
								&& setting.cncrg__Type__c == newSetting.cncrg__Type__c)
							|| (String.isBlank(setting.cncrg__Type__c) 
								&& String.isBlank(newSetting.cncrg__Type__c)
								&& setting.cncrg__Category__c == newSetting.cncrg__Category__c)
							|| (String.isBlank(setting.cncrg__Category__c) 
								&& String.isBlank(newSetting.cncrg__Category__c)
								&& setting.cncrg__Type__c == newSetting.cncrg__Type__c)
						) {
							newSetting.Id = setting.Id;
							break;
						}
					}
				}
			}
		}

		ESAPI.securityUtils().validatedUpsert(new List<cncrg__Action_Setting__c>{newSetting});
		newSetting.cncrg__Category__c = newSetting.cncrg__Category__c != null ? newSetting.cncrg__Category__c : '';
		newSetting.cncrg__Type__c = newSetting.cncrg__Type__c != null ? newSetting.cncrg__Type__c : '';
		return newSetting;
	}

	@AuraEnabled
	public static cncrg__Action_Setting__c deleteSettingAPEX(cncrg__Action_Setting__c newSetting) {
		if (newSetting.Id != null) {
			ESAPI.securityUtils().validatedDelete(new List<cncrg__Action_Setting__c>{newSetting});
		}
		return null;
	}

	public class InfoWrapper {
		@AuraEnabled
		public String name {get;set;}
		@AuraEnabled
		public String label {get;set;}

		public InfoWrapper(String name,String label) {
			this.name = name;
			this.label = label;
		}
	}
}