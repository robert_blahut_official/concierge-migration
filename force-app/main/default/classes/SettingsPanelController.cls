public class SettingsPanelController {
	public static final String BASE64_LABEL_FLAG = ' (Body)';

	@AuraEnabled
	public static List<List<Object>> getListOfConciergeSettings() {
		List<cncrg__Application_Tab__mdt> applicationTabList = [
			SELECT Id, DeveloperName, Label, cncrg__Order_Number__c
			FROM cncrg__Application_Tab__mdt
			ORDER BY cncrg__Order_Number__c
			LIMIT 50000
		];

		return new List<List<Object>> {
			cncrg__Concierge_Settings__c.getAll().values(),
			cncrg__Background_Settings__c.getAll().values(),
			ActionsSectionController.getActionsOrder(),
			applicationTabList
		};
	}
	@AuraEnabled
	public static List<cncrg__Trigger_Activator__c> getTriggerActivatorSettingsList() {
		List<cncrg__Trigger_Activator__c> triggerActivatorList = cncrg__Trigger_Activator__c.getAll().values();
		return triggerActivatorList;
	}

	@AuraEnabled
	public static List<Object> isLightningKnowledgeEnabled() {
		Boolean isLightningKnowlegeEnabled = Utils.isLightningKnowledgeEnabled();
		if(isLightningKnowlegeEnabled) {
			if(Utils.getLightningKnowledgeObjectName() != null) {
				return new List<Object> { true };
			}
			else {
				List<String> articlesList= new List<String> (getMapOfTypes().keySet());
				if(articlesList.size() == 1) {
					ESAPI.securityUtils().validatedInsert(new List<cncrg__Concierge_Settings__c> {new cncrg__Concierge_Settings__c(Name = Utils.FIELD_LIGHTNING_KNOWLEDGE_OBJECT_NAME, Value__c = articlesList[0])});
					delete cncrg__Search_Types_Settings__c.getAll().values();
					return new List<Object> { true };
				}
				else {
					return new List<Object> { true, articlesList};
				}
			}
		}
		return new List<Object> { false };
	}
	@AuraEnabled
	public static void saveLightningArticleType(String articleType) {
		ESAPI.securityUtils().validatedDelete(cncrg__Search_Types_Settings__c.getAll().values());
		ESAPI.securityUtils().validatedInsert(new List<cncrg__Concierge_Settings__c> {new cncrg__Concierge_Settings__c(Name = Utils.FIELD_LIGHTNING_KNOWLEDGE_OBJECT_NAME, Value__c = articleType)});
	}
	@AuraEnabled
	public static List<TypesWrapper> getTypesSettings() {

		Map<String,Map<String,FieldWrapper>> allArticleTypes = Utils.isLightningKnowledgeEnabled() ? getMapOfRecordTypes() : getMapOfTypes();
		List<cncrg__Search_Types_Settings__c> allInfo = new List<cncrg__Search_Types_Settings__c>();
		List<String> fieldsToCheck = new List<String>{'Name',
													  'cncrg__Active__c',
													  'cncrg__Fields_To_Display_1__c',
													  'cncrg__Fields_To_Display_2__c',
													  'cncrg__Fields_To_Display_3__c',
													  'cncrg__Fields_To_Display_4__c',
													  'cncrg__Article_Type_API_Name__c'};

		if(ESAPI.securityUtils().isAuthorizedToView('cncrg__Search_Types_Settings__c',fieldsToCheck)){
			allInfo = [
				SELECT
							Id,
							Name,
							Active__c,
							Fields_To_Display_1__c,
							Fields_To_Display_2__c,
							Fields_To_Display_3__c,
							Fields_To_Display_4__c,
							Article_Type_API_Name__c
				FROM		Search_Types_Settings__c
				WHERE		Is_Usual_Object__c = false
				ORDER BY	Name ASC
				LIMIT		1000
			];
		}
		Map<String,TypesWrapper> convertedInfoMap = new Map<String,TypesWrapper>();

		for (Search_Types_Settings__c info : allInfo) {
			List<String> fieldsToDisplay =  new List<String>();
			if (info.Article_Type_API_Name__c!= null) {
				if(info.Fields_To_Display_1__c!=null) {
					fieldsToDisplay = info.Fields_To_Display_1__c.split(',');
				}
				if(info.Fields_To_Display_2__c!=null) {
					fieldsToDisplay.addAll(info.Fields_To_Display_2__c.split(','));
				}
				if(info.Fields_To_Display_3__c!=null) {
					fieldsToDisplay.addAll(info.Fields_To_Display_3__c.split(','));
				}
				if(info.Fields_To_Display_4__c!=null) {
					fieldsToDisplay.addAll(info.Fields_To_Display_4__c.split(','));
				}

				Integer fieldPosition = 1;
				TypesWrapper setting = new TypesWrapper(info.Article_Type_API_Name__c, info.Active__c,info.Id, info.Name);
				setting.selectedFields = new List<FieldWrapper>();

				if (allArticleTypes.get(info.cncrg__Article_Type_API_Name__c)!= null) {
					FieldWrapper attachmentWrap = null;
					for (String field : fieldsToDisplay) {
						if (field == Utils.ATTACHMENTS_FLAG_NAME) {
							attachmentWrap = new FieldWrapper(
								Utils.ATTACHMENTS_FLAG_NAME,
								Label.Display_Attachments_List,
								'attachments_flag',
								null,
								null
							);
							continue;
						}

						if (allArticleTypes.get(info.cncrg__Article_Type_API_Name__c).containsKey(field)) {
							FieldWrapper newField = allArticleTypes.get(info.cncrg__Article_Type_API_Name__c).get(field);
							newField.position = fieldPosition;
							setting.selectedFields.add(newField);
							fieldPosition++;
						}
					}
					if (attachmentWrap != null) {
						attachmentWrap.position = fieldPosition;
						if (Utils.isLightningKnowledgeEnabled()) {
							setting.selectedFields.add(attachmentWrap);
						}
					}
					convertedInfoMap.put(info.cncrg__Article_Type_API_Name__c, setting);
				}
			}
		}

		for (String type : allArticleTypes.keySet()) {
			if (convertedInfoMap.keySet().contains(type)) {
				convertedInfoMap.get(type).allAvailableFields = allArticleTypes.get(type).values();
			} else {
				TypesWrapper setting = new TypesWrapper(type, false, null,'');
				setting.selectedFields = new List<FieldWrapper>();
				setting.allAvailableFields = allArticleTypes.get(type).values();
				convertedInfoMap.put(type, setting);
			}

			FieldWrapper attachmentWrap = new FieldWrapper(
				Utils.ATTACHMENTS_FLAG_NAME,
				Label.Display_Attachments_List,
				'attachments_flag',
				null,
				null
			);
			if (Utils.isLightningKnowledgeEnabled()) {
				convertedInfoMap.get(type).allAvailableFields.add(attachmentWrap);
			}
		}
		return convertedInfoMap.values();
	}

	public class TypesWrapper {
		@AuraEnabled public String articleType {get;set;}
		@AuraEnabled public List<FieldFilter> filterList {get;set;}
		@AuraEnabled public String articleTypeLabel {get;set;}
		@AuraEnabled public List<FieldWrapper> selectedFields {get;set;}
		@AuraEnabled public List<FieldWrapper> allAvailableFields {get;set;}
		@AuraEnabled public String title {get;set;}
		@AuraEnabled public Boolean isActive {get;set;}
		@AuraEnabled public String recordId {get;set;}
		@AuraEnabled public String name {get;set;}

		public TypesWrapper(String articleType, Boolean isActive, String recordId,String name) {
			this.articleType = articleType;
			this.isActive = isActive;
			this.recordId = recordId;
			this.name = name;
		}
		public TypesWrapper(String articleType, Boolean isActive, String recordId,String name, String Label) {
			this(articleType, isActive, recordId, name);
			this.articleTypeLabel = Label;
		}
		public TypesWrapper(String articleType, Boolean isActive, String recordId,String name, String Label, String filtres) {
			this(articleType, isActive, recordId, name, Label);
			this.filterList = new List<FieldFilter>();
			if(String.isNotBlank(filtres)) {
				List<String> conditionList = filtres.split(';');
				for(Integer i=0, j=conditionList.size(); i<j; i+=3) {
					this.filterList.add(new FieldFilter(conditionList[i], conditionList[i+1], conditionList[i+2]));
				}
			}
		}
	}

	public class FieldFilter {
		@AuraEnabled public String field {get;set;}
		@AuraEnabled public String expression {get;set;}
		@AuraEnabled public String fieldValue {get;set;}

		public FieldFilter(String field, String expression, String fieldValue) {
			this.field = field;
			this.expression = expression;
			this.fieldValue = fieldValue;
		}
	}

	public class FieldWrapper {// implements Comparable
		@AuraEnabled public String fieldName {get;set;}
		@AuraEnabled public String fieldLabel {get;set;}
		@AuraEnabled public String fieldType {get;set;}
		@AuraEnabled public Integer position {get;set;}
		@AuraEnabled public String recordId {get;set;}

		public FieldWrapper(String fieldName, String fieldLabel, String fieldType, Integer position, String recordId) {
			this.fieldName = fieldName;
			this.fieldLabel = fieldLabel;
			this.fieldType = fieldType;
			this.position = position;
			this.recordId = recordId;
		}
	}

	public static Map<String,Map<String,FieldWrapper>> getMapOfTypes() {
		Map<String,Map<String,FieldWrapper>> mapOfTypes = new Map<String,Map<String,FieldWrapper>>();
		Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
		Set<String> keySet = gd.keySet();
		for (String key : keySet) {
			Schema.SObjectType objectType = gd.get(key);
			if (key.endsWith('kav')) {

				mapOfTypes.put(
					objectType.getDescribe().getName(),
					new Map<String,FieldWrapper>(
						getAllFieldsMap(
							objectType.getDescribe().getName()
						)
					)
				);
			}
		}
		return mapOfTypes;
	}

	private static Map<String,Map<String,FieldWrapper>> getMapOfRecordTypes() {
		Map<String,Map<String,FieldWrapper>> mapOfTypes = new Map<String,Map<String,FieldWrapper>>();
		Map<String,Schema.RecordTypeInfo> recordTypesMapInfo = new Map<String,Schema.RecordTypeInfo>();
		String knowledgeArticleObjectName;
		recordTypesMapInfo = Utils.getRecordTypesMapInfo();
		knowledgeArticleObjectName = Utils.getLightningKnowledgeObjectName();
		for(String recordTypeName: recordTypesMapInfo.keySet()) {
			//if(recordTypeName != 'Master' || recordTypesMapInfo.size() == 1) {	
			if(recordTypeName != 'Master') {	
				mapOfTypes.put(
					recordTypeName,
					new Map<String,FieldWrapper>(
						getAllFieldsMap(knowledgeArticleObjectName)
					)
				);
			}
		}
		return mapOfTypes;
	}

	public static Map<String,FieldWrapper> getAllFieldsMap(String typeName) {
		Map<String,Schema.SObjectField> fieldMap = Utils.getDescribeSObjectResult(typeName).fields.getMap();

		Map<String,FieldWrapper> allFieldsMap = new Map<String,FieldWrapper>();
		for (Schema.SObjectField sfield : fieldMap.values()) {
			Schema.DescribeFieldResult dfield = sfield.getDescribe();
			if (dfield.getName() != 'Id' && dfield.getName() != 'Title'
				&& dfield.getLabel()!= 'User ID'
				&& dfield.getType() != Schema.DisplayType.ADDRESS) {

				allFieldsMap.put(
					dfield.getName(),
					new FieldWrapper(
						dfield.getName(),
						dfield.getLabel(),
						dfield.getType().name(),
						null,
						null
					)
				);
			}
		}
		return unionFields(allFieldsMap);
	}
	private static Map<String, FieldWrapper> unionFields(Map<String, FieldWrapper> fieldMap) {
		Map<String, FieldWrapper> result = new Map<String, FieldWrapper>();
		FieldWrapper currentWrapper;
		String currentUnionField;
		Boolean isNormalField;
		Set<String> multiFieldsSet = new Set<String>();
		for(String key : fieldMap.keySet()) {
			currentWrapper = fieldMap.get(key);
			if(currentWrapper.fieldType.equals(Schema.DisplayType.BASE64.name())) {
				currentUnionField = currentWrapper.fieldLabel.replace(BASE64_LABEL_FLAG, '');
				multiFieldsSet.add(currentUnionField);
				currentWrapper.fieldLabel = currentUnionField;
				result.put(currentWrapper.fieldName, currentWrapper);
			}
		}
		for(String key : fieldMap.keySet()) {
			currentWrapper = fieldMap.get(key);
			isNormalField = true;
			for(String unionField : multiFieldsSet) {
				if(currentWrapper.fieldLabel.contains(unionField)) {
					isNormalField = false;
				}
			}
			if(isNormalField) {
				result.put(key, currentWrapper);
			}
		}
		return result;
	}

	@AuraEnabled
	public static List<sObject> getSearchEngineConfiguration() {
		Search_Engine_Configurations__c search_Engine_Configurations = Search_Engine_Configurations__c.getInstance(AbstractSearchEngine.ENGINE_CONFIG_SETTING_NAME);
		Search_Settings__c searchSettings = Search_Settings__c.getOrgDefaults();
		Web_Service_Settings__c ws;
		if(Web_Service_Settings__c.getInstance('HerokuWebService') == null){
			ws = new cncrg__Web_Service_Settings__c(Name = 'HerokuWebService');
			Database.insert(ws,false);
		} else
			ws = Web_Service_Settings__c.getInstance('HerokuWebService');
		return new List<sObject> {search_Engine_Configurations, searchSettings, ws};
	}
	@AuraEnabled
	public static void implementFieldMaps(List<String> usersFieldsList, List<String> historyFieldsList) {
		Set<String> fieldsSet = new Set<String>();
		Map<String, String> userFieldDependencyMap = reverseMap(getFieldDependencyMap('User'));
		Map<String, String> historyFieldDependencyMap = reverseMap(getFieldDependencyMap('cncrg__Search_History__c'));
		List<Concierge_Field_Mappings__c> allField_Mappings = Concierge_Field_Mappings__c.getall().values();
		if(allField_Mappings.size() > 0) {
			ESAPI.securityUtils().validatedDelete(Concierge_Field_Mappings__c.getall().values());
		}
		for(Integer i=0; i<usersFieldsList.size(); i++) {
			fieldsSet.add(userFieldDependencyMap.get(usersFieldsList[i]) + ':' + historyFieldDependencyMap.get(historyFieldsList[i]));
		}
		List<Concierge_Field_Mappings__c> fieldMappingsList = new List<Concierge_Field_Mappings__c>();
		for(String item : fieldsSet) {
			fieldMappingsList.add(new Concierge_Field_Mappings__c(Name = String.valueOf(fieldMappingsList.size()),Value__c = item));
		}
		if(fieldMappingsList.size() > 0) {
			ESAPI.securityUtils().validatedInsert(fieldMappingsList);
		}
	}
	@AuraEnabled
	public static void implementNewTriggerActivatorSettings(String triggerActivatorSettings) {
		List<cncrg__Trigger_Activator__c> triggerActivatorSettingsList = (List<cncrg__Trigger_Activator__c>) JSON.deserialize(triggerActivatorSettings, List<cncrg__Trigger_Activator__c>.class);
		if(triggerActivatorSettingsList != null && triggerActivatorSettingsList.size() > 0) {
			ESAPI.securityUtils().validatedUpdate(triggerActivatorSettingsList);
		}
	}
	private static Map<String, String> reverseMap(Map<String, String> directlyMap) {
		Map<String, String> reversMap = new Map<String, String>();
		for(String key : directlyMap.keySet()) {
			reversMap.put(directlyMap.get(key), key);
		}
		return reversMap;
	}
	@AuraEnabled
	public static String implementNewSettings(String newSettings) {

		NewSettingsClass newSettingsToImplement = (NewSettingsClass)JSON.deserialize(newSettings, NewSettingsClass.class);
		if(newSettingsToImplement.cs != null) {
			ESAPI.securityUtils().validatedUpdate(newSettingsToImplement.cs);
			ArticleReviewUtils.updateBatchStatus();
		}
		else if(newSettingsToImplement.sec != null) {
			ESAPI.securityUtils().validatedUpdate(new List<Search_Engine_Configurations__c>{newSettingsToImplement.sec});
		}
		if(newSettingsToImplement.ws != null) {
			ESAPI.securityUtils().validatedUpdate(new List<Web_Service_Settings__c>{newSettingsToImplement.ws});
		}
		if(newSettingsToImplement.search != null) {
			ESAPI.securityUtils().validatedUpsert(new List<Search_Settings__c> { newSettingsToImplement.search });
		}
		if(newSettingsToImplement.ts != null) {
			List<Search_Types_Settings__c> settings = new List<Search_Types_Settings__c>();
			for (TypesWrapper setting : newSettingsToImplement.ts) {
				if (setting.isActive != null) {
					String fieldsToDisplayArray1 = '';
					String fieldsToDisplayArray2 = '';
					String fieldsToDisplayArray3 = '';
					String fieldsToDisplayArray4 = '';
					Integer fieldsCounter = 0;
					if(String.isBlank(setting.recordId) && setting.selectedFields.size() == 0) {
						continue;
					}

					for (FieldWrapper fieldInfo : setting.selectedFields) {
						if (fieldInfo.position != null && fieldsCounter < 5) {
							fieldsToDisplayArray1+=fieldInfo.fieldName+',';
						}else if(fieldInfo.position != null && fieldsCounter < 10){
							fieldsToDisplayArray2+=fieldInfo.fieldName+',';
						} else if (fieldInfo.position != null && fieldsCounter < 15){
							fieldsToDisplayArray3+=fieldInfo.fieldName+',';
						} else if(fieldInfo.position != null && fieldsCounter < 20){
							fieldsToDisplayArray4+=fieldInfo.fieldName+',';
						}
						fieldsCounter++;
					 }

					String filtresLine = '';
					if(setting.filterList != null && setting.filterList.size() > 0) {
						for(FieldFilter item : setting.filterList) {
							filtresLine += item.field + ';' + item.expression + ';' + (String.isBlank(item.fieldValue) ? 'null' : item.fieldValue) + ';';
						}
					}
					settings.add(
						new Search_Types_Settings__c(
							Name = setting.name,
							Article_Type_API_Name__c = setting.articleType,
							Active__c = setting.isActive,
							Fields_To_Display_1__c = fieldsToDisplayArray1,
							Fields_To_Display_2__c = fieldsToDisplayArray2,
							Fields_To_Display_3__c = fieldsToDisplayArray3,
							Fields_To_Display_4__c = fieldsToDisplayArray4,
							Id = setting.recordId,
							cncrg__Title_Field__c = setting.title,
							cncrg__Is_Usual_Object__c = String.isBlank(setting.title) ? false : true,
							Field_Filters__c = filtresLine
						)
					);
				}
			}
			if (!settings.isEmpty())
				ESAPI.securityUtils().validatedUpsert(settings);
		}
		return 'Success';
	}

	public class NewSettingsClass {
		public List<Concierge_Settings__c> cs {get;set;}
		public Search_Engine_Configurations__c sec {get;set;}
		public Web_Service_Settings__c ws {get;set;}
		public List<TypesWrapper> ts {get;set;}
		public Search_Settings__c search {get;set;}
	}
	@AuraEnabled
	public static FieldMappingWrapper getFieldMappings() {
		FieldMappingWrapper fieldMappingWrapperRecord = new FieldMappingWrapper();
		fieldMappingWrapperRecord.userFieldsList = getFieldsList('User', exceptionUserFields, false);
		fieldMappingWrapperRecord.searchHistoryList = getFieldsList('cncrg__Search_History__c', new Set<String>(), true);
		List<Concierge_Field_Mappings__c> field_MappingsList = Concierge_Field_Mappings__c.getall().values();
		List<FieldMapping> fieldMappingList = new List<FieldMapping>();
		for(Concierge_Field_Mappings__c item : field_MappingsList) {
			fieldMappingList.add(new FieldMapping(item.Value__c.substringBefore(':'), item.Value__c.substringAfter(':')));
		}
		changeToLabelFieldsName(fieldMappingList);
		fieldMappingWrapperRecord.fieldMappingList = fieldMappingList;
		return fieldMappingWrapperRecord;
	}

	@AuraEnabled
	public static List<CaseTypesInfoWrapper> getCaseTypesSettings(){
		List<CaseTypesInfoWrapper>  listOfCaseRecordTypes = new List<CaseTypesInfoWrapper>();
		String tempRecordTypeName = '';

		Schema.DescribeFieldResult dfr = Schema.SObjectType.Case.fields.Type;
		List<Schema.PicklistEntry> pickListValues = Schema.SObjectType.Case.fields.Type.getPicklistValues();
		for(Schema.PicklistEntry item : pickListValues){
			listOfCaseRecordTypes.add(
					new CaseTypesInfoWrapper(item.getLabel(),item.getValue())
			);
		}
		return listOfCaseRecordTypes;
	}


	private static void changeToLabelFieldsName(List<FieldMapping> fieldMappingList) {
		Map<String, String> userFieldDependencyMap = getFieldDependencyMap('User');
		Map<String, String> historyFieldDependencyMap = getFieldDependencyMap('cncrg__Search_History__c');
		for(FieldMapping item : fieldMappingList) {
			//if(item.sobjectName == 'User') {
			//    item.fieldName = userFieldDependencyMap.get(item.fieldName);
			//}
			//else {
			//    item.fieldName = historyFieldDependencyMap.get(item.fieldName);
			//}
			item.usersField = userFieldDependencyMap.get(item.usersField);
			item.historyField = historyFieldDependencyMap.get(item.historyField);
		}
	}
	private static Map<String, String> getFieldDependencyMap(String sObjectName) {
		Map<String, String> fieldDependencyMap = new Map<String, String>();
		SObject objectRecord = (SObject)Type.forName(sObjectName).newInstance();
		Schema.SObjectType sObjType = objectRecord.getSObjectType();
		List <Schema.SObjectField> fieldList = sObjType.getDescribe().fields.getMap().values();
		for(Schema.SObjectField item : fieldList) {
			Schema.DescribeFieldResult describeField = item.getDescribe();
			fieldDependencyMap.put(describeField.getName(), describeField.getLabel());
		}
		if(sObjectName == 'User') {
			fieldDependencyMap.put('Profile', 'Profile');
			fieldDependencyMap.put('Role', 'Role');
		}
		return fieldDependencyMap;
	}
	private static List<String> getFieldsList(String sObjectName, Set<String> exceptionFieldSet, Boolean isOnlyWritableFields) {
		List <Schema.SObjectField> fieldList = Schema.getGlobalDescribe().get(sObjectName).getDescribe().fields.getMap().values();
		List<String> fieldsList = new  List<String>();
		for(Schema.SObjectField item : fieldList) {
			if(isOnlyWritableFields && !item.getDescribe().isUpdateable()) {
				continue;
			}
			Schema.DescribeFieldResult describeField = item.getDescribe();
			if(!exceptionFieldSet.contains(describeField.getName())) {
				fieldsList.add(describeField.getLabel());
			}
		}
		if(sObjectName == 'User') {
			fieldsList.add('Profile');
			fieldsList.add('Role');
		}
		fieldsList.sort();
		return fieldsList;
	}
	private static Set<String> exceptionUserFields {
		get {
			return new Set<String> {
				'AboutMe',
				'Alias',
				'BannerPhotoUrl',
				'CommunityNickname',
				'ContactId',
				'DelegatedApproverId',
				'Email',
				'EmployeeNumber',
				'Extension',
				'FederationIdentifier',
				'FirstName',
				'FullPhotoUrl',
				'Id',
				'LastName',
				'MediumBannerPhotoUrl',
				'MiddleName',
				'MobilePhone',
				'Name',
				'Nickname', //field doesn't exist
				'Phone',
				'SenderEmail',
				'SenderName',
				'Signature',
				'SmallBannerPhotoUrl',
				'SmallPhotoUrl',
				'Title',
				'Username'
			};
		} set;
	}
	public class FieldMapping {
		public FieldMapping(String usersField, String historyField) {
			this.usersField = usersField;
			this.historyField = historyField;
		}
		@AuraEnabled
		public String usersField {get;set;}
		@AuraEnabled
		public String historyField {get;set;}
	}
	public class FieldMappingWrapper {
		@AuraEnabled
		public List<FieldMapping> fieldMappingList {get;set;}
		@AuraEnabled
		public List<String> userFieldsList {get;set;}
		@AuraEnabled
		public List<String> searchHistoryList {get;set;}
	}

	public class CaseTypesInfoWrapper {
		@AuraEnabled
		public String label {get;set;}
		@AuraEnabled
		public String name {get;set;}

		public CaseTypesInfoWrapper(String typeLabel, String typeName){
			this.label = typeLabel;
			this.name = typeName;
		}
	}
	@AuraEnabled
	public static Boolean isCurrentUserAdmin(){
		return SearchQueryInputController.getIsAdminProfile();
	}

	@AuraEnabled
	public static List<ScheduleWrapper> getSchedulerSettings() {
		List<ScheduleWrapper> result = new List<ScheduleWrapper>();
		List<SchedulerSettings__c> settingList = SchedulerSettings__c.getAll().values();
		for(Integer i = 0; i < settingList.size(); i++) {
			result.add(new ScheduleWrapper(settingList[i]));
		}
		result.sort();
		return result;
	}

	@AuraEnabled
	public static List<String> getBatchList() {
		Set<String> result = new Set<String>();
		List<List<sObject>> searchResult = [FIND 'global extends AbstractConciergeBatch' IN ALL FIELDS RETURNING ApexClass(Id, NamespacePrefix, Name)];
		ApexClass currentClass;
		String className;
		if (!searchResult.isEmpty()) {
			for(Integer i = 0; i < searchResult[0].size(); i++) {
				currentClass = (ApexClass) searchResult[0][i];
				if(String.isNotBlank(currentClass.NamespacePrefix)) {
					className = currentClass.NamespacePrefix + '.' + currentClass.Name;
				} else {
					className = currentClass.Name;
				}
				if(className != SettingsPanelController.class.getName()) {
					result.add(className);
				}
			}
		}
		List<SchedulerSettings__c> settings = SchedulerSettings__c.getAll().values();
		for(Integer i = 0; i < settings.size(); i++) {
			result.add(settings[i].Class_Name__c);
		}
		return new List<String>(result);
	}

	@AuraEnabled
	public static CRONWrapper parseCron(String cronValue) {
		return new CRONWrapper(cronValue);
	}

	@AuraEnabled
	public static String getNextRunDate(String cronValue, String  stName) {
		SchedulerSettings__c currentSettings = SchedulerSettings__c.getAll().get(stName);
		Datetime lastDate;
		if(currentSettings != null) {
			currentSettings.CRON__c = cronValue;
			lastDate = CronUtility.checkLastRun(currentSettings);
		} else {
			lastDate = Datetime.now();
		}
		return CronUtility.convertCronToNextDatetime(cronValue, lastDate).format(CronUtility.NORMAL_DATETIME_FORMAT);
	}

	@AuraEnabled
	public static void changeActionsOrder(String actionsList) {
		cncrg__Concierge_Settings__c settingRecord = cncrg__Concierge_Settings__c.getValues(ActionsSectionController.ARTICLE_ACTIONS_DISPLAY_ORDER);
		settingRecord.Value__c = actionsList.replaceAll(',', ';');
		update settingRecord;
	}

	@AuraEnabled
	public static void updateSchedulerSettings(String jsonSettings) {
		List<ScheduleWrapper> newSettingsToImplement = (List<ScheduleWrapper>)JSON.deserialize(jsonSettings, List<ScheduleWrapper>.class);
		List<SchedulerSettings__c> settingsToUpdate = new List<SchedulerSettings__c>();
		List<SchedulerSettings__c> settingsToRemove = new List<SchedulerSettings__c>();
		ScheduleWrapper currentWrapper;
		for(Integer i = 0; i < newSettingsToImplement.size(); i++) {
			currentWrapper = newSettingsToImplement[i];
			if(currentWrapper.isRemove) {
				if(String.isNotBlank(currentWrapper.recId)) {
					settingsToRemove.add(new SchedulerSettings__c(Id = currentWrapper.recId));
				}
			} else {
				settingsToUpdate.add(
						new SchedulerSettings__c(
								Id = currentWrapper.recId,
								CRON__c = currentWrapper.CRON,
								Active__c = currentWrapper.active,
								Batch_Size__c = currentWrapper.batchSize,
								Name = currentWrapper.settingName,
								Class_Name__c = currentWrapper.className,
								Description__c = currentWrapper.description
						)
				);
			}
		}
		if(!settingsToUpdate.isEmpty()) {
			ESAPI.securityUtils().validatedUpsert(settingsToUpdate);
		}
		if(!settingsToRemove.isEmpty()) {
			ESAPI.securityUtils().validatedDelete(settingsToRemove);

		}
		TurboEncabulatorScheduler.recalculate();
	}

	public class ScheduleWrapper implements Comparable {
		@AuraEnabled public String recId {get;set;}
		@AuraEnabled public String settingName {get;set;}
		@AuraEnabled public String className {get;set;}
		@AuraEnabled public String description {get;set;}
		@AuraEnabled public String CRON {get;set;}
		@AuraEnabled public Datetime lastRun {get;set;}
		@AuraEnabled public Boolean active {get;set;}
		@AuraEnabled public Integer lastDuration {get;set;}
		@AuraEnabled public String nextRun {get;set;}
		@AuraEnabled public Integer batchSize {get;set;}
		@AuraEnabled public Boolean isRemove {get;set;}
		@AuraEnabled public Datetime createdDate{get;set;}
		@AuraEnabled public Boolean isRequired {get;set;}

		public ScheduleWrapper(SchedulerSettings__c setting) {
			this.recId = setting.id;
			this.className = setting.Class_Name__c;
			this.settingName = setting.Name;
			this.description = setting.Description__c;
			this.CRON = setting.CRON__c;
			this.lastRun = setting.Last_Run__c;
			this.active = setting.Active__c;
			this.lastDuration = Integer.valueOf(setting.Last_duration__c);
			this.batchSize = Integer.valueOf(setting.Batch_Size__c);
			this.isRemove = false;
			this.isRequired = setting.isStandard__c;
			this.createdDate = setting.CreatedDate;
			Datetime tempTime = CronUtility.checkLastRun(setting);
			if(tempTime < Datetime.now()) {
				tempTime = Datetime.now();
			}
			this.nextRun = CronUtility.convertCronToNextDatetime(this.CRON, tempTime).format(CronUtility.NORMAL_DATETIME_FORMAT);
		}

		public Integer compareTo(Object compareTo) {
			ScheduleWrapper currentUWrapper = (ScheduleWrapper) compareTo;
			if(this.active && !currentUWrapper.active)
				return -1;
			if(!this.active && currentUWrapper.active)
				return 1;
			if(this.createdDate.getTime() > currentUWrapper.createdDate.getTime())
				return 1;
			if(this.createdDate.getTime() < currentUWrapper.createdDate.getTime())
				return -1;
			return 0;
		}
	}

	public class CRONWrapper {
		@AuraEnabled public List<CronUtility.CRONOptionWrapper> optionList {get;set;}
		public CRONWrapper(String cronValue) {
			List<String> splitCronList = cronValue.split(CronUtility.CRON_SEPARATOR);
			optionList = new List<CronUtility.CRONOptionWrapper>();
			for(CronUtility.CRON_MODE mode : CronUtility.CRON_MODE.values()) {
				if(mode != CronUtility.CRON_MODE.SECONDS)
					optionList.add(
							new CronUtility.CRONOptionWrapper(mode.name(), splitCronList[mode.ordinal()], mode)
					);
			}
		}
	}
}