@isTest public class UpdatesOnFeedItemsTriggerTest { 
	@isTest public static void addFeedItemTest(){
		User u = new User(
			Alias				= 'testu',
			Email				= 'testuser@testorg.com',
			EmailEncodingKey	= 'UTF-8',
			LastName			= 'Testuser',
			LanguageLocaleKey	= 'en_US',
			LocaleSidKey		= 'en_US',
			ProfileId			= UserInfo.getProfileId(),
			TimeZoneSidKey		= 'America/Los_Angeles',
			UserName			= 'testu@testorg.com'
		);
		Case c = new Case();
		c.Subject		= 'test';
		c.Description	= 'test';
		c.Status		= 'New';
		c.Origin		= 'Web';
		insert c;
		FeedItem feedRecord = new FeedItem(
				ParentId	= c.Id,
				Body		= 'Test',
				Title		= 'Test',
				Visibility  = 'AllUsers' 
			);
		System.runAs(u) {
			insert feedRecord;
		}
		System.assert(feedRecord.Id != null);
	}

	/*@isTest public static void addFeedItemMissingCasesTest(){
		Profile p = [SELECT Id FROM Profile WHERE Name = 'Standard User'];
		User u = new User(
			Alias				= 'testu',
			Email				= 'testuser@testorg.com',
			EmailEncodingKey	= 'UTF-8',
			LastName			= 'Testuser',
			LanguageLocaleKey	= 'en_US',
			LocaleSidKey		= 'en_US',
			ProfileId			= p.Id,
			TimeZoneSidKey		= 'America/Los_Angeles',
			UserName			= 'testu@testorg.com'
		);
		Case c = new Case();
		c.Subject		= 'test';
		c.Description	= 'test';
		c.Status		= 'New';
		c.Origin		= 'Web';
		insert c;
		insert new FeedItem(
			ParentId	= '50046000004JL9j',
			Body		= 'Test',
			Title		= 'Test',
			Visibility  = 'AllUsers' 
		);
	}
	*/
}