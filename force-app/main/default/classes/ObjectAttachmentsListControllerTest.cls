@IsTest
private class ObjectAttachmentsListControllerTest {
	@IsTest
	static void getAttachmentsMapTest() {
		Account newAccount = new Account(
				Name = 'newAccount'
		);
		insert newAccount;
		Contact newContact = new Contact(
				LastName = 'Brown',
				FirstName = 'Kate',
				AccountId = newAccount.Id
		);
		insert newContact;
		Case newCase = new Case(
				AccountId = newAccount.Id,
				ContactId = newContact.Id,
				Subject = 'salesforce'
		);
		insert newCase;
		Attachment newAttachment = new Attachment(
				Name ='newAttachment',
				ParentId = newCase.Id,
				Body = Blob.valueOf('Body')
		);
		insert newAttachment;
		ContentVersion contentVersion = new ContentVersion(
				Title = 'Penguins',
				PathOnClient = 'Penguins.jpg',
				VersionData = Blob.valueOf('Test Content'),
				IsMajorVersion = true
		);
		insert contentVersion;
		List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
		ContentDocumentLink newDocumentLink = new ContentDocumentLink();
		newDocumentLink.LinkedEntityId = newCase.Id;
		newDocumentLink.ContentDocumentId = documents[0].Id;
		newDocumentLink.ShareType = 'V';
		insert newDocumentLink;
		Map<String, String> attachmentsMap = ObjectAttachmentsListController.getAttachmentsMap(newCase.Id);
		System.assert(attachmentsMap != null);
	}
}