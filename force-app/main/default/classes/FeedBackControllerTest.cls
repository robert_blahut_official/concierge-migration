/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class FeedBackControllerTest {

    static testMethod void testFeedBackControllerWithCase() {
        Case testCase = new Case();
        testCase.Score__c = 4;
        testCase.Comments__c = 'The exact article was found';
        testCase.Suggestion__c = 'User iteraction can be improved';
        //insert testCase;
        ESAPI.securityUtils().validatedInsert(new List<Case>{testCase});

        FeedBackController.moveFeedbackToCase(testCase);
        Case testCase2 = FeedBackController.getCurrentCase(testCase.Id);

        System.assert(testCase2.Suggestion__c == testCase.Suggestion__c);
        System.assert(testCase2.Score__c == testCase.Score__c);
        System.assert(testCase2.Comments__c == testCase.Comments__c);

    }
}