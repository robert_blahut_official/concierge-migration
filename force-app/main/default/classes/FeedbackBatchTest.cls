@isTest
private class FeedbackBatchTest {
    @testSetup
    static void initData() {
        List<SchedulerSettings__c> schedSettingList = new List<SchedulerSettings__c>{
                new SchedulerSettings__c(
                        Name = TurboEncabulatorScheduler.FEED_BATCH_NAME,
                        Class_Name__c = TurboEncabulatorScheduler.FEED_CLASS_NAME,
                        CRON__c = TurboEncabulatorScheduler.FEED_CRON,
                        Batch_Size__c = TurboEncabulatorScheduler.FEED_BATH_SIZE,
                        Description__c = TurboEncabulatorScheduler.FEED_DESCRIPTION,
                        Active__c = true
                )
        };
        Database.insert(schedSettingList, false);
        System.assert(!SchedulerSettings__c.getAll().isEmpty());
    }

    @isTest static void searchStagingShedulerTest() {
        String cronExpr = '0 0 23 * * ?';
        Test.startTest();
        String jobId = TurboEncabulatorScheduler.startScheduler(cronExpr, 'Test TurboEncabulator');
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        Test.stopTest();
        System.assertEquals(cronExpr, ct.CronExpression);
    }

    @isTest static void searchStagingParseTest() {
        Search_Staging__c searchSettingRecord = new Search_Staging__c();
        searchSettingRecord.Biederman__c = JSON.serialize(new Article_Feedback__c(
                Article_ID__c = 'sampleId',
                Article_Title__c = 'sampleTitle',
                Comments__c = 'itIsOk',
                Feedback_Reason__c = 'Other'
        ));
        searchSettingRecord.RecordTypeId = FeedbackBatch.FEEDBACK_RECORD_TYPE_ID;
        WorkWithArticles.encryptedSearchStaging(searchSettingRecord);
        ESAPI.securityUtils().validatedInsert(new List<Search_Staging__c>{searchSettingRecord});

        Test.startTest();
        FeedbackBatch batchable = new FeedbackBatch();
        batchable.setInitParams(new List<Object>{null, Datetime.now(), 'FeedbackBatch', 12});
        Database.executeBatch(batchable, 50);
        Test.stopTest();
        Integer feedbackRecords = [SELECT COUNT() FROM Article_Feedback__c];
        System.assertEquals(1, feedbackRecords);
        Integer stagingRecords = [SELECT COUNT() FROM Search_Staging__c];
        System.assertEquals(0, stagingRecords);
    }

    @isTest static void searchStagingWrongTest() {
        Search_Staging__c searchSettingRecord = new Search_Staging__c();
        searchSettingRecord.Biederman__c = JSON.serialize(new Article_Feedback__c(
                Article_ID__c = 'sampleId',
                Article_Title__c = 'sampleTitle',
                Comments__c = 'itIsOk',
                Feedback_Reason__c = 'Other'
        ));
        searchSettingRecord.RecordTypeId = FeedbackBatch.FEEDBACK_RECORD_TYPE_ID;
        WorkWithArticles.encryptedSearchStaging(searchSettingRecord);
        ESAPI.securityUtils().validatedInsert(new List<Search_Staging__c>{searchSettingRecord});
        Test.setCreatedDate((Id) searchSettingRecord.Id, Date.today().addDays(-65));
        cncrg__Search_Staging__c searchStagingRecord = [	SELECT	Id,
                cncrg__Profigliano__c
        FROM	cncrg__Search_Staging__c
        LIMIT	1
        ];
        searchStagingRecord.Profigliano__c += 'wrong data';
        update searchStagingRecord;

        Test.startTest();
        FeedbackBatch batchable = new FeedbackBatch();
        batchable.setInitParams(new List<Object>{null, Datetime.now(), 'FeedbackBatch', 12});
        Database.executeBatch(batchable, 50);
        Test.stopTest();

        System.assert([SELECT Id, cncrg__Down__c FROM cncrg__Search_Staging__c][0].cncrg__Down__c != NULL);
    }
}