/**
* @description
	This class contains logic for search the Knowledge articles

* @methods
	getSuggestionsForQuery -			method for getting suggestions
	getFavoritesbyId -					method for getting favorites records from Lightning Knowledges by Ids
	rateItem -							method for upvoting or downvoting records (create/delete Vote records)
	toggleFavoriteAPEX -				method for adding or deleting Favorite articles
*/
public abstract class AbstractSearchEngine {
	public static Boolean isTest = false;

	public static final String ENGINE_CONFIG_SETTING_NAME = 'Search Engine Configurations';
	public static final String FEDERATED_SEARCH_TYPE = 'Federated Search';
	public static final String SALESFORCE_SEARCH_TYPE = 'Salesforce';
	public static final String POSTGRESS_SEARCH_TYPE = 'Postgres Adapter';

	public static final String UPDATED_TIME_RANGE_SETTING_NAME = 'Hours New/Updated Flag is Displayed';
	public static final String CHANNEL_FILTER_SETTING_NAME = 'Channel Type For Search';

	public static final String SNIPPET_LENGTH_SETTING_NAME = 'Snippet Length';
	public static final String PASS_SEARCH_TERM_TO_SUGGESTIONS = 'Pass Search Term to Suggestions';
	public static final Map<String, String> CHANNELS_MAP = new Map<String, String> {
		'Internal App' => 'IsVisibleInApp',
		'Customer' => 'IsVisibleInCsp',
		'Partner' => 'IsVisibleInPrm',
		'Knowledge Base' => 'IsVisibleInPkb'
	};

	protected String ONLINE_PUBLISH_STATUS = 'Online';
	protected String KNOWLEDGE_OBJECT_NAME = 'KnowledgeArticleVersion';

	protected Boolean isObjectSearch = false;

	public virtual String getArticleType(sObject article) { return ''; }
	public virtual Set<String> getRequiredFieldsForSearch() { return new Set<String>(); }
	public virtual List<Object> getFavoritesbyId(List<String> recordIds, String prefix) { return new List<Object>(); }

	public virtual List<SearchPhraseWrapper> getSuggestionsForQuery(String searchTerm) {
		searchTerm = searchTerm.toLowerCase().trim();
		List<SearchPhraseWrapper> result = new List<SearchPhraseWrapper>();

		if (searchTerm.length() > 2 || Utils.containsDoubleByteCharacters(searchTerm)) {
			Integer MAX_RESULTS = Integer.valueOf(cncrg__Search_Engine_Configurations__c.getInstance(ENGINE_CONFIG_SETTING_NAME).cncrg__Max_Suggestions__c);

			List<SearchPhraseWrapper> oldSearchTerms = this.getUserQueries(MAX_RESULTS, searchTerm);
			Set<String> searchPhrases = new Set<String>();
			for (SearchPhraseWrapper oldQuery :oldSearchTerms) {
				searchPhrases.add(oldQuery.Phrase);
			}

			List<SearchPhraseWrapper> allSearchTerms = this.getSearchSuggestionResult(searchTerm, searchPhrases, MAX_RESULTS);
			allSearchTerms.addAll(oldSearchTerms);
			allSearchTerms.sort();

			Integer resultsToReturn = Math.min(allSearchTerms.size(), MAX_RESULTS);
			if (!allSearchTerms.isEmpty()) {
				for (Integer i = 0; i < resultsToReturn; i++) {
					result.add(allSearchTerms[i]);
				}
			}
		}
		return result;
	}

	public virtual String rateItem(SearchResultsWrapper item) {
		List<Vote> listOfLikes = [
			SELECT Id, Type, ParentId, CreatedBy.Name, CreatedById
			FROM Vote
			WHERE ParentId = :item.knowledgeArticleId AND CreatedById = :UserInfo.getUserId()
		];

		Boolean insertFlag = false;
		if (listOfLikes.size() > 0) {

			if ((listOfLikes[0].Type == 'Up' && !item.isLike) || (listOfLikes[0].Type == 'Down' && item.isLike)) {
				insertFlag = true;
				listOfLikes = new List<Vote> {
					new Vote(
						Id = listOfLikes[0].Id,
						Type = (item.isLike ? 'Up' : 'Down')
					)
				};
			}
		} else {
			insertFlag = true;
			listOfLikes.add(
				new Vote(
					ParentId = item.knowledgeArticleId,
					Type = (item.isLike ? 'Up' : 'Down')
				)
			);
		}

		if (insertFlag && String.isNotBlank(item.knowledgeArticleId)) {
			ESAPI.securityUtils().validatedUpsert(listOfLikes);
		}
		if (item.isLike) {
			ArticleReviewUtils.updateLikedORFavoritedArticle(item, UserInfo.getLanguage());
		}
		return '';
	}

	public virtual String toggleFavoriteAPEX(String recordId, Boolean isFavorite) {
		List<String> fieldsToCheck = new List<String> { 'cncrg__ArticleId__c', 'OwnerId' };
		List<cncrg__Favorite_article__c> listOfFavorite = new List<cncrg__Favorite_article__c>();
		WorkWithArticles.EncryptionMember CRYPTO_ENGINE = new WorkWithArticles.EncryptionMember();

		if (ESAPI.securityUtils().isAuthorizedToView('cncrg__Favorite_article__c', fieldsToCheck)) {
			if (isFavorite) {
				listOfFavorite = [
					SELECT Id, cncrg__ArticleId__c, OwnerId
					FROM cncrg__Favorite_article__c
					WHERE OwnerId = :UserInfo.getUserId()
					LIMIT 50000
				];

				List<cncrg__Favorite_article__c> listOfFavoriteDelete = new List<cncrg__Favorite_article__c> ();
				for (cncrg__Favorite_article__c item : listOfFavorite) {
					if (CRYPTO_ENGINE.decrypteData(item.cncrg__ArticleId__c) == recordId) {
						listOfFavoriteDelete.add(item);
						break;
					}
				}
				if (listOfFavoriteDelete.size() > 0) {
					ESAPI.securityUtils().validatedDelete(listOfFavoriteDelete);
				}

			} else {
				ArticleReviewUtils.updateLikedORFavoritedArticle(recordId, UserInfo.getLanguage());
				if (!Utils.checkNumberOfFaivoritArticles()) {
					throw new AuraHandledException(Label.Max_Number_of_Favorites);
				}

				listOfFavorite = new List<cncrg__Favorite_article__c> {
					new cncrg__Favorite_article__c(
						cncrg__ArticleId__c = CRYPTO_ENGINE.encrypteData(recordId),
						OwnerId = UserInfo.getUserId()
					)
				};
				ESAPI.securityUtils().validatedInsert(listOfFavorite);
			}
		}
		return null;
	}

	public virtual SearchResultsWrapper getAllArticleSettings(SearchResultsWrapper article, String prefix) {
		Map<String, Set<String>> mapOfCategories = new Map<String, Set<String>>();
		if (article.typeName == 'article') {
			mapOfCategories = KnowledgeDataCategoryService.getMapOfDataCategories(
				new Set<String> { article.recordId },
				getKnowledgeObjectApiNames( new List<SearchResultsWrapper>{article} )
			);
		}

		Set<String> allCategories = new Set<String>();
		for (Set<String> tempSet : mapOfCategories.values()) {
			allCategories.addAll(tempSet);
		}

		String separator = ';';
		article.dataCategories = String.join((Iterable<String>)allCategories, separator);

		this.getArticleVotes(article);
		CustomerExtensionBase uniqueClassMember = Utils.getNewBaseUniqueToolsInstance();
		article = (SearchResultsWrapper) JSON.deserialize(uniqueClassMember.populateArticlePreview(JSON.serialize(new List<String> { JSON.serialize(article), prefix })), SearchResultsWrapper.class);

		return SearchQueryInputController.getPrioritizedSettingRecord(article, allCategories);
	}

	public virtual FavoriteArticlesWrapper getFavoriteArticleDetail(FavoriteArticlesWrapper favorite, String prefix) {
		return favorite;
	}

	public virtual SearchResultsWrapper getArticleDetail(SearchResultsWrapper article, String prefix) {
		Map<String, Utils.sObjectWrapper> mapOfTypesSettings = Utils.prepareTypesSettings(
			Utils.getSearchTypesSettingsByArticleAPIName(new Set<String> { article.type }
		), isObjectSearch);

		List<String> fieldsToDisplay = new List<String>();
		if (mapOfTypesSettings.containsKey(article.type)) {
			fieldsToDisplay = mapOfTypesSettings.get(article.type).availableFieldsList;
			article.showRelatedAttachments = mapOfTypesSettings.get(article.type).showAttachments;
		}

		Set<String> skippedFields = Utils.splitUnionFields(
			new Map<String, Utils.sObjectWrapper> {
				article.type => new Utils.sObjectWrapper('Title', fieldsToDisplay, false)
			}
		);

		Set<String> fieldsToSelect = new Set<String>();
		fieldsToSelect.addAll(fieldsToDisplay);
		fieldsToSelect.addAll(skippedFields);
		fieldsToDisplay = new List<String>(fieldsToSelect);
        fieldsToSelect.add('Id');

		String recordId = article.recordId;
		List<String> articleTypes = new List<String>(getKnowledgeObjectApiNames(new List<SearchResultsWrapper>{article} ));

		String query = 'SELECT ' + String.join((Iterable<String>)fieldsToSelect, ',');
		query += ' FROM ' + articleTypes[0];
		query += ' WHERE Id =: recordId ';
		query += (isObjectSearch ? '' : ' UPDATE VIEWSTAT');

		sObject articleBase = Database.query(query);
		return Utils.getArticleFieldsHTML(article, prefix, articleBase, fieldsToDisplay, skippedFields);
	}

	protected String generateFavoriteBody(sObject article, String prefix, Utils.sObjectWrapper articleConfig) {
		String configurationType = this.getArticleType(article);
		String typeName = Utils.isLightningKnowledgeEnabled() ? Utils.getLightningKnowledgeObjectName() : configurationType;

		Map<String, Utils.sObjectWrapper> mapOfTypesSettings =  new Map<String, Utils.sObjectWrapper>();
		mapOfTypesSettings.put(configurationType, articleConfig);
		Set<String> hidenSystemFields = Utils.splitUnionFields(mapOfTypesSettings);

		Map<String, Schema.SObjectField> fieldMap = article.Id.getSObjectType().getDescribe().fields.getMap();
		String body = Utils.buildArticleDetail(articleConfig.availableFieldsList, article, fieldMap, typeName, hidenSystemFields, prefix);

		Set<String> allInteractiveFields = new Set<String>();
		if (body.contains('{!User.')) {
			allInteractiveFields.addAll(Utils.getAllInteractiveFields(body));
		}

		if (body.contains('{!Document.CompanyLogo}')) {
			String companyLogo = Utils.getCompanyLogoName(prefix);
			companyLogo = String.isNotBlank(companyLogo) ? companyLogo :'';
			body = body.replaceAll('\\{!Document.CompanyLogo}', companyLogo);
		}

		if (!allInteractiveFields.isEmpty()) {
			User theUser = Utils.getAllUserInfo(allInteractiveFields);
			body = Utils.updateArticleBody(
				body,
				theUser,
				allInteractiveFields,
				prefix
			);
		}
		return body;
	}

	public virtual SearchResultsWrapper getArticleVotes(SearchResultsWrapper article) {
		if (ESAPI.securityUtils().isAuthorizedToView('Vote', new List<String> { 'Type', 'LastModifiedDate' })) {
			article.rating = [
				SELECT count()
				FROM Vote
				WHERE ParentId = :article.knowledgeArticleId AND Type = 'Up'
			];
			List<Vote> votesList = [
				SELECT Type, LastModifiedDate
				FROM Vote
				WHERE Type IN('Up', 'Down')
					AND ParentId = :article.knowledgeArticleId
					AND CreatedById = :UserInfo.getUserId()
			];
			if (votesList != null && votesList.size() > 0) {
				for (Vote item : votesList) {
					article.isLike = (item.Type == 'Up');
					article.voteLastModifiedDate = item.LastModifiedDate.format();
				}
				article.isDisabled = true;
			}
		}
		return article;
	}

	@TestVisible
	private List<SearchPhraseWrapper> getUserQueries(Integer MAX_RESULTS, String searchTerm) {
		List<SearchPhraseWrapper> oldSearchTerms = new List<SearchPhraseWrapper>();
		if (Utils.getValueFromConciergeSettings(PASS_SEARCH_TERM_TO_SUGGESTIONS) == 'false') {
			return oldSearchTerms;
		}

		List<cncrg__Search_Term__c> listOfUserQueries = new List<cncrg__Search_Term__c> ();
		if (ESAPI.securityUtils().isAuthorizedToView('cncrg__Search_Term__c', new List<String> {
			'cncrg__Number_of_Search_History_Records__c',
			'Name'
		})) {

			String query = 'SELECT Id, cncrg__Number_of_Search_History_Records__c, Name ';
				query += ' FROM cncrg__Search_Term__c ';
				query += ' WHERE LastModifiedDate >= ' + DateTime.now().addMonths(- 1).format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
				query +=' AND ';
			if (MAX_RESULTS != null) {
				query += ' Name LIKE \'' + String.escapeSingleQuotes(searchTerm) + '%\' ';
			}
			else {
				query += ' Name = \'' + String.escapeSingleQuotes(searchTerm) + '\' ';
			}
			query += ' ORDER BY cncrg__Number_of_Search_History_Records__c DESC, Name DESC';
			if (MAX_RESULTS != null) {
				query += ' LIMIT ' + MAX_RESULTS;
			}
			listOfUserQueries = Database.query(query);
		}

		Integer max_Old = 1;
		Set<String> searchPhrases = new Set<String>();
		for (cncrg__Search_Term__c oldQuery : listOfUserQueries) {
			if (!searchPhrases.contains(oldQuery.Name)) {
				Integer rating = Integer.valueOf(oldQuery.cncrg__Number_of_Search_History_Records__c);
				oldSearchTerms.add( new SearchPhraseWrapper(oldQuery.Name, rating + 1) );
				searchPhrases.add(oldQuery.Name);
				max_Old = Math.max(max_Old, rating);
			}
		}
		return oldSearchTerms;
	}

	public virtual List<SearchPhraseWrapper> getSearchSuggestionResult(String searchTerm, Set<String> oldSearchTerms, Integer MAX_RESULTS) {
		Search.KnowledgeSuggestionFilter filters = new Search.KnowledgeSuggestionFilter();
		filters.setLanguage(Utils.getUserLanguage());
		filters.setPublishStatus(ONLINE_PUBLISH_STATUS);
		//String channelFilter = Utils.getValueFromConciergeSettings(CHANNEL_FILTER_SETTING_NAME)
		//filters.setChannel(CHANNELS_MAP.get(channelFilter));
		Search.SuggestionOption options = new Search.SuggestionOption();
		options.setFilter(filters);
		options.setLimit(MAX_RESULTS);

		List<SearchPhraseWrapper> suggestions = new List<SearchPhraseWrapper>();
		Search.SuggestionResults suggestionResults = Search.suggest(searchTerm, KNOWLEDGE_OBJECT_NAME, options);
		for (Search.SuggestionResult searchResult: suggestionResults.getSuggestionResults()) {
			String articleTitle = ((String) searchResult.getSObject().get('Title')).toLowerCase();
			articleTitle = searchTerm + articleTitle.subStringAfter(searchTerm);

			if (!articleTitle.contains(searchTerm) || oldSearchTerms.contains(articleTitle)) {
				continue;
			}

			oldSearchTerms.add(articleTitle);
			suggestions.add(new SearchPhraseWrapper(articleTitle, 1));
		}
		return suggestions;
	}

	public Integer getUpdateIndicatorTime() {
		Integer indicatorLiveTime = 0;
		String settingsValue = Utils.getValueFromConciergeSettings(UPDATED_TIME_RANGE_SETTING_NAME);
		if (settingsValue != null) {
			indicatorLiveTime = Integer.valueOf(settingsValue);
		}
		return indicatorLiveTime;
	}

	@TestVisible
	protected String getUpdateIndicator(sObject article, Integer indicatorLiveTime) {
		Datetime firstPublished = Datetime.valueOf(article.get('FirstPublishedDate'));
		Datetime lastPublished = Datetime.valueOf(article.get('LastPublishedDate'));
		return this.getUpdateIndicator(firstPublished, lastPublished, indicatorLiveTime);
	}

	protected String getUpdateIndicator(DateTime firstPublished, DateTime lastPublished, Integer indicatorLiveTime) {
		String updateIndicator = '';
		if (firstPublished != null && lastPublished != null) {
			if (lastPublished.date() == firstPublished.date() && lastPublished.addHours(indicatorLiveTime) > Datetime.now()) {
				updateIndicator = 'new';
			} else if (lastPublished.date() != firstPublished.date() && lastPublished.addHours(indicatorLiveTime) > Datetime.now()) {
				updateIndicator = 'updated';
			}
		}
		return updateIndicator;	
	}

	public virtual Set<String> getKnowledgeObjectApiNames(List<SearchResultsWrapper> articles) {
		Set<String> types = new Set<String>();
		for (SearchResultsWrapper article: articles) {
			if (String.isNotBlank(article.type)) {
				types.add(article.type);
			}
		}
		return types;
	}

	public virtual SearchResultsWrapper getInitialSearchResultWrapper(sObject article, String snippet, Integer indicatorLiveTime) {
		String articleType = getArticleType(article);
		String articleBody = 'blankValue';
		String summary = (String) article.get('Summary');
		if (String.isNotBlank(summary) && summary.containsIgnoreCase('{!ltngext.')) {
			articleBody = summary;
		}

		snippet = !Utils.isCustomSnippetNeeded() ? Utils.addSnippetElipsis(snippet, false) : '';
		Integer age = (Date.valueOf(article.get('LastModifiedDate'))).daysBetween(Date.today());
		age = age > 365 ? 365 : age;

		String knowledgeArticleId = (String) article.get('KnowledgeArticleId');
		String updateIndicator = this.getUpdateIndicator(article, indicatorLiveTime);
		String masterVersionId = (String) (Utils.isArticlesHaveMasterVersionIdField() ? article.get('MasterVersionId') : article.get('Id'));

		return new SearchResultsWrapper(
			(String) article.get('Title'),
			snippet,
			(String) article.get('Id'),
			articleBody,
			articleType,
			age,
			knowledgeArticleId,
			masterVersionId,
			updateIndicator,
			(String) article.get('OwnerId')
		);
	}
}