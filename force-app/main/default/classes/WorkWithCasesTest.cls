@IsTest
private class WorkWithCasesTest {

	@testSetup static void setup() {
		cncrg__Concierge_Settings__c cs = new cncrg__Concierge_Settings__c(
			Name = 'Case Origin',
			cncrg__Value__c = 'test'
		);
		cncrg__Concierge_Settings__c  applicationNameSetting = new cncrg__Concierge_Settings__c(
			Name = 'Application Name',
			cncrg__Value__c = 'Concierge'
		);
		cncrg__Concierge_Settings__c caseCloseVisibility = new cncrg__Concierge_Settings__c(
			Name = CloseCasePopupController.CLOSE_CASE_VISIBILITY_SETTING_NAME,
			cncrg__Value__c = 'true'
		);

		insert new List<cncrg__Concierge_Settings__c> {cs, applicationNameSetting, caseCloseVisibility};
		System.assertEquals(3, cncrg__Concierge_Settings__c.getAll().size());
	}

	@IsTest
	public static void testCloseCase() {
		Case theCase = TestClassUtility.generateCase('Subject', 'Description', 'New', 'Web');
		insert theCase;

		Test.startTest();
		List<Object> theFieldsData = CloseCasePopupController.getInitData(theCase.Id);
		System.assert(!theFieldsData.isEmpty());

		Id caseId = CloseCasePopupController.saveSObject(JSON.serialize(theCase));
		System.assert(caseId != null);
		Test.stopTest();
	}

	@IsTest
	public static void testWorkWithCases() {
		Test.startTest();
		List<Object> ls = WorkWithCases.loadEmptyCase(
			'',
			'',
			new cncrg__Case_Layout_Assignment__c(cncrg__Field_Set__c = WorkWithCases.DEFAULT_CASE_CREATE_FEILD_SET),
			'',
			false
		);
		CaseWrapper cWrapper = (CaseWrapper)ls[0];
		cWrapper.newCategory = 'test';
		cWrapper.emailReciever = 'test@test.com';
		SObject c = cWrapper.newCase;
		c.put('Subject',  'test');
		c.put('Description', 'test');
		c.put('Status', 'New');
		c.put('Origin', 'Web');

		WorkWithCases.IsTest = true;

		String caseId = WorkWithCases.createNewCaseApex(JSON.serialize(c), null)[0];

		WorkWithCases.CaseListWrapper tempCase = WorkWithCases.getCaseById(caseId);
		System.assert(caseId == tempCase.c.Id);

		Attachment a = new Attachment(
			Name = 'Test attachment',
			ParentId = caseId,
			Body = Blob.valueOf('test')
		);
		insert a;

		System.assert(WorkWithCases.loadAllCaseAttachments(caseId).size() == 1);


		String fileId = WorkWithCases.saveTheChunk(tempCase.c.Id, 'test name', EncodingUtil.base64Encode(Blob.valueOf('test')), 'test', '', true);
		System.assert(fileId == WorkWithCases.saveTheChunk(tempCase.c.Id, 'test name', EncodingUtil.base64Encode(Blob.valueOf('test')), 'test', fileId, true));

		fileId = WorkWithCases.saveTheChunk(null, 'test name', EncodingUtil.base64Encode(Blob.valueOf('test')), 'test', '', false);
		System.assert(fileId == WorkWithCases.saveTheChunk(null, 'test name', EncodingUtil.base64Encode(Blob.valueOf('test')), 'test', fileId, false));
		//System.assert(WorkWithCases.getMoreCases(null, 5).size() == 1);
		WorkWithCases.addNewFeedItem(tempCase.c.Id, 'test message', fileId);
		WorkWithCases.addNewFeedItem(tempCase.c.Id, '', fileId);
		WorkWithCases.addNewFeedItem(tempCase.c.Id, null, fileId);
		WorkWithCases.addNewFeedItem(tempCase.c.Id, fileId);

		setCustomCaseConfiguration();
		ls = WorkWithCases.loadEmptyCase('','',null,'',true);
		cWrapper = (CaseWrapper)ls[0];
		System.assertEquals(true, WorkWithCases.isUserHasAccessToCase());
		System.assertNotEquals(0, WorkWithCases.loadCaseTypesInfo().size());
		Test.stopTest();
	}

	@IsTest
	public static void testLoadEmptyCase() {
		WorkWithCases.IsTest = true;
		TestClassUtility.createCaseTypesConfig();

		Test.startTest();
		sObject dataCategoryObject = WorkWithActionsSettingsTest.getDataCategorySelectionRecord();
		CaseWrapper result;
		if (dataCategoryObject != null) {
			String dataCategoryName = WorkWithActionsSettingsTest.getDataCategoryObjectName();
			result = (CaseWrapper)WorkWithCases.loadEmptyCase(
				'ka0460000000P3jAAE',
				dataCategoryName,
				new cncrg__Case_Layout_Assignment__c(cncrg__Field_Set__c = WorkWithCases.DEFAULT_CASE_CREATE_FEILD_SET),
				'ka0460000000P3jAAE',
				false
			)[0];
			System.assert(result != null);
		} else {
			System.assert(result == null);
		}
		Test.stopTest();
	}

	@IsTest
	public static void testGetAllCases() {
		List<WorkWithCases.CaseListWrapper> result = WorkWithCases.getMoreCases(5);
		System.assert(result.isEmpty());
	}

	@IsTest
	public static void testGetCaseWrapper() {
		List<WorkWithCases.FieldWrapper> lw = new List<WorkWithCases.FieldWrapper>();
		TestClassUtility.createCaseTypesConfig();

		CaseWrapper cWrapper = (CaseWrapper)WorkWithCases.loadEmptyCase(
			'',
			'',
			new cncrg__Case_Layout_Assignment__c(cncrg__Field_Set__c = 'cncrg__Base_Create_Case'),
			'',
			false
		)[0];

		SObject c = cWrapper.newCase;
		c.put('Subject', 'test');
		c.put('Description', 'test');
		c.put('Status', 'New');
		c.put('Origin', 'Web');
		String caseId = WorkWithCases.createNewCaseApex(JSON.serialize(c), null)[0];
		WorkWithCases.CaseListWrapper tempCase = WorkWithCases.getCaseById(caseId);
		lw = WorkWithCases.getCaseWrapper(String.valueOf(tempCase.c.Id));
		System.assert(!lw.isEmpty());
	}

	@IsTest
	public static void testgetNotificationCircleTabNumber() {
		Integer offset = WorkWithCases.getNotificationCircleTabNumber();
		System.assertNotEquals(null, offset);
	}

	@IsTest
	public static void coverWorkWithCasesSimpleMethods() {
		cncrg__Concierge_Settings__c intervalSetting = new cncrg__Concierge_Settings__c(
			Name = 'Update Interval',
			cncrg__Value__c = '500'
		);

		cncrg__Concierge_Settings__c casesNumberSetting = new cncrg__Concierge_Settings__c(
			Name = 'Number of Cases Displayed',
			cncrg__Value__c = '5'
		);

		cncrg__Concierge_Settings__c casesPerChunckSetting = new cncrg__Concierge_Settings__c(
			Name = 'Cases per Chunk',
			cncrg__Value__c = '5'
		);
		cncrg__Concierge_Settings__c logTickets = new cncrg__Concierge_Settings__c(
			Name = 'Log Cases From My Tickets Tab',
			cncrg__Value__c = 'true'
		);
		ESAPI.securityUtils().validatedInsert(
			new List<cncrg__Concierge_Settings__c> {
				intervalSetting,
				casesNumberSetting,
				casesPerChunckSetting,
				logTickets
		});

		Contact c = new Contact(cncrg__UserId__c = 'testId', LastName = 'test');
		ESAPI.securityUtils().validatedInsert(new List<Contact> {c});

		WorkWithCases.getConciergeSetting();
		WorkWithCases.loadCasesSettings();
		WorkWithCases.getCaseUpdatesCount('contactId');
		WorkWithCases.getLogTicketComponentName();

		WorkWithCases.CaseTypesInfoWrapper testWrapper = new WorkWithCases.CaseTypesInfoWrapper('test', 'test');
		System.assertEquals(3, Limits.getDmlStatements());
	}

	@IsTest
	private static void populateContactIdFieldTest() {
		insert new cncrg__Concierge_Settings__c(Name = 'Populate Contact ID on Case', cncrg__Value__c = 'true');
		System.assertEquals([SELECT Id FROM Case].size(), 0);
		Case theCase = TestClassUtility.generateCase('Subject', 'Description', 'New', 'Web');
		Test.startTest();
		List<String> createdCaseId = WorkWithCases.createNewCaseApex(JSON.serialize(theCase), '');
		Test.stopTest();
		Case createdCase = [SELECT ContactId FROM Case WHERE Id = : createdCaseId];
		System.assertNotEquals(createdCase.ContactId, null);
	}

	@IsTest
	private static void getCustomCaseSettingsTest() {
		Test.startTest();
		CustomerExtensionBase.customCaseSettings = new Map<String, String>();
		Map<String, String> customCaseSettings = WorkWithCases.getCustomCaseSettings();
		Test.stopTest();
		System.assertEquals(customCaseSettings.isEmpty(), true);
	}

	@IsTest
	private static void getMoreCasesTest() {
		setCustomCaseConfiguration();

		insert new cncrg__Concierge_Settings__c(
			Name = 'Populate Contact ID on Case',
			cncrg__Value__c = 'true'
		);
		insert new cncrg__Concierge_Settings__c(
			Name = 'Name of Case Status Indicator Field',
			cncrg__Value__c = 'cncrg__Status_Indicator__c'
		);

		Case case1 = TestClassUtility.generateCase('Subject', 'Description', 'New', 'Web');
		Case case2 = TestClassUtility.generateCase('Subject', 'Description', 'New', 'Web');
		Case case3 = TestClassUtility.generateCase('Subject', 'Description', 'New', 'Web');
		WorkWithCases.createNewCaseApex(JSON.serialize(case1), '');
		WorkWithCases.createNewCaseApex(JSON.serialize(case2), '');
		WorkWithCases.createNewCaseApex(JSON.serialize(case3), '');
		List<Case> createdCases = [SELECT Id, ContactId FROM Case];
		System.assertEquals(3, createdCases.size());
		System.assertNotEquals(null, createdCases[0].ContactId);

		Test.startTest();
		List<WorkWithCases.CaseListWrapper> wrapper = WorkWithCases.getMoreCases(2);
		Test.stopTest();
		System.assertEquals(2, wrapper.size());
	}

	@IsTest
	private static void markUpdatesAsReadTest() {
		setCustomCaseConfiguration();

		insert new Contact(Firstname = 'test',Lastname = 'Test');
		insert new cncrg__Concierge_Settings__c(Name = 'Populate Contact ID on Case', cncrg__Value__c = 'true');
		Case case1 = TestClassUtility.generateCase('Subject', 'Description', 'New', 'Web');
		case1.cncrg__Unread_Update_for_Contact__c = true;
		WorkWithCases.createNewCaseApex(JSON.serialize(case1), '');
		System.assertEquals([SELECT Id FROM Case WHERE cncrg__Unread_Update_for_Contact__c = : true].size(), 1);
		List<WorkWithCases.CaseListWrapper> wrapper = WorkWithCases.getMoreCases(1);
		Test.startTest();
		WorkWithCases.markUpdatesAsRead(JSON.serialize(wrapper[0]));
		Test.stopTest();
		System.assertEquals([SELECT Id FROM Case WHERE cncrg__Unread_Update_for_Contact__c = : false].size(), 1);
		System.assert(!Utils.containsDoubleByteCharacters(Utils.removeEndSpecCharacters('test')));
		System.assertNotEquals(0, LookupFieldController.fetchLookUpValues('test', 'Contact').size());
	}

	@IsTest
	private static void caseWrepperListSortingTest() {
		Contact testContact = new Contact(
			Firstname = 'Test',
			Lastname = 'Test'
		);
		insert testContact;

		List<Case> testCases = new List<Case>();
		for (Integer i = 0; i < 5; i++) {
			testCases.add(
				new Case(
					Status = 'New',
					Origin = 'Email',
					ContactId = testContact.Id
				)
			);
		}
		insert testCases;
		testCases.get(1).Status = 'Closed';
		testCases.get(2).Status = 'Closed';
		update testCases;

		// for testing purposes closed cases goes first in the list 
		testCases = [SELECT Id, IsClosed, Status, CreatedDate FROM Case ORDER BY IsClosed DESC];

		Test.startTest();
		List<WorkWithCases.CaseListWrapper> wrappers = new List<WorkWithCases.CaseListWrapper>();
		for (Case theCase: testCases) {
			wrappers.add( generateTestWrapper(theCase, 'Case') );
		}
		wrappers.sort();

		System.assertNotEquals(testCases.get(0).IsClosed, (Boolean)wrappers.get(0).c.get('IsClosed'));
		System.assert( (Boolean)wrappers.get(3).c.get('IsClosed') );
		System.assert( (Boolean)wrappers.get(4).c.get('IsClosed') );

		// the following code for testing the Case sort with Custom objects

		//CustomerExtensionBase.customCaseSettings = new Map<String, String> {
		//	'ObjectName' => 'Account',
		//	'SortOrder' => JSON.serialize(new List<String>{'cncrg__IsClosed__c', 'CreatedDate'})
		//};
		// for testing purposes. removed because of custom field 
		//List<Account> accounts = new List<Account>();
		//for (Integer i = 0; i < 5; i++) {
		//	accounts.add(new Account(Name ='Test ' + i, cncrg__IsClosed__c = false));
		//}
		//insert accounts;
		//accounts.get(0).cncrg__IsClosed__c = true;
		//accounts.get(1).cncrg__IsClosed__c = true;
		//update accounts;
		//accounts = [SELECT Id, cncrg__IsClosed__c, CreatedDate FROM Account ORDER BY cncrg__IsClosed__c DESC];
		//for (Account acc: accounts) {
		//	wrappers.add( generateTestWrapper(acc, 'Account') );
		//}
		//wrappers.sort();
		//for (WorkWithCases.CaseListWrapper wr: wrappers) {
		//	System.debug('#' + wr.c );
		//}
		Test.stopTest();
	}

	private static WorkWithCases.CaseListWrapper generateTestWrapper(sObject theCase, String objectName) {
		WorkWithCases.CaseListWrapper wrap = new WorkWithCases.CaseListWrapper(theCase, null, null);
		wrap.populateFields(new List<Schema.FieldSetMember>(), null, null, objectName);
		return wrap;
	}

	private static void setCustomCaseConfiguration() {
		CustomerExtensionBase.customCaseSettings = new Map<String, String> {
			'ObjectName' => 'Case',
			'ListFieldSetName' => WorkWithCases.DEFAULT_CASE_CREATE_FEILD_SET,
			'DetailFieldSetName' => WorkWithCases.DEFAULT_CASE_CREATE_FEILD_SET,
			'CreateFieldSetName' => WorkWithCases.DEFAULT_CASE_CREATE_FEILD_SET,
			'Status' => 'Status',
			'Subject' => 'Subject',
			'Status Indicator' => 'cncrg__Status_Indicator__c',
			'Description' => 'cncrg__Comments__c'
		};
	}
}