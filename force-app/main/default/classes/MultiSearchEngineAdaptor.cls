public with sharing class MultiSearchEngineAdaptor extends AbstractSearchEngine implements SearchEngineInterface {

	private AbstractSearchEngine articlesEngine;
	private ObjectSearchEngine customEngine;

	public MultiSearchEngineAdaptor() {
		this.articlesEngine = Utils.isLightningKnowledgeEnabled()
			? (AbstractSearchEngine)new StandAloneLightningEditionController()
			: (AbstractSearchEngine)new StandAloneEditionController();
		this.customEngine = new ObjectSearchEngine();
	}

	public override List<Object> getFavoritesbyId(List<String> recordIds, String prefix) {
		List<FavoriteArticlesWrapper> results = new List<FavoriteArticlesWrapper>();
		if (recordIds.isEmpty()) {
			return results;
		}

		List<HerokuWebService.HerokuArticle> searchResults = HerokuWebService.getArticles(recordIds);
		Map<String, FavoriteArticlesWrapper> salesforceArticles = this.getSalesforceFavoriteResults(searchResults);

		Schema.SObjectType anyArticleRecordType = Utils.getAllKnowledgeObjects().values().get(0).getSobjectType();

		for (HerokuWebService.HerokuArticle docItem : searchResults) {
			FavoriteArticlesWrapper favorite;

			if (docItem.storage == HerokuWebService.StorageType.salesforce.name() && salesforceArticles.containsKey(docItem.knowledgeArticleId)) {
				favorite = salesforceArticles.get(docItem.knowledgeArticleId);
			}
			else if (anyArticleRecordType != null) {
				String articleBody = this.buildArticleDetail(docItem);
				SObject temp = anyArticleRecordType.newSObject();
				temp.put('Title', String.valueOf(docItem.fields.get('Title')));
				temp.put('Summary', String.valueOf(docItem.fields.get('Summary')));
				temp.put('Id', docItem.recordId);

				favorite = new FavoriteArticlesWrapper(
					temp,
					articleBody,
					String.valueOf(docItem.fields.get('Title'))
				);

				favorite.knowledgeArticleId = docItem.knowledgeArticleId;
				favorite.storage = docItem.storage;
				favorite.Created = docItem.createdDate != null ? docItem.createdDate.format(FavoriteArticlesWrapper.DATE_FORMAT) : null;
				favorite.Modified = docItem.modifiedDate != null ? docItem.modifiedDate.format(FavoriteArticlesWrapper.DATE_FORMAT) : null;
			}

			if (favorite != null) {
				results.add(favorite);
			}
		}
		return results;
	}

	public override SearchResultsWrapper getAllArticleSettings(SearchResultsWrapper article, String prefix) {
		Set<String> allCategories = new Set<String>();
		if (article.categoryList != null) {
			for (SearchResultsWrapper.OptionWrapper category: article.categoryList) {
				allCategories.add(category.value);
			}
		}

		String separator = ';';
		article.dataCategories = String.join((Iterable<String>)allCategories, separator);

		if (article.storage == HerokuWebService.StorageType.salesforce.name()) {
			article = this.getArticleVotes(article);
		} else {
			article = this.customEngine.getArticleVotes(article);
		}

		CustomerExtensionBase uniqueClassMember = Utils.getNewBaseUniqueToolsInstance();
		article = (SearchResultsWrapper) JSON.deserialize(uniqueClassMember.populateArticlePreview(JSON.serialize(new List<String> { JSON.serialize(article), prefix })), SearchResultsWrapper.class);

		return SearchQueryInputController.getPrioritizedSettingRecord(article, allCategories);
	}

	public override List<SearchPhraseWrapper> getSearchSuggestionResult(String searchTerm, Set<String> oldSearchTerms, Integer maxResults) {
		List<SearchPhraseWrapper> suggestions = new List<SearchPhraseWrapper>();
		List<HerokuWebService.HerokuArticle> searchResults = HerokuWebService.getSearchResults(searchTerm.trim() + '*');

		for (HerokuWebService.HerokuArticle docItem: searchResults) {
			String articleTitle = String.valueOf(docItem.fields.get('Title')).toLowerCase();
			articleTitle = searchTerm + articleTitle.subStringAfter(searchTerm);

			if (!articleTitle.contains(searchTerm) || oldSearchTerms.contains(articleTitle)) {
				continue;
			}

			oldSearchTerms.add(articleTitle);
			suggestions.add(new SearchPhraseWrapper(articleTitle, 1));
		}
		return suggestions;
	}

	public List<SearchResultsWrapper> getSearchResults(String searchTerm, String prefix) {
		List<SearchResultsWrapper> results = new List<SearchResultsWrapper>();

		List<HerokuWebService.HerokuArticle> searchResults = new List<HerokuWebService.HerokuArticle>();
		if (searchTerm.trim().length() > 1 || Utils.containsDoubleByteCharacters(searchTerm)) {
			searchResults = HerokuWebService.getSearchResults(searchTerm.trim() + '*');
		}
		if (searchResults.isEmpty()) {
			return results;
		}

		Set<String> favoriteArticleIds = new Set<String>();
		if (ESAPI.securityUtils().isAuthorizedToView('cncrg__Favorite_article__c', new List<String> {'cncrg__ArticleId__c'})) {
			WorkWithArticles.EncryptionMember CRYPTO_SERVICE = new WorkWithArticles.EncryptionMember();
			for (Favorite_article__c item : [
				SELECT ArticleId__c
				FROM Favorite_article__c
				WHERE OwnerId = : UserInfo.getUserId()
				LIMIT 50000
			]) {
				favoriteArticleIds.add(CRYPTO_SERVICE.decrypteData(item.ArticleId__c));
			}
		}

		Integer indicatorLiveTime = this.getUpdateIndicatorTime();
		Map<String, SearchResultsWrapper> salesforceArticles = this.getSalesforceSearchResults(searchResults);

		for (HerokuWebService.HerokuArticle docItem : searchResults) {
			SearchResultsWrapper searchResult;

			List<SearchResultsWrapper.OptionWrapper> categories = new List<SearchResultsWrapper.OptionWrapper>();
			if (String.isNotBlank(docItem.category)) {
				List<String> categoryList = docItem.category.replace('(', '').replace(')', '').split(',');
				for (String categoryInfo: categoryList) {
					List<String> parts = categoryInfo.split('\\.');
					String categoryName = parts.get( parts.size() - 1 );
					SearchResultsWrapper.OptionWrapper currentOption = new SearchResultsWrapper.OptionWrapper();
					currentOption.label = categoryName.replace('_', ' ');
					currentOption.value = categoryName;
					categories.add(currentOption);
				}
			}

			if (docItem.storage == HerokuWebService.StorageType.salesforce.name()
					&& salesforceArticles.containsKey(docItem.knowledgeArticleId)) {
				searchResult = salesforceArticles.get(docItem.knowledgeArticleId);

			} else {
				String snippet = this.getSnippet((Map<String, Object>) docItem.fields.remove('highlight'));
				String articleBody = this.buildArticleDetail(docItem);

				searchResult = new SearchResultsWrapper(
					String.valueOf(docItem.fields.get('Title')),
					snippet,
					docItem.recordId,
					articleBody,
					docItem.objectType
				);

				searchResult.updateIndicator = this.getUpdateIndicator(docItem.createdDate, docItem.modifiedDate, indicatorLiveTime);
				searchResult.isKnowledgeObject = false;
				searchResult.type = docItem.objectType;
				searchResult.knowledgeArticleId = docItem.knowledgeArticleId;
				searchResult.storage = docItem.storage;
			}

			searchResult.isFavorite = favoriteArticleIds.contains(searchResult.recordId);
			searchResult.categoryList = categories;
			results.add(searchResult);
		}

		return results;
	}

	public override String rateItem(SearchResultsWrapper item) {
		String response = '';
		if (item.storage == HerokuWebService.StorageType.salesforce.name()) {
			response = this.articlesEngine.rateItem(item);
		} else {
			response = this.customEngine.rateItem(item);
		}
		return response;
	}

	public override FavoriteArticlesWrapper getFavoriteArticleDetail(FavoriteArticlesWrapper favorite, String prefix) {
		if (favorite.storage == HerokuWebService.StorageType.salesforce.name()) {
			favorite = this.articlesEngine.getFavoriteArticleDetail(favorite, prefix);
		}
		// @TODO is the custom details require for heroku ? 
		// see the getArticleDetail implementation
		return favorite;
	}

	public override SearchResultsWrapper getArticleDetail(SearchResultsWrapper article, String prefix) {
		if (article.storage == HerokuWebService.StorageType.salesforce.name()) {
			article = this.articlesEngine.getArticleDetail(article, prefix);
		}
		// @TODO: is this required for Heroku Files??
		//if (article.body.contains('{!Document.CompanyLogo}')) {
		//	String companyLogo = Utils.getCompanyLogoName(prefix);
		//	article.body = article.body.replaceAll( '\\{!Document.CompanyLogo}', companyLogo);
		//}

		//if (article.body.contains('{!User.')) {
		//	Set<String> allInteractiveFields = Utils.getAllInteractiveFields(article.body);
		//	User theUser = Utils.getAllUserInfo(allInteractiveFields);
		//	article.body = Utils.updateArticleBody(
		//		article.body,
		//		theUser,
		//		allInteractiveFields
		//	);
		//}
		return article;
	}

	private Map<String, SearchResultsWrapper> getSalesforceSearchResults(List<HerokuWebService.HerokuArticle> searchResults) {
		Map<String, Set<String>> knowledgeObjects = this.getSalesforceConfigForResults(searchResults);
		List<SObject> salesforceArticles = new List<SObject>();
		if (!knowledgeObjects.isEmpty() && !knowledgeObjects.get('API').isEmpty()) {

			Set<String> fieldsToSearch = this.articlesEngine.getRequiredFieldsForSearch();
			fieldsToSearch.add('KnowledgeArticleId');
			String channelFilter = Utils.getValueFromConciergeSettings(AbstractSearchEngine.CHANNEL_FILTER_SETTING_NAME);
			String locale = Utils.getUserLanguage();
			Set<String> salesforceArticleIds = knowledgeObjects.get('ID');
			Integer counter = 0;

			for (String knowledgeApi: knowledgeObjects.get('API')) {
				counter++;
				// SOQL request in the loop, but no more than 10 requests
				if (counter > 10) break;

				String query = 'SELECT ' + String.join((Iterable<String>)fieldsToSearch, ',');
				query += ' FROM ' + knowledgeApi;
				query += ' WHERE KnowledgeArticleId IN: salesforceArticleIds ';
				query += ' AND PublishStatus = \'Online\' ';
				query += ' AND Language =: locale ';
				if (String.isNotBlank(channelFilter)) {
					query += ' AND ' + AbstractSearchEngine.CHANNELS_MAP.get(channelFilter) + ' = true ';
				}
				salesforceArticles.addAll(Database.query(query));
			}
		}

		// convert salesforce records to SearchResults
		Map<String, SearchResultsWrapper> results = new Map<String, SearchResultsWrapper>();
		Integer indicatorLiveTime = this.articlesEngine.getUpdateIndicatorTime();
		for (SObject article: salesforceArticles) {
			String snippet = CustomerExtensionBase.cutPreviewToLength( (String)article.get('Title') );
			results.put(
				(String)article.get('KnowledgeArticleId'),
				this.articlesEngine.getInitialSearchResultWrapper(article, snippet, indicatorLiveTime)
			);
		}
		return results;
	}

	private Map<String, FavoriteArticlesWrapper> getSalesforceFavoriteResults(List<HerokuWebService.HerokuArticle> searchResults) {
		Map<String, Set<String>> knowledgeObjects = this.getSalesforceConfigForResults(searchResults);

		// convert salesforce records to Favorites
		Map<String, FavoriteArticlesWrapper> results = new Map<String, FavoriteArticlesWrapper>();
		if (!knowledgeObjects.isEmpty() && !knowledgeObjects.get('ID').isEmpty()) {
			List<String> articleIds = new List<String>(knowledgeObjects.get('ID'));
			List<Object> favorites = this.articlesEngine.getFavoritesbyId(articleIds, null);

			for (FavoriteArticlesWrapper favorite: (List<FavoriteArticlesWrapper>)favorites) {
				results.put( (String)favorite.article.get('KnowledgeArticleId'), favorite );
			}
		}
		return results;
	}

	private Map<String, Set<String>> getSalesforceConfigForResults(List<HerokuWebService.HerokuArticle> searchResults) {
		Set<String> salesforceArticleIds = new Set<String>();
		Set<String> knowledgeObjects = new Set<String>();
		for (HerokuWebService.HerokuArticle docItem : searchResults) {
			if (docItem.storage == HerokuWebService.StorageType.salesforce.name()) {
				String knowledgeObject = this.getKnowledgeApiName(docItem);

				if (String.isNotBlank(knowledgeObject)) {
					salesforceArticleIds.add( docItem.knowledgeArticleId );
					knowledgeObjects.add( knowledgeObject );
				}
			}
		}

		return new Map<String, Set<String>> {
			'ID' => salesforceArticleIds,
			'API' => knowledgeObjects
		};
	}

	private String buildArticleDetail(HerokuWebService.HerokuArticle article) {
		article.fields.remove('highlight');
		//@TODO CREATE FIELDS MAPPING
		Map<String, String> fieldsToDisplay = new Map<String, String> {
			'ServerRelativeUrl' => 'Document Link',
			'file-link' => 'Content',
			'URL' => 'Document Link',
			'Summary' => 'Summary'
		};

		Map<String, Schema.SObjectField> salesforceFields = new Map<String, Schema.SObjectField>();
		if (article.storage == HerokuWebService.StorageType.salesforce.name()) {
			Map<String, Schema.SObjectType> globalDesc = Schema.getGlobalDescribe();
			if (article.objectType != null && globalDesc.get(article.objectType.toLowerCase()) != null) {
				salesforceFields = globalDesc.get(article.objectType.toLowerCase()).getDescribe().fields.getMap();
			}
		}

		String summary = '';
		for (String fieldApi : article.fields.keySet()) {
			if (fieldsToDisplay.containsKey(fieldApi)
				|| salesforceFields.containsKey(fieldApi.toLowerCase())
			) {

				String tempSummary = String.valueOf(article.fields.get(fieldApi));
				if (String.isNotBlank(tempSummary)) {
					String fieldLabel = fieldsToDisplay.get(fieldApi);
					fieldLabel = String.isEmpty(fieldLabel)
						? salesforceFields.get(fieldApi.toLowerCase()).getDescribe().getLabel()
						: fieldLabel;

					summary += '<h1>' + fieldLabel + '</h1>';
					if (fieldApi == 'ServerRelativeUrl') {
						String articleTitle = String.valueOf(article.fields.get('Title'));
						summary += '<a target="_blank" href="' + tempSummary + '">' + articleTitle + '</a><br/><br/>';
					} else if (fieldApi == 'URL') {
						String articleTitle = String.valueOf(article.fields.get('Title'));
						summary += '<a target="_blank" href="' + tempSummary + '">' + articleTitle + '</a><br/><br/>';
					} else {
						summary += tempSummary + '<br/><br/>';
					}
				}
			}
		}
		return summary;
	}

	private String getSnippet(Map<String, Object> content) {
		String snippet = '';
		if (content != null && !content.isEmpty()) {
			if (content.containsKey('Title')) {
				snippet = String.join((List<Object>)content.get('Title'), '...');
			} else if (content.containsKey('attachment.content')) {
				snippet = String.join((List<Object>)content.get('attachment.content'), '...');
			}
		}
		return CustomerExtensionBase.cutPreviewToLength(snippet);
	}

	private String getKnowledgeApiName(HerokuWebService.HerokuArticle article) {
		String knowledgeApi = '';
		if (!Utils.isLightningKnowledgeEnabled()) {
			try {
				Schema.SObjectType articleType = Id.valueOf(article.recordId).getSObjectType();
				knowledgeApi = articleType.getDescribe().getName();
			} catch (Exception e) {
				System.debug('MultiSearchEngineAdaptor: Knowledge API Name: ' + e.getMessage() + '\n' + e.getStackTraceString());
			}
		}
		knowledgeApi = Utils.getLightningKnowledgeObjectName();
		return knowledgeApi;
	}
}