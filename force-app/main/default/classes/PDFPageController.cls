public with sharing class PDFPageController  {
    public String renderAs{get;set;}
    public String title{get;set;}
    public String dataCategory {get;set;}
    public String [] arrayOfArticlesData = new List<String>();
    public String [] arrayStr {get;set;}
    public String fieldsToQuery {get;set;}
    public String stringId {get;set;}
    public String articleType {get;set;}
    public Boolean typeCustom {get;set;}
    public String knowledgeObjectName {get;set;}
    public String attachments {get;set;}
    public Map<String, String> fieldNameToDisplayName = new Map<String, String>();

    public String[] getStrings() {
        arrayOfArticlesData = updateJapanese(arrayOfArticlesData, '、');
        arrayOfArticlesData = updateJapanese(arrayOfArticlesData, '。');
        
        return arrayOfArticlesData;
    }
    
    public String getLanguage(){
        return UserInfo.getLanguage();
    }
    
    private List<String> updateJapanese(List<String> startArray, String letter){
        List<String> finalList = new List<String>();
        for(String item : startArray){
            List<String> splittedString = item.split(letter);
            String finalString = String.join(splittedString, letter + ' ');
            finalList.add(finalString);
        }
        return finalList;
    }

    public PDFPageController() {
        stringId = ApexPages.currentPage().getParameters().get('Id');
        stringId = stringId != null ? String.escapeSingleQuotes(stringId) : null;
        articleType = ApexPages.currentPage().getParameters().get('Type');
        articleType = articleType != null ? String.escapeSingleQuotes(articleType) : null;
        typeCustom = Utils.isLightningKnowledgeEnabled();
        knowledgeObjectName = Utils.getLightningKnowledgeObjectName();
        if (!typeCustom) {
            dataCategory = articleType;
        } else {
            String[] articleTypeArr = knowledgeObjectName.split('__');
            if (articleTypeArr.size() < 3) {
                dataCategory = articleTypeArr[0] + '__kav';
            } else {
                dataCategory = articleTypeArr[1] + '__kav';
            }
        }
        try {
            List <cncrg__Search_Types_Settings__c> fieldToDisplay =[	SELECT	cncrg__Fields_To_Display_1__c, 
																				cncrg__Fields_To_Display_2__c,
																				cncrg__Fields_To_Display_3__c,
																				cncrg__Fields_To_Display_4__c,
																				Article_Type_API_Name__c, 
																				cncrg__Active__c,
																				cncrg__Is_Usual_Object__c
																		FROM	cncrg__Search_Types_Settings__c
																		WHERE	Article_Type_API_Name__c = : articleType
																		LIMIT	1
            ];
            String nameAtt;
            String query = getQueryString(fieldToDisplay, stringId);
            List < sObject > listCategory = Database.query(query);
            Integer countAtt =0;
            title = (String) listCategory.get(0).get('Title');
            for (Integer i = 0; i < arrayStr.size(); i++) {
                String fieldName = arrayStr[i];
                String value;

                try {
                    value = (String) listCategory.get(0).get(fieldName);
                } catch (Exception ex) {
                    countAtt++;
                    value = listCategory.get(0).get(fieldName) + '';
                }
                if (value == null) {
                    continue;
                }
                if (value.contains('Blob')) {
//                    List<String> attachList = attachments.split(',');
//                    nameAtt = (countAtt ==2) ? (String) listCategory.get(0).get(attachList[0]) : (String) listCategory.get(0).get(attachList[1]);
//                    value = '<a href="https://' + ApexPages.currentPage().getHeaders().get('Host') + '/servlet/fileField?entityId=' + (String) listCategory.get(0).get('id') + '&field=' + fieldName + '">' + nameAtt + '</a>';
                }
                for (String field: fieldNameToDisplayName.keySet()){
                    if(field == fieldName){
                        fieldName = fieldNameToDisplayName.get(field).replace('(Body)', '');
                    }
                }
                arrayOfArticlesData.add(getFieldName(fieldName));
                if (fieldName.contains('Full Description')) {
                    if (value.contains('width="')) {
                        value = convertBigSizeImage(value);
                    }
                    if (value.contains('style="width:')) {
                        value = value.replaceAll('style="width:.*?;', 'style="width: 100%');
                    }
                }
                if (value.contains('<iframe')) {
                    value = converIframeToLink(value);
                }
                arrayOfArticlesData.add(value);
                arrayOfArticlesData.add('<br>');
            }
            renderAs = 'advanced_pdf';
        } catch (Exception ex) {
            System.debug('Exception ' + ex.getMessage());
            System.debug('Exception ' + ex.getLineNumber());
            renderAs = 'advanced_pdf';
        }

    }

    public String getQueryString(List <Search_Types_Settings__c> fieldToDisplay, String stringId){
        
        /*if(fieldToDisplay[0].Fields_To_Display_1__c != null) {
            fieldsToQuery = fieldToDisplay[0].Fields_To_Display_1__c;
        } if (fieldToDisplay[0].Fields_To_Display_2__c != null) {
            fieldsToQuery = fieldsToQuery + fieldToDisplay[0].Fields_To_Display_2__c;
        }
        attachments = '';*/
        /*for(Schema.SObjectType o : Schema.getGlobalDescribe().values()) {
            Schema.DescribeSObjectResult objResult = o.getDescribe();
            if(objResult.getName().endsWith(articleType)) {
                Map<String, Schema.SObjectField> fields = objResult.fields.getMap();
                for (String field: fields.keySet()) {
                    fieldNameToDisplayName.put(field, fields.get(field).getDescribe().getLabel());
                    if(field.contains('attachment__name__s')){
                        attachments = attachments + field +',';
                    }
                }
            }
        }*/


        //Set<String> tempArray = new Set<String>(fieldsToQuery.split(','));
        String sObjectName = Utils.isLightningKnowledgeEnabled() ? Utils.getLightningKnowledgeObjectName() : fieldToDisplay[0].cncrg__Article_Type_API_Name__c;
        SObject objectRecord = (SObject) Type.forName(sObjectName).newInstance();
        Schema.SObjectType sobjType = objectRecord.getSObjectType();
    
        Schema.DescribeSObjectResult describe = sobjType.getDescribe();
        Map<String, Schema.FieldSet> fsMap = describe.fieldSets.getMap();
        Map<String, Schema.SObjectField> allFieldsMap = describe.fields.getMap();
        List<String> tempArray = Utils.prepareTypesSettings(fieldToDisplay, false).get(fieldToDisplay[0].Article_Type_API_Name__c).availableFieldsList;
        arrayStr = new List<String>();
        for(String key : tempArray) {
            fieldNameToDisplayName.put(key, allFieldsMap.get(key).getDescribe().getLabel());
            if(!key.contains('__s')) {
                arrayStr.add(key);
            }
        }

//        arrayStr = fieldsToQuery.split(',');
        String query = 'SELECT ' + String.join(arrayStr, ',') + ',id' + (tempArray.contains('Title') ? '' : ',Title') + ' FROM ' + dataCategory + ' WHERE id = ' + '\'' + stringId + '\'' + ' LIMIT 1';
        return query;
    }

    public String getFieldName(String fieldName){
        /*String newFieldname;
        String[] arrayfieldName;
        String fieldNameToHTML;
        if (fieldName.contains('__')) {
            arrayfieldName = fieldName.split('__');
            if (arrayfieldName.size() >= 3) {
                newFieldname = arrayfieldName[1].replaceAll('_', ' ');
            } else {
                newFieldname = arrayfieldName[0].replaceAll('_', ' ');
            }
        } else {
            newFieldname = fieldName;
        }
        */
        fieldName = '<b>' + fieldName + '</b>';
        return fieldName;
    }

    public String convertBigSizeImage(String value){
        String convertOutput = '';
        String ahref;
        String [] widthReg;
        String [] heightReg;
        Integer oldWidth;
        Integer oldHeight;
        Pattern p = Pattern.compile('(width=".*?")');
        Matcher m = p.matcher(value);
        Pattern p1 = Pattern.compile('(height=".*?")');
        Matcher m1 = p1.matcher(value);
        while (m.find() == true && m1.find() == true) {
            widthReg = m.group(0).split('"');
            heightReg = m1.group(0).split('"');
            oldWidth = Integer.valueof(widthReg[1]);
            oldHeight = Integer.valueof(heightReg[1]);
            Integer maxSize = 700;
            Double factor = oldWidth/maxSize;
            if(oldWidth>maxSize){
                Integer newWidth = maxSize;
                Double newHeightDec = oldHeight/factor;
                value = value.replace('width="'+oldWidth+'"', 'width="'+newWidth+'"');
                value = value.replace('height="'+oldHeight+'"', 'height="'+newHeightDec+'"');
            }
        }
        return value;
    }

    public String converIframeToLink(String value){
        String convertOutput = '';
        String ahref;
        String src;
        Pattern p = Pattern.compile('(="http.*?")');
        Matcher m = p.matcher(value);
        while (m.find() == true) {
            src = m.group(0);
            if (src.contains('www.youtube.com')) {
                ahref = '<a href' + src + '>' + src.substring(2, src.length() - 1) + '</a>';
                convertOutput = value.replaceAll('<iframe.*?></iframe>', ahref);
            }
        }
        return convertOutput;
    }
}