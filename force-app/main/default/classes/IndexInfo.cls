public with sharing class IndexInfo {
	public Result result;
    public Boolean error;

    public class Result {
        public Integer index_scans;
        public Boolean isready;
        public String index;
        public String table_size;
        public String indexes_size;
        public String total_table_space_size;
        public String database_size;
        public Integer indexed_rows;
        public Integer all_rows;
    }

    public static IndexInfo parse(String json) {
        return (IndexInfo) System.JSON.deserialize(json, IndexInfo.class);
    }
}