global class IndexationBatch implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts {
	
	@TestVisible
	public static String batchResult = '';
	String query;

	@TestVisible
	private String tableName;
	private List<String> fieldNames;
	private String objectName;

	global IndexationBatch(String objectName, List<String> fieldNames) {
		
	}

	global IndexationBatch(String objectName, List<String> fieldNames, String queryProperties) {
		
		//Create Table with fields
		this.fieldNames = fieldNames;
		if(ESAPI.securityUtils().isAuthorizedToView(objectName, fieldNames)){
			
			Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        	Map <String, Schema.SObjectField> fieldMap = schemaMap.get(objectName).getDescribe().fields.getMap();

			this.tableName = createTable(fieldMap, fieldNames, objectName);
		
			query = 'Select ' + String.escapeSingleQuotes(String.join(fieldNames, ',')) + ' FROM ' + String.escapeSingleQuotes(objectName) + ' ' + queryProperties;
		} else
			query = '';

	}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		return (query != '') ? Database.getQueryLocator(query) : null;
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
   		//Fill created table with data
		this.populateTable(scope);
	}

	global void finish(Database.BatchableContext BC) {
		
		//Create Index for created table and update
  		this.createIndexForTable();
		IndexationBatch.batchResult = this.updateIndex();
	
	}

	private String updateIndex() {
		String result;
		if(String.isNotBlank(tableName)) {
			String preparedTableName = this.tableName.remove('documents_');

			String jsonUpdateIndex = JSONOptionsWizard.updateIndexTableJson(this.fieldNames, preparedTableName);

			JsonDeserializerSimpleResponse response = HerokuWebService.updateIndex(jsonUpdateIndex);

			if (!response.error){
				result = 'Success';
			}
		}
		return result;
	}

	private String createIndexForTable() {
		
		String result;

		if(String.isNotBlank(tableName)) {
			String preparedTableName = this.tableName.remove('documents_');
			
			String jsonCreateIndex = JSONOptionsWizard.createIndexJson(preparedTableName, 'GIN');

			JsonDeserializerSimpleResponse response = HerokuWebService.createIndex(jsonCreateIndex);
	
			if (!response.error){
				result = 'Success';
			}
		}

		return result;
	}

	@TestVisible
	private String populateTable(List<SObject> records) {

		String result;
		String jsonInsertTableRequest = JSONOptionsWizard.populateTableJson(records, this.fieldNames, this.tableName);

		JsonDeserializerInsertResponse response = HerokuWebService.populateTable(jsonInsertTableRequest);
		
		if (!response.error){
			result = 'Success';
		}

		return result;
	}

	private String createTable(Map <String, Schema.SObjectField> fieldMap, List<String> fieldNames, String objectName) {
		String result;
		String jsonCreateTableRequest = JSONOptionsWizard.createTableJson(fieldMap, fieldNames, objectName);

		JsonDeserializerSimpleResponse response = HerokuWebService.createTable(jsonCreateTableRequest);
	
		if(response != null){
			result = response.result.remove(' table was created');
		} else {
			result = null;
		}

		return result;
	}
}