global class DesignBGMobileResourcePickList extends VisualEditor.DynamicPickList implements Database.Batchable<sObject>, Database.AllowsCallouts {
	global override VisualEditor.DataRow getDefaultValue() {
		VisualEditor.DataRow defaultValue = new VisualEditor.DataRow('--None--', '--None--');
		return defaultValue;
	}

	global override VisualEditor.DynamicPickListRows getValues() {
		List<Background_Settings__c> backgroundSettingsList = Background_Settings__c.getall().values();
		VisualEditor.DynamicPickListRows  backgroundValues = new VisualEditor.DynamicPickListRows();
		backgroundValues.addRow(new VisualEditor.DataRow('--None--', '--None--'));
		for(Background_Settings__c item : backgroundSettingsList) {
			//if(item.cncrg__Background_File_Type__c == 'Image') {
				backgroundValues.addRow(new VisualEditor.DataRow(item.Name, item.Name));
			//}
		}
		return backgroundValues;
	}

	//TODO DELETE batch logic
	public Database.QueryLocator start(Database.BatchableContext BC) {return Database.getQueryLocator('SELECT Id FROM Account');}
	public void execute(Database.BatchableContext context, List<sObject> scope) {}
	public void finish(Database.BatchableContext context) {}
}