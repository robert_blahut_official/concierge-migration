public with sharing class ArticleReviewUtils {

	public static final String AUTOMATICALLY_REVIEW_CUSTOM_SETTINGS_NAME = 'Review articles after _ days';
	public static final String AUTOMATICALLY_REVIEW_CUSTOM_SETTINGS_DEFAULT = '90';
	public static final String SOURCE_ARTICLE_RECORD_TYPE_ID = Schema.SObjectType.Article_Review__c.getRecordTypeInfosByName().get('Source Article').getRecordTypeId();
	public static final String TRANSLATION_ARTICLE_RECORD_TYPE_ID = Schema.SObjectType.Article_Review__c.getRecordTypeInfosByName().get('Translation Article').getRecordTypeId();

	public static final Integer MAX_EXTERNAL_STORAGE = 100;
	public static final Integer MAX_NAME_LENGTH = 80;

	public static final String HEROKU_SYSTEM_NAME = 'EXTERNAL';

	public static final String DEFAULT_LANGUAGE = 'English';

	public static final List<String> GENERAL_ARTICLE_FIELDS = new List<String> { 'Id', 'Title', 'UrlName', 'OwnerId', 'LastModifiedDate', 'KnowledgeArticleId' };

	private static String mainRecordTypeName;

	public static Boolean ARTICLE_REVIEW_UPDATE = false;

	public static Boolean checkEnableFunctionality() {
		return Concierge_Settings__c.getValues(AUTOMATICALLY_REVIEW_CUSTOM_SETTINGS_NAME) != null
		&& String.isNotBlank((String) Concierge_Settings__c.getValues(AUTOMATICALLY_REVIEW_CUSTOM_SETTINGS_NAME).Value__c)
		&& SchedulerSettings__c.getValues(TurboEncabulatorScheduler.REVIEW_BATCH_NAME) != null
		&& SchedulerSettings__c.getValues(TurboEncabulatorScheduler.REVIEW_BATCH_NAME).Active__c;
	}

	public static Integer getAutomaticallyReviewDay() {
		if (checkEnableFunctionality()) {
			return Integer.valueOf(Concierge_Settings__c.getValues(AUTOMATICALLY_REVIEW_CUSTOM_SETTINGS_NAME).Value__c);
		}
		return null;
	}

	public static Boolean updateLikedORFavoritedArticle(SearchResultsWrapper wrap, String currentLanguage) {
		return updateLikedORFavoritedArticle(wrap.knowledgeArticleId, currentLanguage);
	}

	public static Boolean updateLikedORFavoritedArticle(String articleId, String currentLanguage) {
		Boolean isSuccess = true;
		try {
			List<String> generalFields = new List<String> (GENERAL_ARTICLE_FIELDS);
			if (!checkEnableFunctionality()) {
				return false;
			}
			checkOnMasterLanguage(generalFields);
			Article_Review__c currentArticleReview = getArticleReviewByArticleId(articleId, currentLanguage);
			String query;
			if (Utils.isLightningKnowledgeEnabled()) {
				query = 'SELECT RecordType.Name,' + String.join(generalFields, ',') + ' FROM ' + getMainRecordTypeName() + ' WHERE PublishStatus = \'Online\' AND KnowledgeArticleId = \'' + articleId + '\' AND Language =  \'' + currentLanguage + '\'';
			} else {
				query = 'SELECT ArticleType,' + String.join(generalFields, ',') + ' FROM KnowledgeArticleVersion WHERE PublishStatus = \'Online\' AND KnowledgeArticleId = \'' + articleId + '\' AND Language =  \'' + currentLanguage + '\'';
			}
			List<sObject> currentArticle = Database.query(query);
			if (currentArticleReview != null) {
				currentArticleReview.Full_Name__c = String.valueOf(currentArticle[0].get('Title'));
				currentArticleReview.Name = currentArticleReview.Full_Name__c.abbreviate(MAX_NAME_LENGTH);
				currentArticleReview.UrlName__c = String.valueOf(currentArticle[0].get('UrlName'));
				if ((Datetime) currentArticle[0].get('LastModifiedDate') > currentArticleReview.LastModifiedDate) {
					currentArticleReview.Last_Reviewed_Date__c = Date.today();
					currentArticleReview.Next_Review_Date__c = getNextReviewDay(Date.today());
				}
				isSuccess = updateLikedORFavoritedArticle(currentArticleReview);
			} else {
				if (!currentArticle.isEmpty()) {
					Article_Review__c newArticleReview = createNewArticleReviewRecord(currentArticle[0], currentLanguage, getNextReviewDay());
					newArticleReview.Last_Liked_or_Favorited__c = Datetime.now();
					newArticleReview.Next_Review_Date__c = getNextReviewDay(Date.today());
					ESAPI.securityUtils().validatedInsert(new List<Article_Review__c> { newArticleReview });
				}
			}
		} catch(Exception ex) {
			isSuccess = false;
		}
		return isSuccess;
	}

	public static Boolean updateLikedORFavoritedArticle(Article_Review__c currentArticleReview) {
		Boolean isSuccess = true;
		try {
			currentArticleReview.Last_Liked_or_Favorited__c = Datetime.now();
			currentArticleReview.Next_Review_Date__c = getNextReviewDay(Date.today());
			ESAPI.securityUtils().validatedUpdate(new List<Article_Review__c> { currentArticleReview });
		} catch(Exception ex) {
			isSuccess = false;
		}
		return isSuccess;
	}

	public static Date getNextReviewDay() {
		return getNextReviewDay(Date.today());
	}

	public static Date getNextReviewDay(Date today) {
		return today.addDays(getAutomaticallyReviewDay());
	}

	public static List<Article_Review__c> findArticleReviewByKnowledgeIds(Set<String> knowledgeIds, String currentObject, String currentLanguage) {
		return[
		SELECT Id,
		Last_Reviewed_Date__c,
		Last_Liked_or_Favorited__c,
		Language__c,
		Never_Review__c,
		Next_Review_Date__c,
		Parent_Article__c,
		Expiration_Date__c,
		LastModifiedDate,
		Full_Name__c,
		UrlName__c
		FROM Article_Review__c
		WHERE Parent_Article__c IN : knowledgeIds
		AND Language__c = : currentLanguage
		];
	}

	public static Article_Review__c createNewArticleReviewRecord(sObject article, String language, Date nextReviewDate) {
		if (Utils.isLightningKnowledgeEnabled()) {
			return createNewArticleReviewRecordForLightning(article, language, nextReviewDate);
		}
		String title = String.valueOf(article.get('Title'));
		return new Article_Review__c(
		                             Language__c = language,
		                             Next_Review_Date__c = nextReviewDate,
		                             Parent_Article__c = String.valueOf(article.get('KnowledgeArticleId')),
		                             Name = title.abbreviate(MAX_NAME_LENGTH),
		                             Full_Name__c = title,
		                             System__c = UserInfo.getOrganizationName(),
		                             OwnerId = String.valueOf(article.get('OwnerId')),
		                             UrlName__c = String.valueOf(article.get('UrlName')),
		                             Article_Type__c = getArticleType(article),
		                             RecordTypeId = getRecordTypeForArticleReview(article)
		);
	}

	private static String getArticleType(sObject article) {
		String currentArticleType;
		List<String> articleTypeParts;
		if (article.getSObjectType().getDescribe().getName() == 'KnowledgeArticleVersion') {
			articleTypeParts = String.valueOf(article.get('ArticleType')).split('__');
			currentArticleType = articleTypeParts[articleTypeParts.size() - 2];
		} else {
			articleTypeParts = article.getSObjectType().getDescribe().getName().split('__');
			currentArticleType = articleTypeParts[articleTypeParts.size() - 2];
		}
		return currentArticleType;
	}

	private static String getRecordTypeForArticleReview(sObject article) {
		Map<String, Object> populateFieldsMap = article.getPopulatedFieldsAsMap();
		if (populateFieldsMap.containsKey('IsMasterLanguage') && !Boolean.valueOf(populateFieldsMap.get('IsMasterLanguage'))) {
			return TRANSLATION_ARTICLE_RECORD_TYPE_ID;
		}
		return SOURCE_ARTICLE_RECORD_TYPE_ID;
	}

	private static void checkOnMasterLanguage(List<String> generalFields) {
		if (KnowledgeArticleVersion.sObjectType.getDescribe().fields.getMap().containsKey('isMasterLanguage')) {
			generalFields.add('isMasterLanguage');
		}
	}

	public static List<Article_Review__c> getArticleByParent(List<String> newKnowledgeArticleIds) {
		Map<String, List<Article_Review__c>> articlesByKnowledgeMap = new Map<String, List<Article_Review__c>> ();
		Map<String, String> parentReviewMap = new Map<String, String> ();
		List<Article_Review__c> result = new List<Article_Review__c> ();
		List<Article_Review__c> articleList = [SELECT Id, RecordTypeId, Parent_Article__c FROM Article_Review__c WHERE Parent_Article__c IN :newKnowledgeArticleIds AND Parent_Article_Review__c = null];
		Article_Review__c currentReview;
		for (Integer i = 0; i<articleList.size(); i++) {
			currentReview = articleList[i];
			if (currentReview.RecordTypeId == SOURCE_ARTICLE_RECORD_TYPE_ID) {
				parentReviewMap.put(currentReview.Parent_Article__c, currentReview.Id);
			} else {
				if (articlesByKnowledgeMap.containsKey(currentReview.Parent_Article__c)) {
					articlesByKnowledgeMap.get(currentReview.Parent_Article__c).add(currentReview);
				} else {
					articlesByKnowledgeMap.put(currentReview.Parent_Article__c, new List<Article_Review__c> { currentReview });
				}
				result.add(currentReview);
			}
		}
		List<Article_Review__c> currentReviewList;
		for (String key : articlesByKnowledgeMap.keySet()) {
			currentReviewList = articlesByKnowledgeMap.get(key);
			for (Integer i = 0; i<currentReviewList.size(); i++) {
				currentReviewList[i].Parent_Article_Review__c = parentReviewMap.get(key);
			}
		}
		return result;
	}

	public static Article_Review__c createNewArticleReviewRecord(String articleId, String title, Date nextReviewDate, String articleType) {
		return new Article_Review__c(
		                             Language__c = UserInfo.getLanguage(),
		                             Next_Review_Date__c = nextReviewDate,
		                             Parent_Article__c = articleId,
		                             Name = title.abbreviate(MAX_NAME_LENGTH),
		                             Full_Name__c = title,
		                             System__c = 'External Source',
		                             UrlName__c = title,
		                             Article_Type__c = articleType
		);
	}

	public static Article_Review__c createNewArticleReviewRecordForLightning(sObject article, String language, Date nextReviewDate) {
		String articleType;
		String title = String.valueOf(article.get('Title'));
		try {
			articleType = (String) ((RecordType) article.getSObject('RecordType')).get('Name');
		} catch(Exception e) {
			articleType = 'Default';
		}
		List<String> mainType = getMainRecordTypeName().split('__');
		return new Article_Review__c(
		                             Language__c = language,
		                             Next_Review_Date__c = nextReviewDate,
		                             Parent_Article__c = String.valueOf(article.get('KnowledgeArticleId')),
		                             Name = title.abbreviate(MAX_NAME_LENGTH),
		                             Full_Name__c = title,
		                             System__c = UserInfo.getOrganizationName(),
		                             OwnerId = String.valueOf(article.get('OwnerId')),
		                             UrlName__c = String.valueOf(article.get('UrlName')),
		                             Article_Type__c = mainType[mainType.size() - 2] + '/' + articleType,
		                             RecordTypeId = getRecordTypeForArticleReview(article)
		);
	}

	public static Article_Review__c createNewArticleReviewRecord(JsonDeserializerDocuments.Result result, String language, Date nextReviewDate) {
		return new Article_Review__c(
		                             Language__c = language,
		                             Next_Review_Date__c = nextReviewDate,
		                             Parent_Article__c = String.valueOf(result.id),
		                             Name = result.title.abbreviate(MAX_NAME_LENGTH),
		                             Full_Name__c = result.title,
		                             System__c = HEROKU_SYSTEM_NAME
		);
	}

	public static Boolean reviewArticle(String articleId) {
		Boolean result = true;
		Article_Review__c review = updateReviewDate(getArticleReview(articleId), Date.today());
		try {
			ESAPI.securityUtils().validatedUpdate(new List<Article_Review__c> { review });
		} catch(Exception e) {
			result = false;
		}
		return result;
	}

	public static void updateBatchStatus() {
		if (!checkEnableFunctionality()) {
			SchedulerSettings__c reviewBatch = SchedulerSettings__c.getAll().get('ArticleReviewBatch');
			if (reviewBatch != null) {
				reviewBatch.Active__c = false;
				update reviewBatch;
			}
		}
	}

	public static Article_Review__c updateReviewDate(Article_Review__c articleReview, Date today) {
		articleReview.Last_Reviewed_Date__c = today;
		articleReview.Next_Review_Date__c = getNextReviewDay(today);
		articleReview.Custom_Review_Date__c = null;
		return articleReview;
	}

	public static Article_Review__c getArticleReviewByArticleId(String articleId, String currentLanguage) {
		List<Article_Review__c> reviewList = [SELECT Id, LastModifiedDate FROM Article_Review__c WHERE Parent_Article__c = :articleId AND Language__c = :currentLanguage];
		if (!reviewList.isEmpty()) {
			return reviewList[0];
		}
		return null;
	}

	public static Article_Review__c retrieveArticleReviewRecord(String recordId) {
		return[SELECT Name, Language__c, Parent_Article__c, Article_Type__c, Full_Name__c FROM Article_Review__c WHERE Id = : recordId];
	}

	public static SearchResultsWrapper findArticle(String recordId) {
		return findArticle(retrieveArticleReviewRecord(recordId));
	}

	public static SearchResultsWrapper findArticle(Article_Review__c review) {
		String articleType;
		String recordId;
		String query;
		if (Utils.isLightningKnowledgeEnabled()) {
			if (review.Parent_Article__c.startsWith('x')) {
				articleType = review.Article_Type__c;
				recordId = review.Parent_Article__c;
			} else {
				String objName = getMainRecordTypeName();
				articleType = review.Article_Type__c.split('/') [1];
				query = 'SELECT Id FROM ' + objName + ' WHERE RecordType.Name = \'' + articleType + '\' AND KnowledgeArticleId = \'' + review.Parent_Article__c + '\' AND Language = \'' + review.Language__c + '\' AND PublishStatus = \'Online\'';
			}
		} else {
			String namespacePrefix = Utils.getOrgPrefix();
			articleType = namespacePrefix + review.Article_Type__c + Utils.SALESFORCE_SEPARATOR + 'kav';
			query = 'SELECT Id FROM KnowledgeArticleVersion WHERE ArticleType = \'' + articleType + '\' AND KnowledgeArticleId = \'' + review.Parent_Article__c + '\' AND Language = \'' + review.Language__c + '\' AND PublishStatus = \'Online\'';
		}
		if (query != null) {
			sObject rec = Database.query(query);
			recordId = rec.Id;
		}
		SearchResultsWrapper searchResultsWrapperRecord = new SearchResultsWrapper(
		                                                                           review.Full_Name__c,
		                                                                           '',
		                                                                           recordId,
		                                                                           'blankValue',
		                                                                           articleType,
		                                                                           1,
		                                                                           recordId,
		                                                                           ''
		);
		searchResultsWrapperRecord.typeName = articleType;
		SearchResultsWrapper currentRecord;
		if (Test.isRunningTest()) {
			currentRecord = new SearchResultsWrapper('', '', '', '', articleType);
		} else {
			currentRecord = searchResultsWrapperRecord;
		}
		currentRecord.recordId = recordId;
		return getDetailArticleInformation(currentRecord);
	}

	private static SearchResultsWrapper getDetailArticleInformation(SearchResultsWrapper articleRecord) {
		Boolean isDisabled = false;
		List<Search_Types_Settings__c> typeList = Utils.getSearchTypesSettingsByArticleAPIName(new Set<String> { articleRecord.type });
		if (!typeList.isEmpty()) {
			isDisabled = !typeList[0].Active__c;
		}
		Map<String, Utils.sObjectWrapper> fieldToDisplayNameMap = Utils.prepareTypesSettings(typeList, true, false);
		List<String> fieldToDisplayNameList = fieldToDisplayNameMap.get(articleRecord.type).availableFieldsList;
		Set<String> notShowingFieldSet = Utils.splitUnionFields(fieldToDisplayNameMap);
		Set<String> allFields = new Set<String> ();
		allFields.addAll(fieldToDisplayNameList);
		allFields.addAll(notShowingFieldSet);
		fieldToDisplayNameList = new List<String> (allFields);
		String recordId = articleRecord.recordId;
		String query = 'SELECT ' + String.join(fieldToDisplayNameList, ',') + ',Id FROM ' + (Utils.isLightningKnowledgeEnabled() ? Utils.getLightningKnowledgeObjectName() : articleRecord.type) + ' WHERE Id =: recordId';
		sObject articleRecordFromBase = Database.query(query);
		SearchResultsWrapper result = Utils.getArticleFieldsHTML(articleRecord, '', articleRecordFromBase, fieldToDisplayNameList, notShowingFieldSet);
		result.isDisabled = isDisabled;
		return result;
	}

	public static List<Article_Review__c> getHerokuArticles() {
		Date nextPrevDate = Date.today().addDays(getAutomaticallyReviewDay());
		List<Article_Review__c> result = new List<Article_Review__c> ();
		List<Integer> documentsIds = new List<Integer> ();
		for (Integer i = 0; i<MAX_EXTERNAL_STORAGE; i++) {
			documentsIds.add(i);
		}

		String bodyJson = JSONOptionsWizard.creategetDocumentsJson(documentsIds);

		HttpRequest req = HerokuWebService.formRequest('/fts-srv/documents', bodyJson, 'POST');
		HttpResponse res = HerokuWebService.sendRequest(req);
		JsonDeserializerDocuments articles = JsonDeserializerDocuments.parse(res.getBody());
		if (articles.result != null)
		for (JsonDeserializerDocuments.Result currentArticle : articles.result) {
			result.add(createNewArticleReviewRecord(currentArticle, DEFAULT_LANGUAGE, nextPrevDate));
		}
		return result;
	}

	public static List<String> getAllArticleLanguages() {
		List<String> result = new List<String> ();
		Schema.DescribeFieldResult fieldResult = KnowledgeArticleVersion.Language.getDescribe();

		List<Schema.PicklistEntry> picklistValues = fieldResult.getPicklistValues();

		for (Schema.PicklistEntry picklistEntry : picklistValues) {
			if (picklistEntry.isActive()) {
				result.add(picklistEntry.getValue());
			}
		}
		return result;
	}

	public static Article_Review__c getArticleReview(String articleId) {
		return[SELECT Custom_Review_Date__c, Expiration_Date__c, Full_Name__c, Last_Liked_or_Favorited__c, Last_Reviewed_Date__c, Language__c, Parent_Article__c, Next_Review_Date__c FROM Article_Review__c WHERE Id = : articleId];
	}

	public static Boolean isMasterLanguage(String reviewId) {
		Article_Review__c currentReview = [SELECT Language__c, Parent_Article__c FROM Article_Review__c];
		return isMasterLanguage(currentReview);
	}

	public static Boolean isMasterLanguage(Article_Review__c currentReview) {
		KnowledgeArticleVersion currentVersion = [SELECT ArticleMasterLanguage FROM KnowledgeArticleVersion WHERE KnowledgeArticleId = :currentReview.Parent_Article__c AND PublishStatus = 'Online' LIMIT 1];
		return currentReview.Language__c == currentVersion.ArticleMasterLanguage;
	}

	public static String preparedQuery(String currentArticle, String currentLanguage) {
		List<String> generalFields = new List<String> (GENERAL_ARTICLE_FIELDS);
		if (currentArticle != null) {
			checkOnMasterLanguage(generalFields);
			if (Utils.isLightningKnowledgeEnabled()) {
				return 'SELECT RecordType.Name, ' + String.join(generalFields, ',') + ' FROM ' + getMainRecordTypeName() + ' WHERE PublishStatus = \'Online\' AND Language = \'' + currentLanguage + '\' AND RecordType.Name = \'' + currentArticle + '\'';
			}
			return 'SELECT ' + String.join(generalFields, ',') + ' FROM ' + currentArticle + ' WHERE PublishStatus = \'Online\' AND Language = \'' + currentLanguage + '\'';
		}
		return null;
	}

	public static String getMainRecordTypeName() {
		if (String.isBlank(mainRecordTypeName))
		mainRecordTypeName = Utils.getLightningKnowledgeObjectName();
		return mainRecordTypeName;
	}
}