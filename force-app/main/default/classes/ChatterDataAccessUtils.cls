public with sharing class ChatterDataAccessUtils {

    public static final Integer RECENT_COMMENT_COUNT = 5;

    @AuraEnabled
    public static FeedLike insertLike(String recordId, String feedItemId) {
        FeedLike newLike = new FeedLike(FeedItemId = feedItemId, FeedEntityId = recordId);
        ESAPI.securityUtils().validatedInsert(new List<FeedLike>{newLike});
        return newLike;
    }

    @AuraEnabled
    public static void deleteLike(String likeId) {
        ESAPI.securityUtils().validatedDelete(new List<FeedLike>{new FeedLike(Id = likeId)});
    }

    public static ChatterPage getCaseChatter(String caseId) {
        return getCaseChatter(caseId, null);
    }

    public static ChatterPage getCaseChatter(String caseId, String nextPageToken) {
        ConnectApi.FeedElementPage fep = ConnectApi.ChatterFeeds.getFeedElementsFromFeed(Network.getNetworkId(), ConnectApi.FeedType.Record, caseId, nextPageToken, RECENT_COMMENT_COUNT, null);
        return parseChatter(fep);
    }

    private static List<FeedItemWrapper> getComments(String caseFeedId) {
        List<FeedItemWrapper> result = new List<FeedItemWrapper>();
        Object  compPage;
        if(Test.isRunningTest()) {
            compPage = ChatterDataAccessUtilsTest.generateFeedItem(caseFeedId).capabilities.comments.page;
        } else {
            compPage = ConnectApi.ChatterFeeds.getCommentsForFeedElement(Network.getNetworkId(), caseFeedId);
        }
        FeedItemWrapper currentComment;
        for(ConnectApi.Comment  cap : ((ConnectApi.CommentPage) compPage).items) {
            currentComment = new FeedItemWrapper(
                    cap.id,
                    cap.user.displayName,
                    cap.user.photo.smallPhotoUrl,
                    cap.createdDate,
                    cap.body.text,
                    cap.capabilities.upDownVote.upVoteCount.intValue(),
                    cap.myLike == null ? '' : cap.myLike.id,
                    cap.myLike != null,
                    false,
                    null
            );
            if(cap.capabilities.content != null) {
                addedAttachment(currentComment, cap.capabilities.content);
            }
            result.add(currentComment);
        }
        return result;
    }
    private static ChatterPage parseChatter(ConnectApi.FeedElementPage fep) {
        ConnectApi.FeedItem currentItem;
        List<FeedItemWrapper> result = new List<FeedItemWrapper>();
        FeedItemWrapper currentWrapper;
        FeedItemWrapper currentComment;
        String currentBody;
        Boolean isRichText;
        String likeId;
		
        for(ConnectApi.FeedElement e : fep.elements) {
			//exclude ConnectApi.GenericFeedElement feed items (changind Case fields)
			if(!(e instanceof ConnectApi.FeedItem)) {
				continue;
			}
			System.debug(e);
			//if(!isCommentHaveSupportedType(e)) {
			//	continue;
			//}
            currentItem = (ConnectApi.FeedItem) e;
			if(currentItem.type == ConnectApi.FeedItemType.TrackedChange) {
				continue;
			}
			if(currentItem.visibility != ConnectApi.FeedItemVisibilityType.AllUsers) {
				continue;
			}
            isRichText = e.body.isRichText;
            if(isRichText != null) {
                isRichText = e.body.isRichText;
                currentBody = preparedBody(e.body);
            } else {
                currentBody = '';
            }
            if(currentItem.type == ConnectApi.FeedItemType.CreateRecordEvent) {
                isRichText = false;
                currentBody = Label.Created;
            }
            likeId = e.capabilities.chatterLikes.myLike == null ? '' : e.capabilities.chatterLikes.myLike.id;
            currentWrapper = new FeedItemWrapper(
                    e.id,
                    ((ConnectApi.UserSummary)currentItem.actor).displayName,
                    ((ConnectApi.UserSummary)currentItem.actor).photo.smallPhotoUrl,
                    e.createdDate,
                    currentBody,
                    e.capabilities.chatterLikes.page.total,
                    likeId,
                    e.capabilities.chatterLikes.isLikedByCurrentUser,
                    isRichText,
                    e.capabilities.comments.page.total
            );//String recordId, String userName, String smallPhotoUrl, Datetime createdDate, String feedBody, Integer likeCount, Boolean isLiked
            result.add(currentWrapper);
            if(e.capabilities.files != null) {
                addedAttachment(currentWrapper, e.capabilities.files.items[0]);
            }
            if(e.capabilities.enhancedLink != null) {
                preparedLink(currentWrapper, e.capabilities.enhancedLink);
            }
            if(e.capabilities.link != null) {
                preparedLink(currentWrapper, e.capabilities.link);
            }
            for(ConnectApi.Comment  cap : e.capabilities.comments.page.items) {
                likeId = cap.myLike == null ? '' : cap.myLike.id;
                currentComment = new FeedItemWrapper(
                        cap.id,
                        cap.user.displayName,
                        cap.user.photo.smallPhotoUrl,
                        cap.createdDate,
                        cap.body.text,
                        cap.capabilities.upDownVote.upVoteCount.intValue(),
                        likeId,
                        cap.myLike != null,
                        false,
                        null
                );
                if(cap.capabilities.content != null) {
                    addedAttachment(currentComment, cap.capabilities.content);
                }
                currentWrapper.feedCommentList.add(currentComment);
            }
        }
        ChatterPage  currentPage = new ChatterPage(result, fep.nextPageToken);
        return currentPage;
    }

    private static void addedAttachment(FeedItemWrapper wrapper, ConnectApi.Content cont) {
        wrapper.isImage = wrapper.imageTypesFeed.contains(cont.fileExtension);
        wrapper.relatedRecordId = cont.versionId;
        wrapper.downloadURL = '/sfc/servlet.shepherd/version/download/' + cont.versionId;
        if (cont.fileSize != null && cont.title != null) {
            wrapper.fileSize = Integer.valueOf(cont.fileSize);
            wrapper.fileName = cont.title;
        }
    }

    private static void addedAttachment(FeedItemWrapper wrapper, ConnectApi.ContentCapability cont) {
        wrapper.isImage = wrapper.imageTypesFeed.contains(cont.fileExtension);
        wrapper.relatedRecordId = cont.versionId;
        wrapper.downloadURL = '/sfc/servlet.shepherd/version/download/' + cont.versionId;
        wrapper.fileSize = Integer.valueOf(cont.fileSize);
        wrapper.fileName = cont.title;
    }

    private static void preparedLink(FeedItemWrapper wrapper, ConnectApi.EnhancedLinkCapability capability) {
        wrapper.linkUrl = capability.linkUrl;
		wrapper.linkTitle = capability.title;
        //wrapper.feedBody = capability.title;
    }

    private static void preparedLink(FeedItemWrapper wrapper, ConnectApi.LinkCapability link) {
        wrapper.linkUrl = link.url;
		wrapper.linkTitle = link.urlName;
        //wrapper.feedBody = link.urlName;
    }

    private static String preparedBody(ConnectApi.FeedBody fb) {
        String result = fb.text;
        return  result;
    }

    public static ChatterPage getNextPage(Id caseId, String nextPageToken) {
        ChatterPage result = getCaseChatter(caseId, nextPageToken);
        result.feedItemList.sort();
        return result;
    }

    public static ChatterPage getAllFeedItems(Id caseId) {
        ChatterPage result = getCaseChatter(caseId);
        result.feedItemList.sort();
        return result;
    }

    public static List<FeedItemWrapper> getAllCommentsByFeedItem(String caseFeedId) {
        List<FeedItemWrapper> listOfInfo = new List<FeedItemWrapper>();
        listOfInfo.addAll(getComments(caseFeedId));
        return listOfInfo;
    }

    public static FeedItemWrapper addNewFeedItem(Id recordId, String text, String contentVersionId) {
        if(recordId.getSobjectType() == FeedItem.getSObjectType()) {
            return createFeedComment(recordId, text, contentVersionId);
        }
        return createFeedItem(recordId, text, contentVersionId);
    }

    private static FeedItemWrapper createFeedItem(Id recordId, String text, String contentVersionId) {
        Boolean removeBody = false;
        List<String> fieldsToCheck = new List<String>();
        if ((text == null || text.trim() == '') && (contentVersionId != null && contentVersionId.trim() != '')) {
            removeBody = true;
            text = 'temp text';
        }
        FeedItem newComment = new FeedItem(
                ParentId = recordId,
                Body = text,
                Visibility = 'AllUsers' // MDF 5/12/17 - Uncommented so Customer Community users see their own posts.
                //NetworkScope = 'AllNetworks'
        );
        ESAPI.securityUtils().validatedInsert(new List<FeedItem>{newComment});

        if (contentVersionId != null && contentVersionId != '') {
            fieldsToCheck.add('Title');
            if(ESAPI.securityUtils().isAuthorizedToView('ContentVersion',fieldsToCheck)){
                ContentVersion cv = [
                        SELECT  Title
                        FROM    ContentVersion
                        WHERE   Id =: contentVersionId
                ];

                FeedAttachment f = new FeedAttachment();
                f.FeedEntityId = newComment.Id;
                f.RecordId = contentVersionId;
                f.Title = cv.Title;
                f.Type = 'CONTENT';
                ESAPI.securityUtils().validatedInsert(new List<FeedAttachment>{f});
            }
        }

        List<String> fieldsToFeedItemCheck =  new List<String>{ 'Id',
                'Body',
                'CreatedById',
                'CreatedDate',
                'IsRichText',
                'RelatedRecordId'
        };

        if(ESAPI.securityUtils().isAuthorizedToView('FeedItem',fieldsToFeedItemCheck)){
            newComment = [
                    SELECT
                            Id,
                            Body,
                            CreatedBy.Name,
                            //CreatedBy.SmallPhotoUrl,
                            CreatedById,
                            CreatedDate,
                            IsRichText,
                            RelatedRecordId
                    FROM        FeedItem
                    WHERE       Id =: newComment.Id
            ];
        }

        if (removeBody) {
            newComment.Body = null;
            ESAPI.securityUtils().validatedUpdate(new List<FeedItem>{newComment});
        }

        Map<String, User> allUsersInfo = getAllUsersInfo(new Set<String>{newComment.CreatedById});
        Map<String, ContentVersion> allAttachedFileInfo = getAllAttachedFileInfo(new Set<String>{newComment.Id});
        return new FeedItemWrapper(newComment, allUsersInfo.get(newComment.CreatedById).SmallPhotoUrl, allAttachedFileInfo.get(newComment.Id));
    }

    private static FeedItemWrapper createFeedComment(Id feedItemId, String text, String contentVersionId) {
        FeedComment newComment = new FeedComment(
                CommentBody = text,
                FeedItemId = feedItemId,
                RelatedRecordId = contentVersionId
        );
        ESAPI.securityUtils().validatedInsert(new List<FeedComment>{newComment});
        FeedItem cItem = [
                SELECT  Id,
                        (
                                SELECT  Id,
                                        CommentBody,
                                        CreatedBy.Name,
                                        CreatedById,
                                        CreatedDate,
                                        IsRichText,
                                        RelatedRecordId
                                FROM    FeedComments
                                WHERE   Id = :newComment.Id
                        )
                FROM    FeedItem
                WHERE   Id = :newComment.FeedItemId
        ];
        newComment = cItem.FeedComments[0];
        Map<String, User> allUsersInfo = getAllUsersInfo(new Set<String>{newComment.CreatedById});
        Map<String, ContentVersion> allAttachedFileInfo = new Map<String, ContentVersion>(
            [
                    SELECT  Id,
                            ContentSize,
                            FileExtension,
                            Title
                    FROM    ContentVersion
                    WHERE   Id = :newComment.RelatedRecordId
                    LIMIT   50000
            ]
        );
        return new FeedItemWrapper(newComment, allUsersInfo.get(newComment.CreatedById).SmallPhotoUrl, allAttachedFileInfo.get(newComment.RelatedRecordId));
    }

    private static Map<String,ContentVersion> getAllAttachedFileInfo(Set<String> allFeedItemIds) {
        List<String> fieldsToCheck = new List<String>{'Id','FeedEntityId','RecordId'};
        List<FeedAttachment> allFeedAttachments;
        if(ESAPI.securityUtils().isAuthorizedToView('FeedAttachment',fieldsToCheck)){
            allFeedAttachments = [
                    SELECT
                            Id,
                            FeedEntityId,
                            RecordId
                    FROM    FeedAttachment
                    WHERE   FeedEntityId IN: allFeedItemIds
                    Limit   50000
            ];
        }
        Set<String> setOfContentVersionIds = new Set<String>();
        for (FeedAttachment fa : allFeedAttachments) {
            setOfContentVersionIds.add(fa.RecordId);
        }

        Map<String,ContentVersion> tempMapOfContentVersion;
        List<String> fieldsToContentVersionCheck = new List<String>{'Id','ContentSize','FileExtension','Title'};
        if(ESAPI.securityUtils().isAuthorizedToView('ContentVersion', fieldsToContentVersionCheck)){
            tempMapOfContentVersion = new Map<String,ContentVersion>(
            [
                    SELECT
                            Id,
                            ContentSize,
                            FileExtension,
                            Title
                    FROM    ContentVersion
                    WHERE   Id IN: setOfContentVersionIds
                    Limit   50000
            ]
            );
        }
        Map<String,ContentVersion> allAttachedFileInfo = new Map<String,ContentVersion>();
        for (FeedAttachment fa : allFeedAttachments) {
            allAttachedFileInfo.put(
                    fa.FeedEntityId,
                    tempMapOfContentVersion.get(fa.RecordId)
            );
        }
        return allAttachedFileInfo;
    }

    private static Map<String,User> getAllUsersInfo(Set<String> allUserIds) {
        Map<String,User> allUsersInfo;
        List<String> fieldsToCheck = new List<String>{'Id','SmallPhotoUrl'};
        if(ESAPI.securityUtils().isAuthorizedToView('User',fieldsToCheck)){
            allUsersInfo = new Map<String,User>([
                    SELECT
                            Id,
                            SmallPhotoUrl
                    FROM    User
                    WHERE   Id IN: allUserIds
                    Limit   50000
            ]);
        }
        return allUsersInfo;
    }

    public class ChatterPage {
        @AuraEnabled
        public List<FeedItemWrapper> feedItemList {get;set;}

        @AuraEnabled
        public String nextPageToken{get;set;}

        public ChatterPage(List<FeedItemWrapper> feedItemList, String nextPageToken) {
            this.feedItemList = feedItemList;
            this.nextPageToken = nextPageToken;
        }
    }

    public class FeedItemWrapper implements Comparable{

        @AuraEnabled
        public sObject item {get;set;}

        @AuraEnabled
        public String recordId {get;set;}

        @AuraEnabled
        public String userName {get;set;}

        @AuraEnabled
        public String feedBody {get;set;}

        @AuraEnabled
        public Boolean IsRichText {get;set;}

        @AuraEnabled
        public String SmallPhotoUrl {get;set;}

        @AuraEnabled
        public ContentVersion attachedFileInfo {get;set;}

        @AuraEnabled
        public String fileName {get;set;}

        @AuraEnabled
        public Integer fileSize {get;set;}

        @AuraEnabled
        public String downloadURL {get;set;}

        @AuraEnabled
        public String relatedRecordId {get;set;}

        @AuraEnabled
        public Boolean isImage {get;set;}

        @AuraEnabled
        public Integer likeCount {get;set;}

        @AuraEnabled
        public String linkUrl {get;set;}

		@AuraEnabled
        public String linkTitle {get;set;}

        @AuraEnabled
        public String likeId {get;set;}

        @AuraEnabled
        public Boolean isLiked {get;set;}

        @AuraEnabled
        public List<FeedItemWrapper> feedCommentList {get;set;}

        @AuraEnabled
        public Datetime createdDate {get;set;}

        @AuraEnabled
        public Integer totalComments {get;set;}

        public FeedItemWrapper(String recordId, String userName, String smallPhotoUrl, Datetime createdDate, String feedBody, Integer likeCount, String likeId, Boolean isLiked, Boolean isRichText, Integer totalComments) {
            this.recordId = recordId;
            this.userName = userName;
            this.createdDate = createdDate;
            this.SmallPhotoUrl = smallPhotoUrl;
            this.feedBody = feedBody;
            this.likeCount = likeCount;
            this.likeId = likeId;
            this.isLiked = isLiked;
            this.isRichText = isRichText;
            this.feedCommentList = new List<FeedItemWrapper>();
            this.totalComments = totalComments;
        }

        public FeedItemWrapper(FeedItem item, String SmallPhotoUrl, ContentVersion attachedFileInfo) {
            this.item = item;
            this.feedBody = item.Body;
            this.userName = item.CreatedBy.Name;
            this.createdDate =  item.CreatedDate;
            this.SmallPhotoUrl = SmallPhotoUrl;
            this.attachedFileInfo = attachedFileInfo;
            isImage = false;
            this.IsRichText = item.IsRichText;
            this.totalComments = 0;
            this.likeCount = 0;
            this.recordId = item.Id;
            this.feedCommentList = new List<FeedItemWrapper>();
            if (attachedFileInfo != null) {
                this.fileName = attachedFileInfo.Title;
                this.fileSize = attachedFileInfo.ContentSize / 1000;
                this.downloadURL = '/sfc/servlet.shepherd/version/download/'+attachedFileInfo.Id;
                if (imageTypesFeed.contains(attachedFileInfo.FileExtension) || Test.isRunningTest())
                    isImage = true;
            }
        }

        public FeedItemWrapper(FeedComment item, String SmallPhotoUrl, ContentVersion attachedFileInfo) {
            this.item = item;
            this.feedBody = item.CommentBody;
            this.userName = item.CreatedBy.Name;
            this.createdDate =  item.CreatedDate;
            this.SmallPhotoUrl = SmallPhotoUrl;
            this.attachedFileInfo = attachedFileInfo;
            isImage = false;
            this.IsRichText = item.IsRichText;
            this.totalComments = 0;
            this.likeCount = 0;
            this.recordId = item.Id;
            this.feedCommentList = new List<FeedItemWrapper>();
            if(item.RelatedRecordId != null) {
                this.relatedRecordId = item.RelatedRecordId;
            }
            if (attachedFileInfo != null) {
                this.fileName = attachedFileInfo.Title;
                this.fileSize = attachedFileInfo.ContentSize / 1000;
                this.downloadURL = '/sfc/servlet.shepherd/version/download/'+attachedFileInfo.Id;
                if (imageTypesFeed.contains(attachedFileInfo.FileExtension) || Test.isRunningTest())
                    isImage = true;
            }
        }

        private Set<String> imageTypesFeed = new Set<String>{
                'png',
                'jpeg',
                'gif',
                'jpg'
        };

        public Integer compareTo(Object compareTo) {
            FeedItemWrapper currentUWrapper = (FeedItemWrapper) compareTo;
            if(this.createdDate != null && currentUWrapper.createdDate == null)
                return -1;
            if(this.createdDate == null && currentUWrapper.createdDate != null)
                return 1;
            if(this.createdDate == null && currentUWrapper.createdDate == null)
                return 0;
            if(this.createdDate > currentUWrapper.createdDate)
                return -1;
            if(this.createdDate < currentUWrapper.createdDate)
                return 1;
            return 0;
        }
    }
}