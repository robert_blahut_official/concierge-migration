@isTest
private class CronUtilityTest {
    testMethod static void testConvertForConcreteCRON() {
        Datetime testTime = Datetime.newInstance(2010, 1, 1, 1, 1, 1);
        String CRON = '0 0 * * *'; //every hours
        System.assertEquals(CronUtility.convertCronToNextDatetime(CRON), null);
        System.assertEquals(CronUtility.convertCronToNextDatetime(CRON, null), null);
        CRON = '0 0 * * * *'; //every hours
        System.assertNotEquals(CronUtility.convertCronToNextDatetime(CRON), null);
        Datetime valueByUtil = CronUtility.convertCronToNextDatetime(CRON, testTime);
        System.assertEquals(2, valueByUtil.hour());
        System.assertEquals(0, valueByUtil.minute());
        System.assert(testTime.isSameDay(valueByUtil));

        CRON = '0 1 * * * *'; //every hours
        valueByUtil = CronUtility.convertCronToNextDatetime(CRON, testTime);
        System.assertEquals(2, valueByUtil.hour());
        System.assertEquals(1, valueByUtil.minute());
        System.assert(testTime.isSameDay(valueByUtil));

        CRON = '* * * * * *'; //now
        valueByUtil = CronUtility.convertCronToNextDatetime(CRON, testTime);
        System.assertEquals(1, valueByUtil.hour());
        System.assertEquals(2, valueByUtil.minute());
        System.assert(testTime.isSameDay(valueByUtil));

        CRON = '* * 13 * * *'; //concrete hour
        valueByUtil = CronUtility.convertCronToNextDatetime(CRON, testTime);
        System.assertEquals(13, valueByUtil.hour());
        System.assertEquals(1, valueByUtil.minute());
        System.assert(testTime.isSameDay(valueByUtil));

        CRON = '* * * 13 * *'; //concrete day
        valueByUtil = CronUtility.convertCronToNextDatetime(CRON, testTime);
        System.assertEquals(13, valueByUtil.day());
        System.assertEquals(0, valueByUtil.hour());
        System.assertEquals(0, valueByUtil.minute());
        System.assert(!testTime.isSameDay(valueByUtil));


        CRON = '* * * * 2 *'; //concrete month
        valueByUtil = CronUtility.convertCronToNextDatetime(CRON, testTime);
        System.assertEquals('Feb', valueByUtil.format(CronUtility.MONTH_DATETIME_FORMAT));
        System.assertEquals(1, valueByUtil.day());
        System.assertEquals(0, valueByUtil.hour());
        System.assertEquals(0, valueByUtil.minute());
        System.assert(!testTime.isSameDay(valueByUtil));

        CRON = '* * * * * 1'; //concrete Day of week
        valueByUtil = CronUtility.convertCronToNextDatetime(CRON, testTime);
        System.assertEquals('Sun', valueByUtil.format(CronUtility.DAY_DATETIME_FORMAT));
        System.assertEquals(0, valueByUtil.hour());
        System.assertEquals(0, valueByUtil.minute());
        System.assert(!testTime.isSameDay(valueByUtil));
    }

    testMethod static void testConvertForRangeCRON() {
        Datetime testTime = Datetime.newInstance(2010, 1, 1, 1, 1, 1);
        String CRON = '* * * 6-9 * *';
        Datetime valueByUtil = CronUtility.convertCronToNextDatetime(CRON, testTime);
        System.assertEquals(6, valueByUtil.day());
        System.assertEquals(0, valueByUtil.minute());

        CRON = '* * * * 6-9 *';
        valueByUtil = CronUtility.convertCronToNextDatetime(CRON, testTime);
        System.assertEquals('Jun', valueByUtil.format(CronUtility.MONTH_DATETIME_FORMAT));
        System.assertEquals(0, valueByUtil.minute());

        CRON = '* * * * * 5-7';
        valueByUtil = CronUtility.convertCronToNextDatetime(CRON, testTime);
        System.assertEquals('Fri', valueByUtil.format(CronUtility.DAY_DATETIME_FORMAT));
        System.assertEquals(2, valueByUtil.minute());
        System.assert(testTime.isSameDay(valueByUtil));
    }

    testMethod static void testConvertForDelimiters() {
        Datetime testTime = Datetime.newInstance(2010, 1, 1, 1, 1, 1);
        String CRON = '* * * 6,9 * *';
        Datetime valueByUtil = CronUtility.convertCronToNextDatetime(CRON, testTime);
        System.assertEquals(6, valueByUtil.day());
        System.assertEquals(0, valueByUtil.minute());

        CRON = '* * * * 6,9 *';
        valueByUtil = CronUtility.convertCronToNextDatetime(CRON, testTime);
        System.assertEquals('Jun', valueByUtil.format(CronUtility.MONTH_DATETIME_FORMAT));
        System.assertEquals(0, valueByUtil.minute());

        CRON = '* * * * * 5,7';
        valueByUtil = CronUtility.convertCronToNextDatetime(CRON, testTime);
        System.assertEquals('Sat', valueByUtil.format(CronUtility.DAY_DATETIME_FORMAT));
        System.assertEquals(0, valueByUtil.minute());
        System.assert(!testTime.isSameDay(valueByUtil));
    }

    testMethod static void testConvertForIncrement() {
        Datetime testTime = Datetime.newInstance(2010, 1, 1, 1, 1, 1);
        String CRON = '* 1/5 * * * *';
        Datetime valueByUtil = CronUtility.convertCronToNextDatetime(CRON, testTime);
        System.assertEquals(1, valueByUtil.hour());
        System.assertEquals(5, valueByUtil.minute());

        CRON = '* * 1/5 * * *';
        valueByUtil = CronUtility.convertCronToNextDatetime(CRON, testTime);
        System.assertEquals(5, valueByUtil.hour());

        CRON = '* * * 1/5 * *';
        valueByUtil = CronUtility.convertCronToNextDatetime(CRON, testTime);
        System.assertEquals(5, valueByUtil.day());
        System.assertEquals(0, valueByUtil.minute());

        CRON = '* * * * 1/5 *';
        valueByUtil = CronUtility.convertCronToNextDatetime(CRON, testTime);
        System.assertEquals('May', valueByUtil.format(CronUtility.MONTH_DATETIME_FORMAT));
        System.assertEquals(0, valueByUtil.minute());

        CRON = '* * * * * 1/5';
        valueByUtil = CronUtility.convertCronToNextDatetime(CRON, testTime);
        System.assertEquals('Thu', valueByUtil.format(CronUtility.DAY_DATETIME_FORMAT));
        System.assertEquals(0, valueByUtil.minute());
        System.assert(!testTime.isSameDay(valueByUtil));
    }

    testMethod static void testGetSchedulerSettings() {
        List<SchedulerSettings__c> schedSettingList = new List<SchedulerSettings__c>{
                new SchedulerSettings__c(
                        Name = TurboEncabulatorScheduler.TURBO_BATCH_NAME,
                        Class_Name__c = TurboEncabulatorScheduler.TURBO_BATCH_NAME,
                        CRON__c = TurboEncabulatorScheduler.TURBO_CRON,
                        Batch_Size__c = TurboEncabulatorScheduler.TURBO_BATCH_SIZE,
                        Description__c = TurboEncabulatorScheduler.TURBO_DESCRIPTION,
                        Active__c = true
                ),
                new SchedulerSettings__c(
                        Name = TurboEncabulatorScheduler.REVIEW_BATCH_NAME,
                        Class_Name__c = TurboEncabulatorScheduler.REVIEW_BATCH_NAME,
                        CRON__c = TurboEncabulatorScheduler.REVIEW_CRON,
                        Batch_Size__c = TurboEncabulatorScheduler.REVIEW_BATH_SIZE,
                        Description__c = TurboEncabulatorScheduler.REVIEW_DESCRIPTION,
                        Active__c = false
                )
        };
        Database.insert(schedSettingList, false);
        List<SettingsPanelController.ScheduleWrapper> wList = SettingsPanelController.getSchedulerSettings();
        System.assertNotEquals(wList, null);
        SettingsPanelController.updateSchedulerSettings(JSON.serialize(wList));
        System.assertNotEquals(SettingsPanelController.parseCron('* * * * * 1/5'), null);
        System.assertNotEquals(SettingsPanelController.parseCron('* 1/6 1-3 * * 1/5'), null);
    }
}