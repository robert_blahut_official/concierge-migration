@isTest
public class ChatterDataAccessUtilsTest {
    testMethod static void testCreateComment() {
        Case c = new Case();
        c.Subject		= 'test';
        c.Description	= 'test';
        c.Status		= 'New';
        c.Origin		= 'Web';
        insert c;
        FeedItem feedRecord = new FeedItem(
                ParentId	= c.Id,
                Body		= 'Test',
                Title		= 'Test',
                Visibility  = 'AllUsers'
        );
        insert feedRecord;
        System.assert(feedRecord.Id != null);
        ConnectApi.FeedElementPage testPage = new ConnectApi.FeedElementPage();
        List<ConnectApi.FeedItem> testItemList = new List<ConnectApi.FeedItem>();
        ConnectApi.FeedItem currentItem = generateFeedItem(feedRecord.Id);
        currentItem.type = ConnectApi.FeedItemType.CreateRecordEvent;
        testItemList.add(currentItem);
        currentItem = generateFeedItem(feedRecord.Id);
        currentItem.body.isRichText = true;
        testItemList.add(currentItem);
        testPage.elements = testItemList;

        // Set the test data
        ConnectApi.ChatterFeeds.setTestGetFeedElementsFromFeed(Network.getNetworkId(),  ConnectApi.FeedType.Record, (String)c.Id, null, ChatterDataAccessUtils.RECENT_COMMENT_COUNT, null, testPage);
        System.assertNotEquals(WorkWithCases.getAllFeedItems(c.Id), null);
        System.assertNotEquals(WorkWithCases.getAllCommentsByFeedItem(feedRecord.Id), null);
    }

    testMethod static void testLike() {
        Case c = new Case();
        c.Subject		= 'test';
        c.Description	= 'test';
        c.Status		= 'New';
        c.Origin		= 'Web';
        insert c;
        FeedItem feedRecord = new FeedItem(
                ParentId	= c.Id,
                Body		= 'Test',
                Title		= 'Test',
                Visibility  = 'AllUsers'
        );
        insert feedRecord;

        FeedLike newLike = ChatterDataAccessUtils.insertLike(feedRecord.Id, feedRecord.Id);
        System.assertNotEquals(newLike, null);
        ChatterDataAccessUtils.deleteLike(newLike.Id);
        List<FeedItem> items = [SELECT (SELECT Id FROM FeedLikes) FROM FeedItem];
        System.assertEquals(items[0].FeedLikes.isEmpty(), true);
    }

    public static ConnectApi.FeedItem generateFeedItem(String parentId) {

        ConnectApi.FeedItem feedItemInput = new ConnectApi.FeedItem();
        ConnectApi.FeedBody messageBodyInput = new ConnectApi.FeedBody();
        messageBodyInput.messageSegments = new 	List<ConnectApi.MessageSegment>();
        ConnectApi.TextSegment textSegmentInput = new ConnectApi.TextSegment();
        textSegmentInput.text = 'Something';
        messageBodyInput.messageSegments.add(textSegmentInput);
        feedItemInput.body = messageBodyInput;
        feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
        feedItemInput.id = parentId;

        feedItemInput.capabilities = new ConnectApi.FeedElementCapabilities();
        feedItemInput.capabilities.chatterLikes = new ConnectApi.ChatterLikesCapability();
        feedItemInput.capabilities.chatterLikes.page = new ConnectApi.ChatterLikePage();

        feedItemInput.capabilities.enhancedLink = new ConnectApi.EnhancedLinkCapability();
        feedItemInput.capabilities.link = new ConnectApi.LinkCapability();


        feedItemInput.capabilities.files = new ConnectApi.FilesCapability();
        feedItemInput.capabilities.files.items = new List<ConnectApi.Content>();
        ConnectApi.Content fIC = new ConnectApi.Content();
        fIC.fileSize = '123';
        feedItemInput.capabilities.files.items.add(fIC);

        feedItemInput.capabilities.comments = new ConnectApi.CommentsCapability();
        feedItemInput.capabilities.comments.page = new ConnectApi.CommentPage();
        feedItemInput.capabilities.comments.page.items = new List<ConnectApi.Comment>();

        ConnectApi.Comment com = new ConnectApi.Comment();
        com.user = new ConnectApi.UserSummary();
        com.body = messageBodyInput;
        com.capabilities = new ConnectApi.CommentCapabilities();
        com.capabilities.upDownVote = new ConnectApi.UpDownVoteCapability();
        com.capabilities.upDownVote.upVoteCount = 0;

        com.capabilities.content = new ConnectApi.ContentCapability();
        com.capabilities.content.fileSize = '123';

        feedItemInput.capabilities.comments.page.items.add(com);
        ((ConnectApi.User)com.user).photo = new ConnectApi.Photo();

        feedItemInput.actor = new ConnectApi.UserSummary();
        ((ConnectApi.User)feedItemInput.actor).photo = new ConnectApi.Photo();
        return feedItemInput;
    }

    testMethod static void testNewComment() {
        Case c = new Case();
        c.Subject		= 'test';
        c.Description	= 'test';
        c.Status		= 'New';
        c.Origin		= 'Web';
        insert c;

        Attachment a = new Attachment(
                Name = 'Test attachment',
                ParentId = c.Id,
                Body = Blob.valueOf('test')
        );
        insert a;
        String fileId = WorkWithCases.saveTheChunk(c.Id, 'test name', EncodingUtil.base64Encode(Blob.valueOf('test')), 'test', '', true);
        System.assert(fileId == WorkWithCases.saveTheChunk(c.Id, 'test name', EncodingUtil.base64Encode(Blob.valueOf('test')), 'test', fileId, true));

        fileId = WorkWithCases.saveTheChunk(null, 'test name', EncodingUtil.base64Encode(Blob.valueOf('test')), 'test', '', false);
        System.assert(fileId == WorkWithCases.saveTheChunk(null, 'test name', EncodingUtil.base64Encode(Blob.valueOf('test')), 'test', fileId, false));

        WorkWithCases.addNewFeedItem(c.Id,'test message',fileId);
        WorkWithCases.addNewFeedItem(c.Id,fileId);
        WorkWithCases.addNewFeedItem(c.Id,'',fileId);
        List<FeedItem> feedItemList = [SELECT Id FROM FeedItem];
        System.assertEquals(feedItemList.size(), 3);

        WorkWithCases.addNewFeedItem(feedItemList[0].Id,'test message',fileId);
        WorkWithCases.addNewFeedItem(feedItemList[1].Id,'',fileId);
        Integer countFeedComment = [SELECT COUNT() FROM FeedComment];
        System.assertEquals(countFeedComment, 2);
    }

	testmethod static void testGetAllFeedItems(){
	 Case c = new Case();
        c.Subject		= 'test';
        c.Description	= 'test';
        c.Status		= 'New';
        c.Origin		= 'Web';
        insert c;

		List<FeedItem> feedList = new List<FeedItem>();
		for(Integer i=0; i<3; i++){
			feedList.add(new FeedItem(ParentId	= c.Id,
									  Body		= 'Test'+i,
									  Title		= 'Test'+i,
									  Visibility  = 'AllUsers'));
		}
		insert feedList;
		List<ConnectApi.FeedElement> elList = new List<ConnectApi.FeedElement>();
		for(FeedItem fitem : feedList){
		elList.add(ChatterDataAccessUtilsTest.generateFeedItem(fitem.Id));
		}
		elList[0].body.isRichText = true;
		 ConnectApi.FeedElementPage testPage = new ConnectApi.FeedElementPage();
		 testPage.elements = elList;

        ConnectApi.ChatterFeeds.setTestGetFeedElementsFromFeed(Network.getNetworkId(),  ConnectApi.FeedType.Record, (String)c.Id, null, ChatterDataAccessUtils.RECENT_COMMENT_COUNT, null, testPage);

        Test.startTest();
        ChatterDataAccessUtils.ChatterPage fiwl = ChatterDataAccessUtils.getAllFeedItems(c.Id);
        ChatterDataAccessUtils.ChatterPage page = WorkWithCases.getNextChatterPage(c.Id, null);
		Test.stopTest();
        System.assertNotEquals(null, fiwl);
	}

	testmethod static void testCompareTo(){
		ChatterDataAccessUtils.FeedItemWrapper feedItemWrapper = new ChatterDataAccessUtils.FeedItemWrapper(null,null,null,Datetime.now(),null,null,null,false,false,0);
		ChatterDataAccessUtils.FeedItemWrapper feedItemWrapper2 = new ChatterDataAccessUtils.FeedItemWrapper(null,null,null,null,null,null,null,false,false,0);
		ChatterDataAccessUtils.FeedItemWrapper feedItemWrapper3 = new ChatterDataAccessUtils.FeedItemWrapper(null,null,null,Datetime.now().addDays(1),null,null,null,false,false,0);
		System.assertEquals(-1,feedItemWrapper.compareTo(feedItemWrapper2));
		System.assertEquals(1,feedItemWrapper2.compareTo(feedItemWrapper));
		System.assertEquals(0,feedItemWrapper2.compareTo(feedItemWrapper2));
		System.assertEquals(-1,feedItemWrapper3.compareTo(feedItemWrapper));
		System.assertEquals(1,feedItemWrapper.compareTo(feedItemWrapper3));
		System.assertEquals(0,feedItemWrapper.compareTo(feedItemWrapper));
	}
}