@isTest
public class StandAloneLightningEditionControllerTest {

	@testSetup static void setup() {
		TestClassUtility.createSearchEngineConfig();

		String objectType = WorkWithArticlesTest.getKavObjectName();
		List<cncrg__Search_Types_Settings__c> settings = new List<cncrg__Search_Types_Settings__c> {
			new cncrg__Search_Types_Settings__c(
				Name = objectType,
				cncrg__Article_Type_API_Name__c = objectType,
				cncrg__Active__c = true,
				cncrg__Fields_To_Display_1__c = 'Summary,Title'
			)
		};
		ESAPI.securityUtils().validatedInsert(settings);
		System.assertEquals(1, cncrg__Search_Types_Settings__c.getAll().size());

		List<cncrg__Concierge_Settings__c> cs = new List<cncrg__Concierge_Settings__c> {
			new cncrg__Concierge_Settings__c(
				Name = 'Background file name for App',
				cncrg__Value__c = 'test'
			),
			new cncrg__Concierge_Settings__c(
				Name = 'Application Name',
				cncrg__Value__c = 'Concierge'
			),
			new cncrg__Concierge_Settings__c(
				Name = 'Number of Articles Displayed in Search',
				cncrg__Value__c = '5'
			),
			new cncrg__Concierge_Settings__c(
				Name = 'Background For App on Mobile Devices',
				cncrg__Value__c = '6'
			),
			new cncrg__Concierge_Settings__c(
				Name = 'Hours New/Updated Flag is Displayed',
				cncrg__Value__c = '24'
			),
			new cncrg__Concierge_Settings__c(
				Name = 'Search Log Email Address',
				cncrg__Value__c = 'test@test.com'
			),
			new cncrg__Concierge_Settings__c(
				Name = 'Lightning Knowledge Object Name',
				cncrg__Value__c = objectType
			)
		};
		ESAPI.securityUtils().validatedInsert(cs);
		System.assertEquals(7, cncrg__Concierge_Settings__c.getAll().size());

		Schema.DescribeSObjectResult sObjectDescribe = TestClassUtility.getSobjectDescribe(objectType);
		sObject obj = sObjectDescribe.getSobjectType().newSObject();
		obj.put('Title', 'Unit Test 1 test');
		obj.put('Language', Utils.getUserLanguage());
		obj.put('UrlName', 'test');
		obj.put('Summary', 'test test test test');

		sObject objUpdated = sObjectDescribe.getSobjectType().newSObject();
		objUpdated.put('Title', 'Unit Test 2 test');
		objUpdated.put('Language', Utils.getUserLanguage());
		objUpdated.put('UrlName', 'test2');
		objUpdated.put('Summary', 'test2 test2 test2 test2');

		List<sObject> articles = new List<sObject> {obj, objUpdated};
		ESAPI.securityUtils().validatedInsert(articles);

		List<Id> listOfIds = new List<Id>();
		for (sObject article : articles) {
			listOfIds.add((String) article.get('Id'));
		}
		articles = Database.query('SELECT KnowledgeArticleId FROM ' + objectType + ' WHERE Id IN: listOfIds');

		KbManagement.PublishingService.publishArticle((Id) articles[0].get('KnowledgeArticleId'), true);
		KbManagement.PublishingService.publishArticle((Id) articles[1].get('KnowledgeArticleId'), true);
	}

	@isTest
	public static void testController() {
		String objectType = WorkWithArticlesTest.getKavObjectName();
		List<sObject> records = Database.query('SELECT Id, KnowledgeArticleId FROM ' + objectType);

		StandAloneLightningEditionController controller = new StandAloneLightningEditionController();

		Test.setFixedSearchResults(new List<Id>(new Map<Id, sObject>(records).keySet()));
		Test.startTest();
		List<SearchResultsWrapper> results = controller.getSearchResults('test', '');
		System.assert(results.size() > 0);

		//results.get(0).isLike = true;
		//System.assertEquals('', controller.rateItem(results.get(0)));
		System.assertEquals(null, controller.toggleFavoriteAPEX(results.get(0).recordId, false));
		System.assert([SELECT COUNT() FROM cncrg__Favorite_article__c] > 0);
		List<Object> favoriteArticles = controller.getFavoritesbyId(new List<String>{results.get(0).KnowledgeArticleId}, '');
		System.assert(favoriteArticles.size() > 0);

		System.assert(controller.getRequiredFieldsForSearch().size() > 0);
		Test.stopTest();
	}

	@isTest
	public static void testGetUpdateIndicator() {
		Test.startTest();
		StandAloneLightningEditionController controller = new StandAloneLightningEditionController();
		String objectType = WorkWithArticlesTest.getKavObjectName();
		Schema.DescribeSObjectResult sObjectDescribe = TestClassUtility.getSobjectDescribe(objectType);
		List<sObject> article = Database.query('SELECT Id,KnowledgeArticleId,FirstPublishedDate,LastPublishedDate FROM ' + objectType + ' WHERE PublishStatus = \'Online\'');

		KbManagement.PublishingService.editOnlineArticle((Id) article[1].get('KnowledgeArticleId'), true);
		List<sObject> draftArticle = Database.query('SELECT Id,KnowledgeArticleId,FirstPublishedDate,LastPublishedDate FROM ' + objectType + ' WHERE PublishStatus = \'Draft\'');
		draftArticle[0].put('Summary', 'updateTest');
		update draftArticle[0];

		KbManagement.PublishingService.publishArticle((Id) draftArticle[0].get('KnowledgeArticleId'), true);
		List<sObject> newArticle = Database.query('SELECT Id,KnowledgeArticleId,FirstPublishedDate,LastPublishedDate FROM ' + objectType + ' WHERE PublishStatus = \'Online\' AND Summary = \'updateTest\'');
		System.assertEquals('new', controller.getUpdateIndicator(article[0], 24));
		Test.stopTest();
	}
}