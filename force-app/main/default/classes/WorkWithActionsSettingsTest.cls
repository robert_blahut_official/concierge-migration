@isTest
public class WorkWithActionsSettingsTest {

	static testMethod void testGetFavoriteArticlesWrapperList() {
		TestClassUtility.createSearchEngineConfig();
		WorkWithActionsSettings.isTest = true;
		sObject dataCategoryObject = WorkWithActionsSettingsTest.getDataCategorySelectionRecord();
		List<WorkWithActionsSettings.InfoWrapper> result = new List<WorkWithActionsSettings.InfoWrapper>();

		if (dataCategoryObject != null) {
			String dataCategoryName = WorkWithActionsSettingsTest.getDataCategoryObjectName();

			result = WorkWithActionsSettings.getAllArticleCategories('recordId', dataCategoryName);
			System.assert(!result.isEmpty());
		} else {
			System.assert(result.isEmpty());
		}

	}

	static testMethod void testCreateNewSetting() {
		cncrg__Action_Setting__c setting = new cncrg__Action_Setting__c(
			cncrg__ArticleId__c = 'ArticleId',
			cncrg__Source_System__c = 'salesforce'
		);
		insert setting;
		cncrg__Action_Setting__c result = WorkWithActionsSettings.createNewSetting(
			JSON.serialize(new List<cncrg__Action_Setting__c>{
				setting,
				new cncrg__Action_Setting__c(
					cncrg__ArticleId__c = 'ArticleId',
					cncrg__Source_System__c = 'salesforce'
				)
			})
		);
		System.assertEquals('', result.cncrg__Category__c);
		System.assertEquals('', result.cncrg__Type__c);
	}

	public static sObject getDataCategorySelectionRecord() {
		System.assert(ESAPI.securityUtils().isAuthorizedToView('KnowledgeArticleVersion', new List<String> {'Id'}));
		for (Schema.SObjectType o : Schema.getGlobalDescribe().values()) {
			Schema.DescribeSObjectResult objResult = o.getDescribe();
			if (objResult.getName().contains('DataCategorySelection')) {
				String objectName = objResult.getName();
				String dataCategorySelectionJSON = '{"attributes":{"type":"' + objectName + '"},"DataCategoryName":"Name of category"}';
				Type typeOfObject = Type.forName(objectName);
				sObject dataCategoryObject = (sObject)JSON.deserialize(dataCategorySelectionJSON, typeOfObject);
				return dataCategoryObject;
			}
		}
		return null;
	}

	public static String getDataCategoryObjectName() {
		System.assert(ESAPI.securityUtils().isAuthorizedToView('KnowledgeArticleVersion', new List<String> {'Id'}));
		for (Schema.SObjectType o : Schema.getGlobalDescribe().values()) {
			Schema.DescribeSObjectResult objResult = o.getDescribe();
			if (objResult.getName().contains('DataCategorySelection')) {
				String objectName = objResult.getName().replace('DataCategorySelection', 'kav');
				return objectName;
			}

		}
		return null;
	}
}