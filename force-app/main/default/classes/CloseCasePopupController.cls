public with sharing class CloseCasePopupController {
	public static final String DEFAULT_FIELD_SET = 'cncrg__Case_Close';
	public static final String CASE_CLOSE_SETTING_NAME = 'Case Close Status';
	public static final String CLOSE_CASE_VISIBILITY_SETTING_NAME = 'Enable users to close their own case';

	@AuraEnabled
	public static List<Object> getInitData(String recordId) {
		List<Object> initDataList = new List<Object>();
		if (!checkUserAccess(recordId)) {
			return null;
		}

		initDataList.add(getRecordWithFields(recordId));
		return initDataList;
	}

	@AuraEnabled
	public static List<FieldSetController.FieldSetMember> getRecordWithFields(String recordId) {
		if (!checkUserAccess(recordId)) {
			return null;
		}

		String closedStatusValue = Utils.getValueFromConciergeSettings(CASE_CLOSE_SETTING_NAME);
		closedStatusValue = String.isBlank(closedStatusValue) ? 'Closed' : closedStatusValue;

		List<Schema.FieldSetMember> fields = Utils.getSchemaFieldSetMembers(WorkWithCases.CASE_OBJECT_API, DEFAULT_FIELD_SET);
		SObject currentRecord = getCaseRecord(recordId, fields);
		List<FieldSetController.FieldSetMember> fieldsList = FieldSetController.getFields(WorkWithCases.CASE_OBJECT_API, DEFAULT_FIELD_SET);

		for (FieldSetController.FieldSetMember member: fieldsList) {
			member.type = member.type == 'DOUBLE' ? 'NUBMER' : member.type;
			member.type = member.type.toLowerCase();

			if (member.fieldPath == 'Status') {
				member.value = closedStatusValue;
			}

			if (member.fieldPath != 'Status' && currentRecord != null) {
				if (member.fieldPath.contains('.')) {
					List<String> sObjects = member.fieldPath.split('\\.');
					if (sObjects.size() == 2) {
						member.value = currentRecord.getSObject(sObjects[0]).get(sObjects[1]);
					}
				} else {
					member.value = currentRecord.get(member.fieldPath);
				}
			}
		}
		return fieldsList;
	}

	@AuraEnabled
	public static Id saveSObject(String serializedRecord) {
		Case newRecord = (Case) JSON.deserialize(serializedRecord, Case.class);
		try{
			ESAPI.securityUtils().validatedUpdate(new List<Case>{newRecord});
		} catch (DmlException e) {
			String errorMessage = '';
			for (Integer var = 0; var < e.getNumDml(); var++) {
				errorMessage += e.getDmlMessage(var) + '\n';
			}
			throw new AuraHandledException(errorMessage);
		}
		return newRecord.Id;
	}

	private static Boolean checkUserAccess(String recordId) {
		String closeButtonVisibility = Utils.getValueFromConciergeSettings(CLOSE_CASE_VISIBILITY_SETTING_NAME);
		Boolean closeButtonPermission = String.isBlank(closeButtonVisibility) ? false : Boolean.valueOf(closeButtonVisibility);

		List<UserRecordAccess> recordAccessList = [
			SELECT RecordId, HasEditAccess
			FROM UserRecordAccess
			WHERE UserId = :UserInfo.getUserId() AND RecordId = :recordId
		];
		return closeButtonPermission && (recordAccessList.size() > 0 && recordAccessList[0].HasEditAccess);
	}

	private static SObject getCaseRecord(String recordId, List<Schema.FieldSetMember> fields) {
		Set<String> allFields = new Set<String>();
		allFields.add('Id');
		allFields.add('Status');

		for (Schema.FieldSetMember member: fields) {
			allFields.add(member.getFieldPath());
		}

		String query = 'SELECT ' + String.join((Iterable<String>)allFields, ',');
		query += ' FROM Case '; 
		query += ' WHERE Id=\'' + String.escapeSingleQuotes(recordId) + '\'';

		return Database.query(query);
	}
}