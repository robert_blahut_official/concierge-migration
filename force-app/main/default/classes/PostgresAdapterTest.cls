@isTest
private class PostgresAdapterTest {


    @isTest static void getClassicKnowledgeSuggestionsForQueryTest() {
        init();

        String mockBody = '{"results": {' +
                '"sqlstatement": "SELECT id, product_name FROM products WHERE to_tsvector(\u0027english\u0027,coalesce(description,\u0027\u0027) || \u0027 \u0027 || coalesce(product_name,\u0027\u0027)) @@ plainto_tsquery(\u0027need to connect\u0027)",' +
                '"records": [' +
                '{ "id": 33,' +
                '"name": "New Article"}],' +
                '"{!javatimemls}": 1560},' +
                '"{!user}": "name",' +
                '"error": false}';

        Map<String, String> headers = new Map<String, String>();
        headers.put('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(200, 'OK', mockBody, headers));
        String query = 'Not empty query';

        Test.startTest();
        PostgresAdapter adapter = new PostgresAdapter();
        PostgresAdapter.isTest = true;
        List<SearchPhraseWrapper> results = adapter.getSuggestionsForQuery(query);
        Test.stopTest();

        System.assert(!results.isEmpty());
    }

    @isTest static void getClassicKnowledgeSearchResultsTest() {

        init();
        String articleId = '12345';
        String mockBody = '{"result": {' +
                '"javatimemls": 1,' +
                '"records": [' +
                '{ "id": '+ articleId + ',' +
                '"text": "New Article", "rank":1}],' +
                '"error_message": 1560},' +
                '"error": false}';

        Map<String, String> headers = new Map<String, String>();
        headers.put('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(200, 'OK', mockBody, headers));
		WorkWithArticles.EncryptionMember member = new WorkWithArticles.EncryptionMember();
        cncrg__Favorite_article__c favoriteArticle = new cncrg__Favorite_article__c(
            cncrg__ArticleId__c = member.encrypteData(articleId)
        );
        insert favoriteArticle;

        String companyLogoName = 'Company Logo Name';
        cncrg__Concierge_Settings__c setting = new cncrg__Concierge_Settings__c(
            Name = 'CompanyLogo',
            cncrg__Value__c = companyLogoName
        );
        insert setting;

        String standardFolderId = [
                                    SELECT Id
                                    FROM   Folder
                                    WHERE  Type = 'Document'
                                    LIMIT  1
        ].Id;

        Document document = new Document(
            Body = Blob.valueOf('Some Text'),
            ContentType = 'application/pdf',
            DeveloperName = 'my_document',
            IsPublic = true,
            Name = companyLogoName,
            FolderId = standardFolderId
        );
        insert document;

        Test.startTest();
            PostgresAdapter adapter = new PostgresAdapter();
            List<SearchResultsWrapper> results = adapter.getSearchResults(null,'');
            List<SearchResultsWrapper> resultsWithPrefix = adapter.getSearchResults(null,'Prefix');

        Test.stopTest();
        System.assert(results.size() == 2);
    }

    @isTest static void testFavoriteArticleObjectWrapper() {
        //coverage for favoriteArticleObject wrapper class
        PostgresAdapter.favoriteArticleObject articleObject = new PostgresAdapter.favoriteArticleObject('Id', 'Title', 'Summary');
        System.assertEquals(articleObject.Id, 'Id');
    }

    public static testMethod void testUpdateArticleBody() {
        Set<String> allInteractiveFields = new Set<String>{'FullPhotoUrl'};
        User currentUser = [
                                SELECT Id,
                                       FullPhotoUrl
                                FROM   User
                                WHERE  Id =: UserInfo.getUserId()
        ];
        String body = '{!User.';
        String result = PostgresAdapter.updateArticleBody(body, currentUser, allInteractiveFields);
        System.assert(String.isNotBlank(result));
    }
    public static testMethod void testToggleFavoriteAPEX() {
        PostgresAdapter adapter = new PostgresAdapter();
        adapter.toggleFavoriteAPEX('recordId', false);
        List<cncrg__Favorite_article__c> falseResult = [
                                                                SELECT Id
                                                                FROM   cncrg__Favorite_article__c
                                                                //WHERE  cncrg__ArticleId__c = 'recordId'
        ];

        adapter.toggleFavoriteAPEX('recordId', true);
        System.assert(falseResult.size() > 0);
    }

    public static testMethod void testGetDocumentsbyId() {
        init();
		PostgresAdapter.isTest = true;
        String mockBody =
        '{' +
          '"result": [' +
            '{' +
              '"id": 1,' +
              '"title": "SALESFORCE SUMMER ’16 RELEASE NOTES SUMMARY",' +
              '"summary": "Salesforce delivers powerful new business intelligence capabilities, more ways to collaborate on deals and projects, and even greater control over your data.",' +
              '"fts": {' +
                '"type": "tsvector",' +
                '"value": "way"' +
              '}' +
            '}' +
          '],' +
          '"error": false' +
        '}';

        Map<String, String> headers = new Map<String, String>();
        headers.put('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(200, 'OK', mockBody, headers));

        PostgresAdapter adapter = new PostgresAdapter();
        List<String> recordIds = new List<String>{'12345'};
        List<Object> result = new List<Object>();
        Test.startTest();
        result = adapter.getFavoritesbyId(recordIds, '');
        Test.stopTest();
        System.assert(result.isEmpty());
    }

    private static void init() {
        cncrg__Web_Service_Settings__c customSetting = new cncrg__Web_Service_Settings__c(Name = 'HerokuWebService');
        customSetting.Password__c = 'testPassword';
        customSetting.URL__c = 'test.heroku.com';
        customSetting.User__c = 'testUser';
        insert customSetting;

        TestClassUtility.createSearchEngineConfig();

		cncrg__Concierge_Settings__c cs = new cncrg__Concierge_Settings__c(
			Name = 'Org Namespace',
			cncrg__Value__c = 'cncrg'
		);
		ESAPI.securityUtils().validatedInsert(new List<cncrg__Concierge_Settings__c>{cs});

		cs = new cncrg__Concierge_Settings__c(
			Name = 'Type to display',
			cncrg__Value__c = 'cncrg__Knowledge__kav'
		);
		ESAPI.securityUtils().validatedInsert(new List<cncrg__Concierge_Settings__c>{cs});

		cs = new cncrg__Concierge_Settings__c(
			Name = 'Field to display',
			cncrg__Value__c = 'Summary'
		);
		ESAPI.securityUtils().validatedInsert(new List<cncrg__Concierge_Settings__c>{cs});
    }
}