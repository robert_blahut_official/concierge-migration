/**
 * DEAR SECURITY REVIEWER!
 * The apex class is not using the “with or without sharing” because the class is referenced in package post install class.
 * Please see details in Salesforce documentation. 
 * https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_install_handler.htm
 * “Post install script can’t call another Apex class in the package if that Apex class uses the with sharing keyword.“
*/
global abstract class AbstractConciergeBatch implements Database.Batchable<sObject> {

	private String jobId;
	private Datetime startProcess;
	private String batchName;
	protected Integer batchSize;

	public void setInitParams(List<Object> params) {
		this.jobId = String.valueOf(params[0]);
		this.startProcess = Datetime.valueOf(params[1]);
		this.batchName = String.valueOf(params[2]);
		this.batchSize = Integer.valueOf(params[3]);
	}

	protected List<Object> getInitParams() {
		return new List<Object>{jobId, startProcess, batchName, batchSize};
	}

	protected String getJobId() {
		return jobId;
	}
	protected Datetime getStartTime() {
		return startProcess;
	}

	private void abortSchedule() {
		if(jobId != null) {
			try {
				System.AbortJob(jobId);
			}
			catch(Exception e) {}
		}
	}

	global virtual void finish(Database.BatchableContext context) {
		abortSchedule();
		finallyExecution(context);
		TurboEncabulatorScheduler.updateSettingsAfterRun(context.getJobId(), startProcess, batchName);
	}

	protected virtual void finallyExecution(Database.BatchableContext context){}
}