public with sharing class HerokuWebService {

	public static JsonDeserializerSimpleResponse createIndex(String indexConfiguration) {

		HttpRequest req = formRequest('/fts-srv/create-index', indexConfiguration, 'POST');
		HttpResponse res = sendRequest(req);

		JsonDeserializerSimpleResponse result = JsonDeserializerSimpleResponse.parse(res.getBody());

		return result;
	}

	public static JsonDeserializerSimpleResponse createTable(String jsonTableConfiguration) {

		HttpRequest req = formRequest('/fts-srv/create-tables', jsonTableConfiguration, 'POST');
		HttpResponse res = sendRequest(req);
		JsonDeserializerSimpleResponse result;

		if(res.getStatusCode() == 200){
			result = JsonDeserializerSimpleResponse.parse(res.getBody());
		} else {
			result = null;
		}

		return result;
	}

	public static JsonDeserializerSimpleResponse updateIndex (String jsonIndexUpdateConfiguration) {

		HttpRequest req = formRequest('/fts-srv/update-index', jsonIndexUpdateConfiguration, 'POST');
		HttpResponse res = sendRequest(req);
		JsonDeserializerSimpleResponse result;

		if(res.getStatusCode() == 200){
			result = JsonDeserializerSimpleResponse.parse(res.getBody());
		} else {
			result = null;
		}

		return result;
	}

	public static JsonDeserializerInsertResponse populateTable (String jsonTableConfiguration) {

		HttpRequest req = formRequest('/fts-srv/insert', jsonTableConfiguration, 'POST');
		HttpResponse res = sendRequest(req);
		JsonDeserializerInsertResponse result;

		if(res.getStatusCode() == 200){
			result = JsonDeserializerInsertResponse.parse(res.getBody());
		} else {
			result = null;
		}

		return result;

	}

	public static JsonDeserializer search(String query, String cursor, Integer pageSize, String optionsJson) {

		String bodyJson = JSONOptionsWizard.searchJson(query);
		JsonDeserializer result;
		HttpRequest req = formRequest('/fts-srv/search', bodyJson, 'POST');
		try{
			HttpResponse res = sendRequest(req);

			if (res.getStatusCode() == 404) {
				result = null;
			}else {
				result = JsonDeserializer.parse(res.getBody());
			}

		} catch (Exception e){
			result = null;
		}

		return result;
	}

	public static Map<String, Integer> getSuggestions(String query, String cursor, Integer pageSize, String optionsJson) {

		Map<String, Integer> result;
		try{
			if(query.length()!= 0){
				String bodyJson = JSONOptionsWizard.suggestionJson(query);

				HttpRequest req = formRequest('/fts-srv/suggestions', bodyJson, 'POST');
				req.setTimeout(120000);
				HttpResponse res = sendRequest(req);
				if (res.getStatusCode() == 404) {
					result = null;
				}else {
					result = JSONOptionsWizard.deserializeSuggestion(res.getBody());
				}

			}
		} catch(Exception e){
		}

		return result;
	}

	public static JsonDeserializer checkIndex(String indexId) {

		String bodyJson = JSONOptionsWizard.checkIndexJson();

		HttpRequest req = formRequest('search/index', bodyJson, 'GET');
		HttpResponse res = sendRequest(req);

		JsonDeserializer result = JsonDeserializer.parse(res.getBody());

		return result;
	}

	public static JsonDeserializerDocuments getDocument(List<Integer> documentsIds) {

		JsonDeserializerDocuments result;

		if(!documentsIds.isEmpty()){
			String bodyJson = JSONOptionsWizard.creategetDocumentsJson(documentsIds);

			HttpRequest req = formRequest('/fts-srv/documents', bodyJson, 'POST');
			HttpResponse res = sendRequest(req);

			result = JsonDeserializerDocuments.parse(res.getBody());
		}

		return result;
	}

	public static JsonDeserializerSimpleResponse deleteIndexForTable(String tableName) {

		String user = Web_Service_Settings__c.getInstance('HerokuWebService').User__c;
		String password = Web_Service_Settings__c.getInstance('HerokuWebService').Password__c;
		String mainUrl = Web_Service_Settings__c.getInstance('HerokuWebService').URL__c;

		HttpRequest req = new HttpRequest();
		req.setEndpoint(mainUrl + '/fts-srv/drop-tables-indexes?indexName=' + tableName);
		req.setMethod('POST');
		req.setTimeout(120000);

		Blob headerValue = Blob.valueOf(user+':' + password);

		String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);

		req.setHeader('Authorization', authorizationHeader);
		req.setHeader('Content-Type', 'application/json');

		HttpResponse res = sendRequest(req);

		JsonDeserializerSimpleResponse result = JsonDeserializerSimpleResponse.parse(res.getBody());

		return result;
	}

	public static IndexInfo getIndexInfo(String tableName) {

		String user = Web_Service_Settings__c.getInstance('HerokuWebService').User__c;
		String password = Web_Service_Settings__c.getInstance('HerokuWebService').Password__c;
		String mainUrl = Web_Service_Settings__c.getInstance('HerokuWebService').URL__c;

		HttpRequest req = new HttpRequest();
		req.setEndpoint(mainUrl + '/fts-srv/index-status?indexName=' + tableName);
		req.setMethod('GET');
		req.setTimeout(120000);

		Blob headerValue = Blob.valueOf(user+':' + password);

		String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);

		req.setHeader('Authorization', authorizationHeader);
		req.setHeader('Content-Type', 'application/json');

		HttpResponse res = sendRequest(req);
		IndexInfo result = IndexInfo.parse(res.getBody());

		return result;
	}

	public static HttpRequest formRequest(String url, String bodyJson, String method) {

		String user = Web_Service_Settings__c.getInstance('HerokuWebService').User__c;
		String password = Web_Service_Settings__c.getInstance('HerokuWebService').Password__c;
		String mainUrl = Web_Service_Settings__c.getInstance('HerokuWebService').URL__c;

		HttpRequest req = new HttpRequest();
		req.setEndpoint(mainUrl + url);
		req.setMethod(method);
		req.setTimeout(120000);

		Blob headerValue = Blob.valueOf(user+':' + password);

		String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);

		req.setHeader('Authorization', authorizationHeader);
		req.setHeader('Content-Type', 'application/json');
		req.setBody(bodyJson);

		return req;

	}

	public static HttpResponse sendRequest(HttpRequest req) {
		Http http = new Http();
		HTTPResponse res = http.send(req);
		return res;
	}

	/*********************************************************************************************/
	/*                                  Multi Search methods                                     */
	/*********************************************************************************************/

	public enum StorageType {sharepoint, salesforce, aws}

	public static List<HerokuArticle> getArticles(List<String> recordIds) {
		List<HerokuArticle> herokuArticles = new List<HerokuArticle>();

		if (recordIds != null && !recordIds.isEmpty()) {
			List<String> customerConfiguration = HerokuWebService.getCustomerConfiguration();

			RequestWrapper requestBody = new RequestWrapper();
			requestBody.query = String.join(recordIds, ' ');
			requestBody.id = true;
			requestBody = HerokuWebService.getRequestBody(requestBody, customerConfiguration);

			HttpResponse response = HerokuWebService.getResponse(requestBody, customerConfiguration);
			herokuArticles = HerokuWebService.parseResponse(customerConfiguration, requestBody, response);
		}
		return herokuArticles;
	}

	public static List<HerokuArticle> getSearchResults(String searchTerm) {
		List<HerokuArticle> herokuArticles = new List<HerokuArticle>();

		if (String.isNotBlank(searchTerm)) {
			List<String> customerConfiguration = HerokuWebService.getCustomerConfiguration();

			RequestWrapper requestBody = new RequestWrapper();
			requestBody.query = searchTerm;
			requestBody = HerokuWebService.getRequestBody(requestBody, customerConfiguration);

			HttpResponse response = HerokuWebService.getResponse(requestBody, customerConfiguration);
			herokuArticles = HerokuWebService.parseResponse(customerConfiguration, requestBody, response);
		}
		return herokuArticles;
	}

	private static List<String> getCustomerConfiguration() {
		SourceWrapper salesforceSource = new SourceWrapper();
		salesforceSource.dataSourceType = HerokuWebService.StorageType.salesforce.name();
		salesforceSource.sourceId = UserInfo.getOrganizationId();
		salesforceSource.attrs.sfdcRole.add('admin'); //@TODO filter by SFDC Role ?
		// filter using Concierge Search Type settings configuration level
		salesforceSource.attrs.SFDCArticleType.addAll(Utils.prepareTypesSettings(false).keySet());
		// filter using Data Category Visibility level
		salesforceSource.attrs.SFDCDataCategory = KnowledgeDataCategoryService.getAllCategoriesForMe();
		// filter using User Locale Language
		salesforceSource.lang = Utils.getUserLanguage();

		CustomerExtensionBase uniqueClassMember = Utils.getNewBaseUniqueToolsInstance();
		List<String> customerConfiguration = uniqueClassMember.getMultiSearchRequestBody(
			new List<String>{JSON.serialize(salesforceSource)}
		);
		return customerConfiguration;		
	}

	private static RequestWrapper getRequestBody(RequestWrapper requestBody, List<String> customerConfiguration) {
		if (!customerConfiguration.isEmpty()) {
			// additional source systems
			if (customerConfiguration.size() > 1 
				&& String.isNotBlank(customerConfiguration.get(1))) {

				List<SourceWrapper> customerSourceSystems = (List<SourceWrapper>) JSON.deserialize(
					customerConfiguration.get(1),
					List<SourceWrapper>.class
				);
				requestBody.sources.addAll(customerSourceSystems);
			}
			System.debug('###### BODY: ' + JSON.serializePretty(requestBody));
		}
		return requestBody;
	}

	private static HttpResponse getResponse(RequestWrapper requestBody, List<String> customerConfiguration) {
		HttpRequest req = new HttpRequest();
		req.setMethod('POST');
		req.setHeader('Content-Type', 'application/json');
		req.setTimeout(120000);

		if (!customerConfiguration.isEmpty()) {
			req.setEndpoint( customerConfiguration.get(0) );
			req.setBody(JSON.serialize(requestBody));
			HttpResponse res = sendRequest(req);
			return res;
		}
		return null;
	}

	private static List<HerokuArticle> parseResponse(List<String> customerConfiguration, RequestWrapper requestBody, HttpResponse response) {
		List<HerokuArticle> herokuArticles = new List<HerokuArticle>();

		if (response != null) {
			String responseBody = response.getBody();
			List<Object> items = new List<Object>();
			try {
				items = (List<Object>) JSON.deserializeUntyped(responseBody);
				System.debug('########## responseBody: ' + responseBody);
			} catch (Exception e) {
				System.debug('HerokuWebService.parseResponse: error: \n response: ' + responseBody);
				throw new AuraHandledException('Exception: ' +  '\response: ' + responseBody + '\nrequest: ' + JSON.serialize(requestBody));
			}

			String sharepointUrl = customerConfiguration.size() >= 3 ? customerConfiguration.get(2) : '';
			for (Object item: items) {
				HerokuArticle hArticle = new HerokuArticle(item);
				// @TODO abst Map<String, Object> params
				hArticle.setParams(sharepointUrl);
				herokuArticles.add(hArticle);
			}
		}
		return herokuArticles;
	}

	public class RequestWrapper {
		public String query {get; set;}
		public List<SourceWrapper> sources {get; set;}
		public Boolean id {get; set;}
		public RequestWrapper() {
			this.sources = new List<SourceWrapper>();
			this.id = false;
		}
	}

	public class SourceWrapper {
		public AccessAttributeWrapper attrs {get; set;}
		public String dataSourceType {get; set;}
		public String sourceId {get; set;}
		public String user {get; set;}
		public String lang {get; set;}
		public SourceWrapper() {
			this.attrs = new AccessAttributeWrapper();
		}
	}

	public class AccessAttributeWrapper {
		public List<String> sfdcRole {get; set;}
		public List<String> SFDCArticleType {get; set;}
		public List<String> SFDCDataCategory {get; set;}
		public List<String> Key {get; set;}
		public AccessAttributeWrapper() {
			this.sfdcRole = new List<String>();
			this.SFDCArticleType = new List<String>();
			this.SFDCDataCategory = new List<String>();
			this.Key = new List<String>();
		}
	}

	public class HerokuArticle {
		public String recordId {get; set;}
		public String knowledgeArticleId {get; set;}
		public Map<String, Object> fields {get; set;}
		public String storage {get; set;}
		public String objectType {get; set;}
		public String category {get; set;}
		public DateTime createdDate {get; set;}
		public DateTime modifiedDate {get; set;}

		public HerokuArticle(Object item) {
			this.fields = (Map<String, Object>) item;
			this.fields = this.fields != null ? this.fields: new Map<String, Object>();
			this.fields.put('Title', ' ');
			this.storage = String.valueOf(this.fields.remove('datasource'));
			this.category = String.valueOf(this.fields.remove('category'));

			if (this.storage == HerokuWebService.StorageType.aws.name()
				|| (this.storage == null && this.fields.containsKey('aws_bucket'))) {
				this.parseAwsDetails();
			}
			if (this.storage == HerokuWebService.StorageType.sharepoint.name() 
					|| this.fields.get('ServerRelativeUrl') != null) {
				this.parseSharepointDetails();
			}

			this.setUpdatedDate();
			this.setCreatedDate();
			this.setObjectType();
			this.setArticleIds();
		}

		private void setObjectType() {
			if (this.storage == HerokuWebService.StorageType.salesforce.name() && this.fields.get('highlight') != null) {
				this.objectType = String.valueOf(this.fields.get('highlight')).substringBetween('accessattributes.SFDCArticleType.keyword=(<em>', '</em>');
			}
			this.objectType = String.isBlank(this.objectType) ? this.storage : this.objectType;
		}

		private void setArticleIds() {
			final Integer limitOnIdSize = 135;
			this.recordId = (String)this.fields.get('Id');
			if (this.recordId != null && this.recordId.length() > limitOnIdSize) {
				Integer recordIdSize = this.recordId.length();
				this.recordId = this.recordId.substring(recordIdSize - limitOnIdSize, recordIdSize);
			}
			this.knowledgeArticleId = (String)this.fields.get('KnowledgeArticleId');
			if (this.knowledgeArticleId != null && this.knowledgeArticleId.length() > limitOnIdSize) {
				Integer knowledgeArticleIdSize = this.knowledgeArticleId.length();
				this.knowledgeArticleId = this.knowledgeArticleId.substring(knowledgeArticleIdSize - limitOnIdSize, knowledgeArticleIdSize);
			}
		}

		private void setCreatedDate() {
			if (this.fields.get('CreatedDate') != null) {
				Object createdDate = this.fields.get('CreatedDate');
				this.createdDate = DateTime.newInstance(Long.valueOf(String.valueOf(createdDate)));
			}
			else if (this.fields.get('attachment') != null) {
				Map<String, Object> attachmentInfo = (Map<String, Object>)this.fields.get('attachment');
				if (attachmentInfo.get('date') != null) {
					String createdDateValue = (String)attachmentInfo.get('date');
					String dateValue = createdDateValue.substring(0, 10);
					List<String> timeValue = createdDateValue.subStringAfter('T').split(':');
					try {
						Time theTime = Time.newInstance(Integer.valueOf(timeValue.get(0)), Integer.valueOf(timeValue.get(1)), 0, 0);
						this.createdDate =  DateTime.newInstance(Date.valueOf(dateValue), theTime);
					} catch (Exception e) {
						System.debug('Heroku CratedDate value ' + e.getMessage() + '\n' + e.getStackTraceString());
					}
				}
			}
		}

		private void setUpdatedDate() {
			if (this.fields.get('lastUpdateDate') != null) {
				Object modifiedDate = this.fields.get('lastUpdateDate');
				this.modifiedDate = DateTime.newInstance(Long.valueOf(String.valueOf(modifiedDate)));
			}
		}

		private void parseAwsDetails() {
			String externalKey = (String) this.fields.get('_id');
			String fileName = (String) this.fields.get('fileName');
			fileName = String.isBlank(fileName) ? '': fileName;
			List<String> nameParts = fileName.replaceAll('_', ' ').split('\\.');
			if (nameParts.size() > 1) {
				nameParts.remove(nameParts.size() - 1);
			}

			this.fields.put('Id', externalKey);
			this.fields.put('KnowledgeArticleId', externalKey);
			this.fields.put('Title', String.join(nameParts, ' '));
			this.storage = HerokuWebService.StorageType.aws.name();
		}

		private void parseSharepointDetails() {
			String serverRelativeUrl = (String)this.fields.get('ServerRelativeUrl');
			String externalKey = (String) this.fields.get('_id');
			this.fields.put('Id', externalKey);
			this.fields.put('KnowledgeArticleId', externalKey);
			this.fields.put('Title', serverRelativeUrl.reverse().split('/')[0].reverse().split('\\.')[0]);
			this.storage = HerokuWebService.StorageType.sharepoint.name();
		}

		public void setParams(String sharepointUrl) {
			if (this.storage == HerokuWebService.StorageType.sharepoint.name()) {
				String serverRelativeUrl = (String)this.fields.get('ServerRelativeUrl');
				if (String.isNotBlank(serverRelativeUrl)) {
					this.fields.put('ServerRelativeUrl', sharepointUrl + serverRelativeUrl);
				}
			}
		}
	}
}