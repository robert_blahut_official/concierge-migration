/**
* @description
	Utility class with common methods

* @methods
	getAllInteractiveFields -			method for getting Interactive Fields for articles like Business cards ({!User.field})
	getAllUserInfo -					selecting User record with merge fields
	updateArticleBody -					modification body of Business cards article
*/
public with sharing class Utils {
	private static final String BASE_64_MAP = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/\'';
	private static final String BASE_CLASS_NAME = 'cncrg.CustomerExtensionBase';

	public static final String MAX_FAVORITE_ARTICLES_SETTING_NAME = 'Max Number of Favorite Articles';
	public static final String IS_MASTER_LANGUAGE_FIELD_NAME = 'IsMasterLanguage';
	public static final String UNIQUE_TOOLS_CLASS_SETTING_NAME = 'Unique Tools Class Name';
	public static final String FIELD_EXCLUDE_IF_LIGHTNING_KNOWLEDGE = 'ArticleType';
	public static final String CONCIERGE_TABS_VISIBILITY_SETTING_NAME = 'Concierge Tabs Visibility';
	public static final String BASE64_NAME_FLAG = 'Body__s';
	public static final Set<String> OTHER_FILE_FLAGS = new Set<String> { 'Name__s', 'ContentType__s', 'Length__s' };
	public static final String FIELD_LIGHTNING_KNOWLEDGE_OBJECT_NAME = 'Lightning Knowledge Object Name';
	public static final String SALESFORCE_SEPARATOR = '__';
	public static final String SNIPPET_CONFIGURATION_SETTING_NAME = 'Text to Display in Article Preview';
	public static final String ATTACHMENTS_FLAG_NAME = 'Attachments';

	public static Boolean isLightningKnowledgeEnabled;
	public static SearchEngineInterface engine;
	private static String companyLogo;
	private static String lightningKnowledgeObjectName;
	private static Boolean isArticlesHaveMasterVersionIdField;

	public static String getValueFromConciergeSettings(String settingName) {
		cncrg__Concierge_Settings__c settingRecord = cncrg__Concierge_Settings__c.getValues(settingName);
		return(settingRecord != null && String.isNotBlank(settingRecord.Value__c) ? settingRecord.Value__c : null);
	}
	public static String getOrgPrefix() {
		String orgPrefix = [SELECT NamespacePrefix FROM Organization LIMIT 1].NamespacePrefix;
		if (String.isBlank(orgPrefix)) {
			return '';
		}
		return orgPrefix + SALESFORCE_SEPARATOR;
	}

	public static CustomerExtensionBase getNewBaseUniqueToolsInstance() {
		String className = Utils.getValueFromConciergeSettings(UNIQUE_TOOLS_CLASS_SETTING_NAME);
		className = String.isNotBlank(className) ? className : BASE_CLASS_NAME;
		return(CustomerExtensionBase) Type.forName(className).newInstance();
	}

	public static String buildArticleDetail(List<String> fieldToDisplayNameList, sObject articleRecordFromBase, Map<String, Schema.SobjectField> sObjectFieldsDiscribe, String sObjectApiName, Set<String> notShowingFieldSet, String prefix) {
		String summary = '';

		CustomerExtensionBase uniqueClassMember = Utils.getNewBaseUniqueToolsInstance();
		List<String> specificArticleDetailPreparation = uniqueClassMember.specificArticleDetailPreparation(new List<String>());
		Boolean showLabels = Boolean.valueOf(specificArticleDetailPreparation[0]);

		for (String fieldAPIName: fieldToDisplayNameList) {
			if (fieldAPIName.contains(Utils.BASE64_NAME_FLAG)) {
				summary += Utils.generateAttachmentHTML(fieldAPIName, prefix, articleRecordFromBase, sObjectFieldsDiscribe, showLabels);
			}
			else if (!notShowingFieldSet.contains(fieldAPIName)) {

				Schema.DescribeFieldResult fieldDescribe = sObjectFieldsDiscribe.get(fieldAPIName).getDescribe();
				String fieldLabel = fieldDescribe.getLabel();
				String tempSummary = String.valueOf(articleRecordFromBase.get(fieldAPIName));

				if (tempSummary != null) {
					summary += (showLabels ? '<h1>' + fieldLabel + '</h1>' : '');
					summary += prepareFieldValue(articleRecordFromBase.get(fieldAPIName), fieldDescribe) + '<br/><br/>';

				}
			}
		}
		return summary;
	}

	private static String prepareFieldValue(Object tempBody, Schema.DescribeFieldResult fieldDescribe) {
		String updatedValue;
		String type = String.valueOf(fieldDescribe.getType());
		if (type == 'Date') {
			updatedValue = String.valueOf(Date.valueOf(tempBody));
		}
		else {
			updatedValue = String.valueOf(tempBody);
		}
		return updatedValue;
	}

	public static String generateAttachmentHTML(String fieldToDisplayName, String prefix, sObject article, Map<String, Schema.SobjectField> sObjectFieldsDescribe, Boolean showLabels) {
		String summary = '';
		prefix = String.isBlank(prefix) ? '' : '/' + prefix;
		String fileName = (String) article.get(fieldToDisplayName.replace(Utils.BASE64_NAME_FLAG, 'name__s'));

		if (String.isNotBlank(fileName) || Test.isRunningTest()) {
			String fieldLabel = sObjectFieldsDescribe.get(fieldToDisplayName).getDescribe().getLabel();
			fieldLabel = fieldLabel.replace(SettingsPanelController.BASE64_LABEL_FLAG, '');

			String articleId = (String) article.get('Id');
			String urlToContent = prefix + '/servlet/fileField?entityId=' + articleId + '&field=' + fieldToDisplayName;
			summary += (showLabels ? '<h1>' + fieldLabel + '</h1>' : '');
			summary += '<p><a target = "_blank" href="' + urlToContent + '" download="' + fileName + '">' + fileName + '</a></p>';
			summary += '<br/><br/>';
		}
		return summary;
	}

	public static List<String> getObjectFields(String objectName, List<String> fieldSet) {
		Set<String> availableFields = new Set<String>();
		Map<String, Schema.SObjectField> fieldMap = Utils.getDescribeSObjectResult(objectName).fields.getMap();
		for (String key : fieldSet) {
			String fieldName = key.trim().toLowerCase();
			if (fieldMap.get(fieldName) != null 
					&& fieldMap.get(fieldName).getDescribe().isAccessible()) {
				availableFields.add(key);
			}
		}
		return new List<String>(availableFields);
	}

	public static List<String> getAllAvailableFields(String objectName) {
		List<String> availableFields = new List<String> ();
		Map<String, Schema.SObjectField> fieldMap = Utils.getDescribeSObjectResult(objectName).fields.getMap();
		for (String key : fieldMap.keySet()) {
			if (fieldMap.get(key).getDescribe().isAccessible()) {
				availableFields.add(key);
			}
		}
		return availableFields;
	}

	public static Integer getMaxNumberOfFavoriteArticles() {
		final Integer sfMaxNumber = 50000;
		Concierge_Settings__c setting = Concierge_Settings__c.getValues(MAX_FAVORITE_ARTICLES_SETTING_NAME);
		return setting == null || String.isEmpty(setting.Value__c) ? sfMaxNumber : Integer.valueOf(setting.cncrg__Value__c);
	}

	public static Boolean checkNumberOfFaivoritArticles() {
		Id currentUser = UserInfo.getUserId();
		Boolean autorizedToAddMore = false;

		if (ESAPI.securityUtils().isAuthorizedToView('cncrg__Favorite_article__c', new List<String> { 'OwnerId' })) {
			Integer availableNumber = getMaxNumberOfFavoriteArticles();

			Integer numberOfFavorites = [SELECT COUNT() FROM cncrg__Favorite_article__c WHERE OwnerId = :currentUser LIMIT 50000];
			autorizedToAddMore = availableNumber> numberOfFavorites;
		}
		return autorizedToAddMore;
	}

	public static Boolean isLightningKnowledgeEnabled() {
		if (isLightningKnowledgeEnabled == null) {
			List<String> sobjects = new List<String> { 'KnowledgeArticleVersion' };
			List<Schema.DescribeSObjectResult> describeResults = Schema.describeSObjects(sobjects);
			isLightningKnowledgeEnabled = !describeResults.get(0).fields.getMap().containsKey(FIELD_EXCLUDE_IF_LIGHTNING_KNOWLEDGE);
		}
		return isLightningKnowledgeEnabled;
	}

	public static SearchEngineInterface initEngine() {
		String engineType = Search_Engine_Configurations__c.getInstance(AbstractSearchEngine.ENGINE_CONFIG_SETTING_NAME).Engine_Type__c;
		if (engineType == AbstractSearchEngine.SALESFORCE_SEARCH_TYPE) {
			if (isLightningKnowledgeEnabled()) {
				engine = new StandAloneLightningEditionController();
			}
			else {
				engine = new StandAloneEditionController();
			}
		}
		else if (engineType == AbstractSearchEngine.POSTGRESS_SEARCH_TYPE) {
			engine = new PostgresAdapter();
		}
		else if(engineType == AbstractSearchEngine.FEDERATED_SEARCH_TYPE) {
			engine = new MultiSearchEngineAdaptor();
		}
		return engine;
	}

	public static String getCompanyLogoName(String prefix) {
		if (String.isBlank(companyLogo)) {
			String companyLogoName = Utils.getValueFromConciergeSettings('CompanyLogo');
			if (String.isNotBlank(companyLogoName)) {
				List<Document> documents = new List<Document>();

				if (ESAPI.securityUtils().isAuthorizedToView('Document', new List<String> {'Id','Name'})) {
					documents = [SELECT Id FROM Document WHERE Name = :companyLogoName Limit 50000];
				}

				if (!documents.isEmpty()) {
					String wayToLogo;
					if (prefix != '') {
						wayToLogo = String.escapeSingleQuotes('/' + prefix + '/servlet/servlet.FileDownload?file=' + documents[0].Id);
					}
					else {
						wayToLogo = String.escapeSingleQuotes('/servlet/servlet.FileDownload?file=' + documents[0].Id);
					}
					companyLogo = '<img src="' + wayToLogo.escapeEcmaScript() + '" style="width: 100%; height: 100%"/>';
				}
			}
			companyLogo = String.isBlank(companyLogo) ? '' : companyLogo;
		}
		return companyLogo;
	}

	public static Map<String, sObjectWrapper> prepareTypesSettings(Boolean isUsualObjects) {
		List<cncrg__Search_Types_Settings__c> allArticleTypesList = cncrg__Search_Types_Settings__c.getAll().values();
		return prepareTypesSettings(allArticleTypesList, isUsualObjects);
	}

	public static Map<String, sObjectWrapper> prepareTypesSettings(List<cncrg__Search_Types_Settings__c> articleTypesList, Boolean isUsualObjects) {
		return prepareTypesSettings(articleTypesList, false, isUsualObjects);
	}

	public static Map<String, sObjectWrapper> prepareTypesSettings(List<String> articleTypesList, Boolean isUsualObjects) {
		List<cncrg__Search_Types_Settings__c> allArticleTypesList = cncrg__Search_Types_Settings__c.getAll().values();
		List<cncrg__Search_Types_Settings__c> articleCustomTypesList = new List<cncrg__Search_Types_Settings__c> ();
		for (cncrg__Search_Types_Settings__c item : allArticleTypesList) {
			if (articleTypesList.contains(item.cncrg__Article_Type_API_Name__c)) {
				articleCustomTypesList.add(item);
			}
		}
		return prepareTypesSettings(articleCustomTypesList, isUsualObjects);
	}

	public static Map<String, sObjectWrapper> prepareTypesSettings(List<cncrg__Search_Types_Settings__c> articleTypesList, Boolean withoutActive, Boolean isUsualObjects) {
		String KNOWLEDGE_TITLE_FIELD_NAME = 'Title';
		Map<String, sObjectWrapper> mapOfTypesSettings = new Map<String, sObjectWrapper>();
		String lightningKnowledgeObjectName = Utils.isLightningKnowledgeEnabled() ? Utils.getLightningKnowledgeObjectName() : null;

		for (cncrg__Search_Types_Settings__c setting:  articleTypesList) {
			String articleType = setting.cncrg__Article_Type_API_Name__c;
			String articleTypeInDataBase = lightningKnowledgeObjectName == null || isUsualObjects ? articleType : lightningKnowledgeObjectName;

			if (setting.cncrg__Active__c && setting.cncrg__Is_Usual_Object__c == isUsualObjects || withoutActive) {
				List<String> fields = new List<String> ();
				if (setting.cncrg__Fields_To_Display_1__c != null) {
					fields = setting.cncrg__Fields_To_Display_1__c.split(',');
				}
				if (setting.cncrg__Fields_To_Display_2__c != null) {
					fields.addAll(setting.cncrg__Fields_To_Display_2__c.split(','));
				}
				if (setting.cncrg__Fields_To_Display_3__c != null) {
					fields.addAll(setting.cncrg__Fields_To_Display_3__c.split(','));
				}
				if (setting.cncrg__Fields_To_Display_4__c != null) {
					fields.addAll(setting.cncrg__Fields_To_Display_4__c.split(','));
				}

				Boolean showAttachments = false;
				if (fields.contains(ATTACHMENTS_FLAG_NAME)) {
					showAttachments = true;
					fields.remove(fields.indexOf(ATTACHMENTS_FLAG_NAME));
				}

				String titleField = isUsualObjects ? setting.Title_Field__c : KNOWLEDGE_TITLE_FIELD_NAME;
				List<Boolean> objectAccessibilityList = Utils.isArticleHasAvailableFields(articleTypeInDataBase, titleField);
				if (!objectAccessibilityList[1]) {
					titleField = null;
				}

				if (objectAccessibilityList[0]) {
					List<String> avalaibleFieldsList = Utils.getObjectFields(articleTypeInDataBase, fields);
					mapOfTypesSettings.put(
					   articleType,
					   isUsualObjects 
							? new sObjectWrapper(titleField, avalaibleFieldsList, showAttachments, setting.Field_Filters__c)
							: new sObjectWrapper(titleField, avalaibleFieldsList, showAttachments)
					);
				}
			}
		}
		return mapOfTypesSettings;
	}

	public class sObjectWrapper {
		public String titleFieldName { get; set; }
		public Boolean showAttachments { get; set; }
		public List<String> availableFieldsList { get; set; }
		public List<SettingsPanelController.FieldFilter> filtres { get; set; }

		public sObjectWrapper(String titleFieldName, List<String> availableFieldsList, Boolean showAttachments) {
			this.titleFieldName = titleFieldName;
			this.availableFieldsList = availableFieldsList;
			this.showAttachments = showAttachments;
		}
		public sObjectWrapper(String titleFieldName, List<String> availableFieldsList, Boolean showAttachments, String filtersStr) {
			this(titleFieldName, availableFieldsList, showAttachments);
			filtres = new List<SettingsPanelController.FieldFilter> ();
			if (String.isNotBlank(filtersStr)) {
				List<String> conditionList = filtersStr.split(';');
				for (Integer i = 0, j = conditionList.size(); i<j; i += 3) {
					filtres.add(new SettingsPanelController.FieldFilter(conditionList[i], conditionList[i + 1], conditionList[i + 2]));
				}
			}
		}
	}

	public static List<cncrg__Search_Types_Settings__c> getSearchTypesSettingsByArticleAPIName(Set<String> articleAPINameSet) {
		List<cncrg__Search_Types_Settings__c> allArticleTypesList = cncrg__Search_Types_Settings__c.getAll().values();
		List<cncrg__Search_Types_Settings__c> settingsByNameList = new List<cncrg__Search_Types_Settings__c> ();
		for (Integer i = 0, j = allArticleTypesList.size(); i<j; i++) {
			if (articleAPINameSet.contains(allArticleTypesList[i].cncrg__Article_Type_API_Name__c)) {
				settingsByNameList.add(allArticleTypesList[i]);
			}
		}
		return settingsByNameList;
	}

	private static List<Boolean> isArticleHasAvailableFields(String articleTypeName, String titleFieldName) {
		try {
			Type articleType = Type.forName(articleTypeName);
			if (articleType == null) {
				return new List<Boolean> { false, false };
			}
			Schema.DescribeSObjectResult dsc = Utils.getDescribeSObjectResult(articleTypeName);
			if (!dsc.isAccessible()) {
				return new List<Boolean> { false, false };
			}

			Boolean isUsetHasAccessToTitleField = false;
			if (String.isNotBlank(titleFieldName)) {
				Map<String, Schema.SObjectField> fieldMap = dsc.fields.getMap();
				if (fieldMap.get(titleFieldName.trim().toLowerCase()) != null) {
					isUsetHasAccessToTitleField = fieldMap.get(titleFieldName.trim().toLowerCase()).getDescribe().isAccessible();
				}
			}
			return new List<Boolean> { true, isUsetHasAccessToTitleField };
		} catch(Exception e) {
			System.debug(e.getMessage() + '; ' + e.getStackTraceString());
			return new List<Boolean> { false, false };
		}
	}

	public static String getLightningKnowledgeObjectName() {
		if (lightningKnowledgeObjectName == null) {
			lightningKnowledgeObjectName = Utils.getValueFromConciergeSettings(FIELD_LIGHTNING_KNOWLEDGE_OBJECT_NAME);
		}
		return lightningKnowledgeObjectName;
	}

	public static Map<String, Schema.RecordTypeInfo> getRecordTypesMapInfo() {
		String lightningKnowledgeObjectName = getLightningKnowledgeObjectName();
		return Utils.getDescribeSObjectResult(lightningKnowledgeObjectName).getRecordTypeInfosByName();
	}

	public static String getUserLanguage() {
		return getNewBaseUniqueToolsInstance().getUserLocale(new List<String> ()) [0];
	}

	public static List<String> getArticleFilters() {
		return getNewBaseUniqueToolsInstance().getArticleFilterOptions(null);
	}

	public static Set<String> splitUnionFields(Map<String, sObjectWrapper> mapOfTypesSettings) {
		Set<String> notShowingFieldSet = new Set<String> ();
		for (String key : mapOfTypesSettings.keySet()) {
			List<String> currentFieldList = mapOfTypesSettings.get(key).availableFieldsList;
			for (Integer i = 0; i<currentFieldList.size(); i++) {
				String currentField = currentFieldList[i];
				if (currentField.contains(BASE64_NAME_FLAG)) {
					for (String otherField : OTHER_FILE_FLAGS) {
						String newField = currentField.replace(BASE64_NAME_FLAG, otherField);
						notShowingFieldSet.add(newField);
						currentFieldList.add(newField);
					}
				}
			}
		}
		return notShowingFieldSet;
	}

	public static String escapeSOSLQuery(String term) {
		String escapedText = String.escapeSingleQuotes(term);
		//sescapedText = escapedText.replaceAll( '(?i)( AND NOT | AND | OR )', ' ' );
		Pattern myPatternSpecialCharacters = Pattern.compile('(?i)( AND NOT | AND | OR )');
		Matcher myMatcherSpecialCharacters = myPatternSpecialCharacters.matcher(escapedText);
		escapedText = myMatcherSpecialCharacters.replaceAll(' \"$1\" ');
		Pattern myPattern = Pattern.compile('(\\&|\\||\\{|\\}|\\[|\\]|\\(|\\)|\\^|\\~|\\*|\\:|\\+|\\-|\\!)');

		Matcher myMatcher = myPattern.matcher(escapedText);
		escapedText = myMatcher.replaceAll('\\\\\\\\$1');
		escapedText = escapedText.replaceAll('( )+', ' ').trim();
		return escapedText;
	}

	public static Boolean isArticlesHaveMasterVersionIdField() {
		if (isArticlesHaveMasterVersionIdField == null) {
			isArticlesHaveMasterVersionIdField = KnowledgeArticleVersion.sObjectType.getDescribe().fields.getMap().containsKey(IS_MASTER_LANGUAGE_FIELD_NAME);
		}
		return isArticlesHaveMasterVersionIdField;
	}

	public static SearchResultsWrapper getArticleFieldsHTML(SearchResultsWrapper articleRecord, String prefix, sObject articleRecordFromBase, List<String> fieldToDisplayNameList, Set<String> notShowingFieldSet) {
		CustomerExtensionBase uniqueClassMember = Utils.getNewBaseUniqueToolsInstance();
		articleRecord = (SearchResultsWrapper) JSON.deserialize(uniqueClassMember.setArticlePreviewValue(new List<String> { JSON.serialize(articleRecord), JSON.serialize(articleRecordFromBase) }) [0], SearchResultsWrapper.class);

		DescribeSObjectResult describe = articleRecordFromBase.getSObjectType().getDescribe();
		Map<String, Schema.SobjectField> sObjectFieldsDescribe = describe.fields.getMap();

		String companyLogo = '';

		String summary = Utils.buildArticleDetail(fieldToDisplayNameList, articleRecordFromBase, sObjectFieldsDescribe, describe.getName(), notShowingFieldSet, prefix);
		summary = summary.removeEnd('<br/>');

		if (SearchQueryInputController.isTest) {
			summary = '{!User.Name} {!Document.CompanyLogo}';
			companyLogo = 'Logo';
		}

		if (companyLogo != null && summary.contains('{!Document.CompanyLogo}')) {
			companyLogo = Utils.getCompanyLogoName(prefix);
			summary = summary.replaceAll('\\{!Document.CompanyLogo}', companyLogo);
		}

		Set<String> allInteractiveFields = new Set<String> ();
		if (summary.contains('{!User.')) {
			allInteractiveFields.addAll(Utils.getAllInteractiveFields(summary));
		}

		articleRecord.body = summary;
		if (!allInteractiveFields.isEmpty()) {
			User theUser = Utils.getAllUserInfo(allInteractiveFields);
			articleRecord.body = Utils.updateArticleBody(
				articleRecord.body,
				theUser,
				allInteractiveFields
			);
		}

		List<String> fieldsToCheck = new List<String> { 'cncrg__ArticleId__c', 'OwnerId' };
		List<cncrg__Favorite_article__c> listOfFavoriteEncrypteData = new List<cncrg__Favorite_article__c> ();
		if (ESAPI.securityUtils().isAuthorizedToView('cncrg__Favorite_article__c', fieldsToCheck)) {
			listOfFavoriteEncrypteData = [
				SELECT cncrg__ArticleId__c, OwnerId
				FROM cncrg__Favorite_article__c
				WHERE OwnerId = :UserInfo.getUserId()
				LIMIT 50000
			];
		}
		for (cncrg__Favorite_article__c item : listOfFavoriteEncrypteData) {
			WorkWithArticles.EncryptionMember member = new WorkWithArticles.EncryptionMember();
			if (member.decrypteData(item.cncrg__ArticleId__c) == articleRecord.knowledgeArticleId) {
				articleRecord.isFavorite = true;
			}
		}
		return articleRecord;
	}

	public static Boolean isUserHasPermitionToCreateCase() {
		return Case.sObjectType.getDescribe().isCreateable();
	}

	public static Boolean containsDoubleByteCharacters(String input) {
		Matcher m = Pattern.compile('[\\u0000-\\u007F]*').matcher(input);
		return !m.matches();
	}

	public static String removeEndSpecCharacters(String text) {
		List<String> specCharactersList = new List<String>();
		specCharactersList.add(',');
		specCharactersList.add(';');
		specCharactersList.add(')');
		specCharactersList.add('(');
		specCharactersList.add(':');
		specCharactersList.add('\"');
		specCharactersList.add('\'');
		for (String item : specCharactersList) {
			text = text.removeEnd(item);
		}
		return text;
	}

	public static String addSnippetElipsis(String snippetText, Boolean isPreview) {
		if (String.isNotBlank(snippetText)) {
			if (snippetText.indexOf('...') != 0 && !isPreview) {
				snippetText = '...' + snippetText.trim();
			}
			if (snippetText.substring(snippetText.length() - 1) != '.') {
				snippetText = snippetText + '...';
			}
		}
		return snippetText;
	}

	public static Boolean isCustomSnippetNeeded() {
		Concierge_Settings__c snippetConfiguration = Concierge_Settings__c.getValues(SNIPPET_CONFIGURATION_SETTING_NAME);
		return(snippetConfiguration == null || Boolean.valueOf(snippetConfiguration.Value__c)) ? false : true;
	}

	/*
	  DEAR SECURITY REVIEWER! We have deliberately made this class use without sharing annotation based on 
	  customers' feedback and the need to support users' security needs. 
	  This only updates 'Unread_Update_for_Contact__c' in the application.
	 */
	public without sharing class WithoutSharingUtility {
		public void updateCases(List<Case> listOfCases) {
			update listOfCases;
		}
		public List<SObject> query(String query) {
			return Database.query(query);
		}
	}

	public static String getDynamicComponentName(String blockName) {
		CustomerExtensionBase uniqueClassMember = getNewBaseUniqueToolsInstance();
		Map<String, String> dynamicComponentsMap = (Map<String, String>) JSON.deserialize(uniqueClassMember.getDynamicComponentNamesMap(new List<String>())[0], Map<String, String>.class);
		return dynamicComponentsMap.get(blockName);
	}

	public static Map<String, List<String>> getDependentPicklistValues(Schema.DescribeFieldResult depend) {
		Schema.DescribeFieldResult control = depend.getController().getDescribe();
		List<Schema.PicklistEntry> controlEntries = (control.getType() == Schema.DisplayType.Boolean ? null : control.getPicklistValues());
		List<Schema.PicklistEntry> dependantEntries = depend.getPicklistValues();

		Map<Object, List<String>> dependentPicklistValues = new Map<Object, List<String>>();
		for (Schema.PicklistEntry entry : dependantEntries) {
			if (entry.isActive()) {
				List<String> base64chars = String.valueOf(((Map<String, Object>) JSON.deserializeUntyped(JSON.serialize(entry))).get('validFor')).split('');
				for (Integer i = 0; i < (controlEntries != null ? controlEntries.size() : 2); i++) {
					Object controlValue = (controlEntries == null ? (Object) (i == 1) :
										   (Object) (controlEntries[i].isActive() ? controlEntries[i].getValue() : null));
					Integer bitIndex = i / 6, bitShift = 5 - Math.mod(i, 6);
					if (controlValue == null
							|| (bitIndex >= base64chars.size())
							|| (BASE_64_MAP.indexOf(base64chars[bitIndex]) & (1 << bitShift)) == 0) {
						continue;
					}
					if (!dependentPicklistValues.containsKey(controlValue)) {
						dependentPicklistValues.put(controlValue, new List<String>());
					}
					dependentPicklistValues.get(controlValue).add(entry.getValue());
				}
			}
		}

		Map<String, List<String>> outputMap = new Map<String, List<String>>();
		for (Object keyMap: dependentPicklistValues.keySet()) {
			outputMap.put((String)keyMap, dependentPicklistValues.get(keyMap));
		}
		return outputMap;
	}

	public static List<Schema.FieldSetMember> getSchemaFieldSetMembers(String typeName, String fsName) {
		Schema.DescribeSObjectResult describe = Utils.getDescribeSObjectResult(typeName);
		Map<String, Schema.FieldSet> fsMap = describe.fieldSets.getMap();
		return fsMap.get(fsName).getFields();
	}

	public static Schema.DescribeSObjectResult getDescribeSObjectResult(String typeName) {
		SObject objectRecord = (SObject)Type.forName(typeName).newInstance();
		Schema.SObjectType SObjectTypeObj = objectRecord.getSObjectType();
		return SObjectTypeObj.getDescribe();
	}

	public static Map<String, Schema.DescribeSObjectResult> getAllKnowledgeObjects() {
		Map<String, Schema.DescribeSObjectResult> knowledgeObjects = new Map<String, Schema.DescribeSObjectResult>();
		Map<String, Schema.SObjectType> types = Schema.getGlobalDescribe();
		for (String objectName: types.keySet()) {
			if (objectName.endsWith('__kav')) {
				Schema.DescribeSObjectResult descResults = types.get(objectName).getDescribe();
				knowledgeObjects.put(descResults.getName(), descResults);
			}
		}
		return knowledgeObjects;
	}

	public static Set<String> getAllInteractiveFields(String summary) {
		Set<String> allInteractiveFields = new Set<String> ();
		List<String> firstSplit = summary.split('\\{!User.');
		for (Integer i = 1; i < firstSplit.size(); i++) {
			allInteractiveFields.add(firstSplit[i].split('}') [0]);
		}
		return allInteractiveFields;
	}

	public static User getAllUserInfo(Set<String> allInteractiveFields) {
		String query = 'SELECT Id, ';
		List<String> fieldsToCheck = new List<String>();

		for (String fieldName : allInteractiveFields) {
			query += fieldName + ', ';
			fieldsToCheck.add(fieldName);
		}
		query = query.removeEnd(', ');
		query += ' FROM User WHERE Id = \'' + UserInfo.getUserId() + '\'';
		return (ESAPI.securityUtils().isAuthorizedToView('User', fieldsToCheck)) ? Database.query(query) : null;
	}

	public static String updateArticleBody(String body, User theUser, Set<String> allInteractiveFields) {
		return updateArticleBody(body, theUser, allInteractiveFields, null);
	}

	public static String updateArticleBody(String body, User theUser, Set<String> allInteractiveFields, String prefix) {
		for (String fieldName : allInteractiveFields) {
			String fieldValue = '';

			if (theUser.get(fieldName) != null) {
				fieldValue = String.valueOf(theUser.get(fieldName));

				if (fieldName.equals('FullPhotoUrl')) {
					String communityValue = String.escapeSingleQuotes(fieldValue);
					String appValue = String.escapeSingleQuotes(fieldValue.substringAfter('.com')); // Load profile photo using FullPhotoUrl of current User
					if (appValue == '') {
						fieldValue = '<img src="' + communityValue.escapeEcmaScript() + '" style="width: 100%; height: auto"/>'; //100%
					}
					else {
						fieldValue = '<img src="' + appValue.escapeEcmaScript() + '" style="width: 100%; height: auto"/>'; //100%
					}
				}
				if (fieldName.contains('Good_Thru')) {
					List<String> dateSplit = fieldValue.split('-');
					fieldValue = dateSplit[1] + '/' + dateSplit[2].substringBefore(' ');
				}
			}

			body = body.replaceAll('\\{!User.' + fieldName + '}', fieldValue);
		}
		return body;
	}
}