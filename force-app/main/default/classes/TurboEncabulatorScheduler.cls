/**
 * DEAR SECURITY REVIEWER!
 * The apex class is not using the “with or without sharing” because the class is referenced in package post install class.
 * Please see details in Salesforce documentation. 
 * https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_install_handler.htm
 * “Post install script can’t call another Apex class in the package if that Apex class uses the with sharing keyword.“
*/
global class TurboEncabulatorScheduler implements Schedulable {

	public static final String JOB_NAME = 'TurboEncabulator';

	public static final String TURBO_BATCH_NAME = 'cncrg.TurboEncabulator';
	public static final String REVIEW_BATCH_NAME = 'cncrg.ArticleReviewBatch';
	public static final String FEED_BATCH_NAME = 'Article Feedback';
	public static final String FEED_CLASS_NAME = 'cncrg.FeedbackBatch';

	public static final Integer TURBO_BATCH_SIZE = 50;
	public static final Integer REVIEW_BATH_SIZE = 20;
	public static final Integer FEED_BATH_SIZE = 50;

	public static final String TURBO_DESCRIPTION = 'Turbo Encabulation';
	public static final String REVIEW_DESCRIPTION = 'Generate Article Review';
	public static final String FEED_DESCRIPTION = 'Process Article Feedback';

	public static final String TURBO_CRON = '0 1/30 * * * *';
	public static final String REVIEW_CRON = '0 0 22 * * *';
	public static final String FEED_CRON = '0 15 * * * *';

	public static final Long START_DATETIME_RANGE = 6*23*60*60*1000;

	global void execute(SchedulableContext sc) {
		if(checkStart(sc)) {
			startNextBatch(sc.getTriggerId());
		} else if (sc != null && sc.getTriggerId() != null) {
			// package upgrade issue
			try { System.AbortJob(sc.getTriggerId()); } catch (Exception e) {}
		}
	}

	private Boolean checkStart(SchedulableContext sc) {
		Boolean result = true;
		try {
			CronTrigger cr = [SELECT Id, PreviousFireTime FROM CronTrigger WHERE Id = : sc.getTriggerId()];
			if (Datetime.now().getTime() - cr.PreviousFireTime.getTime() >= START_DATETIME_RANGE) {
				sendNotification();
				result = false;
			}
		} catch (Exception e) {}// for postInstall script (can't access to CronTrigger)
		return result;
	}

	global static String startScheduler(String timer, String nameJob)	{
		return System.schedule(nameJob, timer, new TurboEncabulatorScheduler());
	}

	global static String startScheduler(Datetime timer)	{
		String jobName = JOB_NAME;
		if(Test.isRunningTest()) {
			jobName += String.valueOf(Crypto.getRandomInteger());
		}
		if(timer < Datetime.now())
			return System.schedule(jobName, Datetime.now().addSeconds(5).format('s m * * M ? yyyy'), new TurboEncabulatorScheduler());
		return System.schedule(jobName, timer.addSeconds(5).format('s m * * M ? yyyy'), new TurboEncabulatorScheduler());
	}

	private static void endScheduler() {
		List<CronTrigger> asList = [SELECT Id FROM CronTrigger WHERE CronJobDetail.Name = :JOB_NAME];
		if(!asList.isEmpty())
			System.AbortJob(asList[0].Id);
	}

	private static Boolean checkRelevance() {
		List<SchedulerSettings__c> settingList = SchedulerSettings__c.getAll().values();
		Boolean relevance = false;
		for(Integer i = 0; i < settingList.size(); i++) {
			if(settingList[i].Active__c) {
				relevance = true;
				break;
			}
		}
		return relevance;
	}

	global static void nextStep() {
		if(checkRelevance()) {
			startScheduler(getNextRunDatetime());
		}
	}

	public static void recalculate() {
		endScheduler();
		nextStep();
	}

	private static Datetime getNextRunDatetime() {
		Map<Datetime, SchedulerSettings__c> apexClassByDatetime = buildTimeMap();
		List<Datetime> sortList = new List<Datetime>(apexClassByDatetime.keySet());
		sortList.sort();
		return sortList[0];
	}

	private static SchedulerSettings__c getNextSettings() {
		Map<Datetime, SchedulerSettings__c> apexClassByDatetime = buildTimeMap();
		List<Datetime> sortList = new List<Datetime>(apexClassByDatetime.keySet());
		sortList.sort();
		if(sortList.isEmpty()) {
			return null;
		}
		return apexClassByDatetime.get(sortList[0]);
	}

	private static Map<Datetime, SchedulerSettings__c> buildTimeMap() {
		Map<Datetime, SchedulerSettings__c> apexClassByDatetime = new Map<Datetime, SchedulerSettings__c>();
		for(SchedulerSettings__c currentSettings : SchedulerSettings__c.getAll().values()) {
			if(currentSettings.Active__c) {
				currentSettings.Last_Run__c = CronUtility.checkLastRun(currentSettings);
				Datetime currentDatetime = CronUtility.convertCronToNextDatetime(currentSettings.CRON__c, currentSettings.Last_Run__c);
				apexClassByDatetime.put(currentDatetime, currentSettings);
			}
		}
		return apexClassByDatetime;
	}

	private void startNextBatch(String schedId) {
		SchedulerSettings__c nextBatch = getNextSettings();
		List<String> statusList = new List<String>{'Holding', 'Queued', 'Preparing', 'Processing'};
		List<sObject> apexJobs = new List<sObject>();
		// for postInstall script (can't support to AsyncApexJob)
		try{
			apexJobs = [SELECT Id FROM AsyncApexJob WHERE ApexClass.Name = :nextBatch.Class_Name__c AND Status IN :statusList];
		} catch (Exception e) {}
		if(apexJobs.isEmpty() && nextBatch != null) {
			Type customType = Type.forName(nextBatch.Class_Name__c);
			AbstractConciergeBatch batchable = (AbstractConciergeBatch)customType.newInstance();
			batchable.setInitParams(new List<Object>{schedId, Datetime.now(), nextBatch.Class_Name__c, nextBatch.Batch_Size__c.intValue()});
			Id batchInstanceId = Database.executeBatch(batchable, nextBatch.Batch_Size__c.intValue());
		} else {
			System.AbortJob(schedId);
		}
	}

	global static void updateSettingsAfterRun(String jobId) {
		AsyncApexJob currentJop = [SELECT Id, ApexClass.Name, CreatedDate, CompletedDate FROM AsyncApexJob WHERE Id = :jobId];
		updateSettingsAfterRun(jobId, currentJop.CreatedDate, currentJop.ApexClass.Name);
	}

	public static void updateSettingsAfterRun(String jobId, Datetime startTime, String batchName) {
		Long dt1Long = startTime.getTime();
		Long dt2Long = Datetime.now().getTime();
		Long seconds = (dt2Long - dt1Long) /  1000;
		SchedulerSettings__c currentSettings = getNextSettings();
		if(currentSettings != null && currentSettings.Class_Name__c.contains(batchName)) {
			currentSettings.Last_Duration__c = Integer.valueOf(seconds);
			currentSettings.Last_Run__c = Datetime.now();
			ESAPI.securityUtils().validatedUpdate(new List<SchedulerSettings__c>{currentSettings});
			TurboEncabulatorScheduler.nextStep();
		}
	}
	@TestVisible
	private static void sendNotification() {
		List<SchedulerSettings__c> setList = SchedulerSettings__c.getAll().values();
		List<User> uList = [SELECT Email, Name FROM User WHERE Id = :setList[0].LastModifiedById AND IsActive = true];
		if(uList.isEmpty()) {
			uList = [SELECT  Name, Email
					FROM    User
					WHERE   Profile.PermissionsModifyAllData = true
						AND IsActive = true
			];
		}
		List<String> emailList = new List<String>();
		for(Integer i = 0; i < uList.size(); i++) {
			emailList.add(uList[i].Email);
		}
		try {
			Organization org = [SELECT Id, Name, isSandbox FROM Organization LIMIT 1];
			Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
			message.toAddresses = emailList;
			message.subject = 'TurboEncabulator - Spooling Failed';
			message.plainTextBody = 'Hello.\n Please note, the TurboEncabulator process has failed to run and needs to be restarted. \n Thank you!';
			String htmlBody = 'Hello.<br/> Please note, the TurboEncabulator process has failed to run and needs to be restarted.<br/> Thank you!<br/><br/>';
			htmlBody += 'Organization: ' + org.Name;
			if(org.isSandbox) {
				htmlBody += '(Sandbox)';
			}
			htmlBody += '/' + org.Id;
			message.setHtmlBody(htmlBody);

			Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
			Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
		} catch (Exception e) {}
	}
}