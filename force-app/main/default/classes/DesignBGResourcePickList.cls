global class DesignBGResourcePickList extends VisualEditor.DynamicPickList {
	 global override VisualEditor.DataRow getDefaultValue() {
        
        VisualEditor.DataRow defaultValue = new VisualEditor.DataRow('--None--', '--None--');
        return defaultValue;
    }
    global override VisualEditor.DynamicPickListRows getValues() {
        List<Background_Settings__c> backgroundSettingsList = Background_Settings__c.getall().values();
	   
        VisualEditor.DynamicPickListRows  backgroundValues = new VisualEditor.DynamicPickListRows();
        backgroundValues.addRow(new VisualEditor.DataRow('--None--', '--None--'));
        for(Background_Settings__c item : backgroundSettingsList) {
            backgroundValues.addRow(new VisualEditor.DataRow(item.Name, item.Name));
        }
        return backgroundValues;
    }
}