global with sharing class SearchPhraseWrapper implements Comparable
{
    @AuraEnabled
    public String Phrase {get;set;}
    
    @AuraEnabled
    public Integer Usage {get;set;}

    public SearchPhraseWrapper(String ph, Integer usg) {
        Phrase = ph;
        Usage = usg;
    }

    global Integer compareTo(Object compareTo) {
        SearchPhraseWrapper compareToEmp = (SearchPhraseWrapper)compareTo;
        if (Usage == compareToEmp.Usage) return 0;
        if (Usage > compareToEmp.Usage) return -1;
        return 1;        
    }
}