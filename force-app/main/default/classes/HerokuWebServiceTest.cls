@isTest
private class HerokuWebServiceTest {

	@isTest static void herokuFoundTest() {
		init();
		String mockBody = '[{"highlight":{"fileName":["111 <em>test</em> 333.txt"],"attachment.content":["111 <em>test</em> asvggg  work word worfbb"],"Key":["111 <em>test</em> 333.txt"]},"fileName":"111 test 333.txt","content_type":"text/plain","attachment":{"content_type":"text/plain; charset=ISO-8859-1","language":"et","content_length":34},"_id":"1234522892","lastUpdateDate":1563842363000,"datasource":"sharepoint","aws_bucket":"testellc","ServerRelativeUrl":"https://testellc.s3.us-west-2.amazonaws.com/111%20test%20333.txt","Key":"111 test 333.txt"},{"highlight":{"attachment.content":["<em>Test</em> document for S3<em>Test</em> document for S3<em>Test</em> document","for S3<em>Test</em> document for S3"]},"content_type":"application/vnd.openxmlformats-officedocument.wordprocessingml.document","attachment":{"date":"2019-07-22T13:39:00Z","content_type":"application/vnd.openxmlformats-officedocument.wordprocessingml.document","author":"jbUser","language":"fr","content_length":85},"aws_bucket":"testellc","_id":"1234567890","Key":"TestDocument.docx"},{"highlight":{"attachment.content":["111 <em>test</em> asvggg  work word worfbb"]},"content_type":"text/plain","attachment":{"content_type":"text/plain; charset=ISO-8859-1","language":"et","content_length":34},"_id":"1234567892","aws_bucket":"testellc","Key":"111 test.txt"}]';
		Map<String, String> headers = new Map<String, String>();
		headers.put('Content-Type', 'application/json');

		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(200, 'OK', mockBody, headers));

		Test.startTest();
		List<HerokuWebService.HerokuArticle> articles = HerokuWebService.getSearchResults('test');
		System.assert(!articles.isEmpty());
		articles = HerokuWebService.getArticles(new List<String>{'test'});
		System.assert(!articles.isEmpty());
		Test.stopTest();
	}

	@isTest static void createIndexTest() {
		init();
		String mockBody = '{"result": "succes", "error": false}';
		Map<String, String> headers = new Map<String, String>();
		headers.put('Content-Type', 'application/json');

		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(200, 'OK', mockBody, headers));

		Test.startTest();
		JsonDeserializerSimpleResponse result = HerokuWebService.createIndex('Test');
		Test.stopTest();

		System.assert(result != null);
	}

	@isTest static void createTableTest() {
		init();
		String mockBody = '{"result": "succes","error": false}';

		Map<String, String> headers = new Map<String, String>();
		headers.put('Content-Type', 'application/json');

		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(200, 'OK', mockBody, headers));

		Test.startTest();
		JsonDeserializerSimpleResponse result = HerokuWebService.createTable('Test');
		Test.stopTest();

		System.assert(result != null);
	}

	@isTest static void createTableWithError() {
		init();
		String mockBody = '{"result": "succes","error": false}';
		Map<String, String> headers = new Map<String, String>();
		headers.put('Content-Type', 'application/json');
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(404, 'OK', mockBody, headers));

		Test.startTest();
		JsonDeserializerSimpleResponse result = HerokuWebService.createTable('Test');
		Test.stopTest();
		System.assertEquals(null, result);
	}

	@isTest static void updateIndexTest() {

		init();

		String mockBody = '{"result": "succes", "error": false}';

		Map<String, String> headers = new Map<String, String>();
		headers.put('Content-Type', 'application/json');

		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(200, 'OK', mockBody, headers));

		Test.startTest();
		JsonDeserializerSimpleResponse result = HerokuWebService.updateIndex('Test');
		Test.stopTest();

		System.assert(result != null);
	}

	@isTest static void updateIndexWithError() {
		init();
		String mockBody = '{"result": "succes","error": false}';
		Map<String, String> headers = new Map<String, String>();
		headers.put('Content-Type', 'application/json');
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(404, 'OK', mockBody, headers));

		Test.startTest();
		JsonDeserializerSimpleResponse result = HerokuWebService.updateIndex('Test');
		Test.stopTest();
		System.assertEquals(null, result);
	}

	@isTest static void populateTableTest() {

		init();

		String mockBody = '{"result": [1,2,3], "error": false}';

		Map<String, String> headers = new Map<String, String>();
		headers.put('Content-Type', 'application/json');

		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(200, 'OK', mockBody, headers));

		Test.startTest();
		JsonDeserializerInsertResponse result = HerokuWebService.populateTable('Test');
		Test.stopTest();

		System.assert(result != null);
	}

	@isTest static void populateTableWithError() {
		init();
		String mockBody = '{"result": [1,2,3],"error": false}';
		Map<String, String> headers = new Map<String, String>();
		headers.put('Content-Type', 'application/json');
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(404, 'OK', mockBody, headers));

		Test.startTest();
		JsonDeserializerInsertResponse result = HerokuWebService.populateTable('Test');
		Test.stopTest();

		System.assertEquals(null, result);
	}

	@isTest static void getDocumentTest() {

		init();

		String mockBody =
			'{' +
			'"result": [' +
			'{' +
			'"id": 1,' +
			'"title": "SALESFORCE SUMMER ?16 RELEASE NOTES SUMMARY",' +
			'"summary": "Salesforce delivers powerful new business intelligence capabilities, more ways to collaborate on deals and projects, and even greater control over your data.",' +
			'"fts": {' +
			'"type": "tsvector",' +
			'"value": "way"' +
			'}' +
			'},' +
			'{' +
			'"id": 2,' +
			'"title": "Supported Browsers for Salesforce Classic",' +
			'"summary": "Salesforce Classic is supported with Microsoft? Internet Explorer? versions 9, 10, and 11, Apple? Safari? version 8.x on Mac OS X, and Microsoft? Edge for Windows? 10.",' +
			'"fts": {' +
			'"type": "tsvector",' +
			'"value": "way"' +
			'}' +
			'}' +
			'],' +
			'"error": false' +
			'}';

		Map<String, String> headers = new Map<String, String>();
		headers.put('Content-Type', 'application/json');

		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(200, 'OK', mockBody, headers));

		Test.startTest();
		List<Integer> intList = new List<Integer> {1, 2};
		JsonDeserializerDocuments result = HerokuWebService.getDocument(intList);
		Test.stopTest();

		System.assert(result != null);
	}

	@isTest static void deleteIndexForTableTest() {

		init();

		String mockBody = '{"result": "succes", "error": false}';

		Map<String, String> headers = new Map<String, String>();
		headers.put('Content-Type', 'application/json');

		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(200, 'OK', mockBody, headers));

		Test.startTest();
		JsonDeserializerSimpleResponse result = HerokuWebService.deleteIndexForTable('Test');
		Test.stopTest();

		System.assert(result != null);
	}

	@isTest static void getIndexInfoTest() {

		init();

		String mockBody =
			'{' +
			'"result": {' +
			'"index_scans": 0,' +
			'"isready": true,' +
			'"index": "knowledge__kav",' +
			'"table_size": "64 kB",' +
			'"indexes_size": "24 kB",' +
			'"total_table_space_size": "88 kB",' +
			'"database_size": "8328 kB",' +
			'"indexed_rows": 5,' +
			'"all_rows": 5' +
			'},' +
			'"error": false' +
			'}';

		Map<String, String> headers = new Map<String, String>();
		headers.put('Content-Type', 'application/json');

		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(200, 'OK', mockBody, headers));

		Test.startTest();
		IndexInfo result = HerokuWebService.getIndexInfo('Test');
		Test.stopTest();

		System.assert(result != null);
	}


	@isTest static void searchIndexTest() {

		init();

		String mockBody = '{"results": {' +
						  '"sqlstatement": "SELECT id, product_name FROM products WHERE to_tsvector(\u0027english\u0027,coalesce(description,\u0027\u0027) || \u0027 \u0027 || coalesce(product_name,\u0027\u0027)) @@ plainto_tsquery(\u0027need to connect\u0027)",' +
						  '"records": [' +
						  '{ "id": 33,' +
						  '"name": "New Article"}],' +
						  '"javatimemls": 1560},' +
						  '"error": false}';

		Map<String, String> headers = new Map<String, String>();
		headers.put('Content-Type', 'application/json');

		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(200, 'OK', mockBody, headers));

		Test.startTest();
		JsonDeserializer result = HerokuWebService.search(null, null, null, null);
		Test.stopTest();

		System.assert(result != null);
	}

	@isTest static void searchIndexWithError() {
		init();
		String mockBody = '{"results": {' +
						  '"sqlstatement": "SELECT id, product_name FROM products WHERE to_tsvector(\u0027english\u0027,coalesce(description,\u0027\u0027) || \u0027 \u0027 || coalesce(product_name,\u0027\u0027)) @@ plainto_tsquery(\u0027need to connect\u0027)",' +
						  '"records": [' +
						  '{ "id": 33,' +
						  '"name": "New Article"}],' +
						  '"javatimemls": 1560},' +
						  '"error": false}';

		Map<String, String> headers = new Map<String, String>();
		headers.put('Content-Type', 'application/json');
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(404, 'OK', mockBody, headers));

		Test.startTest();
		JsonDeserializer result = HerokuWebService.search(null, null, null, null);
		Test.stopTest();
		System.assertEquals(null, result);
	}

	@isTest static void searchIndexWithException() {
		init();
		Test.startTest();
		JsonDeserializer result = HerokuWebService.search(null, null, null, null);
		Test.stopTest();
		System.assertEquals(null, result);
	}

	@isTest static void checkIndexTest() {

		init();

		String mockBody = '{"results": "created",' +
						  '"sqlstatement": "resp",' +
						  '"error": false}';

		Map<String, String> headers = new Map<String, String>();
		headers.put('Content-Type', 'application/json');

		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(200, 'OK', mockBody, headers));

		Test.startTest();
		JsonDeserializer result = HerokuWebService.checkIndex(null);
		Test.stopTest();

		System.assert(result != null);
	}

	@isTest static void testGetSuggestionsWithException() {
		init();
		Test.startTest();
		Map<String, Integer> result = HerokuWebService.getSuggestions('query', null, null, null);
		Test.stopTest();
		System.assertEquals(null, result);
	}

	@isTest static void testGetSuggestionsWithError() {
		init();
		String mockBody = '{"error": true}';
		Map<String, String> headers = new Map<String, String>();
		headers.put('Content-Type', 'application/json');

		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(404, 'OK', mockBody, headers));
		Test.startTest();
		Map<String, Integer> result = HerokuWebService.getSuggestions('query', null, null, null);
		Test.stopTest();
		System.assertEquals(null, result);
	}

	@isTest static void testGetSuggestionsSuccess() {
		init();
		String mockBody = '{"results": {' +
						  '"sqlstatement": "SELECT id, product_name FROM products WHERE to_tsvector(\u0027english\u0027,coalesce(description,\u0027\u0027) || \u0027 \u0027 || coalesce(product_name,\u0027\u0027)) @@ plainto_tsquery(\u0027need to connect\u0027)",' +
						  '"records": [' +
						  '{ "id": 33,' +
						  '"name": "New Article"}],' +
						  '"javatimemls": 1560},' +
						  '"error": false}';
		Map<String, String> headers = new Map<String, String>();
		headers.put('Content-Type', 'application/json');

		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(200, 'OK', mockBody, headers));
		Test.startTest();
		Map<String, Integer> result = HerokuWebService.getSuggestions('query', null, null, null);
		Test.stopTest();
		System.assertEquals(2, result.size());
	}

	private static void init() {
		Web_Service_Settings__c customSetting = new Web_Service_Settings__c(Name = 'HerokuWebService');
		customSetting.Password__c = 'testPassword';
		customSetting.URL__c = 'test.heroku.com';
		customSetting.User__c = 'testUser';
		insert customSetting;

		TestClassUtility.createSearchEngineConfig('documents_first', AbstractSearchEngine.SALESFORCE_SEARCH_TYPE);
	}
}