@isTest
private class PostInstallScriptTest {

	@isTest
	private static void testName() {
		PostInstallScript postInstallScriptService = new PostInstallScript();
		postInstallScriptService.onInstall(null);
		System.assert(cncrg__Concierge_Settings__c.getAll().size() > 0);

		cncrg__Action_Setting__c defaultSetting = new cncrg__Action_Setting__c(
			cncrg__Active__c = true,
			cncrg__Log_a_Ticket__c = true
		);
		Database.insert(defaultSetting, false);
		postInstallScriptService.createActionSettings();
		System.assert([SELECT COUNT() FROM cncrg__Action_Setting__c WHERE cncrg__Source_System__c = 'salesforce'] > 0);
	}
}