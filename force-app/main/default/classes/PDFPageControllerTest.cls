@isTest
private class PDFPageControllerTest{
    @isTest
    public static void testPDFPageController() {
        String nameOfObject = 'cncrg__Knowledge__kav';
        sObject obj = Schema.getGlobalDescribe().get(nameOfObject).newSObject();
        obj.put('Title', 'TestArticle');
        obj.put('Summary', 'TestSummery');
        obj.put('UrlName', 'TestUrlName');
        insert obj;

        String kavId = (String) obj.get('Id');
        obj = Database.query('SELECT KnowledgeArticleId, Title,Summary FROM cncrg__Knowledge__kav WHERE Id = :kavId');
        String articleId = (String) obj.get('KnowledgeArticleId');
        PDFPageController pdf = new PDFPageController();
        String [] testarray = pdf.getStrings();
        String title = pdf.title;
		System.assertNotEquals(articleId, null);
    }
    @isTest
    public static void testGetFieldName() {
        PDFPageController pdf = new PDFPageController();
        String actualResault = pdf.getFieldName('test');
        System.assert(actualResault == '<b>test</b>');
    }

    @isTest
    public static void testConvertBigSizeImage() {
        PDFPageController pdf = new PDFPageController();
        String actualResault = pdf.convertBigSizeImage('test text test text width="1400" height="700"');
        System.assert(actualResault != null);
    }

    @isTest
    public static void testConverIframeToLink() {
        PDFPageController pdf = new PDFPageController();
        String actualResault = pdf.converIframeToLink('test text test text <iframe src="http//test@gmail.com" width="1400" height="700"></iframe>');
        System.assertNotEquals(actualResault, 'test text test text <a href="http//test@gmail.com">http//test@gmail.com</a>');
    }

    /*@isTest
    public static void testGetQueryString() {
        String nameOfObject = 'cncrg__Search_Types_Settings__c';
        sObject obj = Schema.getGlobalDescribe().get(nameOfObject).newSObject();
        obj.put('Fields_To_Display_1__c', 'Test1,Test2,');
        obj.put('Name', 'test');
        obj.put('cncrg__Fields_To_Display_2__c', 'Test3,');
        obj.put('cncrg__Article_Type_API_Name__c', 'Knowledge__kav');
        insert obj;
        List<sObject> testList = new List<sObject>();
        testList.add(obj);
        PDFPageController pdf = new PDFPageController();
        pdf.articleType = 'Knowledge__kav';
        Boolean actualResault = pdf.getQueryString(testList, '12345678').contains('Test1,Test2,Test3,');
        Boolean expectedResault = true;
        System.assert(actualResault == expectedResault);
    }*/
}