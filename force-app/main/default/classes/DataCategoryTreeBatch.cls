global class DataCategoryTreeBatch extends AbstractConciergeBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {
	public static final String FILE_NAME = 'DataCategoryTree.json';

	private String fileBody;
	private String sessionId;
	private Boolean removeFile;

	public DataCategoryTreeBatch() {
		sessionId = UserInfo.getSessionId();
		removeFile = true;
	}

	public DataCategoryTreeBatch(String sesId, List<Object> params) {
		sessionId = sesId;
		removeFile = false;
		super.setInitParams(params);
	}

	public Iterable<sObject> start(Database.BatchableContext BC) {
		List<sObject> result = new List<sObject>();
		result.add(getDataCategoryTeeFromFile(removeFile));
		return result;
	}

	public void execute(Database.BatchableContext context, List<ContentVersion> scope) {
		if (!scope.isEmpty() && scope[0].versionData != null) {
			fileBody = scope[0].versionData.toString();
		}

		if (String.isNotBlank(fileBody)) {
			List<KnowledgeDataCategoryService.DataCategoryWrapper> tree = (List<KnowledgeDataCategoryService.DataCategoryWrapper>)JSON.deserialize(fileBody, List<KnowledgeDataCategoryService.DataCategoryWrapper>.class);
			KnowledgeDataCategoryService.DataCategoryWrapper nextCategoryForDescribe = findCategoryForDescribe(tree);
			if (nextCategoryForDescribe != null) {
				KnowledgeDataCategoryService.TopCategoryWrapper nextDescribe = KnowledgeDataCategoryService.getDetailsByDataCategory(nextCategoryForDescribe.endPoint.split('\\.')[0], nextCategoryForDescribe.dataCategoryName, sessionId);
				if (nextDescribe.childCategories != null && !nextDescribe.childCategories.isEmpty()) {
					for (KnowledgeDataCategoryService.TopCategoryWrapper newEl : nextDescribe.childCategories) {
						nextCategoryForDescribe.childList.add(
							new KnowledgeDataCategoryService.DataCategoryWrapper(
								newEl.name,
								nextCategoryForDescribe.endPoint
							)
						);
					}
				} else {
					nextCategoryForDescribe.isCollapse = false;
				}

				fileBody = JSON.serialize(tree);
			}
		} else {
			KnowledgeDataCategoryService.DataCategoryContainer categories = KnowledgeDataCategoryService.requestToDataCategory(sessionId);
			fileBody = JSON.serialize(KnowledgeDataCategoryService.parseDataCategory(categories));
		}
	}

	public override void finish(Database.BatchableContext context) {
		saveTreeIntoFile(fileBody);

		List<KnowledgeDataCategoryService.DataCategoryWrapper> tree = (List<KnowledgeDataCategoryService.DataCategoryWrapper>)JSON.deserialize(fileBody, List<KnowledgeDataCategoryService.DataCategoryWrapper>.class);
		KnowledgeDataCategoryService.DataCategoryWrapper nextCategoryForDescribe = findCategoryForDescribe(tree);

		if (nextCategoryForDescribe != null) {
			Database.executeBatch(new DataCategoryTreeBatch(sessionId, super.getInitParams()));
		} else {
			try {super.finish(context); } catch (Exception e) { System.debug('Exception!' + e.getStackTraceString()); }
		}
	}

	private KnowledgeDataCategoryService.DataCategoryWrapper findCategoryForDescribe(List<KnowledgeDataCategoryService.DataCategoryWrapper> tree) {
		for (Integer i = 0; i < tree.size(); i++) {
			if (tree[i].childList.isEmpty() && tree[i].isCollapse) {
				return tree[i];
			} else {
				KnowledgeDataCategoryService.DataCategoryWrapper result = findCategoryForDescribe(tree[i].childList);
				if (result != null) {
					return result;
				}
			}
		}
		return null;
	}

	public static ContentVersion getDataCategoryTeeFromFile(Boolean isRemove) {
		ContentVersion tree = new ContentVersion();
		if (String.isNotBlank(getFileDocumentId())) {
			tree = [
				SELECT VersionData, ContentDocumentId
				FROM ContentVersion
				WHERE Id IN (
					SELECT LatestPublishedVersionId
					FROM ContentDocument
					WHERE Title = :FILE_NAME)
				LIMIT 1
			];

			if (isRemove) {
				ESAPI.securityUtils().validatedDelete(new List<sObject> {
					new ContentDocument(Id = tree.ContentDocumentId)
				});
				tree = new ContentVersion();
			}
		}
		return tree;
	}

	@TestVisible
	private void saveTreeIntoFile(String content) {
		String fileDocumentId = getFileDocumentId();
		Boolean isNew = String.isBlank(fileDocumentId);

		ContentVersion newFile = new ContentVersion();
		newFile.title = FILE_NAME;
		newFile.ContentDocumentId = fileDocumentId;
		newFile.pathOnClient = FILE_NAME;
		newFile.versionData = Blob.valueOf(content);
		ESAPI.securityUtils().validatedInsert(new List<sObject> {newFile});

		newFile = [SELECT id, ContentDocumentId FROM ContentVersion WHERE Id = : newFile.Id];
		if (isNew) {
			openAccessToFile(newFile.ContentDocumentId);
		}
	}

	private static String getFileDocumentId() {
		String result;
		List<ContentVersion> fileList = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id IN (SELECT LatestPublishedVersionId FROM ContentDocument WHERE Title = :FILE_NAME) LIMIT 1];
		if (!fileList.isEmpty()) {
			result = fileList[0].ContentDocumentId;
		}
		return result;
	}

	private void openAccessToFile(Id contentDocumentId) {
		ContentDocumentLink cl = new ContentDocumentLink();
		cl.ContentDocumentId = contentDocumentId;
		cl.LinkedEntityId = UserInfo.getOrganizationId();
		cl.ShareType = 'V';
		cl.Visibility = 'AllUsers';
		ESAPI.securityUtils().validatedInsert(new List<sObject> {cl});
	}
}