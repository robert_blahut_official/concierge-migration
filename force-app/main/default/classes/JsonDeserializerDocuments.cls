public with sharing class JsonDeserializerDocuments {

    public static void consumeObject(JSONParser parser) {
        Integer depth = 0;
        do {
            JSONToken curr = parser.getCurrentToken();
            if (curr == JSONToken.START_OBJECT ||
                curr == JSONToken.START_ARRAY) {
                depth++;
            } else if (curr == JSONToken.END_OBJECT ||
                curr == JSONToken.END_ARRAY) {
                depth--;
            }
        } while (depth > 0 && parser.nextToken() != null);
    }

    public class Fts {
        public String type_Z {get;set;} // in json: type
        public String value {get;set;}

        public Fts(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'type') {
                            type_Z = parser.getText();
                        } else if (text == 'value') {
                            value = parser.getText();
                        } else {consumeObject(parser);}
                    }
                }
            }
        }
    }

    public List<Result> result {get;set;}
    public Boolean error {get;set;}

    public JsonDeserializerDocuments(JSONParser parser) {
        while (parser.nextToken() != JSONToken.END_OBJECT) {
            if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                String text = parser.getText();
                if (parser.nextToken() != JSONToken.VALUE_NULL) {
                    if (text == 'result') {
                        result = new List<Result>();
                        while (parser.nextToken() != JSONToken.END_ARRAY) {
                            result.add(new Result(parser));
                        }
                    } else if (text == 'error') {
                        error = parser.getBooleanValue();
                    } else {
                        //System.debug(LoggingLevel.WARN, 'Root consuming unrecognized property: '+text);
                        consumeObject(parser);
                    }
                }
            }
        }
    }

    public class Result {
        public Integer id {get;set;}
        public String title {get;set;}
        public String summary {get;set;}
        public Fts fts {get;set;}

        public Result(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'id') {
                            id = parser.getIntegerValue();
                        } else if (text == 'title') {
                            title = parser.getText();
                        } else if (text == 'summary') {
                            summary = parser.getText();
                        } else if (text == 'fts') {
                            fts = new Fts(parser);
                        } else {consumeObject(parser);}
                    }
                }
            }
        }
    }

    public static JsonDeserializerDocuments parse(String json) {
        return new JsonDeserializerDocuments(System.JSON.createParser(json));
    }

}