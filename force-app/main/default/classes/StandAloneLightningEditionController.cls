/**
* @description

	This class contains logic for search into Lightning Knowledges

* @methods
	getSearchResults - method for getting search results
	getFavoritesbyId - method for getting favorites records from Lightning Knowledges by Ids
*/
public with sharing class StandAloneLightningEditionController extends AbstractSearchEngine implements SearchEngineInterface {

	public List<SearchResultsWrapper> getSearchResults(String searchTerm, String prefix) {
		List<SearchResultsWrapper> results = new List<SearchResultsWrapper> ();
		searchTerm = searchTerm.toLowerCase().trim();

		if (searchTerm.trim().length() > 1 || Utils.containsDoubleByteCharacters(searchTerm)) {
			Map<String, Utils.sObjectWrapper> mapOfTypesSettings = Utils.prepareTypesSettings(false);
			results = getKnowledgeSOSLResults(searchTerm, mapOfTypesSettings.keySet());
			if (results.size() == 0) {
				results = getKnowledgeSOSLResults('*' + searchTerm + '*', mapOfTypesSettings.keySet());
			}
		}
		return results;
	}

	private List<SearchResultsWrapper> getKnowledgeSOSLResults(String searchTerm, Set<String> typesList) {
		List<SearchResultsWrapper> results = new List<SearchResultsWrapper>();

		List<Search.SearchResult> searchResultsList = this.getSOSLResults(searchTerm, typesList);
		Integer indicatorLiveTime = this.getUpdateIndicatorTime();

		for (Search.SearchResult searchResult : searchResultsList) {
			results.add(
				this.getInitialSearchResultWrapper(
					searchResult.getSObject(),
					searchResult.getSnippet(),
					indicatorLiveTime
				)
			);
		}
		return results;
	}

	private List<Search.SearchResult> getSOSLResults(String searchTerm, Set<String> typesList) {
		String knowledgeObjectName = Utils.getLightningKnowledgeObjectName();
		String snippetLength = Utils.getValueFromConciergeSettings(SNIPPET_LENGTH_SETTING_NAME);
		String channelFilter = Utils.getValueFromConciergeSettings(CHANNEL_FILTER_SETTING_NAME);
		//@IMPORTANT: implemented article filters by Language
		String locale = Utils.getUserLanguage();
		//@IMPORTANT: implemented article filters by Data Category
		List<String> articleFilters = Utils.getArticleFilters();

		Integer MAX_RESULTS = Integer.valueOf(cncrg__Search_Engine_Configurations__c.getInstance(ENGINE_CONFIG_SETTING_NAME).cncrg__Maximum_number_of_articles_to_retrieve__c);

		String query = 'FIND \'' + Utils.escapeSOSLQuery(searchTerm) + '\' IN ALL FIELDS RETURNING ';
		query += knowledgeObjectName + ' ( ';
		query += String.join((Iterable<String>)this.getRequiredFieldsForSearch(), ',');
		query += ' WHERE PublishStatus = \'' + ONLINE_PUBLISH_STATUS + '\'';
		query += ' AND Language =  \'' + locale + '\'';
		query +=  (!Test.isRunningTest() ? ' AND RecordType.Name IN :typesList ' : '');
		if (String.isNotBlank(channelFilter)) {
			query += ' AND ' + CHANNELS_MAP.get(channelFilter) + ' = true ';
		}
		query += ' ) ';

		if (!articleFilters.isEmpty() && String.isNotBlank(articleFilters.get(0))) {
			query += ' WITH DATA CATEGORY ' + articleFilters.get(0);
		}
		query += ' WITH SNIPPET ' + (String.isNotBlank(snippetLength) ? '(target_length=' + snippetLength + ')' : '');
		query += ' LIMIT ' + MAX_RESULTS;

		Search.SearchResults searchResults = Search.find(query);
		return searchResults.get(knowledgeObjectName);
	}

	public override List<Object> getFavoritesbyId(List<String> recordIds, String prefix) {
		String knowledgeArticleObjectName = Utils.getLightningKnowledgeObjectName();
		Set<String> typesList = Utils.prepareTypesSettings(false).keySet();

		String channelFilter = Utils.getValueFromConciergeSettings(CHANNEL_FILTER_SETTING_NAME);
		String locale = Utils.getUserLanguage();
		String publishStatus = ONLINE_PUBLISH_STATUS;

		String query = 'SELECT Id, Title, CreatedDate, LastModifiedDate, KnowledgeArticleId';
		query += (!Test.isRunningTest() ? ' ,RecordType.Name' : '');
		query += ' FROM ' + knowledgeArticleObjectName;
		query += ' WHERE KnowledgeArticleId IN :recordIds ';
		query += ' AND PublishStatus = :publishStatus ';
		query += ' AND Language = :locale ';
		query +=  (!Test.isRunningTest() ? ' AND RecordType.Name IN :typesList ' : '');
		if (String.isNotBlank(channelFilter)) {
			query += ' AND ' + CHANNELS_MAP.get(channelFilter) + ' = true';
		}
		query += ' LIMIT 50000';

		List<FavoriteArticlesWrapper> result = new List<FavoriteArticlesWrapper>();
		for (SObject item : Database.query(query)) {
			FavoriteArticlesWrapper favorite = new FavoriteArticlesWrapper(item, 'blankBody', (String) item.get('Title'));
			favorite.knowledgeArticleId = (String)item.get('KnowledgeArticleId');
			result.add(favorite);
		}
		return result;
	}

	public override FavoriteArticlesWrapper getFavoriteArticleDetail(FavoriteArticlesWrapper favorite, String prefix) {
		Map<String, Utils.SObjectWrapper> mapOfTypesSettings = Utils.prepareTypesSettings(false);
		if (mapOfTypesSettings.isEmpty()) {
			return favorite;
		}

		String articleObjectName = favorite.article.getSObjectType().getDescribe().getName();
		String sObjectType = this.getArticleType(favorite.article);
		String favoriteId = (String) favorite.article.get('KnowledgeArticleId');
		String locale = Utils.getUserLanguage();
		String publishStatus = ONLINE_PUBLISH_STATUS;
		Utils.SObjectWrapper objectConfig = mapOfTypesSettings.get(sObjectType);

		Set<String> fieldsToQuery = new Set<String>();
		fieldsToQuery.add('Id');
		fieldsToQuery.addAll(this.getRequiredFieldsForSearch());
		fieldsToQuery.addAll(mapOfTypesSettings.get(sObjectType).availableFieldsList);
		Set<String> hidenSystemFields = Utils.splitUnionFields(new Map<String, Utils.SObjectWrapper>{
			sObjectType => objectConfig
		});
		fieldsToQuery.addAll(hidenSystemFields);

		String query = 'SELECT ' + String.join((Iterable<String>)fieldsToQuery, ',');
		query += ' FROM ' + articleObjectName;
		query += ' WHERE KnowledgeArticleId =: favoriteId ';
		query += ' AND PublishStatus =: publishStatus ';
		query += ' AND Language =: locale ';
		query += ' UPDATE VIEWSTAT ';

		sObject searchResult = Database.query(query);
		favorite.showRelatedAttachments = objectConfig.showAttachments;
		favorite.body = this.generateFavoriteBody(searchResult, prefix, objectConfig);
		return favorite;
	}

	public override Set<String> getKnowledgeObjectApiNames(List<SearchResultsWrapper> articles) {
		return new Set<String> { Utils.getLightningKnowledgeObjectName() };
	}

	public override String getArticleType(sObject article) {
		return !Test.isRunningTest() ? (String) article.getSObject('RecordType').get('Name') : '';
	}

	public override Set<String> getRequiredFieldsForSearch() {
		Set<String> initialSet = new Set<String> {
			'Id',
			'Title',
			'Summary',
			'CreatedDate',
			'LastModifiedDate',
			'KnowledgeArticleId',
			'LastPublishedDate',
			'FirstPublishedDate',
			'OwnerId'
		};
		if (!Test.isRunningTest()) {
			initialSet.add('RecordType.Name');
		}
		if (Utils.isArticlesHaveMasterVersionIdField()) {
			initialSet.add('MasterVersionId');
		}
		return initialSet;
	}
}