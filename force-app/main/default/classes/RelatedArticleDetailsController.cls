public class RelatedArticleDetailsController {
	@AuraEnabled
	public static List<Object> getInitialData(String href, String prefix) {
		List<String> hrefList = EncodingUtil.urlDecode(href, 'UTF-8').split('/');
		Integer hrefListLength = hrefList.size();
		hrefList[hrefListLength - 2] += '__kav';

		String query = 'SELECT ';
		List<String> avalaibleFieldsList = Utils.getAllAvailableFields(hrefList[hrefListLength - 2]);
		for (String item : avalaibleFieldsList) {
			query += item + ', ';
		}
		query = (Utils.isLightningKnowledgeEnabled() ? query + 'RecordType.Name ' : query.removeEnd(', '));
		query += ' FROM ' + hrefList[hrefListLength - 2] + ' WHERE UrlName = \'' + hrefList[hrefListLength - 1] + '\'';
		query += (hrefList[hrefListLength - 3] != 'articles' ? ' AND Language = \'' + hrefList[hrefListLength - 3] + '\'' : '');
		query += ' UPDATE VIEWSTAT ';

		sObject articleRecordFromBase = Database.query(query);
		String masterVersionId;
		try {
			masterVersionId = (String) articleRecordFromBase.get('MasterVersionId');
		} catch (Exception e) {
			masterVersionId = (String) articleRecordFromBase.get('Id');
		}

		String knowledgeArticleId = (String) articleRecordFromBase.get('KnowledgeArticleId');
		String articleType = Utils.isLightningKnowledgeEnabled() ? (String) articleRecordFromBase.getSobject('RecordType').get('Name') : hrefList[hrefListLength - 2];
		Map<String, Utils.sObjectWrapper> fieldToDisplayNameMap = Utils.prepareTypesSettings(Utils.getSearchTypesSettingsByArticleAPIName(new Set<String> {articleType}), false);

		SearchResultsWrapper searchResultsWrapperRecord = new SearchResultsWrapper(
			(String)articleRecordFromBase.get('Title'),
			'',
			(String)articleRecordFromBase.get('Id'),
			'blankValue',
			articleType,
			null,
			knowledgeArticleId,
			masterVersionId
		);
		searchResultsWrapperRecord.showRelatedAttachments = fieldToDisplayNameMap.get(articleType).showAttachments;

		List<String> fieldToDisplayNameList = fieldToDisplayNameMap.get(articleType).availableFieldsList;
		Set<String> notShowingFieldSet = Utils.splitUnionFields(fieldToDisplayNameMap);
		SearchResultsWrapper relatedArticle = Utils.getArticleFieldsHTML(searchResultsWrapperRecord, prefix, articleRecordFromBase, fieldToDisplayNameList, notShowingFieldSet);

		return new List<Object> { Utils.initEngine().getAllArticleSettings(relatedArticle, prefix) };
	}

	@AuraEnabled
	public static String toggleFavoriteArticle(String recordId, Boolean isFavorite) {
		return SearchQueryInputController.toggleFavoriteAPEX(recordId, isFavorite);
	}

	@AuraEnabled
	public static String rateItem(String item) {
		return SearchQueryInputController.rateItem(item);
	}

	@AuraEnabled
	public static List<Object> getInitialSettings() {
		return new List<Object> { SearchQueryInputController.getAlwaysDiaplayLogTicket(), SearchQueryInputController.getAllowUsersToPrintArticlesInPDF() };
	}
}