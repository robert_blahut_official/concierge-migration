global class PostInstallScript implements InstallHandler {

	global void onInstall(InstallContext context) {
		// CREATE Default Case Layout Settings
		List<cncrg__Case_Layout_Assignment__c> caseLayouts = [SELECT Id FROM cncrg__Case_Layout_Assignment__c];
		if (caseLayouts.isEmpty()) {
			cncrg__Case_Layout_Assignment__c caseLayoutRecord = new cncrg__Case_Layout_Assignment__c(
				cncrg__Field_Set__c = 'cncrg__Base_Create_Case'
			);
			Database.insert(new List<cncrg__Case_Layout_Assignment__c> {caseLayoutRecord}, false);	
		}
		// CREATE Admin Profile Setting
		List<cncrg__ConciergeAdminProfiles__c> admProfiles = new List<cncrg__ConciergeAdminProfiles__c> {
			new cncrg__ConciergeAdminProfiles__c(
				Name = 'System Administrator'
			)
		};
		Database.insert(admProfiles, false);
		// CREATE Search Settings
		cncrg__Search_Settings__c searchSettingsConfig = cncrg__Search_Settings__c.getOrgDefaults();
		if (searchSettingsConfig.Id == null) {
			searchSettingsConfig.cncrg__Search_History__c = 15;
			searchSettingsConfig.cncrg__Votes__c = 3;
			searchSettingsConfig.cncrg__Age__c = 2;
			searchSettingsConfig.cncrg__Relevance__c = 80;
			ESAPI.securityUtils().validatedInsert(new List<cncrg__Search_Settings__c> {searchSettingsConfig});
		}

		// CREATE Trigger Configuration
		createTriggersConfiguration();
		// CREATE Engine Configuration
		createEngineConfiguration();
		// CERATE Concierge Settings
		createConciergeSettings();
		// CREATE Concierge Background
		createBackgroundSettings();
		// CREATE Action Setting/ Update Source System var
		createActionSettings();
		// CREATE Concierge field mapping
		createFieldMapping();
		// CREATE Schedule Settings
		createScheduleSettingsAndScheduleJob();
	}

	public void createTriggersConfiguration() {
		Map<String, Boolean> defaultSetting = new Map<String, Boolean> {
			'Case Sharing Trigger' => false,
			'Contact Case Sharing Trigger' => false,
			'User Case Sharing Trigger' => false
		};

		Map<String, cncrg__Trigger_Activator__c> allSettings = cncrg__Trigger_Activator__c.getAll();
		List<cncrg__Trigger_Activator__c> triggerActiv = new List<cncrg__Trigger_Activator__c>();

		for (String settingName: defaultSetting.keySet()) {
			if (!allSettings.containsKey(settingName)) {
				triggerActiv.add(
					new cncrg__Trigger_Activator__c(
						Name = settingName,
						cncrg__Active__c = defaultSetting.get(settingName)
					)
				);
			}
		}

		if (!triggerActiv.isEmpty()) {
			Database.insert(triggerActiv, false);
		}
	}

	public void createEngineConfiguration() {
		List<cncrg__Search_Engine_Configurations__c> engineConfig = [
			SELECT Id, Name, cncrg__Engine_Type__c
			FROM cncrg__Search_Engine_Configurations__c
			WHERE Name =: AbstractSearchEngine.ENGINE_CONFIG_SETTING_NAME
		];
		if (engineConfig.isEmpty()) {
			engineConfig.add(new cncrg__Search_Engine_Configurations__c(
				Name = AbstractSearchEngine.ENGINE_CONFIG_SETTING_NAME,
				cncrg__Engine_Type__c = AbstractSearchEngine.SALESFORCE_SEARCH_TYPE,
				cncrg__Max_Suggestions__c = 10,
				cncrg__Maximum_number_of_articles_to_retrieve__c = 200
			));
			Database.insert(engineConfig, false);
		}

		// WARNING: 'Stand Alone' - the old name of Salesforce config
		// This code can be deleted after V2.18 installed on Customer orgs
		if (!engineConfig.isEmpty() && engineConfig.get(0).Id != null && engineConfig.get(0).cncrg__Engine_Type__c == 'Stand Alone') {
			engineConfig.get(0).cncrg__Engine_Type__c = AbstractSearchEngine.SALESFORCE_SEARCH_TYPE;
			Database.update(engineConfig, false);
		}
	}

	public void createConciergeSettings() {
		Map<String, String> defaultSetting = new Map<String, String> {
			'Always Display Log a Ticket' => 'true',
			'Background file name for App' => 'Waterfall',
			'Background For App on Mobile Devices' => 'Bridge',
			'Application Name' => 'Concierge',
			'CompanyLogo' => 'Company Business Card Logo',
			'Display Company Logo' => 'true',
			'Number of Articles Displayed in Search' => '5',
			'Case Origin' => 'Concierge',
			'Case Close Status' => 'Closed',
			'Cases per Chunk' => '5',
			'Enable users to close their own case' => 'true',
			'Number of Cases Displayed' => '20',
			'Populate Contact ID on Case' => 'false',
			'Name of Case Status Indicator Field' => 'cncrg__Status_Indicator__c',
			'Article Score Half Life (Days)' => '10',
			'Update Interval' => '10000',
			'Channel Type For Search' => 'Internal App',
			'Max Number of Favorite Articles' => '1000',
			'Search Log Email Address' => '',
			'Search Log User Id (15 characters)' => '',
			'Text to Display in Article Preview' => 'true',
			'Allow Users to Print Articles in PDF' => 'true',
			'Snippet Length' => '120',
			ArticleReviewUtils.AUTOMATICALLY_REVIEW_CUSTOM_SETTINGS_NAME => ArticleReviewUtils.AUTOMATICALLY_REVIEW_CUSTOM_SETTINGS_DEFAULT,
			'Pass Search Term to Suggestions' => 'true',
			'Article Actions Display Order' => 'Log a Ticket;Call;Link;Live Chat;Email;Flow;',
			'Hours New/Updated Flag is Displayed' => '24',
			Utils.CONCIERGE_TABS_VISIBILITY_SETTING_NAME => 'Favorites;Home;My_Tickets'
		};

		Map<String, cncrg__Concierge_Settings__c> allSettings = cncrg__Concierge_Settings__c.getAll();
		List<cncrg__Concierge_Settings__c> conciergeSettings = new List<cncrg__Concierge_Settings__c>();

		for (String settingName: defaultSetting.keySet()) {
			if (!allSettings.containsKey(settingName)) {
				conciergeSettings.add(
					new cncrg__Concierge_Settings__c(
						Name = settingName,
						cncrg__Value__c = defaultSetting.get(settingName)
					)
				);
			}
		}
		if (!conciergeSettings.isEmpty()) {
			Database.insert(conciergeSettings, false);
		}
	}

	public void createScheduleSettingsAndScheduleJob() {
		Set<String> activeSettings = new Set<String> {
			TurboEncabulatorScheduler.TURBO_BATCH_NAME,
			TurboEncabulatorScheduler.FEED_BATCH_NAME
		};
		Map<String, Integer> batchSizeSettings = new Map<String, Integer> {
			TurboEncabulatorScheduler.TURBO_BATCH_NAME => TurboEncabulatorScheduler.TURBO_BATCH_SIZE,
			TurboEncabulatorScheduler.REVIEW_BATCH_NAME => TurboEncabulatorScheduler.REVIEW_BATH_SIZE,
			TurboEncabulatorScheduler.FEED_BATCH_NAME => TurboEncabulatorScheduler.FEED_BATH_SIZE,
			'cncrg.DataCategoryTreeBatch' => TurboEncabulatorScheduler.TURBO_BATCH_SIZE
		};

		Map<String, List<String>> defaultSetting = new Map<String, List<String>> {
			TurboEncabulatorScheduler.TURBO_BATCH_NAME => new List<String> {
				TurboEncabulatorScheduler.TURBO_BATCH_NAME,
				TurboEncabulatorScheduler.TURBO_CRON,
				TurboEncabulatorScheduler.TURBO_DESCRIPTION
			},
			TurboEncabulatorScheduler.REVIEW_BATCH_NAME => new List<String> {
				TurboEncabulatorScheduler.REVIEW_BATCH_NAME,
				TurboEncabulatorScheduler.REVIEW_CRON,
				TurboEncabulatorScheduler.REVIEW_DESCRIPTION
			},
			TurboEncabulatorScheduler.FEED_BATCH_NAME => new List<String> {
				TurboEncabulatorScheduler.FEED_CLASS_NAME,
				TurboEncabulatorScheduler.FEED_CRON,
				TurboEncabulatorScheduler.FEED_DESCRIPTION
			},
			'cncrg.DataCategoryTreeBatch' => new List<String> {
				'cncrg.DataCategoryTreeBatch',
				'0 * * * * 4',
				'Generate Data Categories Tree'
			}
		};

		Map<String, SchedulerSettings__c> allSettings = SchedulerSettings__c.getAll();
		List<SchedulerSettings__c> schedSettingList = new List<SchedulerSettings__c>();

		for (String settingName: defaultSetting.keySet()) {
			if (!allSettings.containsKey(settingName)) {
				schedSettingList.add(
					new SchedulerSettings__c(
						Name = settingName,
						Class_Name__c = defaultSetting.get(settingName).get(0),
						CRON__c = defaultSetting.get(settingName).get(1),
						Description__c = defaultSetting.get(settingName).get(2),
						IsStandard__c = true,
						Batch_Size__c = batchSizeSettings.get(settingName),
						Active__c = activeSettings.contains(settingName)
					)
				);
			}
		}

		if (!schedSettingList.isEmpty()) {
			Database.insert(schedSettingList, false);

			// SCHEDULE Batch Job
			Integer nextStep = DateTime.now().addMinutes(10).minute();
			try {
				TurboEncabulatorScheduler.startScheduler('0 ' + String.ValueOf(nextStep) + ' * * * ?', TurboEncabulatorScheduler.JOB_NAME);
			} catch (Exception e) {}
		}
	}

	public void createActionSettings() {
		Integer settingsNumber = [
			SELECT COUNT()
			FROM cncrg__Action_Setting__c
		];
		if (settingsNumber == 0) {
			cncrg__Action_Setting__c defaultSetting = new cncrg__Action_Setting__c(
				cncrg__Active__c = true,
				cncrg__Log_a_Ticket__c = true,
				cncrg__Source_System__c = 'salesforce'
			);
			Database.insert(defaultSetting, false);
		}
		List<cncrg__Action_Setting__c> actionSettings = [
			SELECT Id, cncrg__Source_System__c
			FROM cncrg__Action_Setting__c
			WHERE cncrg__Source_System__c = null
		];
		for (cncrg__Action_Setting__c setting: actionSettings) {
			setting.cncrg__Source_System__c = 'salesforce';
		}
		if (!actionSettings.isEmpty()) {
			Database.update(actionSettings, false);
		}
	}

	public void createBackgroundSettings() {
		Map<String, List<String>> defaultSetting = new Map<String, List<String>> {
			'Waterfall' => new List<String> {'cncrg__Waterfall', 'Video'},
			'Bridge' => new List<String> {'cncrg__MainBackground', 'Image'}
		};

		Map<String, cncrg__Background_Settings__c> allSettings = cncrg__Background_Settings__c.getAll();
		List<cncrg__Background_Settings__c> bgSettings = new List<cncrg__Background_Settings__c>();

		for (String settingName: defaultSetting.keySet()) {
			if (!allSettings.containsKey(settingName)) {
				bgSettings.add(
					new cncrg__Background_Settings__c(
						Name = settingName,
						cncrg__File_Location__c = defaultSetting.get(settingName).get(0),
						cncrg__Background_File_Type__c = defaultSetting.get(settingName).get(1)
					)
				);
			}
		}

		if (!bgSettings.isEmpty()) {
			Database.insert(bgSettings, false);
		}
	}

	public void createFieldMapping() {
		Map<String, String> defaultSetting = new Map<String, String> {
			'0' => 'Country:cncrg__Country__c',
			'1' => 'Department:cncrg__Department__c',
			'2' => 'cncrg__How_Long_Since_User_Was_Created__c:cncrg__How_Long_Since_User_Was_Created__c',
			'3' => 'LanguageLocaleKey:cncrg__Language__c',
			'4' => 'LocaleSidKey:cncrg__Locale__c',
			'5' => 'Role:cncrg__Portal_Role__c',
			'6' => 'Profile:cncrg__Profile__c',
			'7' => 'State:cncrg__State__c',
			'7' => 'UserType:cncrg__User_Type__c'
		};

		Map<String, cncrg__Concierge_Field_Mappings__c> allSettings = cncrg__Concierge_Field_Mappings__c.getAll();
		List<cncrg__Concierge_Field_Mappings__c> conciergeFieldMappingsList = new List<cncrg__Concierge_Field_Mappings__c>();

		for (String settingName: defaultSetting.keySet()) {
			if (!allSettings.containsKey(settingName)) {
				conciergeFieldMappingsList.add(
					new cncrg__Concierge_Field_Mappings__c(
						Name = settingName,
						cncrg__Value__c = defaultSetting.get(settingName)
					)
				);
			}
		}

		if (!conciergeFieldMappingsList.isEmpty()) {
			Database.insert(conciergeFieldMappingsList, false);
		}
	}
}