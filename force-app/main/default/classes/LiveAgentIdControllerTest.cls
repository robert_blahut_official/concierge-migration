@isTest
private class LiveAgentIdControllerTest {
	 private static void init() {

        cncrg__Live_Agent_Settings__c customSetting = new cncrg__Live_Agent_Settings__c(Name = 'LA_Setting');

        insert customSetting;

    }

    @isTest
    public static void testLiveAgentIdController() {
        init();
        LiveAgentIdController la = new LiveAgentIdController();

        System.assert(la.live_Agent_Settings!=null);
    }

}