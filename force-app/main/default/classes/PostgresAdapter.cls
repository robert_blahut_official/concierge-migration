/**
* @description 

	This class contains logic for search from PostgreSQL

* @methods
	getFavoritesbyId -					getting article records by Ids 
	getSuggestionsForQuery -			method for getting suggestions
	getSearchResults -					method for getting search results
	toggleFavoriteAPEX -				method for adding or deleting Favorite articles
	updateArticleBody -					modification body of Business cards article
*/ 

public with sharing class PostgresAdapter implements SearchEngineInterface {

	
	public static String KNOWLEDGE_TYPE_NAME = 'Knowledge__kav';

    public List<Integer> recordsFromPostIds;

    @TestVisible
    private static Boolean isTest = false;

    public class favoriteArticleObject {
        @TestVisible
        String id;
        String title;
        String summary;

        public favoriteArticleObject(String id, String title, String summary){
            this.id = id;
            this.title = title;
            this.summary = summary;
        }
    }

    public List<Object> getFavoritesbyId(List<String> recordIds, String prefix) {

        List<Integer> docIds = new List<Integer>();
        List<SObject> allRecords = new List<SObject>();
		List<Object> result = new List<Object>();

		for(String item : recordIds) {
            if(item.isNumeric()){
                docIds.add(Integer.valueOf(item));
            }
        }
		
		String companyLogo = Utils.getCompanyLogoName(prefix);

        if(!docIds.isEmpty()){

            JsonDeserializerDocuments documents = HerokuWebService.getDocument(docIds);

            if(documents != null) {
                for(JsonDeserializerDocuments.Result docItem : documents.result) {
                    try{
                        SObject temp = Schema.getGlobalDescribe().get(
                            PostgresAdapter.KNOWLEDGE_TYPE_NAME
                        ).newSObject();
                        temp.put('title', String.valueOf(docItem.title));
                        temp.put('summary', String.valueOf(docItem.summary));
                        temp.put('Id', String.valueOf(docItem.id));
                        allRecords.add(temp); 
						} catch(Exception e){ 
						}
                }
            }
        }

		result = getClassicFavoriteArticlesWrapperList(companyLogo,allRecords);
        
		return result;
    }

	public static List<FavoriteArticlesWrapper> getClassicFavoriteArticlesWrapperList(String companyLogo, List<sObject> articlesList) {
	
        List<Integer> listOfInteractiveArticles = new List<Integer> ();
        Set<String> allInteractiveFields = new Set<String> ();

        List<FavoriteArticlesWrapper> favoriteArticlesWrapperList = new List<FavoriteArticlesWrapper>();

        for (sObject article : articlesList) {

            String body = ''; //(String)article.get('Summary');
            String typeName = (String) article.get('ArticleType');
  
			body = (String) article.get('summary');
                
			if (body.contains('{!User.')) { //summary != null &&
                    listOfInteractiveArticles.add(favoriteArticlesWrapperList.size());
                    allInteractiveFields.addAll(Utils.getAllInteractiveFields(body));
            }

            if (companyLogo != null && body.contains('{!Document.CompanyLogo}')) {
                    body = body.replaceAll(
                                           '\\{!Document.CompanyLogo}',
                                           companyLogo
                    );
             }
       

            favoriteArticlesWrapperList.add(new FavoriteArticlesWrapper(article, body, ''));
        }

        if (!allInteractiveFields.isEmpty()) {
            User u = Utils.getAllUserInfo(allInteractiveFields);
            for (Integer articleNumber : listOfInteractiveArticles) {
                favoriteArticlesWrapperList[articleNumber].body = Utils.updateArticleBody(
                                                                                                                favoriteArticlesWrapperList[articleNumber].body,
                                                                                                                u,
                                                                                                                allInteractiveFields
                );
            }
        }

        return favoriteArticlesWrapperList;
	}

    public List<SearchPhraseWrapper> getSuggestionsForQuery(String query) {

        this.recordsFromPostIds = new List<Integer>();
        this.recordsFromPostIds.add(1);
        this.recordsFromPostIds.add(2);
        
        Integer MAX_RESULTS = Integer.valueOf(cncrg__Search_Engine_Configurations__c.getInstance(AbstractSearchEngine.ENGINE_CONFIG_SETTING_NAME).cncrg__Max_Suggestions__c);
        List<SearchPhraseWrapper> results = new List<SearchPhraseWrapper>();

        Map<String, Integer> suggRancMap = HerokuWebService.getSuggestions(query, null, null, null);
        if(isTest && suggRancMap != null) {
            suggRancMap.clear();
            suggRancMap.put('{!key}', 1);
        }

        if(suggRancMap != null) {

            for(String record: suggRancMap.keySet()){
                if(record.contains('{!')){
                    record = record.substringBefore('{!');
                }

                results.add(new SearchPhraseWrapper(record.substringBefore('...'),
                                                    suggRancMap.get(record)));
                if(results.size() == MAX_RESULTS){
                    break;
                }
            }

        } else {
            //results.add(new SearchPhraseWrapper('', 0));
        }

        return results;
    }

    public List<SearchResultsWrapper> getSearchResults(String query, String prefix) {

        List<SearchResultsWrapper> results = new List<SearchResultsWrapper>();

        JsonDeserializer response = HerokuWebService.search(query, null, null, null);
        
        if(Test.isRunningTest()){
            String mockBody =
        '{' +
          '"result": [' +
            '{' +
              '"id": 1,' +
              '"title": "SALESFORCE SUMMER ’16 RELEASE NOTES SUMMARY",' +
              '"summary": "{!Document.CompanyLogo} Salesforce delivers powerful new business intelligence capabilities, more ways to collaborate on deals and projects, and even greater control over your data.",' +
              '"fts": {' +
                '"type": "tsvector",' +
                '"value": "way"' +
              '}' +
            '},' +
            '{' +
              '"id": 2,' +
              '"title": "Supported Browsers for Salesforce Classic",' +
              '"summary": "{!User.FullPhotoUrl}",' +
              '"fts": {' +
                '"type": "tsvector",' +
                '"value": "way"' +
              '}' +
            '}' +
          '],' +
          '"error": false' +
        '}';
            Map<String, String> headers = new Map<String, String>();
            headers.put('Content-Type', 'application/json');

            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(200, 'OK', mockBody, headers));

        }
        List<Integer> docIds = new List<Integer>();
        JsonDeserializerDocuments documents;
        if(response != null) {

            for(JsonDeserializer.Records record: response.result.records) {
                docIds.add(record.id);
            }

            documents = HerokuWebService.getDocument(docIds);
        }

        List<String> listIds = new List<String>();
        for(Integer item : docIds) {
            listIds.add(String.valueOf(item));
        }

        List<cncrg__Favorite_article__c> listOfFavorite = new List<cncrg__Favorite_article__c>();
		List<String> fieldsToCheck = new List<String>{ 'cncrg__ArticleId__c',
                                                       'OwnerId'};
        if(ESAPI.securityUtils().isAuthorizedToView('cncrg__Favorite_article__c', fieldsToCheck)){
			listOfFavorite = [
					SELECT
							cncrg__ArticleId__c,
							OwnerId//cncrg__User__c
					FROM	cncrg__Favorite_article__c
					WHERE
							cncrg__ArticleId__c IN: listIds AND
							OwnerId =: UserInfo.getUserId()//cncrg__User__c =: UserInfo.getUserId()
					Limit   50000
			];
		}
        Set<String> exRecordsIds = new Set<String>();
        for(cncrg__Favorite_article__c item : listOfFavorite) {
			WorkWithArticles.EncryptionMember member = new WorkWithArticles.EncryptionMember();
            exRecordsIds.add(member.decrypteData(item.cncrg__ArticleId__c));
        }

        String companyLogo;
        cncrg__Concierge_Settings__c setting = cncrg__Concierge_Settings__c.getValues('CompanyLogo');

            if (setting != null) {
                String companyLogoName = setting.cncrg__Value__c;
				List<Document> conciergeDocuments = new List<Document>();
				fieldsToCheck.clear();
				fieldsToCheck.add('Id');

				if(ESAPI.securityUtils().isAuthorizedToView('Document', fieldsToCheck)){
					conciergeDocuments = [
						SELECT  Id
						FROM    Document
						WHERE   Name =: companyLogoName
						Limit   50000
					];
				}

                if (!conciergeDocuments.isEmpty()) {
                    String wayToLogo;

                    if(prefix!='')
						wayToLogo = String.escapeSingleQuotes('/'+prefix + '/servlet/servlet.FileDownload?file='+conciergeDocuments[0].Id);                  
                    else
                        wayToLogo = String.escapeSingleQuotes('/servlet/servlet.FileDownload?file='+conciergeDocuments[0].Id);

                    companyLogo = '<img src="'+wayToLogo.escapeEcmaScript()+'" style="width: 100%; height: 100%"/>';
                }
            }

        if(response != null) {

            if(documents != null) {
                List<Integer> listOfInteractiveArticles = new List<Integer>();
                Set<String> allInteractiveFields = new Set<String>();

                for(JsonDeserializerDocuments.Result docItem : documents.result) {                  
                    if (docItem.summary.contains('{!User.')) {//summary != null &&
                        listOfInteractiveArticles.add(results.size());
                        allInteractiveFields.addAll(Utils.getAllInteractiveFields(docItem.summary));
                    }
                    if (companyLogo != null && docItem.summary.contains('{!Document.CompanyLogo}')) {
                        docItem.summary = docItem.summary.replaceAll(
                            '\\{!Document.CompanyLogo}',
                            companyLogo
                        );
                    }

                    SearchResultsWrapper resultWr = new SearchResultsWrapper(docItem.title,
                                                                        null,
                                                                        String.valueOf(docItem.id),
                                                                        docItem.summary,
                                                                        'cncrg__Knowledge__kav');
					resultWr.knowledgeArticleId = String.valueOf(docItem.id);
                    resultWr.isFavorite = exRecordsIds.contains(String.valueOf(docItem.id));
                    results.add(resultWr);
                }

                if (!allInteractiveFields.isEmpty()) {
                    User u = Utils.getAllUserInfo(allInteractiveFields);
                    for (Integer articleNumber : listOfInteractiveArticles) {
                        results[articleNumber].body = updateArticleBody(
                            results[articleNumber].body,
                            u,
                            allInteractiveFields
                        );
                    }
                }
            }

        }

        return results;
    }

    public String rateItem(SearchResultsWrapper item) {
        return null;
    }

    public String toggleFavoriteAPEX(String recordId, Boolean isFavorite) {

        if (isFavorite) {
			List<String> fieldsToCheck = new List<String>{ 'cncrg__ArticleId__c',
														   'OwnerId',
														   'Id'
														 };
			List<cncrg__Favorite_article__c> listOfFavorite = new List<cncrg__Favorite_article__c>();
			if(ESAPI.securityUtils().isAuthorizedToView('cncrg__Favorite_article__c', fieldsToCheck)){
				listOfFavorite = [
					SELECT
							Id,
							cncrg__ArticleId__c,
							OwnerId					//cncrg__User__c
					FROM	cncrg__Favorite_article__c
					WHERE
							//cncrg__ArticleId__c =: recordId AND
							OwnerId =: UserInfo.getUserId()//cncrg__User__c =: UserInfo.getUserId()
					Limit   50000
				];
				List<cncrg__Favorite_article__c> listOfFavoriteDelete = new List<cncrg__Favorite_article__c>();
				for(cncrg__Favorite_article__c item : listOfFavorite) {
					WorkWithArticles.EncryptionMember member = new WorkWithArticles.EncryptionMember();
					if(member.decrypteData(item.cncrg__ArticleId__c) == recordId) {
						listOfFavoriteDelete.add(item);
						break;
					}
				}
				ESAPI.securityUtils().validatedDelete(listOfFavoriteDelete);
			}
        } else {
            if (!Utils.checkNumberOfFaivoritArticles()) {
                throw new AuraHandledException(Label.Max_Number_of_Favorites);
            }

			WorkWithArticles.EncryptionMember member = new WorkWithArticles.EncryptionMember();
            List<cncrg__Favorite_article__c> listOfFavorite = new List<cncrg__Favorite_article__c>{
                new cncrg__Favorite_article__c(
                    cncrg__ArticleId__c = member.encrypteData(recordId),
                    OwnerId = UserInfo.getUserId()//cncrg__User__c = UserInfo.getUserId()
                )
            };        
            ESAPI.securityUtils().validatedInsert(listOfFavorite);
        }

        return null;
    }


    public static String updateArticleBody(String body, User u, Set<String> allInteractiveFields) {
        for (String fieldName : allInteractiveFields) {
            String fieldValue = '';
            if (u.get(fieldName) != null) {
                fieldValue = String.valueOf(u.get(fieldName));
                if (fieldName.equals('FullPhotoUrl')) {// != 'SmallPhotoUrl'
					fieldValue = String.escapeSingleQuotes(fieldValue.substringAfter('.com'));
                    fieldValue = '<img src="'+fieldValue.escapeEcmaScript()+'" style="width: 100%; height: auto"/>';//100%
                }
            }
            body = body.replaceAll(
                '\\{!User.'+fieldName+'}',
                fieldValue
            );
        }
        return body;
    }
    public SearchResultsWrapper getAllArticleSettings(SearchResultsWrapper articleRecord, String prefix) {
        // @TODO
        return articleRecord;
    }
    public FavoriteArticlesWrapper getFavoriteArticleDetail(FavoriteArticlesWrapper favorite, String prefix) {
        // @TODO
        return favorite;
    }
    public SearchResultsWrapper getArticleDetail(SearchResultsWrapper article, String prefix) {
        // @TODO
        return article;
    }
}