global class FavoriteArticlesWrapper implements Comparable {
	public static final String DATE_FORMAT = 'MMM dd, yyyy';

	@AuraEnabled
	public sObject article { get; set; }
	@AuraEnabled
	public String Created { get; set; }
	@AuraEnabled
	public String Modified { get; set; }
	@AuraEnabled
	public String body { get; set; }
	@AuraEnabled
	public String title { get; set; }
	@AuraEnabled
	public String type { get; set; }
	@AuraEnabled
	public DateTime createdDate { get; set; }
	@AuraEnabled
	public Boolean showRelatedAttachments { get; set; }
	// article source - Heroku / Salesforce / etc
	@AuraEnabled
	public String storage {get; set;}
	@AuraEnabled
	public String knowledgeArticleId { get; set; }

	public FavoriteArticlesWrapper(sObject article, String body, String title) {
		this.storage = 'salesforce';
		this.showRelatedAttachments = false;
		this.article = article;
		this.body = body;
		this.title = title;
		this.populateCreatedInfo();
	}

	public void populateCreatedInfo() {
		try {
			DateTime createdDate = (DateTime) article.get('CreatedDate');
			DateTime modifiedDate = (DateTime) article.get('LastModifiedDate');
			this.Created = createdDate != null ? createdDate.format(DATE_FORMAT) : null;
			this.Modified = modifiedDate != null ? modifiedDate.format(DATE_FORMAT) : null;
		} catch(Exception e) {}
	}

	public Integer compareTo(Object compareTo) {
		FavoriteArticlesWrapper compareToEmp = (FavoriteArticlesWrapper) compareTo;
		if (createdDate == compareToEmp.createdDate) return 0;
		if (createdDate > compareToEmp.createdDate) return - 1;
		return 1;
	}
}