@isTest
public with sharing class ArticleRateFeedbackControllerTest {
    @isTest
    public static void insertArticleFeedbackTest(){
        ArticleRateFeedbackController.ArticleFeedbackWrapper wrapper = new ArticleRateFeedbackController.ArticleFeedbackWrapper();
        for(ArticleRateFeedbackController.ArticleFeedbackWrapperItem item : wrapper.articleFeedbackWrapperItemList){
            item.isChecked = true;
        }
        Object feedBackItem = ArticleRateFeedbackController.getNewArticleFeedbackWrapper();
        System.assert(feedBackItem  != null);
    }
    @isTest
    public static void createSearchStaging(){
        ArticleRateFeedbackController.ArticleFeedbackWrapper wrapper = new ArticleRateFeedbackController.ArticleFeedbackWrapper();
        for(ArticleRateFeedbackController.ArticleFeedbackWrapperItem item : wrapper.articleFeedbackWrapperItemList){
            item.isChecked = true;
        }
        wrapper.comment = 'comment';
        ArticleRateFeedbackController.createSearchStaging(JSON.serialize(wrapper), 'sampleId', 'sampleTitle', false);
        Integer stagingRecords = [SELECT COUNT() FROM Search_Staging__c];
        System.assertEquals(1, stagingRecords);
    }
}