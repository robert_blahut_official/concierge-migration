/**
 * DEAR SECURITY REVIEWER!
 * The apex class is not using the “with or without sharing” because the class is referenced in package post install class.
 * Please see details in Salesforce documentation.
 * https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_install_handler.htm
 * “Post install script can’t call another Apex class in the package if that Apex class uses the with sharing keyword.“
*/
/**
* @description

	This class takes the Search Staging records, and uses them to populate Search_Term__c, Search_History__c and Article_Search_History__c.
	First it updates the scores in Search_Term__c.Strange__c for existing records. Then it upserts records with the Articles that have been most useful
	for each search term.

* @methods
	cloneStagingToHistories -		method for parsing Search Staging records to Search_History__c records
	calculateSearchScore -			method for calculation Search History scores
	recalculateScores -				recalculate the scores in Strange for the Search Term records(decimal newScore = ((1/TWO.pow((today-charm)/halfLife))*currentScore).setScale(10))
	insertArticleSearchHistories -	method for parsing Search Staging records to Articla_Search_History__c records
	saveFeedBackItems -				method for parsing Search Staging records to Feed_Back records
	pullingCheckboxes -				calculation the values for the various checkboxes that indicate what actions were taken on the article.
*/
public class TurboEncabulator extends AbstractConciergeBatch implements Database.AllowsCallouts {
	public class TurboEncabulatorException extends Exception {}
	public static final String MAIN_STAGING_RECORD_TYPE_ID = Schema.SObjectType.Search_Staging__c.getRecordTypeInfosByName().get('Search Staging').getRecordTypeId();

	public Database.QueryLocator start(Database.BatchableContext BC) {
		DateTime boundaryTracking = Datetime.now().addHours(-1);
		return Database.getQueryLocator(
			'SELECT Id, CreatedDate, Name, Biederman__c, Down__c, Profigliano__c, Schwartz__c, Up__c, Zoom__c, CreatedById ' +
			'FROM Search_Staging__c ' +
			'WHERE (RecordTypeId = NULL OR RecordTypeId = :MAIN_STAGING_RECORD_TYPE_ID) ' +
				'AND ((Down__c = NULL AND CreatedDate <=: boundaryTracking) OR CreatedDate < LAST_N_DAYS:30)'
		);
	}

	public void execute(Database.BatchableContext context, List<cncrg__Search_Staging__c> scope) {
		List<cncrg__Search_Staging__c> stagingRecordsToDelete = new List<cncrg__Search_Staging__c>();
		List<cncrg__Search_Staging__c> actualStagingRecords = new List<cncrg__Search_Staging__c>();

		for (cncrg__Search_Staging__c stagingRecord : scope) {
			if (String.isNotBlank(stagingRecord.cncrg__Down__c)
					|| String.isBlank(stagingRecord.cncrg__Zoom__c)) {
				stagingRecordsToDelete.add(stagingRecord);
			} else {
				actualStagingRecords.add(stagingRecord);
			}
		}

		List<cncrg__Search_Staging__c> backupSearchStaging = (List<cncrg__Search_Staging__c>) JSON.deserialize(JSON.serialize(actualStagingRecords), List<cncrg__Search_Staging__c>.class);
		List<WorkWithArticles.SearchStagingWrapper> searchStagingsList = new List<WorkWithArticles.SearchStagingWrapper>();
		Set<String> zoomSet = new Set<String>();
		Map<String, cncrg__Search_Term__c> searchTermMap = new Map<String, cncrg__Search_Term__c>();

		try {
			for (cncrg__Search_Staging__c item : actualStagingRecords) {
				if (String.isNotBlank(item.cncrg__Zoom__c)) {
					WorkWithArticles.decryptedSearchStaging(item);
					searchStagingsList.add(new WorkWithArticles.SearchStagingWrapper(item));
					zoomSet.add(item.cncrg__Zoom__c.toLowerCase());
				}
			}
			for (cncrg__Search_Term__c item : [
					SELECT Id, Name, cncrg__Charm__c, cncrg__Strange__c
					FROM cncrg__Search_Term__c
					WHERE Name IN : zoomSet
				]) {
				searchTermMap.put(item.Name, item);
			}

			WorkWithArticles.EncryptionMember member = new WorkWithArticles.EncryptionMember();
			Map<String, cncrg__Search_Term__c> searchTermUpsertMap = new Map<String, cncrg__Search_Term__c>();

			for (WorkWithArticles.SearchStagingWrapper item : searchStagingsList) {
				WorkWithArticles.Encabulator encabulatorRecord;
				cncrg__Search_Term__c searchTermRecord = searchTermMap.get(item.zoom.toLowerCase());
				if (searchTermRecord != null) {
					try {
						encabulatorRecord = (WorkWithArticles.Encabulator) JSON.deserialize(searchTermRecord.cncrg__Strange__c, WorkWithArticles.Encabulator.class);
					} catch (Exception ex) {
						encabulatorRecord = (WorkWithArticles.Encabulator) JSON.deserialize(member.decrypteData(searchTermRecord.cncrg__Strange__c), WorkWithArticles.Encabulator.class);
					}
				} else {
					searchTermRecord = new cncrg__Search_Term__c(Name = item.zoom.toLowerCase());
					searchTermMap.put(item.zoom.toLowerCase(), searchTermRecord);
				}

				encabulatorRecord = calculateSearchScore(encabulatorRecord, item, searchTermRecord.cncrg__Charm__c);

				searchTermRecord.cncrg__Charm__c = Datetime.now().dateGMT();
				searchTermRecord.cncrg__Strange__c = member.encrypteData(JSON.serialize(encabulatorRecord));
				searchTermUpsertMap.put(item.zoom.toLowerCase(), searchTermRecord);
			}

			ESAPI.securityUtils().validatedUpsert(searchTermUpsertMap.values());
			insertArticleSearchHistories( cloneStagingToHistories(searchTermUpsertMap, actualStagingRecords) );
			ESAPI.securityUtils().validatedDelete(actualStagingRecords);
			Database.emptyRecycleBin(actualStagingRecords);
		} catch (Exception e) {
			for (cncrg__Search_Staging__c item : backupSearchStaging) {
				item.Down__c = e.getStackTraceString();
				item.Down__c += e.getMessage();
				item.Down__c += e.getLineNumber();
				item.Down__c = item.Down__c.length() > 255 ? item.Down__c.subString(0, 254) : item.Down__c;
			}
			ESAPI.securityUtils().validatedUpdate(backupSearchStaging);
		}

		if (stagingRecordsToDelete.size() > 0) {
			ESAPI.securityUtils().validatedDelete(stagingRecordsToDelete);
			Database.emptyRecycleBin(stagingRecordsToDelete);
		}
	}

	private Map<cncrg__Search_Staging__c, cncrg__Search_History__c> cloneStagingToHistories(Map<String, cncrg__Search_Term__c> searchTermUpsertMap, List<cncrg__Search_Staging__c> searchStagingList) {
		Map<cncrg__Search_Staging__c, cncrg__Search_History__c> searchHistoryMap = new Map<cncrg__Search_Staging__c, cncrg__Search_History__c>();
		List<cncrg__Concierge_Field_Mappings__c> conciergeFieldMappingsList = cncrg__Concierge_Field_Mappings__c.getall().values();

		Set<Id> userIdSet = new Set<Id>();
		for (cncrg__Search_Staging__c item : searchStagingList) {
			userIdSet.add(item.CreatedById);
		}

		Map<String, String> substitutionValuesMap = new Map<String, String> {
			'Profile' => 'Profile.Name',
			'Role' => 'UserRole.Name'
		};
		List<String> fieldsToSelect = new List<String>();
		fieldsToSelect.add('Id');
		for (cncrg__Concierge_Field_Mappings__c item : conciergeFieldMappingsList) {
			String currentField = item.Value__c.split(':')[0];
			if (substitutionValuesMap.containsKey(currentField)) {
				currentField = substitutionValuesMap.get(currentField);
			}
			fieldsToSelect.add(currentField);
		}

		Map<Id, User> creatersMap = new Map<Id, User> ((List<User>)Database.query(
			'SELECT ' + String.join(fieldsToSelect, ',') + ' FROM User WHERE Id IN :userIdSet'
		));
		for (cncrg__Search_Staging__c item : searchStagingList) {
			cncrg__Search_History__c searchHistoryRecord = new cncrg__Search_History__c();

			for (cncrg__Concierge_Field_Mappings__c fields : conciergeFieldMappingsList) {
				List<String> fieldsMappping = fields.Value__c.split(':');
				String currentField = fieldsMappping[0];

				if (substitutionValuesMap.containsKey(currentField)) {
					currentField = substitutionValuesMap.get(currentField);

					try {
						String parentObjectName = currentField.split('\\.')[0];
						String parentObjectFieldName = currentField.split('\\.')[1];
						sObject parentObject = creatersMap.get(item.CreatedById).getSobject(parentObjectName);

						if (parentObject != null) {
							searchHistoryRecord.put(fieldsMappping[1], parentObject.get(parentObjectFieldName) );
						}
					} catch (Exception e) {}
				} else {
					searchHistoryRecord.put(fieldsMappping[1], creatersMap.get(item.CreatedById).get(currentField));
				}
			}

			searchHistoryRecord.Search_Term__c = searchTermUpsertMap.get(item.cncrg__Zoom__c.toLowerCase()).Id;
			searchHistoryRecord.Search_Date_Time__c = item.CreatedDate;
			searchHistoryRecord.Number_of_Results__c = ((List<String>) JSON.deserialize(item.Biederman__c, List<String>.class)).size();
			searchHistoryRecord.Language__c = item.cncrg__Up__c;
			searchHistoryMap.put(item, searchHistoryRecord);
		}

		ESAPI.securityUtils().validatedInsert(searchHistoryMap.values());
		return searchHistoryMap;
	}

	private WorkWithArticles.Encabulator calculateSearchScore(WorkWithArticles.Encabulator encabulatorRecord, WorkWithArticles.SearchStagingWrapper searchStagingRecord, Date charm) {
		if (String.isBlank(searchStagingRecord.schwartz) 
			|| (searchStagingRecord.profigliano.get(searchStagingRecord.schwartz).score >= 256)) {
			return encabulatorRecord;
		}

		if (encabulatorRecord != null) {
			recalculateScores(encabulatorRecord, charm);
			if (encabulatorRecord.encabulator.containsKey(searchStagingRecord.up)) {
				List<WorkWithArticles.ArticleEncabulator> articleEncabulatorList = encabulatorRecord.encabulator.get(searchStagingRecord.up);
				for (WorkWithArticles.ArticleEncabulator item : articleEncabulatorList) {
					if (item.id == searchStagingRecord.schwartz) {
						item.rawscore += 10;
						item.score += 10;
						encabulatorRecord.encabulator.put(searchStagingRecord.up, articleEncabulatorList);
						return encabulatorRecord;
					}
				}

				WorkWithArticles.ArticleEncabulator articleEncabulatorRecord = new WorkWithArticles.ArticleEncabulator(searchStagingRecord.schwartz);
				articleEncabulatorList.add(articleEncabulatorRecord);
				encabulatorRecord.encabulator.put(searchStagingRecord.up, articleEncabulatorList);
			} else {
				WorkWithArticles.ArticleEncabulator articleEncabulatorRecord = new WorkWithArticles.ArticleEncabulator(searchStagingRecord.schwartz);
				encabulatorRecord.encabulator.put(searchStagingRecord.up, new List<WorkWithArticles.ArticleEncabulator> {articleEncabulatorRecord});
			}
		} else {
			encabulatorRecord = new WorkWithArticles.Encabulator();
			encabulatorRecord.encabulator = new Map<String, List<WorkWithArticles.ArticleEncabulator>>();
			WorkWithArticles.ArticleEncabulator articleEncabulatorRecord = new WorkWithArticles.ArticleEncabulator(searchStagingRecord.schwartz);
			encabulatorRecord.encabulator.put(searchStagingRecord.up, new List<WorkWithArticles.ArticleEncabulator> {articleEncabulatorRecord});
		}

		return encabulatorRecord;
	}

	private void recalculateScores(WorkWithArticles.Encabulator encabulatorRecord, Date charm) {
		String ERROR_MESSAGE = 'Article Score Half Life (Days) is not defined';
		String NAME_HALF_LIFE_SETTING = 'Article Score Half Life (Days)';
		Date today = Datetime.now().dateGMT();
		cncrg__Concierge_Settings__c scoreHalfLife = cncrg__Concierge_Settings__c.getInstance(NAME_HALF_LIFE_SETTING);
		if (scoreHalfLife == null || !scoreHalfLife.Value__c.isNumeric() || scoreHalfLife.Value__c == '0') {
			throw new TurboEncabulatorException(ERROR_MESSAGE);
		}
		Integer halfLife = Integer.valueOf(scoreHalfLife.Value__c);
		if (today != charm) {
			for (String key : encabulatorRecord.encabulator.keySet()) {
				for (WorkWithArticles.ArticleEncabulator item : encabulatorRecord.encabulator.get(key)) {
					item.score = ((Decimal)(1 / Math.pow(2, (charm.daysBetween(today)) / (Double)halfLife)) * item.score).setScale(2, RoundingMode.HALF_UP);
				}
			}
		}
	}

	private Set<String> getArticleIdSet(List<cncrg__Search_Staging__c> searchStagingList) {
		Set<String> articleIdSet = new Set<String>();
		for (cncrg__Search_Staging__c item : searchStagingList) {
			Map<String, WorkWithArticles.Profigliano> profigliano = new Map<String, WorkWithArticles.Profigliano>();
			try {
				profigliano = (Map<String, WorkWithArticles.Profigliano>)JSON.deserialize(item.cncrg__Profigliano__c, Map<String, WorkWithArticles.Profigliano>.class);
			} catch (Exception e) {
				Map<String, Integer> profiglianoOld = (Map<String, Integer>)JSON.deserialize(item.cncrg__Profigliano__c, Map<String, Integer>.class);
				for (String key : profiglianoOld.keySet()) {
					profigliano.put(key, new WorkWithArticles.Profigliano(null, profiglianoOld.get(key), null));
				}
			}
			for (String key : profigliano.keySet()) {
				articleIdSet.add(key);
			}
		}
		return articleIdSet;
	}

	private void insertArticleSearchHistories(Map<cncrg__Search_Staging__c, cncrg__Search_History__c> searchHistoryMap) {
		List<cncrg__Article_Search_History__c> articleSearchHistoryList = new List<cncrg__Article_Search_History__c>();
		List<FeedBackWrapper> feedBackItems = new List<FeedBackWrapper>();
		Map<FeedBackWrapper, cncrg__Article_Search_History__c> feedBackArticleHistoryMap = new Map<FeedBackWrapper, cncrg__Article_Search_History__c>();
		for (cncrg__Search_Staging__c item : searchHistoryMap.keySet()) {
			Map<String, WorkWithArticles.Profigliano> profigliano = new Map<String, WorkWithArticles.Profigliano>();
			try {
				profigliano = (Map<String, WorkWithArticles.Profigliano>)JSON.deserialize(item.cncrg__Profigliano__c, Map<String, WorkWithArticles.Profigliano>.class);
			} catch (Exception e) {
				Map<String, Integer> profiglianoOld = (Map<String, Integer>)JSON.deserialize(item.cncrg__Profigliano__c, Map<String, Integer>.class);
				for (String key : profiglianoOld.keySet()) {
					profigliano.put(key, new WorkWithArticles.Profigliano(null, profiglianoOld.get(key), null));
				}
			}
			for (String key : profigliano.keySet()) {
				cncrg__Article_Search_History__c articleSearchHistoryRecord = new cncrg__Article_Search_History__c();

				pullingCheckboxes(articleSearchHistoryRecord, profigliano.get(key).score);

				articleSearchHistoryRecord.cncrg__Article_Id__c = key;
				articleSearchHistoryRecord.cncrg__Article_Type__c = profigliano.get(key).type;
				articleSearchHistoryRecord.cncrg__Article_Name__c = profigliano.get(key).title;
				articleSearchHistoryRecord.cncrg__OwnerID__c = profigliano.get(key).ownerId;

				String dataCategories = profigliano.get(key).dataCategories;
				if (String.isNotBlank(dataCategories) && dataCategories.length() > 400) {
					dataCategories = dataCategories.substring(0, 399).substringBeforeLast(';');
				}
				articleSearchHistoryRecord.Data_Category__c = dataCategories;

				if (profigliano.get(key).comment != null || profigliano.get(key).reasons != null) {
					FeedBackWrapper feedBackRecord = new FeedBackWrapper(articleSearchHistoryRecord, profigliano.get(key).reasons, profigliano.get(key).comment);
					feedBackItems.add(feedBackRecord);
					feedBackArticleHistoryMap.put(feedBackRecord, articleSearchHistoryRecord);
				}

				articleSearchHistoryRecord.cncrg__Search_History__c = searchHistoryMap.get(item).Id;
				articleSearchHistoryList.add(articleSearchHistoryRecord);
			}
		}
		if (feedBackItems != null && feedBackItems.size() > 0) {
			saveFeedBackItems(feedBackItems);
		}
		for (FeedBackWrapper key : feedBackArticleHistoryMap.keySet()) {
			feedBackArticleHistoryMap.get(key).cncrg__Article_Feedback__c = key.feedbackRecord.Id;
		}
		ESAPI.securityUtils().validatedInsert(articleSearchHistoryList);
	}

	private class FeedBackWrapper {
		cncrg__Article_Search_History__c articleSearchHistoryRecord;
		List<String> feedbackReasonList;
		String comment;
		cncrg__Article_Feedback__c feedbackRecord;

		private FeedBackWrapper(cncrg__Article_Search_History__c articleSearchHistoryRecord, List<String> feedbackReasonList, String comment) {
			this.articleSearchHistoryRecord = articleSearchHistoryRecord;
			this.comment = comment;
			this.feedbackReasonList = feedbackReasonList;
		}

		private cncrg__Article_Feedback__c convertToSobject() {
			cncrg__Article_Feedback__c feedbackRecord = new cncrg__Article_Feedback__c();
			feedbackRecord.Article_ID__c = articleSearchHistoryRecord.cncrg__Article_Id__c;
			feedbackRecord.Article_Title__c = articleSearchHistoryRecord.cncrg__Article_Name__c;
			feedbackRecord.Comments__c = comment;
			try {
				feedbackRecord.cncrg__Feedback_Reason__c = String.join(feedbackReasonList, ';');
			} catch (Exception e) {}
			this.feedbackRecord = feedbackRecord;
			return feedbackRecord;
		}
	}

	private void saveFeedBackItems(List<FeedBackWrapper> feedBackItems) {
		List<cncrg__Article_Feedback__c> articleFeedbackList = new List<cncrg__Article_Feedback__c>();
		for (FeedBackWrapper item : feedBackItems) {
			articleFeedbackList.add(item.convertToSobject());
		}
		ESAPI.securityUtils().validatedInsert(articleFeedbackList);
	}

	private void pullingCheckboxes(Article_Search_History__c articlesearchHistoryRecord, Integer profiglianoScore) {
		if (profiglianoScore >= 1024) {
			articlesearchHistoryRecord.Launched_Flow__c = true;
			profiglianoScore -= 1024;
		}
		if (profiglianoScore >= 512) {
			articlesearchHistoryRecord.Viewed_PDF__c = true;
			profiglianoScore -= 512;
		}
		if (profiglianoScore >= 256) {
			articlesearchHistoryRecord.Downvoted_Article__c = true;
			profiglianoScore -= 256;
		}
		if (profiglianoScore >= 128) {
			articlesearchHistoryRecord.Upvoted_Article__c = true;
			profiglianoScore -= 128;
		}
		if (profiglianoScore >= 64) {
			articlesearchHistoryRecord.Unfavorited_Article__c = true;
			profiglianoScore -= 64;
		}
		if (profiglianoScore >= 32) {
			articlesearchHistoryRecord.Favorited_Article__c = true;
			profiglianoScore -= 32;
		}
		if (profiglianoScore >= 16) {
			articlesearchHistoryRecord.Clicked_URL__c = true;
			profiglianoScore -= 16;
		}
		if (profiglianoScore >= 8) {
			articlesearchHistoryRecord.Started_Chat_Session__c = true;
			profiglianoScore -= 8;
		}
		if (profiglianoScore >= 4) {
			articlesearchHistoryRecord.Logged_a_Ticket__c = true;
			profiglianoScore -= 4;
		}
		if (profiglianoScore >= 2) {
			articlesearchHistoryRecord.Viewed_Article_Detail__c = true;
			profiglianoScore -= 2;
		}
		if (profiglianoScore == 1) {
			articlesearchHistoryRecord.Viewed_Snippet__c = true;
		}
	}
}