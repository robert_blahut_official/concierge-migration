@IsTest
public class SettingsPanelControllerTest {

    @testSetup static void setup() {
		List<SchedulerSettings__c> schedSettingList = new List<SchedulerSettings__c>{
				new SchedulerSettings__c(
						Name = TurboEncabulatorScheduler.TURBO_BATCH_NAME,
						Class_Name__c = TurboEncabulatorScheduler.TURBO_BATCH_NAME,
						CRON__c = TurboEncabulatorScheduler.TURBO_CRON,
						Batch_Size__c = TurboEncabulatorScheduler.TURBO_BATCH_SIZE,
						Description__c = TurboEncabulatorScheduler.TURBO_DESCRIPTION,
						Active__c = true
				),
				new SchedulerSettings__c(
						Name = TurboEncabulatorScheduler.REVIEW_BATCH_NAME,
						Class_Name__c = TurboEncabulatorScheduler.REVIEW_BATCH_NAME,
						CRON__c = TurboEncabulatorScheduler.REVIEW_CRON,
						Batch_Size__c = TurboEncabulatorScheduler.REVIEW_BATH_SIZE,
						Description__c = TurboEncabulatorScheduler.REVIEW_DESCRIPTION,
						Active__c = true
				)
		};
		Database.insert(schedSettingList, false);
        cncrg__Concierge_Settings__c cs = new cncrg__Concierge_Settings__c(
            Name = 'Case Origin',
            cncrg__Value__c = 'test'
        );
		cncrg__Concierge_Settings__c cs2 = new cncrg__Concierge_Settings__c(
            Name = 'Article Actions Display Order',
            cncrg__Value__c = 'Skype;Email;'
        );
		
		ESAPI.securityUtils().validatedInsert(new List<cncrg__Concierge_Settings__c>{cs, cs2});
		List<cncrg__Trigger_Activator__c> triggerActivatorList = new List<cncrg__Trigger_Activator__c> {
			new cncrg__Trigger_Activator__c(
                Name = 'User Case Sharing Trigger',
                Active__c = true
            )
		};
		insert triggerActivatorList;
		TestClassUtility.createSearchEngineConfig(AbstractSearchEngine.POSTGRESS_SEARCH_TYPE);

		List<String> listOfTypes = new List<String>();
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        for (String key : gd.keySet()) {
            Schema.SObjectType objectType = gd.get(key);
            if (key.endsWith('kav')) {
				listOfTypes.add(objectType.getDescribe().getName());
            }
        }

        cncrg__Background_Settings__c setting = new cncrg__Background_Settings__c(
            Name = 'Background Setting',
            cncrg__Background_File_Type__c = 'File Type',
            cncrg__File_Location__c = 'Location File'
        );
        insert setting;
		String objName = WorkWithArticlesTest.getKavObjectName();
		List<cncrg__Search_Types_Settings__c> settings = new List<cncrg__Search_Types_Settings__c>{
			new cncrg__Search_Types_Settings__c(
				Name = objName,
				cncrg__Article_Type_API_Name__c = objName,
				cncrg__Active__c = true,
				cncrg__Fields_To_Display_1__c = 'Summary',
				cncrg__Fields_To_Display_2__c = 'Id',
				cncrg__Fields_To_Display_3__c = 'Attachments',
				cncrg__Fields_To_Display_4__c = 'CreatedDate'
			)
		};
		ESAPI.securityUtils().validatedInsert(settings);
		System.assertEquals(1, cncrg__Search_Types_Settings__c.getAll().size());
    }
    
    static testMethod void testSettingsPanelController() {
        Test.startTest();
        System.assert(true, SettingsPanelController.isCurrentUserAdmin());

		List<SettingsPanelController.TypesWrapper> ts = SettingsPanelController.getTypesSettings();
		Integer index = 0;
		List<String> listOfTypes = new List<String>();
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        for (String key : gd.keySet()) {
            Schema.SObjectType objectType = gd.get(key);
            if (key.endsWith('kav')) {
				listOfTypes.add(objectType.getDescribe().getName());
            }
        }

		for(SettingsPanelController.TypesWrapper typeItem : ts){
			typeItem.name = 'Temp Name '+String.valueOf(index);
			index++;
		}
		//cncrg__Search_Types_Settings__c recordToDelect = cncrg__Search_Types_Settings__c.getAll().values()[0];
		/*List<cncrg__Search_Types_Settings__c> recordToDelect = new List<cncrg__Search_Types_Settings__c>{
			new cncrg__Search_Types_Settings__c(
				Name = listOfTypes[0] + ' 5',
				cncrg__Active__c = true,
				cncrg__Fields_To_Display_1__c = 'OwnerId'
			)
		};
		ESAPI.securityUtils().validatedInsert(recordToDelect);*/

        SettingsPanelController.NewSettingsClass obj = new SettingsPanelController.NewSettingsClass();
        obj.cs = (List<cncrg__Concierge_Settings__c>) SettingsPanelController.getListOfConciergeSettings()[0];
        obj.sec = (cncrg__Search_Engine_Configurations__c)SettingsPanelController.getSearchEngineConfiguration()[0];
        //obj.del = new List<String>{}; //recordToDelect[0].Id
		obj.ts = ts;
		//obj.ws = SettingsPanelController.getWebServiceSettings();

        // Test else logic in getWebServiceSettings() method
        //obj.ws = SettingsPanelController.getWebServiceSettings();

        // Test getBackgroundSettings() method
        //List<cncrg__Background_Settings__c> backgroundSettings = SettingsPanelController.getBackgroundSettings();
        //System.assert(!backgroundSettings.isEmpty());

        System.assert(
            SettingsPanelController.implementNewSettings(JSON.serialize(obj)) == 'Success'
        );
        Test.stopTest();
    }
	static testMethod void testGetTriggerActivatorSettingsList() {
		System.assert(SettingsPanelController.getTriggerActivatorSettingsList().size() > 0);
	}
	static testMethod void testImplementFieldMaps() {
		List<String> usersFieldsList = new List<String> {
			'Country', 
			'Department', 
			'How Long Since User Was Created', 
			'Language', 
			'Locale', 
			'Profile', 
			'City/State'
		};
		List<String> historyFieldsList = new List<String> {
			'Country', 
			'Department', 
			'How Long Since User Was Created', 
			'Language', 
			'Locale', 
			'Profile', 
			'State'
		};
		SettingsPanelController.implementFieldMaps(usersFieldsList, historyFieldsList);
		System.assert([SELECT Id FROM cncrg__Concierge_Field_Mappings__c].size() > 0);
	}
	static testMethod void testImplementNewTriggerActivatorSettings() {
		SettingsPanelController.implementNewTriggerActivatorSettings(JSON.serialize(cncrg__Trigger_Activator__c.getAll().values()));
		System.assert([SELECT Id FROM cncrg__Trigger_Activator__c].size() > 0);
	}
	static testMethod void testGetFieldMappings() {
		System.assert(SettingsPanelController.getFieldMappings() != null);
	}
	static testMethod void testGetCaseTypesSettings() {
		System.assert(SettingsPanelController.getCaseTypesSettings() != null);
	}
	static testMethod void testScheduleSettings() {
		Test.startTest();
		List<SettingsPanelController.ScheduleWrapper> settings = SettingsPanelController.getSchedulerSettings();
		settings.get(0).isRemove = true;
		SettingsPanelController.updateSchedulerSettings(JSON.serialize(settings));

		List<String> batchses = SettingsPanelController.getBatchList();
		SettingsPanelController.CRONWrapper cronExp = SettingsPanelController.parseCron('0 2 6 5 * ?');
		System.assertNotEquals(0, settings.size());
		SettingsPanelController.changeActionsOrder('Item1,Item2');
		Test.stopTest();
	}
}