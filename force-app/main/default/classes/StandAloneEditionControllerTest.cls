@isTest
private class StandAloneEditionControllerTest {

	@testSetup static void setup() {
		String objectType = WorkWithArticlesTest.getKavObjectName();
		List<cncrg__Search_Types_Settings__c> settings = new List<cncrg__Search_Types_Settings__c> {
			new cncrg__Search_Types_Settings__c(
				Name = objectType,
				cncrg__Article_Type_API_Name__c = objectType,
				cncrg__Active__c = true,
				cncrg__Fields_To_Display_1__c = 'Summary,Title'
			)
		};
		ESAPI.securityUtils().validatedInsert(settings);

		List<cncrg__Search_Term__c> searchTerms = new List<cncrg__Search_Term__c> {
			new cncrg__Search_Term__c(
				Name = 'test search term'
			)
		};
		ESAPI.securityUtils().validatedInsert(searchTerms);

		TestClassUtility.createSearchEngineConfig();
		System.assert(!Search_Types_Settings__c.getAll().isEmpty());
	}

	@IsTest
	public static void testToggleFavoriteAPEX() {
		WorkWithArticles.EncryptionMember member = new WorkWithArticles.EncryptionMember();
		String articleId = member.encrypteData('articleId');
		cncrg__Favorite_article__c favoriteArticle = new cncrg__Favorite_article__c(
			cncrg__ArticleId__c = articleId
		);
		insert favoriteArticle;

		StandAloneEditionController standAloneController = new StandAloneEditionController();
		String result = standAloneController.toggleFavoriteAPEX('articleId', true);

		List<cncrg__Favorite_article__c> listOfFavorite = [
			SELECT  Id
			FROM    cncrg__Favorite_article__c
			WHERE   cncrg__ArticleId__c = : articleId
		];
		System.assert(listOfFavorite.isEmpty());
	}

	@IsTest
	public static void testUpdateArticleBody() {
		Set<String> allInteractiveFields = new Set<String> { 'FullPhotoUrl', 'MobilePhone'};
		String body = '{!User.FullPhotoUrl} ';
		Set<String> userFields = Utils.getAllInteractiveFields(body);
		System.assert(1 == userFields.size());
		User testUser = Utils.getAllUserInfo(userFields);
		User currentUser = [SELECT FullPhotoUrl, MobilePhone FROM User WHERE FullPhotoUrl != null LIMIT 1];
		String result = Utils.updateArticleBody(body, currentUser, allInteractiveFields);
		System.assert(body != result);
	}

	@IsTest
	public static void testGetCompanyLogoName() {
		String nameOfDocument = 'Document';
		String prefix = 'prefix';
		cncrg__Concierge_Settings__c searchTypesSetting = new cncrg__Concierge_Settings__c(
			Name = 'CompanyLogo ',
			cncrg__Value__c = nameOfDocument
		);
		insert searchTypesSetting;

		String standardFolderId = [
									  SELECT Id
									  FROM   Folder
									  WHERE  Type = 'Document'
											  LIMIT  1
								  ].Id;

		Document document = new Document(
			Body = Blob.valueOf('Some Text'),
			ContentType = 'application/pdf',
			DeveloperName = 'my_document',
			IsPublic = true,
			Name = nameOfDocument,
			FolderId = standardFolderId
		);
		insert document;


		String result = Utils.getCompanyLogoName(prefix);
		System.assert(String.isNotBlank(result) && result.contains(prefix));
	}

	@IsTest
	public static void testStandaloneSearchEngineUtils() {
		List<SearchPhraseWrapper> tempNewResult = new List<SearchPhraseWrapper>();
		tempNewResult.addAll(StandaloneSearchEngineUtils.getOrderedPhrasesForText('test query line', 'test', 5, 5, new Set<String> {'line'}));
		System.assert(tempNewResult != null);
	}

	@IsTest
	public static void testGetClassicKnowledgeDocumentsbyId() {
		String objectType = WorkWithArticlesTest.getKavObjectName();
		if (objectType != null) {
			Schema.DescribeSObjectResult sObjectDescribe = TestClassUtility.getSobjectDescribe(objectType);
			sObject obj = sObjectDescribe.getSobjectType().newSObject();
			obj.put('Title', 'Unit Test 1 test');
			obj.put('Language', TestClassUtility.getAvailableKnowlegeLanguage(sObjectDescribe));
			obj.put('UrlName', 'test');
			obj.put('Summary', 'test test test test');
			List<sObject> articles = new List<sObject> {obj};
			ESAPI.securityUtils().validatedInsert(articles);

			List<Id> listOfIds = new List<Id>();
			for (sObject article : articles) {
				listOfIds.add((String) article.get('Id'));
			}
			articles = Database.query('SELECT KnowledgeArticleId FROM ' + objectType + ' WHERE Id IN: listOfIds');
			KbManagement.PublishingService.publishArticle((Id) articles[0].get('KnowledgeArticleId'), true);

			StandAloneEditionController controller = new StandAloneEditionController();
			AbstractSearchEngine.isTest = true;
			List<Object> favoriteWrappers = controller.getFavoritesbyId(new List<String> {(String) articles[0].get('KnowledgeArticleId')}, '');
			//System.assert(favoriteWrappers.size() > 0);

			cncrg__Concierge_Settings__c setting = new cncrg__Concierge_Settings__c(
				Name = 'Channel Type For Search',
				cncrg__Value__c = 'Internal App'
			);
			insert setting;

			favoriteWrappers = controller.getFavoritesbyId(new List<String> {(String) articles[0].get('KnowledgeArticleId')}, '');
			System.assertNotEquals(favoriteWrappers, null);
			//System.assert(lf.size() > 0);
		}
	}

	@IsTest
	public static void testGetFavoritesbyId() {
		String objectType = WorkWithArticlesTest.getKavObjectName();
		if (objectType != null) {
			Schema.DescribeSObjectResult sObjectDescribe = TestClassUtility.getSobjectDescribe(objectType);
			sObject obj = sObjectDescribe.getSobjectType().newSObject();
			obj.put('Title', 'Unit Test 1 test');
			obj.put('Language', TestClassUtility.getAvailableKnowlegeLanguage(sObjectDescribe));
			obj.put('UrlName', 'test');
			obj.put('Summary', 'test test test test');
			List<sObject> articles = new List<sObject> {obj};
			ESAPI.securityUtils().validatedInsert(articles);

			List<Id> listOfIds = new List<Id>();
			for (sObject article : articles) {
				listOfIds.add((String) article.get('Id'));
			}
			articles = Database.query('SELECT KnowledgeArticleId FROM ' + objectType + ' WHERE Id IN: listOfIds');
			KbManagement.PublishingService.publishArticle((Id) articles[0].get('KnowledgeArticleId'), true);

			StandAloneEditionController standaloneInstance = new StandAloneEditionController();
			AbstractSearchEngine.isTest = true;
			WorkWithArticles.isTest = true;
			List<Object> lo = standaloneInstance.getFavoritesbyId(new List<String> {(String) articles[0].get('KnowledgeArticleId')}, '');
			System.assertNotEquals(lo, null);
		}
	}

	@IsTest
	public static void testGetUserQueries() {
		StandAloneEditionController instance = new StandAloneEditionController();
		List<SearchPhraseWrapper> ls = instance.getUserQueries(5, 'test');
		System.assert(ls.size() > 0);
	}

	@IsTest
	public static void testGenerateAttachmentHTML() {
		String objectType = WorkWithArticlesTest.getKavObjectName();
		Schema.DescribeSObjectResult sObjectDescribe = TestClassUtility.getSobjectDescribe(objectType);
		sObject obj = sObjectDescribe.getSobjectType().newSObject();
		obj.put('Title', 'Unit Test 1 test');
		obj.put('Language', TestClassUtility.getAvailableKnowlegeLanguage(sObjectDescribe));
		obj.put('UrlName', 'test');
		obj.put('Summary', 'test test test test');
		if (sObjectDescribe.fields.getMap().containsKey('cncrg__Attachment__Body__s')) {
			obj.put('cncrg__Attachment__Body__s', Blob.valueOf('test test test test'));
			obj.put('cncrg__Attachment__ContentType__s', 'application/pdf');
			obj.put('cncrg__Attachment__Name__s', 'Demo_attachment.pdf');
			List<sObject> articles = new List<sObject> {obj};
			ESAPI.securityUtils().validatedInsert(articles);
			String summary = Utils.generateAttachmentHTML('cncrg__Attachment__Body__s', '', obj, sObjectDescribe.fields.getMap(), true);
			System.assert(!String.isBlank(summary));
		}

	}

	@IsTest
	public static void testGetClassicKnowledgeSuggestionsForQuery() {
		String objectType = WorkWithArticlesTest.getKavObjectName();
		Schema.DescribeSObjectResult sObjectDescribe = TestClassUtility.getSobjectDescribe(objectType);
		sObject obj = sObjectDescribe.getSobjectType().newSObject();
		obj.put('Title', 'Unit Test 1 test');
		obj.put('Language', TestClassUtility.getAvailableKnowlegeLanguage(sObjectDescribe));
		obj.put('UrlName', 'test');
		obj.put('Summary', 'test test test test');
		List<sObject> articles = new List<sObject> {obj};
		ESAPI.securityUtils().validatedInsert(articles);

		StandAloneEditionController standaloneInstance = new StandAloneEditionController();
		List<SearchPhraseWrapper> suggestions = standaloneInstance.getSuggestionsForQuery('test');
		System.assert(suggestions.size() > 0);
		System.assert(standaloneInstance.getRequiredFieldsForSearch().size() > 0);
	}

	@IsTest
	public static void testSearch() {
		insert new cncrg__Concierge_Settings__c(
			Name = 'Hours New/Updated Flag is Displayed',
			cncrg__Value__c = '24'
		);

		String objectType = WorkWithArticlesTest.getKavObjectName();
		Schema.DescribeSObjectResult sObjectDescribe = TestClassUtility.getSobjectDescribe(objectType);
		sObject obj = sObjectDescribe.getSobjectType().newSObject();
		obj.put('Title', 'Unit Test 1 test');
		obj.put('Language', Utils.getUserLanguage());
		obj.put('UrlName', 'test');
		obj.put('Summary', 'test test test test');
		insert new List<sObject> {obj};
		Id articleId = (Id)obj.get('Id');

		List<sObject> articles = Database.query('SELECT KnowledgeArticleId FROM ' + objectType + ' WHERE Id =: articleId');
		KbManagement.PublishingService.publishArticle((Id) articles[0].get('KnowledgeArticleId'), true);

		Test.startTest();
		Test.setFixedSearchResults(new List<Id>{articleId});

		StandAloneEditionController controller = new StandAloneEditionController();
		List<SearchResultsWrapper> results = controller.getSearchResults('test', '');
		System.assertNotEquals(0, results.size());
		Test.stopTest();
	}
}