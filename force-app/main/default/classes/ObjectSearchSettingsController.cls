public class ObjectSearchSettingsController  {

	public static List<cncrg__Search_Types_Settings__c> getSearchUsualObjectList() {
		List<cncrg__Search_Types_Settings__c> allInfo = new List<cncrg__Search_Types_Settings__c>();
		List<String> fieldsToCheck = new List<String>{'Name',
														'cncrg__Active__c',
														'cncrg__Fields_To_Display_1__c',
														'cncrg__Fields_To_Display_2__c',
														'cncrg__Fields_To_Display_3__c',
														'cncrg__Fields_To_Display_4__c',
														'cncrg__Article_Type_API_Name__c',
														'cncrg__Is_Usual_Object__c',
														'cncrg__Title_Field__c',
														'cncrg__Field_Filters__c'
														};
													  

		if(ESAPI.securityUtils().isAuthorizedToView('cncrg__Search_Types_Settings__c',fieldsToCheck)){
			allInfo = [
				SELECT
							Id,
							Name,
							Active__c,
							Fields_To_Display_1__c,
							Fields_To_Display_2__c,
							Fields_To_Display_3__c,
							Fields_To_Display_4__c,
							Article_Type_API_Name__c,
							Title_Field__c,
							Field_Filters__c
				FROM		Search_Types_Settings__c
				WHERE		Is_Usual_Object__c = true
				ORDER BY	Name ASC
				LIMIT		1000
			];
		}
		return allInfo;
	}

	@AuraEnabled
	public static List<SettingsPanelController.TypesWrapper> getUsualObjectSettings() { 

		Map<String,Map<String,SettingsPanelController.FieldWrapper>> allArticleTypes = new Map<String,Map<String,SettingsPanelController.FieldWrapper>>(); 
		List<Object> objectInfo = getMapOfExternal();
		allArticleTypes = (Map<String,Map<String,SettingsPanelController.FieldWrapper>>) objectInfo[0];

		List<cncrg__Search_Types_Settings__c> allInfo = getSearchUsualObjectList();
		Map<String,SettingsPanelController.TypesWrapper> convertedInfoMap = new Map<String,SettingsPanelController.TypesWrapper>();

		for (cncrg__Search_Types_Settings__c info : allInfo) {
			List<String> fieldsToDisplay =  new List<String>();

			if(info.cncrg__Fields_To_Display_1__c != null) {
				fieldsToDisplay = info.cncrg__Fields_To_Display_1__c.split(',');
			}
			if(info.cncrg__Fields_To_Display_2__c!=null) {
				fieldsToDisplay.addAll(info.cncrg__Fields_To_Display_2__c.split(','));
			}
			if(info.cncrg__Fields_To_Display_3__c!=null) {
				fieldsToDisplay.addAll(info.cncrg__Fields_To_Display_3__c.split(','));
			}
			if(info.cncrg__Fields_To_Display_4__c!=null) {
				fieldsToDisplay.addAll(info.cncrg__Fields_To_Display_4__c.split(','));
			}

			Integer fieldPosition = 1;
			SettingsPanelController.TypesWrapper setting = new SettingsPanelController.TypesWrapper(info.cncrg__Article_Type_API_Name__c, info.cncrg__Active__c,info.Id, info.Name, ((Map<String, String>)objectInfo[1]).get(info.cncrg__Article_Type_API_Name__c), info.Field_Filters__c);
			setting.title = info.cncrg__Title_Field__c;
			setting.selectedFields = new List<SettingsPanelController.FieldWrapper>();

			SettingsPanelController.FieldWrapper attachmentWrap = null;
			for(String field : fieldsToDisplay) {
				if(field == Utils.ATTACHMENTS_FLAG_NAME) {
					attachmentWrap = new SettingsPanelController.FieldWrapper(
						Utils.ATTACHMENTS_FLAG_NAME,
						Label.Display_Attachments_List,
						'attachments_flag',
						null,
						null
					);
					continue;
				}

				if (allArticleTypes.get(info.cncrg__Article_Type_API_Name__c).containsKey(field)) {
					SettingsPanelController.FieldWrapper newField = allArticleTypes.get(info.cncrg__Article_Type_API_Name__c).get(field);
					newField.position = fieldPosition;
					setting.selectedFields.add(newField);
					fieldPosition++;
				}
			}

			if (attachmentWrap != null) {
				attachmentWrap.position = fieldPosition;
				setting.selectedFields.add(attachmentWrap);
			}
			convertedInfoMap.put(info.cncrg__Article_Type_API_Name__c, setting);
		}

		for (String type : allArticleTypes.keySet()) {
			if (convertedInfoMap.keySet().contains(type)) {
				convertedInfoMap.get(type).allAvailableFields = allArticleTypes.get(type).values();
			} else {
				SettingsPanelController.TypesWrapper setting = new SettingsPanelController.TypesWrapper(type, false, null,'', ((Map<String, String>)objectInfo[1]).get(type));
				setting.selectedFields = new List<SettingsPanelController.FieldWrapper>();
				setting.allAvailableFields = allArticleTypes.get(type).values();
				convertedInfoMap.put(
					type,
					setting
				);
			}
			convertedInfoMap.get(type).allAvailableFields.add(
				new SettingsPanelController.FieldWrapper(
					Utils.ATTACHMENTS_FLAG_NAME,
					Label.Display_Attachments_List,
					'attachments_flag',
					null,
					null
				)
			);
		}

		return convertedInfoMap.values();

	}
	
    //private static Map<String,Map<String,FieldWrapper>> getMapOfExternal() {
	private static List<Object> getMapOfExternal() {
        Map<String,Map<String,SettingsPanelController.FieldWrapper>> mapOfTypes = new Map<String,Map<String,SettingsPanelController.FieldWrapper>>();
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        Map<String, String> objectsNamesMap = new Map<String, String>();
        Set<String> keySet = gd.keySet();
        for (String key : keySet) {
            Schema.SObjectType objectType = gd.get(key);
            Schema.DescribeSObjectResult objectDescribe = objectType.getDescribe();
            String sobjectName = objectDescribe.getName();
            objectsNamesMap.put(sobjectName, objectDescribe.getLabel());
            if (objectDescribe.isSearchable() && !objectDescribe.isCustomSetting() && ((objectDescribe.isCustom() && !sobjectName.containsIgnoreCase('__kav') && !sobjectName.startsWithIgnoreCase('cncrg__')) || (
				 (!sobjectName.containsIgnoreCase('history') && 
				 !sobjectName.containsIgnoreCase('tag') && 
				 !sobjectName.containsIgnoreCase('share') && 
				 !sobjectName.containsIgnoreCase('feed') && 
				 !sobjectName.containsIgnoreCase('__kav') &&
				 !sobjectName.containsIgnoreCase('__DataCategorySelection')) && !objectDescribe.isCustom()) )) { //key.endsWith('__c')
				try {
					mapOfTypes.put(
						sobjectName,
						new Map<String,SettingsPanelController.FieldWrapper>(
							SettingsPanelController.getAllFieldsMap(
								sobjectName
							)
						)
					);
				}
				catch(Exception e) {}
            }
        }
        return new List<Object> {mapOfTypes, objectsNamesMap};
    }
	@AuraEnabled
    public static String implementNewSettings(String newSettings) {
		return SettingsPanelController.implementNewSettings(newSettings);
	}
}