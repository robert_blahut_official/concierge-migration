@isTest
public class MockHttpResponseGenerator implements HttpCalloutMock {
	public static String CATEGORY_CHILDS_RESPONSE = '{"categoryGroups":[{"label":"Function","name":"Function","objectUsage":"KnowledgeArticleVersion","topCategories":[{"childCategories":null,"label":"All","name":"All","url":"/services/data/v45.0/support/dataCategoryGroups/Function/dataCategories/All?sObjectName=KnowledgeArticleVersion"}]}]}';
	public static String CATEGORY_TOP_RESPONSE = '{"childCategories":[],"label":"HR","name":"HR","url":"/services/data/v45.0/support/dataCategoryGroups/Function/dataCategories/HR?sObjectName=KnowledgeArticleVersion"}';

	protected Integer code;
	protected String status;
	protected String body;
	protected Map<String, String> responseHeaders;

	public MockHttpResponseGenerator(Integer code, String status, String body, Map<String, String> responseHeaders) {
		this.code = code;
		this.status = status;
		this.body = body;
		this.responseHeaders = responseHeaders;
	}

	public HTTPResponse respond(HTTPRequest req) {
		HttpResponse res = new HttpResponse();
		for (String key : this.responseHeaders.keySet()) {
			res.setHeader(key, this.responseHeaders.get(key));
		}
		res.setBody(this.body);
		res.setStatusCode(this.code);
		res.setStatus(this.status);
		System.assertEquals(this.body, res.getBody());

		String endpoint = req.getEndpoint();
		if (String.isNotBlank(endpoint) && endpoint.contains(KnowledgeDataCategoryService.DATA_CATEGORY_GROUPS_URL)) {
			res.setBody(CATEGORY_CHILDS_RESPONSE);
		}
		if (String.isNotBlank(endpoint) && endpoint.contains('/support/dataCategoryGroups/Function/dataCategories/')) {
			res.setBody(CATEGORY_TOP_RESPONSE);
		}
		return res;
	}
}