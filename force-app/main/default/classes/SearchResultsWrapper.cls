global class SearchResultsWrapper implements Comparable {
	@AuraEnabled
	public String title { get; set; }
	@AuraEnabled
	public String snippet { get; set; }
	@AuraEnabled
	public String recordId { get; set; }
	@AuraEnabled
	public String body { get; set; }
	@AuraEnabled
	public String previewBody { get; set; }
	@AuraEnabled
	// configuration name in Search Settings
	public String type { get; set; }
	@AuraEnabled
	// article or object
	public String typeName { get; set; }
	@AuraEnabled
	// article source - Heroku / Salesforce / etc
	public String storage {get; set;}
	@AuraEnabled
	public String dataCategories { get; set; }
	@AuraEnabled
	public Integer rating { get; set; }
	@AuraEnabled
	public Boolean isLike { get; set; }
	@AuraEnabled
	public Boolean isFavorite { get; set; }
	@AuraEnabled
	public Case_Layout_Assignment__c settingCaseLayout { get; set; }
	@AuraEnabled
	public Action_Setting__c setting { get; set; }
	@AuraEnabled
	public String knowledgeArticleId { get; set; }
	@AuraEnabled
	public Integer articleAge { get; set; }
	@AuraEnabled
	public Boolean isDisabled { get; set; }
	@AuraEnabled
	public String masterVersionId { get; set; }
	@AuraEnabled
	public Boolean isKnowledgeObject { get; set; }
	@AuraEnabled
	public String voteLastModifiedDate { get; set; }
	@AuraEnabled
	public String ownerId { get; set; }
	@AuraEnabled
	public String updateIndicator { get; set; }
	public Decimal totalScore { get; set; }
	@AuraEnabled
	public Boolean showRelatedAttachments { get; set; }
	@AuraEnabled
	public List<OptionWrapper> categoryList {get;set;}
	///////////////////////////
	@AuraEnabled
	public Decimal relevancyScore { get; set; }
	@AuraEnabled
	public Decimal searchHistoryScore { get; set; }
	@AuraEnabled
	public Decimal ageScore { get; set; }
	@AuraEnabled
	public Decimal votesScore { get; set; }
	///////////////////////////

	public SearchResultsWrapper(String title, String snippet, String recordId, String body, String type) { //, Integer rating, String rateField) {
		this.title = title;
		this.showRelatedAttachments = false;
		this.snippet = snippet;
		this.recordId = recordId;
		this.body = body;
		this.rating = 0;
		this.type = type;
		this.typeName = 'article';
		this.storage = 'salesforce';
		this.isFavorite = false;
		this.isDisabled = false;
		this.isKnowledgeObject = true;
	}
	public SearchResultsWrapper(String title, String snippet, String recordId, String body, String type, Integer articleAge, String knowledgeArticleId, String masterVersionId, String updateIndicator, String ownerId) {
		this(title, snippet, recordId, body, type, articleAge, knowledgeArticleId, masterVersionId, updateIndicator);
		this.ownerId = ownerId;
	}
	public SearchResultsWrapper(String title, String snippet, String recordId, String body, String type, Integer articleAge, String knowledgeArticleId, String masterVersionId, String updateIndicator) {
		this(title, snippet, recordId, body, type);
		this.articleAge = articleAge;
		this.knowledgeArticleId = knowledgeArticleId;
		this.masterVersionId = masterVersionId;
		this.updateIndicator = updateIndicator;
	}
	public SearchResultsWrapper(String title, String snippet, String recordId, String body, String type, Integer articleAge, String knowledgeArticleId, String masterVersionId) {
		this(title, snippet, recordId, body, type);
		this.articleAge = articleAge;
		this.knowledgeArticleId = knowledgeArticleId;
		this.masterVersionId = masterVersionId;
	}
	public SearchResultsWrapper(String title, String snippet, String recordId, String body, String type, Integer articleAge) {
		this(title, snippet, recordId, body, type);
		this.articleAge = articleAge;
	}
	public Integer compareTo(Object compareTo) {
		SearchResultsWrapper obj = (SearchResultsWrapper) compareTo;

		if (this.totalScore > obj.totalScore) {
			return - 1;
		}
		else if (this.totalScore == obj.totalScore) {
			return 0;
		}
		return 1;
	}

	public class OptionWrapper {
		@AuraEnabled
		public String value {get;set;}
		@AuraEnabled
		public String label {get;set;}
	}
}