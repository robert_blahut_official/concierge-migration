public with sharing  class ArchiveArticlesBatch implements Database.Batchable<sObject>, Database.Stateful {
    private String query;
    private Set<Id> articleForProcessed;
    private Map<String, Datetime> articleMap;

    public static Boolean isProcessed = false;

    public ArchiveArticlesBatch(Set<Id> articleForArchivedIds) {
        articleForProcessed = articleForArchivedIds;
        articleMap = new Map<String, Datetime>();
        query = 'SELECT Parent_Article__c, Expiration_Date__c FROM Article_Review__c WHERE Id IN :articleForProcessed';
    }

    public Database.QueryLocator start(Database.BatchableContext BC) {
        return (query != '') ? Database.getQueryLocator(query) : null;
    }

    public void execute(Database.BatchableContext BC, List<Article_Review__c> scope) {
        if(scope[0].Expiration_Date__c != null) {
            try {
                KbManagement.PublishingService.deleteDraftArticle(scope[0].Parent_Article__c);
            } catch (Exception ex) {
                System.debug(ex); //the draft article doesn't exist
            }
            KbManagement.PublishingService.archiveOnlineArticle(scope[0].Parent_Article__c, scope[0].Expiration_Date__c);
        }
        else {
            KbManagement.PublishingService.cancelScheduledArchivingOfArticle(scope[0].Parent_Article__c);
        }
        articleMap.put(scope[0].Parent_Article__c, scope[0].Expiration_Date__c);
    }

    public void finish(Database.BatchableContext BC) {
        updateReferencesArticleReview();
    }

    private void updateReferencesArticleReview() {
        isProcessed = true;
        List<Article_Review__c> articleReviewForUpdateList = [SELECT Id, Parent_Article__c, Expiration_Date__c FROM Article_Review__c WHERE Id NOT IN :articleForProcessed AND Parent_Article__c IN :articleMap.keySet()];
        for(Integer i = 0; i < articleReviewForUpdateList.size(); i++) {
            articleReviewForUpdateList[i].Expiration_Date__c = articleMap.get(articleReviewForUpdateList[i].Parent_Article__c);
        }
        if(!articleReviewForUpdateList.isEmpty())
            ESAPI.securityUtils().validatedUpdate(articleReviewForUpdateList);
        isProcessed = false;
    }
}