public with sharing class CaseLayoutSettingsController {
    public class CaseLayoutWrapper {
        @AuraEnabled
        public List<WorkWithActionsSettings.InfoWrapper> categoriesList {get;set;}
        @AuraEnabled
        public List<WorkWithActionsSettings.InfoWrapper> fieldSetList {get;set;}
        @AuraEnabled
        public WorkWithActionsSettings.InfoWrapper articleType {get;set;}
        @AuraEnabled
        public List<RecordTypesWrapper> recordTypesList {get;set;}


        public CaseLayoutWrapper() {

        }
    }
    public static List<String> fieldsToCheckCaseLayout = new List<String> {
            'cncrg__Article_Id__c',
            'cncrg__Article_Type__c',
            'cncrg__Data_Category__c',
            'cncrg__Field_Set__c'
    };
	private static String getsObjectLabel(String sObjectAPIName) {
		try {
			SObject objectRecord = (SObject)Type.forName(sObjectAPIName).newInstance();
			Schema.SObjectType sObjType = objectRecord.getSObjectType();
			return sObjType.getDescribe().getLabel();
		}
		catch(Exception e) {
			return sObjectAPIName;
		}
	}
    @AuraEnabled
    public static CaseLayoutWrapper getCaseLayoutWrapper(String recordId, String articleType) {
        CaseLayoutWrapper caseLayoutWrapperRecord = new CaseLayoutWrapper();
        String atricleTypeName;
        String atricleTypeLabel;
        if(Utils.isLightningKnowledgeEnabled()) {
			try {
				atricleTypeName = articleType;
				atricleTypeLabel = getsObjectLabel(articleType);

			}
			catch(Exception e) {
				atricleTypeLabel = atricleTypeName = articleType;
				articleType = Utils.getLightningKnowledgeObjectName();
			}
            
        }
        else {
			atricleTypeName = articleType;
            atricleTypeLabel = getsObjectLabel(articleType);
        }
        caseLayoutWrapperRecord.categoriesList = WorkWithActionsSettings.getAllArticleCategories(recordId, articleType);
        caseLayoutWrapperRecord.fieldSetList = getFieldSetList();
        caseLayoutWrapperRecord.articleType = new WorkWithActionsSettings.InfoWrapper(atricleTypeName, atricleTypeLabel);
        caseLayoutWrapperRecord.recordTypesList = getRecordTypesInfo();
        return caseLayoutWrapperRecord;
    }
    private static List<WorkWithActionsSettings.InfoWrapper> getFieldSetList() {
        List<WorkWithActionsSettings.InfoWrapper> infoWrappersList = new List<WorkWithActionsSettings.InfoWrapper>();
        String strObjectApiName = 'Case';
        SObject objectRecord = (SObject)Type.forName(strObjectApiName).newInstance();
		Schema.SObjectType sObjType = objectRecord.getSObjectType();
        Map<String, Schema.FieldSet> fieldSetMap = sObjType.getDescribe().fieldsets.getMap();
        for(String item : fieldSetMap.keySet()) {
            infoWrappersList.add(new WorkWithActionsSettings.InfoWrapper(item, fieldSetMap.get(item).getLabel()));
        }
        return infoWrappersList;

    }
    private static List<RecordTypesWrapper> getRecordTypesInfo(){
        List<RecordTypesWrapper> rtList = new List<RecordTypesWrapper>();
        Schema.DescribeSObjectResult R = Case.SObjectType.getDescribe();
        List<Schema.RecordTypeInfo> RT = R.getRecordTypeInfos();

        for(Schema.RecordTypeInfo recType : RT){
            rtList.add(
                    new RecordTypesWrapper(
                            recType.getName(),
                            recType.getRecordTypeId()
                    )
            );
        }

        return rtList;
    }

    @AuraEnabled
    public static cncrg__Case_Layout_Assignment__c createNewSetting(String caseLayoutRecordStr) {
    	cncrg__Case_Layout_Assignment__c caseLayoutRecord = (cncrg__Case_Layout_Assignment__c) JSON.deserialize(caseLayoutRecordStr, cncrg__Case_Layout_Assignment__c.class);
        if(ESAPI.securityUtils().isAuthorizedToView('cncrg__Case_Layout_Assignment__c', fieldsToCheckCaseLayout)) {
            cncrg__Case_Layout_Assignment__c caseLayoutRecordOld;
            List<cncrg__Case_Layout_Assignment__c> caseLayoutRecordOldList;
                caseLayoutRecordOldList = [
                        SELECT
                                Id,
                                cncrg__Article_Id__c,
                                cncrg__Article_Type__c,
                                cncrg__Data_Category__c,
                                cncrg__Field_Set__c
                        FROM    cncrg__Case_Layout_Assignment__c
                        WHERE   cncrg__Article_Id__c = :caseLayoutRecord.cncrg__Article_Id__c AND
                                cncrg__Data_Category__c = :caseLayoutRecord.cncrg__Data_Category__c AND
                                cncrg__Article_Type__c = :caseLayoutRecord.cncrg__Article_Type__c //AND
                                //Id !=: caseLayoutRecord.Id

                        ORDER BY CreatedDate DESC
                ];
                if(caseLayoutRecordOldList.size() == 0) {
                    caseLayoutRecordOld = new cncrg__Case_Layout_Assignment__c(
                            cncrg__Article_Id__c = caseLayoutRecord.cncrg__Article_Id__c,
                            cncrg__Article_Type__c = caseLayoutRecord.cncrg__Article_Type__c,
                            cncrg__Data_Category__c = caseLayoutRecord.cncrg__Data_Category__c,
                            cncrg__Field_Set__c = caseLayoutRecord.cncrg__Field_Set__c
                    );
                    ESAPI.securityUtils().validatedUpsert(new List<cncrg__Case_Layout_Assignment__c> {caseLayoutRecordOld});
                    return caseLayoutRecordOld;
                }
                else if(caseLayoutRecordOldList.size() == 1 && caseLayoutRecordOldList[0].Id == caseLayoutRecord.Id) {
                    ESAPI.securityUtils().validatedUpdate(new List<cncrg__Case_Layout_Assignment__c>{
                            caseLayoutRecord
                    });
                    return caseLayoutRecord;
                }
                else {
                    for(cncrg__Case_Layout_Assignment__c item : caseLayoutRecordOldList) {
                        if(item.Id != caseLayoutRecord.Id) {
                            caseLayoutRecordOld = item;
                            break;
                        }
                    }
                    caseLayoutRecordOld.cncrg__Field_Set__c = caseLayoutRecord.cncrg__Field_Set__c;
                    deleteSetting(JSON.serialize(caseLayoutRecord));
                    ESAPI.securityUtils().validatedUpdate(new List<cncrg__Case_Layout_Assignment__c>{
                            caseLayoutRecordOld
                    });
                    return caseLayoutRecordOld;
                }
        }
        return null;
    }
    @AuraEnabled
    public static void deleteSetting(String caseLayoutRecordStr) {
    	cncrg__Case_Layout_Assignment__c caseLayoutRecord = (cncrg__Case_Layout_Assignment__c) JSON.deserialize(caseLayoutRecordStr, cncrg__Case_Layout_Assignment__c.class);
        if(ESAPI.securityUtils().isAuthorizedToView('cncrg__Case_Layout_Assignment__c', fieldsToCheckCaseLayout)) {
            caseLayoutRecord = [
                    SELECT
                            Id,
                            cncrg__Article_Id__c,
                            cncrg__Article_Type__c,
                            cncrg__Data_Category__c,
                            cncrg__Field_Set__c
                    FROM cncrg__Case_Layout_Assignment__c
                    WHERE Id = :caseLayoutRecord.Id

                    ORDER BY CreatedDate DESC
                    LIMIT 1
            ];
            if (String.isNotBlank(caseLayoutRecord.cncrg__Article_Id__c) || String.isNotBlank(caseLayoutRecord.cncrg__Article_Type__c) || String.isNotBlank(caseLayoutRecord.cncrg__Data_Category__c)) {
                ESAPI.securityUtils().validatedDelete(new List<cncrg__Case_Layout_Assignment__c>{
                        caseLayoutRecord
                });
            }
        }


    }

    public class RecordTypesWrapper {

        @AuraEnabled
        public String RecordTypeName {get; set;}
        @AuraEnabled
        public String RecordTypeId {get; set;}

        public RecordTypesWrapper(String recTypeName, String recTypeId) {
            this.RecordTypeName = recTypeName;
            this.RecordTypeId = recTypeId;
        }
    }

}