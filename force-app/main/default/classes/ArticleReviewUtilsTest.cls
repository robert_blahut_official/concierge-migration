@isTest
private class ArticleReviewUtilsTest {

	@TestSetup static void setup() {
		insert new Concierge_Settings__c(Name = ArticleReviewUtils.AUTOMATICALLY_REVIEW_CUSTOM_SETTINGS_NAME, Value__c = '5');

		List<SchedulerSettings__c> schedSettingList = new List<SchedulerSettings__c>{
				new SchedulerSettings__c(
						Name = TurboEncabulatorScheduler.TURBO_BATCH_NAME,
						Class_Name__c = TurboEncabulatorScheduler.TURBO_BATCH_NAME,
						CRON__c = TurboEncabulatorScheduler.TURBO_CRON,
						Batch_Size__c = TurboEncabulatorScheduler.TURBO_BATCH_SIZE,
						Description__c = TurboEncabulatorScheduler.TURBO_DESCRIPTION,
						Active__c = true
				),
				new SchedulerSettings__c(
						Name = TurboEncabulatorScheduler.REVIEW_BATCH_NAME,
						Class_Name__c = TurboEncabulatorScheduler.REVIEW_BATCH_NAME,
						CRON__c = TurboEncabulatorScheduler.REVIEW_CRON,
						Batch_Size__c = TurboEncabulatorScheduler.REVIEW_BATH_SIZE,
						Description__c = TurboEncabulatorScheduler.REVIEW_DESCRIPTION,
						Active__c = true
				)
		};
		Database.insert(schedSettingList, false);

		String objectType = WorkWithArticlesTest.getKavObjectName();
		TestClassUtility.createSearchEngineConfig(objectType, AbstractSearchEngine.SALESFORCE_SEARCH_TYPE);

		Web_Service_Settings__c wss = new cncrg__Web_Service_Settings__c();
		wss.Name = 'HerokuWebService';
		wss.Password__c = 'password';
		wss.URL__c = 'https://cconsearchengine.herokuapp.com';
		insert wss;



		cncrg__Search_Types_Settings__c setting = new cncrg__Search_Types_Settings__c(
		                                                                                      Name = objectType,
		                                                                                      cncrg__Article_Type_API_Name__c = objectType,
		                                                                                      cncrg__Active__c = true,
		                                                                                      cncrg__Fields_To_Display_1__c = 'OwnerId'
		);
		List<cncrg__Search_Types_Settings__c> settings = new List<cncrg__Search_Types_Settings__c> ();
		settings.add(setting);
		ESAPI.securityUtils().validatedInsert(settings);

		

		if (objectType != null) {
			sObject obj = Schema.getGlobalDescribe().get(objectType).newSObject();
			obj.put('Title', 'test apex');
			obj.put('Language', 'en_US');
			obj.put('UrlName', 'test-apex');
			obj.put('Summary', 'test test test test');

			sObject obj2 = Schema.getGlobalDescribe().get(objectType).newSObject();
			obj2.put('Title', 'test apex2');
			obj2.put('Language', 'en_US');
			obj2.put('UrlName', 'test-apex2');
			obj2.put('Summary', 'test test test test');

			List<sObject> articles = new List<sObject> { obj, obj2 };
			insert articles;

			List<Id> listOfIds = new List<Id> ();
			for (sObject article : articles)
			listOfIds.add((String) article.get('Id'));

			articles = Database.query('SELECT KnowledgeArticleId FROM ' + objectType + ' WHERE Id IN: listOfIds');

			KbManagement.PublishingService.publishArticle((Id) articles[0].get('KnowledgeArticleId'), true);
			KbManagement.PublishingService.publishArticle((Id) articles[1].get('KnowledgeArticleId'), true);

			Article_Review__c artReview = new Article_Review__c(
			                                                    Name = 'test name',
																Full_Name__c = 'test full name',
			                                                    Language__c = 'en_US',
			                                                    Article_Type__c = 'Knowledge',
			                                                    Parent_Article__c = (String) articles[0].get('KnowledgeArticleId'),
			                                                    Expiration_Date__c = Datetime.now().addHours(1));
			insert artReview;
			Datetime dt = Datetime.now().addMinutes(-5);
			Long createTime = dt.getTime();
			Test.setCreatedDate(artReview.Id,Datetime.newInstance(createTime));
		}
	}

	@IsTest
	private static void getNextReviewDayTest() {
		Date d = ArticleReviewUtils.getNextReviewDay();
		System.assertEquals(Date.today().addDays(5), d);
	}

	@isTest
	private static void updateLikedORFavoritedArticleTest() {
		String language = 'en_US';
		String objectType = WorkWithArticlesTest.getKavObjectName();
		SObject article = Database.query('SELECT KnowledgeArticleId,Title,Language,OwnerId, URLName, LastModifiedDate  FROM '
		                                 + objectType + ' WHERE Title= \'test apex\' ');
		SearchResultsWrapper wrap = new SearchResultsWrapper(null, null, null, null, null, null, (String) article.get('KnowledgeArticleId'), null);
		String draftId = KbManagement.PublishingService.editOnlineArticle((Id) article.get('KnowledgeArticleId'), true);
		SObject draftArticle = Database.query('SELECT KnowledgeArticleId,PublishStatus,Title,Language,OwnerId, URLName, LastModifiedDate  FROM '
		                                      + objectType + ' WHERE PublishStatus=\'draft\' AND Title=\'test apex\'');
		draftArticle.put('Title', 'test apex');
		update draftArticle;
		KbManagement.PublishingService.publishArticle((String) draftArticle.get('KnowledgeArticleId'), true);
		SearchResultsWrapper wrap2 = new SearchResultsWrapper(null, null, null, null, null, null, (String) article.get('KnowledgeArticleId'), null);

		SObject article2 = Database.query('SELECT KnowledgeArticleId,Title,Language,OwnerId, URLName, LastModifiedDate  FROM '
		                                  + objectType + ' WHERE Title= \'test apex2\' ');
		SearchResultsWrapper wrap3 = new SearchResultsWrapper(null, null, null, null, null, null, (String) article2.get('KnowledgeArticleId'), null);
		System.assertNotEquals(null, wrap3);
		Test.startTest();
	}

	@isTest
	private static void findArticleReviewByKnowledgeIdsTest() {
		String language = 'en_US';
		String objectType = WorkWithArticlesTest.getKavObjectName();
		List<SObject> articleList = Database.query('SELECT KnowledgeArticleId,Title,Language,OwnerId, URLName  FROM ' + objectType);
		Set<String> idSet = new Set<String> ();
		for (SObject art : articleList) {
			idSet.add((String) art.get('KnowledgeArticleId'));
		}
		Test.startTest();
		List<Article_Review__c> artRevList = ArticleReviewUtils.findArticleReviewByKnowledgeIds(idSet, objectType, language);
		Test.stopTest();
		System.assertEquals(1, artRevList.size());
	}

	@isTest
	private static void getAutomaticallyReviewDayTest() {
		Concierge_Settings__c cosSet = Concierge_Settings__c.getAll().get('Review articles after _ days');
		cosSet.Value__c = null;
		update cosSet;
		Test.startTest();
		Integer i = ArticleReviewUtils.getAutomaticallyReviewDay();
		Test.stopTest();
		System.assertEquals(null, i);
		Concierge_Settings__c retcosSet = Concierge_Settings__c.getAll().get('Review articles after _ days');
		retcosSet.Value__c = '3';
		update retcosSet;
	}

	@isTest
	private static void createNewArticleTest() {
		String objectType = WorkWithArticlesTest.getKavObjectName();
		SObject article = Database.query('SELECT KnowledgeArticleId,Title,Language,OwnerId, URLName  FROM ' + objectType
		                                 + ' WHERE URLName= \'test-apex\' ');
		Test.startTest();
		Article_Review__c artRev = ArticleReviewUtils.createNewArticleReviewRecord((String) article.get('KnowledgeArticleId'),
																				   (String) article.get('Title'),
		                                                                           Date.today(),
		                                                                           objectType);
		Test.stopTest();
		System.assertNotEquals(null, artRev);

	}
	@isTest
	private static void createArticleReviewTest() {
		String objectType = WorkWithArticlesTest.getKavObjectName();
		SObject article = Database.query('SELECT KnowledgeArticleId,Title,Language,OwnerId, URLName  FROM ' + objectType
		                                 + ' WHERE URLName= \'test-apex\' ');
		Test.startTest();
		Article_Review__c articleRev = ArticleReviewUtils.createNewArticleReviewRecord(article,
																					   (String) article.get('Language'),
		                                                                               Date.today());
		String JSONContent = '{"title":"' + (String) article.get('Title') + '", "Id": ' + 11 + '}';
		JSONParser parser = JSON.createParser(JSONContent);
		Article_Review__c articleRev2 = ArticleReviewUtils.createNewArticleReviewRecord(new JsonDeserializerDocuments.Result(parser),
																						(String) article.get('Language'),
		                                                                                Date.today());
		Test.stopTest();
		System.assertNotEquals(null, articleRev);
		System.assertNotEquals(null, articleRev2);
	}



	/*@isTest
	private static void createArticleTest() {
		String objectType = WorkWithArticlesTest.getKavObjectName();
		SObject article = Database.query('SELECT KnowledgeArticleId,Title,Language,OwnerId, URLName  FROM ' + objectType
		                                 + ' WHERE URLName= \'test-apex\' ');
		Test.startTest();
		Article_Review__c articleRev = ArticleReviewUtils.createNewArticleReviewRecordForLightning(article,
																								   (String) article.get('Language'),
		                                                                                           Date.today());
		Test.stopTest();
		System.assertNotEquals(null, articleRev);
	}*/

	@IsTest
	private static void reviewArticleTest() {
		Article_Review__c artReview = [SELECT Id,
		                               Name,
		                               Language__c,
		                               CreatedDate,
		                               Parent_Article__c,
		                               LastModifiedDate,
		                               Expiration_Date__c
		                               FROM Article_Review__c
		                               WHERE Name = 'test name'
		                              ];
		Test.startTest();
		Boolean check = ArticleReviewUtils.reviewArticle(artReview.Id);
		Test.stopTest();
		System.assertEquals(true, check);
	}

	@IsTest
	private static void findArticleTest() {
		Article_Review__c artReview = [SELECT Id,
		                               Name,
		                               Language__c,
		                               CreatedDate,
		                               Parent_Article__c,
		                               LastModifiedDate,
		                               Expiration_Date__c
		                               FROM Article_Review__c
		                               WHERE Name = 'test name'
		                              ];
		Test.startTest();
		SearchResultsWrapper srw = ArticleReviewUtils.findArticle(artReview.Id);
		Test.stopTest();
		System.assertNotEquals(null, srw);
	}

	@IsTest
	private static void getAllArticleLanguagesTest() {
		Test.startTest();
		List<String> artLang = ArticleReviewUtils.getAllArticleLanguages();
		Test.stopTest();
		System.assertNotEquals(null, artLang);
	}

	@IsTest
	private static void isMasterLanguageTest() {
		Article_Review__c artReview = [SELECT Id,
		                               Name,
		                               Language__c,
		                               CreatedDate,
		                               Parent_Article__c,
		                               LastModifiedDate,
		                               Expiration_Date__c
		                               FROM Article_Review__c
		                               WHERE Name = 'test name'
		                              ];
		Test.startTest();
		Boolean lang = ArticleReviewUtils.isMasterLanguage(artReview.Id);
		Test.stopTest();
		System.assertEquals(true, lang);
	}


	@IsTest
	private static void prepareQueryTest() {
		Test.startTest();
		String objectType = WorkWithArticlesTest.getKavObjectName();
		String xEndObjectType = objectType.substring(0,objectType.length()-3)+'x';
		String result = ArticleReviewUtils.preparedQuery(objectType, 'en_US');
		String result2 = ArticleReviewUtils.preparedQuery(xEndObjectType, 'en_US');
		Test.stopTest();
		System.assertNotEquals(null, result);
		System.assertNotEquals(null, result2);
	}

	@IsTest
	private static void prepareQueryTestifNull() {
		Test.startTest();
		String result = ArticleReviewUtils.preparedQuery(null, 'en_US');
		Test.stopTest();
		System.assertEquals(null, result);
	}

	@IsTest
	private static void testGetHerokuArticles() {
		String mockBody = '{"results": {' +
		'"sqlstatement": "SELECT id, product_name FROM products WHERE to_tsvector(\u0027english\u0027,coalesce(description,\u0027\u0027) || \u0027 \u0027 || coalesce(product_name,\u0027\u0027)) @@ plainto_tsquery(\u0027need to connect\u0027)",' +
		'"records": [' +
		'{ "id": 33,' +
		'"name": "New Article"}],' +
		'"javatimemls": 1560},' +
		'"error": false}';
		Map<String, String> headers = new Map<String, String> ();
		headers.put('Content-Type', 'application/json');
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(200, 'OK', mockBody, headers));
		Test.startTest();
		System.assertNotEquals(ArticleReviewUtils.getHerokuArticles().isEmpty(), false);
		Test.stopTest();
	}

}