/*
 * 	ATTENTION
  Create global methods only by template:
  global virtual List<String> methodName(List<String> genericJSONParameterList)
*/
global with sharing virtual class CustomerExtensionBase {
	@TestVisible
	private static Map<String, String> customCaseSettings = new Map<String, String>();
	private static final String CASE_ORIGIN_SETTING_NAME = 'Case Origin';
	private static final String PREVIEW_BODY_FIELD_NAME = 'previewBody';

	global virtual List<String> getBackgroundWrapper(List<String> genericJSONParameterList) {
		cncrg__Background_Settings__c settingRecord = cncrg__Background_Settings__c.getValues(genericJSONParameterList[0]);
		Map<String, Object> config = new Map<String, Object> {
			'name' => settingRecord.Name,
			'backgroundFileType' => settingRecord.cncrg__Background_File_Type__c,
			'fileLocation' => settingRecord.cncrg__File_Location__c,
			'isStaticResurces' => true,
			'portraitDocumintId' => '',
			'landscapeDocumintId' => ''
		};
		return new List<String> { JSON.serialize(config) };
	}

	global virtual List<String> getCountryRelevancyScore(List<String> genericJSONParameterList) {
		return genericJSONParameterList;
	}

	global virtual String getRelatedContactRecord(String genericJSONParameter) {
		return null;
	}

	global virtual String populateArticlePreview(String articleRecordStr) {
		if(Utils.isCustomSnippetNeeded()) {
			List<Object> articleRecordStrList = (List<Object>) JSON.deserializeUntyped(articleRecordStr);
			return getDetailArticleInformation(String.valueOf(articleRecordStrList[0]), String.valueOf(articleRecordStrList[1]));
		}
		String articleRecorddeserialize = String.valueOf(((List<Object>) JSON.deserializeUntyped(articleRecordStr)) [0]);
		return articleRecorddeserialize;
	}

	global static String getDetailArticleInformation(String articleRecordStr, String prefix) {
		return JSON.serialize(SearchQueryInputController.getDetailArticleInformation(articleRecordStr, prefix));
	}

	global virtual List<String> setArticlePreviewValue(List<String> genericJSONParameterList) {
		if (!Utils.isCustomSnippetNeeded()) {
			return genericJSONParameterList;
		}

		Map<String, Object> articleRecordMap = (Map<String, Object>) JSON.deserializeUntyped(genericJSONParameterList[0]);
		Map<String, Object> articleRecordFromBaseMap = (Map<String, Object>) JSON.deserializeUntyped(genericJSONParameterList[1]);

		String currentView = '';
		// get Text value from record
		String articleId = (String)articleRecordFromBaseMap.get('Id');
		if (articleId != null) {
			Map<String, Schema.SObjectField> fieldMap = Id.valueOf(articleId).getSObjectType().getDescribe().fields.getMap();
			for (String fieldName : articleRecordFromBaseMap.keySet()) {
				if (fieldMap.get(fieldName.toLowerCase()) != null) {
					Schema.DisplayType fieldType = fieldMap.get(fieldName.toLowerCase()).getDescribe().getType();
					if ((fieldType == Schema.DisplayType.String || fieldType == Schema.DisplayType.TextArea)
							&& articleRecordFromBaseMap.get(fieldName) != null) {
						currentView = String.valueOf(articleRecordFromBaseMap.get(fieldName));
						break;
					}
				}
			}
		}
		//Set<String> standardFields = new Set<String>{'Id', 'attributes'};
		//for (String key : articleRecordFromBaseMap.keySet()) {
		//	if (!standardFields.contains(key) && !key.contains('__s') && articleRecordFromBaseMap.get(key) != null) {
		//		currentView = String.valueOf(articleRecordFromBaseMap.get(key));
		//		break;
		//	}
		//}

		if (currentView.indexOf('{!') > 0 && currentView.indexOf('}') > 0) {
			if (currentView.indexOf('}') + 1 < currentView.length()) {
				currentView = currentView.substring(currentView.lastIndexOf('}') + 1).trim();
			} else {
				currentView = '';
			}
		}

		currentView = cutPreviewToLength(currentView);
		String filteredSnippet = Utils.addSnippetElipsis(currentView, true);

		articleRecordMap.put('snippet', filteredSnippet);
		articleRecordMap.put(PREVIEW_BODY_FIELD_NAME, filteredSnippet); //Utils.addSnippetElipsis(currentView);
		return new List<String> {JSON.serialize(articleRecordMap), JSON.serialize(articleRecordFromBaseMap)};
	}

	public static String cutPreviewToLength(String currentView) {
		String snippetLength = Utils.getValueFromConciergeSettings(AbstractSearchEngine.SNIPPET_LENGTH_SETTING_NAME);
		if (String.isNotBlank(currentView)) {
			Integer snippetLengthInt = 0;
			try {
				snippetLengthInt = Integer.valueOf(snippetLength);
			} catch(Exception e) {}

			snippetLengthInt = snippetLengthInt == 0 ? 120 : snippetLengthInt;

			Integer currentViewLength = currentView.stripHtmlTags().length();
			if (currentViewLength > snippetLengthInt) {
				currentView = Utils.removeEndSpecCharacters(currentView.stripHtmlTags().substring(0, snippetLengthInt).substringBeforeLast(' ').trim());
				currentView += '...';
			}
		}
		return currentView;
	}

	//@IMPORTANT: implemented article filters by Language: Uniliver
	global virtual List<String> getUserLocale(List<String> genericJSONParameterList) {
		return new List<String> { UserInfo.getLanguage() };
	}

	//@IMPORTANT: implemented article filters by Data Category: Uniliver
	global virtual List<String> getArticleFilterOptions(List<String> genericJSONParameterList) {
		return new List<String>();
	}

	global virtual List<String> getGreeting() {
		String message = '';
		Integer hour = DateTime.now().hour();

		if (SearchQueryInputController.isTest && SearchQueryInputController.valueFoTestGreeting != null) {
			hour = SearchQueryInputController.valueFoTestGreeting;
		}

		if (hour >= 4 && hour < 12) {
			message = 'Good Morning ';      
		} else if (hour >= 12 && hour < 18) {
			message = 'Good Afternoon ';
		} else {
			message = 'Good Evening ';      
		}
		message += UserInfo.getFirstName() + '!';
		return new List<String>{message, UserInfo.getFirstName()};
	}

	global virtual List<String> getActionSettingsAvailableFields(List<String> genericJSONParameterList) {
		return genericJSONParameterList;
	}

	global virtual List<String> getLiveAgentSettings(List<String> genericJSONParameterList) {
		return genericJSONParameterList;
	}

	global virtual List<String> specificArticleDetailPreparation(List<String> genericJSONParameterList) {
		genericJSONParameterList.add('true');
		return genericJSONParameterList;
	}

	global virtual List<String> getDynamicComponentNamesMap(List<String> genericJSONParameterList) {
		Map<String, String> dynamicComponentNamesMap = new Map<String, String> {
			'ActionsSettings' => 'cncrg:ActionsSettings',
			'CaseComment' => 'cncrg:CaseComment',
			'CallArticleAction' => 'cncrg:CallArticleAction'
		};
		return new List<String> { JSON.serialize(dynamicComponentNamesMap) };
	}

	global virtual List<String> getArticleActionSetting(List<String> genericJSONParameterList) {
		SearchResultsWrapper res = (SearchResultsWrapper) JSON.deserialize(genericJSONParameterList[0], SearchResultsWrapper.class);
		Set<String> allCategories = (Set<String>) JSON.deserialize(genericJSONParameterList[1], Set<String>.class);
		List<cncrg__Action_Setting__c> allSettings = SearchQueryInputController.getArticleActionsSettings(res, allCategories);
		cncrg__Action_Setting__c setting = (cncrg__Action_Setting__c) SearchQueryInputController.getPrioritizedSettingRecord(allSettings, allCategories, res);
		return new List<String> { JSON.serialize(setting) };
	}

	/*********************************************************************************************/
	/*                                      Case methods                                         */
	/*********************************************************************************************/
	//Start of the outdated method
	global virtual Id saveCaseRecord(String caseRecordStr, String requestArrayStr) {
		return null;
	}
	//End deprecated method

	global virtual List<String> modifyFieldSetWrapper(List<String> genericJSONParameterList) {
		return genericJSONParameterList;
	}

	global virtual Boolean hasUserPermissionToCreateCase() {
		return Case.sObjectType.getDescribe().isCreateable();
	}

	global virtual List<String> saveCaseRecord(List<String> genericJSONParameterList) {
		Case caseRecord = (Case) JSON.deserialize(genericJSONParameterList[0], Case.class);
		Database.DMLOptions dmo = new Database.DMLOptions();
		dmo.AssignmentRuleHeader.useDefaultRule = true;
		caseRecord.setOptions(dmo);
		ESAPI.securityUtils().validatedInsert(new List<Case> { caseRecord });
		return new List<String> { (String) caseRecord.Id };
	}

	global virtual List<String> saveCustomCaseRecord(List<String> genericJSONParameterList) {
		Map<String, String> settings = (Map<String, String>) JSON.deserialize( getCustomCaseSettings(null)[0], Map<String, String>.class);
		String customObjName = settings.get('ObjectName');
		SObject caseRecord = (SObject) JSON.deserialize(genericJSONParameterList[0], Type.forName(customObjName));
		ESAPI.securityUtils().validatedInsert(new List<SObject> { caseRecord });
		return new List<String> { (String) caseRecord.Id };
	}

	global virtual List<String> getContactId(List<String> genericJSONParameterList) {
		List<Contact> contactRecord = [
			SELECT Id
			FROM Contact
			WHERE cncrg__UserId__c = :UserInfo.getUserId() OR cncrg__UserId__c = :UserInfo.getUserId().subString(0, 15)
			LIMIT 10
		];
		if (contactRecord.size() == 0 && genericJSONParameterList[0] == 'true') {
			User currentUser = [
				SELECT Id,
					LastName,
					FirstName,
					Email,
					MobilePhone,
					Phone
				FROM User
				WHERE Id = :UserInfo.getUserId()
			];
			Contact currentContact = new Contact(
				LastName = currentUser.LastName,
				FirstName = currentUser.FirstName,
				Email = currentUser.Email,
				cncrg__UserId__c = currentUser.Id,
				MobilePhone = currentUser.MobilePhone,
				Phone = currentUser.Phone
			);
			ESAPI.securityUtils().validatedInsert(new List<Contact> { currentContact });
			return new List<String> { (String) currentContact.Id };
		}
		return new List<String> { contactRecord.size() == 0 ? '' : (String) contactRecord[0].Id };
	}

	global virtual List<String> getPackCasesList(List<String> genericJSONParameterList) {
		List<Case> allCases = new List<Case>();
		Set<String> fieldsToCheck = new Set<String> { 'Id', 'ContactId' };

		if (ESAPI.securityUtils().isAuthorizedToView('Case', new List<String> (fieldsToCheck))) {
			fieldsToCheck.addAll( (List<String>) JSON.deserialize(genericJSONParameterList[0], List<String>.class) );
			String query = 'SELECT ' + String.join((Iterable<String>)fieldsToCheck, ',');
			query += ' FROM Case';

			String contactId = genericJSONParameterList[1];
			if (String.isNotBlank(contactId)) {
				query += ' WHERE ContactId = \'' + contactId + '\'';
			}

			query += ' ORDER BY CreatedDate DESC';
			query += ' LIMIT ' + (genericJSONParameterList.size() == 3 ? genericJSONParameterList.get(2) : '5000');

			try {
				if (String.isNotBlank(contactId)) {
					allCases = Database.query(query);
				}
			}
			catch(Exception e) { }
		}
		return new List<String> { JSON.serialize(allCases) };
	}

	global virtual List<String> getPackCustomCasesList(List<String> genericJSONParameterList) {
		List<SObject> allCases = new List<SObject>();

		Set<String> fieldsToCheck = new Set<String>();
		fieldsToCheck.addAll( (List<String>) JSON.deserialize(genericJSONParameterList[0], List<String>.class) );
		Map<String, String> settings = (Map<String, String>) JSON.deserialize(getCustomCaseSettings(null)[0], Map<String, String>.class);

		String customCaseObjectName = settings.get('ObjectName');
		String query = 'SELECT ' + String.join((Iterable<String>)fieldsToCheck, ',');
		query += ' FROM ' + customCaseObjectName;
		query += ' ORDER BY CreatedDate DESC ';
		query += ' LIMIT ' + (genericJSONParameterList.size() == 3 ? genericJSONParameterList.get(2) : '5000');

		if (ESAPI.securityUtils().isAuthorizedToView(customCaseObjectName, new List<String> (fieldsToCheck))) {
			allCases = Database.query(query);
		}
		return new List<String> { JSON.serialize(allCases) };
	}

	global virtual List<String> getCaseUpdatesCount(List<String> genericJSONParameterList) {
		Integer countUpdates = 0;
		if (!genericJSONParameterList.isEmpty() 
			&& String.isNotBlank(genericJSONParameterList[0]) 
			&& ESAPI.securityUtils().isAuthorizedToView('Case', new List<String> { WorkWithCases.UNREAD_UPDATE_ON_CASE })){
			countUpdates = [
				SELECT COUNT() FROM Case
				WHERE cncrg__Unread_Update_for_Contact__c = true AND ContactId = :genericJSONParameterList[0]
			];
		}
		return new List<String> { String.valueOf(countUpdates) };
	}

	global virtual List<String> updateCasesFlag(List<String> genericJSONParameterList) {
		List<Case> caseList = new List<Case> ();
		try {
			caseList = [
				SELECT Id, ContactId
				FROM Case
				WHERE Id IN :genericJSONParameterList AND
					ContactId != NULL AND cncrg__Unread_Update_for_Contact__c = false
				LIMIT 50000
			];
			List<String> contactInfo = getContactId(new List<String> { '' });
			List<Case> updateCaseList = new List<Case> ();
			for (Case item : caseList) {
				if (String.isBlank(contactInfo[0]) || contactInfo[0] != item.ContactId) {
					item.cncrg__Unread_Update_for_Contact__c = true;
				}
				updateCaseList.add(item);
			}
			if (updateCaseList.size() > 0) {
				ESAPI.securityUtils().validatedUpdate(updateCaseList);
			}
		}
		catch(Exception e) { }
		return new List<String>();
	}

	global virtual List<String> getAllFeedItems(List<String> genericJSONParameterList) {
		return new List<String> { JSON.serialize(ChatterDataAccessUtils.getAllFeedItems(genericJSONParameterList[0])) };
	}

	global virtual List<String> getCustomCaseSettings(List<String> genericJSONParameterList) {
		// Sample configuration 
		//Map<String, String> settings = new Map<String, String> {
		//	'ObjectName' => 'cncrg__AR_CASE__c',
		//	'ButtonLabel' => 'Make case private',
		//	'ButtonTitle' => 'This creates a private Associate Relations case.',
		//	'CreateFieldSetName' => 'cncrg__Create',
		//	'ListFieldSetName' => 'cncrg__CaseListViewFieldSet',
		//	'DetailFieldSetName' => 'cncrg__TicketsCaseFieldSet',
		//	'Status Indicator' => '',
		//	'Status' => 'cncrg__Status__c',
		//	'Subject' => 'Name',
		//	'Description' => 'cncrg__Desc__c',
		//	'SortOrder' => JSON.serialize(new List<String>{'IsClosed', 'CreatedDate'})
		//};
		//return new List<String> { JSON.serialize((Test.isRunningTest() ? customCaseSettings : settings)) };
		return new List<String> { JSON.serialize(customCaseSettings) };
	}

	/*********************************************************************************************/
	/*                                      Multi Search methods                                 */
	/*********************************************************************************************/
	global virtual List<String> getMultiSearchRequestBody(List<String> genericJSONParameterList) {
		List<HerokuWebService.SourceWrapper> sourceSystems = new List<HerokuWebService.SourceWrapper>();
		HerokuWebService.SourceWrapper salesforceSource = (HerokuWebService.SourceWrapper) JSON.deserialize(
			genericJSONParameterList.get(0),
			HerokuWebService.SourceWrapper.class
		);
		sourceSystems.add(salesforceSource);

		//////////////////////////////////////////////////////////////////////////////////
		//1. Sharepoint part 
		//String sharepointSourceId = 'cadalysink.sharepoint.com';
		//Related_Credential__c sharepointCredential = getCredentialByStorageId(sharepointSourceId);
		//	SourceWrapper sharepointSource = new SourceWrapper();
		//	sharepointSource.dataSourceType = StorageType.sharepoint.name();
		//	sharepointSource.sourceId = sharepointSourceId;
		//	sharepointSource.user = sharepointCredential.Username__c;
		//sourceSystems.add(sharepointSource);

		//private static Related_Credential__c getCredentialByStorageId(String orgId) {
		//	List<Related_Credential__c> relatedList = [
		//		SELECT Id, Username__c, Storage__c
		//		FROM Related_Credential__c
		//		WHERE Storage__c =:orgId AND User__c = :UserInfo.getUserId()
		//	];
		//	if (relatedList.isEmpty()) {
		//		return null;
		//	}
		//	return relatedList[0];
		//}

		//2. AWS Source
		//String awsSourceId = 'cadalys.aws';
		//	SourceWrapper awsSource = new SourceWrapper();
		//	awsSource.dataSourceType = StorageType.aws.name();
		//	awsSource.sourceId = awsSourceId;
		//sourceSystems.add(awsSource);
		//////////////////////////////////////////////////////////////////////////////////

		List<String> configuration = new List<String>();
		configuration.add(''); // APP endpoint goes here, i.e. configuration.add('http://cadcmos.herokuapp.com/query');
		configuration.add( JSON.serialize(sourceSystems) );
		return configuration;
	}
}