/**
* @description

	The main class for working with Lightning and Classic Knowledges

* @methods
	getAllFavoriteArticles -				method for getting Favorite Article List for current user
	getFavoriteArticleDetailInformation -	method for getting full article body
	deleteFavorite -						remove an Article from Favorite for user
	saveRecord -							insert encrypted Search Staging records
	encryptedSearchStaging/
	decryptedSearchStaging	-				encryption and decryption records by AES256
*/
public with sharing class WorkWithArticles {

	@TestVisible
	private static Boolean isTest = false;

	@AuraEnabled
	public static String getAllFavoriteArticles(String prefix) {
		List<FavoriteArticlesWrapper> favoriteArticlesWrapperList = new List<FavoriteArticlesWrapper>();
		List<Favorite_article__c> listOfFavorite = new List<Favorite_article__c>();
		if (ESAPI.securityUtils().isAuthorizedToView('cncrg__Favorite_article__c', new List<String> { 'Id', 'cncrg__ArticleId__c', 'OwnerId' })) {
			listOfFavorite = [
				SELECT Id, ArticleId__c, OwnerId, CreatedDate
				FROM Favorite_article__c
				WHERE OwnerId = :UserInfo.getUserId()
			];
		}

		WorkWithArticles.EncryptionMember CRYPTO_SERVICE = new WorkWithArticles.EncryptionMember();
		Map<String, Datetime> favoriteOrderMap = new Map<String, Datetime> ();
		List<String> listOfFavoriteArticlesIds = new List<String>();

		for (Favorite_article__c favorite : listOfFavorite) {
			String articleId = null;
			try {
				articleId = CRYPTO_SERVICE.decrypteData(favorite.cncrg__ArticleId__c);
			} catch (Exception e) {
				System.debug('article Id decrypt: ' + e.getMessage() + '\n' + e.getStackTraceString());
				continue;
			}

			favoriteOrderMap.put(articleId, favorite.CreatedDate);
			listOfFavoriteArticlesIds.add(articleId);
		}

		List<Object> articlesList = new List<Object>();
		articlesList = Utils.initEngine().getFavoritesbyId(listOfFavoriteArticlesIds, prefix);
		articlesList.addAll(ObjectSearchEngine.getCustomFavoritesArticles(listOfFavoriteArticlesIds));
		favoriteArticlesWrapperList = (List<FavoriteArticlesWrapper>) articlesList;

		for (FavoriteArticlesWrapper item : favoriteArticlesWrapperList) {
			try {
				item.createdDate = favoriteOrderMap.get((String) item.article.get('KnowledgeArticleId'));
			} catch (Exception e) {
				item.createdDate = favoriteOrderMap.get((String) item.article.get('Id'));
			}
		}
		favoriteArticlesWrapperList.sort();

		if (favoriteArticlesWrapperList.size() > 0 && favoriteArticlesWrapperList[0].body == 'blankBody') {
			favoriteArticlesWrapperList[0] = getFavoriteArticleDetailInformation(JSON.serialize(favoriteArticlesWrapperList[0]), prefix);
		}
		return JSON.serialize(favoriteArticlesWrapperList);
	}

	@AuraEnabled
	public static FavoriteArticlesWrapper getFavoriteArticleDetailInformation(String articleRecordStr, String prefix) {
		FavoriteArticlesWrapper favorite = (FavoriteArticlesWrapper) JSON.deserialize(articleRecordStr, FavoriteArticlesWrapper.class);
		return Utils.initEngine().getFavoriteArticleDetail(favorite, prefix);
	}

	@AuraEnabled
	public static List<Object> getRelatedArticleData(String href, String prefix) {
		List<Object> initialDataList = RelatedArticleDetailsController.getInitialData(href, prefix);
		initialDataList.add(SearchQueryInputController.getLiveAgentPage()[0]);
		return initialDataList;
	}

	@AuraEnabled
	public static void deleteFavorite(String recordId) {
		List<Favorite_article__c> listOfFavorite = new List<Favorite_article__c> ();
		if (ESAPI.securityUtils().isAuthorizedToView('cncrg__Favorite_article__c', new List<String>{'Id','cncrg__ArticleId__c','OwnerId'})) {
			listOfFavorite = [
				SELECT Id, ArticleId__c
				FROM Favorite_article__c
				WHERE OwnerId = :UserInfo.getUserId()
				LIMIT 50000
			];
		}

		WorkWithArticles.EncryptionMember CRYPTO_SERVICE = new WorkWithArticles.EncryptionMember();
		List<cncrg__Favorite_article__c> listOfFavoriteDelete = new List<cncrg__Favorite_article__c> ();
		for (cncrg__Favorite_article__c item : listOfFavorite) {
			if (CRYPTO_SERVICE.decrypteData(item.cncrg__ArticleId__c) == recordId) {
				listOfFavoriteDelete.add(item);
				break;
			}
		}

		if (listOfFavoriteDelete.size() > 0) {
			ESAPI.securityUtils().validatedDelete(listOfFavoriteDelete);
		}
	}

	private static Blob CRYPTOKEY = Blob.valueOf('3h0Db410e8b$1fa97h0Kbe10e8b#1fa9');
	private static Set<String> ENCRYPTEDFIELSSET = new Set<String> {
		'cncrg__Biederman__c',
		'cncrg__Profigliano__c',
		'cncrg__Schwartz__c',
		'cncrg__Up__c',
		'cncrg__Zoom__c'
	};

	public with sharing class SearchStagingWrapper {
		@AuraEnabled
		public List<String> biederman;
		//@AuraEnabled
		//public String down;
		@AuraEnabled
		public Map<String, Profigliano> profigliano;
		@AuraEnabled
		public String schwartz;
		@AuraEnabled
		public String up;
		@AuraEnabled
		public String zoom;
		@AuraEnabled
		public String recordId;

		public SearchStagingWrapper() {
			biederman = new List<String> ();
			profigliano = new Map<String, Profigliano> ();
		}
		public SearchStagingWrapper(cncrg__Search_Staging__c searchStagingRecord) {
			this();
			this.biederman = (List<String>) JSON.deserialize(searchStagingRecord.cncrg__Biederman__c, List<String>.class);
			try {
				this.profigliano = (Map<String, Profigliano>) JSON.deserialize(searchStagingRecord.cncrg__Profigliano__c, Map<String, Profigliano>.class);
			} catch (Exception e) {
				Map<String, Integer> profiglianoOld = (Map<String, Integer>) JSON.deserialize(searchStagingRecord.cncrg__Profigliano__c, Map<String, Integer>.class);
				for (String key : profiglianoOld.keySet()) {
					this.profigliano.put(key, new Profigliano(null, profiglianoOld.get(key), null));
				}
			}
			this.schwartz = searchStagingRecord.cncrg__Schwartz__c;
			this.up = searchStagingRecord.cncrg__Up__c;
			this.zoom = searchStagingRecord.cncrg__Zoom__c;
			this.recordId = searchStagingRecord.Id;
		}
		public void saveRecord() {
			cncrg__Search_Staging__c searchSettingRecord = new cncrg__Search_Staging__c();
			searchSettingRecord.Id = this.recordId;
			searchSettingRecord.cncrg__Zoom__c = this.zoom;
			searchSettingRecord.cncrg__Up__c = this.up;
			searchSettingRecord.cncrg__Schwartz__c = this.schwartz;
			searchSettingRecord.cncrg__Biederman__c = this.biederman != null ? JSON.serialize(this.biederman) : null;
			searchSettingRecord.cncrg__Profigliano__c = this.profigliano != null ? JSON.serialize(this.profigliano) : null;
			encryptedSearchStaging(searchSettingRecord);
			if (this.recordId == null) {
				insert searchSettingRecord; //removed ESAPI for perfonamce CADCSU-17
			} else {
				update searchSettingRecord; //removed ESAPI for perfonamce CADCSU-17
			}
			this.recordId = searchSettingRecord.Id;
		}
	}
	public class Profigliano {
		public Profigliano(List<String> reasons, Integer score, String comment, String type, String dataCategories, String title, String ownerId) {
			this(reasons, score, comment, type, dataCategories);
			this.title = title;
			this.ownerId = ownerId;
		}
		public Profigliano(List<String> reasons, Integer score, String comment, String type, String dataCategories) {
			this(reasons, score, comment, type);
			this.dataCategories = dataCategories;
		}
		public Profigliano(List<String> reasons, Integer score, String comment, String type) {
			this(reasons, score, comment);
			this.type = type;
		}
		public Profigliano(List<String> reasons, Integer score, String comment) {
			this.reasons = reasons;
			this.score = score;
			this.comment = comment;

		}
		@AuraEnabled
		public List<String> reasons;
		@AuraEnabled
		public Integer score;
		@AuraEnabled
		public String comment;
		@AuraEnabled
		public String type;
		@AuraEnabled
		public String dataCategories;
		@AuraEnabled
		public String title;
		@AuraEnabled
		public String ownerId;
	}
	public static void encryptedSearchStaging(cncrg__Search_Staging__c searchSettingRecord) {
		for (String item : ENCRYPTEDFIELSSET) {
			if (String.isNotBlank((String) searchSettingRecord.get(item))) {
				EncryptionMember member = new EncryptionMember();
				searchSettingRecord.put(item, member.encrypteData((String) searchSettingRecord.get(item)));
			}
		}
	}
	public static void decryptedSearchStaging(cncrg__Search_Staging__c searchSettingRecord) {
		for (String item : ENCRYPTEDFIELSSET) {
			if (String.isNotBlank((String) searchSettingRecord.get(item))) {
				EncryptionMember member = new EncryptionMember();
				searchSettingRecord.put(item, member.decrypteData((String) searchSettingRecord.get(item)));
			}
		}
	}
	public class EncryptionMember {
		private final Integer saltLength = 5;
		private String salt;
		private String timestamp;
		private Integer timestampLength;
		public EncryptionMember() {
			timestamp = String.valueOf(DateTime.now());
			timestampLength = timestamp.length();
			salt = EncodingUtil.convertToHex(Crypto.generateAesKey(128)).substring(0, saltLength);
		}
		public String encrypteData(String value) {
			return EncodingUtil.base64Encode(Crypto.encryptWithManagedIV('AES256', CRYPTOKEY, Blob.valueOf(value + timestamp))) + salt;
		}
		public String decrypteData(String encriptedValue) {
			String decryptedStr = Crypto.decryptWithManagedIV('AES256', CRYPTOKEY, EncodingUtil.base64Decode(encriptedValue.substring(0, encriptedValue.length() - saltLength))).toString();
			return decryptedStr.substring(0, decryptedStr.length() - timestampLength);
		}
	}
	public class Encabulator {
		public Map<String, List<ArticleEncabulator>> encabulator;
	}
	public class ArticleEncabulator {
		public String id;
		public Integer rawscore;
		public Decimal score;
		public Boolean isPromoted;

		public ArticleEncabulator(String id) {
			this.id = id;
			this.rawscore = 10;
			this.score = 10;
		}
		public ArticleEncabulator(String id, Boolean isPromoted) {
			this.id = id;
			this.rawscore = 10;
			this.score = 10;
			this.isPromoted = isPromoted;
		}
	}
}