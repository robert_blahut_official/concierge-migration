public with sharing class ArticleReviewBatch extends AbstractConciergeBatch implements Database.Stateful {

    private String query;
    private String currentLanguage;
    private String currentObject;
    private Map<String, List<String>> remainingReviewLanguageByArticleMap;

    private Integer reviewDay = ArticleReviewUtils.getAutomaticallyReviewDay();

    public ArticleReviewBatch() {
        List<SettingsPanelController.TypesWrapper> twList = SettingsPanelController.getTypesSettings();
        List<String> articleTypeList = new List<String>();
        remainingReviewLanguageByArticleMap = new Map<String, List<String>>();
        for(Integer i = 0; i < twList.size(); i++) {
            if(twList[i].isActive) {
                articleTypeList.add(twList[i].articleType);
            }
        }
        List<String> articleLanguageList = ArticleReviewUtils.getAllArticleLanguages();
        for(Integer i = 0; i < articleTypeList.size(); i++) {
            remainingReviewLanguageByArticleMap.put(articleTypeList[i], articleLanguageList.clone());
        }
        query = preparedQuery();
        if(!ArticleReviewUtils.checkEnableFunctionality()) {
            query += ' LIMIT 0';
        }
    }

    public ArticleReviewBatch(Map<String, List<String>> remainingReviewLanguageByArticleMap, List<Object> params) {
        this.remainingReviewLanguageByArticleMap = remainingReviewLanguageByArticleMap;
        super.setInitParams(params);
        query = preparedQuery();
    }

    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<sObject> scope) {
        Map<String, sObject> articleByKnowledgeId = buildArticleByKnowledgeIdMap(scope);
        List<Article_Review__c> reviewForUpsertList = new List<Article_Review__c>();
        Set<String> knowledgeIdSet = articleByKnowledgeId.keySet();
        reviewForUpsertList.addAll(checkExistArticleReview(knowledgeIdSet, articleByKnowledgeId));
        reviewForUpsertList.addAll(createNewArticleReviewRecords(knowledgeIdSet, articleByKnowledgeId));
        if(!reviewForUpsertList.isEmpty()) {
            ESAPI.securityUtils().validatedUpsert(reviewForUpsertList);
        }
    }

    private Map<String, sObject> buildArticleByKnowledgeIdMap(List<sObject> scope) {
        Map<String, sObject> result = new Map<String, sObject>();
        sObject currentArticle;
        for(Integer i = 0; i < scope.size(); i ++) {
            currentArticle = scope[i];
            result.put((String) currentArticle.get('KnowledgeArticleId'), currentArticle);
        }
        return result;
    }

    private List<Article_Review__c> createNewArticleReviewRecords(Set<String> knowledgeIdSet, Map<String, sObject> articleByKnowledgeId) {
        List<Article_Review__c> newArticleReviewList = new List<Article_Review__c>();
        Date nextPrevDate = Date.today().addDays(reviewDay);
        for(String key : knowledgeIdSet) {
            newArticleReviewList.add(ArticleReviewUtils.createNewArticleReviewRecord(articleByKnowledgeId.get(key), currentLanguage, nextPrevDate));
        }
        return newArticleReviewList;
    }

    private List<Article_Review__c> checkExistArticleReview(Set<String> knowledgeIdSet, Map<String, sObject> objectMap) {
        Date currentDay = Date.today();
        Map<String, Article_Review__c> result = new Map<String, Article_Review__c>();
        List<Article_Review__c> existArticleList = ArticleReviewUtils.findArticleReviewByKnowledgeIds(knowledgeIdSet, currentObject, currentLanguage);
        Article_Review__c currentReview;
        sObject currentObject;
        for(Integer i = 0; i < existArticleList.size(); i++) {
            currentReview = existArticleList[i];
            currentObject = objectMap.get(currentReview.Parent_Article__c);
            knowledgeIdSet.remove(currentReview.Parent_Article__c);
            if(!currentReview.Never_Review__c) {
                if(currentReview.Next_Review_Date__c <= currentDay
                        || (Datetime)currentObject.get('LastModifiedDate') > currentReview.LastModifiedDate) {
                    currentReview.Next_Review_Date__c = currentDay.addDays(reviewDay);
                    currentReview.Last_Reviewed_Date__c = currentDay;
                    result.put(currentReview.Id, currentReview);
                }
                if(currentReview.Expiration_Date__c < Datetime.now()) {
                    currentReview.Expiration_Date__c = null;
                    result.put(currentReview.Id, currentReview);
                    ArchiveArticlesBatch.isProcessed = true;
                }
                if((String)currentObject.get('Title') != currentReview.Full_Name__c || (String)currentObject.get('UrlName') != currentReview.UrlName__c) {
                    currentReview.Full_Name__c = (String)currentObject.get('Title');
                    currentReview.Name = ((String)currentObject.get('Title')).abbreviate(ArticleReviewUtils.MAX_NAME_LENGTH);
                    currentReview.UrlName__c = (String)currentObject.get('UrlName');
                    result.put(currentReview.Id, currentReview);
                }
            }
        }
        return result.values();
    }

    public override void finish(Database.BatchableContext BC) {
        if(ArticleReviewUtils.checkEnableFunctionality()) {
            if(!remainingReviewLanguageByArticleMap.isEmpty() && !(remainingReviewLanguageByArticleMap.values().size() == 1 && remainingReviewLanguageByArticleMap.values()[0].isEmpty())) {
                Database.executeBatch(new ArticleReviewBatch(remainingReviewLanguageByArticleMap, super.getInitParams()), super.batchSize);
            } else {
                removeOldArticleReview();
                super.finish(BC);
            }
            ArchiveArticlesBatch.isProcessed = false;
        } else {
            super.finish(BC);
        }
    }

    private void removeOldArticleReview() {
        Set<String> articleIds = new Map<String, sObject>(
            [
                    SELECT  Id
                    FROM    KnowledgeArticle
                    WHERE   Id NOT IN (
                            SELECT  KnowledgeArticleId
                            FROM    KnowledgeArticleVersion
                            WHERE   PublishStatus = 'Online'
                    )
            ]
        ).keySet();
        List<Article_Review__c> articleReviewList = [SELECT Id FROM Article_Review__c WHERE Parent_Article__c IN :articleIds];
        if(!articleReviewList.isEmpty())
            ESAPI.securityUtils().validatedDelete(articleReviewList);
    }

    private String preparedQuery() {
        String currentArticle = getCurrentArticleType();
        if(currentArticle != null) {
            currentLanguage = remainingReviewLanguageByArticleMap.get(currentArticle)[0];
            currentObject = currentArticle;
            remainingReviewLanguageByArticleMap.get(currentArticle).remove(0);
            return ArticleReviewUtils.preparedQuery(currentArticle, currentLanguage);
        }
        return null;
    }

    private String getCurrentArticleType() {
        String currentArticle;
        List<String> currentLanguageList;
        for(String key : remainingReviewLanguageByArticleMap.keySet()) {
            currentArticle = key;
            currentLanguageList = remainingReviewLanguageByArticleMap.get(key);
            break;
        }
        if(currentLanguageList != null) {
            if(currentLanguageList.isEmpty()) {
                remainingReviewLanguageByArticleMap.remove(currentArticle);
                return getCurrentArticleType();
            }
        }
        return currentArticle;
    }
}