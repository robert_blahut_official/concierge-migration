@isTest
private class JSONOptionsWizardTest {

	@isTest static void createIndexJsonTest() {
		Test.startTest();
            String result = JSONOptionsWizard.createIndexJson('tableName', 'indexType');
            
            System.assert(result == '{"indexName": "tableName","indexType": "indexType"}');
        Test.stopTest();

        System.assert(result != null);
	}

	@isTest static void deleteIndexJsonTest() {
		Test.startTest();
            String result = JSONOptionsWizard.deleteIndexJson();
        Test.stopTest();

        System.assert(result != null);
	}

    @isTest static void createTableJsonTest() {
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map <String, Schema.SObjectField> fieldMap = schemaMap.get('Account').getDescribe().fields.getMap();

        String objectName = 'Account';
        List<String> needFields = new List<String>{'Name'};

        Test.startTest();
            String result = JSONOptionsWizard.createTableJson(fieldMap, needFields, objectName);
        Test.stopTest();

        System.assert(result != null);
    }

    @isTest static void populateTableJsonTest() {
        List<SObject> sobjs = [Select Id, Name from Account];

        String objectName = 'Account';
        List<String> needFields = new List<String>{'Name'};

        Test.startTest();
            String result = JSONOptionsWizard.populateTableJson(sobjs, needFields, 'tableName');
        Test.stopTest();

        System.assert(result != null);
    }

     @isTest static void updateIndexTableJsonTest() {
        List<SObject> sobjs = [Select Id, Name from Account];

        String objectName = 'Account';
        List<String> needFields = new List<String>{'Name'};

        Test.startTest();
            String result = JSONOptionsWizard.updateIndexTableJson( needFields, 'tableName');
        Test.stopTest();

        System.assert(result != null);
    }

   @isTest static void searchJsonTest() {

        init();

        String needResult =

                '{"indexName": "testTable",' +
                '"configuration": "english",' +
                '"maxWords": 0,' +
                '"minWords": 0,' +
                '"queriesFrom": "2016-09-09",' +
                '"showAll": true,' +
                    '"table": {' +
                        '"name": "documents_' + PostgresAdapter.KNOWLEDGE_TYPE_NAME + '",' +
                        '"columns": [{' +
                        '"name": "Title",' +
                        '"selectable": true' +
                        '}' +
                    ']},' +
                '"query": "query"}';

        Test.startTest();
            String result = JSONOptionsWizard.searchJson('query');
        Test.stopTest();

        System.assert(result != null);
    }

    @isTest static void checkIndexJsonTest() {
        Test.startTest();
            String result = JSONOptionsWizard.checkIndexJson();
        Test.stopTest();

        System.assert(result != null);
    }

    private static void init() {

        cncrg__Web_Service_Settings__c customSetting = new cncrg__Web_Service_Settings__c(Name = 'HerokuWebService');
        customSetting.Password__c = 'testPassword';
        customSetting.URL__c = 'test.heroku.com';
        customSetting.User__c = 'testUser';
        customSetting.Table_Name__c = 'testTable';

        insert customSetting;

        List<Account> accs = new List<Account>();
        accs.add(new Account(Name = 'Test1'));
        accs.add(new Account(Name = 'Test2'));
        insert accs;

		cncrg__Concierge_Settings__c cs = new cncrg__Concierge_Settings__c(
			Name = 'Org Namespace',
			cncrg__Value__c = 'cncrg'
		);
		ESAPI.securityUtils().validatedInsert(new List<cncrg__Concierge_Settings__c>{cs});

		cs = new cncrg__Concierge_Settings__c(
			Name = 'Type to display',
			cncrg__Value__c = 'cncrg__Knowledge__kav'
		);
		ESAPI.securityUtils().validatedInsert(new List<cncrg__Concierge_Settings__c>{cs});

		cs = new cncrg__Concierge_Settings__c(
			Name = 'Field to display',
			cncrg__Value__c = 'Summary'
		);
		ESAPI.securityUtils().validatedInsert(new List<cncrg__Concierge_Settings__c>{cs});

    }
}