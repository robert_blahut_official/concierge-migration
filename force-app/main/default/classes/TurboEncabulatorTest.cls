@isTest
private class TurboEncabulatorTest {
	@testSetup
	static void initData() {
		insert new Concierge_Settings__c(Name = 'Article Score Half Life (Days)', Value__c = '5');
		List<Concierge_Field_Mappings__c> fieldMapping = new List<Concierge_Field_Mappings__c>();
		fieldMapping.add(new Concierge_Field_Mappings__c(Name = '0', Value__c = 'LanguageLocaleKey:cncrg__Language__c'));
		fieldMapping.add(new Concierge_Field_Mappings__c(Name = '1', Value__c = 'Profile:cncrg__Profile__c'));
		insert fieldMapping;

		List<SchedulerSettings__c> schedSettingList = new List<SchedulerSettings__c> {
			new SchedulerSettings__c(
				Name = TurboEncabulatorScheduler.TURBO_BATCH_NAME,
				Class_Name__c = TurboEncabulatorScheduler.TURBO_BATCH_NAME,
				CRON__c = TurboEncabulatorScheduler.TURBO_CRON,
				Batch_Size__c = TurboEncabulatorScheduler.TURBO_BATCH_SIZE,
				Description__c = TurboEncabulatorScheduler.TURBO_DESCRIPTION,
				Active__c = true
			),
			new SchedulerSettings__c(
				Name = TurboEncabulatorScheduler.REVIEW_BATCH_NAME,
				Class_Name__c = TurboEncabulatorScheduler.REVIEW_BATCH_NAME,
				CRON__c = TurboEncabulatorScheduler.REVIEW_CRON,
				Batch_Size__c = TurboEncabulatorScheduler.REVIEW_BATH_SIZE,
				Description__c = TurboEncabulatorScheduler.REVIEW_DESCRIPTION,
				Active__c = false
			)
		};
		Database.insert(schedSettingList, false);
		System.assert(!SchedulerSettings__c.getAll().isEmpty());
	}

	@IsTest static void testWorkAfterOneHours() {
		insert new Search_Staging__c(cncrg__Biederman__c = 'CZwjyKl69i2WyNAyFwdxlyJypk1LHx6ymLzAOch22mK8LU+12z9PA+fZDbpFviF+Gw8vch7ShODLQfIFoWuvvdeCkZiIA7xS+Ahr5/EOi/KMRBSVXIjo6FaoyjdqFgqnPO4AuGUdGnodHfxhTMtTXp2z0CLsjZ9yJT6IZPQFnesfQnKCLGRgVz/g2QrjiT+ocdW6YHsBtQxMggJVQL4CqPBha6MhgBPDzZ35qyBtngueCquy/Qg7o2mlWDOIB0UkRtCesgV3ACNY8fWCSL3plH/4SAXyoSWayeVrAZdtA8HlJX/HMkDPjjXlMqtspEuSkKS8BsbiqTQk0dUEFPu+PyuZXf0zAgNLxGjQEAV1Ad2M0LtKeHWgm1MzfCu2pA/CmbvkbODcOwygGLa7+y057zVYQiu4lmAWRGCR/n621Yk=69c6e', cncrg__Profigliano__c = 'PpJBVSKeE5oQKTcmQNhDkssujWyIZmXa9EqReYZhW9MoTUDc2ubCK9CUyrOWFbsw5NgNUCozXASTwvAcrtm+1A==f7e42', cncrg__Schwartz__c = 'ENOvGKiC+tbRb1Yo/qJDIY4mjQoF5zgrDMXeyRJIUtDF0AmFx1QQ4Q7gDBMhUtrBvljzdJ3+PoBo3ykrBPTUAA==991f9', cncrg__Up__c = 'i1kBQRjMApptQC7Lik7f6n0D/ldRjdKwRyDGAn87RMXwEwv+ykOCq7zwAjFnuHMx04bdf', cncrg__Zoom__c = '25azen3G/jLNd3UeTe73o2m9nn3wKBfWFBlcrCeyk3YUqS7KEWGfFDF77Cv9SfTwGQIdTaGYDutBTPYVgTJv0w==86f2f');
		insert new Search_Staging__c(cncrg__Biederman__c = '+jTBdgPqW92udh5FpR1LgSJUsb7iZsdS58gNCtsr0ff9ho36K7X0ESXQgNVrOl4CRAqCuaJhmb40P5mXu5TOJxjvuKKZtcyFPb0JNrrV9OGfYr8Md1eXWtdnjyXGjzVNmfGQM8oyTMMXZhHgKopVuOQfAVu9MYMsDLf9FBRt0rS4S07YRmyUyy0L0z7O7EpVTTmPwsr+pcS22JQW+JaFe0OQzpB1evAMW/Fxe7Wd7PS9cdQqcd6Wule/yhOZppTrWsPqxgY+rhhEARJIsB7ED8a2jr3eFiZf/Wqu5gncrH3+avhnay9mu7nLNb/FQfXqnnyxKx/tA5E8UBjPXpcS4gwpzjvXI+BP/lFdvzr+CKnOZPHmNGjDoV/SvYiz6foravT14Zh8Iqk8c7pHUFsJNoyOuoWqxZ9zbHsmTsyhPGI=82071', cncrg__Profigliano__c = 'e+FY6puHihZ6J9CHb32DyVrZ4PNrOBbgRSkAit804l1rGhTRi5JwC+Jhq/UxdFN1zXG8co0k4daF1FWbbeSwVw==ef4a2', cncrg__Schwartz__c = 's/JfFa2q/wyQIc13iwTdxdhNt486ceIiOgfv0Q9xJ+MvWfBz4QiyZpScdM80LJ0XXwdMGTTq8Ia/xzGheEx11Q==9fb86', cncrg__Up__c = 'qIrKjL/i5+NKvsbYwuiRXaU0cSKFr0fN+boufkB+4dvc1/5ukO7PaSFcALxZ7GBB741e0', cncrg__Zoom__c = 'UaW8o800OBRTCf/vEubFmEZ37c3LFUY3DKg2oNMB8+wYXuBD93T40XgRUthfEMFU/FgOyraVCB7ldx+71u42UQ==ba0a8');
		Test.startTest();
		TurboEncabulator batchable = new TurboEncabulator();
		batchable.setInitParams(new List<Object> {null, Datetime.now(), 'TurboEncabulator', 12});
		Database.executeBatch(batchable, 50);
		Test.stopTest();

		Integer statingNumber = [SELECT COUNT() FROM Search_Staging__c WHERE Down__c = null];
		System.assertEquals(2, statingNumber, 'ERROR: Please make sure that TurboEncabulator processes records older than hour');
	}

	@IsTest static void searchStagingShedulerTest() {
		String cronExpr = '0 0 23 * * ?';
		Test.startTest();
		String jobId = TurboEncabulatorScheduler.startScheduler(cronExpr, 'Test TurboEncabulator');
		CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
		Test.stopTest();
		System.assertEquals(cronExpr, ct.CronExpression);
	}

	@IsTest static void searchStagingParseTest() {
		init();
		Integer searchTermBeforeCounter = [SELECT Id FROM cncrg__Search_Term__c].size();
		Profile prof = [SELECT Id, Name FROM Profile WHERE Id =: UserInfo.getProfileId()];

		Test.startTest();
		TurboEncabulator batchable = new TurboEncabulator();
		batchable.setInitParams(new List<Object> {null, Datetime.now(), 'TurboEncabulator', 12});
		Database.executeBatch(batchable, 50);
		Test.stopTest();

		System.assert([SELECT Id FROM cncrg__Search_Term__c].size() > searchTermBeforeCounter);
		List<Search_History__c> history = [SELECT Id, Language__c, Profile__c FROM Search_History__c];
		System.assertEquals(2, history.size(), '2 History records should be created');
		for (Search_History__c hist: history) {
			System.assertEquals(hist.Profile__c, prof.Name);
			System.assertEquals(hist.Language__c, 'en_US');
		}
	}

	@IsTest static void searchStagingWrongTest() {
		init();

		Test.startTest();

		cncrg__Search_Staging__c searchStagingRecord = [SELECT Id, cncrg__Profigliano__c FROM cncrg__Search_Staging__c LIMIT 1];
		searchStagingRecord.Profigliano__c += 'wrong data';
		update searchStagingRecord;

		TurboEncabulator batchable = new TurboEncabulator();
		batchable.setInitParams(new List<Object> {null, Datetime.now(), 'TurboEncabulator', 12});
		Database.executeBatch(batchable, 50);
		Test.stopTest();
		System.assert([SELECT Id, cncrg__Down__c FROM cncrg__Search_Staging__c][0].cncrg__Down__c != NULL);
	}

	private static void init() {
		List<sObject> searchStagingTermList = new List<sObject> {
			new Search_Staging__c(
				Biederman__c = 'CZwjyKl69i2WyNAyFwdxlyJypk1LHx6ymLzAOch22mK8LU+12z9PA+fZDbpFviF+Gw8vch7ShODLQfIFoWuvvdeCkZiIA7xS+Ahr5/EOi/KMRBSVXIjo6FaoyjdqFgqnPO4AuGUdGnodHfxhTMtTXp2z0CLsjZ9yJT6IZPQFnesfQnKCLGRgVz/g2QrjiT+ocdW6YHsBtQxMggJVQL4CqPBha6MhgBPDzZ35qyBtngueCquy/Qg7o2mlWDOIB0UkRtCesgV3ACNY8fWCSL3plH/4SAXyoSWayeVrAZdtA8HlJX/HMkDPjjXlMqtspEuSkKS8BsbiqTQk0dUEFPu+PyuZXf0zAgNLxGjQEAV1Ad2M0LtKeHWgm1MzfCu2pA/CmbvkbODcOwygGLa7+y057zVYQiu4lmAWRGCR/n621Yk=69c6e',
				Profigliano__c = 'PpJBVSKeE5oQKTcmQNhDkssujWyIZmXa9EqReYZhW9MoTUDc2ubCK9CUyrOWFbsw5NgNUCozXASTwvAcrtm+1A==f7e42',
				Schwartz__c = 'ENOvGKiC+tbRb1Yo/qJDIY4mjQoF5zgrDMXeyRJIUtDF0AmFx1QQ4Q7gDBMhUtrBvljzdJ3+PoBo3ykrBPTUAA==991f9',
				Up__c = 'i1kBQRjMApptQC7Lik7f6n0D/ldRjdKwRyDGAn87RMXwEwv+ykOCq7zwAjFnuHMx04bdf',
				Zoom__c = '25azen3G/jLNd3UeTe73o2m9nn3wKBfWFBlcrCeyk3YUqS7KEWGfFDF77Cv9SfTwGQIdTaGYDutBTPYVgTJv0w==86f2f'
			),
			new Search_Staging__c(
				Biederman__c = '+jTBdgPqW92udh5FpR1LgSJUsb7iZsdS58gNCtsr0ff9ho36K7X0ESXQgNVrOl4CRAqCuaJhmb40P5mXu5TOJxjvuKKZtcyFPb0JNrrV9OGfYr8Md1eXWtdnjyXGjzVNmfGQM8oyTMMXZhHgKopVuOQfAVu9MYMsDLf9FBRt0rS4S07YRmyUyy0L0z7O7EpVTTmPwsr+pcS22JQW+JaFe0OQzpB1evAMW/Fxe7Wd7PS9cdQqcd6Wule/yhOZppTrWsPqxgY+rhhEARJIsB7ED8a2jr3eFiZf/Wqu5gncrH3+avhnay9mu7nLNb/FQfXqnnyxKx/tA5E8UBjPXpcS4gwpzjvXI+BP/lFdvzr+CKnOZPHmNGjDoV/SvYiz6foravT14Zh8Iqk8c7pHUFsJNoyOuoWqxZ9zbHsmTsyhPGI=82071',
				Profigliano__c = 'e+FY6puHihZ6J9CHb32DyVrZ4PNrOBbgRSkAit804l1rGhTRi5JwC+Jhq/UxdFN1zXG8co0k4daF1FWbbeSwVw==ef4a2',
				Schwartz__c = 's/JfFa2q/wyQIc13iwTdxdhNt486ceIiOgfv0Q9xJ+MvWfBz4QiyZpScdM80LJ0XXwdMGTTq8Ia/xzGheEx11Q==9fb86',
				Up__c = 'qIrKjL/i5+NKvsbYwuiRXaU0cSKFr0fN+boufkB+4dvc1/5ukO7PaSFcALxZ7GBB741e0',
				Zoom__c = 'UaW8o800OBRTCf/vEubFmEZ37c3LFUY3DKg2oNMB8+wYXuBD93T40XgRUthfEMFU/FgOyraVCB7ldx+71u42UQ==ba0a8'
			),
			new Search_Staging__c(
				Biederman__c = '+jTBdgPqW92udh5FpR1LgSJUsb7iZsdS58gNCtsr0ff9ho36K7X0ESXQgNVrOl4CRAqCuaJhmb40P5mXu5TOJxjvuKKZtcyFPb0JNrrV9OGfYr8Md1eXWtdnjyXGjzVNmfGQM8oyTMMXZhHgKopVuOQfAVu9MYMsDLf9FBRt0rS4S07YRmyUyy0L0z7O7EpVTTmPwsr+pcS22JQW+JaFe0OQzpB1evAMW/Fxe7Wd7PS9cdQqcd6Wule/yhOZppTrWsPqxgY+rhhEARJIsB7ED8a2jr3eFiZf/Wqu5gncrH3+avhnay9mu7nLNb/FQfXqnnyxKx/tA5E8UBjPXpcS4gwpzjvXI+BP/lFdvzr+CKnOZPHmNGjDoV/SvYiz6foravT14Zh8Iqk8c7pHUFsJNoyOuoWqxZ9zbHsmTsyhPGI=82071',
				Profigliano__c = 'e+FY6puHihZ6J9CHb32DyVrZ4PNrOBbgRSkAit804l1rGhTRi5JwC+Jhq/UxdFN1zXG8co0k4daF1FWbbeSwVw==ef4a2',
				Schwartz__c = 's/JfFa2q/wyQIc13iwTdxdhNt486ceIiOgfv0Q9xJ+MvWfBz4QiyZpScdM80LJ0XXwdMGTTq8Ia/xzGheEx11Q==9fb86',
				Up__c = 'qIrKjL/i5+NKvsbYwuiRXaU0cSKFr0fN+boufkB+4dvc1/5ukO7PaSFcALxZ7GBB741e0',
				Zoom__c = 'UaW8o800OBRTCf/vEubFmEZ37c3LFUY3DKg2oNMB8+wYXuBD93T40XgRUthfEMFU/FgOyraVCB7ldx+71u42UQ==ba0a8',
				Down__c = 'Error message'
			),

			new Search_Term__c(
				Name = 'Bitdefender 2018 Upgrade',
				Strange__c = ''
			),
			new Search_Term__c(
				Name = 'Per Diems for International Travel',
				Strange__c = ''
			),
			new Search_Term__c(
				Name = 'Onboarding Checklist',
				Strange__c = ''
			),
			new Search_Term__c(
				Name = 'business unit leadership',
				Charm__c = Date.valueOf('2017-10-19'),
				Strange__c = 'null'
			),
			new Search_Term__c(
				Name = 'use skype for business',
				Charm__c = Date.valueOf('2017-10-19'),
				Strange__c = '{""encabulator"":{""en_US"":[{""score"":20,""rawscore"":20,""id"":""kA046000000QmkxCAC""}]}}'
			),
			new Search_Term__c(
				Name = 'iPhone X Upgrade',
				Strange__c = ''
			),
			new Search_Term__c(
				Name = '2018 Corporate Holidays',
				Strange__c = ''
			),
			new Search_Term__c(
				Name = 'Do We Get Frequent Flier Miles for Business Travel',
				Strange__c = ''
			),
			new Search_Term__c(
				Name = '401(k) Annual Cap',
				Strange__c = ''
			)
		};

		insert searchStagingTermList;
		for (sObject item : searchStagingTermList) {
			Test.setCreatedDate((Id) item.get('Id'), Date.today().addDays(-60));
		}
	}

	testMethod static void checkSomeIssues() {
		TurboEncabulatorScheduler.sendNotification();
		Test.startTest();
		TurboEncabulatorScheduler.recalculate();
		Test.stopTest();
		Integer jobs = [SELECT COUNT() FROM AsyncApexJob];
		System.assert(jobs > 0);
	}
}