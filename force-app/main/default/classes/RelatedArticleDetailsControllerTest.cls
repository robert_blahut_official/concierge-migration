@IsTest
public class RelatedArticleDetailsControllerTest {
	@testSetup static void setup() {
		String objectType = WorkWithArticlesTest.getKavObjectName();
		if (objectType != null) {
			Schema.DescribeSObjectResult sObjectDescribe = TestClassUtility.getSobjectDescribe(objectType);
			sObject obj = sObjectDescribe.getSobjectType().newSObject();
			obj.put('Title', 'Unit Test 1 test');
			obj.put('Language', TestClassUtility.getAvailableKnowlegeLanguage(sObjectDescribe));
			obj.put('UrlName', 'test');
			obj.put('Summary', 'test test test test');
			List<sObject> articles = new List<sObject>{
					obj
			};
			ESAPI.securityUtils().validatedInsert(articles);

			List<Id> listOfIds = new List<Id>();
			for (sObject article : articles)
				listOfIds.add((String) article.get('Id'));

			articles = Database.query('SELECT KnowledgeArticleId FROM ' + objectType + ' WHERE Id IN: listOfIds');

			KbManagement.PublishingService.publishArticle((Id) articles[0].get('KnowledgeArticleId'), true);

			List<cncrg__Search_Types_Settings__c> settings = new List<cncrg__Search_Types_Settings__c>{
					new cncrg__Search_Types_Settings__c(
							Name = String.isNotBlank(objectType) ? objectType : 'cncrg__Knowledge__kav',
							cncrg__Article_Type_API_Name__c = String.isNotBlank(objectType) ? objectType : 'cncrg__Knowledge__kav',
							cncrg__Active__c = true,
							cncrg__Fields_To_Display_1__c = 'Summary'
					)
			};
			ESAPI.securityUtils().validatedInsert(settings);
			System.assertEquals(1, cncrg__Search_Types_Settings__c.getAll().size());
		}
	}

	static testMethod void testGetInitialData() {
		TestClassUtility.createSearchEngineConfig();
		String objectType = WorkWithArticlesTest.getKavObjectName();
		Schema.DescribeSObjectResult sObjectDescribe = TestClassUtility.getSobjectDescribe(objectType);
		System.assert(RelatedArticleDetailsController.getInitialData('articles/' + TestClassUtility.getAvailableKnowlegeLanguage(sObjectDescribe) + '/' + (String.isNotBlank(objectType) ? objectType.removeEnd('__kav') : 'cncrg__Knowledge') + '/test', '') != null);
	}
}