/**
* @description

	This class contains logic for search into classic Knowledges

* @methods
	getSuggestionsForQuery -			method for getting suggestions
	getSearchResults -					method for getting search results
	getFavoritesbyId -					method for getting favorites records from Lightning Knowledges by Ids
	rateItem -							method for upvoting or downvoting records (create/delete Vote records)
	toggleFavoriteAPEX -				method for adding or deleting Favorite articles
*/
public with sharing class StandAloneEditionController extends AbstractSearchEngine implements SearchEngineInterface {

	public List<SearchResultsWrapper> getSearchResults(String searchTerm, String prefix) {
		List<SearchResultsWrapper> results = new List<SearchResultsWrapper> ();
		searchTerm = searchTerm.toLowerCase();

		if (searchTerm.trim().length() > 1 || Utils.containsDoubleByteCharacters(searchTerm)) {

			List<Search.SearchResult> searchResults = this.getSOSLResults(searchTerm.trim());
			Integer indicatorLiveTime = this.getUpdateIndicatorTime();

			for (Search.SearchResult searchResult: searchResults) {
				results.add(
					this.getInitialSearchResultWrapper(
						searchResult.getSObject(),
						searchResult.getSnippet(),
						indicatorLiveTime
					)
				);
			}
		}
		return results;
	}

	private List<Search.SearchResult> getSOSLResults(String searchTerm) {
		CustomerExtensionBase uniqueClassMember = Utils.getNewBaseUniqueToolsInstance();
		List<String> typesList = new List<String> (Utils.prepareTypesSettings(false).keySet());
		String snippetLength = Utils.getValueFromConciergeSettings(SNIPPET_LENGTH_SETTING_NAME);
		String channelFilter = Utils.getValueFromConciergeSettings(CHANNEL_FILTER_SETTING_NAME);
		//@IMPORTANT: implemented article filters by Language
		String locale = Utils.getUserLanguage();
		//@IMPORTANT: implemented article filters by Data Category
		List<String> articleFilters = Utils.getArticleFilters();

		Integer MAX_RESULTS = Integer.valueOf(cncrg__Search_Engine_Configurations__c.getInstance(ENGINE_CONFIG_SETTING_NAME).cncrg__Maximum_number_of_articles_to_retrieve__c);

		String query = 'FIND \'' + Utils.escapeSOSLQuery(searchTerm) + '\' IN ALL FIELDS RETURNING ';
		query += KNOWLEDGE_OBJECT_NAME + ' ( ';
		query += String.join((Iterable<String>)this.getRequiredFieldsForSearch(), ',');
		query += ' WHERE ArticleType IN :typesList AND PublishStatus = \'' + ONLINE_PUBLISH_STATUS + '\' '
					+ ' AND Language = \'' + locale + '\' ';
		if (String.isNotBlank(channelFilter)) {
			query += ' AND ' + CHANNELS_MAP.get(channelFilter) + ' = true ';
		}
		query += ') ';

		if (!articleFilters.isEmpty() && String.isNotBlank(articleFilters.get(0))) {
			query += ' WITH DATA CATEGORY ' + articleFilters.get(0);
		}
		query += ' WITH SNIPPET ' + (String.isNotBlank(snippetLength) ? '(target_length=' + snippetLength + ')' : '');
		query += ' LIMIT ' + MAX_RESULTS;

		Search.SearchResults searchResults = Search.find(query);
		return searchResults.get(KNOWLEDGE_OBJECT_NAME);
	}

	public override List<Object> getFavoritesbyId(List<String> recordIds, String prefix) {
		//Check FLS/CRUD enforcements for Knowledge Article Types
		List<String> typesList = new List<String> (Utils.prepareTypesSettings(false).keySet());

		String channelFilter = Utils.getValueFromConciergeSettings(CHANNEL_FILTER_SETTING_NAME);
		String locale = Utils.getUserLanguage();
		String publishStatus = ONLINE_PUBLISH_STATUS;

		String query = 'SELECT Id, KnowledgeArticleId, Title, UrlName, ArticleType, CreatedDate, LastModifiedDate ';
		query += ' FROM ' + (!isTest ? KNOWLEDGE_OBJECT_NAME : 'cncrg__Knowledge__kav');
		query += ' WHERE ' + (!Test.isRunningTest() ? 'ArticleType IN :typesList AND KnowledgeArticleId IN: recordIds AND ' : '') 
							+ ' PublishStatus = :publishStatus '
							+ ' AND Language = :locale '
							+ ' AND IsLatestVersion = true ';

		if (String.isNotBlank(channelFilter)) {
			query += ' AND ' + CHANNELS_MAP.get(channelFilter) + ' = true';
		}
		query += ' LIMIT 50000';

		List<FavoriteArticlesWrapper> result = new List<FavoriteArticlesWrapper>();
		for (SObject item : Database.query(query)) {
			FavoriteArticlesWrapper favorite = new FavoriteArticlesWrapper(item, 'blankBody', (String) item.get('Title'));
			favorite.knowledgeArticleId = (String)item.get('KnowledgeArticleId');
			result.add(favorite);
		}
		return result;
	}

	public override FavoriteArticlesWrapper getFavoriteArticleDetail(FavoriteArticlesWrapper favorite, String prefix) {
		Map<String, Utils.SObjectWrapper> mapOfTypesSettings = Utils.prepareTypesSettings(false);
		if (mapOfTypesSettings.isEmpty()) {
			return favorite;
		}

		String sObjectType = this.getArticleType(favorite.article);
		String favoriteId = (String) favorite.article.get('KnowledgeArticleId');
		String locale = Utils.getUserLanguage();
		String publishStatus = ONLINE_PUBLISH_STATUS;
		Utils.SObjectWrapper objectConfig = mapOfTypesSettings.get(sObjectType);

		//'FIND \'*00*\' IN ALL FIELDS RETURNING ';
		Set<String> fieldsToQuery = new Set<String>();
		fieldsToQuery.add('Id');
		fieldsToQuery.addAll(this.getRequiredFieldsForSearch());
		fieldsToQuery.addAll(objectConfig.availableFieldsList);
		Set<String> hidenSystemFields = Utils.splitUnionFields(new Map<String, Utils.SObjectWrapper>{
			sObjectType => objectConfig
		});
		fieldsToQuery.addAll(hidenSystemFields);

		String query = 'SELECT ' + String.join((Iterable<String>)fieldsToQuery, ',');
		query += ' FROM ' + sObjectType;
		query += ' WHERE KnowledgeArticleId =: favoriteId ';
		query += ' AND PublishStatus =: publishStatus ';
		query += ' AND Language =: locale ';
		query += ' UPDATE VIEWSTAT ';

		sObject searchResult = Database.query(query);
		favorite.showRelatedAttachments = objectConfig.showAttachments;
		favorite.body = this.generateFavoriteBody(searchResult, prefix, objectConfig);
		return favorite;
	}

	public override String getArticleType(sObject article) {
		return (String) article.get('ArticleType');
	}

	public override Set<String> getRequiredFieldsForSearch() {
		Set<String> initialSet = new Set<String> {
			'Id',
			'Title',
			'Summary',
			'ArticleType',
			'CreatedDate',
			'LastModifiedDate',
			'KnowledgeArticleId',
			'LastPublishedDate',
			'FirstPublishedDate',
			'OwnerId'
		};
		if (Utils.isArticlesHaveMasterVersionIdField()) {
			initialSet.add('MasterVersionId');
		}
		return initialSet;
	}
}