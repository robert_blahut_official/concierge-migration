public interface SearchEngineInterface {

	List<SearchPhraseWrapper> getSuggestionsForQuery(String query);

	List<SearchResultsWrapper> getSearchResults(String query, String prefix);

	List<Object> getFavoritesbyId(List<String> recordIds, String prefix);

	String rateItem(SearchResultsWrapper item);

	String toggleFavoriteAPEX(String recordId, Boolean isFavorite);

	SearchResultsWrapper getAllArticleSettings(SearchResultsWrapper articleRecord, String prefix);

	FavoriteArticlesWrapper getFavoriteArticleDetail(FavoriteArticlesWrapper favorite, String prefix);

	SearchResultsWrapper getArticleDetail(SearchResultsWrapper article, String prefix);
}