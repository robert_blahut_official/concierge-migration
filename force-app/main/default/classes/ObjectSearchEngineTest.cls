@IsTest
private class ObjectSearchEngineTest {

	public static void insertSettings() {
		List<sObject> settings = new List<sObject>();
		Search_Types_Settings__c newCaseSettings = new Search_Types_Settings__c(
			Name = 'Case',
			cncrg__Active__c = true,
			cncrg__Fields_To_Display_1__c = 'Subject',
			cncrg__Fields_To_Display_2__c = 'Id',
			cncrg__Fields_To_Display_3__c = 'Attachments',
			cncrg__Fields_To_Display_4__c = 'CreatedDate',
			cncrg__Is_Usual_Object__c = true,
			cncrg__Title_Field__c = 'Origin',
			cncrg__Article_Type_API_Name__c = 'Case',
			cncrg__Field_Filters__c = 'id;!=;null'
		);
		settings.add(newCaseSettings);
		insert settings;
	}

	@IsTest
	static void getSearchResultsTest() {
		insertSettings();
		Concierge_Settings__c indicatorSettings = new Concierge_Settings__c();
		indicatorSettings.Name = 'Hours New/Updated Flag is Displayed';
		indicatorSettings.cncrg__Value__c='24';
		insert indicatorSettings;
		Account newAccount = new Account(
			Name = 'newAccount'
		);
		insert newAccount;
		Contact newContact = new Contact(
			LastName = 'Brown',
			FirstName = 'Kate',
			AccountId = newAccount.Id
		);
		insert newContact;
		Case newCase = new Case(
			AccountId = newAccount.Id,
			ContactId = newContact.Id,
			Subject = 'salesforce'
		);
		Case newIndicatorCase = new Case(
			AccountId = newAccount.Id,
			ContactId = newContact.Id,
			Subject = 'salesforce'
		);
		List<Case> casesList = new List<Case>{newCase,newIndicatorCase};
		insert casesList;
		Test.setCreatedDate(newCase.Id,  Date.today().addMonths(-25));
		Attachment newAttachment = new Attachment(
			Name ='newAttachment',
			ParentId = newCase.Id,
			Body = Blob.valueOf('Body')
		);
		insert newAttachment;
		ObjectSearchEngine newController = new ObjectSearchEngine();
		newController.DeleteIndex('indexId');
		newController.IndexDocuments();
		List<Id> fixedSearchResults = new List<Id>();
		fixedSearchResults.add(newCase.Id);
		Test.setFixedSearchResults(fixedSearchResults);
		List<SearchResultsWrapper> result = newController.getSearchResults('salesforce', '');
		System.assert(result.size() > 0);
		System.assert(result[0].type == 'Case');
		System.assert(result[0].snippet == 'salesforce');
	}

	@IsTest
	static void getSearchResultsTestSecond() {
		ObjectSearchEngine newController = new ObjectSearchEngine();
		List<SearchResultsWrapper> result = newController.getSearchResults('salesforce', '');
		System.assert(result.size() == 0);
	}

	@IsTest
	static void getSearchResultsTestThird() {
		List<sObject> settings = new List<sObject>();
		Search_Types_Settings__c newCaseSettings = new Search_Types_Settings__c(
			Name = 'AccountShare',
			cncrg__Active__c = true,
			cncrg__Fields_To_Display_1__c = 'AccountId',
			cncrg__Is_Usual_Object__c = true,
			cncrg__Title_Field__c = 'AccountAccessLevel',
			cncrg__Article_Type_API_Name__c = 'AccountShare'
		);
		settings.add(newCaseSettings);
		insert settings;
		ObjectSearchEngine newController = new ObjectSearchEngine();
		List<SearchResultsWrapper> result = newController.getSearchResults('salesforce', '');
		System.assert(result.size() == 0);
	}


	@IsTest
	static void getSuggestionsForQueryTest() {
		insertSettings();
		ObjectSearchEngine newController = new ObjectSearchEngine();
		List<SearchPhraseWrapper> result = newController.getSuggestionsForQuery('sales');
		result.add(new SearchPhraseWrapper('a', 10));
		result.add(new SearchPhraseWrapper('b', 20));
		result.sort();
		System.assert(result != null);
	}

	@IsTest
	static void getFavoritesbyIdTest() {
		insertSettings();
		ObjectSearchEngine newController = new ObjectSearchEngine();
		List<FavoriteArticlesWrapper> result = (List<FavoriteArticlesWrapper>)newController.getFavoritesbyId(new List<String>{'sales'}, '');
		System.assert(result != null);
	}

	@IsTest
	static void rateItemTest() {
		insertSettings();
//		List<Community> communities = [
//				SELECT Id
//				FROM Community
//		];
//		Idea newIdea = new Idea(
//				Title = 'Idea',
//				CommunityId = communities[0].Id
//		);
//		insert newIdea;
//		Vote newVote = new Vote(
//				ParentId = newIdea.Id,
//				Type = 'Up'
//		);
		Account newAccount = new Account(
				Name = 'newAccount'
		);
		insert newAccount;
		String title = 'salesforce';
		String snippet = 'sales';
		String recordId = newAccount.Id;
		String body = 'body';
		String type = 'KnowledgeArticleId';
		SearchResultsWrapper newWrapper = new SearchResultsWrapper(title, snippet, recordId, body, type);
		newWrapper.isLike = true;
		newWrapper.knowledgeArticleId = newAccount.Id;
		ObjectSearchEngine newController = new ObjectSearchEngine();
		String result = newController.rateItem(newWrapper);
		System.assert(result != null);
		Vote__c newCustomVote = new Vote__c(
				cncrg__Parent_Id__c =  newAccount.Id,
				cncrg__Type__c = 'Up'
		);
		insert newCustomVote;
		result = newController.rateItem(newWrapper);
		System.assert(result != null);
	}

	@IsTest
	static void toggleFavoriteAPEXTest() {
		insertSettings();
		ObjectSearchEngine newController = new ObjectSearchEngine();
		String result = newController.toggleFavoriteAPEX('recordId', true);
		System.assert(result == null);
	}

	@IsTest
	static void getCustomFavoritesArticlesTest() {
		insertSettings();
		Account newAccount = new Account(
			Name = 'newAccount'
		);
		insert newAccount;
		Contact newContact = new Contact(
			LastName = 'Brown',
			FirstName = 'Kate',
			AccountId = newAccount.Id
		);
		insert newContact;
		List<Case> caseList = new List<Case>();
		for (Integer i = 0; i < 5; i++) {
			Case newCase = new Case(
				AccountId = newAccount.Id,
				ContactId = newContact.Id,
				Subject = 'salesforce',
				Origin = 'origin'
			);
			caseList.add(newCase);
		}
		insert  caseList;
		Map<Id, Case> caseMap = new Map<Id, Case>(caseList);
		List<String> caseIdList = new List<Id>(caseMap.keySet());
		List<Object> caseResultList = ObjectSearchEngine.getCustomFavoritesArticles(caseIdList);
		System.assert(caseResultList != null);
	}

	@IsTest
	static void createIndexIdTest() {
		insertSettings();
		ObjectSearchEngine newController = new ObjectSearchEngine();
		String result = newController.CreateIndex('indexConfiguration');
		System.assert(result == null);
	}

	@IsTest
	static void getIndexStatusTest() {
		insertSettings();
		ObjectSearchEngine newController = new ObjectSearchEngine();
		String result = newController.GetIndexStatus('indexId');
		System.assert(result == null);
	}
}