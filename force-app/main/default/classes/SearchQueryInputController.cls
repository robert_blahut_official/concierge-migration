/**
* @description 

	The main class related to search logic

* @methods
	getSuggestionsForQuery -				method for getting Suggestions by search term
	getAllAplicationTabs -					method for getting tabs list for App version of Conceirge
	getGreeting -							method for getting Greetings for Concierge Home page
	getSearchResults -						method for getting Search Results by search term
	getDetailArticleInformation -			method for getting full article body
	getRelatedArticleData -					get Related article by href
	sortResults -							aggregated method for sorting search results
	getTotalSortList -						calculation total score
	sendSearchStatistic -					preparing and sending by email csv file with search results
	calculatingCountryRelevancyScore -		calculation Country Relevancy score(specific for Unilever)
	calculatingAgeScore -					calculation age score
	calculatingSearchHistoryScore -			calculation "previous user experience" article score
	calculatingRelevancyScore-				calculation Relevancy score(SF standard sorting)
	prepareSearchStagingRecord -			preparing a Staging Record and Profigliano for first search result
	rateItem -								downvoting or upvoting article
	getBGUrl -								getting backgroung image for Concierge home page
	getIsAdminProfile -						checking if the current user is an admin
	getArticleActionsSettings -				getting a list of Action Setting records for an Article record
	getArticleCaseLayoutSettings -			getting a list of Case Assignment records for an Article record
	getPrioritizedSettingRecord -			getting more Prioritized Action Setting and Case Assignment records
	getExtensionSnippet -					getting Snippet for a dynamic article
*/ 

public with sharing class SearchQueryInputController {
	public static final String LIVE_AGENT_SETTING_NAME = 'Component Name for Live Agent';
	public static final String DISPLAY_LOGO_SETTING_NAME = 'Display Company Logo';
	public static final String LOGO_SETTING_NAME = 'CompanyLogo';
	public static final String DEFAULT_LIVE_AGENT_PAGE = 'cncrg__LiveAgent';
	public static final String APEX_PAGE_PREFIX = '/apex/';
	public static final String HELP_URL_SETTING_NAME = 'It Help Url';

	@TestVisible
	public static Boolean isTest = false;
	@TestVisible
	public static Integer valueFoTestGreeting;

	@AuraEnabled
	public static List<SearchPhraseWrapper> getSuggestionsForQuery(String query) {
		List<SearchPhraseWrapper> listOfSuggestions = new List<SearchPhraseWrapper>();
		listOfSuggestions = Utils.initEngine().getSuggestionsForQuery(query);
		return listOfSuggestions;
	}

	@AuraEnabled
	public static List<Object> getInitialSettings() {
		CustomerExtensionBase customerService = Utils.getNewBaseUniqueToolsInstance();

		String helpUrl = Utils.getValueFromConciergeSettings(HELP_URL_SETTING_NAME);

		List<Object> responseList = new List<Object> ();
		responseList.add(getNumberOfRecordsOnOnePage());
		responseList.add(getIsAdminProfile());
		responseList.add(String.isNotBlank(helpUrl) ? helpUrl : '');
		responseList.add(getRelatedContactRecord());
		responseList.add(Utils.isUserHasPermitionToCreateCase());
		responseList.add(Utils.getDynamicComponentName('ActionsSettings'));
		responseList.add(customerService.hasUserPermissionToCreateCase());

		return responseList;
	}

	@AuraEnabled
	public static List<Object> doInitSearchInput() {
		List<Object> responceList = new List<Object> ();
		responceList.add(getGreeting());
		responceList.add(getUserLanguages());
		responceList.add(getAllAplicationTabs());
		return responceList;
	}

	private static List<Object> getAllAplicationTabs() {
		String settingValue = Utils.getValueFromConciergeSettings(Utils.CONCIERGE_TABS_VISIBILITY_SETTING_NAME);
		List<String> activeTabNames = String.isNotBlank(settingValue) ? settingValue.split(';') : new List<String>();
		Set<String> tabNames = new Set<String>(activeTabNames);

		if (!Case.sObjectType.getDescribe().isAccessible()) {
			tabNames.remove('My_Tickets');
		}

		List<cncrg__Application_Tab__mdt> applicationTabList = [
			SELECT Id, DeveloperName, Label, NamespacePrefix, cncrg__Component_API_Name__c, 
				cncrg__Icon_Pathes__c, cncrg__Order_Number__c, cncrg__Counter_Component_Name__c
			FROM cncrg__Application_Tab__mdt
			WHERE DeveloperName IN: tabNames
			ORDER BY cncrg__Order_Number__c
			LIMIT 50000
		];
		return applicationTabList;
	}

	public static List<String> getGreeting() {
		CustomerExtensionBase uniqueClassMember = Utils.getNewBaseUniqueToolsInstance();
		List<String> greetingInfo = uniqueClassMember.getGreeting();
		return greetingInfo;
	}

	private static String getRelatedContactRecord() {
		CustomerExtensionBase uniqueClassMember = Utils.getNewBaseUniqueToolsInstance();
		String contactRecordJSON = uniqueClassMember.getRelatedContactRecord(null);
		if (String.isNotBlank(contactRecordJSON)) {
			return contactRecordJSON;
		}
		return '';
	}

	@AuraEnabled
	public static Boolean getAlwaysDiaplayLogTicket() {
		String settingValue = Utils.getValueFromConciergeSettings('Always Display Log a Ticket');
		Boolean isAlwaysDiaplayLogTicket = String.isNotBlank(settingValue) ? Boolean.valueOf(settingValue) : true;
		return isAlwaysDiaplayLogTicket;
	}

	@AuraEnabled
	public static Boolean getAllowUsersToPrintArticlesInPDF() {
		String settingValue = Utils.getValueFromConciergeSettings('Allow Users to Print Articles in PDF');
		Boolean isAllowUsersToPrintArticlesInPDF = String.isNotBlank(settingValue) ? Boolean.valueOf(settingValue) : true;
		return isAllowUsersToPrintArticlesInPDF;
	}

	@AuraEnabled
	public static List<Object> getLiveAgentPage() {
		String liveAgentVisualforcePage = cncrg__Concierge_Settings__c.getValues(LIVE_AGENT_SETTING_NAME) != null ? cncrg__Concierge_Settings__c.getValues(LIVE_AGENT_SETTING_NAME).Value__c : DEFAULT_LIVE_AGENT_PAGE;
		CustomerExtensionBase customerExtensionBaseMember = Utils.getNewBaseUniqueToolsInstance();
		Boolean useLiveagentStartChatWithWindowMethod = Boolean.valueOf(customerExtensionBaseMember.getLiveAgentSettings(new List<String> { 'false' }) [0]);
		if (String.isBlank(liveAgentVisualforcePage)) {
			return new List<Object> { APEX_PAGE_PREFIX + DEFAULT_LIVE_AGENT_PAGE, useLiveagentStartChatWithWindowMethod };
		}
		return new List<Object> { APEX_PAGE_PREFIX + liveAgentVisualforcePage, useLiveagentStartChatWithWindowMethod };
	}

	@AuraEnabled
	public static String getLogoLastVersionId() {
		String companyLogo;
		String needToDisplay = Utils.getValueFromConciergeSettings(DISPLAY_LOGO_SETTING_NAME);
		if (String.isBlank(needToDisplay) || !Boolean.valueOf(needToDisplay)) {
			return '';
		}

		String companyLogoName = Utils.getValueFromConciergeSettings(LOGO_SETTING_NAME);
		if (String.isNotBlank(companyLogoName)) {
			List<Document> documents = new List<Document> ();
			if (ESAPI.securityUtils().isAuthorizedToView('Document', new List<String> { 'Id', 'Name' })) {
				documents = [SELECT Id FROM Document WHERE Name = :companyLogoName Limit 50000];
			}

			if (!documents.isEmpty()) {
				companyLogo = documents[0].Id;
			}
		}
		return companyLogo;
	}

	@AuraEnabled
	public static List<Object> getSearchResults(String searchStagingOldRecordStr, String contactRecord, String query, String prefix) {
		List<SearchResultsWrapper> listOfSearchResults = new List<SearchResultsWrapper>();
		listOfSearchResults = Utils.initEngine().getSearchResults(query, prefix);

		ObjectSearchEngine objectEditionInstance = new ObjectSearchEngine();
		List<SearchResultsWrapper> listOfSearchUsualObjectResults = objectEditionInstance.getSearchResults(query, prefix);
		if (listOfSearchUsualObjectResults != null && listOfSearchUsualObjectResults.size()> 0) {
			listOfSearchResults.addAll(listOfSearchUsualObjectResults);
		}

		WorkWithArticles.SearchStagingWrapper searchStagingRecord;
		if (!isTest) {
			try {
				listOfSearchResults = sortResults(listOfSearchResults, query, contactRecord);
			}
			catch(Exception e) {
				System.debug('SORT RESULTS ERROR: ' + e.getMessage() + '\n' + e.getStackTraceString());
				return new List<Object> {new List<SearchResultsWrapper>(), null };
			}
		}

		if (listOfSearchResults != null && listOfSearchResults.size() > 0) {
			if (listOfSearchResults[0].typeName == 'article') {
				listOfSearchResults[0] = Utils.initEngine().getAllArticleSettings(listOfSearchResults[0], prefix);
			} else {
				listOfSearchResults[0] = objectEditionInstance.getAllArticleSettings(listOfSearchResults[0], prefix);
			}
		}

		searchStagingRecord = prepareSearchStagingRecord(listOfSearchResults, query);

		if (String.isNotBlank(searchStagingOldRecordStr)) {
			WorkWithArticles.SearchStagingWrapper searchStagingOldRecord = (WorkWithArticles.SearchStagingWrapper) JSON.deserialize(searchStagingOldRecordStr, WorkWithArticles.SearchStagingWrapper.class);
			if (searchStagingOldRecord != null) {
				searchStagingOldRecord.saveRecord();
			}
		}
		return new List<Object> { listOfSearchResults, searchStagingRecord };
	}

	@AuraEnabled
	public static SearchResultsWrapper getDetailArticleInformation(String articleRecordStr, String prefix) {
		SearchResultsWrapper article = (SearchResultsWrapper) JSON.deserialize(articleRecordStr, SearchResultsWrapper.class);
		if (article.typeName == 'article') {
			article = Utils.initEngine().getArticleDetail(article, prefix);
		} else {
			article = new ObjectSearchEngine().getArticleDetail(article, prefix);
		}
		return article;
	}

	@AuraEnabled
	public static List<Object> getRelatedArticleData(String href, String prefix) {
		return RelatedArticleDetailsController.getInitialData(href, prefix);
	}

	@AuraEnabled
	public static SearchResultsWrapper getAllArticleSettings(String articleRecord, String prefix) {
		SearchResultsWrapper article = (SearchResultsWrapper) JSON.deserialize(articleRecord, SearchResultsWrapper.class);
		if (article.typeName == 'article') {
			article = Utils.initEngine().getAllArticleSettings(article, prefix);
		} else {
			article = new ObjectSearchEngine().getAllArticleSettings(article, prefix);
		}
		return article;
	}

	private static List<SearchResultsWrapper> sortResults(List<SearchResultsWrapper> listOfSearchResults, String query, String contactRecord) {
		Map<String, ArticleSortingWrapper> articleSortingWrapperMap = new Map<String, ArticleSortingWrapper>();
		calculatingRelevancyScore(listOfSearchResults, articleSortingWrapperMap);
		calculatingSearchHistoryScore(articleSortingWrapperMap, query);
		calculatingAgeScore(listOfSearchResults, articleSortingWrapperMap);
		calculatingVoteScore(articleSortingWrapperMap);

		articleSortingWrapperMap = calculatingCountryRelevancyScore(articleSortingWrapperMap, contactRecord);
		return getTotalSortList(articleSortingWrapperMap, listOfSearchResults, query);
	}

	private static List<SearchResultsWrapper> getTotalSortList(Map<String, ArticleSortingWrapper> articleSortingWrapperMap, List<SearchResultsWrapper> listOfSearchResults, String query) {
		Decimal SCORES_FOR_PROMOTED = 1000;
		cncrg__Search_Settings__c searchSettings = cncrg__Search_Settings__c.getOrgDefaults();

		String countryRelevancyScorePercentSetting = Utils.getValueFromConciergeSettings('Relevancy Score by Locale(Percent)');
		Decimal countryRelevancyScorePercent = 0;
		if (countryRelevancyScorePercentSetting != null) {
			countryRelevancyScorePercent = Decimal.valueOf(countryRelevancyScorePercentSetting);
		}

		List<String> totalSortList = new List<String> ();
		for (String item : articleSortingWrapperMap.keySet()) {
			ArticleSortingWrapper articleRecord = articleSortingWrapperMap.get(item);

			articleRecord.totalScore = 0.01 * articleRecord.relevancyScore * searchSettings.cncrg__Relevance__c +
			0.01 * articleRecord.searchHistoryScore * searchSettings.cncrg__Search_History__c +
			0.01 * articleRecord.ageScore * searchSettings.cncrg__Age__c +
			0.01 * articleRecord.votesScore * searchSettings.cncrg__Votes__c +
			(articleRecord.isPromoted != null && articleRecord.isPromoted ? SCORES_FOR_PROMOTED : 0);
		}

		for (SearchResultsWrapper item : listOfSearchResults) {
			ArticleSortingWrapper articleSortingWrapperRecord = articleSortingWrapperMap.get(item.knowledgeArticleId);
			item.totalScore = articleSortingWrapperRecord.totalScore;
			/////////////////////
			item.relevancyScore = articleSortingWrapperRecord.relevancyScore;
			item.searchHistoryScore = articleSortingWrapperRecord.searchHistoryScore;
			item.ageScore = articleSortingWrapperRecord.ageScore;
			item.votesScore = articleSortingWrapperRecord.votesScore;
			//////////////////////
		}
		listOfSearchResults.sort();

		for (Integer i=0, j=listOfSearchResults.size(); i<j && i<10; i++) {
			ArticleSortingWrapper articleRecord = articleSortingWrapperMap.get(listOfSearchResults[i].knowledgeArticleId);
			listOfSearchResults[i].totalScore += 0.01 * articleRecord.countryRelevancyScore * countryRelevancyScorePercent;
		}
		listOfSearchResults.sort();
		//TEMPORARILY

		try {
			cncrg__Concierge_Settings__c settingEmail = cncrg__Concierge_Settings__c.getValues('Search Log Email Address');
			cncrg__Concierge_Settings__c settingUserId = cncrg__Concierge_Settings__c.getValues('Search Log User Id (15 characters)');
			if (settingEmail != null && String.isNotBlank(settingEmail.Value__c) && settingUserId.Value__c == UserInfo.getUserId().subString(0, 15)) {
				sendSearchStatistic(JSON.serialize(articleSortingWrapperMap), JSON.serialize(listOfSearchResults), settingEmail.Value__c, query);
			}
		} catch (Exception e) {
			System.debug(' SCORE Calculation ERROR: ' + e.getMessage() + '\n' + e.getStackTraceString());
		}
		return listOfSearchResults;
	}

	@Future(callout = true)
	private static void sendSearchStatistic(String articleSortingWrapperMapStr, String listOfSearchResultsStr, String sendingEmail, String query) {
		if (sendingEmail.endsWith('@cadalys.com')) {

			String countryRelevancyScorePercentSetting = Utils.getValueFromConciergeSettings('Relevancy Score by Locale(Percent)');
			Decimal countryRelevancyScorePercent = 0;
			if (countryRelevancyScorePercentSetting != null) {
				countryRelevancyScorePercent = Decimal.valueOf(countryRelevancyScorePercentSetting);
			}

			List<SearchResultsWrapper> listOfSearchResults = (List<SearchResultsWrapper>) JSON.deserialize(listOfSearchResultsStr, List<SearchResultsWrapper>.class);
			Map<String, ArticleSortingWrapper> articleSortingWrapperMap = (Map<String, ArticleSortingWrapper>) JSON.deserialize(articleSortingWrapperMapStr, Map<String, ArticleSortingWrapper>.class);

			cncrg__Search_Settings__c searchSettings = cncrg__Search_Settings__c.getOrgDefaults();
			string header = 'Article ID, Article Title, Position in Array, Article Type, Relevancy Score, the ArticleScore, the HighestScore, Search History Score, Age, Age Score, Number of Votes, Vote Score, Relevancy Country Score, Search Score, Relevance, Search History, Age, Votes, Relevancy Score by Locale(Percent) \n';
			string finalstr = header;
			Integer position = 1;
			for (SearchResultsWrapper item : listOfSearchResults) {
				ArticleSortingWrapper articleSortingWrapperItem = articleSortingWrapperMap.get(item.knowledgeArticleId);
				string recordString = '"' + item.knowledgeArticleId + '","' + item.title + '","' + position + '","' + item.Type + '","' + articleSortingWrapperItem.relevancyScore + '", "' + articleSortingWrapperItem.scoreStrange + '", "' + articleSortingWrapperItem.highestScore + '","' + articleSortingWrapperItem.searchHistoryScore + '","' + articleSortingWrapperItem.age + '","' + articleSortingWrapperItem.ageScore + '","' + articleSortingWrapperItem.numberVotes + '","' + articleSortingWrapperItem.votesScore + '","' + articleSortingWrapperItem.countryRelevancyScore + '","' + articleSortingWrapperItem.totalScore + '","' + searchSettings.cncrg__Relevance__c + '","' + searchSettings.cncrg__Search_History__c + '","' + searchSettings.cncrg__Age__c + '","' + searchSettings.cncrg__Votes__c + '","' + countryRelevancyScorePercent + '" \n';
				finalstr = finalstr + recordString;
				position++;
			}

			Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
			csvAttc.setFileName('Search.csv');
			csvAttc.setBody(Blob.valueOf(finalstr));

			Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
			email.setSubject('Search Term: ' + query);
			email.setToAddresses(new List<string> { sendingEmail });
			email.setPlainTextBody('Search CSV ');
			email.setFileAttachments(new Messaging.EmailFileAttachment[] { csvAttc });
			Messaging.SendEmailResult[] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
		}
	}
	//end

	private static Map<String, ArticleSortingWrapper> calculatingCountryRelevancyScore(Map<String, ArticleSortingWrapper> articleSortingWrapperMap, String contactRecord) {
		CustomerExtensionBase uniqueClassMember = Utils.getNewBaseUniqueToolsInstance();
		List<String> requestList = new List<String> ();
		requestList.add(JSON.serialize(articleSortingWrapperMap));
		requestList.add(String.valueOf(contactRecord));
		return(Map<String, ArticleSortingWrapper>) JSON.deserialize(uniqueClassMember.getCountryRelevancyScore(requestList) [0], Map<String, ArticleSortingWrapper>.class);
	}

	private static void calculateVotesForUsualObject(Map<String, ArticleSortingWrapper> articleSortingWrapperMap) {
		List<Object> votesResult = new List<Object> ();
		Map<String, Integer> votesMap = new Map<String, Integer> ();
		Decimal maxVoteNumber = 0;
		for (AggregateResult item :[SELECT COUNT(Id) total, Parent_Id__c
									FROM Vote__c
									WHERE Type__c = 'Up' AND Parent_Id__c IN :articleSortingWrapperMap.keySet()
									GROUP BY Parent_Id__c
									ORDER BY COUNT(Id) DESC]) {
			if (maxVoteNumber == 0) {
				maxVoteNumber = (Integer) item.get('total');
			}
			articleSortingWrapperMap.get((String) item.get('cncrg__Parent_Id__c')).numberVotes = (Decimal) item.get('total');
			if (maxVoteNumber != 0) {
				articleSortingWrapperMap.get((String) item.get('cncrg__Parent_Id__c')).votesScore = (100 * (Decimal) item.get('total') / (Decimal) maxVoteNumber).setScale(15, RoundingMode.HALF_UP);
			}
		}
	}

	private static void calculateVotesForKnowledgeObject(Map<String, ArticleSortingWrapper> articleSortingWrapperMap) {
		List<KnowledgeArticleVoteStat> knowledgeArticleVoteStatList = [
			SELECT Id, ParentId, NormalizedScore, Channel
			FROM KnowledgeArticleVoteStat
			WHERE ParentId IN :articleSortingWrapperMap.keySet() AND Channel = 'AllChannels' AND NormalizedScore != 0
			ORDER BY NormalizedScore DESC
		];
		Decimal maxVote = 0;
		if (knowledgeArticleVoteStatList != null && knowledgeArticleVoteStatList.size() > 0) {
			for (Integer i = 0, j = knowledgeArticleVoteStatList.size(); i<j; i++) {
				if (maxVote<knowledgeArticleVoteStatList[i].NormalizedScore) {
					maxVote = knowledgeArticleVoteStatList[i].NormalizedScore;
				}
			}
		}
		for (KnowledgeArticleVoteStat item : knowledgeArticleVoteStatList) {
			articleSortingWrapperMap.get(item.ParentId).numberVotes = item.NormalizedScore;
			if (maxVote != 0) {
				articleSortingWrapperMap.get(item.ParentId).votesScore = (100 * item.NormalizedScore / (Decimal) maxVote).setScale(15, RoundingMode.HALF_UP);
			}
		}
	}

	private static void calculatingVoteScore(Map<String, ArticleSortingWrapper> articleSortingWrapperMap) {
		calculateVotesForKnowledgeObject(articleSortingWrapperMap);
		calculateVotesForUsualObject(articleSortingWrapperMap);
	}

	private static void calculatingAgeScore(List<SearchResultsWrapper> listOfSearchResults, Map<String, ArticleSortingWrapper> articleSortingWrapperMap) {
		for (SearchResultsWrapper item : listOfSearchResults) {
			if (articleSortingWrapperMap.containsKey(item.knowledgeArticleId)) {
				articleSortingWrapperMap.get(item.knowledgeArticleId).age = item.articleAge;
				if (item.articleAge != null) {
					articleSortingWrapperMap.get(item.knowledgeArticleId).ageScore = (100 * (365 - item.articleAge) / (Decimal) 365).setScale(15, RoundingMode.HALF_UP);
				}
			}
		}
	}

	private static void calculatingSearchHistoryScore(Map<String, ArticleSortingWrapper> articleSortingWrapperMap, String query) {
		try {
			String likeQuery = query + '*';
			List<cncrg__Search_Term__c> searcTerms = (List<cncrg__Search_Term__c>) (new Utils.WithoutSharingUtility()).query(
				'SELECT Id, cncrg__Strange__c ' +
				'FROM cncrg__Search_Term__c ' +
				'WHERE Name =\'' +  query + '\'' + (query.length()> 70 ? ' OR Name LIKE \'' + likeQuery + '\'' : '')
			);
			if (searcTerms.isEmpty()) {
				return;
			}

			cncrg__Search_Term__c searchTermRecord = searcTerms.get(0);
			WorkWithArticles.Encabulator encabulatorRecord;

			try {
				encabulatorRecord = (WorkWithArticles.Encabulator) JSON.deserialize(searchTermRecord.cncrg__Strange__c, WorkWithArticles.Encabulator.class);
			} catch (Exception ex) {
				WorkWithArticles.EncryptionMember member = new WorkWithArticles.EncryptionMember();
				encabulatorRecord = (WorkWithArticles.Encabulator) JSON.deserialize(member.decrypteData(searchTermRecord.cncrg__Strange__c), WorkWithArticles.Encabulator.class);
			}

			if (encabulatorRecord != null 
				&& encabulatorRecord.encabulator != null
				&& encabulatorRecord.encabulator.get(Utils.getUserLanguage()) != null) {

				List<WorkWithArticles.ArticleEncabulator> articleEncabulatorList = encabulatorRecord.encabulator.get(Utils.getUserLanguage());
				List<String> searchArticleTermIdsList = new List<String> ();
				Decimal theHighestScore = 0;
				for (WorkWithArticles.ArticleEncabulator item : articleEncabulatorList) {
					if (!articleSortingWrapperMap.containsKey(item.id)) {
						searchArticleTermIdsList.add(item.id);
					}
					if (articleSortingWrapperMap.containsKey(item.id) && theHighestScore<item.score) {
						theHighestScore = item.score;
					}
				}
				for (WorkWithArticles.ArticleEncabulator item : articleEncabulatorList) {
					if (articleSortingWrapperMap.containsKey(item.id)) {
						articleSortingWrapperMap.get(item.id).scoreStrange = item.score;
						articleSortingWrapperMap.get(item.id).highestScore = theHighestScore;
						articleSortingWrapperMap.get(item.id).searchHistoryScore = theHighestScore != 0 ? (100 * item.score / theHighestScore).setScale(15, RoundingMode.HALF_UP) : 0;
						articleSortingWrapperMap.get(item.id).isPromoted = item.isPromoted != null && item.isPromoted ? true : false;
					}
				}
			}
		}
		catch(Exception e) {
			System.debug(e.getMessage() + ': ' + e.getStackTraceString());
		}
	}

	private static void calculatingRelevancyScore(List<SearchResultsWrapper> listOfSearchResults, Map<String, ArticleSortingWrapper> articleSortingWrapperMap) {
		Map<String, List<String>> typeArticleMap = new Map<String, List<String>> ();
		for (SearchResultsWrapper item : listOfSearchResults) {
			if (item.knowledgeArticleId != null) {
				articleSortingWrapperMap.put(item.knowledgeArticleId, new ArticleSortingWrapper());
				if (typeArticleMap.containsKey(item.typeName)) {
					typeArticleMap.get(item.typeName).add(item.knowledgeArticleId);
				}
				else {
					typeArticleMap.put(item.typeName, new List<String> { item.knowledgeArticleId });
				}
			}
		}

		for (String typeName : typeArticleMap.keySet()) {
			Integer arraySize = typeArticleMap.get(typeName).size();
			for (Integer i = 0; i<arraySize; i++) {
				articleSortingWrapperMap.get(typeArticleMap.get(typeName) [i]).relevancyScore = 100 * ((arraySize - i) / (Decimal) arraySize).setScale(15, RoundingMode.HALF_UP);
			}
		}
	}

	public class ArticleSortingWrapper {
		private Decimal relevancyScore;
		private Decimal scoreStrange;
		private Decimal searchHistoryScore;
		private Integer age;
		private Decimal ageScore;
		private Decimal numberVotes;
		private Decimal votesScore;
		public Decimal countryRelevancyScore;
		private Decimal totalScore;
		private Boolean isPromoted;
		//TEMPORARILY
		private Decimal highestScore;

		public ArticleSortingWrapper() {
			relevancyScore = 0;
			scoreStrange = 0;
			searchHistoryScore = 0;
			age = 0;
			ageScore = 0;
			numberVotes = 0;
			votesScore = 0;
			totalScore = 0;
			countryRelevancyScore = 0;
			//TEMPORARILY
			highestScore = 0;
		}

		public ArticleSortingWrapper(Decimal relevancyScore) {
			this();
			this.relevancyScore = relevancyScore;
		}
	}

	private static WorkWithArticles.SearchStagingWrapper prepareSearchStagingRecord(List<SearchResultsWrapper> listOfSearchResults, String query) {
		WorkWithArticles.SearchStagingWrapper searchStagingRecord = new WorkWithArticles.SearchStagingWrapper();
		for (SearchResultsWrapper item : listOfSearchResults) {
			searchStagingRecord.biederman.add(item.knowledgeArticleId);
		}
		if (listOfSearchResults.size()> 0) {
			searchStagingRecord.profigliano.put(listOfSearchResults[0].knowledgeArticleId, new WorkWithArticles.Profigliano(null, 1, null, listOfSearchResults[0].type, listOfSearchResults[0].dataCategories, listOfSearchResults[0].title, listOfSearchResults[0].ownerId));
			searchStagingRecord.schwartz = listOfSearchResults[0].knowledgeArticleId;
		}
		searchStagingRecord.zoom = query.length()> 70 ? query.substring(0, 70) : query; // reduced from 80 to 75 04/24/2018 in case of too large encrypted string
		searchStagingRecord.up = Utils.getUserLanguage();
		if (String.isNotBlank(searchStagingRecord.zoom)) {
			searchStagingRecord.saveRecord();
		}
		return searchStagingRecord;
	}

	@AuraEnabled
	public static String rateItem(String item) {
		SearchResultsWrapper searchResultsWrapperRecord = (SearchResultsWrapper) JSON.deserialize(item, SearchResultsWrapper.class);
		return searchResultsWrapperRecord.typeName == 'article' ? Utils.initEngine().rateItem(searchResultsWrapperRecord) : new ObjectSearchEngine().rateItem(searchResultsWrapperRecord);
	}

	@TestVisible
	private static String getBGUrl(Boolean isMobile) {
		String bgName = isMobile 
			? Utils.getValueFromConciergeSettings('Background For App on Mobile Devices')
			: Utils.getValueFromConciergeSettings('Background file name for App');
		return bgName;
	}

	@AuraEnabled
	public static String toggleFavoriteAPEX(String recordId, Boolean isFavorite) {
		return Utils.initEngine().toggleFavoriteAPEX(recordId, isFavorite);
	}

	@AuraEnabled
	public static Boolean getIsAdminProfile() {
		Boolean isAdminProfile = false;
		Profile profileRecord;
		List<String> fieldsToCheck = new List<String> { 'Name', 'PermissionsModifyAllData' };
		if (ESAPI.securityUtils().isAuthorizedToView('Profile', fieldsToCheck)) {
			profileRecord = [
				SELECT Name, PermissionsModifyAllData
				FROM Profile
				WHERE Id = :UserInfo.getProfileId()
			];
		}
		if (profileRecord != null && profileRecord.PermissionsModifyAllData) {
			isAdminProfile = true;
		}
		return isAdminProfile;
	}

	@AuraEnabled
	public static Integer getNumberOfRecordsOnOnePage() {
		cncrg__Concierge_Settings__c numberOfRecords = cncrg__Concierge_Settings__c.getValues('Number of Articles Displayed in Search');
		Integer numberOfRecordsOnOnePage = 5;
		if (numberOfRecords != null) {
			numberOfRecordsOnOnePage = Integer.valueOf(numberOfRecords.cncrg__Value__c);
		}
		return numberOfRecordsOnOnePage;
	}

	public static List<cncrg__Action_Setting__c> getArticleActionsSettings(SearchResultsWrapper articleRecord, Set<String> allCategories) {
		List<cncrg__Action_Setting__c> allArticleSettings = new List<cncrg__Action_Setting__c>();
		List<String> fieldsToCheck = new List<String> {
			'cncrg__Active__c',
			'cncrg__Type__c',
			'cncrg__Source_System__c',
			'cncrg__Category__c',
			'cncrg__ArticleId__c',
			'cncrg__Log_a_Ticket__c',
			'cncrg__Support_Name__c',
			'cncrg__Support_Number__c',
			'cncrg__URL__c',
			'cncrg__URL_Label__c',
			'cncrg__Email_Subject__c',
			'cncrg__Email_Address__c',
			'cncrg__Email_Recipient_Name__c',
			'cncrg__Email__c',
			'cncrg__Live_Chat__c',
			'cncrg__Chat_Button__c',
			'cncrg__Flow_Name__c',
			'cncrg__Flow_Label__c'
		};

		CustomerExtensionBase uniqueClassMember = Utils.getNewBaseUniqueToolsInstance();
		fieldsToCheck = uniqueClassMember.getActionSettingsAvailableFields(fieldsToCheck);

		if (ESAPI.securityUtils().isAuthorizedToView('cncrg__Action_Setting__c', fieldsToCheck)) {
			String articleRecordId = articleRecord.knowledgeArticleId;
			String articleRecordType = articleRecord.type;
			String sourceSystem = articleRecord.storage;

			String queryActionSetting = 'SELECT ' + String.join(fieldsToCheck, ',');
			queryActionSetting += ' FROM cncrg__Action_Setting__c ';
			queryActionSetting += ' WHERE cncrg__Active__c = true ';
			queryActionSetting += (String.isNotBlank(sourceSystem) ? ' AND cncrg__Source_System__c =: sourceSystem ' : '');
			queryActionSetting += ' AND (' +
				' cncrg__ArticleId__c =: articleRecordId ' +
				//' OR cncrg__Source_System__c =: sourceSystem ' +
				' OR cncrg__Type__c =: articleRecordType OR cncrg__Category__c IN: allCategories ' +
				' OR (cncrg__ArticleId__c = null AND cncrg__Type__c = null AND cncrg__Category__c = null) ' +
			' ) ' +
			' ORDER BY CreatedDate '+
			' DESC LIMIT 50000';

			allArticleSettings = Database.query(queryActionSetting);
		}
		return allArticleSettings;
	}

	public static List<cncrg__Case_Layout_Assignment__c> getArticleCaseLayoutSettings(SearchResultsWrapper articleRecord, Set<String> allCategories) {
		List<cncrg__Case_Layout_Assignment__c> articleCaseLayoutSettings = new List<cncrg__Case_Layout_Assignment__c> ();
		List<String> fieldsToCheckCaseLayout = new List<String> {
			'cncrg__Article_Id__c',
			'cncrg__Article_Type__c',
			'cncrg__Data_Category__c',
			'cncrg__Field_Set__c'
		};

		if (ESAPI.securityUtils().isAuthorizedToView('cncrg__Case_Layout_Assignment__c', fieldsToCheckCaseLayout)) {
			articleCaseLayoutSettings = [
				SELECT Id, cncrg__Article_Id__c, cncrg__Article_Type__c,
					cncrg__Data_Category__c, cncrg__Field_Set__c
				FROM cncrg__Case_Layout_Assignment__c
				WHERE (cncrg__Article_Id__c = :articleRecord.knowledgeArticleId OR
						cncrg__Article_Type__c = :articleRecord.type OR
						cncrg__Data_Category__c IN :allCategories OR
						(
							cncrg__Article_Id__c = null AND
							cncrg__Article_Type__c = null AND
							cncrg__Data_Category__c = null
						)
					)
				ORDER BY CreatedDate DESC
				LIMIT 50000
			];
		}
		return articleCaseLayoutSettings;
	}

	public static sObject getPrioritizedSettingRecord(List<sObject> allSettings, Set<String> categories, SearchResultsWrapper res) {
		if (allSettings == null || allSettings.size() == 0) {
			return null;
		}

		categories = categories != null ? categories: new Set<String>();
		Boolean isActionSettingObject = (String.valueOf(allSettings[0].getSObjectType()) ==  'cncrg__Action_Setting__c');
		String articleIdFieldName = (isActionSettingObject ? 'cncrg__ArticleId__c' : 'cncrg__Article_Id__c');
		String categoryFieldName = (isActionSettingObject ? 'cncrg__Category__c' : 'cncrg__Data_Category__c');
		String typeFieldName = (isActionSettingObject ? 'cncrg__Type__c' : 'cncrg__Article_Type__c');

		Map<Integer, sObject> settingsPriorityMap = new Map<Integer, sObject>();
		for (sObject setting : allSettings) {

			String articleId = (String) setting.get(articleIdFieldName);
			String category = (String) setting.get(categoryFieldName);
			String type = (String) setting.get(typeFieldName);
			String source = (isActionSettingObject ? (String)setting.get('cncrg__Source_System__c') : '');

			if (String.isNotBlank(articleId) && res.knowledgeArticleId != articleId) {
				break;
			}

			if (String.isNotBlank(articleId) && res.knowledgeArticleId == articleId) {
				settingsPriorityMap.put(1, setting);
				break;
			}

			if (String.isNotBlank(type) 
					&& String.isNotBlank(category)
					&& type == res.type
					&& categories.contains(category)) {

				settingsPriorityMap.put(2, setting);
			}
			else if (String.isBlank(type)
						&& String.isNotBlank(category)
						&& categories.contains(category)) {

				settingsPriorityMap.put(3, setting);
			}
			else if (String.isBlank(category)
						&& String.isNotBlank(type)
						&& type == res.type) {

				settingsPriorityMap.put(4, setting);
			}
			else if (isActionSettingObject 
					&& String.isBlank(type)
					&& String.isBlank(category)
					&& source == res.storage) {

				settingsPriorityMap.put(5, setting);
			}
			else if (String.isBlank(type) && String.isBlank(category)) {

				settingsPriorityMap.put(6, setting);
			}
		}

		sObject toReturn = allSettings[0].getSObjectType().newSObject();
		if (!settingsPriorityMap.isEmpty()) {
			List<Integer> priorityList = new List<Integer>(settingsPriorityMap.keySet());
			priorityList.sort();
			toReturn = settingsPriorityMap.get(priorityList[0]);
		}
		return toReturn;
	}

	@TestVisible
	public static SearchResultsWrapper getPrioritizedSettingRecord(SearchResultsWrapper res, Set<String> allCategories) {
		CustomerExtensionBase uniqueClassMember = Utils.getNewBaseUniqueToolsInstance();
		List<cncrg__Case_Layout_Assignment__c> allCaseLayoutSettings = getArticleCaseLayoutSettings(res, allCategories);
		res.settingCaseLayout = (cncrg__Case_Layout_Assignment__c) getPrioritizedSettingRecord(allCaseLayoutSettings, allCategories, res);
		res.setting = (cncrg__Action_Setting__c) JSON.deserialize(uniqueClassMember.getArticleActionSetting(new List<String> { JSON.serialize(res), JSON.serialize(allCategories) })[0], cncrg__Action_Setting__c.class);
		return res;
	}

	@AuraEnabled
	public static String getApplicationName() {
		return Utils.getValueFromConciergeSettings('Application Name');
	}

	@AuraEnabled
	public static BackgroundWrapper getBackgroundSettings(String bgName, Boolean isMobile) {
		if (String.isBlank(bgName)) {
			bgName = getBGUrl(isMobile);
		}
		CustomerExtensionBase uniqueClassMember = Utils.getNewBaseUniqueToolsInstance();
		return new BackgroundWrapper(uniqueClassMember.getBackgroundWrapper(new List<String> { bgName, String.valueOf(isMobile) }) [0]);
	}

	public class BackgroundWrapper {
		@AuraEnabled
		public String Name { get; set; }
		@AuraEnabled
		public String BackgroundFileType { get; set; }
		@AuraEnabled
		public String FileLocation { get; set; }
		@AuraEnabled
		public String LandscapeDocumintId { get; set; }
		@AuraEnabled
		public String PortraitDocumintId { get; set; }
		@AuraEnabled
		public Boolean IsStaticResurces { get; set; }

		public BackgroundWrapper(String settingsRecordMapJSON) {
			Map<String, Object> settingsRecordMap = (Map<String, Object>) JSON.deserializeUntyped(settingsRecordMapJSON);
			Name = (String) settingsRecordMap.get('name');
			BackgroundFileType = (String) settingsRecordMap.get('backgroundFileType');
			FileLocation = (String) settingsRecordMap.get('fileLocation');
			IsStaticResurces = (Boolean) settingsRecordMap.get('isStaticResurces');
			PortraitDocumintId = (String) settingsRecordMap.get('portraitDocumintId');
			LandscapeDocumintId = (String) settingsRecordMap.get('landscapeDocumintId');
		}
	}

	@AuraEnabled
	public static void saveSearchStagingRecord(String searchStagingRecordStr) {
		WorkWithArticles.SearchStagingWrapper searchStagingRecord = (WorkWithArticles.SearchStagingWrapper) JSON.deserialize(searchStagingRecordStr, WorkWithArticles.SearchStagingWrapper.class);
		searchStagingRecord.saveRecord();
	}

	@AuraEnabled
	public static Boolean getUserLanguages() {
		return User.getSObjectType().getDescribe().fields.getMap().containsKey('HRS_User_Search_Language__c');
	}

	@AuraEnabled
	public static String checkLanguage() {
		return UserInfo.getLanguage();
	}

	@AuraEnabled
	public static String getExtensionSnippet(String className) {
		try {
			Type t = Type.forName(className);
			ExtensionComponentInterface v = (ExtensionComponentInterface)t.newInstance();
			return v.getSnippetValue(null)[0];
		}
		catch(Exception e) {
			return '';
		}
	}
}