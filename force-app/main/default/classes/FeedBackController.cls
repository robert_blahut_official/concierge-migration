public with sharing class FeedBackController {

    @AuraEnabled
    public static void moveFeedbackToCase(Case caseWithFeedBack) {
		Utils.WithoutSharingUtility withoutSharingUtilityInstance = new Utils.WithoutSharingUtility();
		withoutSharingUtilityInstance.updateCases(new List<Case> {caseWithFeedBack});
    }

    @AuraEnabled
    public static Case getCurrentCase(Id caseid) {
        Case currentCase = new Case();
		List<String> fieldsToCheck = new List<String>{'cncrg__Suggestion__c',
													  'cncrg__Score__c',
													  'cncrg__Comments__c'
													};
      
		if(ESAPI.securityUtils().isAuthorizedToView('Case',fieldsToCheck)){
			
            currentCase = [SELECT Id,
                                  cncrg__Suggestion__c,
                                  cncrg__Score__c,
                                  cncrg__Comments__c
                           FROM   Case
                           WHERE  Id =: caseid
                          ];
		}
   
        return currentCase;
    }
}