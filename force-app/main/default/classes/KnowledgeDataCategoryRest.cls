@RestResource(urlMapping = '/ArticleAccessAttributes/*')
global with sharing class KnowledgeDataCategoryRest {

	@HttpGet
	global static List<KnowledgeResponseWrapper> doGet() {
		RestRequest req = RestContext.request;
		RestResponse res = RestContext.response;
		if (res != null) {
			res.addHeader('Content-Type', 'application/json');
		}
		return getArticlesAccessSettings();
	}

	@HttpPost
	global static List<KnowledgeResponseWrapper> doPost() {
		RestRequest req = RestContext.request;
		RestResponse res = RestContext.response;
		if (res != null) {
			res.addHeader('Content-Type', 'application/json');
		}

		List<String> articleIdList = (List<String>)JSON.deserialize(req.requestBody.toString(), List<String>.class);
		return getArticlesAccessSettings(articleIdList);
	}

	private static List<KnowledgeResponseWrapper> getArticlesAccessSettings() {
		Map<String, Schema.DescribeSObjectResult> knowledgeObjectsInfo = Utils.getAllKnowledgeObjects();
		Map<String, List<String>> knowledgeIdsByTypeMap = new Map<String, List<String>>();
		for (String typeName: knowledgeObjectsInfo.keySet()) {
			knowledgeIdsByTypeMap.put(typeName, new List<String>());
		}
		return getArticlesData( knowledgeIdsByTypeMap );
	}

	private static List<KnowledgeResponseWrapper> getArticlesAccessSettings(List<String> articleIdList) {
		Map<String, String> keysMap = new Map<String, String>();
		Map<String, Schema.DescribeSObjectResult> knowledgeObjectsInfo = Utils.getAllKnowledgeObjects();
		for (String articleType: knowledgeObjectsInfo.keySet()) {
			keysMap.put( knowledgeObjectsInfo.get(articleType).getKeyPrefix() , articleType);
		}

		Map<String, List<String>> knowledgeIdsByTypeMap = new Map<String, List<String>>();
		for (String articleId: articleIdList) {
			String keyPrefix = keysMap.get(articleId.substring(0, 3));
			if (!knowledgeIdsByTypeMap.containsKey(keyPrefix)) {
				knowledgeIdsByTypeMap.put(keyPrefix, new List<String>());
			}
			knowledgeIdsByTypeMap.get(keyPrefix).add(articleId);
		}
		return getArticlesData( knowledgeIdsByTypeMap );
	}

	private static List<KnowledgeResponseWrapper> getArticlesData(Map<String, List<String>> knowledgeIdsByTypeMap) {
		List<KnowledgeResponseWrapper> result = new List<KnowledgeResponseWrapper>();
		for (String articleType : knowledgeIdsByTypeMap.keySet()) {
			String query = prepareQuery(articleType, knowledgeIdsByTypeMap.get(articleType));

			for (List<sObject> articles: Database.query(query)) {
				for (sObject article: articles) {
					result.add(new KnowledgeResponseWrapper(article));
				}
			}
		}
		return result;
	}

	private static String prepareQuery(String articleType, List<String> articleIds) {
		String sObjectApiName = Utils.isLightningKnowledgeEnabled() ? Utils.getLightningKnowledgeObjectName() : articleType;
		String requiredFields = Utils.isLightningKnowledgeEnabled() ? ' RecordType.Name, ' : ' ArticleType, ';

		String query = 'SELECT Id, KnowledgeArticleId, ' + requiredFields
						+ '(SELECT Id, DataCategoryName, DataCategoryGroupName FROM DataCategorySelections) '
						+ 'FROM ' + sObjectApiName;

		if (articleIds != null && !articleIds.isEmpty()) {
			query += ' WHERE Id IN (\'' + String.join(articleIds, '\',\'') + '\')';
		}
		return query;
	}

	global class KnowledgeResponseWrapper {
		public String sfdxArticleId {get; set;}
		public String accessattributes {get; set;}
		// sample access attributes
		// 'sfdcRole: admin, sfdcProfileName: administrator, SFDCArticleType: TypeDevNam1, SFDCArticleType: TypeDevNam2';

		public KnowledgeResponseWrapper(sObject article) {
			this.sfdxArticleId = article.Id;

			List<String> accessSettings = new List<String>();
			accessSettings.add(this.getArticleTypeAccess(article));
			accessSettings.addAll( this.getDataCategoryAccess(article) );

			this.accessattributes =  String.join(accessSettings, ',') ;
		}

		private String getArticleTypeAccess(sObject art) {
			Boolean isLightning = Utils.isLightningKnowledgeEnabled();
			sObject typeAttrSObject = isLightning ? art.getSObject('RecordType'): art;
			String output = 'SFDCArticleType:';
			if (typeAttrSObject != null) {
				Object typeAttr = isLightning ? typeAttrSObject.get('Name') : typeAttrSObject.get('ArticleType');
				output += (String) typeAttr;
			}
			return output;
		}

		private List<String> getDataCategoryAccess(sObject article) {
			List<String> result = new List<String>();
			for (sObject dataCategory: article.getSObjects('DataCategorySelections')) {
				result.add('SFDCDataCategory:' + (String)dataCategory.get('DataCategoryGroupName') + '.' + (String)dataCategory.get('DataCategoryName'));
			}
			return result;
		}
	}
}