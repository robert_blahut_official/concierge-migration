trigger ActionSettingOnChange on Action_Setting__c (before insert, before update) {
    for (Action_Setting__c setting : Trigger.new) {
        if ((setting.cncrg__Type__c != null || setting.cncrg__Category__c != null) && setting.cncrg__ArticleId__c != null) {
            setting.addError(Label.Action_Settings_Validation_Error);
        }
    }
}