trigger CaseSharing on Case (after insert, before update) {
    Trigger_Activator__c triggerActivatorRecord = Trigger_Activator__c.getInstance('Case Sharing Trigger');
    if(triggerActivatorRecord != null && triggerActivatorRecord.Active__c) {
        if(Trigger.isInsert) {
            CaseSharingHandler.insertExecute(Trigger.new);   //shareAccess(Trigger.new);
        }
        else {
            CaseSharingHandler.updeteExecute(Trigger.new, Trigger.old);
        }
    }
}