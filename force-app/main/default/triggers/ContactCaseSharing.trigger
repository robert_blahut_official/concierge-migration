trigger ContactCaseSharing on Contact (before update) {
	if (!System.isFuture() && !System.isBatch()) {
		Trigger_Activator__c triggerActivatorRecord = Trigger_Activator__c.getInstance('Contact Case Sharing Trigger');
		if(triggerActivatorRecord != null && triggerActivatorRecord.Active__c) {
			Set<Id> contactIdSet = new Set<Id>();
			for(Integer i=0, j=Trigger.new.size(); i<j; i++) {
				if(Trigger.new[i].cncrg__UserId__c != Trigger.old[i].cncrg__UserId__c && String.isNotBlank(Trigger.new[i].cncrg__UserId__c)) {
					contactIdSet.add(Trigger.new[i].Id);
				}
			}
			if(contactIdSet.size() > 0) {
				Database.executeBatch(new ContactCaseSharingBatch(contactIdSet), 2000);
			}
		}
	}
}