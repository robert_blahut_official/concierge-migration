trigger ArticleReviewTrigger on Article_Review__c (after update, before insert, after insert) {
    if(Trigger.isUpdate && Trigger.isAfter && !ArchiveArticlesBatch.isProcessed && !ArticleReviewUtils.ARTICLE_REVIEW_UPDATE) {
        Set<Id> articleForArchived = new Set<Id>();
        Map<Id, Article_Review__c> oldArticleReviewMap = Trigger.oldMap;
        Map<Id, Article_Review__c> newArticleReviewMap = Trigger.newMap;
        for(String key : newArticleReviewMap.keySet()) {
            if(oldArticleReviewMap.get(key).Expiration_Date__c != newArticleReviewMap.get(key).Expiration_Date__c
                    && newArticleReviewMap.get(key).RecordTypeId == ArticleReviewUtils.SOURCE_ARTICLE_RECORD_TYPE_ID) {
                articleForArchived.add(newArticleReviewMap.get(key).Id);
            }
            if(newArticleReviewMap.get(key).RecordTypeId != ArticleReviewUtils.SOURCE_ARTICLE_RECORD_TYPE_ID
                    && newArticleReviewMap.get(key).Expiration_Date__c != null) {
                newArticleReviewMap.get(key).Expiration_Date__c.addError(Label.Article_Review_Expiration_Date_Validation_Message);
            }
        }
        if(!articleForArchived.isEmpty()) {
            Database.executeBatch(new ArchiveArticlesBatch(articleForArchived), 1);
        }
    }
    if(Trigger.isInsert && Trigger.isBefore) {
        List<Article_Review__c> newRecords = Trigger.new;
        for(Integer i = 0; i < newRecords.size(); i++) {
            if(newRecords[i].RecordTypeId != ArticleReviewUtils.SOURCE_ARTICLE_RECORD_TYPE_ID && newRecords[i].Expiration_Date__c != null) {
                newRecords[i].Expiration_Date__c.addError(Label.Article_Review_Expiration_Date_Validation_Message);
            }
        }
    }
    if(Trigger.isInsert && Trigger.isAfter) {
        ArticleReviewUtils.ARTICLE_REVIEW_UPDATE = true;
        List<Article_Review__c> newRecords = Trigger.new;
        List<String> knowIdList = new List<String>();
        for(Integer i = 0; i < newRecords.size(); i++) {
            knowIdList.add(newRecords[i].Parent_Article__c);
        }
        List<Article_Review__c> reviewsForUpdate = ArticleReviewUtils.getArticleByParent(knowIdList);
        if(!reviewsForUpdate.isEmpty()) {
            ESAPI.securityUtils().validatedUpdate(reviewsForUpdate);
        }
        ArticleReviewUtils.ARTICLE_REVIEW_UPDATE = false;
    }
}