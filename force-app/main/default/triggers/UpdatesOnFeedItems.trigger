trigger UpdatesOnFeedItems on FeedItem (before insert) {
	
	List<String> feedItemIdList = new List<String>();
    for(FeedItem item : Trigger.new){
        if(item.Type != 'CreateRecordEvent' && item.Visibility != 'InternalUsers'){
        	feedItemIdList.add(item.ParentId);
        }
    }
	if(feedItemIdList.size() > 0) {
		Utils.getNewBaseUniqueToolsInstance().updateCasesFlag(feedItemIdList);
	}
}