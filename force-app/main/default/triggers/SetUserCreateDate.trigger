trigger SetUserCreateDate on User (before insert, before update) {
	if(Trigger.isInsert) {
		for (User userInLoop : Trigger.new) {
			userInLoop.User_Create_Date__c = DateTime.now().dateGMT();
		}
	}
	else if (!System.isFuture() && !System.isBatch()) {

		Trigger_Activator__c triggerActivatorRecord = Trigger_Activator__c.getInstance('User Case Sharing Trigger');
		if (triggerActivatorRecord != null && triggerActivatorRecord.Active__c) {
			Set<String> longActiveUserIdsList = new Set<String> ();
			Set<String> shortActiveUserIdsList = new Set<String> ();
			for(Integer i=0, j=Trigger.new.size(); i<j; i++) {
				if(Trigger.new[i].isActive && !Trigger.old[i].isActive) {
					longActiveUserIdsList.add(Trigger.new[i].Id);
					String shortUserId = ((String)Trigger.new[i].Id).subString(0, 15);
					shortActiveUserIdsList.add(shortUserId);
				}
			}
			Database.executeBatch(new ContactCaseSharingBatch(longActiveUserIdsList, shortActiveUserIdsList), 2000);
		}
	}
}